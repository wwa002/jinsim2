package kr.co.onsiter.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import kr.co.onsiter.utils.StringUtil;

public class MemberSession implements Serializable, HttpSessionBindingListener {

	public static final String SESSION_KEY = "MEMBER_SESSION";

	String uuid;

	int idx = 0;
	String type;
	String auth;
	String id;
	String name;
	
	Map<String, Object> data;
	
	public MemberSession() {
		uuid = UUID.randomUUID().toString();
	}
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		HttpSession session = SessionHolder.remove(this.uuid);
		this.uuid = uuid;
		SessionHolder.put(this.uuid, session);
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setData(Map<String, Object> item) {
		idx = Integer.parseInt(StringUtil.getContent(item.get("idx"), "0"));
		type = StringUtil.getContent(item.get("type"));
		auth = StringUtil.getContent(item.get("auth"));
		id = StringUtil.getContent(item.get("id"));
		name = StringUtil.getContent(item.get("name"));
		data = item;
	}
	
	public Map<String, Object> getData() {
		return data;
	}

	public boolean isLogin() {
		return !StringUtil.isEmpty(id);
	}

	public boolean isAdmin() {
		if (!isLogin()) {
			return false;
		}
		return "ADMIN".equals(type);
	}

	String log;
	String agent;
	
	public String getLog() {
		return log;
	}
	
	public void setLog(String log) {
		this.log = log;
	}
	
	public String getAgent() {
		return agent;
	}
	
	public void setAgent(String agent) {
		this.agent = agent;
	}
	
	Map<String, Map<String, Object>> resource;
	public void setResource(List<Map<String, Object>> res) {
		if (res == null) {
			resource = null;
			return;
		}

		resource = new HashMap<>();
		for (Map<String, Object> m : res) {
			resource.put(StringUtil.getContent(m.get("idx")), m);
		}
	}
	
	public String getResourceIdx(String name) {
		if (StringUtil.isEmpty(name)) {
			return null;
		}
		
		if (resource == null || resource.size() == 0) {
			return null;
		}
		
		Iterator<String> keys = resource.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			Map<String, Object> m = resource.get(key);
			if (m.get("name").equals(name)) {
				return key;
			}
		}
		
		return null;
	}
	
	public String getResourceIdx(String parent, String name) {
		if (StringUtil.isEmpty(parent) || StringUtil.isEmpty(name)) {
			return null;
		}
		
		if (resource == null || resource.size() == 0) {
			return null;
		}
		
		Iterator<String> keys = resource.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			Map<String, Object> m = resource.get(key);
			if (Integer.parseInt(m.get("parent").toString()) == Integer.parseInt(parent) && m.get("name").equals(name)) {
				return key;
			}
		}
		
		return null;
	}
	
	public Map<String, Map<String, Object>> getResource() {
		return resource;
	}
	
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		SessionHolder.put(uuid, event.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		SessionHolder.remove(uuid);
	}

}
