package kr.co.onsiter.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.onsiter.service.ResourceService;
import kr.co.onsiter.service.ScheduleService;

@Component
public class Scheduler {
	
	private static Logger logger = Logger.getLogger(Scheduler.class);
	
	@Autowired
	ScheduleService scheduleService;
	
	@Autowired
	ResourceService resourceService;
	
	/* Push Server Start
	static boolean isPushSend = false;
	
	@Scheduled(fixedRate = 1000)
	private void pushSend() throws Exception {
		if (isPushSend) {
			return;
		}
		
		isPushSend = true;
		if (scheduleService.isMainServer()) {
			// To Do - 푸시 발송 코드
			
			
			
		}
		isPushSend = false;
	}
	/* Push Server End */
	/*
	@Scheduled(fixedRate = 5000)
	private void sendSMS() throws Exception {
		scheduleService.sendSMS();
	}
	
	@Scheduled(fixedRate = 5000)
	private void sendMail() throws Exception {
		scheduleService.sendMail();
	}
	*/
	/* Server Management Start */
	/*
	@Scheduled(fixedRate = 60000)
	private void serverStatus() throws Exception {
		scheduleService.serverStatus();
	}
	
	@Scheduled(fixedRate = 300000)
	private void electMainServer() throws Exception {
		scheduleService.electMainServer();
	}
	*/
	/* Server Management End */

	/* Server File Cache Remove Start */
	@Scheduled(cron="0 0 1 * * *")
	public void removeTemporaryFiles() throws Exception {
		try {
			scheduleService.removeTemporaryFiles();
		} catch (Exception e) {
			logger.error(e);
		}
	}
	/* Server File Cache Remove End */
}
