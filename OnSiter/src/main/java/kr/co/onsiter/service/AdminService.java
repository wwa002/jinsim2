package kr.co.onsiter.service;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import kr.co.onsiter.mapper.AdminMapper;
import kr.co.onsiter.mapper.CommonMapper;
import kr.co.onsiter.model.Paging;
import kr.co.onsiter.model.RESTResult;
import kr.co.onsiter.utils.EncryptUtils;
import kr.co.onsiter.utils.StringUtil;

@Service
public class AdminService extends BaseService {

	@Value("${paging.num_per_page}")
	private int numPerPage;

	@Value("${paging.page_per_block}")
	private int pagePerBlock;
	
	@Value("${sms.auth1}")
	private String smsAuth1;
	
	@Value("${sms.auth2}")
	private String smsAuth2;
	
	@Value("${sms.sender}")
	private String smsSender;
	
	@Autowired
	AdminMapper adminMapper;
	
	@Autowired
	CommonMapper commonMapper;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	RestTemplate restTemplete;
	
	public RESTResult apiDashboard(HttpServletRequest request, Map<String, Object> param) throws Exception {
	
		Map<String, Object> ret = new HashMap<>();
		ret.put("counter", adminMapper.dashboardCounter(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public void loadAdmin(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("menuEvent", adminMapper.getEventList(param));
	}
	
	/* Member Start */
	public RESTResult apiCheckId(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		
		if (adminMapper.checkId(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 아이디 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiMemberList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.memberListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.memberList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiMemberInfo(HttpServletRequest request, Map<String, Object> param) throws Exception {
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, adminMapper.memberInfo(param));
		
	}
	
	@Transactional
	public RESTResult apiMemberSave(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			// 가입
			if (StringUtil.isEmpty(param.get("id"))) {
				throw new Exception("아이디를 입력해 주세요.");
			}
			
			if (adminMapper.checkId(param) > 0) {
				throw new Exception("이미 사용중인 아이디 입니다.");
			}
			
			int newIdx = adminMapper.memberNewIdx();
			param.put("idx", newIdx);
			adminMapper.memberJoin(param);
		} else {
			// 수정
			if (StringUtil.isEmpty(param.get("idx"))) {
				throw new Exception("고유정보가 누락되었습니다.");
			}
			
			Map<String, Object> item = adminMapper.memberItemForIdx(param);
			if (item == null) {
				throw new Exception("회원정보를 읽어 올 수 없습니다.");
			}
			adminMapper.memberModify(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMemberDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		Map<String, Object> item = adminMapper.memberItemForIdx(param);
		if (item == null) {
			throw new Exception("회원정보를 읽어 올 수 없습니다.");
		}
		
		adminMapper.memberDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* Member End */
	
	/* SMS Start */
	public void smsList(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("contacts", adminMapper.smsGroupListAll(param));
	}
	
	@Transactional
	public RESTResult apiSmsSend(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("content"))) {
			throw new Exception("내용을 입력해 주세요.");
		}
		
		if (StringUtil.getContent(param.get("target")).equals("P")) {
			if (StringUtil.isEmpty(param.get("mobile"))) {
				throw new Exception("휴대폰 번호를 입력해 주세요.");
			}
			if (!StringUtil.validMobile(StringUtil.getContent(param.get("mobile")))) {
				throw new Exception("올바른 형태의 휴대폰번호를 입력해 주세요. (- 포함)");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("C")) {
			if (StringUtil.isEmpty(param.get("contact"))) {
				throw new Exception("연락처를 선택해 주세요.");
			}
		} else {
			throw new Exception("발송 대상이 선택되지 않았습니다.");
		}
		
		if (!StringUtil.isEmpty(param.get("senddate")) && !StringUtil.isEmpty(param.get("sendtime"))) {
			String date = StringUtil.getContent(param.get("senddate"));
			String time = StringUtil.getContent(param.get("sendtime"));
			param.put("send_date", date.replaceAll("-", "")+time.replaceAll(":", "")+"00");
		} else if (StringUtil.isEmpty(param.get("senddate")) && StringUtil.isEmpty(param.get("sendtime"))) {
		} else {
			throw new Exception("예약발송의 날짜와 시간을 정확하게 입력해 주세요.");
		}
		
		int newIdx = adminMapper.smsNewIdx(param);
		param.put("idx", newIdx);
		
		adminMapper.smsSend(param);
		adminMapper.smsSendContact(param);
		adminMapper.smsSendTotal(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsSendList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.smsSendListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.smsSendListList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiSmsDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		adminMapper.smsDelete(param);
		adminMapper.smsDeleteContact(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsCafe24Count(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("user_id", EncryptUtils.base64Encode(smsAuth1)); // 아이디
		parameters.add("secure", EncryptUtils.base64Encode(smsAuth2)); // 인증키
		parameters.add("mode", EncryptUtils.base64Encode("1"));
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
		String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_remain.php"), entity, String.class);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, result);
	}
	
	@Transactional
	public RESTResult apiSmsContactsGroupAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		adminMapper.smsGroupAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsGroupModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		adminMapper.smsGroupModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsGroupDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		adminMapper.smsGroupDelete(param);
		adminMapper.smsGroupDeleteContacts(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsContactsGroupList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.smsGroupListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.smsGroupList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiSmsContactsContactAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호를 입력해 주세요.");
		}
		
		adminMapper.smsContactAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsContactModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호를 입력해 주세요.");
		}
		
		adminMapper.smsContactModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsContactDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		adminMapper.smsContactDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsContactsContactList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.smsContactListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.smsContactList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiSmsRefuseList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.smsRefuseListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.smsRefuseList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	/* SMS End */
	
	/* Mailer Start */
	public void mailerItem(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("item", adminMapper.mailSendItem(param));
	}
	
	public void mailerSend(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("contacts", adminMapper.mailGroupListAll(param));
		model.addAttribute("templates", adminMapper.mailTemplateListAll(param));
	}
	
	public void mailerTemplateView(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("template", adminMapper.mailTemplateItem(param));
	}
	
	@Transactional
	public RESTResult apiMailTemplateAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		adminMapper.mailTemplateAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailTemplateItem(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, adminMapper.mailTemplateItem(param));
	}
	
	@Transactional
	public RESTResult apiMailTemplateModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		adminMapper.mailTemplateModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailTemplateDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		adminMapper.mailTemplateDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailTemplateFile(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		if (StringUtil.isEmpty(param.get("tidx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 입력해 주세요.");
		}
		
		Map<String, Object> map = resourceService.upload(request, "mailer", file);
		param.put("filename", file.getOriginalFilename());
		param.put("path", map.get("path"));
		if (StringUtil.isEmpty(param.get("idx"))) {
			adminMapper.mailTemplateFileAdd(param);
		} else {
			Map<String, Object> old = adminMapper.mailTemplateFileItem(param);
			resourceService.delete(StringUtil.getContent(old.get("path")));
			adminMapper.mailTemplateFileModify(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailTemplateFileList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("tidx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, adminMapper.mailTemplateFileList(param));
	}
	
	@Transactional
	public RESTResult apiMailTemplateFileDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("tidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		Map<String, Object> map = adminMapper.mailTemplateFileItem(param);
		resourceService.delete(StringUtil.getContent(map.get("path")));
		adminMapper.mailTemplateFileItemDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailTemplateList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.mailTemplateListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.mailTemplateList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiMailSend(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("content"))) {
			throw new Exception("내용을 입력해 주세요.");
		}
		
		if (StringUtil.getContent(param.get("target")).equals("P")) {
			if (StringUtil.isEmpty(param.get("email"))) {
				throw new Exception("이메일 주소를 입력해 주세요.");
			}
			if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
				throw new Exception("올바른 형태의 메일주소를 입력해 주세요.");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("C")) {
			if (StringUtil.isEmpty(param.get("contact"))) {
				throw new Exception("연락처를 선택해 주세요.");
			}
		} else {
			throw new Exception("발송 대상이 선택되지 않았습니다.");
		}
		
		if (!StringUtil.isEmpty(param.get("senddate")) && !StringUtil.isEmpty(param.get("sendtime"))) {
			String date = StringUtil.getContent(param.get("senddate"));
			String time = StringUtil.getContent(param.get("sendtime"));
			param.put("send_date", date.replaceAll("-", "")+time.replaceAll(":", "")+"00");
		} else if (StringUtil.isEmpty(param.get("senddate")) && StringUtil.isEmpty(param.get("sendtime"))) {
		} else {
			throw new Exception("예약발송의 날짜와 시간을 정확하게 입력해 주세요.");
		}
		
		int newIdx = adminMapper.mailNewIdx(param);
		param.put("idx", newIdx);
		
		adminMapper.mailSend(param);
		adminMapper.mailSendContact(param);
		adminMapper.mailSendTotal(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailSendList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.mailSendListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.mailSendList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiMailDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		adminMapper.mailDelete(param);
		adminMapper.mailDeleteContact(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsGroupAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		adminMapper.mailGroupAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsGroupModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		adminMapper.mailGroupModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsGroupDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		adminMapper.mailGroupDelete(param);
		adminMapper.mailGroupDeleteContacts(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailContactsGroupList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.mailGroupListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.mailGroupList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiMailContactsContactAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		adminMapper.mailContactAdd(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsContactModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		adminMapper.mailContactModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsContactDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		adminMapper.mailContactDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailContactsContactList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.mailContactListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.mailContactList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiMailRefuseList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = adminMapper.mailRefuseListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", adminMapper.mailRefuseList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	/* Mailer End */
	
	/* Stats Start */
	public RESTResult apiStatsAnalytics(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, commonMapper.apiStatsAnalytics(param));
	}
	
	public RESTResult apiStatsCounter(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, commonMapper.apiStatsCounter(param));
	}
	
	public RESTResult apiStatsBrowser(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("total", commonMapper.apiStatsBrowserTotal(param));
		for (String b : StringUtil.BROWSERS) {
			param.put("browser", b);
			ret.put(b.replaceAll(" ", "_"), commonMapper.apiStatsBrowserTotal(param));
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiStatsInKeyword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, commonMapper.apiStatsInKeyword(param));
	}
	
	public RESTResult apiStatsOutKeyword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, commonMapper.apiStatsOutKeyword(param));
	}
	
	public RESTResult apiStatsReferer(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = commonMapper.apiStatRefererTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", commonMapper.apiStatReferer(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiStatsUrl(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = commonMapper.apiStatRefererTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(15);
		paging.setBlockSize(5);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", commonMapper.apiStatsUrl(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	/* Stats End */
	
	/* etc Start */
	public void etc(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = adminMapper.etcListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", adminMapper.etcList(param));
	}
	
	public void etcEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("item", adminMapper.etcItem(param));
	}
	
	public RESTResult apiEtcCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("콘텐츠 제목을 입력해 주세요.");
		}
		
		adminMapper.etcCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEtcCopy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		adminMapper.etcCopy(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEtcModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("템플릿 제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			param.put("tidx", null);
		}
		
		param.put("ismember", "Y".equals(param.get("ismember")) ? "Y" : "N");
		adminMapper.etcModify(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEtcDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		adminMapper.etcDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* etc End */
	
	/* Event Start */
	public void eventList(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = adminMapper.eventListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", adminMapper.eventList(param));
	}
	
	public void eventEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("item", adminMapper.eventItem(param));
	}
	
	public void eventContentList(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		model.addAttribute("event", adminMapper.eventItem(param));
		
		int totalCount = adminMapper.eventContentListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", adminMapper.eventContentList(param));
	}
	
	public void eventContentEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("event", adminMapper.eventItem(param));
		model.addAttribute("item", adminMapper.eventContentItem(param));
	}
	
	public void eventFeedback(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("event", adminMapper.eventItem(param));
		model.addAttribute("list", adminMapper.eventFeedbackList(param));
		if (!StringUtil.isEmpty(param.get("idx"))) {
			model.addAttribute("item", adminMapper.eventFeedbackItem(param));
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = adminMapper.eventFeedbackUserListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("feedbacks", adminMapper.eventFeedbackUserList(param));
	}
	
	public void eventFeedbackData(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("list", adminMapper.eventFeedBackDataList(param));
	}
	
	public void eventFeedbackResult(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("result", adminMapper.eventFeedBackResult(param));
		model.addAttribute("list", adminMapper.eventFeedBackResultList(param));
	}
	
	public void eventManager(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("event", adminMapper.eventItem(param));
		
		List<Map<String, Object>> dateList = adminMapper.eventDateList(param);
		for (Map<String, Object> date : dateList) {
			date.put("sessions", adminMapper.eventSessionList(date));
		}
		model.addAttribute("dateList", dateList);
		
	}
	
	public void eventSession(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("event", adminMapper.eventItem(param));
		model.addAttribute("eventDate", adminMapper.eventDateItem(param));
		model.addAttribute("item", adminMapper.eventSessionItem(param));
	}
	
	public void eventPlay(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("event", adminMapper.eventItem(param));
		model.addAttribute("eventDate", adminMapper.eventDateItem(param));
		model.addAttribute("sessions", adminMapper.eventSessionList(param));
	}
	
	public void eventDate(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		model.addAttribute("event", adminMapper.eventItem(param));
		model.addAttribute("item", adminMapper.eventDateItem(param));
	}
	
	public RESTResult apiCheckEvent(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (adminMapper.checkEvent(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 행사 코드 입니다.", RESTResult.ERROR);
		}
	}
	
	@Transactional
	public RESTResult apiEventCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("행사명을 입력해 주세요.");
		}
		
		param.put("code", param.get("code").toString().toLowerCase());
		
		if (adminMapper.checkEvent(param) > 0) {
			throw new Exception("이미 사용중인 행사 코드 입니다.");
		}
		
		adminMapper.eventCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("행사명을 입력해 주세요.");
		}
		
		adminMapper.eventModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventMain(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		adminMapper.eventMain(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventPageAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		adminMapper.eventPageAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventPageModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("페이지 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventPageModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventPageDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("페이지 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventPageDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	@Transactional
	public RESTResult apiEventDateAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edate"))) {
			throw new Exception("행사 날짜를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("place"))) {
			throw new Exception("행사 장소를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("principal1"))) {
			throw new Exception("좌장 1을 입력해 주세요.");
		}
		
		adminMapper.eventDateAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventDateModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edate"))) {
			throw new Exception("행사 날짜를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("place"))) {
			throw new Exception("행사 장소를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("principal1"))) {
			throw new Exception("좌장 1을 입력해 주세요.");
		}
		
		adminMapper.eventDateModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventDateDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		List<Map<String, Object>> sessions = adminMapper.eventSessionList(param);
		for (Map<String, Object> session : sessions) {
			if (!StringUtil.isEmpty(session.get("filepath1"))) {
				resourceService.delete(StringUtil.getContent(session.get("filepath1")));
			}
			if (!StringUtil.isEmpty(session.get("filepath2"))) {
				resourceService.delete(StringUtil.getContent(session.get("filepath2")));
			}
			
			Map<String, Object> map = new HashMap<>();
			map.put("code", session.get("code"));
			map.put("edidx", session.get("edidx"));
			map.put("sidx", session.get("idx"));
			
			List<Map<String, Object>> votes = adminMapper.eventVoteList(map);
			for (Map<String, Object> m : votes) {
				if (!StringUtil.isEmpty(m.get("filepath"))) {
					resourceService.delete(StringUtil.getContent(m.get("filepath")));
				}
			}
		}
		
		adminMapper.eventDateDelete(param);
		adminMapper.eventDateSessionDeleteAll(param);
		adminMapper.eventDateVoteDeleteAll(param);
		adminMapper.eventDateVoteResultDeleteAll(param);
		adminMapper.eventDateQuestionDeleteAll(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventSessionAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		int newIdx = adminMapper.eventSessionNewIdx();
		param.put("idx", newIdx);
		adminMapper.eventSessionAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, newIdx);
	}
	
	@Transactional
	public RESTResult apiEventSessionModify(HttpServletRequest request, Map<String, Object> param, MultipartFile file1, MultipartFile file2) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유번호를 입력해 주세요.");
		}
		
		Map<String, Object> session = adminMapper.eventSessionItem(param);
		if (file1 != null && !file1.isEmpty()) {
			Map<String, Object> f1 = resourceService.upload(request, "session", file1);
			if (f1 != null) {
				param.put("filename1", file1.getOriginalFilename());
				param.put("filepath1", f1.get("path"));
				if (!StringUtil.isEmpty(session.get("filepath1"))) {
					resourceService.delete(StringUtil.getContent(session.get("filepath1")));
				}
			}
		}
		
		if (file2 != null && !file2.isEmpty()) {
			Map<String, Object> f2 = resourceService.upload(request, "session", file2);
			if (f2 != null) {
				param.put("filename2", file2.getOriginalFilename());
				param.put("filepath2", f2.get("path"));
				if (!StringUtil.isEmpty(session.get("filepath2"))) {
					resourceService.delete(StringUtil.getContent(session.get("filepath2")));
				}
			}
		}
		
		adminMapper.eventSessionModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventSessionDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유번호를 입력해 주세요.");
		}
		
		Map<String, Object> session = adminMapper.eventSessionItem(param);
		if (!StringUtil.isEmpty(session.get("filepath1"))) {
			resourceService.delete(StringUtil.getContent(session.get("filepath1")));
		}
		if (!StringUtil.isEmpty(session.get("filepath2"))) {
			resourceService.delete(StringUtil.getContent(session.get("filepath2")));
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("code", session.get("code"));
		map.put("edidx", session.get("edidx"));
		map.put("sidx", session.get("idx"));
		
		List<Map<String, Object>> votes = adminMapper.eventVoteList(map);
		for (Map<String, Object> m : votes) {
			if (!StringUtil.isEmpty(m.get("filepath"))) {
				resourceService.delete(StringUtil.getContent(m.get("filepath")));
			}
		}
		
		adminMapper.eventSessionDelete(param);
		adminMapper.eventSessionVoteDeleteAll(param);
		adminMapper.eventSessionVoteResultDeleteAll(param);
		adminMapper.eventSessionQuestionDeleteAll(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventSessionPage(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventSessionPage(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventVoteAdd(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("행사 세션 고유번호를 입력해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			Map<String, Object> f = resourceService.upload(request, "vote", file);
			if (f != null) {
				param.put("filename", file.getOriginalFilename());
				param.put("filepath", f.get("path"));
			}
		}
		
		adminMapper.eventVoteAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventVoteModify(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("설문 고유번호를 입력해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			Map<String, Object> f = resourceService.upload(request, "vote", file);
			if (f != null) {
				param.put("filename", file.getOriginalFilename());
				param.put("filepath", f.get("path"));
				Map<String, Object> old = adminMapper.eventVoteItem(param);
				if (!StringUtil.isEmpty(old.get("filepath"))) {
					resourceService.delete(StringUtil.getContent(old.get("filepath")));
				}
			}
		}
		
		adminMapper.eventVoteModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventVoteDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유번호를 입력해 주세요.");
		}
		
		Map<String, Object> old = adminMapper.eventVoteItem(param);
		if (!StringUtil.isEmpty(old.get("filepath"))) {
			resourceService.delete(StringUtil.getContent(old.get("filepath")));
		}
		
		adminMapper.eventVoteDelete(param);
		adminMapper.eventVoteResultDeleteAll(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventVoteRun(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("보팅 고유번호를 입력해 주세요.");
		}
		adminMapper.eventVoteRun(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventVoteReset(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("보팅 고유번호를 입력해 주세요.");
		}
		adminMapper.eventVoteReset(param);
		adminMapper.eventVoteResetData(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventVoteMonitor(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("보팅 고유번호를 입력해 주세요.");
		}
		adminMapper.eventVoteMonitor(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEventVoteResult(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("보팅 고유번호를 입력해 주세요.");
		}
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("item", adminMapper.eventVoteItem(param));
		ret.put("result", adminMapper.eventVoteResult(param));
		ret.put("list", adminMapper.eventVoteResultList(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiEventVoteCurrent(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		Map<String, Object> item = adminMapper.eventVoteCurrent(param);
		if (item == null) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, null);
		}
		param.put("idx", item.get("idx"));
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("item", item);
		ret.put("result", adminMapper.eventVoteResult(param));
		ret.put("list", adminMapper.eventVoteResultList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiEventVoteList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("행사 세션 고유번호를 입력해 주세요.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, adminMapper.eventVoteList(param));
	}
	
	public RESTResult apiEventQuestionList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("speakers", adminMapper.eventQuestionSpeakers(param));
		ret.put("list", adminMapper.eventQuestionList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiEventQuestionCurrent(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜 고유번호를 입력해 주세요.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, adminMapper.eventQuestionCurrent(param));
	}
	
	@Transactional
	public RESTResult apiEventQuestionMonitor(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("질문 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventQuestionMonitor(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventQuestionPrincipal(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("질문 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventQuestionPrincipal(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventFeedbackAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		adminMapper.eventFeedbackAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventFeedbackModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("피드백 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventFeedbackModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventFeedbackOrd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("피드백 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventFeedbackOrd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventFeedbackDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("피드백 고유번호를 입력해 주세요.");
		}
		
		adminMapper.eventFeedbackDelete(param);
//		adminMapper.eventVoteResultDeleteAll(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEventFeedbackResult(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("피드백 고유번호를 입력해 주세요.");
		}
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("item", adminMapper.eventVoteItem(param));
		ret.put("result", adminMapper.eventVoteResult(param));
		ret.put("list", adminMapper.eventVoteResultList(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	/* Event End */
}
