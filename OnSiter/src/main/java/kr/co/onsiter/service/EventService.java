package kr.co.onsiter.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.onsiter.mapper.APIMapper;
import kr.co.onsiter.mapper.AdminMapper;
import kr.co.onsiter.mapper.EventMapper;
import kr.co.onsiter.mapper.ResourceMapper;
import kr.co.onsiter.model.RESTResult;
import kr.co.onsiter.utils.StringUtil;

@Service
public class EventService extends BaseService {

	private static Logger logger = Logger.getLogger(EventService.class);
	
	@Value("${paging.num_per_page}")
	private int numPerPage;

	@Value("${paging.page_per_block}")
	private int pagePerBlock;
	
	@Autowired
	AdminMapper adminMapper;
	
	@Autowired
	EventMapper eventMapper;
	
	@Autowired
	ResourceMapper resourceMapper;
	
	@Autowired
	APIMapper apiMapper;
	
	@Autowired
	ResourceService resourceService;
	
	public String index(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> html = resourceMapper.event(param);
		if (html == null) {
			return "/error/404";
		} else {
			if ("N".equals(StringUtil.getContent(html.get("status")))) {
				throw new Exception("잘못된 경로로 접근하셨습니다.");
			}
			
			model.addAttribute("html", html);
			return "/template";
		}
	}
	
	public String content(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> template = resourceMapper.event(param);
		Map<String, Object> html = resourceMapper.eventContent(param);
		if (html == null) {
			return "/error/404";
		} else {
			html.put("header", template.get("header"));
			html.put("footer", template.get("footer"));
			model.addAttribute("html", html);
			return "/template";
		}
	}
	
	public RESTResult apiAgenda(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		
		List<Map<String, Object>> dateList = adminMapper.eventDateList(param);
		for (Map<String, Object> date : dateList) {
			date.put("sessions", adminMapper.eventSessionList(date));
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, dateList);
	}
	
	public RESTResult apiSessionItem(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유정보가 누락되었습니다.");
		}
		Map<String, Object> session = adminMapper.eventSessionItem(param);
		if (StringUtil.isEmpty(session.get("filename1"))) {
			session.put("filetype", "N");
		} else if (StringUtil.getContent(session.get("filename1")).toLowerCase().endsWith("pdf")) {
			session.put("filetype", "pdf");
		} else {
			session.put("filetype", "image");
		}
		session.put("path_api", "/res/session/"+param.get("code")+"/"+param.get("idx")+"/1/"+session.get("filename1"));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, session);
	}
	
	public RESTResult apiSessions(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, eventMapper.sessions(param));
	}
	
	@Transactional
	public RESTResult apiVote(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜고유번호를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("세션 고유정보가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("투표 고유정보가 누락되었습니다.");
		}
		
		String code = StringUtil.getContent(param.get("code"));
		if (code.contains(";")) {
			param.put("code", code.split(";")[0]);
		}
		
		eventMapper.vote(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiVoteList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, eventMapper.voteItems(param));
	}
	
	@Transactional
	public RESTResult apiQuestion(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("edidx"))) {
			throw new Exception("행사 날짜고유번호를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("세션 고유정보가 누락되었습니다.");
		}
		eventMapper.question(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiFeedbackList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, eventMapper.feedbackList(param));
	}
	
	@Transactional
	public RESTResult apiFeedback(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사 코드를 입력해 주세요.");
		}

		int newIdx = eventMapper.feedbackNewIdx();
		param.put("idx", newIdx);
		eventMapper.feedback(param);
		
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			if (key.startsWith("feedback")) {
				String[] skey = key.split("-"); 
				Map<String, Object> m = new HashMap<>();
				m.put("code", param.get("code"));
				m.put("fidx", skey[1]);
				m.put("uidx", newIdx);
				String type = StringUtil.getContent(param.get(key));
				if ("S".equals(type)) {
					m.put("answer1", "N");
					m.put("answer2", "N");
					m.put("answer3", "N");
					m.put("answer4", "N");
					m.put("answer5", "N");
					m.put("answer_etc", param.get("answer-"+skey[1]+"-etc"));
				} else if("M".equals(type)) {
					m.put("answer1", StringUtil.getContent(param.get("answer1-"+skey[1]), "N"));
					m.put("answer2", StringUtil.getContent(param.get("answer2-"+skey[1]), "N"));
					m.put("answer3", StringUtil.getContent(param.get("answer3-"+skey[1]), "N"));
					m.put("answer4", StringUtil.getContent(param.get("answer4-"+skey[1]), "N"));
					m.put("answer5", StringUtil.getContent(param.get("answer5-"+skey[1]), "N"));
					m.put("answer_etc", "");
				} else {
					m.put("answer1", "N");
					m.put("answer2", "N");
					m.put("answer3", "N");
					m.put("answer4", "N");
					m.put("answer5", "N");
					m.put("answer_etc", "");
					switch (Integer.parseInt(StringUtil.getContent(param.get("answer-"+skey[1])))) {
					case 1: m.put("answer1", "Y"); break;
					case 2: m.put("answer2", "Y"); break;
					case 3: m.put("answer3", "Y"); break;
					case 4: m.put("answer4", "Y"); break;
					case 5: m.put("answer5", "Y"); break;
					}
				}
				eventMapper.feedbackData(m);
			}
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
}
