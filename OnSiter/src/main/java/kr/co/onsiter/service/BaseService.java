package kr.co.onsiter.service;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.co.onsiter.session.MemberSession;
import kr.co.onsiter.utils.StringUtil;

/**
 * @author addios4u
 * 회원 관리를 위해 필요한 부분에서 상속을 받기 위한 추상 클래스
 */
public abstract class BaseService {

	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession session = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (session == null) {
			session = new MemberSession();
			request.getSession().setAttribute(MemberSession.SESSION_KEY, session);
		}
		return session;
	}
	
	public Map<String, Object> replaceTemplate(Map<String, Object> html, Map<String, Object> data) throws Exception {
		Iterator<String> keys = html.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			html.put(key, replaceTemplate(StringUtil.getContent(html.get(key)), data));
		}
		return html;
	}
	
	public String replaceTemplate(String html, Map<String, Object> data) throws Exception {
		Iterator<String> keys = data.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			html = replaceTemplate(html, key, StringUtil.getContent(data.get(key)));
		}
		return html;
	}
	
	public String replaceTemplate(String html, String key, String data) throws Exception {
		return html.replaceAll("\\{\\{" + key + "\\}\\}", data);
	}
	
}
