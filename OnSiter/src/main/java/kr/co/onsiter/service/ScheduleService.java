package kr.co.onsiter.service;

import java.io.File;
import java.net.InetAddress;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import kr.co.onsiter.mapper.ScheduleMapper;
import kr.co.onsiter.utils.DateUtil;
import kr.co.onsiter.utils.EncryptUtils;
import kr.co.onsiter.utils.StringUtil;

@Service
public class ScheduleService extends BaseService {
	
	private static Logger logger = Logger.getLogger(ScheduleService.class);

	private static boolean isSMSSending = false;
	private static boolean isMailSending = false;
	
	@Value("${site.url}")
	private String siteUrl;
	
	@Value("${site.mail.from}")
	private String mailFrom;
	
	@Value("${sms.auth1}")
	private String smsAuth1;
	
	@Value("${sms.auth2}")
	private String smsAuth2;
	
	@Value("${sms.sender}")
	private String smsSender;
	
	@Autowired
	ScheduleMapper scheduleMapper;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	RestTemplate restTemplete;
	
	public String getServerName() throws Exception {
		return InetAddress.getLocalHost().getHostName();
	}
	
	/* Server Management Start */
	public boolean isMainServer() throws Exception {
		if (scheduleMapper.isMainServer(getServerName()) > 0) {
			return true;
		}
		return false;
	}
	
	public void serverStatus() throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("host", getServerName());
		param.put("mem_total", String.format("%.1f MB", ((double) Runtime.getRuntime().totalMemory() / (1024 * 1024))));
		param.put("mem_free", String.format("%.1f MB", ((double) Runtime.getRuntime().freeMemory() / (1024 * 1024))));
		param.put("mem_max", String.format("%.1f MB", ((double) Runtime.getRuntime().maxMemory() / (1024 * 1024))));

		if (scheduleMapper.checkServer(param) > 0) {
			scheduleMapper.updateServer(param);
		} else {
			scheduleMapper.insertServer(param);
		}
	}
	
	public void electMainServer() throws Exception {
		scheduleMapper.removeOldServer();
		if (scheduleMapper.checkMainServer() > 0) {
			return;
		}
		
		Map<String, Object> param = new HashMap<>();
		param.put("host", scheduleMapper.getNewMainServer());
		scheduleMapper.electMainServer(param);
	}
	/* Server Management End */
	
	/* SMS Service Start */
	public void sendSMS() throws Exception {
		if (!isMainServer()) {
			return;
		}
		
		if (isSMSSending) {
			return;
		}
		
		try {
			isSMSSending = true;
			
			List<Map<String, Object>> list = scheduleMapper.smsList();
			for (Map<String, Object> m : list) {
				if ("M".equals(StringUtil.getContent(m.get("send_type")))) {
					if (scheduleMapper.checkRefuseSMS(m) > 0) {
						m.put("status", "R");
						scheduleMapper.smsSendedResult(m);
						scheduleMapper.smsSendedCount(m);
						continue;
					}
				}
				
				m.put("content", replaceTemplate(StringUtil.getContent(m.get("content")), "name", StringUtil.getContent(m.get("name"))));
				
				String content = StringUtil.getContent(m.get("content"));
				String[] sender = smsSender.split("-");
				
				MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
				parameters.add("user_id", EncryptUtils.base64Encode(smsAuth1)); // 아이디
				parameters.add("secure", EncryptUtils.base64Encode(smsAuth2)); // 인증키
				parameters.add("sphone1", EncryptUtils.base64Encode(sender[0])); // 보내는 번호
				parameters.add("sphone2", EncryptUtils.base64Encode(sender[1])); // 보내는 번호
				parameters.add("sphone3", EncryptUtils.base64Encode(sender[2])); // 보내는 번호
				parameters.add("rphone", EncryptUtils.base64Encode(StringUtil.getContent(m.get("mobile")).replaceAll("-", ""))); // 받는번호
				parameters.add("msg", EncryptUtils.base64Encode(content)); // 메시지
				parameters.add("mode", EncryptUtils.base64Encode("1"));
				
				if (content.length() <= 45) {
					parameters.add("smsType", EncryptUtils.base64Encode("S"));
					
					HttpHeaders headers = new HttpHeaders();
					HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
					String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
					m.put("status", result.startsWith("success") ? "S" : "E");
				} else {
					parameters.add("smsType", EncryptUtils.base64Encode("L"));
					parameters.add("subject", EncryptUtils.base64Encode(StringUtil.getContent(m.get("title"))));
					
					HttpHeaders headers = new HttpHeaders();
					HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
					String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
					m.put("status", result.startsWith("success") ? "S" : "E");
				}
				
				scheduleMapper.smsSendedResult(m);
				scheduleMapper.smsSendedCount(m);
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			isSMSSending = false;
		}
	}
	/* SMS Service End */
	
	/* Mail Service Start */
	public void sendMail() throws Exception {
		if (!isMainServer()) {
			return;
		}
		
		if (isMailSending) {
			return;
		}
		
		try {
			isMailSending = true;
			
			List<Map<String, Object>> list = scheduleMapper.mailList();
			for (Map<String, Object> m : list) {
				boolean send = true;
				if ("M".equals(StringUtil.getContent(m.get("send_type")))) {
					if (scheduleMapper.checkRefuseMail(m) > 0) {
						m.put("status", "R");
						scheduleMapper.mailSendedResult(m);
						scheduleMapper.mailSendedCount(m);
						send = false;
					}
				}
				if (send) {
					String readCounter = "<br /><img src='"+siteUrl+StringUtil.getContent(m.get("read_url"))+"' alt='' />";
					m.put("title", replaceTemplate(StringUtil.getContent(m.get("title")), "name", StringUtil.getContent(m.get("name"))));
					m.put("content", replaceTemplate(StringUtil.getContent(m.get("content")), "name", StringUtil.getContent(m.get("name"))));
					m.put("content", StringUtil.getContent(m.get("content")) + readCounter);
					m.put("from", mailFrom);
					try {
						m.put("status", resourceService.sendFromMailer(m) ? "S" : "E");
					} catch (Exception e) {
						logger.error(e);
						m.put("status", "E");
					}
					
					scheduleMapper.mailSendedResult(m);
					scheduleMapper.mailSendedCount(m);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			isMailSending = false;
		}
	}
	/* Mail Service End */
	
	
	public void removeTemporaryFiles() throws Exception {
		Resource resource = resourceLoader.getResource("/temp");
		File temporaryDir = resource.getFile();
		if (temporaryDir.isDirectory()) {
			File[] files = temporaryDir.listFiles();
			for (File f : files) {
				if (!DateUtil.getDateString("yyyyMMdd").equals(f.getName())) {
					deleteFile(f);
				}
			}
		}
	}
	
	private void deleteFile(File path) {
		if (!path.exists()) {
			return;
		}
		
		if (path.isDirectory()) {
			File[] files = path.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					deleteFile(file);
				} else {
					file.delete();
				}
			}
		}

		path.delete();
	}
	
}
