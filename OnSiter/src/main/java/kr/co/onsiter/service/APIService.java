package kr.co.onsiter.service;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import kr.co.onsiter.mapper.APIMapper;
import kr.co.onsiter.model.RESTResult;
import kr.co.onsiter.session.MemberSession;
import kr.co.onsiter.utils.EncryptUtils;
import kr.co.onsiter.utils.StringUtil;

@Service
public class APIService extends BaseService {

	private static Logger logger = Logger.getLogger(APIService.class);
	
	@Autowired
	APIMapper apiMapper;
	
	@Autowired
	ResourceService resourceService;
	
	@Transactional
	public RESTResult login(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("passwd"))) {
			throw new Exception("비밀번호를 입력해 주세요.");
		}
		
		param.put("ip", request.getRemoteAddr());
		
		Map<String, Object> mem = apiMapper.login(param);
		if (mem == null) {
			throw new Exception("아이디와 비밀번호를 확인해 주세요.");
		}
		
		MemberSession ms = getMemberSession(request);
		ms.setData(mem);
		
		if (StringUtil.isEmpty(ms.getAuth())) {
			String auth = EncryptUtils.toMD5(UUID.randomUUID().toString());
			param.put("auth", auth);
			mem.put("auth", auth);
			ms.setAuth(auth);
			apiMapper.loginAuthUpdate(param);
		} else {
			apiMapper.loginDateUpdate(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, mem);
	}
	
	public RESTResult summernoteUpload(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드 해주세요.");
		}

		Map<String, Object> map = resourceService.upload(request, "summernote", file);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, request.getScheme() + "://" + request.getServerName()+"/api/summernote/" + StringUtil.getContent(map.get("destName")));
	}
	
	public RESTResult refuseSms(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호를 입력해 주세요.");
		}
		
		if (!StringUtil.validMobile(StringUtil.getContent(param.get("mobile")))) {
			throw new Exception("올바른 형태의 휴대폰번호를 입력해 주세요. (- 포함)");
		}
		
		if (apiMapper.checkRefuseSMS(param) > 0) {
			throw new Exception("이미 수신거부된 번호 입니다.");
		}
		
		apiMapper.refuseSms(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult refuseEmail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
			throw new Exception("올바른 형태의 메일 주소를 입력해 주세요.");
		}
		
		if (apiMapper.checkRefuseEmail(param) > 0) {
			throw new Exception("이미 수신거부된 이메일 입니다.");
		}
		
		apiMapper.refuseEmail(param);

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult acceptSms(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호를 입력해 주세요.");
		}
		
		if (!StringUtil.validMobile(StringUtil.getContent(param.get("mobile")))) {
			throw new Exception("올바른 형태의 휴대폰번호를 입력해 주세요. (- 포함)");
		}
		
		if (apiMapper.checkRefuseSMS(param) == 0) {
			throw new Exception("수신 허용된 번호 입니다.");
		}
		
		apiMapper.acceptSms(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult acceptEmail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
			throw new Exception("올바른 형태의 메일 주소를 입력해 주세요.");
		}
		
		if (apiMapper.checkRefuseEmail(param) == 0) {
			throw new Exception("수신 허용된 메일 주소 입니다.");
		}
		
		apiMapper.acceptEmail(param);

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
}
