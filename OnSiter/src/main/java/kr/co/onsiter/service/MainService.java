package kr.co.onsiter.service;

import java.nio.charset.Charset;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import kr.co.onsiter.mapper.ResourceMapper;
import kr.co.onsiter.utils.StringUtil;

@Service
public class MainService extends BaseService {

	private static Logger logger = Logger.getLogger(MainService.class);
	
	@Autowired
	ResourceMapper resourceMapper;
	
	public String index(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		String code = resourceMapper.eventMain();
		if (!StringUtil.isEmpty(code)) {
			return "redirect:/e/"+code;
		}
		return "redirect:/admin/login";
	}

	public ResponseEntity<byte[]> etcResource(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param, Model model) throws Exception {

		Map<String, Object> item = resourceMapper.resourceEtc(param);
		if (item == null) {
			throw new Exception("정보를 조회 할 수 없습니다.");
		}
		
		String content = StringUtil.getContent(item.get("content"));
		byte[] bytes = content.getBytes(Charset.defaultCharset());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);

		response.setCharacterEncoding("UTF-8");
		if ("JS".equals(item.get("type"))) {
			response.setContentType("application/javascript");
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if ("CSS".equals(item.get("type"))) {
			response.setContentType("text/css");
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if ("XML".equals(item.get("type"))) {
			headers.setContentType(MediaType.TEXT_XML);
			response.setContentType(MediaType.TEXT_XML_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
}
