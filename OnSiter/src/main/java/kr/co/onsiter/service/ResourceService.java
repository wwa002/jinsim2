package kr.co.onsiter.service;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.util.IOUtils;

import kr.co.onsiter.mapper.APIMapper;
import kr.co.onsiter.mapper.AdminMapper;
import kr.co.onsiter.mapper.ResourceMapper;
import kr.co.onsiter.model.RESTResult;
import kr.co.onsiter.session.MemberSession;
import kr.co.onsiter.utils.DateUtil;
import kr.co.onsiter.utils.EncryptUtils;
import kr.co.onsiter.utils.StringUtil;

@Service
public class ResourceService extends BaseService {

	private static Logger logger = Logger.getLogger(ResourceService.class);
	
	@Value("${aws.accessKey}")
	private String awsAccessKey;
	
	@Value("${aws.secretKey}")
	private String awsSecretKey;
	
	@Value("${aws.bucket}")
	private String awsBucket;
	
	@Value("${aws.s3endpoint}")
	private String awsS3EndPoint;
	
	@Autowired
	ResourceMapper resourceMapper;
	
	@Autowired
	APIMapper apiMapper;
	
	@Autowired
	AdminMapper adminMapper;
	
	@Autowired
	RestTemplate restTemplete;
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession();
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = apiMapper.loginForHeader(param);
				ms.setData(mem);
				apiMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	private boolean isValidMime(Object mime) {
		return isValidMime(StringUtil.getContent(mime));
	}
	
	private boolean isValidMime(String mime) {
		if (StringUtil.isEmpty(mime)) {
			return false;
		}
		String m = mime.toLowerCase();
		if (m.startsWith("text") || m.startsWith("image")) {
			return true;
		} else if (m.equals("application/font-woff") || m.equals("application/font-woff2")) {
			return true;
		}
		return false;
	}
	
	private File getTemporaryFile(HttpServletRequest request, String path) throws Exception {
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator+DateUtil.getDateString("yyyyMMdd")+File.separator;
		File file = new File(tempPath+path);
		if (!file.exists()) {
			logger.debug("Download From S3 : " + path);
			S3Object s3obj = download(path);
			S3ObjectInputStream ois = s3obj.getObjectContent();
			
			Files.createDirectories(Paths.get(file.getParentFile().getAbsolutePath()));
			IOUtils.copy(ois, new FileOutputStream(file));
		}
		return file;
	}
	
	private File getThumbnail(File file, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("width")) && StringUtil.isEmpty(param.get("height"))) {
			return file;
		} else if (StringUtil.isEmpty(param.get("width"))) {
			return getThumbnail(file, 0, Integer.parseInt(StringUtil.getContent(param.get("height"))));
		} else if (StringUtil.isEmpty(param.get("height"))) {
			return getThumbnail(file, Integer.parseInt(StringUtil.getContent(param.get("width"))), 0);
		} else {
			return getThumbnail(file, Integer.parseInt(StringUtil.getContent(param.get("width"))), Integer.parseInt(StringUtil.getContent(param.get("height"))));
		}
	}
	
	private File getThumbnail(File file, int width, int height) throws Exception {
		String filename = file.getName().toLowerCase();
		String extension = filename.substring(filename.lastIndexOf(".")+1);
		if (extension.equals("png") || extension.equals("jpg")) {
			String folder = file.getParentFile().getAbsolutePath() + File.separator + "thumbnails";
			File dir = new File(folder);
			if (!dir.exists()) {
				Files.createDirectories(Paths.get(folder));
			}

			String destname = width+"x"+height+"_"+filename;
			File dest = new File(folder+File.separator+destname);
			if (dest.exists()) {
				return dest;
			}
			
			BufferedImage bufferOriginal = ImageIO.read(file);
			if (width == 0) {
				width = (bufferOriginal.getWidth() * height)/bufferOriginal.getHeight();
			} else if (height == 0) {
				height = (bufferOriginal.getHeight() * width)/bufferOriginal.getWidth();
			}
			BufferedImage bufferThumbnail = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
			Graphics2D graphic = bufferThumbnail.createGraphics();
			graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphic.drawImage(bufferOriginal, 0, 0, width, height, null);
			ImageIO.write(bufferThumbnail, extension, dest);
			return dest;
		}
		
		return file;
	}
	
	public ResponseEntity<byte[]> imageDownload(String path, HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(path)) {
			throw new Exception("정보를 읽어올 수 없습니다.");
		}
		
		File file = getTemporaryFile(request, path);
		if (!StringUtil.isEmpty(param.get("width")) || !StringUtil.isEmpty(param.get("height"))) {
			file = getThumbnail(file, param);
		}
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (path.endsWith("png")) {
			headers.setContentType(MediaType.IMAGE_PNG);
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (path.endsWith("jpg")) {
			headers.setContentType(MediaType.IMAGE_JPEG);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (path.endsWith("gif")) {
			headers.setContentType(MediaType.IMAGE_GIF);
			response.setContentType(MediaType.IMAGE_GIF_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(StringUtil.getContent(param.get("name")), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	// for WEB
	public ResponseEntity<byte[]> resourceDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = resourceMapper.itemForFile(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (isValidMime(item.get("mime"))) {
			String mime = item.get("mime").toString();
			MediaType mt = MediaType.valueOf(mime);
			headers.setContentType(mt);
			response.setContentType(mt.toString());
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> resourceFrontDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (ms.getResource() == null) {
			ms.setResource(resourceMapper.listForResource(param));
		}
		
		Map<String, Object> item = ms.getResource().get(StringUtil.getContent(param.get("idx")));
		if (item == null) {
			ms.setResource(resourceMapper.listForResource(param));
		}
		
		item = ms.getResource().get(StringUtil.getContent(param.get("idx")));
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (isValidMime(item.get("mime"))) {
			String mime = item.get("mime").toString();
			MediaType mt = MediaType.valueOf(mime);
			headers.setContentType(mt);
			response.setContentType(mt.toString());
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> resourceFrontDownloadDir(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		String dir1= StringUtil.getContent(param.get("dir1"));
		String dir2= StringUtil.getContent(param.get("dir2"));
		String dir3= StringUtil.getContent(param.get("dir3"));
		String name= StringUtil.getContent(param.get("name"));
		if (StringUtil.isEmpty(dir1)) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (ms.getResource() == null) {
			ms.setResource(resourceMapper.listForResource(param));
		}
		
		if (!StringUtil.isEmpty(dir3)) {
			String parent = ms.getResourceIdx("0", dir1);
			parent = ms.getResourceIdx(parent, dir2);
			parent = ms.getResourceIdx(parent, dir3);
			String idx = ms.getResourceIdx(parent, name);
			param.put("idx", idx);
		} else if (!StringUtil.isEmpty(dir2)) {
			String parent = ms.getResourceIdx("0", dir1);
			parent = ms.getResourceIdx(parent, dir2);
			String idx = ms.getResourceIdx(parent, name);
			param.put("idx", idx);
		} else {
			String parent = ms.getResourceIdx("0", dir1);
			String idx = ms.getResourceIdx(parent, name);
			param.put("idx", idx);
		}
		
		return resourceFrontDownload(request, response, param);
	}
	
	public ResponseEntity<byte[]> sessionDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		if (StringUtil.isEmpty(param.get("code")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("num")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = adminMapper.eventSessionItem(param);
		if (item == null) {
			throw new Exception("세션 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("filepath"+StringUtil.getContent(param.get("num")))), request, response, param);
	}
	
	public ResponseEntity<byte[]> voteDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		if (StringUtil.isEmpty(param.get("code")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = adminMapper.eventVoteItem(param);
		if (item == null) {
			throw new Exception("투표 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("filepath")), request, response, param);
	}
	
	public ResponseEntity<byte[]> mailFileDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("tidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}

		Map<String, Object> item = adminMapper.mailTemplateFileItem(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		// 회원전용검증
		String filename = StringUtil.getContent(item.get("filename")).toLowerCase(); 
		if (filename.endsWith("png") || filename.endsWith("jpg") || filename.endsWith("jpeg") || filename.endsWith("jpe") || filename.endsWith("gif")) {
			return imageDownload(StringUtil.getContent(item.get("path")), request, response, param);
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("filename").toString(), "UTF-8").replaceAll("\\+", "%20"));
		return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
	}
	
	public ResponseEntity<byte[]> summernoteDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		String path = "summernote/" + StringUtil.getContent(param.get("name"));
		return imageDownload(path, request, response, param);
	}
	
	// for API
	private List<Map<String, Object>> listForJSTree(int parent, int depth) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("parent", parent);
		
		Map<String, Object> state = new HashMap<>();
		state.put("opened", depth == 0);
		state.put("disabled", false);
		state.put("selected", false);
		
		List<Map<String, Object>> children = new ArrayList<>();
		List<Map<String, Object>> list = resourceMapper.listForJSTree(param);
		for (Map<String, Object> m : list) {
			Map<String, Object> child = new HashMap<>();
			child.put("id", m.get("idx"));
			child.put("text", m.get("name"));
			child.put("data", m);
			child.put("state", state);
			if (!"Y".equals(m.get("isfolder"))) {
				child.put("icon", "none");
			}
			child.put("children", listForJSTree(Integer.parseInt(m.get("idx").toString()), depth + 1));
			children.add(child);
		}
		return children;
	}
	
	public JSONObject apiJSTree(HttpServletRequest request, Map<String, Object> param) throws Exception {
		JSONObject ret = new JSONObject();
		
		Map<String, Object> state = new HashMap<>();
		state.put("opened", true);
		state.put("disabled", false);
		state.put("selected", false);
		
		param.put("parent", 0);
		ret.put("id", "0");
		ret.put("text", "ROOT");
		ret.put("state", state);
		ret.put("children", listForJSTree(0, 0));
		return ret;
	}
	
	public RESTResult apiFolderList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			param.put("idx", 0);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, resourceMapper.listForFolder(param));
	}
	
	public RESTResult apiFolderAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("folder"))) {
			throw new Exception("폴더명을 입력해 주세요.");
		}
		
		if (resourceMapper.checkFolder(param) > 0) {
			throw new Exception("같은 폴더명이 존재합니다.");
		}
		
		if (StringUtil.isEmpty(param.get("parent"))) {
			param.put("parent", 0);
		}
		resourceMapper.createFolder(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	private void deleteFolder(int idx) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("idx", idx);
		
		List<Map<String, Object>> list = resourceMapper.listForFolder(param);
		for (Map<String, Object> m : list) {
			if ("Y".equals(m.get("isfolder"))) {
				deleteFolder(Integer.parseInt(m.get("idx").toString()));
			} else {
				delete(m.get("path").toString());
			}
			resourceMapper.deleteFile(m);
		}
		resourceMapper.deleteFile(param);
	}
	
	@Transactional
	public RESTResult apiDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}

		Map<String, Object> item = resourceMapper.itemForFile(param);
		if ("Y".equals(item.get("isfolder"))) {
			deleteFolder(Integer.parseInt(param.get("idx").toString()));
		} else {
			delete(item.get("path").toString());
			resourceMapper.deleteFile(item);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	@Transactional
	public RESTResult apiUploadDropzone(MultipartHttpServletRequest request, Map<String, Object> param) throws Exception {
		Iterator<String> itr = request.getFileNames();
		while (itr.hasNext()) {
			String uploadedFile = itr.next();
			MultipartFile file = request.getFile(uploadedFile);
			Map<String, Object> map = new HashMap<>();
			map.put("filename", file.getOriginalFilename());
			map.put("mimetype", file.getContentType());
			map.put("size", file.getSize());
			map.put("parent", param.get("idx"));
			map.putAll(upload(request, file));
			resourceMapper.uploadFile(map);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for AWS
	private AWSCredentials getCredentials() {
		return new BasicAWSCredentials(awsAccessKey, awsSecretKey);
	}
	
	private AmazonS3Client getS3Client() {
		AmazonS3Client client = new AmazonS3Client(getCredentials());
		client.setEndpoint(awsS3EndPoint);
		return client;
	}
	
	public String getUrl(String path) {
		return "https://"+awsS3EndPoint+"/"+awsBucket+"/"+path;
	}
	
	private Map<String, Object> upload(HttpServletRequest request, MultipartFile file) throws Exception {
		if (file == null || file.isEmpty()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator;
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = "resource/" + destName;
	
		File convertFile = new File(tempPath+destName);
		try {
			file.transferTo(convertFile);
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, convertFile);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} finally {
			convertFile.delete();
		}
		
		return map;
	}
	
	public Map<String, Object> upload(HttpServletRequest request, String path, MultipartFile file) throws Exception {
		if (file == null || file.isEmpty()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator;
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		if ("file".equals(extension)) {
			extension = "png";
		}
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = path + "/" + destName;
	
		File convertFile = new File(tempPath+destName);
		try {
			file.transferTo(convertFile);
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, convertFile);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} finally {
			convertFile.delete();
		}
		
		return map;
	}
	
	public Map<String, Object> upload(HttpServletRequest request, String path, File file) throws Exception {
		if (file == null || !file.exists()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getName().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = path + "/" + destName;
	
		try {
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, file);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} 
		return map;
	}

	private S3Object download(String path) throws Exception {
		try {
            return getS3Client().getObject(new GetObjectRequest(awsBucket, path));
        } catch (AmazonServiceException ase) {
            logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
            logger.info("Error Message:    " + ase.getMessage());
            logger.info("HTTP Status Code: " + ase.getStatusCode());
            logger.info("AWS Error Code:   " + ase.getErrorCode());
            logger.info("Error Type:       " + ase.getErrorType());
            logger.info("Request ID:       " + ase.getRequestId());
            throw ase;
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to communicate with S3, such as not being able to access the network.");
            logger.info("Error Message: " + ace.getMessage());
            throw ace;
        }
	}
	
	private void move(String path, String destPath) throws Exception{
		try {
            getS3Client().copyObject(new CopyObjectRequest(awsBucket, path, awsBucket, destPath));
            getS3Client().deleteObject(new DeleteObjectRequest(awsBucket, path));
        } catch (AmazonServiceException ase) {
        	logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
        	logger.info("Error Message:    " + ase.getMessage());
        	logger.info("HTTP Status Code: " + ase.getStatusCode());
        	logger.info("AWS Error Code:   " + ase.getErrorCode());
        	logger.info("Error Type:       " + ase.getErrorType());
        	logger.info("Request ID:       " + ase.getRequestId());
        	throw ase;
        } catch (AmazonClientException ace) {
        	logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to  communicate with S3, such as not being able to access the network.");
        	logger.info("Error Message: " + ace.getMessage());
        	throw ace;
        } catch (Exception e) {
			throw e;
		}
	}
	
	public void delete(String path) throws Exception {
		try {
			getS3Client().deleteObject(new DeleteObjectRequest(awsBucket, path));
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to communicate with S3, such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public boolean sendMail(String[] to, String from, String subject, String content) throws Exception {
		Destination destination = new Destination().withToAddresses(to);
		
		Content sesSubject = new Content().withData(subject);
		Content sesContent = new Content().withData(content);
		Body body = new Body().withText(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
	
	public boolean sendMailHtml(String[] to, String from, String subject, String content) throws Exception {
		Destination destination = new Destination().withToAddresses(to);
		
		Content sesSubject = new Content().withData(subject);
		Content sesContent = new Content().withData(content);
		Body body = new Body().withHtml(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
	
	public boolean sendFromMailer(Map<String, Object> param) throws Exception {
		Destination destination = new Destination().withToAddresses(StringUtil.getContent(param.get("email")));
		
		Content sesSubject = new Content().withData(StringUtil.getContent(param.get("title")));
		Content sesContent = new Content().withData(StringUtil.getContent(param.get("content")));
		Body body = new Body().withHtml(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(StringUtil.getContent(param.get("from"))).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Transactional
	public void mailRead(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		resourceMapper.mailRead(param);
		resourceMapper.mailReadCount(param);
	}
	
	public void sendSMS(Map<String, Object> cfg, Map<String, Object> data) throws Exception {
		String content = StringUtil.getContent(data.get("content"));
		String[] sender = StringUtil.getContent(cfg.get("sender")).split("-");
		
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("user_id", EncryptUtils.base64Encode(StringUtil.getContent(cfg.get("auth1")))); // 아이디
		parameters.add("secure", EncryptUtils.base64Encode(StringUtil.getContent(cfg.get("auth2")))); // 인증키
		parameters.add("sphone1", EncryptUtils.base64Encode(sender[0])); // 보내는 번호
		parameters.add("sphone2", EncryptUtils.base64Encode(sender[1])); // 보내는 번호
		parameters.add("sphone3", EncryptUtils.base64Encode(sender[2])); // 보내는 번호
		parameters.add("rphone", EncryptUtils.base64Encode(StringUtil.getContent(data.get("mobile")).replaceAll("-", ""))); // 받는번호
		parameters.add("msg", EncryptUtils.base64Encode(content)); // 메시지
		parameters.add("mode", EncryptUtils.base64Encode("1"));
		
		if (content.length() <= 45) {
			parameters.add("smsType", EncryptUtils.base64Encode("S"));
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
			String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
			if (!result.startsWith("success")) {
				throw new Exception("발송에 실패하였습니다.");
			}
		} else {
			parameters.add("smsType", EncryptUtils.base64Encode("L"));
			parameters.add("subject", EncryptUtils.base64Encode(StringUtil.getContent(data.get("title"))));
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
			String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
			if (!result.startsWith("success")) {
				throw new Exception("발송에 실패하였습니다.");
			}
		}
	}
}
