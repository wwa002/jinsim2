package kr.co.onsiter.web;

import java.io.File;
import java.nio.file.Files;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.onsiter.service.ResourceService;
import kr.co.onsiter.utils.StringUtil;

@Controller
@RequestMapping("/res")
public class ResController {

	private static Logger logger = Logger.getLogger(ResController.class);
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping(value={"/{dir1}/{name:.+}", "/{dir1}/{dir2}/{name:.+}", "/{dir1}/{dir2}/{dir3}/{name:.+}"}, method=RequestMethod.GET)
	public ResponseEntity<byte[]> resourcesDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String dir1, @PathVariable(required=false) String dir2, @PathVariable(required=false) String dir3, @PathVariable String name) throws Exception {
		try {
			if (StringUtil.isNumeric(dir1)) {
				param.put("idx", dir1);
				param.put("name", name);
				return resourceService.resourceFrontDownload(request, response, param);
			} else {
				param.put("dir1", dir1);
				param.put("dir2", dir2);
				param.put("dir3", dir3);
				param.put("name", name);
				return resourceService.resourceFrontDownloadDir(request, response, param);
			}
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/session/{code}/{idx}/{num}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> sessionDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx, @PathVariable int num, @PathVariable String name) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			param.put("num", num);
			param.put("name", name);
			return resourceService.sessionDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/vote/{code}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> voteDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.voteDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}

	@RequestMapping(value="/mail/read/{sidx}/{idx}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> mailRead(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int sidx, @PathVariable int idx) throws Exception {
		try {
			param.put("sidx", sidx);
			param.put("idx", idx);
			resourceService.mailRead(request, response, param);
		} catch (Exception e) {
			logger.error(e);
		}
		
		String icon = request.getSession().getServletContext().getRealPath("/")+"resources"+File.separator+"images"+File.separator+"readmail.png";
		File file = new File(icon);
		if (file.exists()) {
			try {
				byte[] bytes = Files.readAllBytes(file.toPath());
				HttpHeaders headers = new HttpHeaders();
				headers.setContentLength(bytes.length);
				headers.setContentType(MediaType.IMAGE_PNG);
				response.setContentType(MediaType.IMAGE_PNG_VALUE);
				return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
			} catch (Exception e) {
				logger.error(e);
			}
		} 
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);
		return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
	}
}
