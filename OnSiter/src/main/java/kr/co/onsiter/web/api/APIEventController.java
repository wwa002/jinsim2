package kr.co.onsiter.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.co.onsiter.model.RESTResult;
import kr.co.onsiter.service.EventService;

@RestController
@RequestMapping("/api/event")
public class APIEventController {
	
	public static Logger logger = Logger.getLogger(APIEventController.class);
	
	@Autowired
	EventService eventService;

	@RequestMapping("/agenda")
	public RESTResult agenda(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAgenda(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/{code}/{idx}")
	public RESTResult session(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return eventService.apiSessionItem(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/sessions")
	public RESTResult sessions(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiSessions(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote")
	public RESTResult vote(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiVote(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote/{code}")
	public RESTResult voteList(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return eventService.apiVoteList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/question")
	public RESTResult question(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiQuestion(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/feedback/list")
	public RESTResult feedbackList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiFeedbackList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/feedback")
	public RESTResult feedback(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiFeedback(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
