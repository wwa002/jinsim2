package kr.co.onsiter.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.co.onsiter.model.RESTResult;
import kr.co.onsiter.service.APIService;
import kr.co.onsiter.service.CommonService;
import kr.co.onsiter.service.ResourceService;

@RestController
@RequestMapping("/api")
public class APIController {

	public static Logger logger = Logger.getLogger(APIController.class);
	
	@Autowired
	APIService apiService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping
	public RESTResult index(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	@RequestMapping("/login")
	public RESTResult login(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.login(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessLogin(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/summernote/upload")
	public RESTResult summernoteUpload(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return apiService.summernoteUpload(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/summernote/{name:.+}")
	public ResponseEntity<byte[]> download(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String name) throws Exception {
		try {
			param.put("name", name);
			return resourceService.summernoteDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping("/refuse/sms")
	public RESTResult refuseSms(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.refuseSms(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/refuse/email")
	public RESTResult refuseEmail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.refuseEmail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/accept/sms")
	public RESTResult acceptSms(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.acceptSms(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/accept/email")
	public RESTResult acceptEmail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.acceptEmail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
}
