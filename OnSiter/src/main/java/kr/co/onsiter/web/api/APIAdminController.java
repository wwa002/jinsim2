package kr.co.onsiter.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.onsiter.model.RESTResult;
import kr.co.onsiter.service.AdminService;
import kr.co.onsiter.service.ResourceService;

@RestController
@RequestMapping("/api/admin")
public class APIAdminController {

	public static Logger logger = Logger.getLogger(APIController.class);
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	ResourceService resourceService;

	@RequestMapping
	public RESTResult index(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	@RequestMapping("/dashboard")
	public RESTResult dashboard(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiDashboard(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	/* Member Start */
	@RequestMapping("/member/check/id")
	public RESTResult memberCheckId(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiCheckId(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/list")
	public RESTResult memberList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/info")
	public RESTResult memberInfo(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberInfo(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/save")
	public RESTResult memberSave(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberSave(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/delete")
	public RESTResult memberDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	/* Member End */
	
	@RequestMapping("/sms/send")
	public RESTResult smsSend(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsSend(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/send/list")
	public RESTResult smsSendList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsSendList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/delete")
	public RESTResult smsDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/cafe24/count")
	public RESTResult smsCafe24Count(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsCafe24Count(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/add")
	public RESTResult smsContactsGroupAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/modify")
	public RESTResult smsContactsGroupModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/delete")
	public RESTResult smsContactsGroupDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/list")
	public RESTResult smsContactsGroupList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/add")
	public RESTResult smsContactsContactAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/modify")
	public RESTResult smsContactsContactModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/delete")
	public RESTResult smsContactsContactDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/list")
	public RESTResult smsContactsContactList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/refuse/list")
	public RESTResult smsRefuseList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsRefuseList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/send")
	public RESTResult mailSend(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailSend(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/send/list")
	public RESTResult mailSendList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailSendList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/delete")
	public RESTResult mailDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/add")
	public RESTResult mailTemplateAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
		
	@RequestMapping("/mail/template/item")
	public RESTResult mailTemplateItem(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateItem(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/modify")
	public RESTResult mailTemplateModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/delete")
	public RESTResult mailTemplateDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/file")
	public RESTResult mailTemplateFile(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return adminService.apiMailTemplateFile(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/mail/template/file/list")
	public RESTResult mailTemplateFileList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateFileList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/mail/template/file/delete")
	public RESTResult mailTemplateFileDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateFileDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	
	@RequestMapping("/mail/template/list")
	public RESTResult mailTemplateList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/add")
	public RESTResult mailContactsGroupAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/modify")
	public RESTResult mailContactsGroupModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/delete")
	public RESTResult mailContactsGroupDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/list")
	public RESTResult mailContactsGroupList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/add")
	public RESTResult mailContactsContactAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/modify")
	public RESTResult mailContactsContactModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/delete")
	public RESTResult mailContactsContactDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/list")
	public RESTResult mailContactsContactList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/refuse/list")
	public RESTResult mailRefuseList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailRefuseList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	/* Stats Start */
	@RequestMapping("/stats/analytics")
	public RESTResult statsAnalytics(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsAnalytics(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/counter")
	public RESTResult statsCounter(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsCounter(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/browser")
	public RESTResult statsBrowser(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsBrowser(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/inkeyword")
	public RESTResult statsInKeyword(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsInKeyword(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/outkeyword")
	public RESTResult statsOutKeyword(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsOutKeyword(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/referer")
	public RESTResult statsReferer(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsReferer(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/url")
	public RESTResult statsUrl(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsUrl(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	/* Stats End */
	
	/* Resources Start */
	@RequestMapping("/resource/jstree")
	public JSONObject jstree(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiJSTree(request, param);
		} catch (Exception e) {
			return new JSONObject();
		}
	}
	
	@RequestMapping("/resource/folder/list")
	public RESTResult forderList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiFolderList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/resource/folder/add")
	public RESTResult folderAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiFolderAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/resource/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/resource/upload/dropzone", method=RequestMethod.POST)
	public RESTResult uploadDropzone(MultipartHttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
            return resourceService.apiUploadDropzone(request, param);
        } catch (Exception e) {
        	return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
        }
	}
	/* Resources End */
	
	/* etc Start */
	@RequestMapping("/etc/create")
	public RESTResult etcCreate(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEtcCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/etc/copy")
	public RESTResult etcCopy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEtcCopy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/etc/modify")
	public RESTResult etcModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEtcModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/etc/delete")
	public RESTResult etcDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEtcDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	/* etc End */
	
	/* Event Start */
	@RequestMapping("/event/check")
	public RESTResult eventCheck(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiCheckEvent(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/create")
	public RESTResult eventCreate(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/modify")
	public RESTResult eventModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/main")
	public RESTResult eventMain(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventMain(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/page/add")
	public RESTResult eventPageAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventPageAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/page/modify")
	public RESTResult eventPageModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventPageModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/page/delete")
	public RESTResult eventPageDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventPageDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/date/add")
	public RESTResult eventDateAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventDateAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/date/modify")
	public RESTResult eventDateModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventDateModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/date/delete")
	public RESTResult eventDateDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventDateDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/session/add")
	public RESTResult eventSessionAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventSessionAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/session/modify")
	public RESTResult eventSessionModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file1, @RequestParam(required=false) MultipartFile file2) throws Exception {
		try {
			return adminService.apiEventSessionModify(request, param, file1, file2);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/session/delete")
	public RESTResult eventSessionDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventSessionDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/session/page")
	public RESTResult eventSessionPage(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventSessionPage(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/list")
	public RESTResult eventVoteList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventVoteList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/add")
	public RESTResult eventVoteAdd(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return adminService.apiEventVoteAdd(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/modify")
	public RESTResult eventVoteModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return adminService.apiEventVoteModify(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/delete")
	public RESTResult eventVoteDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventVoteDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/run")
	public RESTResult eventVoteRun(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventVoteRun(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/reset")
	public RESTResult eventVoteReset(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventVoteReset(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/result")
	public RESTResult eventVoteResult(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventVoteResult(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/monitor")
	public RESTResult eventVoteMonitor(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventVoteMonitor(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/vote/current")
	public RESTResult eventVoteCurrent(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventVoteCurrent(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/question/list")
	public RESTResult eventQuestionList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventQuestionList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/question/current")
	public RESTResult eventQuestionCurrent(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventQuestionCurrent(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/question/monitor")
	public RESTResult eventQuestionMonitor(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventQuestionMonitor(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/question/principal")
	public RESTResult eventQuestionPrincipal(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventQuestionPrincipal(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/feedback/add")
	public RESTResult eventFeedbackAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventFeedbackAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/feedback/modify")
	public RESTResult eventFeedbackModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventFeedbackModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/feedback/ord")
	public RESTResult eventFeedbackOrd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventFeedbackOrd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/feedback/delete")
	public RESTResult eventFeedbackDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventFeedbackDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/event/feedback/result")
	public RESTResult eventFeedbackResult(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiEventFeedbackResult(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	
	/* Event End */
}
