package kr.co.onsiter.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.onsiter.service.CommonService;
import kr.co.onsiter.service.EventService;

@Controller
@RequestMapping("/e")
public class EventController {
	
	public static Logger logger = Logger.getLogger(EventController.class);
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	EventService eventService;
	

	@RequestMapping("/{code}")
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return eventService.index(request, param, model);
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping({"/{code}/{code1}", "/{code}/{code1}/{code2}", "/{code}/{code1}/{code2}/{code3}"})
	public String content(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable(required=false) String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3) throws Exception {
		try {
			param.put("code", code);
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			return eventService.content(request, param, model);
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
}
