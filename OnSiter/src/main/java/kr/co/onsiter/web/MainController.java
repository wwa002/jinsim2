package kr.co.onsiter.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.onsiter.service.CommonService;
import kr.co.onsiter.service.MainService;
import kr.co.onsiter.service.ResourceService;
import kr.co.onsiter.utils.StringUtil;

@Controller
public class MainController {

	private static Logger logger = Logger.getLogger(MainController.class);
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	MainService mainService;
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping(value={"", "/"})
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			commonService.accessAnalysis(request, param);
			return mainService.index(request, param, model);
		} catch (Exception e) {
			logger.error(e);
		}
		return "/index";
	}

	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			HttpSession session = request.getSession();
			if (session != null) {
				session.invalidate();
			}
			return "redirect:/";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping({"/js/{code1}/{name:.+}", "/js/{code1}/{code2}/{name:.+}", "/js/{code1}/{code2}/{code3}/{name:.+}"})
	public ResponseEntity<byte[]> etcJS(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3, @PathVariable String name) throws Exception {
		try {
			param.put("type", "JS");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			param.put("filename", name);
			return mainService.etcResource(request, response, param, model);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping({"/css/{code1}/{name:.+}", "/css/{code1}/{code2}/{name:.+}", "/css/{code1}/{code2}/{code3}/{name:.+}"})
	public ResponseEntity<byte[]> etcCSS(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3, @PathVariable String name) throws Exception {
		try {
			param.put("type", "CSS");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			param.put("filename", name);
			return mainService.etcResource(request, response, param, model);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping({"/xml/{code1}/{name:.+}", "/xml/{code1}/{code2}/{name:.+}", "/xml/{code1}/{code2}/{code3}/{name:.+}"})
	public ResponseEntity<byte[]> etcXML(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3, @PathVariable String name) throws Exception {
		try {
			param.put("type", "XML");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			param.put("filename", name);
			return mainService.etcResource(request, response, param, model);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping("/refuse/sms")
	public String refuseSms(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return "/refuse/sms";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/refuse/email")
	public String refuseEmail(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return "/refuse/email";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping(value="/mail/{tidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> mailFile(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int tidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("tidx", tidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.mailFileDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
}
