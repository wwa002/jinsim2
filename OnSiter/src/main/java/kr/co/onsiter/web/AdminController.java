package kr.co.onsiter.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.co.onsiter.service.AdminService;
import kr.co.onsiter.service.ResourceService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	public static Logger logger = Logger.getLogger(AdminController.class);
	
	@Autowired
	AdminService adminService;
	
	
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		return "redirect:/admin/dashboard";
	}
	
	@RequestMapping("/dashboard")
	public String dashboard(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/dashboard";
	}
	
	@RequestMapping("/login")
	public String login(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		return "/admin/login";
	}
	
	/* Member Start */
	@RequestMapping("/member")
	public String member(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/member/list";
	}
	/* Member End */
	
	/* SMS Start */
	@RequestMapping("/sms/list")
	public String smsList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			adminService.smsList(request, param, model);
			return "/admin/sms/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/sms/contacts")
	public String smsContacts(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/sms/contacts";
	}
	
	@RequestMapping("/sms/refuse")
	public String smsRefuse(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/sms/refuse";
	}
	/* SMS End */
	
	
	/* Mailer Start */
	@RequestMapping("/mailer/list")
	public String mailerList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/mailer/list";
	}
	
	@RequestMapping("/mailer/item/{idx}")
	public String mailerItem(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("idx", idx);
			adminService.mailerItem(request, param, model);
			return "/admin/mailer/item";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/send")
	public String mailerSend(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			adminService.mailerSend(request, param, model);
			return "/admin/mailer/send";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/send/view")
	public String mailerTemplateSampleView(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			return "/admin/mailer/send_view";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/template")
	public String mailerTemplate(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/mailer/template";
	}
	
	@RequestMapping("/mailer/template/{idx}")
	public String mailerTemplateEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("idx", idx);
			adminService.mailerTemplateView(request, param, model);
			return "/admin/mailer/template_editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/template/{idx}/view")
	public String mailerTemplateView(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("idx", idx);
			adminService.mailerTemplateView(request, param, model);
			return "/admin/mailer/template_display";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/contacts")
	public String mailerContacts(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/mailer/contacts";
	}
	
	@RequestMapping("/mailer/refuse")
	public String mailerRefuse(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/mailer/refuse";
	}
	/* Mailer End */
	
	/* Stats Start */
	@RequestMapping("/stats")
	public String stats(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/stats";
	}
	/* Stats End */
	
	/* Resources Start */
	@RequestMapping("/resources")
	public String resources(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		adminService.loadAdmin(request, param, model);
		return "/admin/resources";
	}
	@RequestMapping(value="/resources/download/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> resourcesDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.resourceDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	/* Resources End */
	
	/* etc Start */
	@RequestMapping("/etc")
	public String etc(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			adminService.etc(request, param, model);
			return "/admin/etc/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/etc/{idx}")
	public String etcEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("idx", idx);
			adminService.etcEdit(request, param, model);
			return "/admin/etc/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* etc End */
	
	/* Event Start */
	@RequestMapping("/event/list")
	public String eventList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			adminService.eventList(request, param, model);
			return "/admin/event/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/editor/{code}")
	public String eventEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			adminService.eventEdit(request, param, model);
			return "/admin/event/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/content/{code}/list")
	public String eventContentList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			adminService.eventContentList(request, param, model);
			return "/admin/event/content_list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/content/{code}/{idx}")
	public String eventContentEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventContentEdit(request, param, model);
			return "/admin/event/content_editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/feedback/{code}")
	public String eventFeedback(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			adminService.eventFeedback(request, param, model);
			return "/admin/event/feedback";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/feedback/{code}/data/{idx}")
	public String eventFeedbackData(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventFeedbackData(request, param, model);
			return "/admin/event/feedback_data";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/feedback/{code}/result/{idx}")
	public String eventFeedbackResult(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventFeedbackResult(request, param, model);
			return "/admin/event/feedback_result";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}")
	public String eventManager(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			adminService.eventManager(request, param, model);
			return "/admin/event/manager";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}/{idx}")
	public String eventSession(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventSession(request, param, model);
			return "/admin/event/session";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}/play/{idx}")
	public String eventPlay(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventPlay(request, param, model);
			return "/admin/event/play";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}/monitor/speaker/{idx}")
	public String eventMonitorSpeaker(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventSession(request, param, model);
			return "/admin/event/monitor/speaker";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}/monitor/operator/{idx}")
	public String eventMonitorOperator(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventSession(request, param, model);
			return "/admin/event/monitor/operator";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}/monitor/principal/{idx}")
	public String eventMonitorPrincipal(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventDate(request, param, model);
			return "/admin/event/monitor/principal";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}/monitor/vote/{idx}")
	public String eventMonitorVote(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventDate(request, param, model);
			return "/admin/event/monitor/vote";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/{code}/monitor/question/{idx}")
	public String eventMonitorQuestion(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			adminService.loadAdmin(request, param, model);
			param.put("code", code);
			param.put("idx", idx);
			adminService.eventDate(request, param, model);
			return "/admin/event/monitor/question";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	/* Event End */
}
