package kr.co.onsiter.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import kr.co.onsiter.interceptors.APIAdminInterceptor;
import kr.co.onsiter.interceptors.AdminInterceptor;
import kr.co.onsiter.interceptors.MainInterceptor;

@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// Front
		registry.addInterceptor(new MainInterceptor()).addPathPatterns("/**").excludePathPatterns("/admin/**", "/api/**");
		
		// Admin
		registry.addInterceptor(new AdminInterceptor()).addPathPatterns("/admin/**");
		registry.addInterceptor(new APIAdminInterceptor()).addPathPatterns("/api/admin/**");
	}
	
}
