package kr.co.onsiter.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface APIMapper {

	public Map<String, Object> login(Map<String, Object> param) throws Exception;
	public Map<String, Object> loginForHeader(Map<String, Object> param) throws Exception;
	public void loginAuthUpdate(Map<String, Object> param) throws Exception;
	public void loginDateUpdate(Map<String, Object> param) throws Exception;
	
	/* for Refuse Start */
	public int checkRefuseSMS(Map<String, Object> param) throws Exception;
	public int checkRefuseEmail(Map<String, Object> param) throws Exception;
	
	public void refuseSms(Map<String, Object> param) throws Exception;
	public void acceptSms(Map<String, Object> param) throws Exception;
	public void refuseEmail(Map<String, Object> param) throws Exception;
	public void acceptEmail(Map<String, Object> param) throws Exception;
	/* for Refuse End */
	
	/* Mailer Start */
	public Map<String, Object> getMailTemplate(Map<String, Object> param) throws Exception;
	/* Mailer End */
	
}
