package kr.co.onsiter.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper {

	public List<Map<String, Object>> dashboardCounter(Map<String, Object> param) throws Exception;
	
	/* Member Start */
	public int checkId(Map<String, Object> param) throws Exception;
	
	public int memberListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> memberList(Map<String, Object> param) throws Exception;
	public Map<String, Object> memberInfo(Map<String, Object> param) throws Exception;
	public Map<String, Object> memberItemForIdx(Map<String, Object> param) throws Exception;
	
	public int memberNewIdx() throws Exception;
	public void memberJoin(Map<String, Object> param) throws Exception;
	public void memberModify(Map<String, Object> param) throws Exception;
	public void memberDelete(Map<String, Object> param) throws Exception;
	
	/* Member End */
	
	/* for Admin SMS Start */
	public int smsGroupListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsGroupList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsGroupListAll(Map<String, Object> param) throws Exception;
	
	public int smsContactCheck(Map<String, Object> param) throws Exception;
	public int smsContactListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsContactList(Map<String, Object> param) throws Exception;
	
	public int smsRefuseListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsRefuseList(Map<String, Object> param) throws Exception;
	
	public int smsNewIdx(Map<String, Object> param) throws Exception;
	
	public int smsSendListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsSendListList(Map<String, Object> param) throws Exception;
	
	public void smsGroupAdd(Map<String, Object> param) throws Exception;
	public void smsGroupModify(Map<String, Object> param) throws Exception;
	public void smsGroupDelete(Map<String, Object> param) throws Exception;
	public void smsGroupDeleteContacts(Map<String, Object> param) throws Exception;
	
	public void smsContactAdd(Map<String, Object> param) throws Exception;
	public void smsContactModify(Map<String, Object> param) throws Exception;
	public void smsContactDelete(Map<String, Object> param) throws Exception;
	
	public void smsSend(Map<String, Object> param) throws Exception;
	public void smsSendContact(Map<String, Object> param) throws Exception;
	public void smsSendTotal(Map<String, Object> param) throws Exception;
	
	public void smsDelete(Map<String, Object> param) throws Exception;
	public void smsDeleteContact(Map<String, Object> param) throws Exception;
	/* for Admin SMS End */
	
	/* for Admin Mail Start */
	public int mailTemplateListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailTemplateList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailTemplateListAll(Map<String, Object> param) throws Exception;
	public Map<String, Object> mailTemplateItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailTemplateFileList(Map<String, Object> param) throws Exception;
	public Map<String, Object> mailTemplateFileItem(Map<String, Object> param) throws Exception;
	
	public int mailGroupListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailGroupList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailGroupListAll(Map<String, Object> param) throws Exception;
	
	public int mailContactCheck(Map<String, Object> param) throws Exception;
	public int mailContactListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailContactList(Map<String, Object> param) throws Exception;
	
	public int mailRefuseListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailRefuseList(Map<String, Object> param) throws Exception;
	
	public int mailNewIdx(Map<String, Object> param) throws Exception;
	
	public int mailSendListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailSendList(Map<String, Object> param) throws Exception;
	public Map<String, Object> mailSendItem(Map<String, Object> param) throws Exception;
	
	public void mailTemplateAdd(Map<String, Object> param) throws Exception;
	public void mailTemplateModify(Map<String, Object> param) throws Exception;
	public void mailTemplateDelete(Map<String, Object> param) throws Exception;
	
	public void mailTemplateFileAdd(Map<String, Object> param) throws Exception;
	public void mailTemplateFileModify(Map<String, Object> param) throws Exception;
	public void mailTemplateFileItemDelete(Map<String, Object> param) throws Exception;
	
	public void mailGroupAdd(Map<String, Object> param) throws Exception;
	public void mailGroupModify(Map<String, Object> param) throws Exception;
	public void mailGroupDelete(Map<String, Object> param) throws Exception;
	public void mailGroupDeleteContacts(Map<String, Object> param) throws Exception;
	
	public void mailContactAdd(Map<String, Object> param) throws Exception;
	public void mailContactModify(Map<String, Object> param) throws Exception;
	public void mailContactDelete(Map<String, Object> param) throws Exception;
	
	public void mailSend(Map<String, Object> param) throws Exception;
	public void mailSendContact(Map<String, Object> param) throws Exception;
	public void mailSendTotal(Map<String, Object> param) throws Exception;
	
	public void mailDelete(Map<String, Object> param) throws Exception;
	public void mailDeleteContact(Map<String, Object> param) throws Exception;
	/* for Admin Mail End */
	
	/* for Etc Start */
	public int etcListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> etcList(Map<String, Object> param) throws Exception;
	public Map<String, Object> etcItem(Map<String, Object> param) throws Exception;
	
	public void etcCreate(Map<String, Object> param) throws Exception;
	public void etcCopy(Map<String, Object> param) throws Exception;
	public void etcModify(Map<String, Object> param) throws Exception;
	public void etcDelete(Map<String, Object> param) throws Exception;
	/* for Etc End */
	
	/* Event Start */
	public List<Map<String, Object>> getEventList(Map<String, Object> param) throws Exception;
	
	public int eventListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> eventList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventItem(Map<String, Object> param) throws Exception;
	
	public int checkEvent(Map<String, Object> param) throws Exception;
	public void eventCreate(Map<String, Object> param) throws Exception;
	public void eventModify(Map<String, Object> param) throws Exception;
	public void eventMain(Map<String, Object> param) throws Exception;
	
	public int eventContentListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> eventContentList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventContentItem(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> eventFeedbackList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventFeedbackItem(Map<String, Object> param) throws Exception;
	
	public void eventFeedbackAdd(Map<String, Object> param) throws Exception;
	public void eventFeedbackModify(Map<String, Object> param) throws Exception;
	public void eventFeedbackOrd(Map<String, Object> param) throws Exception;
	public void eventFeedbackDelete(Map<String, Object> param) throws Exception;
	
	public int eventFeedbackUserListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> eventFeedbackUserList(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> eventFeedBackDataList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventFeedBackResult(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> eventFeedBackResultList(Map<String, Object> param) throws Exception;
	
	public void eventPageAdd(Map<String, Object> param) throws Exception;
	public void eventPageModify(Map<String, Object> param) throws Exception;
	public void eventPageDelete(Map<String, Object> param) throws Exception;
	
	public void eventDateAdd(Map<String, Object> param) throws Exception;
	public void eventDateModify(Map<String, Object> param) throws Exception;
	public void eventDateDelete(Map<String, Object> param) throws Exception;
	public void eventDateSessionDeleteAll(Map<String, Object> param) throws Exception;
	public void eventDateVoteDeleteAll(Map<String, Object> param) throws Exception;
	public void eventDateVoteResultDeleteAll(Map<String, Object> param) throws Exception;
	public void eventDateQuestionDeleteAll(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> eventDateList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventDateItem(Map<String, Object> param) throws Exception;
	
	public int eventSessionNewIdx() throws Exception;
	public void eventSessionAdd(Map<String, Object> param) throws Exception;
	public void eventSessionModify(Map<String, Object> param) throws Exception;
	public void eventSessionDelete(Map<String, Object> param) throws Exception;
	public void eventSessionVoteDeleteAll(Map<String, Object> param) throws Exception;
	public void eventSessionVoteResultDeleteAll(Map<String, Object> param) throws Exception;
	public void eventSessionQuestionDeleteAll(Map<String, Object> param) throws Exception;
	public void eventSessionPage(Map<String, Object> param) throws Exception;
	
	
	public List<Map<String, Object>> eventSessionList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventSessionItem(Map<String, Object> param) throws Exception;
	
	public void eventVoteAdd(Map<String, Object> param) throws Exception;
	public void eventVoteModify(Map<String, Object> param) throws Exception;
	public void eventVoteDelete(Map<String, Object> param) throws Exception;
	public void eventVoteResultDeleteAll(Map<String, Object> param) throws Exception;
	public void eventVoteRun(Map<String, Object> param) throws Exception;
	public void eventVoteReset(Map<String, Object> param) throws Exception;
	public void eventVoteResetData(Map<String, Object> param) throws Exception;
	public void eventVoteMonitor(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> eventVoteList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventVoteItem(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventVoteCurrent(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventVoteResult(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> eventVoteResultList(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> eventQuestionSpeakers(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> eventQuestionList(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventQuestionCurrent(Map<String, Object> param) throws Exception;
	
	public void eventQuestionMonitor(Map<String, Object> param) throws Exception;
	public void eventQuestionPrincipal(Map<String, Object> param) throws Exception;
	
	/* Event End */
}
