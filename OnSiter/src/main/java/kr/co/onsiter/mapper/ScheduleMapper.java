package kr.co.onsiter.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScheduleMapper {

	/* Server Management Start */
	public int isMainServer(String host) throws Exception;
	public int checkServer(Map<String, Object> param) throws Exception;
	
	public void insertServer(Map<String, Object> param) throws Exception;
	public void updateServer(Map<String, Object> param) throws Exception;
	
	public void electMainServer(Map<String, Object> param) throws Exception;
	public void removeOldServer() throws Exception;
	
	public int checkMainServer() throws Exception;
	public String getNewMainServer() throws Exception;
	/* Server Management End */
	
	
	/* SMS Start */
	public List<Map<String, Object>> smsList() throws Exception;
	public int checkRefuseSMS(Map<String, Object> param) throws Exception;
	
	public void smsSendedResult(Map<String, Object> param) throws Exception;
	public void smsSendedCount(Map<String, Object> param) throws Exception;
	/* SMS END */
	
	/* Mail Start */
	public List<Map<String, Object>> mailList() throws Exception;
	public int checkRefuseMail(Map<String, Object> param) throws Exception;
	
	public void mailSendedResult(Map<String, Object> param) throws Exception;
	public void mailSendedCount(Map<String, Object> param) throws Exception;
	/* Mail End */
	
}
