package kr.co.onsiter.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ResourceMapper {

	public List<Map<String, Object>> listForJSTree(Map<String, Object> param) throws Exception;
	
	public int checkFolder(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> listForFolder(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> listForResource(Map<String, Object> param) throws Exception;
	public Map<String, Object> itemForFile(Map<String, Object> param) throws Exception;
	
	public void createFolder(Map<String, Object> param) throws Exception;
	public void uploadFile(Map<String, Object> param) throws Exception;
	public void deleteFile(Map<String, Object> param) throws Exception;
	
	/* Content Start */
	public String eventMain() throws Exception;
	public Map<String, Object> event(Map<String, Object> param) throws Exception;
	public Map<String, Object> eventContent(Map<String, Object> param) throws Exception;
	public Map<String, Object> resourceEtc(Map<String, Object> param) throws Exception;
	/* Content End */
	
	/* for Mail Start */
	public void mailRead(Map<String, Object> param) throws Exception;
	public void mailReadCount(Map<String, Object> param) throws Exception;
	/* for Mail End */
	
}
