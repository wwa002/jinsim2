package kr.co.onsiter.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EventMapper {

	public List<Map<String, Object>> sessions(Map<String, Object> param) throws Exception;
	
	public void vote(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> voteItems(Map<String, Object> param) throws Exception;
	
	public void question(Map<String, Object> param) throws Exception;

	public List<Map<String, Object>> feedbackList(Map<String, Object> param) throws Exception;
	public int feedbackNewIdx() throws Exception;
	public void feedback(Map<String, Object> param) throws Exception;
	public void feedbackData(Map<String, Object> param) throws Exception;
}
