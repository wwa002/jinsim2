package kr.co.onsiter.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * @author addios4u
 * Web Log를 위한 Mapper
 */
@Mapper
public interface CommonMapper {
	
	// for Access Analysis
	public Map<String, Object> getUrl(Map<String, Object> param) throws Exception;
	public int countUrl(Map<String, Object> param) throws Exception;
	public Map<String, Object> getMinUrl(Map<String, Object> param) throws Exception;
	public Map<String, Object> getInKey(Map<String, Object> param) throws Exception;
	public int countReferer(Map<String, Object> param) throws Exception;
	public Map<String, Object> getMinReferer(Map<String, Object> param) throws Exception;
	public Map<String, Object> getOutKey(Map<String, Object> param) throws Exception;
	public Map<String, Object> getBrowser(Map<String, Object> param) throws Exception;
	public Map<String, Object> getToday(Map<String, Object> param) throws Exception;
	public Map<String, Object> getInfo(Map<String, Object> param) throws Exception;
	
	public void insertUrl(Map<String, Object> param) throws Exception;
	public void updateUrl(Map<String, Object> param) throws Exception;
	public void deleteUrls(Map<String, Object> param) throws Exception;
	
	public void updateAccessPageCounter(Map<String, Object> param) throws Exception;
	public void insertInKey(Map<String, Object> param) throws Exception;
	public void updateInKey(Map<String, Object> param) throws Exception;
	
	public void insertReferer(Map<String, Object> param) throws Exception;
	public void deleteReferers(Map<String, Object> param) throws Exception;
	
	public void insertOutKey(Map<String, Object> param) throws Exception;
	public void updateOutKey(Map<String, Object> param) throws Exception;
	
	public void insertBrowser(Map<String, Object> param) throws Exception;
	public void updateBrowser(Map<String, Object> param) throws Exception;
	
	public void insertToday(Map<String, Object> param) throws Exception;
	public void updateToday(Map<String, Object> param) throws Exception;
	
	public void insertInfo(Map<String, Object> param) throws Exception;
	public void updateInfo(Map<String, Object> param) throws Exception;
	
	public void join(Map<String, Object> param) throws Exception;
	public void expire(Map<String, Object> param) throws Exception;
	public void login(Map<String, Object> param) throws Exception;
	public void loginReferer(Map<String, Object> param) throws Exception;
	
	public void boardWrite(Map<String, Object> param) throws Exception;
	public void boardCommentWrite(Map<String, Object> param) throws Exception;
	public void boardFileUpload(Map<String, Object> param) throws Exception;
	public void boardFileDownload(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> apiStatsAnalytics(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsCounter(Map<String, Object> param) throws Exception;
	public int apiStatsBrowserTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsInKeyword(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsOutKeyword(Map<String, Object> param) throws Exception;
	public int apiStatRefererTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatReferer(Map<String, Object> param) throws Exception;
	public int apiStatsUrlTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsUrl(Map<String, Object> param) throws Exception;

}
