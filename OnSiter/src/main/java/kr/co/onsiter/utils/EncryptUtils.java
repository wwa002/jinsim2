package kr.co.onsiter.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import com.amazonaws.util.IOUtils;

public class EncryptUtils {

	public static final String toMD5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	public static String base64Encode(String str) throws java.io.IOException {
		sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
		byte[] strByte = str.getBytes();
		String result = encoder.encode(strByte);
		return result;
	}

	public static String base64Decode(String str) throws java.io.IOException {
		sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		byte[] strByte = decoder.decodeBuffer(str);
		String result = new String(strByte);
		return result;
	}
	
	public static String base64Encode(File file) throws Exception {
		InputStream io = new FileInputStream(file);
		byte[] bytes = IOUtils.toByteArray(io);
		String base64 = Base64.getEncoder().encodeToString(bytes);
		io.close();
		return base64;
	}
	
	public static File base64Decode(String base64, File file) throws Exception {
		OutputStream io = new FileOutputStream(file);
		io.write(Base64.getDecoder().decode(base64));
		io.close();
		return file;
	}

}
