<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${event.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>행사관리</li>
			<li>${event.title}</li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-md-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>세션 정보 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="modifyForm" id="modifyForm" method="post" class="form-horizontal" onsubmit="return false;" enctype="multipart/form-data">
						<input type="hidden" name="code" value="${code}" />
						<input type="hidden" name="idx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">종류</label>
							<div class="col-lg-9">
								<label class="radio-inline">
									<input type="radio" name="type" class="type" value="N" <c:if test="${item.type eq 'N'}"> checked="checked"</c:if> /> 일반
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" class="type" value="S" <c:if test="${item.type eq 'S'}"> checked="checked"</c:if> /> 강연
								</label>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">타이틀</label>
							<div class="col-lg-9">
								<input type="text" name="title" class="form-control" value="${fn:escapeXml(item.title)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">발표자</label>
							<div class="col-lg-9">
								<input type="text" name="speaker" class="form-control" value="${fn:escapeXml(item.speaker)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">강의록</label>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-lg-12">
										<input type="file" name="file1" class="form-control" accept="image/*,application/pdf" />
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										&lt;jpg, png, gif, pdf 만 등록가능&gt;
									</div>
								</div>
								<c:if test="${item.filepath1 ne null and item.filepath1 ne ''}">
									<div class="row">
										<div class="col-lg-12">
											<a href="/res/session/${item.code}/${item.idx}/1/${item.filename1}" target="_blank">${item.filename1}</a>
										</div>
									</div>
								</c:if>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">CV자료</label>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-lg-12">
										<input type="file" name="file2" class="form-control" accept="image/*,application/pdf" />
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										&lt;jpg, png, gif, pdf 만 등록가능&gt;
									</div>
								</div>
								<c:if test="${item.filepath2 ne null and item.filepath2 ne ''}">
									<div class="row">
										<div class="col-lg-12">
											<a href="/res/session/${item.code}/${item.idx}/2/${item.filename2}" target="_blank">${item.filename2}</a>
										</div>
									</div>
								</c:if>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">시간</label>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
											<input type="text" name="stime" value="${item.stime}" class="form-control clockpicker" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
											<input type="text" name="etime" value="${item.etime}" class="form-control clockpicker" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-save" type="submit">수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-md-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>투표 목록</h5>
				</div>
				<div class="ibox-content">
					<table class="footable table table-stripped toggle-arrow-tiny">
						<thead>
						<tr>
							<th class="text-center col-md-*">질문</th>
							<th data-hide="all">답1</th>
							<th data-hide="all">답2</th>
							<th data-hide="all">답3</th>
							<th data-hide="all">답4</th>
							<th data-hide="all">답5</th>
							<th data-hide="all">첨부파일</th>
							<th class="text-center col-md-3">관리</th>
						</tr>
						</thead>
						<tbody id="list"></tbody>
						<tfoot>
							<tr>
							    <td colspan="5">
							        <ul class="pagination pull-right"></ul>
							    </td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox" id="vote-form-add">
				<div class="ibox-title">
					<h5>투표 등록</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="voteForm" id="voteForm" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;" enctype="multipart/form-data">
						<input type="hidden" name="code" id="code" value="${code}" />
						<input type="hidden" name="edidx" id="edidx" value="${item.edidx}" />
						<input type="hidden" name="sidx" id="sidx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">질문</label>
							<div class="col-lg-9">
								<input type="text" name="question" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 갯수</label>
							<div class="col-lg-9">
								<select name="answer_size" id="answer_size" class="form-control">
									<c:forEach begin="0" end="6" step="1" var="i">
										<option value="${i}">${i}개</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<c:forEach begin="1" end="10" step="1" var="i">
							<div id="answer-${i}" class="answers hidden">
								<hr />
								<div class="form-group">
									<label class="col-lg-3 control-label">답변 ${i}</label>
									<div class="col-lg-9">
										<input type="text" name="answer${i}" class="form-control" />
									</div>
								</div>
							</div>
						</c:forEach>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<input type="file" name="file" class="form-control" accept="image/*" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-add" type="submit">등록</button>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox hidden" id="vote-form-modify">
				<div class="ibox-title">
					<h5>투표 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="voteModifyForm" id="voteModifyForm" method="post" class="form-horizontal" onsubmit="return false;" enctype="multipart/form-data">
						<input type="hidden" name="code" value="${code}" />
						<input type="hidden" name="edidx" id="edidx" value="${item.edidx}" />
						<input type="hidden" name="sidx" value="${idx}" />
						<input type="hidden" name="idx" id="idx" />
						<div class="form-group">
							<label class="col-lg-3 control-label">질문</label>
							<div class="col-lg-9">
								<input type="text" name="question" id="question" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 갯수</label>
							<div class="col-lg-9">
								<select name="answer_size" id="answer_modify_size" class="form-control">
									<c:forEach begin="0" end="6" step="1" var="i">
										<option value="${i}">${i}개</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<c:forEach begin="1" end="10" step="1" var="i">
							<div id="answer-modify-${i}" class="answers-modify hidden">
								<hr />
								<div class="form-group">
									<label class="col-lg-3 control-label">답변 ${i}</label>
									<div class="col-lg-9">
										<input type="text" name="answer${i}" id="answer${i}" class="form-control" />
									</div>
								</div>
							</div>
						</c:forEach>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<input type="file" name="file" class="form-control" accept="image/*" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-warning" id="btn-modify" type="submit">수정</button>
							<button class="btn btn-default" id="btn-modify-cancel" type="reset">취소</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td>{{question}}</td>
	<td>{{answer1}}</td>
	<td>{{answer2}}</td>
	<td>{{answer3}}</td>
	<td>{{answer4}}</td>
	<td>{{answer5}}</td>
	<td>{{{fileurl}}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="{{idx}}" data-question="{{question}}" data-answer_size="{{answer_size}}" data-answer1="{{answer1}}" data-answer2="{{answer2}}" data-answer3="{{answer3}}" data-answer4="{{answer4}}" data-answer5="{{answer5}}" data-answer6="{{answer6}}" data-answer7="{{answer7}}" data-answer8="{{answer8}}" data-answer9="{{answer9}}" data-answer10="{{answer10}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<!-- FooTable -->
<script src="/resources/bootstrap/inspinia/js/plugins/footable/footable.all.min.js"></script>

<script type="text/javascript">
var code = '${code}';
var edidx = '${item.edidx}';
var sidx = '${idx}';

function loadList() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/event/vote/list"
		, cache:false
		, data : {code:code, edidx:edidx, sidx:sidx}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $list = $('#list');
				$list.empty();
				
				var template = $('#list-item').html();
				Mustache.parse(template);
				
				var list = data.data;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					if (l.filepath != undefined) {
						var path = '/res/vote/'+code+'/'+l.idx+'/'+l.filename;
						l.fileurl = '<a href="'+path+'" target="_blank">'+l.filename+'</a>';
					}
					$list.append(Mustache.render(template, list[i]));
				}
				$('.footable').trigger('footable_redraw'); 
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	$('.footable').footable();
	loadList();
	
	$("#btn-save").click(function(){
		$("#modifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/event/session/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#modifyForm").submit();
	});
	
	$("#btn-add").click(function(){
		$("#voteForm").ajaxForm({
			type:"POST"
			, url : "/api/admin/event/vote/add"
			, enctype:"multipart/form-data"
			, success:function(data){
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#voteForm").submit();
	});
	
	$(document).on('click', '.btn-modify', function(){
		$('#idx').val($(this).data('idx'));
		$('#question').val($(this).data('question'));
		$('#answer_modify_size').val($(this).data('answer_size'));
		for (var i = 1; i <= 10 ; i++) {
			$('#answer'+i).val($(this).data('answer'+i));
		}
		$('.answers-modify').addClass('hidden');
		for (var i = 1; i <= parseInt($(this).data('answer_size')); i++) {
			$('#answer-modify-'+i).removeClass('hidden');
		}
		
		$('#vote-form-add').addClass('hidden');
		$('#vote-form-modify').removeClass('hidden');
	});
	
	$('#btn-modify-cancel').click(function(){
		$('#vote-form-add').removeClass('hidden');
		$('#vote-form-modify').addClass('hidden');
	});
	
	$("#btn-modify").click(function(){
		$("#voteModifyForm").ajaxForm({
			type:"POST"
			, url : "/api/admin/event/vote/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				if (data.error == 0) {
					$('#voteModifyForm')[0].reset();
					loadList();
					$('#vote-form-add').removeClass('hidden');
					$('#vote-form-modify').addClass('hidden');
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#voteModifyForm").submit();
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/event/vote/delete"
				, cache:false
				, data : {code:code, edidx:edidx, sidx:sidx, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});

	$('#answer_size').change(function(){
		var size = $(this).val();
		$('.answers').addClass('hidden');
		for (var i = 1; i <= parseInt(size); i++) {
			$('#answer-'+i).removeClass('hidden');
		}
	});
	
	$('#answer_modify_size').change(function(){
		var size = $(this).val();
		$('.answers-modify').addClass('hidden');
		for (var i = 1; i <= parseInt(size); i++) {
			$('#answer-modify-'+i).removeClass('hidden');
		}
	});

});
</script>