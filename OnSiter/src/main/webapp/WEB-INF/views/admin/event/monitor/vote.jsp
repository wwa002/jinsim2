<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>온사이터 관리자 페이지</title>
	<style type="text/css">
		body {position:relative; margin:0px; padding:0px; background:#edeef2; color:#101a39;}
		.center {text-align:center;}
		.tp70 {padding-top:70px;}
		#wrapper {position:fixed; left:0px; top:0px; right:0px; bottom:0px;}
		#question-wrapper {position:fixed; top:0px; left:0px; bottom:0px; width:70%;}
		#content-wrapper {padding:30px;}
		#title {padding:0px; margin:0px; font-size:46px;}
		#result-wrapper {position:fixed; padding-top:100px; top:0px; right:0px; bottom:0px; width:30%; background:#101a39;}
		#content-wrapper ul {list-style: none; margin:20px 0px; padding:0px;}
		#content-wrapper ul li {font-size:30px; margin-bottom:15px;}
		#content-wrapper ul li span.num {margin-right:15px; font-size:44px; padding:10px; width:50px; text-align:center; display:inline-block; font-weight:bold;}
		#img-wrapper {}
		#image {max-width:100%;}
		
		div.step {position:relative; margin:0 auto 18px; width:20px; height:2px; background: #fff01c;}
		
		#result-wrapper .title {font-size:30px; color:#ffffff; text-align:center;}
		
		#result-wrapper .counter-wrapper {margin:70px auto 0px; width:238px; height:238px; border-radius:119px; background:#fff01c; color:#101a39; display:table;}
		#result-wrapper .counter-wrapper span {font-size:170px; padding-top:20px; display:table-cell; text-align: center; vertical-align: middle; font-weight:bold;}
		
		#vote-before {position:relative; height:100%;}
		#vote-before-content {width:100%; height:100%; display:table;}
		#vote-before-content div {position:relative; display:table-cell; text-align:center; vertical-align: middle;}
		
		#vote-run {position:relative; height:100%;}
		#vote-run .step {width:40px;}
		
		#vote-end {position:relative; height:100%;}
		#vote-end .step {width:60px;}
		#vote-end .counter-wrapper span {font-size:100px;}
		#vote-end .show-result {margin-top:40px; color:#fff01c; font-size:40px; text-align:center;}
		
		#vote-result {position:relative; height:100%;}
		#vote-result .step {width:80px;}
		#vote-result-content {position:relative; width:100%; height:100%;}
		#vote-result-content .answer-wrapper {padding:10px 0px 0px;}
		#vote-result-content .answer-wrapper .title {margin:15px 0px 0px; text-align:left; padding:0px 15px; color:#ffffff; font-size:2em;}
		#vote-result-content .answer-wrapper .title .percent {float:right; clear:right; color:#fff01c;}
		#vote-result-content .bar-wrapper {position:relative; margin:0 15px; height:50px; background:#cccccc;}
		#vote-result-content .bar-wrapper .bar {position:relative; width:10px; height:50px; background:#65b744;}
		#vote-result-content .bar-wrapper .bar.on {background:#ac3929;}
	</style>
</head>

<body>
	<div id="wrapper" style="display:none;">
		<div id="question-wrapper">
			<div id="content-wrapper">
				<h1 id="title"></h1>
				<div id="img-wrapper" style="display:none;"><img id="image" src=""></div>
				<ul>
					<li class="answers" data-num="1"><span class="num">1</span> <span id="answer1"></span></li>
					<li class="answers" data-num="2"><span class="num">2</span> <span id="answer2"></span></li>
					<li class="answers" data-num="3"><span class="num">3</span> <span id="answer3"></span></li>
					<li class="answers" data-num="4"><span class="num">4</span> <span id="answer4"></span></li>
					<li class="answers" data-num="5"><span class="num">5</span> <span id="answer5"></span></li>
					<li class="answers" data-num="6"><span class="num">6</span> <span id="answer6"></span></li>
					<li class="answers" data-num="7"><span class="num">7</span> <span id="answer7"></span></li>
					<li class="answers" data-num="8"><span class="num">8</span> <span id="answer8"></span></li>
					<li class="answers" data-num="9"><span class="num">9</span> <span id="answer9"></span></li>
					<li class="answers" data-num="10"><span class="num">10</span> <span id="answer10"></span></li>
				</ul>
			</div>
		</div>
		<div id="result-wrapper">
			<div id="vote-before" style="display:none;" class="vote-wrapper">
				<div class="step"></div>
				<h2 class="title">투표가 곧 시작됩니다.</h2>
				<div class="center tp70"><img src="/resources/images/v1.png" /></div>
			</div>
			
			<div id="vote-run" style="display:none;" class="vote-wrapper">
				<div class="step"></div>
				<h2 class="title">투표중</h2>
				<div class="counter-wrapper"><span id="counter">15</span></div>
			</div>
			
			<div id="vote-end" style="display:none;" class="vote-wrapper">
				<div class="step"></div>
				<h2 class="title">투표 종료</h2>
				<div class="counter-wrapper"><span>종료</span></div>
				<div class="show-result">결과보기 <img src="/resources/images/arrow.png" /></div>
			</div>
			
			<div id="vote-result" style="display:none;" class="vote-wrapper">
				<div class="step"></div>
				<h2 class="title">투표 결과</h2>
				<div id="vote-result-content">
					<c:forEach begin="1" end="10" step="1" var="i">
						<div class="answer-wrapper" data-num="${i}">
							<h3 class="title">${i} <span id="result-${i}-percent" class="percent">0%</span></h3>
							<div id="bar-base" class="bar-wrapper">
								<div id="result-${i}-bar" class="bar"></div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<script src="/resources/jquery/jquery.min.js"></script>
<script src="/resources/plugins/moment/moment.min.js"></script>
<script type="text/javascript">
var voteCounter = 0;
var voteTimer = null;

function voteRunner() {
	if (voteCounter <= 0) {
		clearInterval(voteTimer);
		voteTimer = null;
		$('.vote-wrapper').hide();
		$('#vote-end').show();
		return;
	}
	
	voteCounter--;
	$('#counter').text(voteCounter);
}

function loadVote() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/event/vote/current"
		, cache:false
		, data : {code:'${code}', edidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				if (voteTimer != null) {
					return;
				}
				if (data.data == undefined) {
					$('#wrapper').hide();
				} else {
					var item = data.data.item;
					var result = data.data.result;
					$('#title').text(item.question);
					if (item.filepath != undefined) {
						$('#image').attr('src', '/res/vote/'+item.code+'/'+item.idx+'/'+item.filename);
						$('#img-wrapper').show();
						$('#content-wrapper #title, #content-wrapper ul').hide();
					} else {
						$('#img-wrapper').hide();
						$('#content-wrapper #title, #content-wrapper ul').show();
					}
					
					$('#answer1').text(item.answer1);
					$('#answer2').text(item.answer2);
					$('#answer3').text(item.answer3);
					$('#answer4').text(item.answer4);
					$('#answer5').text(item.answer5);
					$('#answer6').text(item.answer6);
					$('#answer7').text(item.answer7);
					$('#answer8').text(item.answer8);
					$('#answer9').text(item.answer9);
					$('#answer10').text(item.answer10);
					
					$('.answers').each(function(){
						var num = parseInt($(this).data('num'));
						console.log(num + ' ' + item.answer_size);
						if (num <= item.answer_size) {
							$(this).show(0);
						} else {
							$(this).hide(0);
						}
					});
					
					$('#wrapper').show();
					
					$('.vote-wrapper').hide();
					if (item.monitor == 'R') {
						if (result.total == 0) {
							$('.bar').css('width', '10px');
							$('.bar').removeClass('on');
							$('.percent').text('0%');
						} else {
							var a1 = parseFloat(result.answer1_percent);
							var a2 = parseFloat(result.answer2_percent);
							var a3 = parseFloat(result.answer3_percent);
							var a4 = parseFloat(result.answer4_percent);
							var a5 = parseFloat(result.answer5_percent);
							var a6 = parseFloat(result.answer6_percent);
							var a7 = parseFloat(result.answer7_percent);
							var a8 = parseFloat(result.answer8_percent);
							var a9 = parseFloat(result.answer9_percent);
							var a10 = parseFloat(result.answer10_percent);
							
							$('#result-1-percent').text(a1+'%');
							$('#result-2-percent').text(a2+'%');
							$('#result-3-percent').text(a3+'%');
							$('#result-4-percent').text(a4+'%');
							$('#result-5-percent').text(a5+'%');
							$('#result-6-percent').text(a6+'%');
							$('#result-7-percent').text(a7+'%');
							$('#result-8-percent').text(a8+'%');
							$('#result-9-percent').text(a9+'%');
							$('#result-10-percent').text(a10+'%');
							
							$('.bar').removeClass('on');
							$('#result-1-bar').css('width', result.answer1 == 0 ? '10px' : a1+'%');
							$('#result-2-bar').css('width', result.answer2 == 0 ? '10px' : a2+'%');
							$('#result-3-bar').css('width', result.answer3 == 0 ? '10px' : a3+'%');
							$('#result-4-bar').css('width', result.answer4 == 0 ? '10px' : a4+'%');
							$('#result-5-bar').css('width', result.answer5 == 0 ? '10px' : a5+'%');
							$('#result-6-bar').css('width', result.answer6 == 0 ? '10px' : a6+'%');
							$('#result-7-bar').css('width', result.answer7 == 0 ? '10px' : a7+'%');
							$('#result-8-bar').css('width', result.answer8 == 0 ? '10px' : a8+'%');
							$('#result-9-bar').css('width', result.answer9 == 0 ? '10px' : a9+'%');
							$('#result-10-bar').css('width', result.answer10 == 0 ? '10px' : a10+'%');
							
							var temp = [result.answer1, result.answer2, result.answer3, result.answer4, result.answer5, result.answer6, result.answer7, result.answer8, result.answer9, result.answer10];
							var cVal = 0, cPos = 0;
							for (var i = 0; i < temp.length; i++) {
								if (cVal < temp[i]) {
									cVal = temp[i];
									cPos = i;
								}
							}
							$('#result-'+(cPos+1)+'-bar').addClass('on');
							
							$('.answer-wrapper').each(function(){
								var num = parseInt($(this).data('num'));
								console.log(num + ' ' + item.answer_size);
								if (num <= item.answer_size) {
									$(this).show(0);
								} else {
									$(this).hide(0);
								}
							});
						}
						$('#vote-result').show();
					} else {
						if (item.startdate == undefined) {
							$('#vote-before').show();
						} else {
							var now = new Date();
							var startdate = moment(item.startdate, 'YYYYMMDDHHmmss').toDate();
							var diffTime = (now.getTime() - startdate.getTime())/1000; 
							if (diffTime <= 15) {
								voteCounter = 15;
								$('#counter').text(voteCounter);
								voteTimer = setInterval(voteRunner, 1000);
								$('#vote-run').show();
							} else {
								$('#vote-end').show();
							}
						}
					}
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}
$(function(){
	loadVote();
	setInterval(loadVote, 5000);
});
</script>