<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>행사관리</h2>
		<ol class="breadcrumb">
			<li><a href="/admin/dashboard">HOME</a></li>
			<li>행사관리</li>
			<li>${event.title }</li>
			<li class="active"><strong>피드백</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>피드백 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-2" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">문항 순서</th>
									<th class="text-center">질문</th>
									<th class="text-center">관리</th>
									<th class="text-center">결과</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											<select data-idx="${l.idx}" class="ord form-control">
												<c:forEach begin="1" end="${fn:length(list)}" step="1" var="i">
													<option value="${i}" <c:if test="${i eq l.ord}"> selected="selected"</c:if>>${i}</option>
												</c:forEach>
											</select>
										</td>
										<td class="text-center">${l.question}</a></td>
										<td class="text-center">
											<a href="/admin/event/feedback/${l.code}?idx=${l.idx}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> 수정</a>
											<button type="button" data-idx="${l.idx}" class="btn btn-danger btn-xs btn-delete"><i class="fa fa-trash"></i> 삭제</button>
										</td>
										<td class="text-center">
											<button type="button" data-idx="${l.idx}" class="btn btn-primary btn-xs btn-result"><i class="fa fa-bar-chart-o"></i> 결과</button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div class="ibox">
				<div class="ibox-title">
					<h5>피드백 참여 명단</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-md-12">
							<form id="searchForm" class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-4">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-3" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">이름</th>
									<th class="text-center">소속</th>
									<th class="text-center">내용보기</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${feedbacks}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td class="text-center">${l.name}</td>
										<td class="text-center">${l.office}</td>
										<td class="text-center">
											<button type="button" data-idx="${l.idx}" class="btn btn-xs btn-primary btn-feedback"><i class="fa fa-laptop"></i> 보기</button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div id="qrcode-wrapper" class="row hidden">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>QRCode</h5>
						</div>
						<div class="ibox-content">
							<h3>접속정보</h3>
							<div>기본 URL : <span id="longUrl"></span></div>
							<div>단축 URL : <span id="shortUrl"></span></div>
							<div>QR 코드</div>
							<div id="qrcode"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>행사 생성</h5>
						</div>
						<div class="ibox-content">
							<form id="feedbackForm" class="form-horizontal" onsubmit="return false;">
								<input type="hidden" name="code" value="${code}" />
								<c:if test="${param.idx ne null and param.idx ne '' }">
									<input type="hidden" name="idx" value="${param.idx}" />
								</c:if>
								<div class="form-group">
									<label class="col-sm-4 control-label">종류</label>
									<div class="col-sm-8">
										<c:choose>
											<c:when test="${param.idx ne null and param.idx ne '' }">
												<label><input type="radio" name="type" id="type-o" class="type" value="O" <c:if test="${item.type eq 'O' }"> checked="checked"</c:if> /> 객관식 </label>
												<label><input type="radio" name="type" id="type-m" class="type" value="M" <c:if test="${item.type eq 'M' }"> checked="checked"</c:if> /> 객관식 (다중) </label>
												<label><input type="radio" name="type" id="type-s" class="type" value="S" <c:if test="${item.type eq 'S' }"> checked="checked"</c:if> /> 주관식 </label>
											</c:when>
											<c:otherwise>
												<label><input type="radio" name="type" id="type-o" class="type" value="O" checked="checked" /> 객관식 </label>
												<label><input type="radio" name="type" id="type-m" class="type" value="M" /> 객관식 (다중) </label>
												<label><input type="radio" name="type" id="type-s" class="type" value="S" /> 주관식 </label>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">질문</label>
									<div class="col-sm-8">
										<input type="text" name="question" id="question" value="${item.question}" class="form-control" />
									</div>
								</div>
								<div class="form-group answer-wrapper">
									<label class="col-sm-4 control-label">답변 1</label>
									<div class="col-sm-8">
										<input type="text" name="answer1" value="${item.answer1}" class="form-control answer" />
									</div>
								</div>
								<div class="form-group answer-wrapper">
									<label class="col-sm-4 control-label">답변 2</label>
									<div class="col-sm-8">
										<input type="text" name="answer2" value="${item.answer2}" class="form-control answer" />
									</div>
								</div>
								<div class="form-group answer-wrapper">
									<label class="col-sm-4 control-label">답변 3</label>
									<div class="col-sm-8">
										<input type="text" name="answer3" value="${item.answer3}" class="form-control answer" />
									</div>
								</div>
								<div class="form-group answer-wrapper">
									<label class="col-sm-4 control-label">답변 4</label>
									<div class="col-sm-8">
										<input type="text" name="answer4" value="${item.answer4}" class="form-control answer" />
									</div>
								</div>
								<div class="form-group answer-wrapper">
									<label class="col-sm-4 control-label">답변 5</label>
									<div class="col-sm-8">
										<input type="text" name="answer5" value="${item.answer5}" class="form-control answer" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<c:choose>
											<c:when test="${param.idx ne null and param.idx ne '' }">
												<button type="button" id="btn-save" class="btn btn-warning">수정</button>
												<button type="reset" class="btn btn-default btn-back">취소</button>
											</c:when>
											<c:otherwise>
												<button type="button" id="btn-save" class="btn btn-primary">생성</button>
												<button type="reset" class="btn btn-default">취소</button>
											</c:otherwise>
										</c:choose>
										
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script type="text/javascript">
var idx = '${param.idx}';
var code = '${code}';
$(function(){
	$('#btn-save').click(function(){
		var url = idx == undefined || idx == '' ? '/api/admin/event/feedback/add' : '/api/admin/event/feedback/modify';
		$.ajax({
			type:"POST"
			, url : url
			, cache:false
			, data : $('#feedbackForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					if (idx == undefined || idx == '') {
						location.reload();
					} else {
						document.location.href='/admin/event/feedback/'+code;
					}
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	typeValied();
	$('.type').click(function(){
		typeValied();
	});
	
	$('.btn-back').click(function(){
		history.back();
	});
	
	$('.btn-delete').click(function(){
		if (confirm('삭제 하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({
				type:"POST"
				, url : '/api/admin/event/feedback/delete'
				, cache:false
				, data : {code:code, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$('.ord').change(function(){
		var idx = $(this).data('idx');
		var ord = $(this).val();
		$.ajax({
			type:"POST"
			, url : '/api/admin/event/feedback/ord'
			, cache:false
			, data : {code:code, idx:idx, ord:ord}
			, success : function(data) {
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('.btn-feedback').click(function(){
		var idx = $(this).data('idx');
		window.open('/admin/event/feedback/${code}/data/'+idx, 'event_data', 'width=600,height=850,status=1');
	});
	
	$('.btn-result').click(function(){
		var idx = $(this).data('idx');
		window.open('/admin/event/feedback/${code}/result/'+idx, 'event_result', 'width=600,height=850,status=1');
	});
	
});

function typeValied() {
	$('.answer-wrapper').show();
	$('.type').each(function(){
		if ($(this).prop('checked')) {
			if ($(this).val() == 'S') {
				$('.answer').val('');
				$('.answer-wrapper').hide();
			}
		}
	});
}
</script>
