<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${event.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>행사관리</li>
			<li class="active"><strong>${event.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>세션 목록</h5>
				</div>
				<div class="ibox-content">
					<c:forEach items="${dateList}" var="d">
						<div class="row">
							<div class="col-md-12">
								<h2>
									${d.edate}
									<small>
										<button type="button" class="btn btn-warning btn-xs btn-date-modify-toggle" data-idx="${d.idx }"><i class="fa fa-edit"></i></button>
										<button type="button" class="btn btn-danger btn-xs btn-date-delete" data-idx="${d.idx }"><i class="fa fa-trash"></i></button>
									</small>
									<span class="pull-right">
										<a href="/admin/event/${code}/play/${d.idx}" target="_blank" class="btn btn-success btn-lg btn-date-play"><i class="fa fa-play"></i> 운영시작</a>
									</span>
								</h2>
								<h4>장소 : ${d.place}</h4>
								<h4>좌장 : ${d.principal1}<c:if test="${d.principal2 ne null and d.principal2 ne '' }">, ${d.principal2}</c:if></h4>
							</div>
						</div>
						<div id="modify-date-wrapper-${d.idx}" class="row" style="display:none;">
							<div class="col-sm-12">
								<form id="modifyDateForm-${d.idx}" class="form-inline">
									<input type="hidden" name="code" value="${d.code}" />
									<input type="hidden" name="idx" value="${d.idx}" />
									<div class="form-group">
										<label for="edate" class="sr-only">행사일</label>
										<input type="text" placeholder="행사일" name="edate" value="${d.edate}" class="form-control datepicker">
									</div>
									<div class="form-group">
										<label for="place" class="sr-only">장소</label>
										<input type="text" placeholder="장소" name="place" value="${d.place}" class="form-control">
									</div>
									<div class="form-group">
										<label for="principal1" class="sr-only">좌장 1</label>
										<input type="text" placeholder="좌장 1" name="principal1" value="${d.principal1}" class="form-control">
									</div>
									<div class="form-group">
										<label for="principal2" class="sr-only">좌장 2</label>
										<input type="text" placeholder="좌장 2" name="principal2" value="${d.principal2}" class="form-control">
									</div>
									<div class="form-group">
										<button type="button" class="btn btn-warning btn-date-modify" data-idx="${d.idx}"><i class="fa fa-edit"></i> 날짜 수정</button>
									</div>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-striped table-hover table-bordered">
										<caption class="text-right">
											<button type="button" class="btn btn-primary btn-xs btn-session-add" data-code="${d.code}" data-idx="${d.idx}"><i class="fa fa-plus"></i> 세션 생성</button>
										</caption>
										<colgroup>
											<col class="col-sm-2" />
											<col class="col-sm-*" />
											<col class="col-sm-2" />
											<col class="col-sm-1" />
											<col class="col-sm-1" />
											<col class="col-sm-1" />
											<col class="col-sm-1" />
											<col class="col-sm-2" />
										</colgroup>
										<thead>
											<tr>
												<th class="text-center" rowspan="2">시간</th>
												<th class="text-center" rowspan="2">타이틀</th>
												<th class="text-center" rowspan="2">강연자</th>
												<th class="text-center" colspan="4">상태</th>
												<th class="text-center" rowspan="2">관리</th>
											</tr>
											<tr>
												<th class="text-center">강의록</th>
												<th class="text-center">CV</th>
												<th class="text-center">퀴즈</th>
												<th class="text-center">질문</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${d.sessions }" var="s">
												<tr>
													<td class="text-center">${s.stime} ~ ${s.etime}</td>
													<td class="text-center">${s.title}</td>
													<td class="text-center">${s.speaker}</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${s.filepath1 ne null and s.filepath1 ne '' }">Y</c:when>
															<c:otherwise>N</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center">
														<c:choose>
															<c:when test="${s.filepath2 ne null and s.filepath2 ne '' }">Y</c:when>
															<c:otherwise>N</c:otherwise>
														</c:choose>
													</td>
													<td class="text-center">${s.cnt_quiz}건</td>
													<td class="text-center">${s.cnt_question}건</td>
													<td class="text-center">
														<a href="/admin/event/${s.code}/${s.idx}" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> 수정</a>
														<a href="javascript:void(0);" class="btn btn-danger btn-xs btn-session-delete" data-code="${s.code}" data-idx="${s.idx}"><i class="fa fa-trash"></i> 삭제</a>
													</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						
						
						<hr />
					</c:forEach>
					<div class="row">
						<div class="col-sm-12">
							<form id="addDateForm" class="form-inline">
								<input type="hidden" name="code" value="${code}" />
								<div class="form-group">
									<label for="edate" class="sr-only">행사일</label>
									<input type="text" placeholder="행사일" id="edate" name="edate" class="form-control datepicker">
								</div>
								<div class="form-group">
									<label for="place" class="sr-only">장소</label>
									<input type="text" placeholder="장소" id="place" name="place" class="form-control">
								</div>
								<div class="form-group">
									<label for="principal1" class="sr-only">좌장 1</label>
									<input type="text" placeholder="좌장 1" id="principal1" name="principal1" class="form-control">
								</div>
								<div class="form-group">
									<label for="principal2" class="sr-only">좌장 2</label>
									<input type="text" placeholder="좌장 2" id="principal2" name="principal2" class="form-control">
								</div>
								<div class="form-group">
									<button type="button" id="btn-date-add" class="btn btn-primary"><i class="fa fa-plus"></i> 날짜 추가</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script type="text/javascript">
$(function(){
	
	$('#btn-date-add').click(function(){
		$.ajax({type:"POST", url : "/api/admin/event/date/add", cache:false
			, data : $('#addDateForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('.btn-date-modify-toggle').click(function(){
		var idx = $(this).data('idx');
		$('#modify-date-wrapper-'+idx).toggle();
	});
	
	$('.btn-date-modify').click(function(){
		var idx = $(this).data('idx');
		$.ajax({type:"POST", url : "/api/admin/event/date/modify", cache:false
			, data : $('#modifyDateForm-'+idx).serialize()
			, success : function(data) {
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('.btn-date-delete').click(function(){
		if (confirm('삭제 하시겠습니까?\n되돌릴 수 없습니다.')) {
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url : "/api/admin/event/date/delete", cache:false
				, data : $('#modifyDateForm-'+idx).serialize()
				, success : function(data) {
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$('.btn-session-add').click(function(){
		var code = $(this).data('code');
		var idx = $(this).data('idx');
		$.ajax({type:"POST", url : "/api/admin/event/session/add", cache:false
			, data : {code:code, edidx:idx}
			, success : function(data) {
				if (data.error == 0) {
					document.location.href = '/admin/event/'+code+'/'+data.data;
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('.btn-session-delete').click(function(){
		if (confirm('삭제 하시겠습니까?\n되돌릴 수 없습니다.')) {
			var code = $(this).data('code');
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url : "/api/admin/event/session/delete", cache:false
				, data : {code:code, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>