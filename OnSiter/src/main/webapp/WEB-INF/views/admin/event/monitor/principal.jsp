<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-popup-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2><span class="label label-success">좌장</span> ${event.title} - ${item.edate }</h2>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>전체 질문 내용</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-3"><select id="speaker" class="form-control"></select></th>
									<th class="text-center col-md-*">질문</th>
									<th class="text-center col-md-1">출력</th>
								</tr>
							</thead>
							<tbody id="question-list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-popup-footer.jsp"%>
<script id="question-list-item" type="x-tmpl-mustache">
<tr>
	<td>{{{speaker}}}</td>
	<td style="font-size:17px;">{{question}}</td>
	<td class="text-center">
		{{#isMonitor}}
			<button type="button" class="btn btn-xs btn-success btn-send-monitor" data-idx="{{idx}}"><i class="fa fa-laptop"></i> 취소</button>
		{{/isMonitor}}
		{{^isMonitor}}
			<button type="button" class="btn btn-xs btn-default btn-send-monitor" data-idx="{{idx}}"><i class="fa fa-laptop"></i> 출력</button>
		{{/isMonitor}}
	</td>
</tr>
</script>
<script type="text/javascript">
function loadQuestions() {
	var sidx = $('#speaker').val();
	$.ajax({
		type:"POST"
		, url : "/api/admin/event/question/list"
		, cache:false
		, data : {code:'${code}', edidx:'${idx}', sidx:sidx}
		, success : function(data) {
			if (data.error == 0) {
				var $speaker = $('#speaker');
				$speaker.empty();
				$speaker.append('<option value="">연자 전체</option>');
				
				var speakers = data.data.speakers;
				for (var i = 0; i < speakers.length; i++) {
					var s = speakers[i]; 
					$speaker.append('<option value="'+s.idx+'">'+s.speaker+'</option>');
				}
				$speaker.val(sidx);
				var $list = $('#question-list');
				$list.empty();
				
				var template = $('#question-list-item').html();
				Mustache.parse(template);
				
				var list = data.data.list;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.isMonitor = l.monitor=='Y';
					l.isStatus = l.status=='Y';
					if (l.isStatus) {
						$list.append(Mustache.render(template, l));
					}
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadQuestions();
	setInterval(loadQuestions, 5000);
	$('#speaker').change(function(){
		loadQuestions();
	});
	$(document).on('click', '.btn-send-monitor', function(){
		var idx = $(this).data('idx');
		$.ajax({
			type:"GET"
			, url : "/api/admin/event/question/monitor"
			, cache:false
			, data : {code:'${code}', idx:idx}
			, success : function(data) {
				if (data.error == 0) {
					loadQuestions();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>