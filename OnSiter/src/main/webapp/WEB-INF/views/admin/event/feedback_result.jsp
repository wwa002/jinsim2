<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />
<c:set var="admin" value="${sessionScope.MEMBER_SESSION }" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>온사이터 관리자 페이지</title>
	<link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/resources/plugins/jstree/themes/default/style.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
	
	<!-- Summernote -->
	<link href="/resources/plugins/summernote/summernote.css" rel="stylesheet">
	
	<!-- Dropzone -->
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/basic.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/dropzone.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/plugins/codemirror/ambiance.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
	<link href="/resources/css/admin.css" rel="stylesheet">
</head>

<body>
	<div id="wrapper">
		<div class="wrapper wrapper-content">
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-*" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">답1</th>
									<th class="text-center">답2</th>
									<th class="text-center">답3</th>
									<th class="text-center">답4</th>
									<th class="text-center">답5</th>
									<th>기타</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">${result.answer1}명 (<fmt:formatNumber value="${result.answer1_percent}" type="percent" pattern="#.##" />%)</td>
									<td class="text-center">${result.answer2}명 (<fmt:formatNumber value="${result.answer2_percent}" type="percent" pattern="#.##" />%)</td>
									<td class="text-center">${result.answer3}명 (<fmt:formatNumber value="${result.answer3_percent}" type="percent" pattern="#.##" />%)</td>
									<td class="text-center">${result.answer4}명 (<fmt:formatNumber value="${result.answer4_percent}" type="percent" pattern="#.##" />%)</td>
									<td class="text-center">${result.answer5}명 (<fmt:formatNumber value="${result.answer5_percent}" type="percent" pattern="#.##" />%)</td>
									<td>-</td>
								</tr>
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">${l.answer1}</td>
										<td class="text-center">${l.answer2}</td>
										<td class="text-center">${l.answer3}</td>
										<td class="text-center">${l.answer4}</td>
										<td class="text-center">${l.answer5}</td>
										<td>${l.answer_etc}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Mainly scripts -->
	<script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
	<script src="/resources/bootstrap/inspinia/js/bootstrap.min.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	
	<!-- Custom and plugin javascript -->
	<script src="/resources/bootstrap/inspinia/js/inspinia.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/pace/pace.min.js"></script>

	<!-- JSTree -->
	<script src="/resources/plugins/jstree/jstree.min.js"></script>

	<!-- iCheck -->
	<script src="/resources/bootstrap/inspinia/js/plugins/iCheck/icheck.min.js"></script>
	
	<!-- Data picker -->
	<script src="/resources/bootstrap/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>

	<!-- Clock picker -->
	<script src="/resources/bootstrap/inspinia/js/plugins/clockpicker/clockpicker.js"></script>
	
	<!-- jQuery Form -->
	<script src="/resources/jquery/jquery.form.min.js"></script>
	
	<!-- jQuery QR Code -->
	<script src="/resources/jquery/jquery.qrcode.min.js"></script>
	
	<!-- DataTable -->
	<script src="/resources/bootstrap/inspinia/js/plugins/dataTables/datatables.min.js"></script>
	
	<!-- Template -->
	<script src="/resources/plugins/mustache/mustache.min.js"></script>
	
	<!-- SUMMERNOTE -->
	<script src="/resources/plugins/summernote/summernote.js"></script>
	<script src="/resources/plugins/summernote/lang/summernote-ko-KR.min.js"></script>
	
	<!-- DROPZONE -->
	<script src="/resources/bootstrap/inspinia/js/plugins/dropzone/dropzone.js"></script>
	
	<!-- CodeMirror -->
    <script src="/resources/bootstrap/inspinia/js/plugins/codemirror/codemirror.js"></script>
    <script src="/resources/bootstrap/inspinia/js/plugins/codemirror/mode/javascript/javascript.js"></script>
	
	<!-- blueimp gallery -->
    <script src="/resources/bootstrap/inspinia/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
    
    <!-- Moment -->
    <script src='/resources/plugins/moment/moment.min.js'></script>
	
	<script src="/resources/js/admin.js"></script>
</body>
</html>


<script type="text/javascript">
$(function(){
	
});
</script>
