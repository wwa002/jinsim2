<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>행사관리</h2>
		<ol class="breadcrumb">
			<li><a href="/admin/dashboard">HOME</a></li>
			<li class="active"><strong>행사관리</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>학술 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-md-12">
							<form id="searchForm" class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
								<col class="col-sm-4" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">행사코드</th>
									<th>행사명</th>
									<th>경로</th>
									<th class="text-center">상태</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td class="text-center"><a href="/admin/event/editor/${l.code}">${l.code}</a></td>
										<td><a href="/admin/event/content/${l.code}/list">${l.title}</a></td>
										<td>
											<a href="/e/${l.code}" target="_blank">/e/${l.code}</a>
										</td>
										<td class="text-center">
											<c:choose>
												<c:when test="${l.status eq 'Y'}">사용중</c:when>
												<c:otherwise>사용안함</c:otherwise>
											</c:choose>
										</td>
										<td class="text-center">
											<c:choose>
												<c:when test="${l.ismain eq 'Y'}">
													<button type="button" class="btn btn-xs btn-primary"><i class="fa fa-check-square"></i> 메인</button>
												</c:when>
												<c:otherwise>
													<button type="button" class="btn btn-xs btn-default btn-main" data-code="${l.code}"><i class="fa fa-check-square"></i> 메인</button>
												</c:otherwise>
											</c:choose>
											<a href="/admin/event/feedback/${l.code}" class="btn btn-warning btn-xs"><i class="fa fa-question"></i> 피드백</a>
											<a href="javascript:void(0);" class="btn btn-default btn-qrcode btn-xs" data-code="${l.code}"><i class="fa fa-qrcode"></i> QR</a>
											<a href="/e/${l.code}" target="_blank" class="btn btn-success btn-xs"><i class="fa fa-play"></i> 보기</a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div id="qrcode-wrapper" class="row hidden">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>QRCode</h5>
						</div>
						<div class="ibox-content">
							<h3>접속정보</h3>
							<div>기본 URL : <span id="longUrl"></span></div>
							<div>단축 URL : <span id="shortUrl"></span></div>
							<div>QR 코드</div>
							<div id="qrcode"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>행사 생성</h5>
						</div>
						<div class="ibox-content">
							<form id="eventForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<label class="col-sm-4 control-label">행사 코드</label>
									<div class="col-sm-8">
										<div class="input-group">
											<input type="text" name="code" id="code" maxlength="30" class="form-control" />
											<span class="input-group-btn">
												<button type="button" id="code-check" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">행사 이름</label>
									<div class="col-sm-8">
										<input type="text" name="title" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<button type="button" id="btn-create" class="btn btn-primary">생성</button>
										<button type="reset" class="btn btn-default">취소</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script type="text/javascript">
function load() {
	gapi.client.setApiKey('AIzaSyDPvKF4YJBueGygxrBPOtFfgSuT2dWqjQ8');
}

$(function(){
	$("#code-check").click(function(){
		var code = $("#code").val();
		$.ajax({type:"POST", url : "/api/admin/event/check", cache:false
			, data : {code:code}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("사용할 수 있는 코드 입니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-create").click(function(){
		if (confirm("입력하신 정보로 행사를 생성하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/admin/event/create", cache:false
				, data : $("#eventForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-main").click(function(){
		if (confirm("선택하신 행사를 메인으로 지정하시겠습니까?")) {
			var code = $(this).data("code");
			$.ajax({type:"POST", url : "/api/admin/event/main", cache:false
				, data : {code:code}
				, success : function(data) {
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$('.btn-qrcode').click(function(){
		var code = $(this).data('code');
		var longUrl = 'http://'+window.location.hostname+'/e/'+code;
		gapi.client.load('urlshortener', 'v1',function(){
			var request = gapi.client.urlshortener.url.insert({
		        'resource' : {
		            'longUrl' : longUrl
		        }
		    });
		    request.execute(function(response) {
		        if (response.id != null) {        
		            var shortUrl = response.id;
		            $('#longUrl').text(longUrl);
		            $('#shortUrl').text(shortUrl);
		            $("#qrcode").empty();
		            $("#qrcode").qrcode({width:300, height:300, text: shortUrl});
		            $('#qrcode-wrapper').removeClass('hidden');
		        } else {
		            alert("error: creating short url");
		        }
		    });
		});
	});
});
</script>
<!-- Google -->
<script src="https://apis.google.com/js/client.js?onload=load"></script>