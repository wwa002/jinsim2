<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>온사이터 관리자 페이지</title>
	<style type="text/css">
		body {position:relative; margin:0px; padding:0px; background:#101a39; color:#ffffff;}
		#wrapper {position:fixed; left:0px; top:60px; right:0px; bottom:0px;}
		#wrapper h1 {margin:0px; padding:50px; font-size:3em;}
		#title-wrapper {width:90%; height:121px; border:#fff01d solid 6px;}
		#title-wrapper img {float:left; clear:left;}
		#title-wrapper h1 { padding:15px 0px 0px 247px; color:#fff01d; font-size:80px; text-align:center;}
	</style>
</head>

<body>
	<div id="wrapper">
		<div id="title-wrapper">
			<img src="/resources/images/qna_01.jpg" />
			<h1>실시간 질문</h1>
		</div>
		<h1 id="question"></h1>
	</div>
</body>
</html>

<script src="/resources/jquery/jquery.min.js"></script>
<script type="text/javascript">
function loadQuestions() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/event/question/current"
		, cache:false
		, data : {code:'${code}', edidx:'${idx}'}
		, success : function(data) {
			if (data.error == 0) {
				if (data.data != undefined) {
					$('#question').text(data.data.question);
				} else {
					$('#question').text('');
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}
$(function(){
	loadQuestions();
	setInterval(loadQuestions, 5000);
});
</script>