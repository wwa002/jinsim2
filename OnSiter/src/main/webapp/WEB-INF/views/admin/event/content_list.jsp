<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>행사관리</h2>
		<ol class="breadcrumb">
			<li><a href="/admin/dashboard">HOME</a></li>
			<li>행사관리</li>
			<li>${event.title}</li>
			<li><strong>페이지 관리</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>페이지 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-md-2">
							<button type="button" id="btn-page-add" class="btn btn-primary"><i class="fa fa-plus"></i> 페이지 생성</button>
						</div>
						<div class="col-md-10">
							<form id="searchForm" class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th>페이지명</th>
									<th>경로</th>
									<th class="text-center">상태</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td><a href="/admin/event/content/${l.code}/${l.idx}">${l.title}</a></td>
										<td>
											<c:choose>
												<c:when test="${l.code1 ne null and l.code1 ne '' }">
													<c:set var="cpath" value="/e/${l.code}/${l.code1}" />
													<c:if test="${l.code2 ne null and l.code2 ne '' }">
														<c:set var="cpath" value="${cpath}/${l.code2}" />
													</c:if>
													<c:if test="${l.code3 ne null and l.code3 ne '' }">
														<c:set var="cpath" value="${cpath}/${l.code3}" />
													</c:if>
													<a href="${cpath}" target="_blank">${cpath}</a>
												</c:when>
												<c:otherwise>-</c:otherwise>
											</c:choose>
										</td>
										<td class="text-center">
											<c:choose>
												<c:when test="${l.isuse eq 'Y'}">사용중</c:when>
												<c:otherwise>사용안함</c:otherwise>
											</c:choose>
										</td>
										<td class="text-center">
											<a href="/admin/event/content/${l.code}/${l.idx}" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
											<a href="javascript:void(0);" class="btn btn-danger btn-delete btn-xs" data-code="${l.code}" data-idx="${l.idx}"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script type="text/javascript">
$(function(){
	$('#btn-page-add').click(function(){
		$.ajax({type:"POST", url : "/api/admin/event/page/add", cache:false
			, data : {code:'${code}'}
			, success : function(data) {
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('.btn-delete').click(function(){
		if (confirm('삭제 하시겠습니까?')) {
			var code = $(this).data('code');
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url : "/api/admin/event/page/delete", cache:false
				, data : {code:code, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>