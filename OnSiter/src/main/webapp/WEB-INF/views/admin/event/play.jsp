<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-popup-header.jsp"%>
		<div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-sm-12">
				<h2><span class="label label-primary">운영자</span> ${event.title} - ${eventDate.edate }</h2>
			</div>
		</div>
		<div class="wrapper wrapper-content">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>세션 목록</h5>
						</div>
						<div class="ibox-content">
							<div class="row text-right">
								<div class="col-sm-12">
									<a href="/admin/event/${code}/monitor/principal/${idx}" target="_blank" class="btn btn-success">좌장 모니터</a>
									<a href="/admin/event/${code}/monitor/vote/${idx}" target="_blank" class="btn btn-warning">퀴즈 모니터</a>
									<a href="/admin/event/${code}/monitor/question/${idx}" target="_blank" class="btn btn-warning">질문 모니터</a>
								</div>
							</div>
							<hr />
							<div class="row text-right">
								<div class="col-sm-12">
									<span class="label label-danger">※ 주의!</span> 세션별로 좌장모니터, 퀴즈모니터, 질문모니터는 새로 열고, 기존에 3가지 열린창은 모두 닫아주십시오.
								</div>
							</div>
							<hr />
							<div class="row">
								<div class="col-sm-12">
									<div class="table-responsive">
										<table class="table table-striped table-hover table-bordered">
											<colgroup>
												<col class="col-sm-2" />
												<col class="col-sm-*" />
												<col class="col-sm-2" />
												<col class="col-sm-1" />
												<col class="col-sm-1" />
												<col class="col-sm-1" />
												<col class="col-sm-1" />
												<col class="col-sm-2" />
											</colgroup>
											<thead>
												<tr>
													<th class="text-center" rowspan="2">시간</th>
													<th class="text-center" rowspan="2">타이틀</th>
													<th class="text-center" rowspan="2">강연자</th>
													<th class="text-center" colspan="4">상태</th>
													<th class="text-center" rowspan="2">운영 모니터</th>
												</tr>
												<tr>
													<th class="text-center">강의록</th>
													<th class="text-center">CV</th>
													<th class="text-center">퀴즈</th>
													<th class="text-center">질문</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items="${sessions }" var="s">
													<tr>
														<td class="text-center">${s.stime} ~ ${s.etime}</td>
														<td class="text-center">${s.title}</td>
														<td class="text-center">${s.speaker}</td>
														<td class="text-center">
															<c:choose>
																<c:when test="${s.filepath1 ne null and s.filepath1 ne '' }">Y</c:when>
																<c:otherwise>N</c:otherwise>
															</c:choose>
														</td>
														<td class="text-center">
															<c:choose>
																<c:when test="${s.filepath2 ne null and s.filepath2 ne '' }">Y</c:when>
																<c:otherwise>N</c:otherwise>
															</c:choose>
														</td>
														<td class="text-center">${s.cnt_quiz}건</td>
														<td class="text-center">${s.cnt_question}건</td>
														<td class="text-center">
															<c:if test="${s.type eq 'S'}">
																<a href="/admin/event/${code}/monitor/speaker/${s.idx}" target="_blank" class="btn btn-warning">강연자</a>
																<a href="/admin/event/${code}/monitor/operator/${s.idx}" target="_blank" class="btn btn-primary">퀴즈&amp;질문</a>
															</c:if>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<%@ include file="/WEB-INF/views/include/admin-popup-footer.jsp"%>

<script type="text/javascript">
$(function(){
	
});
</script>