<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${item.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/admin/dashboard">HOME</a></li>
			<li>행사관리</li>
			<li>${event.title}</li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-save" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content">
	<form id="modifyForm" class="form-horizontal">
		<input type="hidden" name="code" value="${code}" />
		<input type="hidden" name="idx" value="${idx}" />
		<div class="row">
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-12">
						<div class="ibox">
							<div class="ibox-title">
								<h5>콘텐츠 기본정보</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">제목</label>
											<div class="col-sm-10">
												<input type="text" name="title" class="form-control" value="${item.title}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">1차 경로</label>
											<div class="col-sm-10">
												<input type="text" name="code1" class="form-control" value="${item.code1}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">2차 경로</label>
											<div class="col-sm-10">
												<input type="text" name="code2" class="form-control" value="${item.code2}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">3차 경로</label>
											<div class="col-sm-10">
												<input type="text" name="code3" class="form-control" value="${item.code3}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">사용여부</label>
											<div class="col-sm-10">
												<label class="radio-inline i-checks">
													<input type="radio" value="Y" name="isuse" <c:if test="${item.isuse eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
												</label>
												<label class="radio-inline i-checks">
													<input type="radio" value="N" name="isuse" <c:if test="${item.isuse eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>Index</h3>
										<textarea name="content" id="content">${fn:escapeXml(item.content)}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>탐색기</h5>
				</div>
				<div class="ibox-content">
					<div id="resources"></div>
				</div>
			</div>
		</div>
		</div>
	</form>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script type="text/javascript">
function loadTree() {
	$('#resources').jstree({
		'core' : {
			'data' : {
				'url' : function(node) {
					return "/api/admin/resource/jstree";
				},
				'data' : function(node) {
					return {
						'id' : node.id
					};
				}
			}
		}
	});
	
	$('#resources').on("changed.jstree", function (e, data) {
		console.log(data.node);
		if (data.node.id == "0") {
		} else {
			if (data.node.data.isfolder == "Y") {
			} else {
				console.log(data.node.data);
				var path = '/res/'+data.node.data.idx+'/'+data.node.data.name;
				window.prompt("경로복사", path);
			}
		}
	});
}

function save() {
	content.save();
	$.ajax({type:"POST", url : "/api/admin/event/page/modify", cache:false
		, data : $("#modifyForm").serialize()
		, success : function(data) {
			if (data.error == 0) {
				alert("저장되었습니다.");
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

var content;
$(function(){
	loadTree();
	
	content = CodeMirror.fromTextArea(document.getElementById("content"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	$("#btn-save").click(function(){
		save();
	});
	
	$(window).bind('keydown', function(event) {
		if (event.ctrlKey || event.metaKey) {
			switch (String.fromCharCode(event.which).toLowerCase()) {
			case 's':
				event.preventDefault();
				save();
				break;
			}
		}
	});
});
</script>









