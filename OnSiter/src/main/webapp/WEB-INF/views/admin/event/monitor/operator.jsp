<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-popup-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2><span class="label label-primary">운영자</span> ${item.title }</h2>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-content">
					<div class="row text-right">
						<div class="col-sm-12">
							<a href="/admin/event/${code}/monitor/principal/${item.edidx}" target="_blank" class="btn btn-success">좌장 모니터</a>
							<a href="/admin/event/${code}/monitor/vote/${item.edidx}" target="_blank" class="btn btn-warning">퀴즈 모니터</a>
							<a href="/admin/event/${code}/monitor/question/${item.edidx}" target="_blank" class="btn btn-warning">질문 모니터</a>
						</div>
					</div>
					<hr />
					<div class="row text-right">
						<div class="col-sm-12">
							<span class="label label-danger">※ 주의!</span> 세션별로 좌장모니터, 퀴즈모니터, 질문모니터는 새로 열고, 기존에 3가지 열린창은 모두 닫아주십시오.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>실시간 투표</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-*">질문</th>
									<th class="text-center col-md-3">관리</th>
									<th class="text-center col-md-4">모니터</th>
								</tr>
							</thead>
							<tbody id="vote-list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="ibox">
				<div class="ibox-title">
					<h5>전체 질문 내용</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-3"><select id="speaker" class="form-control"></select></th>
									<th class="text-center col-md-*">질문</th>
									<th class="text-center col-md-3">관리</th>
								</tr>
							</thead>
							<tbody id="question-list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox">
				<div class="ibox-title">
					<h5>좌장에게 전달된 질문</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-3">연자</th>
									<th class="text-center col-md-*">질문</th>
								</tr>
							</thead>
							<tbody id="question-principal-list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="vote-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title">투표 진행중</h4>
				<small class="font-bold">현재 투표를 진행중입니다.</small>
			</div>
			<div class="modal-body text-center">
				남은시간 : <span id="vote-second"></span>초
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="vote-result" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title" id="vote-result-title"></h4>
			</div>
			<div class="modal-body text-center">
				<canvas id="chart" style="height:140px;"></canvas>
				<hr />
				<div id="vote-result-answer" class="row">
					<div class="col-sm-2 col-sm-offset-1 text-center" id="vote-result-answer1"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer2"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer3"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer4"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer5"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-popup-footer.jsp"%>
<script id="vote-list-item" type="x-tmpl-mustache">
<tr>
	<td>{{question}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-primary btn-run" data-idx="{{idx}}"><i class="fa fa-play"></i></button>
		<!-- <button type="button" class="btn btn-xs btn-success btn-result" data-idx="{{idx}}"><i class="fa fa-laptop"></i></button> -->
		<button type="button" class="btn btn-xs btn-danger btn-reset" data-idx="{{idx}}"><i class="fa fa-refresh"></i></button>
	</td>
	<td class="btn-group text-center">
		{{#isMonitor}}
			<button type="button" class="btn btn-xs btn-success btn-vote-monitor" data-idx="{{idx}}" data-monitor="N"><i class="fa fa-laptop"></i> 투표</button>
		{{/isMonitor}}
		{{^isMonitor}}
			<button type="button" class="btn btn-xs btn-default btn-vote-monitor" data-idx="{{idx}}" data-monitor="Y"><i class="fa fa-laptop"></i> 투표</button>
		{{/isMonitor}}
		{{#isMonitorResult}}
			<button type="button" class="btn btn-xs btn-success btn-vote-monitor" data-idx="{{idx}}" data-monitor="N"><i class="fa fa-laptop"></i> 결과</button>
		{{/isMonitorResult}}
		{{^isMonitorResult}}
			<button type="button" class="btn btn-xs btn-default btn-vote-monitor" data-idx="{{idx}}" data-monitor="R"><i class="fa fa-laptop"></i> 결과</button>
		{{/isMonitorResult}}
	</td>
</tr>
</script>
<script id="vote-result-list-item" type="x-tmpl-mustache">
<tr>
	<td>{{no}}</td>
	<td>{{answer1}}</td>
	<td>{{answer2}}</td>
	<td>{{answer3}}</td>
	<td>{{answer4}}</td>
	<td>{{answer5}}</td>
	<td>{{answer_etc}}</td>
</tr>
</script>

<script id="question-list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{{speaker}}}</td>
	<td>{{question}}</td>
	<td class="btn-group text-center">
		{{#isMonitor}}
			<button type="button" class="btn btn-xs btn-success btn-send-monitor" data-idx="{{idx}}"><i class="fa fa-laptop"></i> 취소</button>
		{{/isMonitor}}
		{{^isMonitor}}
			<button type="button" class="btn btn-xs btn-default btn-send-monitor" data-idx="{{idx}}"><i class="fa fa-laptop"></i> 출력</button>
		{{/isMonitor}}
		{{#isStatus}}
			<button type="button" class="btn btn-xs btn-warning btn-send-principal" data-idx="{{idx}}">취소 &lt;</button>
		{{/isStatus}}
		{{^isStatus}}
			<button type="button" class="btn btn-xs btn-danger btn-send-principal" data-idx="{{idx}}">전달 &gt;</button>
		{{/isStatus}}
	</td>
</tr>
</script>

<script id="question-principal-list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{{speaker}}}</td>
	<td>{{question}}</td>
</tr>
</script> 

<script type="text/javascript">
var timer;
var time = 0;

function loadVoteList() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/event/vote/list"
		, cache:false
		, data : {code:'${item.code}', edidx:'${item.edidx}', sidx:'${idx}'}
		, success : function(data) {
			if (data.error == 0) {
				var $list = $('#vote-list');
				$list.empty();
				
				var template = $('#vote-list-item').html();
				Mustache.parse(template);
				
				var list = data.data;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.isMonitor = l.monitor == 'Y';
					l.isMonitorResult = l.monitor == 'R';
					$list.append(Mustache.render(template, list[i]));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function voteTimerStart() {
	if (timer) {
		alert('현재 진행중인 투표가 있습니다.');
		return;
	}
	
	time = 15;
	$('#vote-second').text(time);
	timer = setInterval(voteTimerRunning, 1000);
	$('#vote-modal').modal('show');
}

function voteTimerRunning() {
	if (time <= 0) {
		clearInterval(timer);
		timer = null;
		$('#vote-modal').modal('hide');
		return;
	}
	
	time--;
	$('#vote-second').text(time);
}

function loadQuestions() {
	var sidx = $('#speaker').val();
	$.ajax({
		type:"POST"
		, url : "/api/admin/event/question/list"
		, cache:false
		, data : {code:'${item.code}', edidx:'${item.edidx}', sidx:sidx}
		, success : function(data) {
			if (data.error == 0) {
				var $speaker = $('#speaker');
				$speaker.empty();
				$speaker.append('<option value="">연자 전체</option>');
				
				var speakers = data.data.speakers;
				for (var i = 0; i < speakers.length; i++) {
					var s = speakers[i]; 
					$speaker.append('<option value="'+s.idx+'">'+s.speaker+'</option>');
				}
				$speaker.val(sidx);
				var $list = $('#question-list');
				var $listPrincipal = $('#question-principal-list');
				$list.empty();
				$listPrincipal.empty();
				
				var template = $('#question-list-item').html();
				var templatePrincipal = $('#question-principal-list-item').html();
				Mustache.parse(template);
				Mustache.parse(templatePrincipal);
				
				var list = data.data.list;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.isStatus = l.status=='Y';
					l.isMonitor = l.monitor=='Y';
					$list.append(Mustache.render(template, l));
					if (l.isStatus) {
						$listPrincipal.append(Mustache.render(templatePrincipal, l));
					}
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadVoteList();
	loadQuestions();
	setInterval(loadQuestions, 5000);
	
	$(document).on('click', '.btn-run', function(){
		if (timer) {
			alert('현재 진행중인 투표가 있습니다.');
			return;
		}
		if (confirm('투표를 시작하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/event/vote/run"
				, cache:false
				, data : {code:'${code}', idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						voteTimerStart();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-reset', function(){
		if (confirm('투표를 리셋하시겠습니까?\n리셋하시면 투표 진행여부와 투표에 참여한 데이터가 초기화 됩니다.')) {
			var idx = $(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/event/vote/reset"
				, cache:false
				, data : {code:'${code}', idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						alert('투표가 초기화 되었습니다.');
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-result', function(){
		var idx=$(this).data('idx');
		$.ajax({
			type:"GET"
			, url : "/api/admin/event/vote/result"
			, cache:false
			, data : {code:'${code}', idx:idx}
			, success : function(data) {
				if (data.error == 0) {
					$('#vote-result-title').text(data.data.item.question);
					$('#vote-result-answer1').html('1. '+data.data.item.answer1 + '<br />'+data.data.result.answer1+'회<br />'+parseFloat(data.data.result.answer1_percent).toFixed(1)+'%');
					$('#vote-result-answer2').html('2. '+data.data.item.answer2 + '<br />'+data.data.result.answer2+'회<br />'+parseFloat(data.data.result.answer2_percent).toFixed(1)+'%');
					$('#vote-result-answer3').html('3. '+data.data.item.answer3 + '<br />'+data.data.result.answer3+'회<br />'+parseFloat(data.data.result.answer3_percent).toFixed(1)+'%');
					$('#vote-result-answer4').html('4. '+data.data.item.answer4 + '<br />'+data.data.result.answer4+'회<br />'+parseFloat(data.data.result.answer4_percent).toFixed(1)+'%');
					$('#vote-result-answer5').html('5. '+data.data.item.answer5 + '<br />'+data.data.result.answer5+'회<br />'+parseFloat(data.data.result.answer5_percent).toFixed(1)+'%');
					
					var $list = $('#vote-result-history');
					$list.empty();
					
					var doughnutData = {
				        labels: [data.data.item.answer1
				                 , data.data.item.answer2
				                 , data.data.item.answer3
				                 , data.data.item.answer4
				                 , data.data.item.answer5],
				        datasets: [{
				            data: [parseFloat(data.data.result.answer1_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer2_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer3_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer4_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer5_percent).toFixed(1)],
				            backgroundColor: ["#a3e1d4","#dedede","#b5b8cf","#f3a4a4","#e5e19c"]
				        }]
				    } ;

				    var doughnutOptions = {
				        responsive: true
				    };

				    var ctx4 = document.getElementById("chart").getContext("2d");
				    new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
					
					$('#vote-result').modal('show');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('#speaker').change(function(){
		loadQuestions();
	});
	
	$(document).on('click', '.btn-send-monitor', function(){
		var idx = $(this).data('idx');
		$.ajax({
			type:"GET"
			, url : "/api/admin/event/question/monitor"
			, cache:false
			, data : {code:'${code}', idx:idx}
			, success : function(data) {
				if (data.error == 0) {
					loadQuestions();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-send-principal', function(){
		var idx = $(this).data('idx');
		$.ajax({
			type:"GET"
			, url : "/api/admin/event/question/principal"
			, cache:false
			, data : {code:'${code}', idx:idx}
			, success : function(data) {
				if (data.error == 0) {
					loadQuestions();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-vote-monitor', function(){
		var idx = $(this).data('idx');
		var monitor = $(this).data('monitor');
		$.ajax({
			type:"GET"
			, url : "/api/admin/event/vote/monitor"
			, cache:false
			, data : {code:'${code}', edidx:'${item.edidx}', idx:idx, monitor:monitor}
			, success : function(data) {
				if (data.error == 0) {
					loadVoteList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	
});
</script>