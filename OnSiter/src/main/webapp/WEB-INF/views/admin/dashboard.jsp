<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>접속량</h5>
						</div>
						<div class="ibox-content">
							<div class="flot-chart">
								<div class="flot-chart-content" id="flot-dashboard-chart"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<!-- Flot -->
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.js"></script>
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.time.js"></script>

<script type="text/javascript">
function gd(year, month, day) {
	return new Date(year, month - 1, day).getTime();
}

function gd2(date) {
	var year = parseInt(date.substring(0, 4));
	var month = parseInt(date.substring(4, 6));
	var day = parseInt(date.substring(6, 8));
	return new Date(year, month - 1, day).getTime();
}

$(function(){
	$.ajax({
		type : "GET"
			, url : "/api/admin/dashboard"
			, cache : false
			, success : function(data){
				if (data.error == 0) {
					var counter = data.data.counter;
					var data2 = [];
					var data3 = [];
					for (var i = 0; i < counter.length; i++) {
						var c = counter[i];
						data2.push([gd2(c.date), c.visit]);
						data3.push([gd2(c.date), c.page]);
					}
					
					var dataset = [ 
						{label : "접속자", data : data2, color : "#1ab394", bars : {show : true, align : "center", barWidth : 24 * 60 * 60 * 600, lineWidth : 0}}
						, {label : "페이지 뷰", data : data3, yaxis : 2, color : "#1C84C6"
							, lines : {lineWidth : 1, show : true, fill : true
								, fillColor : {colors : [ {opacity : 0.2}, {opacity : 0.4} ]}}
							, splines : {show : false,tension : 0.6,lineWidth : 1,fill : 0.1}}
						];
					var options = {
						xaxis : {mode : "time", tickSize : [ 3, "day" ], tickLength : 0, axisLabel : "Date", axisLabelUseCanvas : true, axisLabelFontSizePixels : 12, axisLabelFontFamily : 'Arial', axisLabelPadding : 10, color : "#d5d5d5"}
						, yaxes : [ 
							{position : "left", color : "#d5d5d5", axisLabelUseCanvas : true, axisLabelFontSizePixels : 12, axisLabelFontFamily : 'Arial', axisLabelPadding : 3}
							, {position : "right", clolor : "#d5d5d5", axisLabelUseCanvas : true, axisLabelFontSizePixels : 12, axisLabelFontFamily : ' Arial', axisLabelPadding : 67}
							]
						, legend : {noColumns : 1, labelBoxBorderColor : "#000000", position : "nw"}
						, grid : {hoverable : false, borderWidth : 0}
					};
					var previousPoint = null, previousLabel = null;
					$.plot($("#flot-dashboard-chart"), dataset, options);
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
	
});
</script>