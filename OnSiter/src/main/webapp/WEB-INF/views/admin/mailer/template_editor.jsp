<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>템플릿</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>메일</li>
			<li>템플릿</li>
			<li class="active"><strong>${template.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-5">
			<div class="ibox">
				<div class="ibox-title">
					<h5>템플릿 생성</h5>
				</div>
				<div class="ibox-content">
					<form id="modifyForm" class="form-horizontal" enctype="multipart/form-data">
						<input type="hidden" name="idx" value="${idx}" />
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="title" value="${template.title }" class="form-control" placeholder="제목을 입력해 주세요." />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="code" value="${template.code }" class="form-control" placeholder="코드를 입력해 주세요." />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<textarea name="content" id="content" class="hidden">${fn:escapeXml(template.content)}</textarea>
								<div id="summernote"></div>
							</div>
						</div>
						<div class="text-right">
							<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
						</div>
					</form>
					<hr />
					<div>
						수신거부 URL : <span id="refuse-url"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="ibox">
				<div class="ibox-title">
					<h5>파일관리</h5>
				</div>
				<div class="ibox-content">
					<div class="row" id="file-list">
						
					</div>
					<hr />
					<div class="row">
						<div class="col-sm-12">
							<form id="uploadForm" method="post" enctype="multipart/form-data" class="form-horizontal uploadForm" onsubmit="return false;">
								<input type="hidden" name="tidx" value="${idx}">
								<div class="form-group">
									<div class="col-sm-12">
										<div class="input-group">
											<input type="file" id="uploadForm-file" name="file" class="form-control" />
											<span class="input-group-btn">
												<button type="button" class="btn btn-primary btn-upload" data-id="uploadForm"><i class="fa fa-plus"></i></button>
											</span>
 										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>미리보기</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<iframe id="display" src="/admin/mailer/template/${idx}/view" style="width:100%; height:600px; border:none;"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="file-item" type="x-tmpl-mustache">
<div class="col-sm-12" id="file-item-{{idx}}">
	<h4>
		<a href="/mail/${idx}/{{idx}}/{{filename}}" target="_blank">{{no}}. {{filename}}</a>
		<span class="btn-group pull-right">
			<button type="button" data-filename="{{filename}}" data-url="http://{{url}}/mail/${idx}/{{idx}}/{{filename}}" class="btn btn-xs btn-primary btn-code-file"><i class="fa fa-code"></i></button>
			<button type="button" data-idx="{{idx}}" class="btn btn-xs btn-danger btn-delete-file"><i class="fa fa-trash"></i></button>
		</span>
	</h4>
	<form id="uploadForm-{{idx}}" method="post" enctype="multipart/form-data" class="form-horizontal uploadForm" onsubmit="return false;">
		<input type="hidden" name="tidx" value="${idx}">
		<input type="hidden" name="idx" value="{{idx}}">
		<div class="form-group">
			<div class="col-sm-12">
				<div class="input-group">
					<input type="file" id="uploadForm-{{idx}}-file" name="file" class="form-control" />
					<span class="input-group-btn">
						<button type="button" class="btn btn-warning btn-upload" data-id="uploadForm-{{idx}}"><i class="fa fa-edit"></i></button>
					</span>
				</div>
			</div>
		</div>
	</form>
</div>
</script>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script type="text/javascript">
function loadRefuseUrl() {
	var refuseUrl = 'http://'+document.location.hostname+'/refuse/email';
	gapi.client.setApiKey('AIzaSyDPvKF4YJBueGygxrBPOtFfgSuT2dWqjQ8');
	gapi.client.load('urlshortener', 'v1',function(){
		if (refuseUrl != "") {
			var request = gapi.client.urlshortener.url.insert({
		        'resource' : {
		            'longUrl' : refuseUrl
		        }
		    });
		    request.execute(function(response) {
		        if (response.id != null) {        
		            console.log(response.id);
		            shortUrl = response.id;
		            $("#refuse-url").html('<a href="'+response.id+'" target="_blank">'+response.id+'</a>');
		        } else {
		            alert("error: creating short url");
		        }
		    });
		}
	});
}

$(function(){
	$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	$('#summernote').summernote('code', $("#content").val());
	$("#modifyForm").submit(function(){
		$("#content").val($('#summernote').summernote('code'));
		$.ajax({type:"POST", url : "/api/admin/mail/template/modify", cache:false
			, data : $("#modifyForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$("#display").attr('src', '/admin/mailer/template/${idx}/view');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	loadFileList();
	
	$(document).on('click', '.btn-upload', function(){
		var id = $(this).data('id');
		if (!$('#'+id+'-file').val()) {
			alert('파일을 선택해 주세요.');
			return false;
		}
		
		$('#'+id).ajaxForm({
			type:"POST"
			, url:"/api/admin/mail/template/file"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					$('#'+id)[0].reset();
					loadFileList();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$('#'+id).submit();
		
		return false;
	});
	
	$(document).on('click', '.btn-delete-file', function(){
		if (confirm('삭제 하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url : "/api/admin/mail/template/file/delete", cache:false
				, data : {tidx:'${idx}', idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadFileList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-code-file', function(){
		var url = $(this).data('url');
		var filename = $(this).data('filename');
		var farr = filename.split('.');
		var extension = farr[farr.length-1].toLowerCase();
		var html = "";
		if (extension == 'jpg' || extension == 'png' || extension == 'gif') {
			html = '<div><img src="'+url+'" alt="'+filename+'" /></div>';
		} else {
			html = '<div>첨부파일 - <a href="'+url+'">'+filename+'</a></div>';
		}
		$('#summernote').summernote('code', $('#summernote').summernote('code') + html)
	});
	
});

function loadFileList() {
	$.ajax({type:"GET", url : "/api/admin/mail/template/file/list", cache:false
		, data : {tidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $list = $('#file-list');
				$list.empty();
				
				var list = data.data;
				var template = $('#file-item').html();
				Mustache.parse(template);
				
				for (var i = 0; i < list.length; i++) {
					var f = list[i];
					f.no = i+1;
					f.url = window.location.hostname;
					var rendered = Mustache.render(template, f);
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}
</script>
<script src="https://apis.google.com/js/client.js?onload=loadRefuseUrl"></script>