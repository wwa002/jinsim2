<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>템플릿</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>메일</li>
			<li class="active"><strong>템플릿</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>템플릿 생성</h5>
				</div>
				<div class="ibox-content">
					<form id="templateForm" class="form-horizontal" enctype="multipart/form-data">
						<div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<input type="text" name="title" class="form-control" placeholder="제목을 입력해 주세요." />
									<span class="input-group-btn">
										<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button>
									</span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox">
				<div class="ibox-title">
					<h5>템플릿 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" id="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">제목</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		<div id="display-wrapper" class="col-sm-6 hidden">
			<div class="ibox">
				<div class="ibox-title">
					<h5>미리보기</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<iframe id="display" style="width:100%; height:600px; border:none;"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td><a href="/admin/mailer/template/{{idx}}">{{title}}</a></td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-primary btn-view" data-idx="{{idx}}"><i class="fa fa-laptop"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>
<script type="text/javascript">
function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	var keyword = $("#keyword").val();
	$.ajax({type:"GET", url:"/api/admin/mail/template/list", cache:false
		, data : {page:page, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, idx:l.idx, title:l.title
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadList(1);
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	$("#btn-search").click(function(){
		loadList(1);
	});
	
	$("#templateForm").submit(function(){
		$.ajax({type:"POST", url:"/api/admin/mail/template/add", cache:false
			, data : $("#templateForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					$("#templateForm")[0].reset();
					loadList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제한 데이터는 되돌릴 수 없습니다.\n삭제 하시겠습니까?')) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/admin/mail/template/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						$("#display-wrapper").addClass("hidden");
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-view', function(){
		var idx = $(this).data("idx");
		$("#display-wrapper").removeClass("hidden");
		$("#display").attr('src', '/admin/mailer/template/'+idx+'/view');
	});
});
</script>