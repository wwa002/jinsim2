<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>메일발송</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>메일</li>
			<li class="active"><strong>메일발송</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>메일 발송</h5>
				</div>
				<div class="ibox-content">
					<form id="sendForm" class="form-horizontal" enctype="multipart/form-data">
						<div class="form-group">
							<div class="col-sm-12">
								<select name="template" id="template" class="form-control">
									<option value="">템플릿 선택</option>
									<c:forEach items="${templates}" var="t">
										<option value="${t.idx}">${t.title}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div id="content-form">
							<div class="form-group">
								<div class="col-sm-12">
									<input type="text" name="title" id="title" class="form-control" placeholder="제목" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12">
									<textarea name="content" id="content" class="hidden"></textarea>
									<div id="summernote"></div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<div class="i-checks pull-left margin-right-20">
									<label>
										<input type="radio" value="P" name="target" class="target" checked="checked" /> <i></i> 단일 
									</label>
								</div>
								<c:if test="${fn:length(contacts) > 0}">
									<div class="i-checks pull-left margin-right-20">
										<label>
											<input type="radio" value="C" name="target" class="target" /> <i></i> 연락처 
										</label>
									</div>
								</c:if>
							</div>
						</div>
						<div class="form-group" id="target-p">
							<div class="col-sm-12">
								<input type="text" name="email" id="email" class="form-control" placeholder="이메일 주소" />
							</div>
						</div>
						<div class="form-group hidden" id="target-c">
							<div class="col-sm-12">
								<select name="contact" id="contact" class="form-control">
									<option value="">연락처선택</option>
									<c:forEach items="${contacts}" var="c">
										<option value="${c.idx}">${c.title}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<hr />
						<div class="form-group">
							<label class="col-sm-3 control-label">광고여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="send_type" checked="checked" /> <i></i> 일반메일 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="M" name="send_type" /> <i></i> 광고메일 
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">예약발송</label>
							<div class="col-sm-4">
								<input type="text" name="senddate" id="senddate" placeholder="발송날짜" class="form-control datepicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="sendtime" id="sendtime" placeholder="발송시간" class="form-control clockpicker" />
							</div>
						</div>
						
						<hr />
						
						<div class="text-right">
							<button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> 발송</button>
						</div>
						
						<hr />
						
						<div>
							수신거부 URL : <span id="refuse-url"></span>
						</div>
						
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>미리보기</h5>
					<div class="ibox-tools">
						<button type="button" class="btn btn-link btn-display"><i class="fa fa-refresh"></i></button>
					</div>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<iframe id="display" name="display" style="width:100%; height:600px; border:none;"></iframe>
							<form id="displayForm" action="/admin/mailer/send/view" target="display" class="hidden" method="post">
								<textarea name="content" id="display-content"></textarea>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script type="text/javascript">
function loadRefuseUrl() {
	var refuseUrl = 'http://'+document.location.hostname+'/refuse/email';
	gapi.client.setApiKey('AIzaSyDPvKF4YJBueGygxrBPOtFfgSuT2dWqjQ8');
	gapi.client.load('urlshortener', 'v1',function(){
		if (refuseUrl != "") {
			var request = gapi.client.urlshortener.url.insert({
		        'resource' : {
		            'longUrl' : refuseUrl
		        }
		    });
		    request.execute(function(response) {
		        if (response.id != null) {        
		            console.log(response.id);
		            shortUrl = response.id;
		            $("#refuse-url").html('<a href="'+response.id+'" target="_blank">'+response.id+'</a>');
		        } else {
		            alert("error: creating short url");
		        }
		    });
		}
	});
}

function loadDisplay() {
	$('#display-content').val($('#summernote').summernote('code'));
	$('#displayForm').submit();
}

$(function(){
	$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	
	$('.target').on('ifChecked', function(event){
		var value = $(this).val();
		switch (value) {
		case 'P':
			$("#target-p").removeClass('hidden');
			$("#target-c").addClass('hidden');
			break;
		case 'C':
			$("#target-p").addClass('hidden');
			$("#target-c").removeClass('hidden');
			break;
		}
	});
	
	$('.btn-display').click(function(){
		loadDisplay();
	});
	
	$("#template").change(function(){
		if ($(this).val() != "") {
			$.ajax({
				type:"GET"
				, url : "/api/admin/mail/template/item"
				, cache:false
				, data : {idx:$("#template").val()}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						$('#title').val(data.data.title);
						$('#summernote').summernote('code', data.data.content);
						loadDisplay();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
			
			
		} 
	});
	
	$("#sendForm").submit(function(){
		$("#content").val($('#summernote').summernote('code'));
		
		$.ajax({
			type:"POST"
			, url : "/api/admin/mail/send"
			, cache:false
			, data : $("#sendForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('등록되었습니다.');
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
});
</script>
<script src="https://apis.google.com/js/client.js?onload=loadRefuseUrl"></script>