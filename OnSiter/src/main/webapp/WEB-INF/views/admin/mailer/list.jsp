<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>발송내역</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>메일</li>
			<li class="active"><strong>발송내역</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>발송 내역</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" id="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-sm-1" rowspan="2">번호</th>
									<th class="text-center col-sm-*" rowspan="2">제목</th>
									<th class="text-center col-sm-4" colspan="6">상태</th>
									<th class="text-center col-sm-2" rowspan="2">발송일</th>
									<th class="text-center col-sm-2" rowspan="2">등록일</th>
									<th class="text-center col-sm-1" rowspan="2">관리</th>
								</tr>
								<tr>
									<th class="text-center">전체</th>
									<th class="text-center">성공</th>
									<th class="text-center">실패</th>
									<th class="text-center">거절</th>
									<th class="text-center">에러</th>
									<th class="text-center">조회</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center"><a href="/admin/mailer/item/{{idx}}" target="_blank">{{title}}</a></td>
	<td class="text-center">{{send_total}}</td>
	<td class="text-center">{{send_success}}</td>
	<td class="text-center">{{send_failure}}</td>
	<td class="text-center">{{send_refuse}}</td>
	<td class="text-center">{{send_error}}</td>
	<td class="text-center">{{read_count}}</td>
	<td class="text-center">{{senddate}}</td>
	<td class="text-center">{{regdate}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	var keyword = $("#keyword").val();
	$.ajax({type:"GET", url:"/api/admin/mail/send/list", cache:false
		, data : {page:page, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, idx:l.idx, title:l.title == '' ? l.content : l.title
						, senddate:getDefaultDataFormat(l.send_date)
						, send_total:l.send_total, send_success:l.send_success, send_failure:l.send_failure, send_refuse:l.send_refuse, send_error:l.send_error, read_count:l.read_count
						, regdate:getDefaultDataFormat(l.regdate)
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadList(1);
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	$("#btn-search").click(function(){
		loadList(1);
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제한 데이터는 되돌릴 수 없습니다.\n삭제 하시겠습니까?')) {
			var idx = $(this).data("idx");
			$.ajax({
				type:"POST"
				, url : "/api/admin/mail/delete"
				, cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
});
</script>