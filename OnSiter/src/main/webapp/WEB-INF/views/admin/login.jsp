<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>온사이터 - 관리자 영역</title>
<link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
<link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
<link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
	<div class="loginColumns animated fadeInDown">
		<div class="row">

			<div class="col-md-6">
				<h2 class="font-bold">관리자 영역입니다.</h2>

				<hr />
				<p>이곳은 관리 영역입니다.</p>
				<p>온사이터의 관리자이실경우 아이디와 비밀번호로 로그인 해주세요.</p>
				<p>
					문의사항은 <a href="mailto:jinka@jinsimsoft.co.kr">jinka@jinsimsoft.co.kr</a>으로 연락해주세요.
				</p>
			</div>
			<div class="col-md-6">
				<div class="ibox-content">
					<form class="m-t" id="loginForm" role="form" method="post" onsubmit="return false;">
						<div class="form-group">
							<input type="text" name="id" class="form-control" placeholder="ID" required="required" />
						</div>
						<div class="form-group">
							<input type="password" name="passwd" class="form-control" placeholder="Password" required="required" />
						</div>
						<button type="submit" class="btn btn-primary block full-width m-b">Login</button>

						<small>Contact : <a href="mailto:jinka@jinsimsoft.co.kr">jinka@jinsimsoft.co.kr</a></small>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
<script type="text/javascript">
$(function(){
	$('#loginForm').submit(function(){
		$.ajax({
			type:"POST"
			, url : "/api/login"
			, cache:false
			, data : $("#loginForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					document.location.href="/admin/dashboard";
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	return false;
});
</script>
