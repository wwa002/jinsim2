<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>SMS 발송 및 내역</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>SMS</li>
			<li class="active"><strong>발송 및 내역</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>발송 내역</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" id="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-sm-1" rowspan="2">번호</th>
									<th class="text-center col-sm-*" rowspan="2">제목</th>
									<th class="text-center col-sm-4" colspan="5">상태</th>
									<th class="text-center col-sm-2" rowspan="2">발송일</th>
									<th class="text-center col-sm-2" rowspan="2">등록일</th>
									<th class="text-center col-sm-1" rowspan="2">관리</th>
								</tr>
								<tr>
									<th class="text-center">전체</th>
									<th class="text-center">성공</th>
									<th class="text-center">실패</th>
									<th class="text-center">거절</th>
									<th class="text-center">에러</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>SMS 발송 <small id="sms-count"></small></h5>
				</div>
				<div class="ibox-content">
					<form id="sendForm" class="form-horizontal" enctype="multipart/form-data">
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="title" id="title" class="form-control" placeholder="제목" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<textarea rows="5" name="content" id="content" class="form-control" placeholder="내용"></textarea>
							</div>
							<div class="col-sm-12 text-right" id="content-size">
								0 SMS
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<div class="i-checks pull-left margin-right-20">
									<label>
										<input type="radio" value="P" name="target" class="target" checked="checked" /> <i></i> 단일 
									</label>
								</div>
								<c:if test="${fn:length(contacts) > 0}">
									<div class="i-checks pull-left margin-right-20">
										<label>
											<input type="radio" value="C" name="target" class="target" /> <i></i> 연락처 
										</label>
									</div>
								</c:if>
							</div>
						</div>
						<div class="form-group" id="target-p">
							<div class="col-sm-12">
								<input type="text" name="mobile" id="mobile" maxlength="13" class="form-control" placeholder="휴대폰 번호 (- 포함)" />
							</div>
						</div>
						<div class="form-group hidden" id="target-c">
							<div class="col-sm-12">
								<select name="contact" id="contact" class="form-control">
									<option value="">연락처선택</option>
									<c:forEach items="${contacts}" var="c">
										<option value="${c.idx}">${c.title}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<hr />
						<div class="form-group">
							<label class="col-sm-3 control-label">광고여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="send_type" checked="checked" /> <i></i> 일반문자 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="M" name="send_type" /> <i></i> 광고문자 
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">예약발송</label>
							<div class="col-sm-4">
								<input type="text" name="senddate" id="senddate" placeholder="발송날짜" class="form-control datepicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="sendtime" id="sendtime" placeholder="발송시간" class="form-control clockpicker" />
							</div>
						</div>
						
						<hr />
						
						<div class="text-right">
							<button type="submit" class="btn btn-primary"><i class="fa fa-send"></i> 발송</button>
						</div>
						
						<hr />
						
						<div>
							수신거부 URL : <span id="refuse-url"></span>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{title}}</td>
	<td class="text-center">{{send_total}}</td>
	<td class="text-center">{{send_success}}</td>
	<td class="text-center">{{send_failure}}</td>
	<td class="text-center">{{send_refuse}}</td>
	<td class="text-center">{{send_error}}</td>
	<td class="text-center">{{senddate}}</td>
	<td class="text-center">{{regdate}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
var smsType = "${smsCfg.type}";

function loadCafe24Count() {
	$.ajax({
		type:"GET"
		, url : "/api/admin/sms/cafe24/count"
		, cache:false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var sms = parseInt(data.data);
				var lms = sms == 0 ? 0 : Math.round(sms/3);
				$("#sms-count").text('(Cafe24 SMS : '+sms+'건, LMS : '+lms+'건 발송가능)');
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadRefuseUrl() {
	var refuseUrl = 'http://'+document.location.hostname+'/refuse/sms';
	gapi.client.setApiKey('AIzaSyDPvKF4YJBueGygxrBPOtFfgSuT2dWqjQ8');
	gapi.client.load('urlshortener', 'v1',function(){
		if (refuseUrl != "") {
			var request = gapi.client.urlshortener.url.insert({
		        'resource' : {
		            'longUrl' : refuseUrl
		        }
		    });
		    request.execute(function(response) {
		        if (response.id != null) {        
		            console.log(response.id);
		            shortUrl = response.id;
		            $("#refuse-url").html('<a href="'+response.id+'" target="_blank">'+response.id+'</a>');
		        } else {
		            alert("error: creating short url");
		        }
		    });
		}
	});
}

function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	var keyword = $("#keyword").val();
	$.ajax({type:"GET", url:"/api/admin/sms/send/list", cache:false
		, data : {page:page, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, idx:l.idx, title:l.title == '' ? l.content : l.title
						, senddate:getDefaultDataFormat(l.send_date)
						, send_total:l.send_total, send_success:l.send_success, send_failure:l.send_failure, send_refuse:l.send_refuse, send_error:l.send_error
						, regdate:getDefaultDataFormat(l.regdate)
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadCafe24Count();
	loadList(1);
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	$("#btn-search").click(function(){
		loadList(1);
	});
	
	$("#content").keyup(function(){
		var content = $(this).val();
		$("#content-size").text(content.length + (content.length > 45 ? ' LMS' : ' SMS'));
	});
	
	$('.target').on('ifChecked', function(event){
		var value = $(this).val();
		switch (value) {
		case 'P':
			$("#target-p").removeClass('hidden');
			$("#target-c").addClass('hidden');
			break;
		case 'C':
			$("#target-p").addClass('hidden');
			$("#target-c").removeClass('hidden');
			break;
		}
	});
	
	$("#sendForm").submit(function(){
		$.ajax({
			type:"POST"
			, url : "/api/admin/sms/send"
			, cache:false
			, data : $("#sendForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('등록되었습니다.');
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제한 데이터는 되돌릴 수 없습니다.\n삭제 하시겠습니까?')) {
			var idx = $(this).data("idx");
			$.ajax({
				type:"POST"
				, url : "/api/admin/sms/delete"
				, cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
});
</script>
<script src="https://apis.google.com/js/client.js?onload=loadRefuseUrl"></script>