<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>회원</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li class="active"><strong>회원</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-9">
			<div class="ibox">
				<div class="ibox-title">
					<h5>회원 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-2">
										<select id="type" class="form-control">
											<option value="">회원종류</option>
											<option value="ADMIN" <c:if test="${param.type eq 'ADMIN' }"> selected="selected"</c:if>>관리자</option>
											<option value="STEP" <c:if test="${param.type eq 'STEP' }"> selected="selected"</c:if>>운영자</option>
											<!-- 
											<option value="PRINCIPAL" <c:if test="${param.type eq 'PRINCIPAL' }"> selected="selected"</c:if>>좌장</option>
											<option value="SPEAKER" <c:if test="${param.type eq 'SPEAKER' }"> selected="selected"</c:if>>강연자</option>
											-->
										</select>
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input type="text" id="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">이름</th>
									<th class="text-center hidden">회원종류</th>
									<th class="text-center">등록일</th>
									<th class="text-center">접속일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="ibox" id="member-form-add">
				<div class="ibox-title">
					<h5>회원추가</h5>
				</div>
				<div class="ibox-content">
					<form id="memberForm" class="form-horizontal" enctype="multipart/form-data" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">아이디</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" name="id" id="id" maxlength="10" class="form-control" />
									<span class="input-group-btn">
										<button type="button" id="id-check" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호</label>
							<div class="col-sm-8">
								<input type="password" name="passwd" class="form-control" />
							</div>
						</div>
						<div class="form-group hidden">
							<label class="col-sm-4 control-label">회원타입</label>
							<div class="col-sm-8">
								<select name="type" class="form-control">
									<option value="ADMIN">관리자</option>
									<option value="STEP">운영자</option>
									<!-- 
									<option value="PRINCIPAL">좌장</option>
									<option value="SPEAKER">강연자</option>
									 -->
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이름</label>
							<div class="col-sm-8">
								<input type="text" name="name" placeholder="이름" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="isuse" checked="checked"> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="isuse"> <i></i> 차단
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="member-save" class="btn btn-primary">회원등록</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox" id="member-form-modify" style="display:none;">
				<div class="ibox-title">
					<h5>회원수정</h5>
				</div>
				<div class="ibox-content">
					<form id="memberModifyForm" class="form-horizontal" enctype="multipart/form-data" onsubmit="return false;">
						<input type="hidden" name="idx" id="idx-modify" />
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호</label>
							<div class="col-sm-8">
								<input type="password" name="passwd" class="form-control" />
							</div>
						</div>
						<div class="form-group hidden">
							<label class="col-sm-4 control-label">회원타입</label>
							<div class="col-sm-8">
								<select name="type" id="type-modify" class="form-control">
									<option value="ADMIN">관리자</option>
									<option value="STEP">운영자</option>
									<!-- 
									<option value="PRINCIPAL">좌장</option>
									<option value="SPEAKER">강연자</option>
									 -->
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이름</label>
							<div class="col-sm-8">
								<input type="text" name="name" id="name-modify" placeholder="성" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="isuse" id="isuse-y-modify"> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="isuse" id="isuse-n-modify"> <i></i> 차단
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="member-modify" class="btn btn-primary">정보수정</button>
								<button type="reset" id="member-modify-cancel" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{name}}</td>
	<td class="text-center hidden">{{{typeHtml}}}</td>
	<td class="text-center">{{regdate}}</td>
	<td class="text-center">{{logdate}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="{{idx}}" data-type="{{type}}" data-name="{{name}}" data-isuse="{{isuse}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	var type = $("#type").val();
	var gcd = $("#gcd").val();
	var keyword = $("#keyword").val();
	$.ajax({type:"GET", url:"/api/admin/member/list", cache:false
		, data : {page:page, type:type, gcd:gcd, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					switch (l.type) {
					case 'ADMIN':
						l.typeHtml = '<span class="label label-danger">관리자</span>';
						break;
					case 'STEP':
						l.typeHtml = '<span class="label label-info">운영자</span>';
						break;
					case 'PRINCIPAL':
						l.typeHtml = '<span class="label label-info">좌장</span>';
						break;
					case 'SPEAKER':
						l.typeHtml = '<span class="label label-warning">강연자</span>';
						break;
					}
					
					l.no = virtualNo--;
					l.regdate=getDefaultDataFormat(l.regdate);
					l.logdate=getDefaultDataFormat(l.logdate);
					$list.append(Mustache.render(listTemplate, l));
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	loadList(1);
	$("#btn-search").click(function(){
		loadList(1);
	});
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	
	$("#id-check").click(function(){
		var id = $("#id").val();
		$.ajax({type:"POST", url : "/api/admin/member/check/id", cache:false
			, data : {id:id}
			, success : function(data) {
				if (data.error == 0) {
					alert("사용할 수 있는 아이디 입니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#member-save").click(function(){
		$("#memberForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/member/save"
			, enctype:"multipart/form-data"
			, success:function(data){
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				alert(data.responseJSON.error);
			}
		});
		$("#memberForm").submit();
	});
	
	$("#member-modify").click(function(){
		$("#memberModifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/member/save"
			, enctype:"multipart/form-data"
			, success:function(data){
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				alert(data.responseJSON.error);
			}
		});
		$("#memberModifyForm").submit();
	});
	
	$(document).on('click', '.btn-modify', function(){
		$("#idx-modify").val($(this).data("idx"));
		$("#type-modify").val($(this).data("type"));
		$("#name-modify").val($(this).data("name"));
		if ($(this).data("isuse") == "Y") {
			$("#isuse-y-modify").iCheck("check");
		} else {
			$("#isuse-n-modify").iCheck("check");
		}
		
		$("#member-form-add").hide();
		$("#member-form-modify").show();
	});
	
	$("#member-modify-cancel").click(function(){
		$("#member-form-add").show();
		$("#member-form-modify").hide();
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm("선택하신 회원을 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/admin/member/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
});


</script>

