<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />
<c:set var="admin" value="${sessionScope.MEMBER_SESSION }" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>온사이터 관리자 페이지</title>
	<link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/resources/plugins/jstree/themes/default/style.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
	
	<!-- Summernote -->
	<link href="/resources/plugins/summernote/summernote.css" rel="stylesheet">
	
	<!-- Dropzone -->
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/basic.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/dropzone.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/plugins/codemirror/ambiance.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
	<link href="/resources/css/admin.css" rel="stylesheet">
</head>

<body>
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav metismenu" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element">
							<a href="#">
								<span class="clear">
									<span class="m-t-xs"><strong class="font-bold">${admin.name}</strong></span> 
									<small class="text-muted text-xs">${admin.type}</small>
							</span>
							</a>
						</div>
						<div class="logo-element">${admin.name}</div>
					</li>
					<li class="<c:if test="${path eq '/admin/dashboard'}">active</c:if>">
						<a href="/admin/dashboard"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboards</span></a>
					</li>
					
					<li class="<c:if test="${fn:startsWith(path, '/admin/resources') or fn:startsWith(path, '/admin/etc')}">active</c:if>">
						<a href="#"><i class="fa fa-laptop"></i> <span class="nav-label">화면관리</span><span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li class="<c:if test="${fn:startsWith(path, '/admin/resources')}">active</c:if>">
								<a href="/admin/resources"><i class="fa fa-cloud-upload"></i> <span class="nav-label">자원</span></a>
							</li>
							<li class="<c:if test="${fn:startsWith(path, '/admin/etc')}">active</c:if>">
								<a href="/admin/etc"><i class="fa fa-file-text"></i> <span class="nav-label">기타</span></a>
							</li>
						</ul>
					</li>
					
					<li class="<c:if test="${fn:startsWith(path, '/admin/member')}">active</c:if>">
						<a href="/admin/member"><i class="fa fa-users"></i> <span class="nav-label">회원</span></a>
					</li>
					
					<li class="<c:if test="${fn:startsWith(path, '/admin/event')}">active</c:if>">
						<a href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label">행사관리</span> <span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li class="<c:if test="${fn:startsWith(path, '/admin/event/list')}">active</c:if>">
								<a href="/admin/event/list"><i class="fa fa-graduation-cap"></i> <span class="nav-label">관리</span></a>
							</li>
							<c:forEach items="${menuEvent}" var="e">
								<c:set var="b_url" value="/admin/event/${e.code}" />
								<li class="<c:if test="${fn:startsWith(path, b_url)}">active</c:if>">
									<a href="/admin/event/${e.code}"><i class="fa fa-edit"></i> ${e.title}</a>
								</li>
							</c:forEach>
						</ul>
					</li>
					<!-- 
					<li class="<c:if test="${fn:startsWith(path, '/admin/sms')}">active</c:if>">
						<a href="#"><i class="fa fa-mobile"></i> <span class="nav-label">SMS</span> <span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li class="<c:if test="${fn:startsWith(path, '/admin/sms/list')}">active</c:if>">
								<a href="/admin/sms/list"><i class="fa fa-send"></i> 발송 및 내역</a>
							</li>
							<li class="<c:if test="${fn:startsWith(path, '/admin/sms/contacts')}">active</c:if>">
								<a href="/admin/sms/contacts"><i class="fa fa-users"></i> 연락처</a>
							</li>
							<li class="<c:if test="${fn:startsWith(path, '/admin/sms/refuse')}">active</c:if>">
								<a href="/admin/sms/refuse"><i class="fa fa-warning"></i> 수신거부</a>
							</li>
						</ul>
					</li>
					 -->
					<!-- 
					<li class="<c:if test="${fn:startsWith(path, '/admin/mailer')}">active</c:if>">
						<a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">메일</span> <span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/list')}">active</c:if>">
								<a href="/admin/mailer/list"><i class="fa fa-list"></i> 발송내역</a>
							</li>
							<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/send')}">active</c:if>">
								<a href="/admin/mailer/send"><i class="fa fa-send"></i> 메일발송</a>
							</li>
							<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/template')}">active</c:if>">
								<a href="/admin/mailer/template"><i class="fa fa-crop"></i> 템플릿</a>
							</li>
							<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/contacts')}">active</c:if>">
								<a href="/admin/mailer/contacts"><i class="fa fa-users"></i> 연락처</a>
							</li>
							<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/refuse')}">active</c:if>">
								<a href="/admin/mailer/refuse"><i class="fa fa-warning"></i> 수신거부</a>
							</li>
						</ul>
					</li>
					 -->
					<li class="<c:if test="${fn:startsWith(path, '/admin/stats')}">active</c:if>">
						<a href="/admin/stats"><i class="fa fa-tasks"></i> <span class="nav-label">접속통계</span></a>
					</li>
				</ul>
			</div>
		</nav>

		<div id="page-wrapper" class="gray-bg">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation"
					style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
							href="#"><i class="fa fa-bars"></i> </a>
					</div>
					<ul class="nav navbar-top-links navbar-right">
						<li>
							<a href="/"> <i class="fa fa-home"></i> Home</a>
						</li>
						<li>
							<a href="/logout"> <i class="fa fa-sign-out"></i> Log-out</a>
						</li>
					</ul>
				</nav>
			</div>