<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />
<c:set var="admin" value="${sessionScope.MEMBER_SESSION }" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>온사이터 관리자 페이지</title>
	<link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/resources/plugins/jstree/themes/default/style.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
	
	<!-- Summernote -->
	<link href="/resources/plugins/summernote/summernote.css" rel="stylesheet">
	
	<!-- Dropzone -->
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/basic.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/dropzone.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/plugins/codemirror/ambiance.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
	<link href="/resources/css/admin.css" rel="stylesheet">
</head>

<body class="gray-bg">
	<div id="wrapper">
		