<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>온사이터 - 이메일 수신거부</title>
    <link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<h3>이메일 수신거부</h3>
			<p>
				수신거부하실 이메일 주소를 입력후 저장해 주세요.
			</p>
			<form id="refuse" class="m-t" role="form">
				<div class="form-group">
					<input type="email" name="email" class="form-control" placeholder="이메일" required="">
				</div>
				<button type="submit" class="btn btn-primary block full-width m-b"><i class="fa fa-save"></i> 저장</button>
			</form>
			<p class="m-t">
				<small>ONSITER &copy;</small>
			</p>
		</div>
	</div>
</body>
</html>
<!-- Mainly scripts -->
<script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
<script src="/resources/bootstrap/inspinia/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
	$("#refuse").submit(function(){
		$.ajax({
			type:"POST"
			, url : "/api/refuse/email"
			, cache:false
			, data : $("#refuse").serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('등록되었습니다.');
					document.location.href='/';
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		
		return false;
	});
});
</script>
