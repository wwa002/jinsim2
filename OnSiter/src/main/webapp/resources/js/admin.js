function goPage(page, params) {
	if (page == 0) {
		return;
	}
	location.href="?page="+page+params;
}

function getDefaultDataFormat(str) {
	if (!str) {
		return '';
	}
	switch (str.length) {
	case 8:
		var year = str.substring(0, 4);
		var month = str.substring(4, 6);
		var day = str.substring(6, 8);
		return year + '-' + month + '-' + day;
	case 14:
		var year = str.substring(0, 4);
		var month = str.substring(4, 6);
		var day = str.substring(6, 8);
		var hour = str.substring(8, 10);
		var minute = str.substring(10, 12);
		var second = str.substring(12, 14);
		return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
	default:
		return '';
	}
}

function getDateFromFormat(str) {
	return new Date(str.replace(
		    /^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/,
		    '$4:$5:$6 $2/$3/$1'
		));
}

function getDateStr(date) {
	var mm = date.getMonth() + 1; // getMonth() is zero-based
	var dd = date.getDate();
	return [ date.getFullYear(), '-', (mm > 9 ? '' : '0') + mm, '-', (dd > 9 ? '' : '0') + dd ].join('');
}

function convertDateString(str) {
	return getDateStr(getDateFromFormat(str));
}

function formatDate(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = ampm + ' ' + hours + ':' + minutes;
	return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + strTime;
}

function formatSizeUnits(bytes) {
    if ( ( bytes >> 30 ) & 0x3FF ) {
//        bytes = ( bytes >>> 30 ) + '.' + ( bytes & (3*0x3FF )) + 'GB' ;
        bytes = ( bytes >>> 30 ) + 'GB' ;
    } else if ( ( bytes >> 20 ) & 0x3FF ) {
//        bytes = ( bytes >>> 20 ) + '.' + ( bytes & (2*0x3FF ) ) + 'MB' ;
        bytes = ( bytes >>> 20 ) + 'MB' ;
    } else if ( ( bytes >> 10 ) & 0x3FF ) {
//        bytes = ( bytes >>> 10 ) + '.' + ( bytes & (0x3FF ) ) + 'KB' ;
        bytes = ( bytes >>> 10 ) + 'KB' ;
    } else if ( ( bytes >> 1 ) & 0x3FF ) {
        bytes = ( bytes >>> 1 ) + 'Bytes' ;
    } else {
        bytes = bytes + 'Byte' ;
    }
    return bytes ;
}

function nl2br(str){  
	return str.replace(/\n/g, "<br />");  
}

function getSearchEngine(referer) {
	var engine = ["naver", "nate", "daum", "yahoo", "google"];
	for (var i = 0; i < engine.length; i++) {
		if (referer.includes(engine[i])) {
			return engine[i];
		}
	}
	return referer;
}

function getBrowser(agent) {
	var browser = ["MSIE 11","MSIE 10","MSIE 9","MSIE 8","MSIE 7","MSIE 6","Firefox","Opera","Chrome","Safari"];
	for (var i = 0; i < browser.length; i++) {
		if (agent.includes(browser[i])) {
			return browser[i];
		}
	}
	return '';
}

function getSearchKeyword(referer) {
	if (!referer) {
		return '';
	}
	
	var urlexp = referer.split('?');
	if (urlexp.length < 2) {
		return '';
	}
	if (!urlexp[1]) {
		return '';
	}
	var queexp = urlexp[1].split('&');
	for (var i = 0; i < queexp.length; i++) {
		var varexp = queexp[i].split('=');
		if (!varexp || varexp.length < 2) {
			continue;
		}
		if (varexp[0] == 'query' || varexp[0]=='q' || varexp['p']) {
			return decodeURIComponent(varexp[1]);
		}
	}
	return '';
}

function getByteLength(s){
    for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
    return b;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function sendSummernoteFile(file, el) {
	console.log(file);
	var form = new FormData();
	form.append('file', file);
	$.ajax({
		type: "POST", url: "/api/summernote/upload", data: form, cache: false, contentType: false, processData: false, dataType: 'json',
		success: function(data) {
			$(el).summernote('editor.insertImage', data.data);                 
		},
		error : function(error) {
			alert('error');
		},
		complete : function(response) {
		}
    });
}

$(function(){
	$('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
	
	$(".datepicker").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
	});
	$('.clockpicker').clockpicker({
	    placement: 'top',
	    align: 'right',
	    autoclose:true
	});
});