package kr.or.sleepnet.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

public class MainInterceptor extends BaseInterceptor {
	
	public static Logger logger = Logger.getLogger(MainInterceptor.class);

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception)
			throws Exception {
		long startTime = (Long) request.getAttribute(SITESTART);
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		
//		logger.info("RequestURL :: " + request.getRequestURL().toString() + " :: End - " + endTime);
//		logger.info("RequestURL :: " + request.getRequestURL().toString() + " :: Taken - " + timeTaken + " ("+((float)timeTaken/1000)+"초)");
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		long startTime = System.currentTimeMillis();
//		logger.info("RequestURL :: " + request.getRequestURL().toString() + " :: Start - " + System.currentTimeMillis());
		request.setAttribute(SITESTART, startTime);
		
		return true;
	}

}