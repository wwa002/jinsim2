package kr.or.sleepnet.scheduler;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.or.sleepnet.service.ResourceService;
import kr.or.sleepnet.service.ScheduleService;

@Component
public class Scheduler {
	
	private static Logger logger = Logger.getLogger(Scheduler.class);

	@Autowired
	ScheduleService scheduleService;
	
	@Autowired
	ResourceService resourceService;
	
	/* Server Management Start */
	@Scheduled(fixedRate = 60000)
	private void serverStatus() throws Exception {
		scheduleService.serverStatus();
	}
	
	@Scheduled(fixedRate = 300000)
	private void electMainServer() throws Exception {
		scheduleService.electMainServer();
	}
	/* Server Management End */

	/* Server File Cache Remove Start */
	@Scheduled(cron="0 0 1 * * *")
	public void removeTemporaryFiles() throws Exception {
		try {
			scheduleService.removeTemporaryFiles();
		} catch (Exception e) {
			logger.error(e);
		}
	}
	/* Server File Cache Remove End */
}
