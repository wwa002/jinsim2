package kr.or.sleepnet.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.or.sleepnet.service.CommonService;
import kr.or.sleepnet.service.MainService;
import kr.or.sleepnet.utils.StringUtil;

@Controller
public class MainController {

	private static Logger logger = Logger.getLogger(MainController.class);
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	MainService mainService;
	
	@RequestMapping(value={"", "/"})
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		mainService.index(request, param, model);
		return "/index";
	}
	
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			HttpSession session = request.getSession();
			if (session != null) {
				session.invalidate();
			}
			return "redirect:/";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	/* Content Page Start */
	@RequestMapping("/introduce/{view}")
	public String introduce(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String view) throws Exception {
		try {
			model.addAttribute("menu", "introduce");
			model.addAttribute("submenu", view);
			commonService.accessAnalysis(request, param);
		} catch (Exception e) {
			logger.error(e);
		}
		return "/introduce/"+view;
	}
	
	@RequestMapping("/sleep/{view}")
	public String sleep(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String view) throws Exception {
		try {
			model.addAttribute("menu", "sleep");
			model.addAttribute("submenu", view);
			commonService.accessAnalysis(request, param);
		} catch (Exception e) {
			logger.error(e);
		}
		if (StringUtil.isEmpty(param.get("content"))) {
			return "/sleep/"+view;
		} else {
			return "/sleep/"+view+"/"+StringUtil.getContent(param.get("content"));
		}
	}
	
	/* Content Page End */
	
}
