package kr.or.sleepnet.session;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import kr.or.sleepnet.utils.StringUtil;

public class MemberSession implements Serializable, HttpSessionBindingListener {

	public static final String SESSION_KEY = "MEMBER_SESSION";

	String uuid;

	int idx = 0;
	String type;
	String id;
	
	Map<String, Object> data;
	
	public MemberSession() {
		uuid = UUID.randomUUID().toString();
	}
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		HttpSession session = SessionHolder.remove(this.uuid);
		this.uuid = uuid;
		SessionHolder.put(this.uuid, session);
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setData(Map<String, Object> item) {
		idx = Integer.parseInt(StringUtil.getContent(item.get("idx"), "0"));
		type = StringUtil.getContent(item.get("type"));
		id = StringUtil.getContent(item.get("id"));
		data = item;
	}
	
	public Map<String, Object> getData() {
		return data;
	}

	public boolean isLogin() {
		return !StringUtil.isEmpty(id);
	}

	public boolean isAdmin() {
		if (!isLogin()) {
			return false;
		}
		return "ADMIN".equals(type);
	}

	String log;
	String agent;
	
	public String getLog() {
		return log;
	}
	
	public void setLog(String log) {
		this.log = log;
	}
	
	public String getAgent() {
		return agent;
	}
	
	public void setAgent(String agent) {
		this.agent = agent;
	}
	
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		SessionHolder.put(uuid, event.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		SessionHolder.remove(uuid);
	}

}
