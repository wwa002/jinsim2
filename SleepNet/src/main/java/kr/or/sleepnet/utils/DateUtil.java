package kr.or.sleepnet.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author addios4u
 * Java내 시간 변환용 클래스
 */
public class DateUtil {

	public static int getUnixTimestamp() {
		return (int) (System.currentTimeMillis() / 1000L);
	}

	public static long toSystemTime(int date) {
		return (long) date * 1000L;
	}
	
	public static String getDateString() {
		return getDateString(null, "yyyyMMddHHmmss");
	}
	
	public static String getDateString(String format) {
		Date dt = new Date();
		SimpleDateFormat df = new SimpleDateFormat(format != null ? format : "yyyyMMddHHmmssSSS");
		return df.format(dt);
	}
	
	public static String getDateString(long date, String format) {
		Date dt = new Date(date);
		SimpleDateFormat df = new SimpleDateFormat(format != null ? format : "yyyyMMddHHmmssSSS");
		return df.format(dt);
	}

	public static String getDateString(Date dt, String format) {
		SimpleDateFormat df = new SimpleDateFormat(format != null ? format : "yyyyMMddHHmmssSSS");
		Date date = dt != null ? dt : new Date();
		return df.format(date);
	}

	public static String getDateString(String dt, String oriFormat, String destFormat) throws ParseException {
		SimpleDateFormat oriDF = new SimpleDateFormat(oriFormat);
		SimpleDateFormat destDF = new SimpleDateFormat(destFormat);
		return destDF.format(oriDF.parse(dt));
	}
	
	public static Date getDateFromString(String dt, String format) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat(format != null ? format : "yyyyMMddHHmmssSSS");
		return df.parse(dt);
	}
	
	public static String getDateOrTime(String dt, String dateFormat) throws ParseException {
		SimpleDateFormat oriDf = new SimpleDateFormat(dateFormat);
		SimpleDateFormat todayFormat = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat hourFormat = new SimpleDateFormat("a hh:mm");
		SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy.MM.dd");
		
		Date date = oriDf.parse(dt);
		Date cDate = new Date();
		if (todayFormat.format(date).equals(todayFormat.format(cDate))) {
			return hourFormat.format(date);
		} else {
			return dayFormat.format(date);
		}
	}
	
}
