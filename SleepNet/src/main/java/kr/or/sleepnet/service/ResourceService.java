package kr.or.sleepnet.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.util.IOUtils;

import kr.or.sleepnet.mapper.APIMapper;
import kr.or.sleepnet.mapper.MemberMapper;
import kr.or.sleepnet.session.MemberSession;
import kr.or.sleepnet.utils.DateUtil;
import kr.or.sleepnet.utils.EncryptUtils;
import kr.or.sleepnet.utils.StringUtil;
import net.coobird.thumbnailator.Thumbnails;

@Service
public class ResourceService extends BaseService {
	
	private static Logger logger = Logger.getLogger(ResourceService.class);
	
	@Autowired
	JavaMailSender mailSender;
	
	@Value("${aws.accessKey}")
	private String awsAccessKey;
	
	@Value("${aws.secretKey}")
	private String awsSecretKey;
	
	@Value("${aws.bucket}")
	private String awsBucket;
	
	@Value("${aws.s3endpoint}")
	private String awsS3EndPoint;
	
	@Autowired
	APIMapper apiMapper;
	
	@Autowired
	RestTemplate restTemplete;
	
	@Autowired
	MemberMapper memberMapper;
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession();
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				/*
				Map<String, Object> param = new HashMap<>();
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = memberMapper.loginForAuth(param);
				if (mem == null) {
					throw new AuthExpireException();
				}
				ms.setData(mem);
				mem.put("ip", request.getRemoteAddr());
				memberMapper.loginDateUpdate(mem);
				*/
			}
		}
		return ms;
	}
	
	private boolean isValidMime(Object mime) {
		return isValidMime(StringUtil.getContent(mime));
	}
	
	private boolean isValidMime(String mime) {
		if (StringUtil.isEmpty(mime)) {
			return false;
		}
		String m = mime.toLowerCase();
		if (m.startsWith("text") || m.startsWith("image")) {
			return true;
		} else if (m.equals("application/font-woff") || m.equals("application/font-woff2")) {
			return true;
		}
		return false;
	}
	
	private File getTemporaryFile(HttpServletRequest request, String path) throws Exception {
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator+DateUtil.getDateString("yyyyMMdd")+File.separator;
		File file = new File(tempPath+path);
		if (!file.exists()) {
			logger.debug("Download From S3 : " + path);
			S3Object s3obj = download(path);
			S3ObjectInputStream ois = s3obj.getObjectContent();
			
			Files.createDirectories(Paths.get(file.getParentFile().getAbsolutePath()));
			IOUtils.copy(ois, new FileOutputStream(file));
		}
		return file;
	}
	
	private File getThumbnail(File file, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("width")) && StringUtil.isEmpty(param.get("height"))) {
			return file;
		} else if (StringUtil.isEmpty(param.get("width"))) {
			return getThumbnail(file, 0, Integer.parseInt(StringUtil.getContent(param.get("height"))));
		} else if (StringUtil.isEmpty(param.get("height"))) {
			return getThumbnail(file, Integer.parseInt(StringUtil.getContent(param.get("width"))), 0);
		} else {
			return getThumbnail(file, Integer.parseInt(StringUtil.getContent(param.get("width"))), Integer.parseInt(StringUtil.getContent(param.get("height"))));
		}
	}
	
	private File getThumbnail(File file, int width, int height) throws Exception {
		String filename = file.getName().toLowerCase();
		String extension = filename.substring(filename.lastIndexOf(".")+1);
		if (extension.equals("png") || extension.equals("jpg")) {
			String folder = file.getParentFile().getAbsolutePath() + File.separator + "thumbnails";
			File dir = new File(folder);
			if (!dir.exists()) {
				Files.createDirectories(Paths.get(folder));
			}

			String destname = width+"x"+height+"_"+filename;
			File dest = new File(folder+File.separator+destname);
			if (dest.exists()) {
				return dest;
			}
			
			BufferedImage bufferOriginal = ImageIO.read(file);
			if (width == 0) {
				width = (bufferOriginal.getWidth() * height)/bufferOriginal.getHeight();
			} else if (height == 0) {
				height = (bufferOriginal.getHeight() * width)/bufferOriginal.getWidth();
			}

			Thumbnails.of(bufferOriginal).size(width, height).outputFormat(extension).toFile(dest);
			return dest;
		}
		
		return file;
	}
	
	public ResponseEntity<byte[]> imageDownload(String path, HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(path)) {
			throw new Exception("정보를 읽어올 수 없습니다.");
		}
		
		File file = getTemporaryFile(request, path);
		if (!StringUtil.isEmpty(param.get("width")) || !StringUtil.isEmpty(param.get("height"))) {
			file = getThumbnail(file, param);
		}
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (path.endsWith("png")) {
			headers.setContentType(MediaType.IMAGE_PNG);
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (path.endsWith("jpg")) {
			headers.setContentType(MediaType.IMAGE_JPEG);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (path.endsWith("gif")) {
			headers.setContentType(MediaType.IMAGE_GIF);
			response.setContentType(MediaType.IMAGE_GIF_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(StringUtil.getContent(param.get("name")), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> summernoteDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		String path = "summernote/" + StringUtil.getContent(param.get("name"));
		return imageDownload(path, request, response, param);
	}
	
	
	// for AWS
	private AWSCredentials getCredentials() {
		return new BasicAWSCredentials(awsAccessKey, awsSecretKey);
	}
	
	private AmazonS3Client getS3Client() {
		AmazonS3Client client = new AmazonS3Client(getCredentials());
		client.setEndpoint(awsS3EndPoint);
		return client;
	}
	
	public String getUrl(String path) {
		return "https://"+awsS3EndPoint+"/"+awsBucket+"/"+path;
	}
	
	private Map<String, Object> upload(HttpServletRequest request, MultipartFile file) throws Exception {
		if (file == null || file.isEmpty()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator;
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = "resource/" + destName;
	
		File convertFile = new File(tempPath+destName);
		try {
			file.transferTo(convertFile);
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, convertFile);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} finally {
			convertFile.delete();
		}
		
		return map;
	}
	
	public Map<String, Object> upload(HttpServletRequest request, String path, MultipartFile file) throws Exception {
		if (file == null || file.isEmpty()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator;
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		if ("file".equals(extension)) {
			extension = "png";
		}
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = path + "/" + destName;
	
		File convertFile = new File(tempPath+destName);
		try {
			file.transferTo(convertFile);
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, convertFile);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} finally {
			convertFile.delete();
		}
		
		return map;
	}
	
	public Map<String, Object> upload(HttpServletRequest request, String path, File file) throws Exception {
		if (file == null || !file.exists()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getName().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = path + "/" + destName;
	
		try {
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, file);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} 
		return map;
	}

	private S3Object download(String path) throws Exception {
		try {
            return getS3Client().getObject(new GetObjectRequest(awsBucket, path));
        } catch (AmazonServiceException ase) {
            logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
            logger.info("Error Message:    " + ase.getMessage());
            logger.info("HTTP Status Code: " + ase.getStatusCode());
            logger.info("AWS Error Code:   " + ase.getErrorCode());
            logger.info("Error Type:       " + ase.getErrorType());
            logger.info("Request ID:       " + ase.getRequestId());
            throw ase;
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to communicate with S3, such as not being able to access the network.");
            logger.info("Error Message: " + ace.getMessage());
            throw ace;
        }
	}
	
	private void move(String path, String destPath) throws Exception{
		try {
            getS3Client().copyObject(new CopyObjectRequest(awsBucket, path, awsBucket, destPath));
            getS3Client().deleteObject(new DeleteObjectRequest(awsBucket, path));
        } catch (AmazonServiceException ase) {
        	logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
        	logger.info("Error Message:    " + ase.getMessage());
        	logger.info("HTTP Status Code: " + ase.getStatusCode());
        	logger.info("AWS Error Code:   " + ase.getErrorCode());
        	logger.info("Error Type:       " + ase.getErrorType());
        	logger.info("Request ID:       " + ase.getRequestId());
        	throw ase;
        } catch (AmazonClientException ace) {
        	logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to  communicate with S3, such as not being able to access the network.");
        	logger.info("Error Message: " + ace.getMessage());
        	throw ace;
        } catch (Exception e) {
			throw e;
		}
	}
	
	public void delete(String path) throws Exception {
		try {
			getS3Client().deleteObject(new DeleteObjectRequest(awsBucket, path));
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to communicate with S3, such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public boolean sendMail(String[] to, String from, String subject, String content) throws Exception {
		Destination destination = new Destination().withToAddresses(to);
		
		Content sesSubject = new Content().withData(subject);
		Content sesContent = new Content().withData(content);
		Body body = new Body().withText(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
	
	public boolean sendMailHtml(String[] to, String from, String subject, String content) throws Exception {
		Destination destination = new Destination().withToAddresses(to);
		
		Content sesSubject = new Content().withData(subject);
		Content sesContent = new Content().withData(content);
		Body body = new Body().withHtml(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
	
	public boolean sendFromMailer(Map<String, Object> param) throws Exception {
		Destination destination = new Destination().withToAddresses(StringUtil.getContent(param.get("email")));
		
		Content sesSubject = new Content().withData(StringUtil.getContent(param.get("title")));
		Content sesContent = new Content().withData(StringUtil.getContent(param.get("content")));
		Body body = new Body().withHtml(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(StringUtil.getContent(param.get("from"))).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	public void sendSMS(Map<String, Object> cfg, Map<String, Object> data) throws Exception {
		String content = StringUtil.getContent(data.get("content"));
		String[] sender = StringUtil.getContent(cfg.get("sender")).split("-");
		
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("user_id", EncryptUtils.base64Encode(StringUtil.getContent(cfg.get("auth1")))); // 아이디
		parameters.add("secure", EncryptUtils.base64Encode(StringUtil.getContent(cfg.get("auth2")))); // 인증키
		parameters.add("sphone1", EncryptUtils.base64Encode(sender[0])); // 보내는 번호
		parameters.add("sphone2", EncryptUtils.base64Encode(sender[1])); // 보내는 번호
		parameters.add("sphone3", EncryptUtils.base64Encode(sender[2])); // 보내는 번호
		parameters.add("rphone", EncryptUtils.base64Encode(StringUtil.getContent(data.get("mobile")).replaceAll("-", ""))); // 받는번호
		parameters.add("msg", EncryptUtils.base64Encode(content)); // 메시지
		parameters.add("mode", EncryptUtils.base64Encode("1"));
		
		if (content.length() <= 45) {
			parameters.add("smsType", EncryptUtils.base64Encode("S"));
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
			String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
			if (!result.startsWith("success")) {
				throw new Exception("발송에 실패하였습니다.");
			}
		} else {
			parameters.add("smsType", EncryptUtils.base64Encode("L"));
			parameters.add("subject", EncryptUtils.base64Encode(StringUtil.getContent(data.get("title"))));
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
			String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
			if (!result.startsWith("success")) {
				throw new Exception("발송에 실패하였습니다.");
			}
		}
	}
	
	
	/*
	public void sendMail() throws Exception {
		String html = "<h1>Email Test</h1>";
		html += "<p>테스트 HTML 메세지";
		html += "<a href=\"http://www.naver.com\">네이버</a> 바로가기";
		html += "</p>";
		
		MimeMessage message = mailSender.createMimeMessage();
		message.setSubject("테스트 메일 제목");
		message.setText(html, "UTF-8", "html");
		message.setFrom(new InternetAddress("addios4u@dmon.kr", "나태호 (디몬)"));
		message.addRecipient(RecipientType.TO, new InternetAddress("addios4u@gmail.com", "나태호"));
		mailSender.send(message);
	}
	*/

}
