package kr.or.sleepnet.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import kr.or.sleepnet.mapper.APIMapper;
import kr.or.sleepnet.mapper.MemberMapper;
import kr.or.sleepnet.session.MemberSession;
import kr.or.sleepnet.utils.StringUtil;

@Service
public class MainService extends BaseService {
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	MemberMapper memberMapper;
	
	@Autowired
	APIMapper apiMapper;
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession();
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				/*
				Map<String, Object> param = new HashMap<>();
				param.put("auth", request.getHeader("auth"));

				Map<String, Object> mem = memberMapper.loginForAuth(param);
				if (mem == null) {
					throw new AuthExpireException();
				}
				ms.setData(mem);
				mem.put("ip", request.getRemoteAddr());
				memberMapper.loginDateUpdate(mem);
				*/
			}
		}
		return ms;
	}
	
	public void index(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		
		
		commonService.accessAnalysis(request, param);
	}

}
