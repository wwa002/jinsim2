package kr.or.sleepnet.service;

import javax.servlet.http.HttpServletRequest;

import kr.or.sleepnet.session.MemberSession;

public abstract class BaseService {

	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession session = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (session == null) {
			session = new MemberSession();
			request.getSession().setAttribute(MemberSession.SESSION_KEY, session);
		}
		return session;
	}
	
}
