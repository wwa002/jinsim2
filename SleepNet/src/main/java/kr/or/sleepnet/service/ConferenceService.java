package kr.or.sleepnet.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import kr.or.sleepnet.exceptions.AuthExpireException;
import kr.or.sleepnet.mapper.APIMapper;
import kr.or.sleepnet.mapper.ConferenceMapper;
import kr.or.sleepnet.mapper.MemberMapper;
import kr.or.sleepnet.session.MemberSession;
import kr.or.sleepnet.utils.StringUtil;

@Service
public class ConferenceService extends BaseService {
	
	public static Logger logger = Logger.getLogger(AdminService.class);

	@Value("${paging.num_per_page}")
	private int numPerPage;

	@Value("${paging.page_per_block}")
	private int pagePerBlock;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	MemberMapper memberMapper;
	
	@Autowired
	ConferenceMapper conferenceMapper;
	
	@Autowired
	APIMapper apiMapper;
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession();
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				/*
				Map<String, Object> param = new HashMap<>();
				param.put("auth", request.getHeader("auth"));

				Map<String, Object> mem = memberMapper.loginForAuth(param);
				if (mem == null) {
					throw new AuthExpireException();
				}
				ms.setData(mem);
				mem.put("ip", request.getRemoteAddr());
				memberMapper.loginDateUpdate(mem);
				*/
			}
		}
		return ms;
	}

}
