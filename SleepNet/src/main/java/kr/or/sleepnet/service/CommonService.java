package kr.or.sleepnet.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.or.sleepnet.mapper.CommonMapper;
import kr.or.sleepnet.mapper.MemberMapper;
import kr.or.sleepnet.model.Paging;
import kr.or.sleepnet.model.RESTResult;
import kr.or.sleepnet.session.MemberSession;
import kr.or.sleepnet.utils.DateUtil;
import kr.or.sleepnet.utils.StringUtil;

@Service
public class CommonService extends BaseService {
	
	private static Logger logger = Logger.getLogger(CommonService.class);
	
	@Autowired
	CommonMapper commonMapper;

	public void accessJoin (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			commonMapper.join(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@Transactional
	public void accessLogin (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			MemberSession ms = getMemberSession(request);
			if (ms.isLogin()) {
				param.put("id", ms.getId());
				param.put("ip", request.getRemoteAddr());
				commonMapper.login(param);
				commonMapper.loginReferer(param);
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardWrite (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			commonMapper.boardWrite(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardCommentWrite (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			commonMapper.boardCommentWrite(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardUpload (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			commonMapper.boardFileUpload(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardDownload (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			commonMapper.boardFileDownload(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@Transactional
	public void accessAnalysis (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			MemberSession ms = getMemberSession(request);
			
			String ip = request.getRemoteAddr();
			String userAgent = request.getHeader("User-Agent");
			String referer = request.getHeader("referer");
			
			param.put("ip", ip);
			param.put("agent", userAgent);
			param.put("referer", referer);
			param.put("url", request.getRequestURL().toString());
			
			if (userAgent.contains("Google") || userAgent.contains("Yahoo") || userAgent.contains("naver") || userAgent.contains("bot")) {
				ms.setLog(ip + "-" + DateUtil.getDateString());
				ms.setAgent(userAgent);
				return;
			}
			
			Map<String, Object> url = commonMapper.getUrl(param);
			if (url == null) {
				commonMapper.insertUrl(param);
			} else {
				commonMapper.updateUrl(url);
			}
			if (commonMapper.countUrl(param) > 50000) {
				url = commonMapper.getMinUrl(param);
				commonMapper.deleteUrls(url);
			}
			
			if (!StringUtil.isEmpty(ms.getLog())) {
				commonMapper.updateAccessPageCounter(param);
				if (!StringUtil.isEmpty(param.get("keyword"))) {
					Map<String, Object> inKey = commonMapper.getInKey(param);
					if (inKey == null) {
						commonMapper.insertInKey(param);
					} else {
						commonMapper.updateInKey(inKey);
					}
				}
			} else {
				if (ms.isLogin()) {
					param.put("id", ms.getId());
				}
				
				param.put("engine", StringUtil.getSearchEngine(referer));
				param.put("outkey", StringUtil.getSearchKeyword(referer));
				param.put("browser", StringUtil.getBrowser(userAgent));
				
				commonMapper.insertReferer(param);
				if (commonMapper.countReferer(param) > 50000) {
					Map<String, Object> minRef = commonMapper.getMinReferer(param);
					commonMapper.deleteReferers(minRef);
				}
				if (!StringUtil.isEmpty(param.get("outkey"))) {
					Map<String, Object> outKey = commonMapper.getOutKey(param);
					if (outKey == null) {
						commonMapper.insertOutKey(param);
					} else {
						outKey.put("engine", param.get("engine"));
						commonMapper.updateOutKey(outKey);
					}
				}
				Map<String, Object> browser = commonMapper.getBrowser(param);
				if (browser == null) {
					commonMapper.insertBrowser(param);
				} else {
					commonMapper.updateBrowser(browser);
				}
				Map<String, Object> today = commonMapper.getToday(param);
				if (today == null) {
					commonMapper.insertToday(param);
				} else {
					commonMapper.updateToday(today);
				}
				Map<String, Object> info = commonMapper.getInfo(param);
				if (info == null) {
					commonMapper.insertInfo(param);
				} else {
					commonMapper.updateInfo(info);
				}

				ms.setLog(ip + "-" + DateUtil.getDateString());
				ms.setAgent(userAgent);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		
	}
	
	/* Stats API Start */
	
	public RESTResult apiStatsCounter(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, commonMapper.apiStatsCounter(param));
	}
	
	public RESTResult apiStatsBrowser(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("total", commonMapper.apiStatsBrowserTotal(param));
		for (String b : StringUtil.BROWSERS) {
			param.put("browser", b);
			ret.put(b.replaceAll(" ", "_"), commonMapper.apiStatsBrowserTotal(param));
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiStatsOutKeyword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, commonMapper.apiStatsOutKeyword(param));
	}
	
	public RESTResult apiStatsReferer(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = commonMapper.apiStatRefererTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(15);
		paging.setBlockSize(5);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", commonMapper.apiStatReferer(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiStatsUrl(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = commonMapper.apiStatRefererTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(15);
		paging.setBlockSize(5);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", commonMapper.apiStatsUrl(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	/* Stats API End */
	
}
