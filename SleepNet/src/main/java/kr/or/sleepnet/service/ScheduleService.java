package kr.or.sleepnet.service;

import java.io.File;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import kr.or.sleepnet.mapper.ScheduleMapper;
import kr.or.sleepnet.utils.DateUtil;

@Service
public class ScheduleService extends BaseService {
	
	private static Logger logger = Logger.getLogger(ScheduleService.class);

	@Autowired
	ScheduleMapper scheduleMapper;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	RestTemplate restTemplete;
	
	public String getServerName() throws Exception {
		return InetAddress.getLocalHost().getHostName();
	}
	
	/* Server Management Start */
	public boolean isMainServer() throws Exception {
		if (scheduleMapper.isMainServer(getServerName()) > 0) {
			return true;
		}
		return false;
	}
	
	public void serverStatus() throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("host", getServerName());
		param.put("mem_total", String.format("%.1f MB", ((double) Runtime.getRuntime().totalMemory() / (1024 * 1024))));
		param.put("mem_free", String.format("%.1f MB", ((double) Runtime.getRuntime().freeMemory() / (1024 * 1024))));
		param.put("mem_max", String.format("%.1f MB", ((double) Runtime.getRuntime().maxMemory() / (1024 * 1024))));

		if (scheduleMapper.checkServer(param) > 0) {
			scheduleMapper.updateServer(param);
		} else {
			scheduleMapper.insertServer(param);
		}
	}
	
	public void electMainServer() throws Exception {
		scheduleMapper.removeOldServer();
		if (scheduleMapper.checkMainServer() > 0) {
			return;
		}
		
		Map<String, Object> param = new HashMap<>();
		param.put("host", scheduleMapper.getNewMainServer());
		scheduleMapper.electMainServer(param);
	}
	/* Server Management End */
	
	public void removeTemporaryFiles() throws Exception {
		Resource resource = resourceLoader.getResource("/temp");
		File temporaryDir = resource.getFile();
		if (temporaryDir.isDirectory()) {
			File[] files = temporaryDir.listFiles();
			for (File f : files) {
				if (!DateUtil.getDateString("yyyyMMdd").equals(f.getName())) {
					deleteFile(f);
				}
			}
		}
	}
	
	private void deleteFile(File path) {
		if (!path.exists()) {
			return;
		}
		
		if (path.isDirectory()) {
			File[] files = path.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					deleteFile(file);
				} else {
					file.delete();
				}
			}
		}

		path.delete();
	}
	
}
