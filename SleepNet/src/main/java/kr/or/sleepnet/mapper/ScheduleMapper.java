package kr.or.sleepnet.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScheduleMapper {
	
	/* Server Management Start */
	public int isMainServer(String host) throws Exception;
	public int checkServer(Map<String, Object> param) throws Exception;
	
	public void insertServer(Map<String, Object> param) throws Exception;
	public void updateServer(Map<String, Object> param) throws Exception;
	
	public void electMainServer(Map<String, Object> param) throws Exception;
	public void removeOldServer() throws Exception;
	
	public int checkMainServer() throws Exception;
	public String getNewMainServer() throws Exception;
	
	public List<Map<String, Object>> serverListAll(Map<String, Object> param) throws Exception;
	/* Server Management End */
	
}
