<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:choose>
	<c:when test="${menu eq 'member' }">
		<div class="left">
        <ul class="lnb">
          <li <c:if test="${submenu eq 'join' }">class="is-selected"</c:if>><a href="/member/join">회원가입</a></li>
          <li <c:if test="${submenu eq 'login' }">class="is-selected"</c:if>><a href="/member/login">로그인</a></li>
          <li <c:if test="${submenu eq 'find_idpw' }">class="is-selected"</c:if>><a href="/member/find_idpw">ID/PW 찾기</a></li>
          <li <c:if test="${submenu eq 'terms_privacy' }">class="is-selected"</c:if>><a href="/member/terms_privacy">개인정보처리방침</a></li>
        </ul>
      </div>
	</c:when>
	<c:when test="${menu eq 'introduce' }">
		<div class="left">
			<ul class="lnb">
				<li <c:if test="${submenu eq 'greeting' }">class="is-selected"</c:if>><a href="/introduce/greeting">인사말</a></li>
				<li <c:if test="${submenu eq 'history' }">class="is-selected"</c:if>><a href="/introduce/history">연혁</a></li>
				<li <c:if test="${submenu eq 'rule' }">class="is-selected"</c:if>><a href="/introduce/rule">회칙</a></li>
				<li <c:if test="${submenu eq 'executive' }">class="is-selected"</c:if>><a href="/introduce/executive">임원진 명단</a></li>
				<li <c:if test="${submenu eq 'office' }">class="is-selected"</c:if>><a href="/introduce/office">사무국 안내</a></li>
				<li <c:if test="${submenu eq 'join' }">class="is-selected"</c:if>><a href="/introduce/join">회원가입 안내</a></li>
			</ul>
		</div>
	</c:when>
	<c:when test="${menu eq 'conference' }">
		<div class="left">
        <ul class="lnb">
          <li <c:if test="${submenu eq 'event_' }">class="is-selected"</c:if>><a href="/conference">학술행사 일정</a></li>
          <li <c:if test="${submenu eq 'history_list' }">class="is-selected"</c:if>><a href="/conference/history">학술행사 연혁</a></li>
          <li><a href="/conference/data">학술대회 자료</a></li>
        </ul>
      </div>
	</c:when>
	<c:when test="${menu eq 'board' }">
		<div class="left">
        <ul class="lnb">
          <li <c:if test="${submenu eq 'notice_' }">class="is-selected"</c:if>><a href="/board/notice">공지사항</a></li>
          <li <c:if test="${submenu eq 'data_' }">class="is-selected"</c:if>><a href="/board/data">자료실</a></li>
          <li <c:if test="${submenu eq 'thesis_' }">class="is-selected"</c:if>><a href="/board/thesis">회원 논문 검색</a></li>
          <li <c:if test="${submenu eq 'news_' }">class="is-selected"</c:if>><a href="/board/news">학회 소식지</a></li>
          <li <c:if test="${submenu eq 'board_' }">class="is-selected"</c:if>><a href="/board/members">회원 게시판</a></li>
          <li <c:if test="${submenu eq 'member_' }">class="is-selected"</c:if>><a href="/member/search">회원검색</a></li>
          <li>
            <a href="#">My page</a>
            <ul class="depth03">
              <li><a href="/member/mypage">- 회원정보 수정</a></li>
              <li><a href="/member/mypage/fee">- 회비납부내역</a></li>
              <li><a href="/member/mypage/password">- 비밀번호 변경</a></li>
              <li><a href="/member/mypage/withdraw">- 회원 탈퇴</a></li>
            </ul>
          </li>
        </ul>
      </div>
	</c:when>
	<c:when test="${menu eq 'hospital' }">
		<div class="left">
        <ul class="lnb">
          <li class="is-selected"><a href="/hospital">수면 클리닉 병원 찾기</a></li>
        </ul>
      </div>
	</c:when>
	<c:when test="${menu eq 'faq' }">
		<div class="left">
        <ul class="lnb">
          <li class="is-selected">
            <a href="#">수면 상담 FAQ</a>
            <ul class="depth03">
              <li <c:if test="${param.category eq 1}">class="on"</c:if>><a href="/board/faq?category=1">- 잠이란</a></li>
              <li <c:if test="${param.category eq 2}">class="on"</c:if>><a href="/board/faq?category=2">- 불면증</a></li>
              <li <c:if test="${param.category eq 3}">class="on"</c:if>><a href="/board/faq?category=3">- 사건수면</a></li>
              <li <c:if test="${param.category eq 4}">class="on"</c:if>><a href="/board/faq?category=4">- 소아수면장애</a></li>
              <li <c:if test="${param.category eq 5}">class="on"</c:if>><a href="/board/faq?category=5">- 수면과다증, 기면증</a></li>
              <li <c:if test="${param.category eq 6}">class="on"</c:if>><a href="/board/faq?category=6">- 수면일반-상식,꿈</a></li>
              <li <c:if test="${param.category eq 7}">class="on"</c:if>><a href="/board/faq?category=7">- 수면주기장애</a></li>
              <li <c:if test="${param.category eq 8}">class="on"</c:if>><a href="/board/faq?category=8">- 치료-수면위생</a></li>
              <li <c:if test="${param.category eq 9}">class="on"</c:if>><a href="/board/faq?category=9">- 코골이, 수면무호흡증</a></li>
              <li <c:if test="${param.category eq 10}">class="on"</c:if>><a href="/board/faq?category=10">- 하지불안증 및 주기성 하지 운동장애</a></li>
              <li <c:if test="${param.category eq 11}">class="on"</c:if>><a href="/board/faq?category=11">- 기타</a></li>
            </ul>
          </li>
        </ul>
      </div>
	</c:when>
	<c:when test="${menu eq 'sleep' }">
		<div class="left">
        <ul class="lnb">
          <li <c:if test="${submenu eq 'normal' }">class="is-selected"</c:if>><a href="/sleep/normal">정상 수면</a></li>
          <li <c:if test="${submenu eq 'disorder' }">class="is-selected"</c:if>>
            <a href="#" class="opendepth">수면 장애</a>
            <ul class="depth03">
              <li <c:if test="${param.content eq 'OSAHS' }">class="on"</c:if>><a href="/sleep/disorder?content=OSAHS">- 폐쇄성 수면 무호흡-저호흡</a></li>
              <li <c:if test="${param.content eq 'insomnia' }">class="on"</c:if>><a href="/sleep/disorder?content=insomnia">- 불면증</a></li>
              <li <c:if test="${param.content eq 'hypnolepsy' }">class="on"</c:if>><a href="/sleep/disorder?content=hypnolepsy">- 기면증</a></li>
              <li <c:if test="${param.content eq 'ds' }">class="on"</c:if>><a href="/sleep/disorder?content=ds">- 주간졸림증</a></li>
              <li <c:if test="${param.content eq 'child' }">class="on"</c:if>><a href="/sleep/disorder?content=child">- 어린이의 수면장애</a></li>
              <li <c:if test="${param.content eq 'at' }">class="on"</c:if>><a href="/sleep/disorder?content=at">- 하지불안증과 주기성 사지 운동증</a></li>
              <li <c:if test="${param.content eq 'parasomnia' }">class="on"</c:if>><a href="/sleep/disorder?content=parasomnia">- 사건수면</a></li>
              <li <c:if test="${param.content eq 'CRSD' }">class="on"</c:if>><a href="/sleep/disorder?content=CRSD">- 일주기 리듬 수면 장애</a></li>
            </ul>
          </li>
          <li <c:if test="${submenu eq 'check' }">class="is-selected"</c:if>>
            <a href="#" class="opendepth">수면 검사</a>
            <ul class="depth03">
              <li <c:if test="${param.content eq 'PSG' }">class="on"</c:if>><a href="/sleep/check?content=PSG">- 수면다원검사</a></li>
              <li <c:if test="${param.content eq 'MSLT' }">class="on"</c:if>><a href="/sleep/check?content=MSLT">- 다중입면잠복기 검사</a></li>
            </ul>
          </li>
          <li <c:if test="${submenu eq 'news_' }">class="is-selected"</c:if>><a href="/info/news_list">수면 뉴스</a></li>
          <li <c:if test="${submenu eq 'self-test' }">class="is-selected"</c:if>><a href="/sleep/self-test">자가진단</a></li>
          <li <c:if test="${submenu eq 'commandment' }">class="is-selected"</c:if>><a href="/sleep/commandment">건강한 수면을 위한 10계명</a></li>
        </ul>
      </div>
	
	</c:when>
</c:choose>