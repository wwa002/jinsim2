<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- header -->
	<header>
		<div class="inner">
      <h1><a href="/" class="sprites hide_text">대한수면연구학회</a></h1> <a href="#" class="btn_openGnb sprites hide_text">메뉴열기</a>
    </div>
	</header>
	<!-- //header -->

	<!-- menu -->
	<nav class="menu">
    <div class="inner">
      <div class="util">
        <span class="myInfo"><img src="/resources/images/ico_mypage.gif" alt=""> 홍길동 님</span>
        <a href="/" class="pc">HOME</a>
        <a href="/member/login">LOGIN</a>
        <a href="/member/join" class="pc">JOIN</a>
        <a href="mailto:koreasleep@empal.com" class="pc">CONTACT US</a>
      </div>

      <ul class="gnb clear_float">
        <li>
          <a href="#">학회소개</a>
          <ul class="depth02">
            <li><a href="/introduce/greeting">인사말</a></li>
            <li><a href="/introduce/history">학회 연혁</a></li>
            <li><a href="/introduce/rule">회칙</a></li>
            <li><a href="/introduce/executive">임원진 명단</a></li>
            <li><a href="/introduce/office">사무국 안내</a></li>
            <li><a href="/introduce/join">회원가입 안내</a></li>
          </ul>
        </li>
        <li>
          <a href="#">학술행사</a>
          <ul class="depth02">
            <li><a href="/conference">학술행사 일정</a></li>
            <li><a href="/conference/history">학술행사 연혁</a></li>
            <li><a href="/conference/data">학술대회 자료</a></li>
          </ul>
        </li>
        <li>
          <a href="#">학회지</a>
          <ul class="depth02">
            <li><a href="https://jsm.jams.or.kr/co/com/EgovMenu.kci?s_url=/sj/search/sjSereClasList.kci&s_MenuId=MENU-000000000053000&accnId=" target="_blank">학회지 검색</a></li>
            <li><a href="https://jsm.jams.or.kr/co/main/jmMain.kci" target="_blank">온라인 논문투고</a></li>
          </ul>
        </li>
        <li>
          <a href="#">회원마당</a>
          <ul class="depth02">
            <li><a href="/board/notice">공지사항</a></li>
            <li><a href="/board/data">자료실</a></li>
            <li><a href="/board/thesis">회원 논문 검색</a></li>
            <li><a href="/board/news">학회 소식지</a></li>
            <li><a href="/board/members">회원 게시판</a></li>
            <li><a href="/member/search">회원검색</a></li>
            <li><a href="/member/mypage">My page</a></li>
          </ul>
        </li>
        <li>
          <a href="#">수면클리닉 병원찾기</a>
          <ul class="depth02">
            <li><a href="/hospital">수면 클리닉 병원 찾기</a></li>
          </ul>
        </li>
        <li>
          <a href="#">수면상담FAQ</a>
          <ul class="depth02">
            <li><a href="/board/faq">수면 상담 FAQ</a></li>
          </ul>
        </li>
        <li>
          <a href="#">수면정보</a>
          <ul class="depth02 last">
            <li><a href="/sleep/normal">정상 수면</a></li>
            <li><a href="/sleep/disorder?content=OSAHS">수면 장애</a></li>
            <li>
              <a href="/sleep/check?content=PSG">수면 검사</a>
              <ul class="depth03">
                <li><a href="/sleep/check?content=PSG">- 수면다원검사</a></li>
                <li><a href="/sleep/check?content=MSLT">- 다중입면잠복기 검사</a></li>
              </ul>
            </li>
            <li><a href="#">수면 뉴스</a></li>
            <li><a href="/sleep/self-test">자가진단</a></li>
            <li><a href="/sleep/commandment">건강한 수면을 위한 십계명</a></li>
          </ul>
        </li>
      </ul>
    </div>
	</nav>
  <a href="#" class="btn_closeGnb sprites hide_text">메뉴닫기</a>
	<!-- //menu -->