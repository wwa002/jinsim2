<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <!-- footer -->
  	<footer>
      <div class="inner">
        <p class="f_logo sprites hide_text">대한수면연구학회</p>
        <div class="link">
          <a href="#">개인정보취급방침</a>
          <a href="#">이메일 무단 수집 거부</a>
        </div>
        <address>COPYRIGHT 2009 KOREAN SLEEP SOCIETY. ALL RIGHTS RESERVED.<br />
        [07061]서울시 동작구 보라매로5길20 서울대학교 보라매병원 행복관 11132호 교수 의국<br />
        TEL : 02-870-3864 FAX : 02-870-3866 EMAIL : koreasleep@empass.com</address>
      </div>
  	</footer>
  	<!-- //footer -->
	</section>
	<!-- //contents -->

	<!-- dim -->
	<div class="dim"></div>
	<!-- //dim -->
</body>
</html>
