<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>정상 수면</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;정상 수면</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <h4 class="bulletBox mgt0">정상수면</h4>
          <p class="txt">수면은 빠른 안구운동이 수면중에 빠른 안구운동 수면 또는 렘수면(REM 수면)과 빠른안구운동이 나타나지 않는 비렘수면(non-REM수면)으로 구성되어 있습니다. 렘수면에서는 꿈을 꾸게 되고 이때는 몸을 움직일 수 없어 꿈을 꾸면서도 행동을 직접 하지 않게 만들어 줍니다. 렘수면은 전체수면의 20~25%를 차지하고 비렘수면은 75~80%를 차지합니다.<br />
          비렘수면은 얕은 수면인 1-2기 수면과 깊은 수면인 3-4기 수면으로 구성되어 있습니다. 아래그림에서 보이는 것처럼 렘수면-비렘수면은 약 90-120분의 주기로 반복되어 정상적으로는 약 5회 정도의 주기를 하룻밤에 가지게 됩니다.</p>
          <div class="box_img">
            <img src="/resources/images/img_sleep01_01.jpg" alt="">
            <img class="m" src="/resources/images/img_sleep01_01_m.jpg" width="320" alt="">
          </div>
          <p class="txt">사람마다 필요한 수면시간은 개인별로 차이가 있습니다. 일반적으로는 성인은 평균 7-8시간이 필요하고 어린이는 9-10시간이 필요한 것으로 알려져 있습니다. 그러나 각자에게 필요한 수면시간은 개개인마다 다르며 잠을 자고 일어났을 때 아침에 일어나서 피곤하지 않고 낮동안 졸립지 않게 생활할 수 있는 수면시간이 개인에게 필요한 잠의 양입니다.<br />
            수면의 역할은 회복, 에너지 보존, 기억, 면역, 감정조절 등의 역할을 하고 있으며 잠이 부족한 경우는 피곤하고 주간졸림증과 기억력 및 집중력의 감소, 감정기복이 심하고 식욕이 증가하여 체중증가가 일어납니다. 실제로 24시간이상 안자는 경우는 혈중알코올농도 0.1%의 상태와 같습니다.
            체르노빌 폭발사건과 우주선 챌린저호의 폭발과 같은 대형 참사가 근무자의 수면부족으로 인한 실수로 비롯된 사고라는 것은 잘 알려져 있습니다. 그외 잦은 교통사고를 일으키기도 합니다. 수면의 양이 부족하지 않음에도 주간졸림증이 있거나 자고 일어나도 개운치 않을 때는 수면의 질에 문제를 일으키는 수면장애가 있는지 점검을 해보는 것이 필요합니다.</p>

          <h4 class="bulletBox">수면위생 - 잠을 잘 자기 위한 기본 원칙</h4>
          <ul class="txt indent_list_bullet">
            <li><span class="bullet_round_blue">1</span> 매일 규칙적인 시간에 잠자리에 들고 일정한 시간에 일어나십시오.</li>
            <li><span class="bullet_round_blue">2</span> 잠자는 환경이 조용하고 환하지 않도록 하십시오. 너무 덥거나 춥지 않도록 하십시오.</li>
            <li><span class="bullet_round_blue">3</span> 매일 규칙적인 운동을 하십시오. 그러나 자기 직전에 지나치게 운동을 많이 하는 것은 더 잠을 방해할 수 있습니다.</li>
            <li><span class="bullet_round_blue">4</span> 카페인이 들어 있는 음료나 음식을 피하십시오.</li>
            <li><span class="bullet_round_blue">5</span> 자기 전에 흡연이나 음주를 피하십시오. 술은 처음에는 수면을 유도하는 것으로 생각되지만 수면의 후반기에서 잠을 자주 깨게 하며 수면무호흡증을 악화시킬 수 있습니다.</li>
            <li><span class="bullet_round_blue">6</span> 자기 전 따뜻한 목욕은 도움이 될 수 있습니다.</li>
            <li><span class="bullet_round_blue">7</span> 배가 고프거나 과식을 피하십시오.</li>
            <li><span class="bullet_round_blue">8</span> 잠자리에서 시계를 보거나 휴대전화를 자꾸 보는 일은 잠을 방해합니다.</li>
            <li><span class="bullet_round_blue">9</span> 잠자리에서 TV를 보거나 독서를 하거나 먹거나 다른 일을 하는 것은 잠을 방해합니다. 잠자리에서는 잠을 자는 일만 하도록 하십시오.</li>
            <li><span class="bullet_round_blue">10</span> 잠이 오지 않거나 중간에 깨어 잠이 오지 않을 경우에는 차라리 잠자리에서 일어나서 다른 일을 해보십시오. 다른 일을 하던 중 잠이 오면 그 때 잠자리로 가도록 하십시오.</li>
            <li><span class="bullet_round_blue">11</span> 밤에 밝은 빛에 노출되지 않도록 하십시오.</li>
          </ul>

          <h4 class="bulletBox">수면의 조절</h4>
          <p class="txt">수면과 각성은 "수면욕구"와 "생체시계"의 두 가지에 의해 조절됩니다.</p>
          <dl class="txt define">
            <dt>수면욕구</dt>
            <dd>모든 사람에게는 오래 깨어있는 시간이 길수록 잠을 자고자 하는 욕구가 생깁니다.<br />수면욕구는 자동적으로 조절되는 "신체항상성"의 한가지로 이로 인하여 잠을 못 자거나 부족하게 잔 경우는 낮에도 자주 졸립거나 일찍 잠이 들게되는 현상이 생기게 되는 것입니다. 그리고 잠을 자게 되면 수면 욕구가 감소하여 본인이 필요한 만큼 잔 경우에는 더 이상 졸립지 않게 됩니다.</dd>
          </dl>
          <dl class="txt define">
            <dt>생체시계</dt>
            <dd>수면욕구만이 수면과 각성을 조절하는 요소라면 우리는 밤에 잠을 못자거나 잔다음날에는 깨어있기가 어려울 것입니다. 그러나 실제로 전날 적게 잤다고 하더라도 낮에는 깨어있는 경우가 대부분입니다. 이는 우리 뇌에 존재하는 "생체 시계'때문입니다. 생체시계는 뇌의 시상하부의 "상교차핵"이라는 부분에 존재하며 각성 즉 우리가 낮에 깨어있을 수 있도록 합니다. 낮에는 깨어 있고 밤에는 잠을 자게 만다는 낮-밤을 결정하게 됩니다.
              <div class="box_img">
                <img src="/resources/images/img_sleep01_02.jpg" alt="">
                <img class="m" src="/resources/images/img_sleep01_02_m.jpg" alt="">
              </div>
              대개 점심 먹고 졸립게 되는 식곤증은 오전 중에 깨어 있음으로 인하여 증가한 수면욕구가 점점 활성화되는 생체시계의 각성신호보다 일시적으로 강해서 졸립게 되는 것입니다. 구후에는 다시 생체시계의 각성신호가 더 강하므로 저녁에 자기 직전까지는 그다지 졸립지 않게 됩니다. <br/>
              생체시계의 주기에 따라 일부의 사람들은 일찍 자고 일찍 일어나는 스케줄을 선호하고 일부의 사람들은 늦게 자고 늦게 일어나는 수면-각성주기를 가지게 됩니다.<br/>
              사람마다 약간씩의 차이가 있는데 일부사람들은 위에서 언급한 것처럼 지나치게 극단적인 수면각성주기를 가지게 되어(초저녁에 잠들고 새벽에 깨거나 새벽에 잠들어 점심 경에 일어나는 경우) 정상적인 사회생활을 하기 힘든 경우가 종종 있습니다. 이를 일주기성 수면장애라 부르며 적절한 치료가 필요합니다. 수면장애-일주기 장애 항목을 참조하세요.
              <div class="box_img">
                <img src="/resources/images/img_sleep01_03.jpg" alt="">
                <img class="m" src="/resources/images/img_sleep01_03_m.jpg" alt="">
              </div>
            </dd>
          </dl>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>