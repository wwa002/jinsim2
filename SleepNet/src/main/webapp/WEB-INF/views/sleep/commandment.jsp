<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>건강한 수면을 위한 십계명</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;건강한 수면을 위한 십계명</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="box_img tit_commandment">
          <img src="/resources/images/tit_commandment.jpg" alt="">
          <img class="m" src="/resources/images/tit_commandment_m.jpg" width="229" alt="">
        </div>
        <div class="commandment">
          <ul class="indent_list_bullet list_commandment">
            <li><span class="bullet_round_blue">1</span> 잠자리에 드는 시간과 아침에 일어 나는 시간을 규칙적으로 하라.</li>
            <li><span class="bullet_round_blue">2</span> 잠자리에 소음을 없애고, 온도와 조명을 안락하게 하라.</li>
            <li><span class="bullet_round_blue">3</span> 낮잠은 피하고 자더라도 15분 이내로 제한하라.</li>
            <li><span class="bullet_round_blue">4</span> 낮에 40분 동안 땀이 날 정도의 운동은 수면에 도움이 된다. <br />(그러나, 늦은 밤에 운동은 도리어 수면에 방해가 된다)</li>
            <li><span class="bullet_round_blue">5</span> 카페인이 함유된 음식, 알코올 그리고 니코틴은 피하라.<br />(술은 일시적으로 졸음을 증가시키지만, 아침에 일찍 깨어나게 한다.)<li>
            <li><span class="bullet_round_blue">6</span> 잠자기 전 과도한 식사를 피하고 적당한 수분 섭취를 하라.</li>
            <li><span class="bullet_round_blue">7</span> 수면제의 일상적 사용을 피하라.</li>
            <li><span class="bullet_round_blue">8</span> 과도한 스트레스와 긴장을 피하고 이완하는 것을 배우면 수면에 도움이 된다.</li>
            <li><span class="bullet_round_blue">9</span> 잠자리는 수면과 부부 생활을 위해서만 사용하라.<br />(즉, 잠자리에 누워서 책을 보거나 TV를 보는 것을 피하라.)</li>
            <li><span class="bullet_round_blue">10</span> 잠자리에 들어 20분 이내 잠이 오지 않는다면, 잠자리에서 일어나 이완하고 있다가 피곤한 느낌이 들 때 다시 잠자리에 들어라. 즉, 잠들지 않고 잠자리에 오래 누워있지 마라. 이는 오히려 과도한 긴장을 유발하여 더욱 잠들기 어렵게 만든다.<li>
          </ul>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>