<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 장애 : <span>폐쇄성 수면 무호흡-저호흡</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 장애</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <h4 class="bulletBox mgt0">폐쇄성 수면 무호흡-저호흡 (Obstructive Sleep Apnea-hypopnea)</h4>
          <p class="txt">가장 흔한 형태의 수면 무호흡증입니다. 잠을 잘 때는 정상적으로 숨쉬는 길, 즉 기도를 둘러싸고 있는 근육들이 이완되면서 목젖, 편도, 혀 등이 뒤로 쳐지게 됩니다. 이로 인해 깨어 있을 때보다 기도가 약간 좁아지나 대부분의 사람들에서는 문제가 생기지 않습니다. 그러나 일부 사람에서는 잠잘 때 기도가 심하게 좁아져 공기가 기도를 통과하는 것을 막기 때문에 코골이와 수면 무호흡증이 발생합니다.<br />
          기도가 심하게 좁아지거나 아예 기도의 벽이 서로 붙어버리면 숨을 쉴 수 없게 되고 이와 같이 숨이 자주 멎어서 코고는 중간에 조용해지는 것입니다. 어떻게 해서라도 숨을 쉬기 위하여 뇌의 신호를 받고 횡경막과 가슴근육은 더욱 힘을 주게 되고, 결과적으로 잠에서 자주 깹니다. 이러한 수면중단은 기도의 근육을 자극하여 더 좁아지게 하는 악순환이 반복됩니다. 그러나 깨는 시간이 너무 짧아서 다음날 아침에 자주 깬 사실을 기억하지 못합니다. 수면 무호흡은 하루 밤에 대개 수십번에서 수백번 발생합니다.<br />
          폐쇄성 수면 무호흡증의 원인으로는 정상보다 턱이 작은 경우, 혀나 편도선이 큰 경우, 목젖이 길게 늘어져 있는 경우처럼 기도를 부분적으로 막는 조직이 있는 경우들이 있습니다. 과체중인 경우 목의 지방조직으로 인하여 기도를 좁게 만들게 됩니다.<br />
          비만, 나이가 많은 경우, 남자, 당뇨병, 폐경 여성, 코에 문제가 있는 사람들에서 폐쇄성 수면 무호흡증이 더 많이 발생합니다. 술, 수면제, 안정제등은 근육의 긴장도를 더욱 떨어뜨려서 기도가 더 잘 막히게 하기 때문에 수면 무호흡이 있는 사람은 피하거나 주의하여야 합니다.<br />
          수면 중에 무호흡이 발생하면 체내의 산소가 부족하게 됩니다. 심장은 더 많은 피를 순환시키기 위하여 빨리 뛰게 됩니다. 또한 자주 깨고 교감신경이 향진되어서 심장은 더 세게 박동을 해야 합니다. 혈압이 올라가고 심장박동이 불규칙하여지며 심장박동이 일시적으로 멈추는 경우도 있습니다. 이와 같이 심장정지가 오래 지속되면 다른 질병이 없던 건강한 사람도 사망할 수 있습니다. 이런 경우가 건강하다고 생각했던 사람이 잠자리에 들었다가 갑자기 사망하는 원인의 하나입니다. 수면 무호흡증이 오래되면 성생활에 문제가 생기고 혈압이 올라가며 심장부전, 부정맥, 심근경색증 및 뇌졸중 등이 발생할 수도 있습니다. 따라서 코골이나 수면 무호흡증이 있으면, 수면 클리닉을 방문하여 필요한 검사와 치료를 받아야 합니다.</p>
          <div class="box_img">
            <img src="/resources/images/img_sleep02_01.jpg" alt="">
            <img class="m" src="/resources/images/img_sleep02_01_m.jpg" width="320" alt="">
          </div>
          <p class="txt">정환한 진단입니다. 수면의 질, 무호흡 및 저호흡의 횟수, 무호흡의 지속시간, 저산소증의 정도, 심장박동의 변화, 무호흡-저호흡이 주로 발생하는 수면단계(sleep stage)와 자세, 수면 중 깨어나는 횟수, 기도의 어느 부위가 주로 막히는지 등에 대한 정확한 정보가 있어야 합니다. 이 모든 것을 알아내는 검사가 수면다원검사입니다. 이 검사는 하루 밤 동안 실시됩니다. 무호흡-저호흡이 있는 경우 다음날밤이나 다른 날 밤, 2차 수면다원검사를 시행하게 됩니다. 2차 수면다원검사에서는 아래에 설명되어 있는 지속적인 기도 양압기를 착용하고 검사를 시행하는 것으로 무호흡과 저호흡 및 코골이가 없어지는 적정한 압력을 찾게 됩니다.</p>

          <h4 class="bulletBox">치료</h4>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 수면욕구</dt>
            <dd>모든 사람에게는 오래 깨어있는 시간이 길수록 잠을 자고자 하는 욕구가 생깁니다.<br />수면욕구는 자동적으로 조절되는 "신체항상성"의 한가지로 이로 인하여 잠을 못 자거나 부족하게 잔 경우는 낮에도 자주 졸립거나 일찍 잠이 들게되는 현상이 생기게 되는 것입니다. 그리고 잠을 자게 되면 수면 욕구가 감소하여 본인이 필요한 만큼 잔 경우에는 더 이상 졸립지 않게 됩니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 수술</dt>
            <dd>늘어나거나 커진 연구개, 목젖, 편도 등을 수술하여 기도를 넓게 해주는 방법으로 전신마취를 통한 수술이 있습니다. 정상 수면호흡으로
            회복되는 성공률은 약 50% 정도이며 환자의 구강구조 및 수면무호흡의 정도에 따라 성공률이 다릅니다. 수면 무호흡-저호흡의 정도에
            따라 재발하는 경우가 많습니다. 부작용으로 코 소리, 물을 삼킬 때 코로 역류하는 것, 인후 통증 등이 생길 수 있습니다. 따라서 다른 치료
            법이 여의치 않을 때 수술을 생가하는 것이 좋으며, 이때도 정확한 검사를 통하여 수술의 성공 가능성을 잘 알아본 후에 받는 것을 추천
            합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">3</span> 구강 내 기구</dt>
            <dd>이것은 틀니같이 생긴 것인데 잘 때만 사용하는 것으로 일부 환자에서 치료효과가 좋습니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">4</span> 체중감량</dt>
            <dd>과체중의 경우 체중을 줄이면 수면 중 호흡이 좋아지는데, 자기 체중의 약 10%를 줄이면 (예:80kg-> 72kg) 수면 무호흡-저호흡의 횟수가
            반으로 줍니다. 매일 1시간정도 운동을 하고 간식을 줄이고 저녁식사를 적게 하는 것이 좋습니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">5</span> 금주</dt>
            <dd>술을 마시게 되면 모든 근육이 이완되어 쳐지게 됩니다. 따라서 수면 무호흡을 악화시키므로 마시지 않아야 합니다. 피할 수 없는 경우
            라도 잠자기 4시간 이내에는 삼가하여야 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">6</span> 수면제를 금한다</dt>
            <dd>일부 수면제는 호흡중추를 억제하고 근육을 이완시켜 수면 무호흡을 더 심하게 합니다. 꼭 필요한 경우에는 수면장애전문의와 상의를 해야 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">7</span> 약을 복용 시 주의한다</dt>
            <dd>두통, 불안, 기타 신체증상을 조절 하기 위하여 처방되는 약들도 수면과 호흡에 영향을 주기 때문에 주의를 해야 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">8</span> 옆으로 누워서 자는 습관을 들인다</dt>
            <dd>어떤 사람들은 수면 무호흡이 똑바로 누워서 잘 때 심해지기 때문에 옆으로 누워 자는 것이 도움이 됩니다.</dd>
          </dl>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>