<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 장애 : <span>사건수면</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 장애</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
    	<%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <h4 class="bulletBox mgt0">사건수면</h4>
          <p class="txt">사건수면이란 수면과 관련되어 방해가 되는 모든 상황을 총칭하는 광범위한 용어입니다. 이 같은 행동이나 경험들은 주로 밤에 발생합니다. 대부분의 경우 사건수면은 증상이 가볍고, 드물게 일어납니다. 하지만, 때때로 약물치료가 필요할 만큼 괴로운 경우도 있습니다.</p>

          <h4 class="bulletBox mgt0">수면-각성장애란 무엇인가?</h4>
          <p class="txt">가장 일반적인 사건수면은 수면-각성장애이며, 여기에는 혼돈각성, 몽유병, 야경증이 있습니다. 이 병은 수면과 각성사이의 혼재된 시간동안 일어나며, 때로는 가장 깊이 자고 있을 때 발생합니다. 환자는 복잡한 행동을 할 정도로 각성하게 되지만, 아직 잠들어 있는 상태이며, 당시 활동을 기억하지 못 합니다.<br />
          사건수면은 어린 소아에서 가장 흔하며, 심각한 정신병이 있는 것은 아닙니다. 이 같은 병들은 유전되는 경향이 있지만, 피곤했을 때, 열이 날 때, 특정한 약물 복용시 더욱 심한 증상을 보입니다. 스트레스에 영향을 받습니다.</p>

          <h4 class="bulletBox mgt0">혼돈각성</h4>
          <p class="txt">혼돈각성은 유아나 젖먹이에서 흔하게 발생하지만 성인에서도 발생할 수 있습니다. 수면 중 소리 지르고 발버둥치는 것으로 시작하여, 기상한 듯 보이게 됩니다. 하지만, 혼돈스러워 보이고 기분이 나빠 보이며, 진정시키기 힘듭니다. 사건수면중에 환자를 각성시키기는 어렵습니다. 혼돈각성은 30분까지 지속될 수 있으며, 대부분 동요가 멈추고 잠깐 각성된 후 다시 잠자리에 드는 것으로 끝이 납니다.</p>

          <h4 class="bulletBox mgt0">몽유병</h4>
          <p class="txt">이 병은 학동기 소아에서 흔히 보일 수 있으며, 침대에서 나와 침실을 걸어다니는 가벼운 증상에서 다른 방이나 집 밖으로 나가는 복잡한 행동까지 다양하게 보일 수 있습니다. 때때로 대화를 하기도 하지만, 이치에 맞지 않는 말을 합니다. 때로는 몽유병 기간 동안 가구를 옮기는 것 같은 복잡한 행동도 하지만 대부분 특별한 목적은 없습니다. 몽유병 기간 동안 다치는 경우는 흔하지 않지만, 추운 날 잠옷을 입고 밖에 나가는 등의 위험한 상황을 만들 수도 있습니다. 대부분의 경우, 치료는 필요치 않습니다. 이런 증상들이 심각한 병이나 정신병을 의미하는 경우는 매우 드문 경우로, 몽유병 환자나 가족들은 큰 걱정을 할 필요 없습니다. 소아에서는 나이가 들어감에 따라 횟수가 줄지만, 성인에서도 증상이 남는 경우도 있습니다.</p>

          <h4 class="bulletBox mgt0">수면관련식이장애</h4>
          <p class="txt">몽유병의 드문 형태로 수면관련식이장애가 있습니다. 이 증상은 반복적으로 의식 각성없이 수면중 음식을 섭취하는 삽화를 특징으로 합니다. 수면관련식이장애는 심각하게 살이 찔 수 있습니다. 성별이나 나이에 상관없이 발생할 수 있지만, 젊은 성인여성에서 가장 흔하게 발생합니다.</p>

          <h4 class="bulletBox mgt0">야경증</h4>
          <p class="txt">각성장애중 가장 극적이고 심하게 표현되며, 보호자에게 걱정을 가져오는 병입니다. 야경증은 섬뜩하게 하는 비명소리와 함께 시작하며, 극도의 공포를 나타내는 증상들(동공확대, 거친 호흡, 심박동수 증가, 발한, 과도한 흥분)이 동반됩니다. 야경증 삽화중 환자는 침대 밖으로 튀어나와, 방이나 심지어 집 밖을 걸어 다닐 수 있습니다. 광분 상태시 환자는 자신이나 다른 사람을 다치게 할 수 있습니다.<br />
          이런 삽화들은 목격자를 무섭게 하고 불안하게 하지만, 환자 본인은 대개 각성되지 않은 상태로 각성후에는 했던 일을 기억하지 못 합니다. 전형적인 악몽이나 나쁜 꿈과는 달리, 야경증 삽화는 깨어난 후 기억나는 생생한 꿈과 동반되지 않습니다.</p>

          <h4 class="bulletBox mgt0">렘 수면 행동 장애</h4>
          <p class="txt">호흡과 관련된 근육을 제외하고 몸의 모든 근육들은 렘 수면동안 정상적으로 마비됩니다. 어떤 사람들에게서는(특히 노인의 경우) 마비가 불완전하거나 없어서 꿈의 행동을 실제로 하게 됩니다. 이런 수면과 관련된 행동은 폭력적일 수 있고, 환자 본인이나 같이 자는 사람을 다치게 하기도 합니다. 야경증이 있는 사람과는 반대로 환자는 꿈을 생생히 기억합니다. 렘 수면 행동장애는 약물로 조절 가능합니다.</p>

          <h4 class="bulletBox mgt0">수면각성 장애의 진단은 ?</h4>
          <p class="txt">자세한 병력 청취 및 경우에 따라 수면다원검사가 필요합니다.<br />전형적인 소아기 각성장애는 의학적 평가가 반드시 필요한 것은 아닙니다. 하지만 다음과 같은 증상이 있다면 의료기관을 방문하여야 합니다. 또한 소아기 이후 이런 수면각성장애는 드물기 때문에, 성인에서는 반드시 평가가 필요합니다. 수면각성장애는 수면무호흡증이나 위식도역류증 등에 의해 유발될 수 있습니다. 수면 전문의의 의한 환자의 행동이나 과거력에 대한 평가가 필요합니다.</p>
          <ul class="txt indent_list_bullet">
            <li><span class="bullet_round_blue">1</span> 폭력적이거나 상처를 입을 우려가 있을 때</li>
            <li><span class="bullet_round_blue">2</span> 다른 가족 구성원을 곤란하게 할 때</li>
            <li><span class="bullet_round_blue">3</span> 낮 동안 과도한 수면을 가져올 때</li>
          </ul>

          <h4 class="bulletBox mgt0">수면각성장애의 치료는 무엇인가?</h4>
          <p class="txt">수면각성장애의 경우 몇 가지 예방지침으로 주변사람들이나 환자 자신의 안전을 보장해 줄 수 있습니다. 침실에 다치게 만들 수 있는 물건을 없애고, 유리창을 깨지지 않게 하며, 침대가 아닌 바닥에서 자고, 문이나 유리창에 자물쇠나 알람을 설치하는 것이 환자나 가족들의 안전에 도움을 줄 수 있습니다.<br />
          수면각성장애의 경우 몇 가지 예방지침으로 주변사람들이나 환자 자신의 안전을 보장해 줄 수 있습니다. 침실에 다치게 만들 수 있는 물건을 없애고, 유리창을 깨지지 않게 하며, 침대가 아닌 바닥에서 자고, 문이나 유리창에 자물쇠나 알람을 설치하는 것이 환자나 가족들의 안전에 도움을 줄 수 있습니다.</p>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>