<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 장애 : <span>불면증</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 장애</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <h4 class="bulletBox mgt0">불면증</h4>
          <p class="txt">밤에 잠들기가 어렵거나 자주 깨는 것, 아침에 일찍 깨는 경우 그리고 숙면을 취하지 못하는 것을 불면증이라 합니다. 불면증이 있는 사람은 밤에도 괴롭지만 다음날 졸리고, 피곤하며, 정신 집중이 안되므로 낮시간에도 몹시 괴롭습니다.<br />
          밤에 잠을 못 자면, 다음날 피곤하고 졸립지만 다음날 밤에는 또 잠을 잘 수가 없는 것은 수수께끼와 같이 이해하기가 어렵습니다. 다행히 최근 수면의학의 발전으로 이런 분들에게 도움을 줄 수가 있습니다.<br />
          불면증은 모든 연령층에서 발생합니다. 여자와 노인에서 더 흔합니다. 우리나라의 약 5%에서 만성불면증을 가지고 있고 약 20%이상이 불면증을 경험합니다. 수일에서 일주일 못 자는 단기 불면증에서 종종 몇 주, 몇 달, 몇 년 동안 지속되는 만성불면증이 되는 경우가 매우 흔합니다.</p>

          <h4 class="bulletBox">불면증의 종류</h4>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 급성 불면증(acute insomnia)</dt>
            <dd>수일에서 수주 동안만 잠을 못 자는 것을 말합니다. 이 형태는 대개 흥분이나 스트레스가 원인입니다. 예를 들면 중요한 시험이나 운동시합을 앞둔 학생, 사업상 중요한 만남을 앞둔 사람, 부부싸움을 한 후 등에서 볼 수 있습니다. 잘 시간 가까이에 격렬한 운동을 한다거나, 열병을 앓는 경우에도 잠을 잘 이루지 못합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 만성 불면증 (Chronic insomnia)</dt>
            <dd>한달 이상 지속되는 불면증을 의미하며 한국인 전체의 약 15-20%가 만성불면증으로 고생하고 있습니다. 대부분이 항상 잠에 대하여 많은 걱정을 하고, 여러 가지 생각이나 걱정거리 때문에 잠을 못 잔다고 합니다. 그러나 수면중 호흡장애나 주기성 사지 운동증, 하지 불안 증후군 등의 다른 수면장애가 원인이거나 동반되는 경우가 많습니다. 따라서, 수면장애 클리닉을 방문하여서 정확한 진단을 받아야 합니다.</dd>
          </dl>

          <h4 class="bulletBox">불면증의 원인별 분류</h4>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> “일차성” 불면증</dt>
            <dd>다른 유발요인 없이 생긴 “일차성 불면증” </dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> “다른 질환과 동반되는 불면증” (또는 이차성 불면증)</dt>
            <dd>동반되는 다른 질환에는 폐쇄성 수면무호흡증, 하지 불안 증후군, 주기성 사지 운동증, 일주기성 수면장애와 같은 다른 수면장애, 우울증, 불안 장애등의 정신과 질환 등이 있습니다. 또한 호흡기 질환, 심장질환, 치매, 파킨슨병과 같은 신경과 질환도 불면증을 동반하는 경우가 흔합니다.</dd>
          </dl>

          <h4 class="bulletBox">불면증의 치료</h4>
          <p class="txt">불면증이 한 달 이상 계속되고 주간활동에 지장이 있다면 수면의학전문의의 진료를 받아야 합니다. 자세한 병력, 이학적 검사, 수면다원검사 등이 원인을 찾는데 필요합니다. 배우자나 동료에게 자기가 잠잘 때 코를 고는지, 숨을 멈추는 경우가 있는지와 잘 때 어떻게 행동을 하는지 등에 대한 정보가 도움이 많이 됩니다. 몇몇 불면증은 올바른 정보와 교육만으로 좋아지는 경우도 있습니다. 치료에 있어서 가장 중요한 것은 역시 정확한 진단이므로 수면클리닉의 진료를 추천합니다.</p>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 수면제가 도움이 됩니까?</dt>
            <dd>수면제가 잠을 잘 자게 할 수 있습니다. 일시적인 불면증에는 적절한 수면제를 쓰는 것이 큰 도움이 됩니다. 그러나, 불면증의 원인을 치료하는 것이 아니기 때문에 그 효과가 일시적일 수 있고 수면 무호흡이 원인인 불면증에는 수면제 종류에 따라 수면무호흡을 더 악화시킬 수 있습니다. 특히 술을 마신 후 에는 절대로 수면제를 복용하지 마십시오. 따라서, 수면제를 복용하시기 전에 수면의학전문의와 꼭 상의 하셔야 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 동반질환의 치료</dt>
            <dd>불면증을 완화시키기 위해서는 수면장애, 우울증, 내과적인 질환이나 통증에 대한 치료가 함께 수반되어야 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">3</span> 만성불면증을 위한 “인지행동치료”</dt>
            <dd>만성불면증의 경우 원인질환이나 동반질환을 치료하여도 불면증이 남아있는 경우가 있습니다.이때는 수면에 대한 이해도를 높이면서 자신의 잘못된 수면습관이나 믿음을 교정하며 수면제를 줄여서 끊어주는 “인지행동치료”가 만성 불면증의 첫번째 치료입니다.</dd>
          </dl>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>