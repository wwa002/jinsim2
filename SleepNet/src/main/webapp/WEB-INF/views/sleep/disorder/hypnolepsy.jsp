<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 장애 : <span>기면병</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 장애</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <h4 class="bulletBox mgt0">기면병이란</h4>
          <p class="txt">기면병은 낮시간의 과도한 졸림을 일으키는 중추성 질환으로 우리 머리에 있는 시상하부라고 하는 부분에서 정상적인 각성을 유지시켜주는 물질인 “hypocretin (히포크레틴)”이 분비가 결여되어 생기는 질환입니다. 밤에 잠을 충분히 잤는데도 낮에 잠이 너무 쏟아져서 일상생황이 힘든 경우가 많고 또한 때와 장소를 가리지 않고 마치 전기 스위치를 내리는 것처럼 갑자기 잠이 드는 수면발작을 보이기도 합니다.</p>

          <h4 class="bulletBox">기면병의 주증상</h4>
          <p class="txt">기면병은 5가지의 대표적인 증상을 가지고 있습니다. 이 증상들이 한꺼번에 나타나는 경우도 있지만, 대부분 한 두 가지 정도 보이는 경우가 많고 오랜 시간을 거쳐 점차적으로 진행될 수도 있습니다.</p>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 과도한 낮 졸림</dt>
            <dd>과도한 낮 졸림은 보통 기면병의 첫 증상입니다. 기면병 환자들은 종종 쉽게 피로를 느끼는 상황(예를 들면 식사직후, 지루한 강의를 듣는 동안..)뿐만 아니라 대부분의 사람들이 깨어 있을 수 있는 상황(영화를 보거나 편지를 쓸 때 혹은 운전 중)에서도 잠에 빠져드는 경향이 있습니다. 기면병을 가지고 있는 사람들은 비정상적인 시간에 또는 잠들게 되면 위험해 질 수 있는 상황에서 졸거나 몽롱해 집니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 탈력 발작</dt>
            <dd>탈력 발작이란 갑작스러운 근육의 힘이 짧은 시간 동안 빠지는 것을 말합니다. 탈력 발작은 잠깐 무릎에 힘이 빠지거나 그런 느낌이 드는 정도로 약하게 올 수 있고, 연체동물처럼 몸이 풀어져 완전히 맥없이 주저 앉거나 넘어지기도 합니다. 이러한 탈력 발작이 발생했을 때 환자는 의식이 멀쩡하기 때문에 주변의 소리를 다 들을 수 있고 무슨 일이 벌어졌는지도 압니다. 탈력 발작은 보통 웃거나 화낼 때 놀람과 같은 강한 감정 변화가 있을 때 유발됩니다. 탈력 발작의 빈도는 개인적인 차이가 커서 하루에도 몇 번씩 발생하기도 하고 평생동안 한 두 번만 발생하기도 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">3</span> 수면마비</dt>
            <dd>수면마비는 잠이 들거나 잠에서 깰 때 발생하는 단시간동안 근육의 힘이 없어지는 현상입니다. 흔히 가위눌림으로 불리기도 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">4</span> 입면 환각</dt>
            <dd>입면 환각은 환자가 잠에 들 때 혹은 잠에서 깰 때 발생하는 생생한 꿈 같은 환각입니다. 환자가 부분적으로 의식이 깨어 있는 상태에서 느끼는 생생한 장면, 소리, 느낌으로 보통 1분-15분 정도 지속됩니다. 주로 공포스러운 내용들입니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">5</span> 야간 수면의 방해</dt>
            <dd>이 증상은 종종 기면병 환자에게서 발생합니다. 낮에 깨어 있는데 문제가 있을 뿐만 아니라 역시 밤에 숙면을 취하는 데에도 문제가 있습니다.</dd>
          </dl>

          <h4 class="bulletBox">기면병의 진단은</h4>
          <ul class="txt indent_list_bullet">
            <li><span class="bullet_round_blue">1</span> 밤잠을 검사하는 수면다원검사와 낮잠을 검사하는 반복적 수면잠복기 검사를 통해 진단을 내립니다.</li>
            <li><span class="bullet_round_blue">2</span> 검사결과 정상인의 경우에는 얕은 수면에서 깊은 수면 단계로 바뀐 후 꿈을 꾸는 렘(REM) 수면이 나올 때까지 보통 80~90분 정도 걸리지만 기면병 환자는 잠이 들고 15분 이내에 렘 수면이 나오는 경우가 많습니다.</li>
            <li><span class="bullet_round_blue">3</span> 갑자기 발생한 경우 드물게 이차성 기면병의 원인이 될 수 있는 뇌질환을 배제하기 위해 뇌 MRI를 촬영하는 경우도 있습니다.</li>
          </ul>

          <h4 class="bulletBox">기면병의 치료</h4>
          <p class="txt">기면병은 현대 의학으로 아직 완치가 불가능합니다. 그러나 꾸준한 치료를 통해 증상들을 조절 개선하여 거의 정상적인 생활을 할 수 있습니다.</p>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 투약</dt>
            <dd>증상에 따라 낮의 졸림을 줄여주는 각성제와 탈력 발작을 예방하는 약물들이 처방됩니다. 약에 관한 의료진의 지시는 반드시 지키시고 약으로 인한 증상의 악화나 문제가 발생할 대는 꼭 의료진에게 문의를 하십시오.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 행동 치료</dt>
            <dd>1) 규칙적인 수면습관을 갖도록 합니다.기상과 취침시각을 일정하게 합니다.</dd>
            <dd>2) 매일 정해진 시간에 짧은 한 두번의 낮잠을 자도록 합니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">3</span> 주변 환경</dt>
            <dd>기면병은 환자의 가족, 친구, 동료, 주변 사람들이 이 질환에 대해 올바르게 이해를 하지 못한다면 치료가 어렵습니다. 낮 시간의 졸음은 게으른 사람, 무능한 사람으로 오해를 받을 수 있게 합니다. 또한 깨어 있는 동안의 탈력 발작과 꿈 꾸는 증상은 정신병으로 오해를 받을 수 있습니다. 따라서 가족들과 학교선생님에게 기면병임을 알리고 이에 관해 기본적인 정보를 알려주는 것이 필요합니다.</dd>
          </dl>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>