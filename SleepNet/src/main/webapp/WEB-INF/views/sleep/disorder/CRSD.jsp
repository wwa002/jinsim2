<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 장애 : <span>일주기 리듬 수면 장애</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 장애</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
    	<%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <h4 class="bulletBox mgt0">일주기 리듬 수면 장애</h4>
          <p class="txt">사람의 수면과 각성은 일정한 주기가 있어 반복됩니다. 이 일정한 주기를 일주기리듬이라고 부르며 약 24시간으로 이에 따라 매일 일정한 시간에 잠이 들고 일정한 시간에 일어나게 되는 것입니다. 이러한 일주기 리듬은 사람뿐만이 아니라 동물에도 존재하며 일부 식물에도 존재합니다. 이러한 일주기 리듬을 따르는 것은 수면-각성주기 외에 사람의 기초체온, 신체의 호르몬 주기, 그리고 잠의 호르몬인 멜라토닌(melatonin)의 분비 등이 있습니다.<br />
          일주기리듬을 결정하는 것은 사람의 뇌의 시상하부에 있는 상교차핵이라는 부분으로 우리의 생체시계라 할 수 있습니다. 이 시계의 리듬에 따라 대부분의 사람들은 저녁에 잠이 들고 아침에 잠이 깨게 되는데 일부의 사람에서는 생체시계의 셋팅이 일반적인 사회생활에 맞지 않는 일주기리듬을 가지고 있습니다. 이러한 사람들은 일반적으로 잠을 자는 저녁에 잠이 오지 못한다거나 아침에 일찍 깨기 어려운 증상, 또는 초저녁에 잠이 들어 새벽에 깨는 등의 증상으로 불면증이나 주간졸림증의 증상을 호소하게 됩니다. 이러한 수면장애를 일주기리듬수면장애라 부릅니다.<br />
          흔한 일주기 리듬 수면장애는 다음과 같은 2가지 종류가 있습니다.</p>

          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 지연성 수면 위상증후군 (delayed sleep phase syndrome)</dt>
            <dd>늦게 잠이 들고 늦게 일어나는 일주기리듬을 가진 경우로 저녁늦게까지 잠이 오지 않고 심한 경우에는 새벽 5-6시가 되어야 잠이 드는 경우도 있습니다. 이런 사람들은 아침일찍 일어나는 것이 힘들며 출근이나 등교를 위해 겨우 일어났다 하더라도 학교에서나 직장에서 졸음으로 인하여 특히 아침시간에 어려움을 겪는 경우가 많으며 만성수면부족에 시달릴 수 있습니다.<br />
            청소년이나 젊은 사람들에게 많이 있는 수면장애로 특히 저녁때 늦게까지 컴퓨터 게임을 하거나 TV를 보거나 하여 빛에 노출이 된 경우 수면을 더욱 지연시키게 됩니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 전진성 수면위상 증후군 (advanced sleep phase syndrome )</dt>
            <dd>초저녁에 잠이 들어 이른 새벽에 깨서 더 이상 잠을 이루지 못하는 수면주기를 가진 경우로 노인들에게 흔합니다. 나이가 들면서 수면주기가 전진되는 경우가 많이 있습니다. 일찍 잠이 들어 일찍 일어나는 아침형인간의 수면주기이므로 큰 문제가 되지 않는 경우들이 대부분이나 초저녁 7-8시부터 졸려서 사회생활에 문제가 있거나 새벽에 지나치게 일찍 깨서 잠을 이루지 못하여 괴로워하기도 합니다.<br />
            그 외에 해외로 여행을 간 경우 시차로 인하여 잠이 오지 않거나 낮에 졸린 경우들도 일주기 리듬 수면장애이며 주/야간 교대근무를 하는 경우 불면증이나 근무중 졸림증, 피곤함 등을 호소하게 되는데 이러한 교대근무수면장애들도 일주기리듬 수면장애입니다.</dd>
          </dl>

          <h4 class="bulletBox mgt0">진단은?</h4>
          <p class="txt">잠자러 가는 시간, 잠이 든 시간, 잠에서 깨는 시간을 주말과 주중을 모두 표시하는 수면일기를 일주일이상 작성하여 수면패턴을 파악하여 진단할 수 있습니다. 다른 수면장애가 동반되었을 것으로 의심이 되면 수면다원검사를 시행하기도 합니다.</p>

          <h4 class="bulletBox mgt0">치료는?</h4>
          <p class="txt">일반적인 수면제는 크게 도움이 되지 않습니다. 적절한 시간에 빛을 쏘이는 “광치료”와 개개인의 수면패턴에 따라 적절한 시간에 최소량의 멜라토닌을 복용하는 치료가 있으며 한가지 치료만 하거나 병행할 수 있습니다. 개개인의 수면패턴에 따라 시간이 달라지며 이는 수면전문가의 조언과 처방이 필요합니다.</p>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->


<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>