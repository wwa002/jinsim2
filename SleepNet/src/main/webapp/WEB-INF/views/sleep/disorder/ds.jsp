<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 장애 : <span>주간졸림증</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 장애</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
    	<%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <p class="txt mgt0">밤에 많이 잔 것 같은데도 낮에 자꾸 졸리십니까? </p>

          <p class="txt">주간 졸림증이란 밤에 충분한 수면을 취했음에도 불구하고 낮에 주체할 수 없는 졸리움을 느끼는 것으로, 때와 장소를 가리지 않고 졸거나 잠에 빠져들게 되는 증상을 말합니다. 이런 증상을 가진 환자들은 낮 동안 또렷하게 깨어 있어야 함에도 불구하고 자기도 모르게 꾸벅꾸벅 졸게 되고 잠에 빠져 들게 됩니다. 이로 인해 집중도 및 일상적인 업무와 학업의 수행도를 감소시켜 자존심에 상처를 입고 좌절감을 느끼기도 합니다. 또한 주변 사람에게 게으른 사람으로 취급을 받기도 합니다. 그래서 주간 졸림증을 가진 환자들은 정상적인 사회생활을 유지하기가 힘든 경우가 흔합니다.</p>

          <p class="txt">주간 졸림증은 기면병을 포함한 여러 가지 수면질환에 의해서 나타날 수 있습니다. </p>

          <p class="txt">주간 졸림증을 일으킬 수 있는 경우는 다음과 같습니다.</p>

          <p class="txt">첫째, <span class="color_red">야간 수면의 양이 부족한 경우</span>로 이 것이 주간 졸림증의 가장 흔한 원인입니다.<br />
          사람에게 필요한 하루 평균 수면의 양은 개개인별로 다르지만 대개 7~8시간 정도입니다. <br />
          시험 공부를 하기 위해 밤을 새면 다음날 매우 졸린 것처럼 야간 수면이 모자라면 낮에 자꾸 졸게 됩니다. 밤을 새는 것은 아니라도 밤에 늦게 자고 아침에 일찍 일어나게 되어 총 수면시간이 4-5시간뿐이 안 되는 경우 만성 수면부족으로 인한 주간 졸림증을 일으킵니다.<br />
          이 경우에는 일단 야간 수면시간을 늘리는 것이 주간 졸림증에 대한 첫 치료입니다. </p>

          <p class="txt">둘째, <span class="color_red">야간 수면의 질이 좋지 않은 경우</span>로 침대에 누워서 잔 시간은 충분하나 어떤 원인으로 인하여 자주 깨어 정상적인 수면구조를 가지지 못 한 경우가 있습니다. 이 때 자다가 깨는 것을 본인이 인식할 수도 있고 그렇지 못 할 수도 있습니다. 본인은 깨었다고 인식하지는 못하나 수면다원검사를 시행해보면 뇌파변화가 3초 이상 존재하는 미세 각성이 잦은 경우가 많이 있습니다. 이런 경우에도 숙면을 취하지 못하여 낮에 졸립게 됩니다. 야간 수면을 방해하는 수면 질환에는 다양한 야간 수면 장애 예를 들어 수면 무호흡증, 주기성 사지 운동증, 하지 불안 증후군 등이 있습니다.</p>

          <p class="txt">셋째, <span class="color_red">중추성의 원인으로 기면병과 특발성 중추성 과다수면증</span>이 이에 해당됩니다. <br />
          주간졸림증에는 위와 같이 여러 가지 원인이 있을 수 있으므로 정확한 진단과 치료를 위해서는 수면 클리닉에서의 자세한 병력 청취 및 수면 다원검사의 시행이 반드시 필요합니다.</p>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>