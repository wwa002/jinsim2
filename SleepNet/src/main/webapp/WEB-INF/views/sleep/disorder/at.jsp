<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 장애 : <span>하지불안증과 주기성 사지 운동증</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 장애</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <div class="box_img mgt0">
            <img src="/resources/images/img_sleep02_06.jpg" alt="">
            <img class="m" src="/resources/images/img_sleep02_06_m.jpg" width="320" alt="">
          </div>
          <p class="txt">이런 증상이 있으십니까?<br />
          이런 증상들은 하지 불안증후군을 경험해 보지 못한 사람에게는 기괴하고 믿을 수 없게 들릴 수 있지만 하지 불안증후군 환자는 실제로 느끼는 증상입니다.</p>

          <h4 class="bulletBox">하지 불안증후군이란 무엇일까요?</h4>
          <p class="txt">하지 불안증후군은 감정적 스트레스나 정신과적인 병에서 오는 증상이 아닙니다. 대부분 다리에서 증상이 발생하지만 팔에도 증상이 발생합니다. 환자들은 호소하는 증상은 서로 다르게 이야기 할 수 있지만, 공통적으로 가만히 앉아 있거나 누어있을 때, 특히 자려고 할 때 다리가 무언가 불편한 느낌, 벌레가 기어다니는 듯한 이상한 느낌이 든다고 호소하며 우리 나라 사람들은 종종 다리가 시리고 저리다고 표현합니다. 이 증상은 밤에 심해집니다. 이 것은 다리에 경련이 생기는 것이나 다리가 눌려 쥐나는 것과는 다릅니다. 또한 당뇨병 환자에게서 흔히 호소하는 바늘로 찌르는 것처럼 따끔거리거나 화끈화끈한 통증과도 차이가 있습니다. 이런 다리의 “불편한” 느낌은 종아리에서 가장 많이 발생하며, 다리를 뻗거나 움직이면 일시적으로 좋아지나 가만히 있게 되면 다시 나타납니다. <br />
          심한 경우에는 통증으로 느끼기도 하며, 이 때문에 잠들기가 어려워 불면증이 생기는 경우도 흔합니다. 다리를 계속 뻗거나 움직여야 증상이 호전되기 때문에 잠들 수 없게 되는 것입니다. 잠을 충분히 못 자기 때문에 환자는 낮 동안에 비정상적으로 피곤하고 졸리게 되며, 일이나 사회생활에 지장을 받게 됩니다.<br />
          낮에 과도하게 졸리는 것만이 하지 불안증후군으로 인하여 낮에 생기는 문제는 아닙니다. 낮 동안에 자동차나 비행기 등의 이동수단을 이용 시 오랜 시간 동안 앉아 있는 것이 힘들며, 영화나 콘서트장 또는 직장의 회의에서도 괴로움을 느끼게 됩니다. 잠을 못 자고 낮 동안의 일상생활에 지장을 받음으로써 신경이 날카로와지고 우울해질 수 있습니다. </p>

          <h4 class="bulletBox">주기성 사지 운동증이란 무엇일까요?</h4>
          <p class="txt">밤 동안에 다리에 생기는 잠을 방해하는 또 다른 질병입니다. 하지 불안증후군은 환자가 깨어 있을 때 다리에 불편한 감각으로 본인이 움직이는 병이지만, 주기성 사지 운동증에서의 다리 움직임은 대부분 환자가 자고 있을 때 발생하며 불수의적으로 발생합니다. 즉 무의식적으로 발생합니다. 그러나 간혹 깨어 있을 때도 다리가 나도 모르게 저절로 움직여 잠드는 것을 방해한다고 합니다. <br />
          주기성 사지 운동증 환자는 대부분 이런 움직임을 모르고 있습니다. 드물게 깨어있을 때 증상이 발생하여 이 같은 불수의적 움직임을 아는 경우도 있습니다. 하지 불안증후군이 있는 대부분의 사람이 주기성 사지 운동증을 호소하지만, 주기성 사지 운동증환자가 하지 불안증후군이 있는 경우는 흔하지 않습니다. 주기성 사지 운동증은 대부분 하지에 증상이 발생하지만 팔에도 증상이 올 수 있습니다. 이름에서 알 수 있듯이 주기성 사지 운동증은 주기적으로 발생합니다. 대부분 30초 단위로 증상이 발생하며, 전형적으로 엄지발가락을 펴지는 동작과 함께 발목, 무릎 또는 고관절을 굽히는 증상이 발생합니다. 이 움직임은 밤에 자다가 움찔하는 동작이나 다리를 차는 동작과 유사 합니다. 이런 증상은 밤 동안 내내 지속되지는 않고, 수면의 전반부에 주로 발생합니다.<br />
          만약 당신이 1시간에 5회 이상 주기성 사지 운동증을 경험한다면, 잠을 계속 자기 어려워 질 수 있습니다. 잠이 들자마자 다리 움직임으로 인해 잠이 깰 수 있으며 이런 경우 환자는 잠을 잤었다는 생각조차 못 하게 됩니다. 이 같은 일 때문에 환자는 실제로는 잠이 들었어도 잠들기 어렵다고 호소하거나 전혀 못 잤다고 호소합니다. <br />
          주기성 사지 운동증 환자의 경우에는 잠을 유지하기 힘들다고 호소하는데, 다리 움직임으로 인해 자주 깨는 “미세각성(매우 짧게 깨는 것)”에 의하여 전체적인 수면의 질이 떨어지게 됩니다. 당신은 아마도 다리의 움직임은 인식하지 못한 채 잠을 잘 못 잤다고만 느낄 수도 있습니다.<br />
          몇몇 사람들은 수면곤란을 느끼지 못 할 수도 있지만, 밤 동안 자주 깸으로서 잠을 방해받고 낮 동안 과도하게 졸리게 됩니다. 이런 사람들은 수면에는 문제가 없다고 얘기하지만, 낮 동안 책을 읽거나 텔레비전을 보거나 운전시에 잠이 들곤 합니다. <br />
          주기성 사지 운동증은 다른 여러 문제들의 원인이 됩니다. 침대에서 같이 자는 사람을 차거나 침대를 흔들어 같이 자는 사람의 수면을 방해하기도 합니다.</p>

          <h4 class="bulletBox">하지 불안증후군과 주기성 사지 운동증은 얼마나 흔할까요?</h4>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 하지 불안증후군</dt>
            <dd>100명당 5명에서 10명까지 일생동안 하지 불안증후군을 경험하게 됩니다. 노인에게 흔하지만 어느 나이에나 발생할 수 있으며 성별에 따른 차이는 없습니다. 최근에서 우리나라 수면 학회에서 시행한 연구에 의하면 100명당 7.5명의 비율로 생긴다고 하였습니다. 하지 불안증후군은 임신시 심해질 수 있으며 특히 마지막 여섯달 동안 심해집니다. 수년이 경과함에 따라 증상은 특별한 원인이 없이 심해졌다 호전되었다 하는 경과를 반복합니다. 빈혈이 생긴 경우에는 증상이 급격히 심해질 수 있습니다. </dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 주기성 사지 운동증</dt>
            <dd>주기성 사지 운동증은 30세 이하의 젋은 나이에서는 드물지만 나이가 들면서 늘어나게 됩니다. 30세에서 50세에서는 비교적 적은 수에서만이 증상을 경험하는 반면 50세에서 65세까지는 3분의 1에서, 65세 이상에서는 반 수에서 주기성 사지 운동증을 가지게 됩니다. 성별에 따른 차이는 없으며 불면증이 있는 환자 10명중 2명에서 주기성 사지 운동증이 있는 것으로 알려져 있습니다.</dd>
          </dl>

          <h4 class="bulletBox">이 병들의 원인은 무엇일까요?</h4>
          <p class="txt">사실 하지 불안증후군과 주기성 사지 운동증의 확실한 원인은 밝혀져 있지 않지만 고려해 보아야 할 여러 주변 환경과 유전적인 소인들이 있습니다.</p>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 하지 불안증후군의 원인</dt>
            <dd>하지 불안증후군 환자의 30%에서 유전적인 소인이 있습니다. 이 말은 100명중 30명에서 아버지나 어머니가 동일한 증상을 가지고 있다는 말입니다. 나머지 70%에서 원인은 밝혀져 있지 않습니다. 하지 불안증후군은 세대로 유전되어 갈수록 증상이 심해지게 되며, 더욱 치료하기 어려워지는 경향을 보입니다. <br />여러 가지 상황들이 하지 불안증후군과 관련되어 있거나 유발할 수 있습니다. 철분 결핍성 빈혈, 하지 혈류장애, 신경병, 근육병, 신장 장애, 음주, 비타민이나 미네랄 결핍등이 하지 불안증후군과 관련 있는 것으로 알려져 있습니다. 다른 원인으로는 특정한 약물을 끊거나 시작했을 때, 카페인, 담배, 피로, 고온, 추위에 장시간 노출들이 관련되어 있습니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 주기성 사지 운동증의 원인</dt>
            <dd>하지 불안증후군과 같은 원인들이 주기성 사지 운동증을 유발합니다. 주기성 사지 운동증은 신장이 좋지 않거나 기면병이 있는 사람에게서 흔합니다. 일부 항우울제는 주기성 사지 운동 발생률을 증가시킵니다.</dd>
          </dl>

          <h4 class="bulletBox">진단</h4>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 하지 불안증후군</dt>
            <dd>특정한 치료에 대하여 기술하기 전에, 당신의 주치의는 당신이 하지 불안증후군이 있다는 확신이 있어야 합니다. 수면에 관하여 전문의의 상담이 필요합니다. 하지 불안증후군은 당신이 표현하는 특징적인 증상들로 인해 진단되는 경우가 많습니다. 이런 증상들은 벌레가 기어가는 느낌이라던지 근질근질한 느낌, 당기는 느낌 등으로 표현입니다. 이런 느낌들로 인해 다리를 움직여야 하게 됩니다.<br />
            하지 불안증후군을 진단하는 또 다른 증상은 움직이는 것이 증상호전에 도움을 준다는 것입니다. 하지만 하지 불안 증후군과 유사한 다른 병을 감별하게 위해 자세한 문진과 신체 검진이 필요하게 됩니다.<br />
            또한 철분 결핍성 빈혈이나 신장의 이상, 비타민 문제등의 원인이 동반되어 있는지 혈액검사를 시행하게 되고 필요시 신경전도검사등을 시행하기도 합니다. </dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 주기성 사지 운동증인</dt>
            <dd>주기성 사지 운동증은 적절한 진단을 내리기 위해서는 수면 검사가 필요하게 됩니다. 주기성 사지 운동증 환자는 밤 동안의 움직임을 인지하지 못 하며 낮 동안의 과도한 졸리움이나 얕은 잠, 숙면을 취하지 못하는 증상을 호소합니다. 만약 증상이 의심된다면 주기성 사지 운동증을 진단하기 위하여 하룻밤을 검사실에서 자면서 당신의 수면을 체크하는 수면검사가 필요합니다.</dd>
          </dl>

          <h4 class="bulletBox">그렇다면 이 병들은 어떻게 치료가 될까요?</h4><br />
          <p class="txt">하지 불안증후군이나 주기성 사지 운동증 치료의 첫 단계는 질병에 영향을 줄 수 있는 연관된 상황들(예를 들면, 철 결핍성 빈혈, 당뇨, 관절염, 항우울제 사용 등)에 관해 조사해 보아야 합니다. 때때로 이런 상황들에 관해 적절한 진단을 하고 치료하는 것이 하지 불안증후군이나 주기성 사지 운동증의 증상 조절에 도움을 줍니다. 철분이 부족한 경우에는 철분을 보충하는 것이 필요합니다. <br />
          원인에 대한 치료를 받은 경우에도 많은 환자들이 여전히 불편을 호소합니다. 그 때는 적절한 약물을 선택하여 투약하게 됩니다.</p>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">1</span> 하지 불안증후군의 치료</dt>
            <dd>몇몇 사람들에게는 다음과 같은 가정요법들이 도움을 줄 수 있습니다: 뜨거운 목욕, 다리 마사지, 온열 요법, 얼음팩, 아스피린이나 다른 진통제, 정기적인 운동, 카페인 안 먹기. 비타민E나 칼슘 보충이 몇몇 사람에게서 증상을 호전시키지만 이 같은 치료들이 효과가 있다는 연구는 없습니다. 가정요법이 효과 없을 경우, 하지 불안증후군은 병원에서 처방된 약에 의해 조절될 수 있습니다. 사람마다 맞는 약이 다를 수 있으므로, 오랜 시간에 걸쳐 여러 가지 약으로 증상조절을 시도해 볼 수 있습니다. 약의 효과는 질병의 심각한 정도, 다른 동반 질환, 현재 복용중인 약물들에 의해 달라질 수 있습니다.<br />약을 복용하는 경우 대부분 증상이 호전됩니다.</dd>
          </dl>
          <dl class="txt define_line">
            <dt><span class="bullet_round_blue">2</span> 주기성 사지 운동증</dt>
            <dd>주기성 사지 운동증이 있는 많은 사람에게서 낮 동안의 과도한 졸림증 같은 일상생활을 방해하는 증상이 없다면 치료가 필요치 않습니다. 주기성 사지 운동증이 있는 환자 중 얕게 잠이 들거나 조그만 움직임에도 쉽게 잠이 깨는 사람들은 아래에 있는 잠을 잘 자기 위한 원칙을 지키신다면 수면에 도움을 받으실 수 있습니다. 움직임에 의해 잠이 자주 깨는 경우 하지 불안 증후군에서 치료로 쓰여지는 약이 역시 주기성 사지 운동증에도 효과가 있습니다.</dd>
          </dl>

        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>