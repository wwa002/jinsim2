<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>자가진단</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;자가진단</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <!-- 자가진단 -->
        <div class="btns_selfTest">
          <a href="#" class="btn_selfTest01" onclick="javascript:openPopup('self01'); return false;">주간졸림증<br />자가진단</a>
          <a href="#" class="btn_selfTest02" onclick="javascript:openPopup('self02'); return false;">불면증<br />자가진단</a>
          <a href="#" class="btn_selfTest03" onclick="javascript:openPopup('self03'); return false;">하지불안증후군<br />자가진단</a>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

    <!-- 주간졸림증 자가진단 -->
    <div class="popup pop_self" id="self01">
      <div class="popup_inner">
        <p class="tit_test">주간 졸림증 자가진단</p>
        <p class="desc">아래의 상황들에서 당신은 어느 정도나 졸음을 느끼십니까?</p>
        <div class="test_guide">
          <p>졸리운 정도</p>
          <ul>
            <li>0점 – 전혀 졸지 않는다.</li>
            <li>1점 - 가끔 졸음에 빠진다.</li>
            <li>2점 - 종종 졸음에 빠진다.</li>
            <li>3점 - 자주 졸음에 빠진다.</li>
          </ul>
        </div>
        <p class="addInfo">* 전부 체크하시면 결과가 나타납니다.</p>
        <table class="tbl_test">
          <colgroup>
            <col />
            <col style="width:5%;" />
            <col style="width:5%;" />
            <col style="width:5%;" />
            <col style="width:5%;" />
          </colgroup>
          <thead>
            <tr>
              <th rowspan="2">활 동</th>
              <th colspan="4">졸리운 정도</th>
            </tr>
            <tr>
              <th>0</th>
              <th>1</th>
              <th>2</th>
              <th>3</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>앉아서 책을 읽을때</td>
              <td><input type="radio" name="q01" value="0"/></td>
              <td><input type="radio" name="q01" value="1" /></td>
              <td><input type="radio" name="q01" value="2" /></td>
              <td><input type="radio" name="q01" value="3"/></td>
            </tr>
            <tr>
              <td>텔레비전을 볼 때</td>
              <td><input type="radio" name="q02" value="0" /></td>
              <td><input type="radio" name="q02" value="1" /></td>
              <td><input type="radio" name="q02" value="2" /></td>
              <td><input type="radio" name="q02" value="3" /></td>
            </tr>
            <tr>
              <td>극장이나 회의석상과 같은 공공장소에서 가만히 앉아있을 때</td>
              <td><input type="radio" name="q03" value="0" /></td>
              <td><input type="radio" name="q03" value="1" /></td>
              <td><input type="radio" name="q03" value="2" /></td>
              <td><input type="radio" name="q03" value="3" /></td>
            </tr>
            <tr>
              <td>오후 휴식시간에 편안히 누워 있을 때</td>
              <td><input type="radio" name="q04" value="0" /></td>
              <td><input type="radio" name="q04" value="1" /></td>
              <td><input type="radio" name="q04" value="2" /></td>
              <td><input type="radio" name="q04" value="3" /></td>
            </tr>
            <tr>
              <td>앉아서 누군가에게 말을 하고 있을 때</td>
              <td><input type="radio" name="q05" value="0" /></td>
              <td><input type="radio" name="q05" value="1" /></td>
              <td><input type="radio" name="q05" value="2" /></td>
              <td><input type="radio" name="q05" value="3" /></td>
            </tr>
            <tr>
              <td>점심식사 후 조용히 앉아 있을 때</td>
              <td><input type="radio" name="q06" value="0" /></td>
              <td><input type="radio" name="q06" value="1" /></td>
              <td><input type="radio" name="q06" value="2" /></td>
              <td><input type="radio" name="q06" value="3" /></td>
            </tr>
            <tr>
              <td>차를 운전하고 가다가 교통체증으로 몇 분간 멈추었을 때</td>
              <td><input type="radio" name="q07" value="0" /></td>
              <td><input type="radio" name="q07" value="1" /></td>
              <td><input type="radio" name="q07" value="2" /></td>
              <td><input type="radio" name="q07" value="3" /></td>
            </tr>
          </tbody>
        </table>
        <div class="result"><span>결과 : 총 <strong class="resutl_point" id="jol_result">0</strong>점</span> 이므로 <strong class="result_txt" id="jol_result_txt">정상범위 입니다</strong>.</div>
        <div class="result_guide">
          <p>결과치</p>
          <ul>
            <li><strong>&lt; 10 :</strong> 정상범위</li>
            <li><strong>≥ 10 :</strong> 주간 졸림증이 있음</li>
            <li><strong>14~18 :</strong> 중등도의 주간 졸림증</li>
            <li><strong>≥ 19 :</strong> 심한 주간졸림증이 있음</li>
          </ul>
        </div>
        <a href="#" class="close_popup">닫기 <img src="/resources/images/close_self.png" alt=""></a>
      </div>
    </div>

	<script>
	
	$('input[type=radio][name^=q0]').click(function(){
		var sum = 0;
		$('input[type=radio][name^=q0]:checked').each(function(){
			sum += Number($(this).val());
		});
		$('#jol_result').text(sum);
		if(sum < 10)
		{
			$('#jol_result_txt').text('정상범위 입니다');
		}
		else if(sum >= 10 && sum <14)
		{
			$('#jol_result_txt').text('주간 졸림증이 있습니다');
		}
		else if(sum >= 14 && sum <19)
		{
			$('#jol_result_txt').text('중등도의 주간 졸림증이 있습니다');
		}
		else if(sum > 19)
		{
			$('#jol_result_txt').text('심한 주간 졸림증이 있습니다');
		}
	});
	
	</script>


    <!-- end 주간졸림증 자가진단 -->

    <!-- 불면증 자가진단 -->
    <div class="popup pop_self" id="self02">
      <div class="popup_inner">
        <p class="tit_test">불면증 자가진단</p>
        <div class="test_guide">
          <ul>
            <li>없다-0점</li>
            <li>약간정도-1점</li>
            <li>중간정도-2점</li>
            <li>심하다-3점</li>
            <li>매우 심하다-4점</li>
          </ul>
        </div>
        <p class="addInfo">* 전부 체크하시면 결과가 나타납니다.</p>

        <p class="question">1. 당신의 불면증에 관한 문제들의 현재(최근2주간) 심한 정도를 표시해 주세요.</p>
        <table class="tbl_test">
          <colgroup>
            <col />
            <col style="width:7%;" />
            <col style="width:7%;" />
            <col style="width:7%;" />
            <col style="width:7%;" />
            <col style="width:10%;" />
          </colgroup>
          <thead>
            <tr>
              <th>&nbsp;</th>
              <th>없음</th>
              <th>약간</th>
              <th>중간</th>
              <th>심함</th>
              <th>매우심함</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>a. 잠들기 어렵다.</td>
              <td><input type="radio" name="self02_q01_01" value="0" /></td>
              <td><input type="radio" name="self02_q01_01" value="1" /></td>
              <td><input type="radio" name="self02_q01_01" value="2" /></td>
              <td><input type="radio" name="self02_q01_01" value="3" /></td>
              <td><input type="radio" name="self02_q01_01" value="4" /></td>
            </tr>
            <tr>
              <td>b. 잠을 유지하기 어렵다.</td>
              <td><input type="radio" name="self02_q01_02" value="0" /></td>
              <td><input type="radio" name="self02_q01_02" value="1" /></td>
              <td><input type="radio" name="self02_q01_02" value="2" /></td>
              <td><input type="radio" name="self02_q01_02" value="3" /></td>
              <td><input type="radio" name="self02_q01_02" value="4" /></td>
            </tr>
            <tr>
              <td>c. 쉽게 깬다.</td>
              <td><input type="radio" name="self02_q01_03" value="0" /></td>
              <td><input type="radio" name="self02_q01_03" value="1" /></td>
              <td><input type="radio" name="self02_q01_03" value="2" /></td>
              <td><input type="radio" name="self02_q01_03" value="3" /></td>
              <td><input type="radio" name="self02_q01_03" value="4" /></td>
            </tr>
          </tbody>
        </table>

        <p class="question">2. 현재 수면 양상에 관하여 얼마나 만족하고 있습니까?</p>
        <ul class="distractor">
          <li><label for="q02_a01">매우만족</label><input type="radio" id="q02_a01" name="self02_q02" value="0"></li>
          <li><label for="q02_a02">약간만족</label><input type="radio" id="q02_a02" name="self02_q02" value="1"></li>
          <li><label for="q02_a03">그저그렇다</label><input type="radio" id="q02_a03" name="self02_q02" value="2"></li>
          <li><label for="q02_a04">약간불만족</label><input type="radio" id="q02_a04" name="self02_q02" value="3"></li>
          <li><label for="q02_a05">매우불만족</label><input type="radio" id="q02_a05" name="self02_q02" value="4"></li>
        </ul>

        <p class="question">3. 당신의 수면 장애가 어느 정도나 당신의 낮 활동을 방해 한다고 생각합니까?<br />(예. 낮에 피곤함, 직장이나 가사에 일하는 능력, 집중력, 기억력, 기분, 등).</p>
        <ul class="distractor">
          <li><label for="q03_a01">전혀 방해되지 않는다</label><input type="radio" id="q03_a01" name="self02_q03" value="0"></li>
          <li><label for="q03_a02">약간</label><input type="radio" id="q03_a02" name="self02_q03" value="1"></li>
          <li><label for="q03_a03">다소</label><input type="radio" id="q03_a03" name="self02_q03" value="2"></li>
          <li><label for="q03_a04">상당히</label><input type="radio" id="q03_a04" name="self02_q03" value="3"></li>
          <li><label for="q03_a05">매우많이</label><input type="radio" id="q03_a05" name="self02_q03" value="4"></li>
        </ul>

        <p class="question">4. 불면증으로 인한 장애가 당신의 삶의 질의 손상정도를 다른 사람들에게 어떻게 보인다고 생각합니까?</p>
        <ul class="distractor">
          <li><label for="q04_a01">전혀 방해되지 않는다</label><input type="radio" id="q04_a01" name="self02_q04" value="0"></li>
          <li><label for="q04_a02">약간</label><input type="radio" id="q04_a02" name="self02_q04" value="1"></li>
          <li><label for="q04_a03">다소</label><input type="radio" id="q04_a03" name="self02_q04" value="2"></li>
          <li><label for="q04_a04">상당히</label><input type="radio" id="q04_a04" name="self02_q04" value="3"></li>
          <li><label for="q04_a05">매우많이</label><input type="radio" id="q04_a05" name="self02_q04" value="4"></li>
        </ul>

        <p class="question">5. 당신은 현재 불면증에 관하여 얼마나 걱정하고 있습니까?</p>
        <ul class="distractor">
          <li><label for="q05_a01">전혀그렇지않다</label><input type="radio" id="q05_a01" name="self02_q05" value="0"></li>
          <li><label for="q05_a02">약간만족</label><input type="radio" id="q05_a02" name="self02_q05" value="1"></li>
          <li><label for="q05_a03">그저그렇다</label><input type="radio" id="q05_a03" name="self02_q05" value="2"></li>
          <li><label for="q05_a04">약간불만족</label><input type="radio" id="q05_a04" name="self02_q05" value="3"></li>
          <li><label for="q05_a05">매우불만족</label><input type="radio" id="q05_a05" name="self02_q05" value="4"></li>
        </ul>

        <div class="result"><span>결과 : 총 <strong class="resutl_point" id="bul_result">0</strong>점</span> 이므로 <strong class="result_txt" id="bul_result_txt">유의할만한 불면증이 없습니다</strong>.</div>
        <div class="result_guide">
          <p>결과치</p>
          <ul>
            <li><strong>0-7 :</strong> 유의할 만한 불면증이 없습니다.</li>
            <li><strong>8-14 :</strong> 약간의 불면증 경향이 있습니다.</li>
            <li><strong>15~21 :</strong> 중등도의 불면증이 있습니다.</li>
            <li><strong>22-28 :</strong> 심한 불면증이 있습니다.</li>
          </ul>
        </div>
        <a href="#" class="close_popup">닫기 <img src="/resources/images/close_self.png" alt=""></a>
      </div>
    </div>

	<script>
	
	$('input[type=radio][name^=self02]').click(function(){
		var sum = 0;
		$('input[type=radio][name^=self02]:checked').each(function(){
			sum += Number($(this).val());
		});
		$('#bul_result').text(sum);
		if(sum < 8)
		{
			$('#bul_result_txt').text('유의할만한 불면증이 없습니다');
		}
		else if(sum >= 8 && sum <15)
		{
			$('#bul_result_txt').text('약간의 불면증 경향이 있습니다');
		}
		else if(sum >= 15 && sum <21)
		{
			$('#bul_result_txt').text('중등도의 불면증이 있습니다');
		}
		else if(sum > 21)
		{
			$('#bul_result_txt').text('심한 불면증이 있습니다');
		}
	});
	
	</script>
    <!-- end 불면증 자가진단 -->

    <!-- 하지불안증후군 자가진단 -->
    <div class="popup pop_self" id="self03">
      <div class="popup_inner">
        <p class="tit_test">자기 전 다리가 저리십니까?</p>
        <div class="txt"><strong class="bg01">1</strong><p>다리에 불편하고 불쾌한 감각이 동반되거나 이 감각없이 다리를 움직이고 싶은 충동이 있다. 주로 다리에 있지만 다리 부분에 더해 팔이나 다른 부분에서도 나타난다.</p></div>
        <div class="txt"><strong class="bg02">2</strong><p>움직이고자 하는 충동이나 불쾌한 감각들이 눕거나 앉아 있는 상태 즉, 쉬거나 활동을 하지 않을 때만 생기거나 그때 심해진다.</p></div>
        <div class="txt"><strong class="bg03">3</strong><p>움직이고자 하는 충동이나 불쾌한 감각들이 걷거나 스트레칭과 같은 운동에 의하여, 지속적으로 움직이고 있는 동안은 부분적으로 또는 거의 모두 완화된다.</p></div>
        <div class="txt"><strong class="bg04">4</strong><p>움직이고자 하는 충동이나 불쾌한 감각들이 낮보다는 저녁이나 밤에 악화되거나 밤에만 나타난다.</p></div>
        <p class="conclusion">위의 4가지에 모두 해당되신다면<br/>당신은 <strong>하지불안증후군</strong>을 가지고 있는 것입니다.</p>
        <a href="#" class="close_popup">닫기 <img src="/resources/images/close_self.png" alt=""></a>
      </div>
    </div>
    <!-- end 하지불안증후군 자가진단 -->


<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>