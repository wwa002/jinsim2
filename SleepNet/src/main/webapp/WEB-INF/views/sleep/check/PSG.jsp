<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 검사 : <span>수면다원검사</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 검사</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <p class="txt mgt0">수면장애의 진단에 가장 도움이 되는 검사가 수면다원검사 입니다. 수면다원검사는 수면뿐만 아니라 수면 중 여러 가지 중요한 신체기능을 검사합니다. 몸에 각종 센서를 부착하고 잠을 자게 됩니다. 수면단계를 측정하는 뇌파전극을 머리에 부착하고, 호흡 정도를 측정하기 위해 코, 가슴, 복부에 센서와 벨트를 두릅니다. 눈 움직임, 팔다리 움직임, 턱 근육의 긴장도를 측정하는 전극을 부착하고, 가슴에 심전도 전극을 부착합니다. 그런 다음 수면을 취하게 됩니다. </p>

          <h4 class="bulletBox">야간 수면다원검사 (밤잠 검사): night polysomnography</h4>
          <ul class="txt indent_list_bullet">
            <li><span class="bullet_round_blue">1</span> 머리에 붙인 전극에 의하여 수면의 양과 질을 검사하여 수면의 각 단계별로 자는 시간을 분석하여 얼마나 잘 자는지를 알아봅니다.</li>
            <li><span class="bullet_round_blue">2</span> 수면 중 호흡량을 측정하여 수면무호흡 또는 저호흡의 횟수와 정도를 파악합니다.</li>
            <li><span class="bullet_round_blue">3</span> 심전도로 수면중 심장의 활동을 모니터한다.</li>
            <li><span class="bullet_round_blue">4</span> 수면 중 호흡을 위한 가슴과 배의 운동을 측정하는데 이것은 수면 무호흡증의 형태를 진단하는데 필요하며, 호흡장애 시 환자가 겪는 스트레스의 지표가 됩니다.</li>
            <li><span class="bullet_round_blue">5</span> 수면 중 혈중 산소량을 지속적으로 모니터한다.</li>
            <li><span class="bullet_round_blue">6</span> 수면 중 코골이 정도를 측정한다.</li>
            <li><span class="bullet_round_blue">7</span> 다리근육의 근전도전극을 통하여 수면 중 다리 움직임 및 이상행동을 관찰한다.</li>
            <li><span class="bullet_round_blue">8</span> 몸의 위치를 감지하는 센서로 잠을 잘 때 어떻게 누워 있는지가 수면장애와 관련이 있습니다. 여러 가지의 몸 위치별로 잠잔 시간을 산출하는 것이 필요합니다.</li>
            <li><span class="bullet_round_blue">9</span> CPAP 압력검사를 시행한다 : 지속적 상기도양압술(CPAP : Continuous Positive Airway Pressure)은 수면 무호흡의 가장 좋은 치료법입니다. 수면 검사중에 무호흡증이 있거나 코골이가 심한 경우에는 CPAP 수면 다원수면를 다시 시행하여 치료에 알맞은 압력을 찾아줍니다.</li>
            <li><span class="bullet_round_blue">10</span> 수면 무호흡증인 경우 구강내 기구의 치료효과를 검사한다 : 일부 수면 무호흡증 환자는 간단한 구강내 기구(dental appliance)를 착용하면 치료가 되기도합니다. 따라서, 수면다원검사 후반부에 이 기구를 착용하게 하여 치료효과가 있는지를 검사합니다.<br />
            이상과 같은 여러 가지 검사를 한밤 내내 시행하고, 그 후 정확한 판독과 분석을 하여 다음 번 외래에 방문하셨을 때 결과를 알려 드립니다. 수면다원검사의 결과는 수면의 질과 잠자는 동안 발생하는 모든 신체의 문제를 알려주며, 수면장애의 정확한 진단과 치료는 물론, 관련된 다른 질환(고혈압, 뇌졸중, 심장병, 당뇨병, 우울증 등)의 치료와 예방에도 크게 도움을 줍니다.</li>
          </ul>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>