<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">수면 정보</h2>
        <div class="right">
          <h3>수면 검사 : <span>다중입면잠복기 검사</span></h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 정보&nbsp;&nbsp;&gt;&nbsp;&nbsp;수면 검사</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="mBox">
          <h4 class="bulletBox mgt0">다중 입면 잠복기검사 - 낮잠검사 (MSLT: Multiple Sleep Latency Test)</h4>
          <p class="txt">기면병 (narcolepsy)과 특발성 수면과다증(idiophatic hypersomnia)의 진단과 치료경과 판정에 꼭 필요한 검사이며, 수면 무호흡증이나 기타 불면증의 주간 졸리움을 평가하는데도 필요합니다. 반복적 입면잠복기검사(Multiple Sleep Latency Test )는 전날밤에 시행한 수면다원검사를 통해 밤 동안의 수면 양상을 분석한 후, 아침부터 2시간 간격으로 4-5회에 걸쳐서 30분씩 낮잠에 빠져들어가는 속도를 검사합니다. 여기서 잠이 든 후 15분 이내에 2번 이상 렘수면이 발생하면 기면증의 가능성이 높습니다. 또한 평균수면잠복기(sleep latency)에 따라서 주간에 졸리운 정도를 객관적으로 평가할 수 있습니다.</p>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>