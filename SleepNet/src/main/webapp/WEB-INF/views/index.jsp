<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header-index.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <!-- visual -->
    <div class="main_top">
      <div class="inner">
        <div class="carousel">
          <div class="mainTop_slider owl-carousel owl-theme">
            <div class="item"><img src="/resources/images/main_carousel01.png" alt=""></div>
            <div class="item"><a href="#"><img src="/resources/images/main_carousel02.png" alt=""></a></div>
          </div>
        </div>
        <script>
        $('.mainTop_slider').owlCarousel({
            loop:true,
            nav:true,
            items:1,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true
        })
        </script>
        <div class="top_link">
          <div class="links owl-carousel owl-theme">
            <div class="item"><a href="#">수면 장애</a></div>
            <div class="item"><a href="#">불면증/졸림증<br />자가진단</a></div>
            <div class="item"><a href="#">하지불안증후군<br />진단기준</a></div>
            <div class="item"><a href="#">건강한 수면을<br />위한 10계명</a></div>
            <div class="item"><a href="#">수면 FAQ</a></div>
            <div class="item"><a href="#">수면 클리닉<br />병원 검색</a></div>
          </div>
        </div>
        <!-- //top_link -->
        <script>
        var win_width = $(window).width();
        initToplink();
        $(window).resize(function(){
          win_width = $(window).width();
          initToplink();
        })

        function initToplink(){
          var $link = $('.links');
          if(win_width<=767){
            $link.owlCarousel({
              items:3,
              nav:true
            })
          }else{
            $link.owlCarousel('destroy');
          }
        }
        </script>
      </div>
      <!-- //inner end -->
    </div>
    <!-- //main top end -->

    <!-- recently board -->
    <div class="recentlyBoard">
      <div class="inner">
        <div class="box_board notice">
          <p class="tit_board">NOTICE &amp; NEWS</p>
          <ul>
            <li><span class="ellipsis"><img src="/resources/images/ico_moon.gif" alt=""> <a href="#">[대한뇌전증학회] 제22차 대한뇌전증학회대한뇌전증학회대한뇌전증학회</a></span>2017.06.20</li>
            <li><span class="ellipsis"><img src="/resources/images/ico_moon.gif" alt=""> <a href="#">[대한뇌전증학회] 제22차 대한뇌전증학회</a></span>2017.06.20</li>
            <li><span class="ellipsis"><img src="/resources/images/ico_moon.gif" alt=""> <a href="#">[대한뇌전증학회] 제22차 대한뇌전증학회</a></span>2017.06.20</li>
            <li><span class="ellipsis"><img src="/resources/images/ico_moon.gif" alt=""> <a href="#">[대한뇌전증학회] 제22차 대한뇌전증학회</a></span>2017.06.20</li>
            <li><span class="ellipsis"><img src="/resources/images/ico_moon.gif" alt=""> <a href="#">[대한뇌전증학회] 제22차 대한뇌전증학회</a></span>2017.06.20</li>
          </ul>
          <a href="#" class="btn_more"><img src="/resources/images/ico_more.gif" alt=""></a>
        </div>

        <div class="box_board event">
          <p class="tit_board">학술행사</p>
          <ul>
            <li><a href="#" class="ellipsis">[대한뇌전증학회] 제22차 대한뇌전증학회대한뇌전증학회대한뇌전증학회</a>2017.06.30~7.1</li>
            <li><a href="#" class="ellipsis">[대한뇌전증학회] 제22차 대한뇌전증학회</a>2017.06.30~7.1</li>
            <li><a href="#" class="ellipsis">[대한뇌전증학회] 제22차 대한뇌전증학회</a>2017.06.30~7.1</li>
          </ul>
          <a href="#" class="btn_more"><img src="/resources/images/ico_more.gif" alt=""></a>
        </div>

        <div class="jounal">
          <a href="#">Journal Search</a>
          <a href="#">E-submission</a>
        </div>
      </div>
    </div>

    <!-- link -->
    <div class="main_link">
      <div class="inner">
        <a href="#">자료실<span>학술대회/보수교육</span></a>
        <a href="#">회원가입</a>
        <a href="#">회비납부</a>
        <a href="#">학회갤러리</a>
        <a href="#">회원게시판</a>
        <a href="#">회원논문검색</a>
      </div>
    </div>

    <!-- banner -->
    <div></div>

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>