<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">학회소개</h2>
        <div class="right">
          <h3>회칙</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;학회소개&nbsp;&nbsp;&gt;&nbsp;&nbsp;회칙</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>      
      <!-- //lnb end -->

      <div class="right">
        <div class="ver">
          2004년 06월 27일 제정<br />
          2013년 06월 29일 개정<br />
          2016년 11월 10일 개정<br />
          2017년 06월 15일 개정
        </div>
        <h3 class="bulletBox">제 1 장 총 칙</h3>
        <div class="txt_rule"><strong class="color_blue">제1조 (명칭)</strong> 본 학회는 대한수면연구학회(Korean Sleep Research Society, KSRS, 이하 “학회”라 한
        다)라 칭한다.</div>

        <div class="txt_rule"><strong class="color_blue">제 2 조 (목적)</strong> 본 학회는 수면의학의 발전과 회원의 권익보호 및 친목도모를 목적으로 한다.</div>

        <div class="txt_rule"><strong class="color_blue">제 3 조 (사업)</strong> 본 학회는 다음과 같은 사업을 수행한다.<br />
        1. 수면의학의 발전을 위한 학술모임(학술대회, 보수교육, 워크샵, 집담회 등)을 주관한다.<br />
        2. 회원들의 관심이 있는 연구 과제를 계획, 지원하고 수행한다.<br />
        3. 학술지 발간 및 기타 도서 간행을 한다.<br />
        4. 국제학술회의 및 인적 교류를 통하여 세계화를 추구한다.<br />
        5. 회원간의 정보 교류와 친목을 도모한다.<br />
        6. 공동의 목표를 가진 타 학회나 연구 단체들과의 교류증진을 위해 노력한다.<br />
        7. 정부 및 관계 당국과의 협의 등을 통해 우리나라 수면의학의 질 향상을 꾀한다.<br />
        8. 의료인 및 일반인을 상대로 수면의학 분야에 관하여 홍보 활동과 교육을 실시한다.<br />
        9. 기타 목적달성을 위한 업무를 수행한다.
        </div>

        <h3 class="bulletBox">제 2 장 회 원</h3>
        <div class="txt_rule"><strong class="color_blue">제 4 조 (회원의 구성)</strong> 본 학회의 회원구성은 정회원, 준회원, 일반회원 및 특별회원으로 한다.<br />
        1. 정회원: 수면의학 영역의 연구 및 진료를 수행하고 있으며 본 학회의 설립목적에
        찬동하는 관련 분야의 전문의로 한다.<br />
        2. 준회원: 수면의학 관련 분야의 수련과정에 있는 자로 한다.<br />
        3. 일반회원: 수면의학 연구와 진료에 관심을 가지고 본 학회의 목적에 찬동하는 전문가
        또는 기타 의료 분야 종사자로 한다.<br />
        4. 특별회원: 본 학회의 취지에 찬동하고 소정의 회비를 납부하는 개인 또는 법인으로서
        이사회에서 승인 받은 자로 한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 5 조 (입회)</strong> 본 학회에 입회하고자 하는 자는 입회원서, 소정의 입회비 및 초년도 회비를 제출
        및 납부하여야 하며, 각 회원의 자격은 이사회의 사후 승인을 받아야 한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 6 조 (권리와 의무)</strong><br />
        1. 본 학회의 정회원은 선거권과 소정의 의결권이 있다.<br />
        2. 본 학회의 모든 회원은 매년 소정의 연회비를 납부하여야 한다<br />
        3. 당해 년도 1 월 1 일을 기준으로 만 65 세 이상 회원은 해당연도 및 그 이후의 연회비를
        면제한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 7 조 (자격정지)</strong> 본 학회 회원으로서 정당한 이유 없이 연회비를 납부하지 않은 자는 회원의
        자격을 상실한다.<br />
        1. 회원이 사망하거나, 자진탈퇴를 요청하거나, 이사회에서 제명이 결정되는 경우에
        회원자격을 상실한다.<br />
        2. 회원이 학회의 명예와 이익에 반하는 행위를 한 경우 이사회의 결정으로 제명할 수 있다.<br />
        3. 본 학회에서 제명된 회원은 기 납부한 학회 관련 회비에 대하여 반환 청구를 할 수 없다
        </div>

        <h3 class="bulletBox">제 3 장 임원의 임기 및 임무</h3>
        <div class="txt_rule"><strong class="color_blue">제 8 조 (임 원)</strong> 본 학회는 다음의 임원을 둔다.<br />
        1. 회장: 1 명<br />
        2. 부회장: 1 명<br />
        3. 이사: 25 명 이내<br />
        4. 감사: 2 명 이내<br />
        5. 고문: 10 명이내
        </div>

        <div class="txt_rule"><strong class="color_blue">제 9 조 (임원의 임무)</strong><br />
        1. 회장은 본 학회를 대표하고 총회와 이사회, 평의원회의 의장이 되며, 학술대회를 주관한다.<br />
        2. 부회장은 총회, 이사회에서 회장을 보좌하며 회장의 유고 시에 그 임무를 대행한다.<br />
        3. 이사는 회장을 보좌하여 소정의 업무를 담당한다.<br />
        4. 감사는 본 학회의 업무 및 재정을 감사하며 이를 총회에 보고한다.<br />
        5. 고문은 본 학회의 제반 활동에 관한 자문 역할을 수행한다.<br />
        6. 정회원만이 이사로 선출될 수 있다<br />
        7. 회장, 부회장, 이사는 의결권이 있고, 감사와 고문은 의결권이 없다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 10 조 (임원의 임기)</strong><br />
        1. 회장의 임기는 2 년으로 한다.<br />
        2. 부회장, 이사, 감사, 고문의 임기는 2 년으로 한다.<br />
        3. 보선된 임원의 임기는 전임자의 잔여기간으로 한다.<br />
        4. 새 임원의 임기의 시작은 9 월 1 일로 한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 11 조 (임원의 선출)</strong><br />
        1. 회장은 평의원회에서 선출하고 총회에 보고한다.<br />
        2. 부회장과 이사는 회장이 임명한다.<br />
        3. 감사는 총회에서 선출한다<br />
        4. 고문은 전임회장 및 이사회에서 추대된 자로 구성되며 회장이 위촉한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 12 조 (임원의 업무)</strong><br />
        1. 기획의사: 본회 발전을 위해 장단기 계획 수립 담당.<br />
        2. 법제이사: 정관, 세칙 등의 개정과 신규 입안. 학회운영과 관련된 법적 자문.<br />
        3. 총무이사: 본회의 회무를 총괄하며, 이사회 및 총회의 의결사항을 실행하며, 회원에 관한
        기록과 모든 의사록을 관리하고 행정업무를 수행<br />
        4. 학술이사: 학술대회, 집담회 등 각종 학술행사 업무를 담당한다.<br />
        5. 대외협력이사: 타 학회, 타 단체와의 연관 업무 수행<br />
        6. 재무이사: 일반 회계 업무를 관장.<br />
        7. 편집이사: 본회 학술지 발간 및 본 학회의 모든 간행물의 출간에 관한 업무 담당.<br />
        8. 국제이사: 해외관련 정보 수집 및 교류 담당.<br />
        9. 교육이사: 보수교육, 워크샵 등 각종 교육에 관한 업무 담당.<br />
        10. 연구이사: 수면 또는 학회와 관련된 연구에 관한 업무 담당.<br />
        11. 정보이사: 본회 활동에 관한 각종 전산화 업무 및 정보 관리 업무 담당<br />
        12. 보험이사: 보험 업무에 관한 사항을 담당.<br />
        13. 홍보이사: 홍보에 관한 사항을 담당.<br />
        14. 윤리이사: 저작과 관련된 윤리적 문제와 학회와 관련된 윤리적 문제 담당.<br />
        15. 특임이사: 특별사안에 대한 과제를 수행.<br />
        16. 무임소이사: 특별사안에 대하여 과제수행을 준비.<br />
        17. 이외의 업무가 필요한 경우 이사회의 의결로 임원을 임명할 수 있다.
        </div>

        <h3 class="bulletBox">제 4 장 총회</h3>
        <div class="txt_rule"><strong class="color_blue">제 13 조 (총회)</strong><br />
        1. 총회는 정기총회와 임시총회로 구분한다.<br />
        2. 총회에는 정회원, 준회원, 일반회원, 특별회원이 참여할 수 있다.<br />
        3. 정기총회는 연 1 회 회장이 소집하고 의장이 된다.<br />
        4. 총회는 회장이 의장이 되며, 재석 정회원 과반수의 찬성으로 의결한다.<br />
        5. 임시총회는 학회에 긴급사항이 발생하였을 때 회장, 임원의 1/3 이상 또는 정회원의
        1/3 이상의 발의, 그리고 평의원회 재적 1/3 이상의 요구가 있을 경우에 소집될 수 있다.<br />
        6. 재석회원 과반수의 찬성으로 의결한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 14 조 (총회의 심의 또는 의결사항)</strong><br />
        1. 회칙제정 및 개정에 관한 사항<br />
        2. 회장, 감사의 보고<br />
        3. 기타 이사회에서 부의된 사항<br />
        4. 감사의 선출
        </div>

        <h3 class="bulletBox">제 5 장 이사회 및 위원회</h3>
        <div class="txt_rule"><strong class="color_blue">제 15 조 (이사회의 소집)</strong><br />
        1. 이사회는 회장이 주관하며 이사회 위원은 회장, 부회장, 이사진 등으로 구성된다.
        연 4 회의 정기 이사회를 가지며 필요 시 임시 이사회를 개최한다.<br />
        2. 회장이 필요하다고 인정할 때 또는 위원의 3 분의 1 이상의 요구가 있을 때는 이사회를
        소집할 수 있다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 16 조 (이사회의 운영)</strong><br />
        1. 이사회는 이사회 위원 과반수의 출석으로 성립되며 재석위원의 과반수의 찬성으로
        의결된다.<br />
        2. 가부 동수일 대는 회장이 결정한다.<br />
        3. 고문, 감사, 간사는 이사회에 참석할 수 있으나 의결권은 없다.<br />
        4. 이사회는 불가피한 사유로 이사회 위원 전부 또는 일부가 회의에 직접 출석하지 않은
        경우, 사전에 구두 또는 문서로 의장에게 위임할 수 있으며, 불참한 위원이 참석한
        위원과 동시에 음성을 송수신하는 원격통신수단에 의하여 의결에 참가할 수 있다. 이
        경우 위원이 이사회에 직접 출석한 것과 동일한 것으로 간주한다.<br />
        5. 이사회는 회원 개인이나 회원 그룹에게 학회활동과 관련된 특별한 임무를 부여할 수
        있다.<br />
        6. 이사회의 안건은 회의 전까지 부의 안건을 명시하여 공고 또는 통고되어야 한다. 단,
        긴급이사회 소집 때는 예외로 한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 17 조 (이사회의 결의사항)</strong> 이사회는 다음 사항을 심의 및 의결한다.<br />
        1. 회원 자격심사에 관한 사항.<br />
        2. 학술대회, 보수교육 및 학술강연 시행에 관한 사항.<br />
        3. 대외연락업무에 관한 사항.<br />
        4. 예산편성 및 운용에 관한 사항.<br />
        5. 기타 각 위원회의 사업계획 및 사업에 관한 사항.<br />
        6. 사무직제 및 제규정 제정에 관한 사항.<br />
        7. 학회 자산 운용에 관한 사항.<br />
        8. 포상 및 징계에 관한 사항.<br />
        9. 지부 승인에 관한 사항.<br />
        10. 총회에 부의 할 사항.<br />
        11. 기타 본 학회의 발전에 필요한 사항.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 18 조 (위원회)</strong><br />
        1. 회장은 학회의 제반 업무의 원활한 수행을 위하여 위원회를 구성할 수 있다.<br />
        2. 회장은 구성된 위원회의 목적달성에 적합한 해당 이사를 위원회의 위원장으로 임명한다.<br />
        3. 각 위원회의 위원은 10 명 이내로 하며 위원장의 제청으로 이사회의 의결을 거쳐 회장이
        임명한다.<br />
        4. 위원의 임기는 위원장의 임기와 동일하도록 한다.<br />
        5. 위원회의 주요 의결사항은 이사회에 보고하여 동의를 받아야 한다.<br />
        6. 위원회는 회의록과 관련 자료를 작성 보관하고 이사회의 요청이 있으면 제출해야 한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 19 조 (회의록)</strong> 총회, 이사회 및 각 위원회의 회의 때는 회의록을 작성하여야 하며, 총회와
        이사회 회의록은 의장과 이사 2 인 이상의 서명 또는 날인을 받은 후 보관하여야 하고 위원회의
        회의록은 의장과 위원 1 인 이상의 서명 또는 날인을 받은 후 보관하여야 한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 20 조 (공지)</strong> 본 회 제반 회의의 소집은 회의 전까지 부의 안건을 명시해서 공고 또는
        통고되어야 한다. 단 긴급회의 소집은 예외로 한다.
        </div>

        <h3 class="bulletBox">제 6 장 평의원회</h3>
        <div class="txt_rule"><strong class="color_blue">제 21 조 (평의원회의 구성 및 임기)</strong><br />
        1. 평의원 구성은 최근 2 년 이상 본 학회 정회원으로서 20 명 이상으로 한다.<br />
        2. 평의원은 당연직 평의원과 선출직 평의원으로 구분한다.<br />
        3. 당연직 평의원의 자격은 다음과 같다.<br />
        1) 현 회장 및 부회장, 현 이사<br />
        2) 역대 회장<br />
        4. 선출직 평의원은 정회원으로 2 년이 경과한 자중, 이사회에서 추천을 받아 회장이
        임명한다.<br />
        5. 선출된 평의원은 수락의사를 밝힘으로써 평의원으로 확정된다. 평의원은 본인의 의사에
        따라 사임 할 수 있다.<br />
        6. 임기는 3 년이며 연임이 가능하다. 임기의 시작은 정기 학술대회가 종료되는 시점부터
        한다.<br />
        7. 3 년 이상 평의원회에 특별한 사유 없이 불참한 자는 평의원 자격을 자동 상실한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 22 조 (평의원회의 개최 및 의결)</strong><br />
        1. 평의원회의 의장은 현 회장으로 한다.<br />
        2. 평의원회는 회장의 소집으로 1 년에 1 회 이상 정기회의를 개최한다. 임시 평의원회의는
        재적 평의원의 3 분의 1 이상 또는 이사회의 요구로 소집할 수 있다.<br />
        3. 평의원회는 재적 평의원 과반수의 출석으로 성립되고, 출석 평의원 과반수의 찬성으로
        의결한다. 가부동수인 경우 회장이 결정한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 23 조 (평의원회의 의결사항)</strong><br />
        1. 임원 선출 및 불신에 관한 사항<br />
        2. 이사의 인준에 관한 사항<br />
        3. 회칙의 제정과 개정 및 폐지 인준에 관한 사항<br />
        4. 예산 및 결산의 심의 인준에 관한 사항<br />
        5. 포상, 징계, 회원의 제명에 관한 사항<br />
        6. 기타 학회 의무에 관한 사항
        </div>

        <h3 class="bulletBox">제 7 장 재정</h3>
        <div class="txt_rule"><strong class="color_blue">제 24 조 (수입의 종류)</strong><br />
        본 학회의 운영비용은 입회비, 연회비, 찬조금 및 학술대회 참가비, 보조금, 기타의 수입으로
        충당한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 25 조 (회계연도)</strong><br />
        본 학회의 회계연도는 당해 년도 9 월 1 일부터 다음 해 8 월 31 일까지로 한다.
        </div>

        <div class="txt_rule"><strong class="color_blue">제 26 조 (회계보고)</strong><br />
        각 연도의 세입, 세출에 대한 결산은 정기 총회 전에 감사의 심사와 평의원회의 승인을 거쳐
        정기총회에서 인준을 받는다.
        </div>

        <h3 class="bulletBox">제 8 장 회칙개정</h3>
        <div class="txt_rule"><strong class="color_blue">제 27 조 (회칙개정)</strong><br />
        1. 회장 또는 이사회 위원의 1/3, 또는 정회원의 1/3 이 동의한 경우에 회칙 개정안을
        이사회에 제출할 수 있다.<br />
        2. 이사회 소집일로부터 최소한 1 주 이전에 이사들에게 서면 또는 이메일로 통보하여야
        한다.<br />
        3. 재적 이사 과반수의 출석으로 성립한 이사회에서 재석 이사 2/3 이상의 찬성으로
        개정한다.<br />
        4. 개정된 회칙은 이사회의 의결을 거쳐 총회의 승인을 받은 즉시 시행한다.
        </div>

        <h3 class="bulletBox">제 9 장 부칙</h3>
        <div class="txt_rule"><strong class="color_blue">제 28 조 (준칙)</strong><br />
        1. 본 회칙에 규정되지 않은 사항은 일반 관례를 따른다. 본 회칙은 2004년 6월 27일
        창립총회의 결의로 그 효력을 발생한다.<br />
        2. 본 개정회칙은 2013년 6월 29일부터 시행한다.<br />
        3. 본 개정회칙은 2016년 11월 10일부터 시행한다.<br />
        4. 본 개정회칙은 2017년 6월 15일부터 시행한다.
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>