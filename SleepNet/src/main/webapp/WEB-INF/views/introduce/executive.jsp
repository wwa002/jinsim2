<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">학회소개</h2>
        <div class="right">
          <h3>임원진 명단</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;학회소개&nbsp;&nbsp;&gt;&nbsp;&nbsp;임원진 명단</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <table class="tbl_executive">
          <colgrou>
            <col style="width:14%;" />
            <col style="width:14%;" />
            <col />
          </colgrou>
          <thead>
            <th>직위</th>
            <th>성명</th>
            <th>소속</th>
          </thead>
          <tbody>
          <tr>
            <th>회장</th>
            <td>남현우</td>
            <td class="align_left">보라매병원, 서울의대 신경과</td>
          </tr>
          <tr>
            <th>부회장</th>
            <td>김지언</td>
            <td class="align_left">대구가톨릭대학교병원, 가톨릭의대 신경과</td>
          </tr>
          <tr>
            <th>기획이사</th>
            <td>김원주</td>
            <td class="align_left">강남세브란스병원, 연세의대 신경과</td>
          </tr>
          <tr>
            <th>총무이사</th>
            <td>양광익</td>
            <td class="align_left">순천향대학교 천안병원, 순천향의대 신경과</td>
          </tr>
          <tr>
            <th>총무간사</th>
            <td>구대림</td>
            <td class="align_left">보라매병원, 서울의대 신경과</td>
          </tr>
          <tr>
            <th>학술이사</th>
            <td>주은연</td>
            <td class="align_left">삼성서울병원, 성균관의대 신경과</td>
          </tr>
          <tr>
            <th>교육이사</th>
            <td>조양제</td>
            <td class="align_left">세브란스병원, 연세의대 신경과</td>
          </tr>
          <tr>
            <th>편집이사</th>
            <td>정기영</td>
            <td class="align_left">서울대학교병원, 서울의대 신경과</td>
          </tr>
          <tr>
            <th>연구이사</th>
            <td>윤창호</td>
            <td class="align_left">분당서울대학교병원, 서울의대 신경과</td>
          </tr>
          <tr>
            <th>대외협력이사</th>
            <td>이호원</td>
            <td class="align_left">칠곡경북대학교병원, 경북의대 신경과</td>
          </tr>
          <tr>
            <th>국제이사</th>
            <td>조용원</td>
            <td class="align_left">동산병원, 계명의대 신경과</td>
          </tr>
          <tr>
            <th>홍보이사</th>
            <td>김혜윤</td>
            <td class="align_left">국제성모병원, 관동의대 신경과</td>
          </tr>
          <tr>
            <th>재무이사</th>
            <td>김지현</td>
            <td class="align_left">고대구로병원, 고려의대 신경과</td>
          </tr>
          <tr>
            <th>보험이사</th>
            <td>주민경</td>
            <td class="align_left">강남성심병원, 한림의대 신경과</td>
          </tr>
          <tr>
            <th>정보이사</th>
            <td>박기형</td>
            <td class="align_left">길병원, 가천의대 신경과</td>
          </tr>
          <tr>
            <th>윤리이사</th>
            <td>한선정</td>
            <td class="align_left">원광대산본병원, 원광의대 신경과</td>
          </tr>
          <tr>
            <th rowspan="2">특임이사</th>
            <td>김성택</td>
            <td class="align_left">연세대학교 치과대학병원, 연세치대 구강내과 (치과담당)</td>
          </tr>
          <tr>
            <td>김광기</td>
            <td class="align_left">동국대학교일산병원, 동국의대 신경과 (사료관리 위원장)</td>
          </tr>
          <tr>
            <th rowspan="2">무임소이사</th>
            <td>신원철</td>
            <td class="align_left">강동경희대학교병원, 경희의대 신경과</td>
          </tr>
          <tr>
            <td>김지현</td>
            <td class="align_left">천안단국대학교병원, 단대의대 신경과</td>
          </tr>
          <tr>
            <th>감사</th>
            <td>이향운</td>
            <td class="align_left">이대목동병원, 이대의대 신경과</td>
          </tr>
          </tbody>
        </table>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>