<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">학회소개</h2>
        <div class="right">
          <h3>사무국 안내</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;학회소개&nbsp;&nbsp;&gt;&nbsp;&nbsp;사무국 안내</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="top_txt_box">
          <ul class="officeInfo">
            <li class="bullet_arrow"><strong class="color_blue">주소</strong class="color_blue">(07061) 서울시 동작구 보라매로5길 20 서울대학교 보라매병원 행복관 11132호 교수 의국</li>
            <li class="bullet_arrow"><strong class="color_blue">문의전화</strong class="color_blue">+82-2-870-3864</li>
            <li class="bullet_arrow"><strong class="color_blue">팩스</strong class="color_blue">+82-2-870-3866</li>
            <li class="bullet_arrow"><strong class="color_blue">E-mail</strong class="color_blue">koreasleep@empas.com</li>
          </ul>
        </div>

        <iframe class="google-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4225.442102644759!2d126.92565285990472!3d37.49600766663184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1216596b31acbb05!2z7ISc7Jq47Yq567OE7Iuc67O0652866ek67OR7JuQ!5e0!3m2!1sko!2skr!4v1504736346279" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

        <div class="mBox joinGuide">
          <h4 class="bulletBox">대중교통</h4>
          <div class="traffic">
            <img src="/resources/images/ico_bus.gif" alt="버스">
            <table class="tbl_executive mgt20 bus">
              <colgrou>
                <col style="width:28%;" />
                <col />
              </colgrou>
              <thead>
                <th>정류장명</th>
                <th>운행 버스 정보</th>
              </thead>
              <tbody>
              <tr>
                <th>A. 서울특별시 보라매병원</th>
                <td class="align_left">153, 5516, 5525</td>
              </tr>
              <tr>
                <th>B. 보라매병원 (하행)</th>
                <td class="align_left">동작05</td>
              </tr>
              <tr>
                <th>C. 보라매병원 (상행)</th>
                <td class="align_left">동작05-1</td>
              </tr>
              <tr>
                <th>D. 롯데관악점</th>
                <td class="align_left">153, 5516, 5522B호암, 5524, 5535, 6511, 6516</td>
              </tr>
              <tr>
                <th>E. 롯데관악점</th>
                <td class="align_left">5522A난곡, 5524, 5535, 6511, 6516</td>
              </tr>
              <tr>
                <th>F. 롯데관악점 보라매병원</th>
                <td class="align_left">152, 153, 461, 504, 5516, 5525, 6513, 6514, 6515, 6516</td>
              </tr>
              <tr>
                <th>G. 보라매공원 보라매병원</th>
                <td class="align_left">152, 461, 504, 6513, 6514, 6515, 6516</td>
              </tr>
            </table>
          </div>

          <div class="traffic">
            <img src="/resources/images/ico_subway.gif" alt="지하철">
            <table class="tbl_executive mgt20 subway">
              <colgrou>
                <col style="width:12%;" />
                <col />
              </colgrou>
              <tbody>
              <tr>
                <th>1호선</th>
                <td class="align_left">대방역 3번출구 -> 마을버스 05번 이용(20분)</td>
              </tr>
              <tr>
                <th>2호선</th>
                <td class="align_left">신대방역 3번 출구->좌측길(약50m)->마을버스05-1번 이용(10분)<br />신림역 7번 출구->시내버스 5516, 5525번 이용(10분)</td>
              </tr>
              <tr>
                <th>7호선</th>
                <td class="align_left">신대방삼거리역 2번 출구 -> 직진(약150m)->길건너 마을버스 05번 이용 또는 도보<br />보라매역 1번 출구 ->도보20분 </td>
              </tr>
            </table>
          </div>

        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>