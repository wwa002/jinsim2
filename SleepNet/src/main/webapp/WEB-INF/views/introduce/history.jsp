<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">학회소개</h2>
        <div class="right">
          <h3>연혁</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;학회소개&nbsp;&nbsp;&gt;&nbsp;&nbsp;연혁</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <table class="tbl_history02">
          <colgrou>
            <col style="width:29%;" />
            <col />
          </colgrou>
          <tr>
            <th>2001년 12월 14일</th>
            <td>대한수면연구회 준비<br/>회장 : 김주한 / 부회장 : 홍승봉</td>
          </tr>
          <tr>
            <th>2002년 4월 12일</th>
            <td>대한수면연구회 창립총회 및 심포지움 (경주교육문화회관)</td>
          </tr>
          <tr>
            <th>2002년 4월 10일</th>
            <td>대한수면연구회 홈페이지 오픈</td>
          </tr>
          <tr>
            <th>2002년 7월 12일</th>
            <td>대한수면연구회 모임 (삼성서울병원 지하 1층 중강당)</td>
          </tr>
          <tr>
            <th>2004년 6월 27일</th>
            <td>제1회 대한수면연구회 심포지엄 (한양대 HIT 강당)</td>
          </tr>
          <tr>
            <th>2005년 7월 3일</th>
            <td>제2회 대한수면연구회 학술대회 개최 (서울건국대학교병원 강당)<br />이후 매년 학술대회 개최 </td>
          </tr>
          <tr>
            <th>2006년 5월 11일</th>
            <td>제2회 대한수면연구회 집담회 실시 (건국대병원 3층 제2강의실)<br />이후 매년 2회~4회의 집담회 실시 </td>
          </tr>
          <tr>
            <th>2008년 2월 23일</th>
            <td>제1회 대한수면연구회 수면검사워크샵 (한양대 HIT강당)<br />이후 매년 수면검사 워크샵 시행</td>
          </tr>
          <tr>
            <th>2008년 9월 9일</th>
            <td>대한수면연구회에서 대한수면연구학회로 학회명칭 변경<br />회장 : 홍승봉 / 부회장 : 이상암</td>
          </tr>
          <tr>
            <th>2009년 4월 제1회</th>
            <td>제1회 불면증인지행동치료 워크샵 (4/18-20) (삼성서울병원)</td>
          </tr>
          <tr>
            <th>2009년 9월</th>
            <td>대한수면연구학회지 수면의 영문명을 Journal of Korean Research Society 로 변경 </td>
          </tr>
          <tr>
            <th>2012년 9월</th>
            <td>새로운 회장단, 이사진 구성 (회장 : 이상암 / 부회장 : 허경)</td>
          </tr>
          <tr>
            <th>2014년 9월</th>
            <td>새로운 회장단, 이사진 구성 (회장 : 허경 / 부회장 : 남현우)</td>
          </tr>
          <tr>
            <th>2016년 9월</th>
            <td>새로운 회장단, 이사진 구성 (회장: 남현우 / 부회장 : 김지언)</td>
          </tr>
        </table>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>