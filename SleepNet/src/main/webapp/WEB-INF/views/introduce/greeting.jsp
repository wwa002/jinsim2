<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">학회소개</h2>
        <div class="right">
          <h3>인사말</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;학회소개&nbsp;&nbsp;&gt;&nbsp;&nbsp;인사말</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <div class="greeting mBox">
          <p class="imgBox">
            <img src="/resources/images/img_greeting.jpg" alt="">
            <span>안녕하세요?<br />
            <strong>대한수면연구학회 홈페이지를 방문해 주셔서 감사드립니다.</strong></span>
          </p>

          <p>우리 대한수면연구학회는 2002년 4월 12일 창립 이후 꾸준히 발전하여 우리나라를 대표하는 수면의학단체로서의 역할을 수행하여 왔습니다. “대한민국 수면의학의 리더”라는 비전 하에 전국의 수면의학을 전공하는 신경과, 치과, 소아과 의사들이 주축을 이루어 정기적인 학술활동과 사회기여, 국가정책에의 참여 등 활발한 활동을 이어오고 있습니다. 앞으로 세계의 수면의학을 선도하는 학회가 되고자 더욱 노력하겠습니다.</p>

          <p>본 학회는 다음의 세 가지 목표에 중점을 두고 있습니다.</p>

          <p>첫째는 회원들의 연구역량 강화입니다. 이를 위해 연1회의 학술대회와 연1회의 워크샵, 연4회의 학술집담회를 진행하고 있습니다.
          또, 학회차원의 연구도 진행하고 있습니다.</p>

          <p>둘째는 대국민홍보입니다.
          수면의 날 행사와 각 병원 별 강의 등을 주관 또는 지원하여 국민들에게 수면질환에 대해 알리고 있습니다.</p>

          <p>셋째는 보건당국의 인식제고를 위한 노력입니다. 현재 비보험인 각종 수면검사와 치료의 보험적용을 위해 관계당국에 적극 협조
          하고 있으며 사회적 이슈에 대해 자문하고 있습니다.</p>

          <p>향후 일신하여 대한민국 국민의 수면건강은 우리가 책임진다는 사명 하에 더욱 정진할 각오이오니 여러분의 많은 관심과 협조를 부탁드립니다. 감사합니다.</p>

          <p class="writer">대한수면연구학회 회장 남현우 배상</p>
        </div>

      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>