<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/front-header.jsp"%>
<%@ include file="/WEB-INF/views/include/front-menu.jsp"%>    

	<!-- contents -->
	<section>
    <div class="box_tit bg_board">
      <div class="inner">
        <h2 class="left">학회소개</h2>
        <div class="right">
          <h3>회원가입 안내</h3>
          <span class="location"><span class="home sprites hide_text">home</span>&nbsp;&nbsp;&gt;&nbsp;&nbsp;학회소개&nbsp;&nbsp;&gt;&nbsp;&nbsp;회원가입 안내</span>
          <span class="options">
              <a href="#" class="zoomIn sprites hide_text">확대</a>
              <a href="#" class="zoomOut sprites hide_text">축소</a>
              <a href="#" class="print sprites hide_text">인쇄</a>
          </span>
        </div>
      </div>
    </div>
    <!-- //box_tit -->

    <div class="contents inner">
      <%@ include file="/WEB-INF/views/include/front-left-menu.jsp"%>
      <!-- //lnb end -->

      <div class="right">
        <p class="top_txt_box"><strong>대한수면연구학회 (Korean Sleep Research Society)</strong>는 매년 수면의학과 관련된 다양한 심포지엄과 학술대회, 그리고
        수면다원검사에 대한 워크샵 등의 행사를 주관하고 있는 학술단체로, 수면의학과 관련된 의사, 기사, 간호사, 기초 의학자
        들의 참여하고 있습니다. <br />
        <strong class="red">대한수면연구학회의 가입을 위해서는 입회신청서와 입회비를 보내주셔야 가입승인이 됩니다.</strong></p>
        <div class="mBox joinGuide">
          <h4 class="bulletBox">회원 구분</h4>
          <table class="tbl_blue">
            <colgroup>
              <col style="width:14%;" />
              <col />
            </colgroup>
            <tr>
              <th>정회원</th>
              <td>수면의학 영역의 연구 및 진료를 수행하고 있으며 본회의 설립목적에 찬동하는 관련 분야의 전문의 및 기초의학 연구자</td>
            </tr>
            <tr>
              <th>준회원</th>
              <td>수면의학 관련 분야의 전공의</td>
            </tr>
            <tr>
              <th>일반회원</th>
              <td>수면의학 관련 분야의 전공의</td>
            </tr>
          </table>

          <h4 class="bulletBox">회원 혜택</h4>
          <ul class="list_indent">
            <li class="bullet_arrow">대한수면연구학회 주관의 학술대회 및 각종 행사의 등록비 감면</li>
            <li class="bullet_arrow">대한수면연구학회 학회지 무료 구독 (년 2회)</li>
            <li class="bullet_arrow">대한수면연구학회 홈페이지 (www.sleepnet.or.kr) 회원 공간 무료 이용
              <ul>
                <li>1) 학회지 검색 및 full text access</li>
                <li>2) 대한수면연구학회 주관의 각종 심포지엄, 워크-샵의 강의록 및 강의 슬라이드 이용</li>
                <li>3) 수면관련 정보 및 뉴스 제공</li>
              </ul>
            </li>
          </ul>

          <h4 class="bulletBox">온라인 회원 가입</h4>
          <ul class="list_indent">
            <li class="bullet_arrow">회원가입 항목에 따라 정보를 입력하셔야 합니다.</li>
            <li class="bullet_arrow">입회비 입금 확인 후 회원으로 승인되며, 회원 승인은 입금 확인 후 1주일 내에 이메일로 발송됩니다.</li>
            <li class="bullet_arrow">회원 승인 후 홈페이지의 모든 서비스를 이용하실 수 있습니다.</li>
          </ul>
          <table class="tbl_executive mgt20">
            <colgrou>
              <col style="width:33.3333%;" />
              <col style="width:33.3333%;" />
              <col />
            </colgrou>
            <thead>
              <th>회원구분</th>
              <th>입회비</th>
              <th>연회비</th>
            </thead>
            <tbody>
            <tr>
              <th>평생회원</th>
              <td>320,000원</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <th>정회원</th>
              <td>20,000원</td>
              <td>30,000원</td>
            </tr>
            <tr>
              <th>준회원 (전공의)</th>
              <td>10,000원</td>
              <td>30,000원</td>
            </tr>
          </table>
          <ul class="list_indent mgt20">
            <li>1) 입회비는 별도 공지가 있을 때까지 면제입니다.</li>
            <li>2) 매년 연회비는 3만원입니다(연회비를 납부하셔야 회원자격이 유지됩니다).</li>
            <li>3) 2017년 7월 31일까지 평생연회비는 32만원입니다.</li>
          </ul>

          <h4 class="bulletBox">입금계좌</h4>
          <p class="bullet_arrow account"><strong class="color_red">신한은행 100-026-435460</strong> <span>(예금주 : 대한수면연구학회)</span></p>

          <h4 class="bulletBox">입회 신청서 보내실 곳 / 회원가입 관련 문의</h4>
          <ul class="list_indent">
            <li class="bullet_arrow"><strong class="color_blue">E-mail :</strong> koreasleep@empal.com</li>
            <li class="bullet_arrow"><strong class="color_blue">전화 :</strong> 02-870-3864</li>
            <li class="bullet_arrow"><strong class="color_blue">Fax :</strong> 02-870-3866 ("대한수면연구학회 앞")</li>
          </ul>
        </div>
      </div>
      <!-- //right end -->
    </div>
    <!--//contents -->

<%@ include file="/WEB-INF/views/include/front-footer.jsp"%>