$(document).ready(function(){
  setHeight();
})

function setHeight(){
  var docHeight = $("body").height();
  var winHeight = $(window).height();

  if(winHeight>docHeight){
    var contHeight = $("section").height();
    var adjustH = contHeight+(winHeight-docHeight);
    $("section").height(adjustH);
  }
}
