package kr.dmon.wb.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import kr.dmon.wb.utils.StringUtil;

public class SiteSession implements Serializable, HttpSessionBindingListener {

	public static final String SESSION_KEY = "SITE_SESSION_KEY";

	String uuid;
	
	String sitecd;
	String type;
	String title;
	String email;
	String isused;
	String sms;
	String mailer;
	String app;
	String society;
	
	String shop;
	String store;
	String medicaldb;
	String event;
	String banner;
	String infodb;
	String cast;
	String conference;
	
	Map<String, Object> cfg;
	Map<String, Map<String, Object>> resource;
	
	public SiteSession() {
		uuid = UUID.randomUUID().toString();
	}

	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		HttpSession session = SiteSessionHolder.remove(this.uuid);
		this.uuid = uuid;
		SiteSessionHolder.put(this.uuid, session);
	}
	
	public String getSitecd() {
		return sitecd;
	}

	public void setSitecd(String sitecd) {
		this.sitecd = sitecd;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getIsused() {
		return isused;
	}

	public void setIsused(String isused) {
		this.isused = isused;
	}
	
	public boolean isUsed() {
		return !"N".equals(isused);
	}
	
	public String getSms() {
		return sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getMailer() {
		return mailer;
	}

	public void setMailer(String mailer) {
		this.mailer = mailer;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getSociety() {
		return society;
	}

	public void setSociety(String society) {
		this.society = society;
	}
	
	public String getShop() {
		return shop;
	}

	public void setShop(String shop) {
		this.shop = shop;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}
	
	public String getMedicaldb() {
		return medicaldb;
	}

	public void setMedicaldb(String medicaldb) {
		this.medicaldb = medicaldb;
	}
	
	public boolean useMedicaldb() {
		return "Y".equals(medicaldb);
	}
	
	public boolean useCast() {
		return "Y".equals(cast);
	}
	
	public boolean useSociety() {
		return "Y".equals(society);
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getBanner() {
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getInfodb() {
		return infodb;
	}

	public void setInfodb(String infodb) {
		this.infodb = infodb;
	}

	public String getCast() {
		return cast;
	}

	public void setCast(String cast) {
		this.cast = cast;
	}
	
	public String getConference() {
		return conference;
	}

	public void setConference(String conference) {
		this.conference = conference;
	}
	
	public boolean useConference() {
		return "Y".equals(conference);
	}

	public Map<String, Object> getCfg() {
		return cfg;
	}

	public void setCfg(Map<String, Object> cfg) {
		this.cfg = cfg;
	}
	
	public void setResource(List<Map<String, Object>> res) {
		if (res == null) {
			resource = null;
			return;
		}

		resource = new HashMap<>();
		for (Map<String, Object> m : res) {
			resource.put(StringUtil.getContent(m.get("idx")), m);
		}
	}
	
	public String getResourceIdx(String name) {
		if (StringUtil.isEmpty(name)) {
			return null;
		}
		
		if (resource == null || resource.size() == 0) {
			return null;
		}
		
		Iterator<String> keys = resource.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			Map<String, Object> m = resource.get(key);
			if (m.get("name").equals(name)) {
				return key;
			}
		}
		
		return null;
	}
	
	public String getResourceIdx(String parent, String name) {
		if (StringUtil.isEmpty(parent) || StringUtil.isEmpty(name)) {
			return null;
		}
		
		if (resource == null || resource.size() == 0) {
			return null;
		}
		
		Iterator<String> keys = resource.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			Map<String, Object> m = resource.get(key);
			if (Integer.parseInt(m.get("parent").toString()) == Integer.parseInt(parent) && m.get("name").equals(name)) {
				return key;
			}
		}
		
		return null;
	}
	
	public Map<String, Map<String, Object>> getResource() {
		return resource;
	}
	
	public void setData(Map<String, Object> item) {
		try {
			sitecd = StringUtil.getContent(item.get("sitecd"));
			type = StringUtil.getContent(item.get("type"));
			title = StringUtil.getContent(item.get("title"));
			email = StringUtil.getContent(item.get("email"));
			isused = StringUtil.getContent(item.get("isused"));
			sms = StringUtil.getContent(item.get("sms"), "N");
			mailer = StringUtil.getContent(item.get("mailer"), "N");
			app = StringUtil.getContent(item.get("app"), "N");
			society = StringUtil.getContent(item.get("society"), "N");
			
			shop = StringUtil.getContent(item.get("shop"), "N");
			store = StringUtil.getContent(item.get("store"), "N");
			medicaldb = StringUtil.getContent(item.get("medicaldb"), "N");
			event = StringUtil.getContent(item.get("event"), "N");
			banner = StringUtil.getContent(item.get("banner"), "N");
			infodb = StringUtil.getContent(item.get("infodb"), "N");
			cast = StringUtil.getContent(item.get("cast"), "N");
			
			conference = StringUtil.getContent(item.get("conference"), "N");
			
			cfg = item;
		} catch (NullPointerException e) {
		}
	}
	
	String log;
	String agent;
	
	public String getLog() {
		return log;
	}
	
	public void setLog(String log) {
		this.log = log;
	}
	
	public String getAgent() {
		return agent;
	}
	
	public void setAgent(String agent) {
		this.agent = agent;
	}
	
	
	public boolean isBuilderSite() {
		return StringUtil.isEmpty(sitecd);
	}
	
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		SiteSessionHolder.put(uuid, event.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		SiteSessionHolder.remove(uuid);
	}
}
