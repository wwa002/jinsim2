package kr.dmon.wb.session;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class BoardSessionHolder {

	private static Map<String, HttpSession> SESSION_MAP = new HashMap<String, HttpSession>();

	public static synchronized HttpSession HttpSession(String id) {
		return SESSION_MAP.get(id);
	}

	public static synchronized void put(String id, HttpSession session) {
		SESSION_MAP.put(id, session);
	}

	public static synchronized void remove(String id) {
		SESSION_MAP.remove(id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BoardSessionHolder []");
		return builder.toString();
	}
	
}
