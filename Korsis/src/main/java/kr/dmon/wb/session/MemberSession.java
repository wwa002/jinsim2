package kr.dmon.wb.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import kr.dmon.wb.utils.StringUtil;

public class MemberSession implements Serializable, HttpSessionBindingListener {

	public static final String SESSION_KEY = "MEMBER_SESSION_KEY";

	String uuid;

	String sitecd;
	int idx;
	String type;
	String memno;
	String auth;
	String id;
	String name;
	String nickname;
	String mobile;
	String email;
	String birth;
	String sex;
	String logdate;
	Map<String, Object> data;
	Map<String, Boolean> gcds;
	
	public MemberSession() {
		uuid = UUID.randomUUID().toString();
	}
	
	public MemberSession(String sitecd) {
		uuid = UUID.randomUUID().toString();
		this.sitecd = sitecd;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		HttpSession session = MemberSessionHolder.remove(this.uuid);
		this.uuid = uuid;
		MemberSessionHolder.put(this.uuid, session);
	}

	public String getSitecd() {
		return sitecd;
	}
	
	public void setSitecd(String sitecd) {
		this.sitecd = sitecd;
	}
	
	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMemno() {
		return memno;
	}

	public void setMemno(String memno) {
		this.memno = memno;
	}
	
	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public boolean isPermitGcd(String gcd) {
		try {
			return gcds.get(gcd);
		} catch (Exception e) {
			return false;
		}
	}
	
	public void setGcds(List<Map<String, Object>> gcds) {
		this.gcds = new HashMap<>();
		for (Map<String, Object> m : gcds) {
			this.gcds.put(m.get("gcd").toString(), true);
		}
	}

	public String getLogdate() {
		return logdate;
	}

	public void setLogdate(String logdate) {
		this.logdate = logdate;
	}

	public void setData(Map<String, Object> item) {
		sitecd = StringUtil.getContent(item.get("sitecd"));
		idx = Integer.parseInt(StringUtil.getContent(item.get("idx"), "0"));
		type = StringUtil.getContent(item.get("type"));
		memno = StringUtil.getContent(item.get("memno"));
		auth = StringUtil.getContent(item.get("auth"));
		id = StringUtil.getContent(item.get("id"));
		name = StringUtil.getContent(item.get("name"));
		nickname = StringUtil.getContent(item.get("nickname"));
		mobile = StringUtil.getContent(item.get("mobile"));
		email = StringUtil.getContent(item.get("email"));
		birth = StringUtil.getContent(item.get("birth"));
		sex = StringUtil.getContent(item.get("sex"));
		logdate = StringUtil.getContent(item.get("logdate"));
		data = item;
	}
	
	public Map<String, Object> getData() {
		return data;
	}

	public boolean isLogin() {
		return !StringUtil.isEmpty(id);
	}

	public boolean isAdmin() {
		if (!isLogin()) {
			return false;
		}
		return "ADMIN".equals(type) || "BUILDER".equals(type);
	}

	public boolean isBuilder() {
		if (!isLogin()) {
			return false;
		}
		return "BUILDER".equals(type);
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		MemberSessionHolder.put(uuid, event.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		MemberSessionHolder.remove(uuid);
	}

}
