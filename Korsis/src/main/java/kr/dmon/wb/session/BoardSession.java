package kr.dmon.wb.session;

import java.io.Serializable;
import java.util.Map;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import kr.dmon.wb.utils.StringUtil;

public class BoardSession implements Serializable, HttpSessionBindingListener {

private static final String _PREFIX = "BOARD_";
	
	String sitecd;
	String code;
	int fidx;

	public String getSitecd() {
		return sitecd;
	}
	
	public void setSitecd(String sitecd) {
		this.sitecd = sitecd;
	}
	
	public int getFidx() {
		return fidx;
	}
	
	public void setFidx(int fidx) {
		this.fidx = fidx;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getId() {
		return _PREFIX + sitecd+ "_" + code + "_" + fidx;
	}
	
	public static final String getSessionKey(String sitecd, String code, int fidx) {
		return _PREFIX + sitecd + "_" + code + "_" + fidx;
	}
	
	public static final String getSessionKey(Map<String, Object> map) {
		return getSessionKey(StringUtil.getContent(map.get("sitecd")), StringUtil.getContent(map.get("code")), Integer.parseInt(StringUtil.getContent(map.get("fidx"))));
	}
	
	public void setData(Map<String, Object> map) {
		sitecd = StringUtil.getContent(map.get("sitecd"));
		code = StringUtil.getContent(map.get("code"));
		fidx = Integer.parseInt(StringUtil.getContent(map.get("fidx")));
	}

	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		BoardSessionHolder.put(getId(), event.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent event) {
		BoardSessionHolder.remove(getId());
	}
	
}
