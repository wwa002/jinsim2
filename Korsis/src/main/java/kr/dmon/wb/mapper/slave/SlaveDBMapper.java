package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveDBMapper {

	/* Hospital Search Start */
	public List<Map<String, Object>> menuSubjects(Map<String, Object> param) throws Exception;
	public Map<String, Object> menuSubjectItem(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> hospitalCodes() throws Exception;
	public List<Map<String, Object>> hospitalSubjects() throws Exception;
	
	public int hospitalListTotalInSubject(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalListInSubject(Map<String, Object> param) throws Exception;
	public int hospitalListTotalNotInSubject(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalListNotInSubject(Map<String, Object> param) throws Exception;
	/* Hospital Search End */
	
	/* Search Start */ 
	public List<Map<String, Object>> searchStoreMap(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> searchHospital(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> searchHospitalSubject(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> searchHospitalSubject2(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> searchHospitalType(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> searchHospitalSepcapp(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> searchPharmacy(Map<String, Object> param) throws Exception;
	public Map<String, Object> hospitalItem(Map<String, Object> param) throws Exception;
	public Map<String, Object> hospitalItemDetail(Map<String, Object> param) throws Exception;
	
	public int hospitalCheckRead(Map<String, Object> param) throws Exception;
	public Map<String, Object> hospitalReadItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalPhotoList(Map<String, Object> param) throws Exception;
	public Map<String, Object> hospitalPhotoItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalCommentList(Map<String, Object> param) throws Exception;
	public String hospitalRating(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> pharmacyItem(Map<String, Object> param) throws Exception;
	
	public int pharmacyCheckRead(Map<String, Object> param) throws Exception;
	public Map<String, Object> pharmacyReadItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> pharmacyPhotoList(Map<String, Object> param) throws Exception;
	public Map<String, Object> pharmacyPhotoItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> pharmacyCommentList(Map<String, Object> param) throws Exception;
	public String pharmacyRating(Map<String, Object> param) throws Exception;
	/* Search End */
	
	/* List Start */
	public int hospitalListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalSubjectListAll(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalTypeListAll(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalSpecialListAll(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalLocalListAll(Map<String, Object> param) throws Exception;
	
	public int pharmacyListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> pharmacyList(Map<String, Object> param) throws Exception;
	/* List End */
	
	/* Cast Start */
	public int castNewIdx(Map<String, Object> param) throws Exception;
	public int castListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> castList(Map<String, Object> param) throws Exception;
	public Map<String, Object> castItem(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> castPageList(Map<String, Object> param) throws Exception;
	public Map<String, Object> castPageItem(Map<String, Object> param) throws Exception;
	/* Cast End */
	
	/* Info DB Start */
	public int infoNewIdx(Map<String, Object> param) throws Exception;
	public int infoListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> infoList(Map<String, Object> param) throws Exception;
	public Map<String, Object> infoItem(Map<String, Object> param) throws Exception;
	
	
	public List<Map<String, Object>> infoGroupList(Map<String, Object> param) throws Exception;
	public Map<String, Object> infoGroupItem(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> infoContentList(Map<String, Object> param) throws Exception;
	public Map<String, Object> infoContentItem(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> infoPageList(Map<String, Object> param) throws Exception;
	public Map<String, Object> infoPageItem(Map<String, Object> param) throws Exception;
	
	public int apiInfoListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiInfoList(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> apiInfoItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiInfoGroupList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiInfoContentList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiInfoContentSearch(Map<String, Object> param) throws Exception;
	public Map<String, Object> apiInfoContentItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiInfoContentPages(Map<String, Object> param) throws Exception;
	
	public int apiInfoCheckRead(Map<String, Object> param) throws Exception;
	/* Info DB End */
	
	
	/* Cast Start */
	public int apiCastListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiCastList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiCastListBanner(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiCastSearch(Map<String, Object> param) throws Exception;
	public Map<String, Object> apiCastItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiCastPageList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiCastRecommend(Map<String, Object> param) throws Exception;
	
	public int apiCastCheckRead(Map<String, Object> param) throws Exception;
	/* Cast End */
	
	/* MyPage Start */
	public List<Map<String, Object>> myPageHospital(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> myPagePharmacy(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> myPageCast(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> myPageInfodb(Map<String, Object> param) throws Exception;
	/* MyPage End */
	
	public Map<String, Object> storePhotoItem(Map<String, Object> param) throws Exception;
}
