package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveEventMapper {
	
	public List<Map<String, Object>> categoryListAll(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> pageList(Map<String, Object> param) throws Exception;
	public Map<String, Object> pageItem(Map<String, Object> param) throws Exception;
	
	/* for API Start */
	public List<Map<String, Object>> apiBanners(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiNews(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiWeeks(Map<String, Object> param) throws Exception;
	
	public int apiListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiLocal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiSearch(Map<String, Object> param) throws Exception;
	
	public int apiExhibitionListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiExhibitionList(Map<String, Object> param) throws Exception;
	public Map<String, Object> apiExhibitionItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiExhibitionItems(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> apiEventItem(Map<String, Object> param) throws Exception;
	public int apiEventCheckRead(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiEventPageItems(Map<String, Object> param) throws Exception;
	/* for API End */
	
	
	/* Admin Start */
	public int eventNewIdx(Map<String, Object> param) throws Exception;
	public int adminListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminList(Map<String, Object> param) throws Exception;
	public Map<String, Object> adminItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminSearch(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> adminBannerList(Map<String, Object> param) throws Exception;
	
	public int planNewIdx(Map<String, Object> param) throws Exception;
	public int adminPlanListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminPlanList(Map<String, Object> param) throws Exception;
	public Map<String, Object> adminPlanItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminPlanSearch(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminPlanPageList(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> adminNewList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminWeekList(Map<String, Object> param) throws Exception;
	/* Admin End */
	
	/* MyPage Start */
	public List<Map<String, Object>> myPageEventFavorite(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> myPageEventApply(Map<String, Object> param) throws Exception;
	/* MyPage End */
}
