package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterEventMapper {
	
	/* for API Start */
	public void apiEventAddRead(Map<String, Object> param) throws Exception;
	public void apiEventApply(Map<String, Object> param) throws Exception;
	public void apiEventFavorite(Map<String, Object> param) throws Exception;
	
	
	/* for API End */
	
	/* Admin Start */
	public void adminCreate(Map<String, Object> param) throws Exception;
	public void adminModify(Map<String, Object> param) throws Exception;
	public void adminDelete(Map<String, Object> param) throws Exception;
	public void adminStatus(Map<String, Object> param) throws Exception;
	
	public void adminPageCreate(Map<String, Object> param) throws Exception;
	public void adminPageModify(Map<String, Object> param) throws Exception;
	public void adminPageDelete(Map<String, Object> param) throws Exception;
	
	public void adminBannerAdd(Map<String, Object> param) throws Exception;
	public void adminBannerDelete(Map<String, Object> param) throws Exception;
	public void adminBannerStatus(Map<String, Object> param) throws Exception;
	
	public void adminPlanCreate(Map<String, Object> param) throws Exception;
	public void adminPlanModify(Map<String, Object> param) throws Exception;
	public void adminPlanDelete(Map<String, Object> param) throws Exception;
	public void adminPlanStatus(Map<String, Object> param) throws Exception;
	
	public void adminPlanPageAdd(Map<String, Object> param) throws Exception;
	public void adminPlanPageDelete(Map<String, Object> param) throws Exception;
	public void adminPlanPageDeleteAll(Map<String, Object> param) throws Exception;
	
	public void adminNewAdd(Map<String, Object> param) throws Exception;
	public void adminNewDelete(Map<String, Object> param) throws Exception;
	public void adminNewStatus(Map<String, Object> param) throws Exception;
	
	public void adminWeekAdd(Map<String, Object> param) throws Exception;
	public void adminWeekDelete(Map<String, Object> param) throws Exception;
	public void adminWeekStatus(Map<String, Object> param) throws Exception;
	/* Admin End */
}
