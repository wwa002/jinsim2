package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveSiteMapper {

	public Map<String, Object> getSiteInfo(String sitecd) throws Exception;
	public String getMainUrl(String sitecd) throws Exception;
	public String getRobots(String sitecd) throws Exception;
	public Map<String, Object> getSmsCfg(Map<String, Object> param) throws Exception;
	
	// for Content
	public Map<String, Object> getMainContent(Map<String, Object> param) throws Exception;
	public Map<String, Object> getConference(Map<String, Object> param) throws Exception;
	public Map<String, Object> getContent(Map<String, Object> param) throws Exception;
	public Map<String, Object> getResourceEtc(Map<String, Object> param) throws Exception;
	public Map<String, Object> getMainTemplate(Map<String, Object> param) throws Exception;
	public Map<String, Object> getTemplate(Map<String, Object> param) throws Exception;
	
	// for Access Analysis
	public Map<String, Object> getUrl(Map<String, Object> param) throws Exception;
	public int countUrl(Map<String, Object> param) throws Exception;
	public Map<String, Object> getMinUrl(Map<String, Object> param) throws Exception;
	public Map<String, Object> getInKey(Map<String, Object> param) throws Exception;
	public int countReferer(Map<String, Object> param) throws Exception;
	public Map<String, Object> getMinReferer(Map<String, Object> param) throws Exception;
	public Map<String, Object> getOutKey(Map<String, Object> param) throws Exception;
	public Map<String, Object> getBrowser(Map<String, Object> param) throws Exception;
	public Map<String, Object> getToday(Map<String, Object> param) throws Exception;
	public Map<String, Object> getInfo(Map<String, Object> param) throws Exception;
	
	// for API Stats
	public List<Map<String, Object>> apiStatsAnalytics(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsCounter(Map<String, Object> param) throws Exception;
	public int apiStatsBrowserTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsInKeyword(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsOutKeyword(Map<String, Object> param) throws Exception;
	public int apiStatRefererTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatReferer(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> apiStatsUrl(Map<String, Object> param) throws Exception;
}
