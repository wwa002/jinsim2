package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveAPIMapper {
	
	public Map<String, Object> notice(Map<String, Object> param) throws Exception;
	public Map<String, Object> noticeMedical(Map<String, Object> param) throws Exception;
	
	/* for Member Start */
	public Map<String, Object> login(Map<String, Object> param) throws Exception;
	public Map<String, Object> loginSociety(Map<String, Object> param) throws Exception;
	public Map<String, Object> loginForHeader(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> gcds(Map<String, Object> param) throws Exception;
	public Map<String, Object> myinfo(Map<String, Object> param) throws Exception;
	public Map<String, Object> myinfoSociety(Map<String, Object> param) throws Exception;
	public Map<String, Object> myinfoTerms(Map<String, Object> param) throws Exception;
	public int checkId(Map<String, Object> param) throws Exception;
	public int checkNickname(Map<String, Object> param) throws Exception;
	public int memberNewIdx(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> findId(Map<String, Object> param) throws Exception;
	public Map<String, Object> findPassword(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> checkPassword(Map<String, Object> param) throws Exception;
	/* for Member End */
	
	/* for Refuse Start */
	public int checkRefuseSMS(Map<String, Object> param) throws Exception;
	public int checkRefuseEmail(Map<String, Object> param) throws Exception;
	/* for Refuse End */

	/* Store Start */
	public List<Map<String, Object>> storeList(Map<String, Object> param) throws Exception;
	/* Store End */
	
	/* Cert SMS Start */
	public Map<String, Object> smsCfg(Map<String, Object> param) throws Exception;
	public Map<String, Object> getSmsCert(Map<String, Object> param) throws Exception;
	/* Cert SMS End */
	
	/* Mailer Start */
	public Map<String, Object> getMailTemplate(Map<String, Object> param) throws Exception;
	/* Mailer End */
}
