package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterScheduleMapper {

	/* Server Management Start */
	public void insertServer(Map<String, Object> param) throws Exception;
	public void updateServer(Map<String, Object> param) throws Exception;
	public void electMainServer(Map<String, Object> param) throws Exception;
	public void removeOldServer() throws Exception;
	/* Server Management End */
	
	/* Board File Remove Start */
	public void removeBoardFile(Map<String, Object> param) throws Exception;
	/* Board File Remove End */
	
	/* Workshop File Remove Start */
	public void removeWorkshopFile(Map<String, Object> param) throws Exception;
	/* Workshop File Remove End */
	
	/* SMS Start */
	public void smsSendedResult(Map<String, Object> param) throws Exception;
	public void smsSendedCount(Map<String, Object> param) throws Exception;
	/* SMS END */
	
	/* Mail Start */
	public void mailSendedResult(Map<String, Object> param) throws Exception;
	public void mailSendedCount(Map<String, Object> param) throws Exception;
	/* Mail End */
}
