package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterSiteMapper {

	// for Access Analysis
	public void insertUrl(Map<String, Object> param) throws Exception;
	public void updateUrl(Map<String, Object> param) throws Exception;
	public void deleteUrls(Map<String, Object> param) throws Exception;
	
	public void updateAccessPageCounter(Map<String, Object> param) throws Exception;
	public void insertInKey(Map<String, Object> param) throws Exception;
	public void updateInKey(Map<String, Object> param) throws Exception;
	
	public void insertReferer(Map<String, Object> param) throws Exception;
	public void deleteReferers(Map<String, Object> param) throws Exception;
	
	public void insertOutKey(Map<String, Object> param) throws Exception;
	public void updateOutKey(Map<String, Object> param) throws Exception;
	
	public void insertBrowser(Map<String, Object> param) throws Exception;
	public void updateBrowser(Map<String, Object> param) throws Exception;
	
	public void insertToday(Map<String, Object> param) throws Exception;
	public void updateToday(Map<String, Object> param) throws Exception;
	
	public void insertInfo(Map<String, Object> param) throws Exception;
	public void updateInfo(Map<String, Object> param) throws Exception;
	
	public void join(Map<String, Object> param) throws Exception;
	public void expire(Map<String, Object> param) throws Exception;
	public void login(Map<String, Object> param) throws Exception;
	public void loginReferer(Map<String, Object> param) throws Exception;
	
	public void boardWrite(Map<String, Object> param) throws Exception;
	public void boardCommentWrite(Map<String, Object> param) throws Exception;
	public void boardFileUpload(Map<String, Object> param) throws Exception;
	public void boardFileDownload(Map<String, Object> param) throws Exception;
}
