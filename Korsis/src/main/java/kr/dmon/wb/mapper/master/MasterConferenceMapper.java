package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterConferenceMapper {
	
	public void voteResultAdd(Map<String, Object> param) throws Exception;
	public void questionAdd(Map<String, Object> param) throws Exception;
	
	public void register(Map<String, Object> param) throws Exception;
	
}
