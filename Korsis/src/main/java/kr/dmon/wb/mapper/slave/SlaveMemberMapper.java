package kr.dmon.wb.mapper.slave;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveMemberMapper {

	public Map<String, Object> getInfoFromIdx(Map<String, Object> param) throws Exception;
	
}
