package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveConferenceMapper {

	public List<Map<String, Object>> sesionDates(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> sessionPlaces(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> sessions(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> sessionItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> voteItems(Map<String, Object> param) throws Exception;
	
	public int checkRegister(Map<String, Object> param) throws Exception;
	public Map<String, Object> checkRegisterItem(Map<String, Object> param) throws Exception;
	
}
