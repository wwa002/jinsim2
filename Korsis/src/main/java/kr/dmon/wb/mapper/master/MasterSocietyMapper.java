package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterSocietyMapper {
	
	public void workshopCfgCreate(Map<String, Object> param) throws Exception;
	public void workshopCfgModify(Map<String, Object> param) throws Exception;
	
	public void workshopFileDelete(Map<String, Object> param) throws Exception;
	public void workshopRegistrationDelete(Map<String, Object> param) throws Exception;
	public void workshopDelete(Map<String, Object> param) throws Exception;
	public void workshopCfgDelete(Map<String, Object> param) throws Exception;
	
	public void workshopWrite(Map<String, Object> param) throws Exception;
	public void workshopLectureAdd(Map<String, Object> param) throws Exception;
	public void workshopUploadedFileUpdate(Map<String, Object> param) throws Exception;
	public void workshopModify(Map<String, Object> param) throws Exception;
	public void workshopBannerDelete(Map<String, Object> param) throws Exception;
	public void workshopModifyStatus(Map<String, Object> param) throws Exception;
	public void workshopRemovedFileUpdate(Map<String, Object> param) throws Exception;
	
	public void workshopDeleteRegistration(Map<String, Object> param) throws Exception;
	public void workshopDeleteLecture(Map<String, Object> param) throws Exception;
	public void workshopDeleteFiles(Map<String, Object> param) throws Exception;
	
	public void workshopRegistration(Map<String, Object> param) throws Exception;
	public void workshopRegistrationLecture(Map<String, Object> param) throws Exception;
	
	public void workshopRegistrationModify(Map<String, Object> param) throws Exception;
	public void workshopRegistrationStatus(Map<String, Object> param) throws Exception;
	
	public void workshopRegistrationDeleteItem(Map<String, Object> param) throws Exception;
	public void workshopRegistrationDeleteItemLecture(Map<String, Object> param) throws Exception;
	
	public void memberType(Map<String, Object> param) throws Exception;
	public void memberConfirm(Map<String, Object> param) throws Exception;
	public void memberConfirmDate(Map<String, Object> param) throws Exception;
	
	public void memberRecovery(Map<String, Object> param) throws Exception;
	
	public void adminMemberSaveBasic(Map<String, Object> param) throws Exception;
	public void adminMemberSaveSociety(Map<String, Object> param) throws Exception;
	public void adminMemberSaveAgree(Map<String, Object> param) throws Exception;
	public void adminMemberSavePermission(Map<String, Object> param) throws Exception;
	
	public void adminWorkshopRegistrationModify(Map<String, Object> param) throws Exception;
	public void adminWorkshopRegistrationDeleteLectures(Map<String, Object> param) throws Exception;
	
	public void adminHospitalAdd(Map<String, Object> param) throws Exception;
	public void adminHospitalModify(Map<String, Object> param) throws Exception;
	public void adminHospitalRemove(Map<String, Object> param) throws Exception;
}
