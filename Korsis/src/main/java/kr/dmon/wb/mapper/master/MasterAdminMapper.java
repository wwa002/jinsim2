package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterAdminMapper {
	
	/* for Admin Member Start */
	public void memberJoin(Map<String, Object> param) throws Exception;
	public void memberModify(Map<String, Object> param) throws Exception;
	public void memberDelete(Map<String, Object> param) throws Exception;
	public void memberDeleteSociety(Map<String, Object> param) throws Exception;
	public void memberDeleteTerms(Map<String, Object> param) throws Exception;
	
	public void memberGCDAdd(Map<String, Object> param) throws Exception;
	public void memberGCDDelete(Map<String, Object> param) throws Exception;
	/* for Admin Member End */
	
	/* for Admin Store Start */
	public void storeCreate(Map<String, Object> param) throws Exception;
	public void storeModify(Map<String, Object> param) throws Exception;
	public void storePhotoAdd(Map<String, Object> param) throws Exception;
	public void storePhotoRemove(Map<String, Object> param) throws Exception;
	public void storeDeletePhtos(Map<String, Object> param) throws Exception;
	public void storeDelete(Map<String, Object> param) throws Exception;
	public void storeDeleteHospital(Map<String, Object> param) throws Exception;
	
	public void storeManagerAdd(Map<String, Object> param) throws Exception;
	public void storeManagerDelete(Map<String, Object> param) throws Exception;
	
	public void storeHospitalAdd(Map<String, Object> param) throws Exception;
	public void storeHospitalDelete(Map<String, Object> param) throws Exception;
	/* for Admin Store End */
	
	/* for Admin Board Start */
	/* for Admin Board End */
	
	/* for Admin SMS Start */
	public void smsGroupAdd(Map<String, Object> param) throws Exception;
	public void smsGroupModify(Map<String, Object> param) throws Exception;
	public void smsGroupDelete(Map<String, Object> param) throws Exception;
	public void smsGroupDeleteContacts(Map<String, Object> param) throws Exception;
	
	public void smsContactAdd(Map<String, Object> param) throws Exception;
	public void smsContactModify(Map<String, Object> param) throws Exception;
	public void smsContactDelete(Map<String, Object> param) throws Exception;
	
	public void smsSend(Map<String, Object> param) throws Exception;
	public void smsSendContact(Map<String, Object> param) throws Exception;
	public void smsSendTotal(Map<String, Object> param) throws Exception;
	
	public void smsDelete(Map<String, Object> param) throws Exception;
	public void smsDeleteContact(Map<String, Object> param) throws Exception;
	/* for Admin SMS End */
	
	/* for Admin Mail Start */
	public void mailTemplateAdd(Map<String, Object> param) throws Exception;
	public void mailTemplateModify(Map<String, Object> param) throws Exception;
	public void mailTemplateDelete(Map<String, Object> param) throws Exception;
	
	public void mailTemplateFileAdd(Map<String, Object> param) throws Exception;
	public void mailTemplateFileModify(Map<String, Object> param) throws Exception;
	public void mailTemplateFileItemDelete(Map<String, Object> param) throws Exception;
	
	public void mailGroupAdd(Map<String, Object> param) throws Exception;
	public void mailGroupModify(Map<String, Object> param) throws Exception;
	public void mailGroupDelete(Map<String, Object> param) throws Exception;
	public void mailGroupDeleteContacts(Map<String, Object> param) throws Exception;
	
	public void mailContactAdd(Map<String, Object> param) throws Exception;
	public void mailContactModify(Map<String, Object> param) throws Exception;
	public void mailContactDelete(Map<String, Object> param) throws Exception;
	
	public void mailSend(Map<String, Object> param) throws Exception;
	public void mailSendContact(Map<String, Object> param) throws Exception;
	public void mailSendTotal(Map<String, Object> param) throws Exception;
	
	public void mailDelete(Map<String, Object> param) throws Exception;
	public void mailDeleteContact(Map<String, Object> param) throws Exception;
	/* for Admin Mail End */
	
	/* for Admin App System Start */
	public void systemAdd(Map<String, Object> param) throws Exception;
	public void systemModify(Map<String, Object> param) throws Exception;
	public void systemDelete(Map<String, Object> param) throws Exception;
	/* for Admin App System End */
	
	/* for Admin Conference Start */
	public void conferenceSessionCreate(Map<String, Object> param) throws Exception;
	public void conferenceSessionModify(Map<String, Object> param) throws Exception;
	public void conferenceSessionPage(Map<String, Object> param) throws Exception;
	public void conferenceSessionDelete(Map<String, Object> param) throws Exception;
	public void conferenceSessionVoteDelete(Map<String, Object> param) throws Exception;
	public void conferenceSessionVoteResultDelete(Map<String, Object> param) throws Exception;
	public void conferenceSessionQuestionDelete(Map<String, Object> param) throws Exception;
	
	public void conferenceVoteAdd(Map<String, Object> param) throws Exception;
	public void conferenceVoteModify(Map<String, Object> param) throws Exception;
	public void conferenceVoteRun(Map<String, Object> param) throws Exception;
	public void conferenceVoteDelete(Map<String, Object> param) throws Exception;
	public void conferenceVoteResultDelete(Map<String, Object> param) throws Exception;
	
	public void conferenceQuestionDelete(Map<String, Object> param) throws Exception;
	public void conferenceQuestionShow(Map<String, Object> param) throws Exception;
	public void conferenceQuestionDisplay(Map<String, Object> param) throws Exception;
	
	public void conferenceRegisterModify(Map<String, Object> param) throws Exception;
	public void conferenceRegisterDelete(Map<String, Object> param) throws Exception;
	/* for Admin Conference End */

}
