package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterResourceMapper {

	public void createFolder(Map<String, Object> param) throws Exception;
	public void uploadFile(Map<String, Object> param) throws Exception;
	public void deleteFile(Map<String, Object> param) throws Exception;
	
	/* for Board Start */
	public void uploadBoardFile(Map<String, Object> param) throws Exception;
	/* for Board End */
	
	/* for Society Workshop Start */
	public void uploadSocietyWorkshopFile(Map<String, Object> param) throws Exception;
	/* for Society Workshop End */
	
	/* for Mail Start */
	public void mailRead(Map<String, Object> param) throws Exception;
	public void mailReadCount(Map<String, Object> param) throws Exception;
	/* for Mail End */
}
