package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterAPIMapper {
	
	/* for Member Start */
	public void loginAuthUpdate(Map<String, Object> param) throws Exception;
	public void loginDateUpdate(Map<String, Object> param) throws Exception;
	public void memberJoin(Map<String, Object> param) throws Exception;
	public void memberUpdate(Map<String, Object> param) throws Exception;
	public void memberJoinTerms(Map<String, Object> param) throws Exception;
	public void memberJoinSociety(Map<String, Object> param) throws Exception;
	public void memberUpdateSociety(Map<String, Object> param) throws Exception;
	public void memberProfilePhoto(Map<String, Object> param) throws Exception;
	
	public void resetPassword(Map<String, Object> param) throws Exception;
	
	public void memberPasswordChange(Map<String, Object> param) throws Exception;
	public void memberPasswordSociety(Map<String, Object> param) throws Exception;
	
	public void memberSocietyExit(Map<String, Object> param) throws Exception;
	/* for Member End */
	
	/* for Refuse Start */
	public void refuseSms(Map<String, Object> param) throws Exception;
	public void acceptSms(Map<String, Object> param) throws Exception;
	public void refuseEmail(Map<String, Object> param) throws Exception;
	public void acceptEmail(Map<String, Object> param) throws Exception;
	/* for Refuse End */
	
	/* Cert SMS Start */
	public void smsCert(Map<String, Object> param) throws Exception;
	/* Cert SMS End */
}
