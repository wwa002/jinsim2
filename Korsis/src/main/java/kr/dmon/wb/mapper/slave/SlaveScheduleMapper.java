package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveScheduleMapper {

	/* Server Management Start */
	public int isMainServer(String host) throws Exception;
	public int checkServer(Map<String, Object> param) throws Exception;
	
	public int checkMainServer() throws Exception;
	public String getNewMainServer() throws Exception;
	/* Server Management End */
	
	/* Board File Remove Start */
	public List<Map<String, Object>> getRemovedBoardFiles() throws Exception;
	/* Board File Remove End */
	
	/* Workshop File Remove Start */
	public List<Map<String, Object>> getRemovedWorkshopFiles() throws Exception;
	/* Workshop File Remove End */
	
	/* SMS Start */
	public List<Map<String, Object>> smsList() throws Exception;
	public int checkRefuseSMS(Map<String, Object> param) throws Exception;
	/* SMS END */
	
	/* Mail Start */
	public List<Map<String, Object>> mailList() throws Exception;
	public int checkRefuseMail(Map<String, Object> param) throws Exception;
	/* Mail End */
	

	
	/* Site Remover Start */
	public List<String> listTables() throws Exception;
	/* Site Remover End */
}
