package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveStoreMapper {
	
	public List<Map<String, Object>> search(Map<String, Object> param) throws Exception;

}
