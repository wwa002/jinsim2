package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveCommonMapper {
	
	public List<Map<String, Object>> getCommonsFromCode(Map<String, Object> param) throws Exception;
	public Map<String, Object> getCommonItem(Map<String, Object> param) throws Exception;

	/* Keyword Start */
	public int keywordListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> keywordList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> keywordListAll(Map<String, Object> param) throws Exception;
	public Map<String, Object> keywordSort(Map<String, Object> param) throws Exception;
	/* Keyword End */
	
}
