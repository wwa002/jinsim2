package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveBuilderMapper {
	
	/* for Dashboard Start */
	public Map<String, Object> dashboard() throws Exception;
	public List<Map<String, Object>> dashboardCounter() throws Exception;
	/* for Dashboard End */
	
	/* for Builder Member Start */
	public Map<String, Object> login(Map<String, Object> param) throws Exception;
	public Map<String, Object> getBuilderInfo(int idx) throws Exception;
	/* for Builder Member End */
	
	/* for Builder Start */
	public List<Map<String, Object>> getSites() throws Exception;
	public int checkSiteCd(Map<String, Object> param) throws Exception;
	public int checkGradeCd(Map<String, Object> param) throws Exception;
	public int checkId(Map<String, Object> param) throws Exception;
	public int checkBoard(Map<String, Object> param) throws Exception;
	public int checkConference(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> siteListAll(Map<String, Object> param) throws Exception;
	/* for Builder End */
	
	/* for Server Start */
	public List<Map<String, Object>> serverListAll(Map<String, Object> param) throws Exception;
	/* for Server End */
	
	/* for System Start */
	public List<Map<String, Object>> systemUrlList(Map<String, Object> param) throws Exception;
	public int systemUrlCount(Map<String, Object> param) throws Exception;
	public Map<String, Object> systemSite(String sitecd) throws Exception;
	public Map<String, Object> systemSiteCfg(String sitecd) throws Exception;
	public Map<String, Object> systemSMSCfg(String sitecd) throws Exception;
	public Map<String, Object> systemAppCfg(String sitecd) throws Exception;
	public Map<String, Object> systemSocietyCfg(String sitecd) throws Exception;
	
	public int systemMemberListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> systemMemberList(Map<String, Object> param) throws Exception;
	
	public int systemMemberGradeListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> systemMemberGradeList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> systemMemberGradeListAll(Map<String, Object> param) throws Exception;
	
	public int systemMemberNewIdx(Map<String, Object> param) throws Exception;
	public Map<String, Object> systemMemberItemForIdx(Map<String, Object> param) throws Exception;
	public Map<String, Object> systemMemberItemForId(Map<String, Object> param) throws Exception;
	
	/* for System End */
	
	/* for Template Start */
	public int templateListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> templateList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> templateListAll(Map<String, Object> param) throws Exception;
	public Map<String, Object> templateItem(Map<String, Object> param) throws Exception;
	/* for Template End */
	
	/* for Content Start */
	public int contentListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> contentList(Map<String, Object> param) throws Exception;
	public Map<String, Object> contentItem(Map<String, Object> param) throws Exception;
	/* for Content End */
	
	/* for Etc Start */
	public int etcListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> etcList(Map<String, Object> param) throws Exception;
	public Map<String, Object> etcItem(Map<String, Object> param) throws Exception;
	/* for Etc End */
	
	/* for Board Start */
	public int boardListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> boardList(Map<String, Object> param) throws Exception;
	public Map<String, Object> boardItem(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> boardFileForDelete(Map<String, Object> param) throws Exception;
	/* for Board End */
	
	/* for Conference Start */
	public int conferenceListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> conferenceList(Map<String, Object> param) throws Exception;
	public Map<String, Object> conferenceItem(Map<String, Object> param) throws Exception;
	/* for Conference End */
}
