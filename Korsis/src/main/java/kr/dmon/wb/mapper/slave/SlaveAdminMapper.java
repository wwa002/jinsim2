package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveAdminMapper {

	/* for Admin Dashboard Start */
	public Map<String, Object> dashboardMember(Map<String, Object> param) throws Exception;
	public Map<String, Object> dashboardDB(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> dashboardCounter(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> dashboardBoards(Map<String, Object> param) throws Exception;
	
	/* for Admin Dashboard End */
	
	/* for Admin Member Start */
	public Map<String, Object> getAdminInfo(Map<String, Object> param) throws Exception;
	public Map<String, Object> getSocietyCfg(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> gradeList(Map<String, Object> param) throws Exception;
	public int memberListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> memberList(Map<String, Object> param) throws Exception;
	public Map<String, Object> memberInfo(Map<String, Object> param) throws Exception;
	public Map<String, Object> memberTerms(Map<String, Object> param) throws Exception;
	public Map<String, Object> memberInfoSociety(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> memberInGroup(Map<String, Object> param) throws Exception;
	
	public int checkId(Map<String, Object> param) throws Exception;
	public int memberNewIdx(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> memberItemForIdx(Map<String, Object> param) throws Exception;
	public Map<String, Object> memberItemForId(Map<String, Object> param) throws Exception;
	
	public int memberGCDCheck(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> memberGCDList(Map<String, Object> param) throws Exception;
	/* for Admin Member End */
	
	/* for Admin Store Start */
	public int storeListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> storeList(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> storeItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> storePhotoList(Map<String, Object> param) throws Exception;
	public Map<String, Object> storePhotoItem(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> storeManagers(Map<String, Object> param) throws Exception;
	public int storeManagerCheck(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> storeManagersListAll(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> storeHospitals(Map<String, Object> param) throws Exception;
	public int storeHospitalCheck(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> storeHospitalsListAll(Map<String, Object> param) throws Exception;
	
	/* for Admin Store End */
	
	/* for Admin Board Start */
	public List<Map<String, Object>> getBoardList(Map<String, Object> param) throws Exception;
	/* for Admin Board End */
	
	/* for Admin Workshop Start */
	public List<Map<String, Object>> getWorkshopList(Map<String, Object> param) throws Exception;
	/* for Admin Workshop End */
	
	/* for Admin Conference Start */
	public List<Map<String, Object>> getConferenceList(Map<String, Object> param) throws Exception;
	public Map<String, Object> getConferenceItem(Map<String, Object> param) throws Exception;
	public Map<String, Object> getConferenceSessionItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> conferenceQuestionList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> conferenceQuestionListPrincipal(Map<String, Object> param) throws Exception;
	public Map<String, Object> conferenceQuestionListMonitor(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> conferenceSessionVoteList(Map<String, Object> param) throws Exception;
	public Map<String, Object> conferenceSessionVoteItem(Map<String, Object> param) throws Exception;
	public Map<String, Object> conferenceSessionVoteResult(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> conferenceSessionVoteResultList(Map<String, Object> param) throws Exception;
	
	public int conferenceRegisterListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> conferenceRegisterList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> conferenceRegisterListAll(Map<String, Object> param) throws Exception;
	public Map<String, Object> conferenceRegisterItem(Map<String, Object> param) throws Exception;
	/* for Admin Conference End */
	
	/* for Admin SMS Start */
	public int smsGroupListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsGroupList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsGroupListAll(Map<String, Object> param) throws Exception;
	
	public int smsContactCheck(Map<String, Object> param) throws Exception;
	public int smsContactListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsContactList(Map<String, Object> param) throws Exception;
	
	public int smsRefuseListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsRefuseList(Map<String, Object> param) throws Exception;
	
	public int smsNewIdx(Map<String, Object> param) throws Exception;
	
	public int smsSendListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> smsSendListList(Map<String, Object> param) throws Exception;
	/* for Admin SMS End */
	
	/* for Admin Mail Start */
	public int mailTemplateListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailTemplateList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailTemplateListAll(Map<String, Object> param) throws Exception;
	public Map<String, Object> mailTemplateItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailTemplateFileList(Map<String, Object> param) throws Exception;
	public Map<String, Object> mailTemplateFileItem(Map<String, Object> param) throws Exception;
	
	public int mailGroupListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailGroupList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailGroupListAll(Map<String, Object> param) throws Exception;
	
	public int mailContactCheck(Map<String, Object> param) throws Exception;
	public int mailContactListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailContactList(Map<String, Object> param) throws Exception;
	
	public int mailRefuseListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailRefuseList(Map<String, Object> param) throws Exception;
	
	public int mailNewIdx(Map<String, Object> param) throws Exception;
	
	public int mailSendListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> mailSendList(Map<String, Object> param) throws Exception;
	public Map<String, Object> mailSendItem(Map<String, Object> param) throws Exception;
	/* for Admin Mail End */
	
	/* for Admin App System Start */
	public int systemListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> systemList(Map<String, Object> param) throws Exception;
	/* for Admin App System End */
}
