package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterDBMapper {

	/* Hospital Search Start */
	
	public void hospitalSubjectAdd(Map<String, Object> param) throws Exception;
	public void hospitalSubjectDelete(Map<String, Object> param) throws Exception;

	/* Hospital Search End */
	
	/* Hospital Start */
	public void hospitalAdd(Map<String, Object> param) throws Exception;
	public void hospitalDelete(Map<String, Object> param) throws Exception;
	public void hospitalDeleteDetail(Map<String, Object> param) throws Exception;
	public void hospitalDeleteFacility(Map<String, Object> param) throws Exception;
	public void hospitalDeleteSpcapp(Map<String, Object> param) throws Exception;
	public void hospitalDeleteSpecial(Map<String, Object> param) throws Exception;
	public void hospitalDeleteSubject(Map<String, Object> param) throws Exception;
	public void hospitalDeleteSubjectMapping(Map<String, Object> param) throws Exception;
	public void hospitalDeleteTransport(Map<String, Object> param) throws Exception;
	public void hospitalModifyBasic(Map<String, Object> param) throws Exception;
	public void hospitalModifyDoctor(Map<String, Object> param) throws Exception;
	public void hospitalModifyDetail(Map<String, Object> param) throws Exception;
	
	public void hospitalAddRead(Map<String, Object> param) throws Exception;
	public void hospitalRating(Map<String, Object> param) throws Exception;
	public void hospitalFavorite(Map<String, Object> param) throws Exception;
	
	public void hospitalCommentAdd(Map<String, Object> param) throws Exception;
	public void hospitalPhotoAdd(Map<String, Object> param) throws Exception;
	
	/* Hospital End */
	
	/* Pharmacy Start */
	public void pharmacyAdd(Map<String, Object> param) throws Exception;
	public void pharmacyDelete(Map<String, Object> param) throws Exception;
	public void pharmacyModifyBasic(Map<String, Object> param) throws Exception;
	public void pharmacyModifyDetail(Map<String, Object> param) throws Exception;
	
	public void pharmacyAddRead(Map<String, Object> param) throws Exception;
	public void pharmacyRating(Map<String, Object> param) throws Exception;
	public void pharmacyFavorite(Map<String, Object> param) throws Exception;
	
	public void pharmacyCommentAdd(Map<String, Object> param) throws Exception;
	public void pharmacyPhotoAdd(Map<String, Object> param) throws Exception;
	/* Pharmacy End */
	
	/* Cast Start */
	public void castCreate(Map<String, Object> param) throws Exception;
	public void castModify(Map<String, Object> param) throws Exception;
	public void castDelete(Map<String, Object> param) throws Exception;
	public void castReadDelete(Map<String, Object> param) throws Exception;
	
	public void castBanner(Map<String, Object> param) throws Exception;
	public void castStatus(Map<String, Object> param) throws Exception;
	
	public void castPageCreate(Map<String, Object> param) throws Exception;
	public void castPageModify(Map<String, Object> param) throws Exception;
	public void castPageDelete(Map<String, Object> param) throws Exception;
	
	
	public void castAddRead(Map<String, Object> param) throws Exception;
	public void castFavorite(Map<String, Object> param) throws Exception;
	public void castRecommend(Map<String, Object> param) throws Exception;
	/* Cast End */
	
	/* Info DB Start */
	public void infoCreate(Map<String, Object> param) throws Exception;
	public void infoModify(Map<String, Object> param) throws Exception;
	public void infoDelete(Map<String, Object> param) throws Exception;
	public void infoStatus(Map<String, Object> param) throws Exception;
	
	public void infoGroupCreate(Map<String, Object> param) throws Exception;
	public void infoGroupModify(Map<String, Object> param) throws Exception;
	public void infoGroupDelete(Map<String, Object> param) throws Exception;
	
	public void infoContentCreate(Map<String, Object> param) throws Exception;
	public void infoContentModify(Map<String, Object> param) throws Exception;
	public void infoContentDelete(Map<String, Object> param) throws Exception;
	
	public void infoContentReadDelete(Map<String, Object> param) throws Exception;
	
	public void infoContentAddRead(Map<String, Object> param) throws Exception;
	public void infoContentFavorite(Map<String, Object> param) throws Exception;
	public void infoContentRecommend(Map<String, Object> param) throws Exception;
	
	public void infoPageCreate(Map<String, Object> param) throws Exception;
	public void infoPageModify(Map<String, Object> param) throws Exception;
	public void infoPageDelete(Map<String, Object> param) throws Exception;
	/* Info DB End */
}
