package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveSocietyMapper {
	
	public int checkWorkshop(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> workshopFileForDelete(Map<String, Object> param) throws Exception;

	public int workshopCfgListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> workshopCfgList(Map<String, Object> param) throws Exception;
	public Map<String, Object> workshopCfgItem(Map<String, Object> param) throws Exception;
	
	public int workshopNewIdx(Map<String, Object> param) throws Exception;
	
	public int workshopListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> workshopList(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> workshopView(Map<String, Object> param) throws Exception;
	public Map<String, Object> workshopViewForFile(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> workshopViewFileList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> workshopViewLectureList(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> workshopMonthlyList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> workshopMonthlyListCalendar(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> workshopRegistrationInfo(Map<String, Object> param) throws Exception;
	public Map<String, Object> workshopRegistrationLectureInfo(Map<String, Object> param) throws Exception;
	
	public int workshopRegistrationCheckCount(Map<String, Object> param) throws Exception;
	public int workshopRegistrationNewIdx(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> workshopRegistrationCheck(Map<String, Object> param) throws Exception;
	public Map<String, Object> workshopRegistrationItem(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> workshopRegistrationCheckLecture(Map<String, Object> param) throws Exception;

	public Map<String, Object> memberCount(Map<String, Object> param) throws Exception;
	public int memberListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> memberList(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> hospitalLocationSearch(Map<String, Object> param) throws Exception;
	public int hospitalListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> hospitalList(Map<String, Object> param) throws Exception;
	
	// for Admin
	public int adminMemberListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminMemberList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminMemberListExcel(Map<String, Object> param) throws Exception;
	public Map<String, Object> adminMemberItem(Map<String, Object> param) throws Exception;
	
	public int adminWorkshopRegistrationListAllTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminWorkshopRegistrationListAllList(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminWorkshopRegistrationListAllWorkshops(Map<String, Object> param) throws Exception;
	
	public int adminWorkshopRegistrationListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminWorkshopRegistrationList(Map<String, Object> param) throws Exception;
	public Map<String, Object> adminWorkshopRegistrationCost(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminWorkshopRegistrationExcel(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminWorkshopRegistrationListLecture(Map<String, Object> param) throws Exception;

	
	public int adminHospitalListTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> adminHospitalList(Map<String, Object> param) throws Exception;
	public Map<String, Object> adminHospitalItem(Map<String, Object> param) throws Exception;
	
	
}
