package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveBoardMapper {

	public Map<String, Object> getBoardCfg(Map<String, Object> param) throws Exception;
	
	public int boardNewIdx(Map<String, Object> param) throws Exception;
	
	/* for List */
	public int listTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> list(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> listBanner(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> listNotice(Map<String, Object> param) throws Exception;
	
	/* for PPPP Start */
	public int listPPPPPartnerTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> listPPPPPartner(Map<String, Object> param) throws Exception;
	/* for PPPP End */
	
	/* for BEAUTYRUN Start */
	public int listBEAUTYRUNPartnerTotal(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> listBEAUTYRUNPartner(Map<String, Object> param) throws Exception;
	/* for BEAUTYRUN End */
	
	/* for View */
	public Map<String, Object> view(Map<String, Object> param) throws Exception;
	public Map<String, Object> flagStatus(Map<String, Object> param) throws Exception;
	public Map<String, Object> viewForPermission(Map<String, Object> param) throws Exception;
	public Map<String, Object> viewForFirstHidden(Map<String, Object> param) throws Exception;
	public List<Map<String, Object>> viewFileList(Map<String, Object> param) throws Exception;
	public int viewCheckRead(Map<String, Object> param) throws Exception;
	public Map<String, Object> fileItem(Map<String, Object> param) throws Exception;
	public int checkPassword(Map<String, Object> param) throws Exception;
	
	/* for Reply */
	public Map<String, Object> replyThread(Map<String, Object> param) throws Exception;
	
	/* for Delete */
	public int deleteThread(Map<String, Object> param) throws Exception;
	
	/* for Comment */
	public List<Map<String, Object>> commentList(Map<String, Object> param) throws Exception;
	public int commentNewIdx(Map<String, Object> param) throws Exception;
	public Map<String, Object> commentItem(Map<String, Object> param) throws Exception;
	public int commentDeleteThread(Map<String, Object> param) throws Exception;
	public Map<String, Object> commentReplyThread(Map<String, Object> param) throws Exception;
	public int checkCommentPassword(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> popupList(Map<String, Object> param) throws Exception;
}
