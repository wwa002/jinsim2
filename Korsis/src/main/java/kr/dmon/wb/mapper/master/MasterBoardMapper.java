package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterBoardMapper {
	
	// for Write
	public void write(Map<String, Object> param) throws Exception;
	
	// for Modify
	public void modify(Map<String, Object> param) throws Exception;
	public void modifyCategory(Map<String, Object> param) throws Exception;
	public void modifyCount(Map<String, Object> param) throws Exception;
	public void modifyBanner(Map<String, Object> param) throws Exception;
	
	// for Reply
	public void reply(Map<String, Object> param) throws Exception;
	
	// for Delete
	public void delete(Map<String, Object> param) throws Exception;
	public void deleteComments(Map<String, Object> param) throws Exception;
	public void deleteReads(Map<String, Object> param) throws Exception;
	public void deleteFiles(Map<String, Object> param) throws Exception;
	
	// for View
	public void viewReadInsert(Map<String, Object> param) throws Exception;
	public void viewCount(Map<String, Object> param) throws Exception;
	public void viewCountAbsolute(Map<String, Object> param) throws Exception;
	
	public void favorite(Map<String, Object> param) throws Exception;
	public void favoriteUp(Map<String, Object> param) throws Exception;
	public void favoriteDown(Map<String, Object> param) throws Exception;
	public void recommend(Map<String, Object> param) throws Exception;
	public void recommendUp(Map<String, Object> param) throws Exception;
	public void recommendDown(Map<String, Object> param) throws Exception;
	
	// for Upload
	public void uploadedFileUpdate(Map<String, Object> param) throws Exception;
	public void removedFileUpdate(Map<String, Object> param) throws Exception;
	public void updateMainPicture(Map<String, Object> param) throws Exception;
	
	// for Comment
	public void commentWrite(Map<String, Object> param) throws Exception;
	public void commentDelete(Map<String, Object> param) throws Exception;
	public void commentModify(Map<String, Object> param) throws Exception;
	public void commentReply(Map<String, Object> param) throws Exception;
	public void commentCount(Map<String, Object> param) throws Exception;
}
