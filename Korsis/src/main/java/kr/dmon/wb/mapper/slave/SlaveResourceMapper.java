package kr.dmon.wb.mapper.slave;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="slaveSqlSessionFactory")
public interface SlaveResourceMapper {

	public List<Map<String, Object>> listForJSTree(Map<String, Object> param) throws Exception;
	
	public int checkFolder(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> listForFolder(Map<String, Object> param) throws Exception;
	
	public List<Map<String, Object>> listForResource(Map<String, Object> param) throws Exception;
	
	public Map<String, Object> itemForFile(Map<String, Object> param) throws Exception;
	
	/* for Board Start */
	public int boardFileNewIdx(Map<String, Object> param) throws Exception;
	/* for Board End */
	
	/* for Society Workshop Start */
	public int societyWorkshopFileNewIdx(Map<String, Object> param) throws Exception;
	/* for Society Workshop End */
}
