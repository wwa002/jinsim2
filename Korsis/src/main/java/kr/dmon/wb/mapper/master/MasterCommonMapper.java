package kr.dmon.wb.mapper.master;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterCommonMapper {

	public void excelLog(Map<String, Object> param) throws Exception;
	
	/* Keyword Start */
	
	public void keywordCreate(Map<String, Object> param) throws Exception;
	public void keywordDelete(Map<String, Object> param) throws Exception;
	public void keywordSortUpdate(Map<String, Object> param) throws Exception;
	public void keywordSortUp(Map<String, Object> param) throws Exception;
	public void keywordSortDown(Map<String, Object> param) throws Exception;
	
	/* Keyword End */
	
}
