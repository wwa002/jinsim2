package kr.dmon.wb.mapper.master;

import java.util.Map;

import javax.annotation.Resource;

@Resource(name="masterSqlSessionFactory")
public interface MasterBuilderMapper {
	
	/* for Builder Member Start */
	public void logined(Map<String, Object> param) throws Exception;
	/* for Builder Member End */
	
	/* for Builder Start */
	public void siteCreate(Map<String, Object> param) throws Exception;
	public void siteCreateConfig(Map<String, Object> param) throws Exception;
	public void siteModify(Map<String, Object> param) throws Exception;
	/* for Builder End */
	
	/* for System Start */
	public void systemUrlAdd(Map<String, Object> param) throws Exception;
	public void systemUrlModify(Map<String, Object> param) throws Exception;
	public void systemUrlDelete(Map<String, Object> param) throws Exception;
	public void systemUrlMain(Map<String, Object> param) throws Exception;
	public void systemCfg(Map<String, Object> param) throws Exception;
	public void systemCfgApp(Map<String, Object> param) throws Exception;
	public void systemCfgSociety(Map<String, Object> param) throws Exception;
	public void systemAddCfgApp(Map<String, Object> param) throws Exception;
	public void systemAddCfgSociety(Map<String, Object> param) throws Exception;
	
	public void systemMemberGradeAdd(Map<String, Object> param) throws Exception;
	public void systemMemberGradeModify(Map<String, Object> param) throws Exception;
	public void systemMemberGradeDelete(Map<String, Object> param) throws Exception;
	
	public void systemMemberJoin(Map<String, Object> param) throws Exception;
	public void systemMemberModify(Map<String, Object> param) throws Exception;
	public void systemMemberDelete(Map<String, Object> param) throws Exception;
	
	public void systemSmsCfg(Map<String, Object> param) throws Exception;
	public void systemAppCfg(Map<String, Object> param) throws Exception;
	public void systemRobot(Map<String, Object> param) throws Exception;
	/* for System End */
	
	/* for Template Start */
	public void templateCreate(Map<String, Object> param) throws Exception;
	public void templateCopy(Map<String, Object> param) throws Exception;
	public void templateModify(Map<String, Object> param) throws Exception;
	public void templateDelete(Map<String, Object> param) throws Exception;
	public void templateMain(Map<String, Object> param) throws Exception;
	/* for Template End */
	
	/* for Content Start */
	public void contentCreate(Map<String, Object> param) throws Exception;
	public void contentCopy(Map<String, Object> param) throws Exception;
	public void contentModify(Map<String, Object> param) throws Exception;
	public void contentDelete(Map<String, Object> param) throws Exception;
	public void contentMain(Map<String, Object> param) throws Exception;
	/* for Content End */
	
	/* for ETC Start */
	public void etcCreate(Map<String, Object> param) throws Exception;
	public void etcCopy(Map<String, Object> param) throws Exception;
	public void etcModify(Map<String, Object> param) throws Exception;
	public void etcDelete(Map<String, Object> param) throws Exception;
	/* for ETC End */
	
	/* for Board Start */
	public void boardCreate(Map<String, Object> param) throws Exception;
	public void boardModify(Map<String, Object> param) throws Exception;
	
	public void boardFileDelete(Map<String, Object> param) throws Exception;
	public void boardReadDelete(Map<String, Object> param) throws Exception;
	public void boardCommentDelete(Map<String, Object> param) throws Exception;
	public void boardDelete(Map<String, Object> param) throws Exception;
	public void boardCfgDelete(Map<String, Object> param) throws Exception;
	/* for Board End */
	
	/* for Board Start */
	public void conferenceCreate(Map<String, Object> param) throws Exception;
	public void conferenceModify(Map<String, Object> param) throws Exception;
	/* for Board End */
}
