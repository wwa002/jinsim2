package kr.dmon.wb.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDiff {

	public static int dateCount(String date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return numberOfDays(date, sdf.format(new Date()));
	}
	
	public static int numberOfDays(String fromDate,String toDate) { 
		java.util.Calendar cal1 = new java.util.GregorianCalendar();
		java.util.Calendar cal2 = new java.util.GregorianCalendar();

		//split year, month and days from the date using StringBuffer.
		StringBuffer sBuffer = new StringBuffer(fromDate);
		String yearFrom = sBuffer.substring(0,4);
		String monFrom = sBuffer.substring(4,6);
		String ddFrom = sBuffer.substring(6,8);
		int intYearFrom = Integer.parseInt(yearFrom);
		int intMonFrom = Integer.parseInt(monFrom);
		int intDdFrom = Integer.parseInt(ddFrom);

		// set the fromDate in java.util.Calendar
		cal1.set(intYearFrom, intMonFrom, intDdFrom);

		//split year, month and days from the date using StringBuffer.
		StringBuffer sBuffer1 = new StringBuffer(toDate);
		String yearTo = sBuffer1.substring(0,4);
		String monTo = sBuffer1.substring(4,6);
		String ddTo = sBuffer1.substring(6,8);
		int intYearTo = Integer.parseInt(yearTo);
		int intMonTo = Integer.parseInt(monTo);
		int intDdTo = Integer.parseInt(ddTo);
	
		// set the toDate in java.util.Calendar
		cal2.set(intYearTo, intMonTo, intDdTo);
	
		//call method daysBetween to get the number of days between two dates
		int days = daysBetween(cal1.getTime(),cal2.getTime());
		return days;
	}
	
	public static int daysBetween(Date d1, Date d2) {
		return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}

	public static int diffHour(String format, String date1, String date2) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat(format);
		return diffHour(df.parse(date1), df.parse(date2));
	}
	
	public static int diffHour(Date date1, Date date2) {
		/*
		long diffSeconds = timeDifferenceMilliseconds / 1000;
		long diffMinutes = timeDifferenceMilliseconds / (60 * 1000);
		long diffHours = timeDifferenceMilliseconds / (60 * 60 * 1000);
		long diffDays = timeDifferenceMilliseconds / (60 * 60 * 1000 * 24);
		long diffWeeks = timeDifferenceMilliseconds / (60 * 60 * 1000 * 24 * 7);
		long diffMonths = (long) (timeDifferenceMilliseconds / (60 * 60 * 1000 * 24 * 30.41666666));
		long diffYears = (long) (timeDifferenceMilliseconds / (60 * 60 * 1000 * 24 * 365));
		*/
		return (int) ((date1.getTime() - date2.getTime()) / (60 * 60 * 1000));
	}
}
