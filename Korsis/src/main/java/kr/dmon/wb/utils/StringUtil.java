package kr.dmon.wb.utils;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;

public class StringUtil {

	/**
	 * String 값이 null이거나 빈값이면 true
	 * @param val
	 * @return
	 */
	public static boolean isEmpty(String val){
		if ( val == null || "".equals(val) )
			return true;
		else return false;
	}
	
	public static boolean isEmpty(Object obj) {
		if (obj == null) {
			return true;
		}
		if ("".equals(obj.toString().trim())) {
			return true;
		}
		return false;
	}
	
	public static String getContent(Object obj) {
		if (isEmpty(obj))
			return "";
		return obj.toString();
	}
	
	public static String getContent(Object obj, String def) {
		if (isEmpty(obj)) {
			return def;
		}
		return obj.toString();
	}
	
	public static String getContent(Object[] objs, int position, String def) {
		if (isEmpty(objs)) {
			return def;
		}
		if (objs.length < position+1) {
			return def;
		}
		return getContent(objs[position], def);
	}
	
	/**
	 * string 값이 null일때 빈문자 리턴
	 * @param val
	 * @return
	 */
	public static String nullToEmpty(String val){
		if ( val == null ) return "";
		else return val;
	}
	
	/**
	 * val 값이 null일때 기본값 리턴 
	 * @param val
	 * @param defaultVal
	 * @return
	 */
	public static String NVL(String val, String defaultVal){
		if ( val == null ) return defaultVal;
		else return val;
	}
	
	/**
	 * html 문자를 태그로 변경 
	 * @param strValue
	 * @return
	 */
	public static String htmlOn(String strValue){
		if ( strValue == null ) return null;
		strValue = strValue.replace("&amp;", "&");
		strValue = strValue.replace("&amp;","&");
		strValue = strValue.replace("&#39;","'" );
		strValue = strValue.replace("&#124;","|" );
		strValue = strValue.replace("&lt;","<");
		strValue = strValue.replace("&gt",">");
		strValue = strValue.replace("&quot;","\"");
		strValue = strValue.replace("‡", "<br>");
		return strValue;
	}
	
	/**
	 * Integer변환 가능 스트링여부 확인 
	 * @param val
	 * @return
	 */
	public static boolean parseIntAvail(String val){
		if ( val == null || "".equals(val) ) return false;
		try{
			Integer.parseInt(val);
			return true;
		}catch (Exception e ){
			return false;
		}
	}

	/**
	 * java replaceAll 처리용 
	 * @param str
	 * @param regex
	 * @param replacement
	 * @return
	 */
	public static String replaceAll(String str, String regex, String replacement){
		if ( isEmpty(str) || isEmpty(regex) || isEmpty(replacement) ) return "";
		return str.replaceAll(regex, replacement);
	}
	
	/**
	 * Email Check 
	 * @param str
	 * @return
	 */
	public static boolean validEmail(String str) {
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}
	
	public static boolean validMobile(String str) {
		Pattern pattern = Pattern.compile("^(?:(010-[0-9]{4})|(01[1|6|7|8|9]-[0-9]{3,4}))-([0-9]{4})$");
		Matcher matcher = pattern.matcher(str);
		return matcher.matches();
	}
	
	public static String addSlashes(String s) {
        s = s.replaceAll("\\\\", "\\\\\\\\");
        s = s.replaceAll("\\n", "\\\\n");
        s = s.replaceAll("\\r", "\\\\r");
        s = s.replaceAll("\\00", "\\\\0");
        s = s.replaceAll("'", "\\\\'");
        s = s.replaceAll("\"","&quote;");
        return s;
    }
	
	public static boolean isNumeric(String s) {  
	    return s.matches("[-+]?\\d*\\.?\\d+");  
	}
	
	public static String stripTags(String html) {
		return Jsoup.parse(html).text();
	}
	
	public static String shareContent(String html) {
		String content = Jsoup.parse(html).text(); 
		return addSlashes(content.length() > 255 ? content.substring(0, 255) + "..." : content);
	}
	
	public static String shareContent(String html, int length) {
		String content = Jsoup.parse(html).text(); 
		return addSlashes(content.length() > length ? content.substring(0, length) + "..." : content);
	}
	
	public static String getSearchEngine(String referer) {
		if (isEmpty(referer)) {
			return "etc";
		}
		String[] engine = {"naver", "nate", "daum", "yahoo", "google"};
		for(String e : engine) {
			if (referer.contains(e)) {
				return e;
			}
		}
		return "etc";
	}
	
	public static String getSearchKeyword(String referer) {
		if (isEmpty(referer)) {
			return "";
		}
		String[] urlexp = referer.split("\\?");
		if (urlexp == null || urlexp.length < 2) {
			return "";
		}
		if (StringUtil.isEmpty(urlexp[1])) {
			return "";
		}
		String[] queexp = urlexp[1].split("&");
		for (String e : queexp) {
			String[] varexp = e.split("=");
			if (varexp == null || varexp.length < 2) {
				continue;
			}
			if ((varexp[0].equals("query") || varexp[0].equals("q") || varexp[0].equals("p")) && !isNumeric(varexp[1])) {
				return varexp[1];
			}
		}
		return "";
	}
	
	public static final String[] BROWSERS = {"MSIE 11","MSIE 10","MSIE 9","MSIE 8","MSIE 7","MSIE 6","Firefox","Opera","Chrome","Safari"};
	public static String getBrowser(String userAgent) {
		if (isEmpty(userAgent)) {
			return "";
		}
		for(String e : BROWSERS) {
			if (userAgent.contains(e)) {
				return e;
			}
		}
		return "";
	}
	
	public static final String randomString(int length) {
		String[] str = new String[]{
				"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
				, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
				, "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
				, "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "[", "]", "{", "}", "-", "+", "="
		};
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < length; i++) {
			sb.append(str[random.nextInt(str.length)]);
		}
		return sb.toString();
	}
}
