package kr.dmon.wb.schedule;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.dmon.wb.service.ScheduleService;

@Component
public class Scheduler {
	
	private static Logger logger = Logger.getLogger(Scheduler.class);

	@Autowired
	ScheduleService scheduleService;
	
	@Scheduled(fixedRate = 60000)
	private void serverStatus() throws Exception {
		scheduleService.serverStatus();
	}
	
	@Scheduled(fixedRate = 300000)
	private void electMainServer() throws Exception {
		scheduleService.electMainServer();
	}
	
	@Scheduled(fixedRate = 5000)
	private void sendSMS() throws Exception {
		scheduleService.sendSMS();
	}
	
	@Scheduled(fixedRate = 5000)
	private void sendMail() throws Exception {
		scheduleService.sendMail();
	}

	@Scheduled(cron="0 0 1 * * *")
	public void removeTemporaryFiles() throws Exception {
		try {
			scheduleService.removeTemporaryFiles();
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@Scheduled(cron="0 0 4 * * *")
	public void removeBoardFiles() throws Exception {
		if (scheduleService.isMainServer()) {
			try {
				scheduleService.removeBoardFiles();
				scheduleService.removeWorkshopFiles();
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
}
