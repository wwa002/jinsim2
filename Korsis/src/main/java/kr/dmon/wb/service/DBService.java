package kr.dmon.wb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterDBMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveDBMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.StringUtil;

@Service
public class DBService extends BaseService {
	
	private static Logger logger = Logger.getLogger(BaseService.class);

	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterDBMapper mDBMapper;
	
	@Autowired
	SlaveDBMapper sDBMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	/* Medical DB Start */
	@Transactional
	public RESTResult apiHospitalAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("yadmNm"))) {
			throw new Exception("병원명을 입력해주세요.");
		}
		String ykiho = UUID.randomUUID().toString().replaceAll("-", "");
		param.put("ykiho", ykiho);
		mDBMapper.hospitalAdd(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiHospitalModifyBasic(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("병원 고유정보가 누락되었습니다.");
		}
		if (!StringUtil.isEmpty(param.get("estbDd"))) {
			param.put("estbDd", param.get("estbDd").toString().replaceAll("-", ""));
		}
		mDBMapper.hospitalModifyBasic(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiHospitalModifyDoctor(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("병원 고유정보가 누락되었습니다.");
		}
		mDBMapper.hospitalModifyDoctor(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiHospitalModifyDetail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("병원 고유정보가 누락되었습니다.");
		}
		param.put("trmtSunStart", StringUtil.isEmpty(param.get("trmtSunStart"))? null : param.get("trmtSunStart").toString().replaceAll(":", ""));
		param.put("trmtSunEnd", StringUtil.isEmpty(param.get("trmtSunEnd"))? null : param.get("trmtSunEnd").toString().replaceAll(":", ""));
		param.put("trmtMonStart", StringUtil.isEmpty(param.get("trmtMonStart"))? null : param.get("trmtMonStart").toString().replaceAll(":", ""));
		param.put("trmtMonEnd", StringUtil.isEmpty(param.get("trmtMonEnd"))? null : param.get("trmtMonEnd").toString().replaceAll(":", ""));
		param.put("trmtTueStart", StringUtil.isEmpty(param.get("trmtTueStart"))? null : param.get("trmtTueStart").toString().replaceAll(":", ""));
		param.put("trmtTueEnd", StringUtil.isEmpty(param.get("trmtTueEnd"))? null : param.get("trmtTueEnd").toString().replaceAll(":", ""));
		param.put("trmtWedStart", StringUtil.isEmpty(param.get("trmtWedStart"))? null : param.get("trmtWedStart").toString().replaceAll(":", ""));
		param.put("trmtWedEnd", StringUtil.isEmpty(param.get("trmtWedEnd"))? null : param.get("trmtWedEnd").toString().replaceAll(":", ""));
		param.put("trmtThuStart", StringUtil.isEmpty(param.get("trmtThuStart"))? null : param.get("trmtThuStart").toString().replaceAll(":", ""));
		param.put("trmtThuEnd", StringUtil.isEmpty(param.get("trmtThuEnd"))? null : param.get("trmtThuEnd").toString().replaceAll(":", ""));
		param.put("trmtFriStart", StringUtil.isEmpty(param.get("trmtFriStart"))? null : param.get("trmtFriStart").toString().replaceAll(":", ""));
		param.put("trmtFriEnd", StringUtil.isEmpty(param.get("trmtFriEnd"))? null : param.get("trmtFriEnd").toString().replaceAll(":", ""));
		param.put("trmtSatStart", StringUtil.isEmpty(param.get("trmtSatStart"))? null : param.get("trmtSatStart").toString().replaceAll(":", ""));
		param.put("trmtSatEnd", StringUtil.isEmpty(param.get("trmtSatEnd"))? null : param.get("trmtSatEnd").toString().replaceAll(":", ""));
		mDBMapper.hospitalModifyDetail(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiHospitalDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("병원 고유정보가 누락되었습니다.");
		}

		mDBMapper.hospitalDelete(param);
		mDBMapper.hospitalDeleteDetail(param);
		mDBMapper.hospitalDeleteFacility(param);
		mDBMapper.hospitalDeleteSpcapp(param);
		mDBMapper.hospitalDeleteSpecial(param);
		mDBMapper.hospitalDeleteSubject(param);
		mDBMapper.hospitalDeleteSubjectMapping(param);
		mDBMapper.hospitalDeleteTransport(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiPharmacyAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dutyName"))) {
			throw new Exception("약국명을 입력해주세요.");
		}
		String hpid = UUID.randomUUID().toString().replaceAll("-", "");
		param.put("hpid", hpid);
		mDBMapper.pharmacyAdd(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiPharmacyModifyBasic(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("약국 고유정보가 누락되었습니다.");
		}
		mDBMapper.pharmacyModifyBasic(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiPharmacyModifyDetail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("약국 고유정보가 누락되었습니다.");
		}
		param.put("dutyTime1s", StringUtil.isEmpty(param.get("dutyTime1s"))? null : param.get("dutyTime1s").toString().replaceAll(":", ""));
		param.put("dutyTime1c", StringUtil.isEmpty(param.get("dutyTime1c"))? null : param.get("dutyTime1c").toString().replaceAll(":", ""));
		param.put("dutyTime2s", StringUtil.isEmpty(param.get("dutyTime2s"))? null : param.get("dutyTime2s").toString().replaceAll(":", ""));
		param.put("dutyTime2c", StringUtil.isEmpty(param.get("dutyTime2c"))? null : param.get("dutyTime2c").toString().replaceAll(":", ""));
		param.put("dutyTime3s", StringUtil.isEmpty(param.get("dutyTime3s"))? null : param.get("dutyTime3s").toString().replaceAll(":", ""));
		param.put("dutyTime3c", StringUtil.isEmpty(param.get("dutyTime3c"))? null : param.get("dutyTime3c").toString().replaceAll(":", ""));
		param.put("dutyTime4s", StringUtil.isEmpty(param.get("dutyTime4s"))? null : param.get("dutyTime4s").toString().replaceAll(":", ""));
		param.put("dutyTime4c", StringUtil.isEmpty(param.get("dutyTime4c"))? null : param.get("dutyTime4c").toString().replaceAll(":", ""));
		param.put("dutyTime5s", StringUtil.isEmpty(param.get("dutyTime5s"))? null : param.get("dutyTime5s").toString().replaceAll(":", ""));
		param.put("dutyTime5c", StringUtil.isEmpty(param.get("dutyTime5c"))? null : param.get("dutyTime5c").toString().replaceAll(":", ""));
		param.put("dutyTime6s", StringUtil.isEmpty(param.get("dutyTime6s"))? null : param.get("dutyTime6s").toString().replaceAll(":", ""));
		param.put("dutyTime6c", StringUtil.isEmpty(param.get("dutyTime6c"))? null : param.get("dutyTime6c").toString().replaceAll(":", ""));
		param.put("dutyTime7s", StringUtil.isEmpty(param.get("dutyTime7s"))? null : param.get("dutyTime7s").toString().replaceAll(":", ""));
		param.put("dutyTime7c", StringUtil.isEmpty(param.get("dutyTime7c"))? null : param.get("dutyTime7c").toString().replaceAll(":", ""));
		param.put("dutyTime8s", StringUtil.isEmpty(param.get("dutyTime8s"))? null : param.get("dutyTime8s").toString().replaceAll(":", ""));
		param.put("dutyTime8c", StringUtil.isEmpty(param.get("dutyTime8c"))? null : param.get("dutyTime8c").toString().replaceAll(":", ""));
		mDBMapper.pharmacyModifyDetail(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiPharmacyDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("약국 고유정보가 누락되었습니다.");
		}

		mDBMapper.pharmacyDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSearchHospitalSubjectIn(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("과목정보가 누락되었습니다.");
		}

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.hospitalListTotalInSubject(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.hospitalListInSubject(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiSearchHospitalSubjectNotIn(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("과목정보가 누락되었습니다.");
		}

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.hospitalListTotalNotInSubject(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.hospitalListNotInSubject(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiSearchHospitalSubjectAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("과목정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("병원 고유정보가 누락되었습니다.");
		}

		mDBMapper.hospitalSubjectAdd(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSearchHospitalSubjectDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("과목정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("병원 고유정보가 누락되었습니다.");
		}

		mDBMapper.hospitalSubjectDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* Medical DB End */
	
	/* Cast Start */
	public RESTResult apiCastList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.castListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.castList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiCastCreate(HttpServletRequest request, Map<String, Object> param, MultipartFile image, MultipartFile background) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "cast", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
		}
		
		if (background != null && !background.isEmpty()) {
			if (!background.getOriginalFilename().toLowerCase().endsWith("jpg") && !background.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "cast", background);
			param.put("background", background.getOriginalFilename());
			param.put("background_path", map.get("path"));
		}
		
		int newIdx = sDBMapper.castNewIdx(param);
		param.put("idx", newIdx);
		mDBMapper.castCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCastModify(HttpServletRequest request, Map<String, Object> param, MultipartFile image, MultipartFile background) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> old = sDBMapper.castItem(param);
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "cast", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
			
			if (!StringUtil.isEmpty(old.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(old.get("image_path")));
			}
		}
		
		if (background != null && !background.isEmpty()) {
			if (!background.getOriginalFilename().toLowerCase().endsWith("jpg") && !background.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "cast", background);
			param.put("background", background.getOriginalFilename());
			param.put("background_path", map.get("path"));
			
			if (!StringUtil.isEmpty(old.get("background_path"))) {
				resourceService.delete(StringUtil.getContent(old.get("background_path")));
			}
		}
		
		mDBMapper.castModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCastDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		param.put("cidx", param.get("idx"));
		List<Map<String, Object>> list = sDBMapper.castPageList(param);
		for (Map<String, Object> m : list) {
			if (!StringUtil.isEmpty(m.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(m.get("image_path")));
			}
			mDBMapper.castPageDelete(m);
		}
		Map<String, Object> old = sDBMapper.castItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		if (!StringUtil.isEmpty(old.get("background_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("background_path")));
		}
		mDBMapper.castDelete(param);
		mDBMapper.castReadDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCastStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		mDBMapper.castStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCastBanner(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		mDBMapper.castBanner(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiCastPageList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.castPageList(param));
	}
	
	
	@Transactional
	public RESTResult apiCastPageCreate(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") && !file.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
		}
		
		Map<String, Object> map = resourceService.upload(request, "cast", file);
		param.put("image", file.getOriginalFilename());
		param.put("image_path", map.get("path"));
		mDBMapper.castPageCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCastPageModify(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") && !file.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
		}
		
		Map<String, Object> old = sDBMapper.castPageItem(param);
		
		Map<String, Object> map = resourceService.upload(request, "cast", file);
		param.put("image", file.getOriginalFilename());
		param.put("image_path", map.get("path"));
		
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}

		mDBMapper.castPageModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCastPageDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> old = sDBMapper.castPageItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		
		mDBMapper.castPageDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* Cast End */
	
	/* Info DB Start */
	public RESTResult apiInfoList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.infoListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.infoList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiInfoCreate(HttpServletRequest request, Map<String, Object> param, MultipartFile image) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "info", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
		}
		
		int newIdx = sDBMapper.infoNewIdx(param);
		param.put("idx", newIdx);
		mDBMapper.infoCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoModify(HttpServletRequest request, Map<String, Object> param, MultipartFile image) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> old = sDBMapper.infoItem(param);
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "info", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
			
			if (!StringUtil.isEmpty(old.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(old.get("image_path")));
			}
		}
		
		mDBMapper.infoModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		mDBMapper.infoStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		param.put("dbidx", param.get("idx"));
		List<Map<String, Object>> list = sDBMapper.infoGroupList(param);
		for (Map<String, Object> group : list) {
			group.put("dbgidx", group.get("idx"));
			List<Map<String, Object>> contents = sDBMapper.infoContentList(group);
			for (Map<String, Object> content : contents) {
				content.put("dbcidx", content.get("idx"));
				List<Map<String, Object>> pages = sDBMapper.infoPageList(content);
				for (Map<String, Object> p : pages) {
					if (!StringUtil.isEmpty(p.get("image_path"))) {
						resourceService.delete(StringUtil.getContent(p.get("image_path")));
					}
					mDBMapper.infoPageDelete(p);
				}
				
				if (!StringUtil.isEmpty(content.get("image_path"))) {
					resourceService.delete(StringUtil.getContent(content.get("image_path")));
				}
				if (!StringUtil.isEmpty(content.get("background_path"))) {
					resourceService.delete(StringUtil.getContent(content.get("background_path")));
				}
				mDBMapper.infoContentDelete(content);
			}
			mDBMapper.infoGroupDelete(group);
		}

		Map<String, Object> old = sDBMapper.infoItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		
		mDBMapper.infoDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	public RESTResult apiInfoGroupList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.infoGroupList(param));
	}
	
	@Transactional
	public RESTResult apiInfoGroupCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		mDBMapper.infoGroupCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoGroupModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		mDBMapper.infoGroupModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoGroupDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		param.put("dbgidx", param.get("idx"));
		List<Map<String, Object>> list = sDBMapper.infoContentList(param);
		for (Map<String, Object> content : list) {
			content.put("dbcidx", content.get("idx"));
			List<Map<String, Object>> pages = sDBMapper.infoPageList(content);
			for (Map<String, Object> p : pages) {
				if (!StringUtil.isEmpty(p.get("image_path"))) {
					resourceService.delete(StringUtil.getContent(p.get("image_path")));
				}
				mDBMapper.infoPageDelete(p);
			}
			
			if (!StringUtil.isEmpty(content.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(content.get("image_path")));
			}
			if (!StringUtil.isEmpty(content.get("background_path"))) {
				resourceService.delete(StringUtil.getContent(content.get("background_path")));
			}
			mDBMapper.infoContentDelete(content);
		}
		
		mDBMapper.infoGroupDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiInfoContentList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.infoContentList(param));
	}
	
	@Transactional
	public RESTResult apiInfoContentCreate(HttpServletRequest request, Map<String, Object> param, MultipartFile image, MultipartFile background) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "info", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
		}
		
		if (background != null && !background.isEmpty()) {
			if (!background.getOriginalFilename().toLowerCase().endsWith("jpg") && !background.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "info", background);
			param.put("background", background.getOriginalFilename());
			param.put("background_path", map.get("path"));
		}
		
		mDBMapper.infoContentCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoContentModify(HttpServletRequest request, Map<String, Object> param, MultipartFile image, MultipartFile background) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		Map<String, Object> old = sDBMapper.infoContentItem(param);
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "info", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
			
			if (!StringUtil.isEmpty(old.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(old.get("image_path")));
			}
		}
		
		if (background != null && !background.isEmpty()) {
			if (!background.getOriginalFilename().toLowerCase().endsWith("jpg") && !background.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "info", background);
			param.put("background", background.getOriginalFilename());
			param.put("background_path", map.get("path"));
			
			if (!StringUtil.isEmpty(old.get("background_path"))) {
				resourceService.delete(StringUtil.getContent(old.get("background_path")));
			}
		}
		
		mDBMapper.infoContentModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoContentDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		param.put("dbcidx", param.get("idx"));
		List<Map<String, Object>> list = sDBMapper.infoPageList(param);
		for (Map<String, Object> m : list) {
			if (!StringUtil.isEmpty(m.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(m.get("image_path")));
			}
			mDBMapper.infoPageDelete(m);
		}

		Map<String, Object> old = sDBMapper.infoContentItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		if (!StringUtil.isEmpty(old.get("background_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("background_path")));
		}
		
		mDBMapper.infoContentDelete(param);
		mDBMapper.infoContentReadDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiInfoPageList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("dbcidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.infoPageList(param));
	}
	
	@Transactional
	public RESTResult apiInfoPageCreate(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("dbcidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") && !file.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
		}
		
		Map<String, Object> map = resourceService.upload(request, "info", file);
		param.put("image", file.getOriginalFilename());
		param.put("image_path", map.get("path"));
		mDBMapper.infoPageCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoPageModify(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("dbcidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") && !file.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
		}
		
		Map<String, Object> old = sDBMapper.infoPageItem(param);
		
		Map<String, Object> map = resourceService.upload(request, "info", file);
		param.put("image", file.getOriginalFilename());
		param.put("image_path", map.get("path"));
		
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}

		mDBMapper.infoPageModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfoPageDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("dbcidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> old = sDBMapper.infoPageItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		
		mDBMapper.infoPageDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* Info DB End */

}
