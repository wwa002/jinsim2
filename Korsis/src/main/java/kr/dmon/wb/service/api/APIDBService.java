package kr.dmon.wb.service.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterDBMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveDBMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.BaseService;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.StringUtil;

@Service
public class APIDBService extends BaseService {

	private static Logger logger = Logger.getLogger(APIService.class);

	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;

	@Autowired
	ResourceService resourceService;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterDBMapper mDBMapper;
	
	@Autowired
	SlaveDBMapper sDBMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	public RESTResult apiSearchMapStore(HttpServletRequest request, Map<String, Object> param) throws Exception {
		
		if(StringUtil.isEmpty(param.get("sitecd"))){
			throw new Exception("서비스정보가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("keyword"))) {
			if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
					|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
				throw new Exception("범위정보가 누락되었습니다.");
			}
		}
		
//		Map<String, Object> data = new HashMap<>();
//		data.put("store", sDBMapper.searchStoreMap(param));
	
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.searchStoreMap(param));
	}
	
	public RESTResult apiSearchMedical(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("keyword"))) {
			if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
					|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
				throw new Exception("범위정보가 누락되었습니다.");
			}
		}
		
		Map<String, Object> data = new HashMap<>();
		data.put("hospital", sDBMapper.searchHospital(param));
		data.put("pharmacy", sDBMapper.searchPharmacy(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, data);
	}
	
	public RESTResult apiSearchHospital(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("keyword"))) {
			if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
					|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
				throw new Exception("범위정보가 누락되었습니다.");
			}
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.searchHospital(param));
	}
	
	public RESTResult apiSearchHospitalSubject(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
				|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
			throw new Exception("범위정보가 누락되었습니다.");
		}
		
		if(StringUtil.isEmpty(param.get("subject"))){
			throw new Exception("진료과목 정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.searchHospitalSubject(param));
	}
	
	public RESTResult apiSearchHospitalSubject2(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
				|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
			throw new Exception("범위정보가 누락되었습니다.");
		}
		
		if(StringUtil.isEmpty(param.get("code"))){
			throw new Exception("진료과목 정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.searchHospital(param));
	}
	
	public RESTResult apiSearchHospitalTheme(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
				|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
			throw new Exception("범위정보가 누락되었습니다.");
		}
		
		if(StringUtil.isEmpty(param.get("searchMode"))){
			throw new Exception("타입이 누락되었습니다.");
		}
		if(StringUtil.isEmpty(param.get("code"))){
			throw new Exception("타입 코드가 누락되었습니다.");
		}
		String mode = StringUtil.getContent(param.get("searchMode"));
		if(mode.equals("type")){
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.searchHospitalType(param));
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.searchHospitalSepcapp(param));
	}
	
	public RESTResult apiSearchPharmacy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		if (StringUtil.isEmpty(param.get("keyword"))) {
			if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
					|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
				throw new Exception("범위정보가 누락되었습니다.");
			}
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.searchPharmacy(param));
	}
	
	public RESTResult apiListHospital(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.hospitalListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.hospitalList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiListHospitalSubject(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.hospitalSubjectListAll(param));
	}
	
	public RESTResult apiListHospitalType(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.hospitalTypeListAll(param));
	}
	
	public RESTResult apiListHospitalSpecial(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.hospitalSpecialListAll(param));
	}
	
	public RESTResult apiListHospitalLocal(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sDBMapper.hospitalLocalListAll(param));
	}
	
	public RESTResult apiListPharmacy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.pharmacyListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.pharmacyList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiListInfodb(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.apiInfoListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.apiInfoList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiListCast(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useCast()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sDBMapper.apiCastListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sDBMapper.apiCastList(param));
		if (Integer.parseInt(StringUtil.getContent(param.get("page"))) == 1) {
			ret.put("banner", sDBMapper.apiCastListBanner(param));
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiInformationHospital(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		Map<String, Object> ret = sDBMapper.hospitalItem(param);
		if (ret == null) {
			throw new Exception("병원정보를 읽어 올 수 없습니다.");
		}
		MemberSession ms = getMemberSession(request);
		param.put("midx", ms.isLogin() ? ms.getIdx() : -1);
		if (ms.isLogin()) {
			if (sDBMapper.hospitalCheckRead(param) == 0) {
				mDBMapper.hospitalAddRead(param);
			}
		}
		
		ret.put("me", ms.getData());
		ret.put("read", sDBMapper.hospitalReadItem(param));
		ret.put("detail", sDBMapper.hospitalItemDetail(param));
		ret.put("photos", sDBMapper.hospitalPhotoList(param));
		ret.put("comments", sDBMapper.hospitalCommentList(param));
		ret.put("rating", sDBMapper.hospitalRating(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiInformationPharmacy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		Map<String, Object> ret = sDBMapper.pharmacyItem(param);
		if (ret == null) {
			throw new Exception("약국정보를 읽어 올 수 없습니다.");
		}
		MemberSession ms = getMemberSession(request);
		param.put("midx", ms.isLogin() ? ms.getIdx() : -1);
		if (ms.isLogin()) {
			if (sDBMapper.pharmacyCheckRead(param) == 0) {
				mDBMapper.pharmacyAddRead(param);
			}
		}
		
		ret.put("me", ms.getData());
		ret.put("read", sDBMapper.pharmacyReadItem(param));
		ret.put("photos", sDBMapper.pharmacyPhotoList(param));
		ret.put("comments", sDBMapper.pharmacyCommentList(param));
		ret.put("rating", sDBMapper.pharmacyRating(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiCommentHospital(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		
		String mode = StringUtil.getContent(param.get("mode"));
		if ("write".equals(mode)) {
			mDBMapper.hospitalCommentAdd(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCommentPharmacy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		
		String mode = StringUtil.getContent(param.get("mode"));
		if ("write".equals(mode)) {
			mDBMapper.pharmacyCommentAdd(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiRatingHospital(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		mDBMapper.hospitalRating(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiRatingPharmacy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		mDBMapper.pharmacyRating(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiFavoriteHospital(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		mDBMapper.hospitalFavorite(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiFavoritePharmacy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		mDBMapper.pharmacyFavorite(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	public RESTResult apiInformationInfodb(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("정보DB 고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		MemberSession ms = getMemberSession(request);
		if (ms != null && ms.isLogin()) {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> retMap = sDBMapper.apiInfoItem(param);
		List<Map<String, Object>> items = sDBMapper.apiInfoGroupList(param);
		for (Map<String, Object> m : items) {
			Map<String, Object> p = new HashMap<>();
			p.put("sitecd", ss.getSitecd());
			p.put("midx", param.get("midx"));
			p.put("dbidx", param.get("idx"));
			p.put("dbgidx", m.get("idx"));
			m.put("items", sDBMapper.apiInfoContentList(p));
		}
		retMap.put("items", items);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, retMap);
	}
	
	public RESTResult apiInformationInfodbDetail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) ||StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (ms != null && ms.isLogin()) {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> p = new HashMap<>();
		p.put("sitecd", ss.getSitecd());
		p.put("idx", param.get("dbidx"));
		
		Map<String, Object> info = sDBMapper.apiInfoItem(p);
		if (info == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}

		Map<String, Object> item = sDBMapper.apiInfoContentItem(param);
		if (item == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		
		if (ms.isLogin()) {
			if (sDBMapper.apiInfoCheckRead(param) == 0) {
				mDBMapper.infoContentAddRead(param);
			}
		}
		
		item.put("me", ms.getData());
		item.put("items", sDBMapper.apiInfoContentPages(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, item);
	}
	
	@Transactional
	public RESTResult apiInformationCast(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useCast()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("캐스트 고유번호가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> retMap = sDBMapper.apiCastItem(param);
		if (retMap == null) {
			throw new Exception("캐스트 정보를 읽어올 수 없습니다.");
		}
		
		if (ms.isLogin()) {
			if (sDBMapper.apiCastCheckRead(param) == 0) {
				mDBMapper.castAddRead(param);
			}
		}
		
		retMap.put("me", ms.getData());
		retMap.put("items", sDBMapper.apiCastPageList(param));
		retMap.put("recommend", sDBMapper.apiCastRecommend(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, retMap);
	}
	
	@Transactional
	public RESTResult apiCastFavorite(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useCast()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("캐스트 고유번호가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		} else {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> retMap = sDBMapper.apiCastItem(param);
		if (retMap == null) {
			throw new Exception("캐스트 정보를 읽어올 수 없습니다.");
		}
		
		mDBMapper.castFavorite(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiCastRecommend(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useCast()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("캐스트 고유번호가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		} else {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> retMap = sDBMapper.apiCastItem(param);
		if (retMap == null) {
			throw new Exception("캐스트 정보를 읽어올 수 없습니다.");
		}
		
		mDBMapper.castRecommend(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfodbFavorite(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) ||StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		} else {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> item = sDBMapper.apiInfoContentItem(param);
		if (item == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		
		
		mDBMapper.infoContentFavorite(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiInfodbRecommend(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) ||StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		} else {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> item = sDBMapper.apiInfoContentItem(param);
		if (item == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		
		
		mDBMapper.infoContentRecommend(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	
	@Transactional
	public RESTResult apiHospitalPhotoUpload(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		
		Map<String, Object> map = resourceService.upload(request, "hospital", file);
		param.put("image", map.get("destName"));
		param.put("image_path", map.get("path"));
		
		
		mDBMapper.hospitalPhotoAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiPharmacyPhotoUpload(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (!ss.useMedicaldb()) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("hpid"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		
		Map<String, Object> map = resourceService.upload(request, "pharmacy", file);
		param.put("image", map.get("destName"));
		param.put("image_path", map.get("path"));
		
		mDBMapper.pharmacyPhotoAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
}
