package kr.dmon.wb.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterEventMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveDBMapper;
import kr.dmon.wb.mapper.slave.SlaveEventMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.StringUtil;

@Service
public class EventService extends BaseService {
	
	private static Logger logger = Logger.getLogger(EventService.class);

	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	SlaveDBMapper sDBMapper;
	
	@Autowired
	MasterEventMapper mEventMapper;
	
	@Autowired
	SlaveEventMapper sEventMapper;

	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	
	// for API
	public RESTResult apiEventList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		param.put("sort", StringUtils.isEmpty(param.get("sort")) ? "date" : param.get("sort"));
		param.put("sidoCd", StringUtils.isEmpty(param.get("sidoCd")) ? "" : param.get("sidoCd"));
		
		Map<String, Object> retMap = new HashMap<>();

		retMap.put("sort", param.get("sort"));
		retMap.put("sidoCd", param.get("sidoCd"));
		
		if (Integer.parseInt(StringUtil.getContent(param.get("page"))) == 1 && StringUtil.isEmpty(param.get("cat1")) && StringUtil.isEmpty(param.get("cat2"))) {
			retMap.put("banner", sEventMapper.apiBanners(param));
			retMap.put("new", sEventMapper.apiNews(param));
			retMap.put("week", sEventMapper.apiWeeks(param));
		}

		MemberSession ms = getMemberSession(request);
		if (ms != null && ms.isLogin()) {
			param.put("midx", ms.getIdx());
		}
		
		int totalCount = sEventMapper.apiListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);

		param.put("start", (paging.getPageNo() - 1) * numPerPage);
		param.put("pageSize", numPerPage);
		retMap.put("paging", paging);
		retMap.put("list", sEventMapper.apiList(param));
		retMap.put("local", sEventMapper.apiLocal(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, retMap);
	}
	
	@Transactional
	public RESTResult apiEventView(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		param.put("midx", ms.isLogin() ? ms.getIdx() : -1);

		Map<String, Object> ret = sEventMapper.apiEventItem(param);
		if (ret == null) {
			throw new Exception("존재하지 않는 이벤트 입니다.");
		}

		if (ms.isLogin()) {
			if (sEventMapper.apiEventCheckRead(param) == 0) {
				mEventMapper.apiEventAddRead(param);
			}
		}
		
		ret.put("me", ms.getData());
		ret.put("items", sEventMapper.apiEventPageItems(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiEventApply(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());

		Map<String, Object> ret = sEventMapper.apiEventItem(param);
		if (ret == null) {
			throw new Exception("존재하지 않는 이벤트 입니다.");
		}
		
		mEventMapper.apiEventApply(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiEventFavorite(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());

		Map<String, Object> ret = sEventMapper.apiEventItem(param);
		if (ret == null) {
			throw new Exception("존재하지 않는 이벤트 입니다.");
		}
		
		mEventMapper.apiEventFavorite(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	

	public RESTResult apiExhibition(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		Map<String, Object> retMap = new HashMap<>();
		
		int totalCount = sEventMapper.apiExhibitionListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);

		param.put("start", (paging.getPageNo() - 1) * numPerPage);
		param.put("pageSize", numPerPage);
		retMap.put("paging", paging);
		
		MemberSession ms = getMemberSession(request);
		
		List<Map<String, Object>> list = sEventMapper.apiExhibitionList(param);
		for (Map<String, Object> map : list) {
			Map<String, Object> p = new HashMap<>();
			p.put("sitecd", ss.getSitecd());
			p.put("idx", map.get("idx"));
			if (ms != null) {
				param.put("midx", ms.getIdx());
			}
			map.put("items", sEventMapper.apiExhibitionItems(p));
		}
		
		retMap.put("list", list);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, retMap);
	}
	
	public RESTResult apiExhibitionDetail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (ms != null) {
			param.put("midx", ms.getIdx());
		}
		
		Map<String, Object> retMap = sEventMapper.apiExhibitionItem(param);
		retMap.put("items", sEventMapper.apiExhibitionItems(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, retMap);
	}
	
	public RESTResult apiCategory(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.categoryListAll(param));
	}
	
	public RESTResult apiCategories(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		List<Map<String, Object>> list = sEventMapper.categoryListAll(param);
		for (Map<String, Object> map : list) {
			param.put("category", map.get("idx"));
			map.put("data", sEventMapper.categoryListAll(param));
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, list);
	}
	
	
	// for Admin API
	public RESTResult apiAdminList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sEventMapper.adminListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sEventMapper.adminList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiAdminCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("cat1"))) {
			param.put("cat1", null);
		}
		
		if (StringUtil.isEmpty(param.get("cat2"))) {
			param.put("cat2", null);
		}
		
		int newIdx = sEventMapper.eventNewIdx(param);
		param.put("idx", newIdx);
		mEventMapper.adminCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, newIdx);
	}
	
	@Transactional
	public RESTResult apiAdminModify(HttpServletRequest request, Map<String, Object> param, MultipartFile image) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Date start = null, end = null;
		if (!StringUtil.isEmpty(param.get("startdt")) && !StringUtil.isEmpty(param.get("starttm"))) {
			start = DateUtil.getDateFromString(param.get("startdt").toString()+param.get("starttm").toString(), "yyyy-MM-ddHH:mm");
			param.put("startdate", DateUtil.getDateString(start, "yyyyMMddHHmmss"));
		}
		
		if (!StringUtil.isEmpty(param.get("enddt")) && !StringUtil.isEmpty(param.get("endtm"))) {
			end = DateUtil.getDateFromString(param.get("enddt").toString()+param.get("endtm").toString(), "yyyy-MM-ddHH:mm");
			param.put("enddate", DateUtil.getDateString(end, "yyyyMMddHHmmss"));
		}
		
		if (start != null && end != null && start.getTime() > end.getTime()) {
			throw new Exception("종료일이 시작일 보다 앞설 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("cat1"))) {
			param.put("cat1", null);
		}
		
		if (StringUtil.isEmpty(param.get("cat2"))) {
			param.put("cat2", null);
		}
		
		Map<String, Object> old = sEventMapper.adminItem(param);
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "event", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
			
			if (!StringUtil.isEmpty(old.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(old.get("image_path")));
			}
		}
		
		mEventMapper.adminModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		param.put("eidx", param.get("idx"));
		List<Map<String, Object>> list = sEventMapper.pageList(param);
		for (Map<String, Object> m : list) {
			if (!StringUtil.isEmpty(m.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(m.get("image_path")));
			}
			mEventMapper.adminPageDelete(m);
		}
		Map<String, Object> old = sEventMapper.adminItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		mEventMapper.adminDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		mEventMapper.adminStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminSearch(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.adminSearch(param));
	}
	
	public RESTResult apiAdminPageList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("eidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.pageList(param));
	}
	
	
	@Transactional
	public RESTResult apiAdminPageCreate(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("eidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") && !file.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
		}
		
		Map<String, Object> map = resourceService.upload(request, "event", file);
		param.put("image", file.getOriginalFilename());
		param.put("image_path", map.get("path"));
		mEventMapper.adminPageCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminPageModify(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("eidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") && !file.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
		}
		
		Map<String, Object> old = sEventMapper.pageItem(param);
		
		Map<String, Object> map = resourceService.upload(request, "event", file);
		param.put("image", file.getOriginalFilename());
		param.put("image_path", map.get("path"));
		
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}

		mEventMapper.adminPageModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminPageDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("eidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> old = sEventMapper.pageItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		
		mEventMapper.adminPageDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	public RESTResult apiAdminBannerList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.adminBannerList(param));
	}
	
	@Transactional
	public RESTResult apiAdminBannerAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if ("E".equals(param.get("type"))) {
			if (StringUtil.isEmpty(param.get("eidx"))) {
				throw new Exception("이벤트를 선택해 주세요.");
			}
			param.put("pidx", null);
		} else if ("P".equals(param.get("type"))) {
			if (StringUtil.isEmpty(param.get("pidx"))) {
				throw new Exception("프로젝트를 선택해 주세요.");
			}
			param.put("eidx", null);
		} else {
			throw new Exception("배너 타입을 확인해 주세요.");
		}
		
		mEventMapper.adminBannerAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminBannerDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mEventMapper.adminBannerDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminBannerStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		
		mEventMapper.adminBannerStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	public RESTResult apiPlanAdminList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sEventMapper.adminPlanListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sEventMapper.adminPlanList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiAdminPlanCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		int newIdx = sEventMapper.planNewIdx(param);
		param.put("idx", newIdx);
		mEventMapper.adminPlanCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, newIdx);
	}
	
	@Transactional
	public RESTResult apiAdminPlanModify(HttpServletRequest request, Map<String, Object> param, MultipartFile image) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> old = sEventMapper.adminPlanItem(param);
		if (image != null && !image.isEmpty()) {
			if (!image.getOriginalFilename().toLowerCase().endsWith("jpg") && !image.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
			Map<String, Object> map = resourceService.upload(request, "plan", image);
			param.put("image", image.getOriginalFilename());
			param.put("image_path", map.get("path"));
			
			if (!StringUtil.isEmpty(old.get("image_path"))) {
				resourceService.delete(StringUtil.getContent(old.get("image_path")));
			}
		}
		
		mEventMapper.adminPlanModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminPlanDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mEventMapper.adminPlanPageDeleteAll(param);
		Map<String, Object> old = sEventMapper.adminPlanItem(param);
		if (!StringUtil.isEmpty(old.get("image_path"))) {
			resourceService.delete(StringUtil.getContent(old.get("image_path")));
		}
		mEventMapper.adminPlanDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminPlanStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		mEventMapper.adminPlanStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminPlanSearch(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.adminPlanSearch(param));
	}
	
	public RESTResult apiAdminPlanPageList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.adminPlanPageList(param));
	}
	
	@Transactional
	public RESTResult apiAdminPlanPageAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("pidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("eidx"))) {
			throw new Exception("이벤트를 선택해 주세요.");
		}
		
		mEventMapper.adminPlanPageAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminPlanPageDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("pidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mEventMapper.adminPlanPageDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminNewList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.adminNewList(param));
	}
	
	@Transactional
	public RESTResult apiAdminNewAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("eidx"))) {
			throw new Exception("이벤트를 선택해 주세요.");
		}
		
		mEventMapper.adminNewAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminNewDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mEventMapper.adminNewDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminNewStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		
		mEventMapper.adminNewStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminWeekList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sEventMapper.adminWeekList(param));
	}
	
	@Transactional
	public RESTResult apiAdminWeekAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("eidx"))) {
			throw new Exception("이벤트를 선택해 주세요.");
		}
		
		mEventMapper.adminWeekAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminWeekDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mEventMapper.adminWeekDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminWeekStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("status", "Y".equals(StringUtil.getContent(param.get("status"))) ? "Y" : "N");
		
		mEventMapper.adminWeekStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
}
