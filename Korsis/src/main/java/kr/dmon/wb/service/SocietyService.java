package kr.dmon.wb.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterCommonMapper;
import kr.dmon.wb.mapper.master.MasterSocietyMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveSocietyMapper;
import kr.dmon.wb.model.ConferenceRegisterXlsView;
import kr.dmon.wb.model.ExcelDownloadView;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.StringUtil;

@Service
public class SocietyService extends BaseService {

	private static Logger logger = Logger.getLogger(SocietyService.class);

	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterSocietyMapper mSocietyMapper;
	
	@Autowired
	SlaveSocietyMapper sSocietyMapper;
	
	@Autowired
	MasterCommonMapper mCommonMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	// for API
	
	// for Admin
	public RESTResult apiAdminMemberList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sSocietyMapper.adminMemberListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sSocietyMapper.adminMemberList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public ModelAndView apiAdminMemberExcel(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("content"))) {
			throw new Exception("사유가 누락되었습니다.");
		}
		
		Map<String, Object> logMap = new HashMap<>();
		logMap.put("sitecd", ss.getSitecd());
		logMap.put("midx", getMemberSession(request).getIdx());
		logMap.put("type", "A");
		logMap.put("code", "MEMBER_LIST");
		logMap.put("title", "회원 정보 다운로드");
		logMap.put("content", param.get("content"));
		mCommonMapper.excelLog(logMap);
		
		List<Map<String, Object>> list = sSocietyMapper.adminMemberListExcel(param);
		for (Map<String, Object> m : list) {
			if ("M".equals(m.get("society_type"))) {
				m.put("society_type_text", "준회원");
			} else if ("R".equals(m.get("society_type"))) {
				m.put("society_type_text", "정회원");
			} else {
				m.put("society_type_text", "대기자");
			}
			if ("M".equals(m.get("sex"))) {
				m.put("sex_text", "남");
			} else {
				m.put("sex_text", "여");
			}
			m.put("regdate_text", DateUtil.getDateString(StringUtil.getContent(m.get("regdate")), "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss"));
			m.put("moddate_text", DateUtil.getDateString(StringUtil.getContent(m.get("moddate")), "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss"));
			if (!StringUtil.isEmpty(m.get("birth"))) {
				m.put("birth_text", DateUtil.getDateString(StringUtil.getContent(m.get("birth")), "yyyyMMdd", "yyyy-MM-dd"));
			} else {
				m.put("birth_text", "-");
			}
		}
		
		List<String> columns = new ArrayList<>();
		columns.add("id");
		columns.add("name");
		columns.add("name_eng");
		columns.add("birth_text");
		columns.add("sex_text");
		columns.add("email");
		columns.add("mobile");
		columns.add("major");
		columns.add("license_doctor");
		columns.add("license_expert");
		columns.add("office_type");
		columns.add("office");
		columns.add("zipcode");
		columns.add("address");
		columns.add("address_etc");
		columns.add("phone1");
		columns.add("phone2");
		columns.add("regdate_text");
		columns.add("moddate_text");
		columns.add("society_type_text");
		
		Map<String, String> columnTitles = new HashMap<>();
		columnTitles.put("id", "아이디");
		columnTitles.put("name", "이름(국문)");
		columnTitles.put("name_eng", "이름(영문)");
		columnTitles.put("birth_text", "생년월일");
		columnTitles.put("sex_text", "성별");
		columnTitles.put("email", "이메일");
		columnTitles.put("mobile", "휴대폰번호");
		columnTitles.put("major", "전문과목");
		columnTitles.put("license_doctor", "의사면허번호");
		columnTitles.put("license_expert", "전문의번호");
		columnTitles.put("office_type", "근무처 구분");
		columnTitles.put("office", "병원명");
		columnTitles.put("zipcode", "우편번호");
		columnTitles.put("address", "기본주소");
		columnTitles.put("address_etc", "상세주소");
		columnTitles.put("phone1", "근무처 연락처");
		columnTitles.put("phone2", "근무처 FAX");
		columnTitles.put("regdate_text", "회원가입일");
		columnTitles.put("moddate_text", "최근수정일");
		columnTitles.put("society_type_text", "회원등급");
		
		Map<String, Object> excel = new HashMap<>();
		excel.put("filename", "MEMBERS");
		excel.put("columns", columns);
		excel.put("titles", columnTitles);
		excel.put("list", list);
		return new ModelAndView(new ExcelDownloadView(), excel);
	}
	
	@Transactional
	public RESTResult apiAdminMemberType(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("회원 고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("type"))) {
			throw new Exception("등급 정보가 누락되었습니다.");
		}
		
		mSocietyMapper.memberType(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminMemberConfirm(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("회원 고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> map = sSocietyMapper.adminMemberItem(param);
		if (map == null) {
			throw new Exception("회원 정보를 읽어올 수 없습니다.");
		}
		
		mSocietyMapper.memberConfirm(param);
		mSocietyMapper.memberConfirmDate(param);
		
		if ("M".equals(map.get("society_type"))) {
			map.put("society_type_text", "준회원");
		} else if ("R".equals(map.get("society_type"))) {
			map.put("society_type_text", "정회원");
		} else {
			map.put("society_type_text", "-");
		}
		
		param.put("code", "join_confirm");
		Map<String, Object> mailTemplate = sAPIMapper.getMailTemplate(param);
		if (mailTemplate != null) {
			mailTemplate.put("content", replaceTemplate(StringUtil.getContent(mailTemplate.get("content")), map));
			resourceService.sendMailHtml(new String[]{StringUtil.getContent(map.get("email"))}, ss.getEmail(), StringUtil.getContent(mailTemplate.get("title")), StringUtil.getContent(mailTemplate.get("content")));
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminMemberRecovery(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("회원 고유번호가 누락되었습니다.");
		}
		
		mSocietyMapper.memberRecovery(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminMemberSaveBasic(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		if (!StringUtil.isEmpty(param.get("birth"))) {
			param.put("birth", StringUtil.getContent(param.get("birth")).replaceAll("-", ""));
		}
		mSocietyMapper.adminMemberSaveBasic(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminMemberSaveSociety(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		mSocietyMapper.adminMemberSaveSociety(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminMemberSaveAgree(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		mSocietyMapper.adminMemberSaveAgree(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminMemberSavePermission(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		mSocietyMapper.adminMemberSavePermission(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminWorkshopWrite(HttpServletRequest request, Map<String, Object> param, MultipartFile banner, Map<String, String[]> lectures) throws Exception {

		// 작성 필수값 정리
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		if (!StringUtil.isEmpty(param.get("sdate")) && !StringUtil.isEmpty(param.get("stime"))) {
			String startdate = StringUtil.getContent(param.get("sdate")) + StringUtil.getContent(param.get("stime"));
			startdate = startdate.replaceAll("-", "").replaceAll(":", "");
			if (startdate.length() <= 12) {
				startdate += "00";
			}
			param.put("startdate", startdate);
		}
		if (!StringUtil.isEmpty(param.get("edate")) && !StringUtil.isEmpty(param.get("etime"))) {
			String enddate = StringUtil.getContent(param.get("edate")) + StringUtil.getContent(param.get("etime"));
			enddate = enddate.replaceAll("-", "").replaceAll(":", "");
			if (enddate.length() <= 12) {
				enddate += "00";
			}
			param.put("enddate", enddate);
		}
		if (!StringUtil.isEmpty(param.get("registration_sdate")) && !StringUtil.isEmpty(param.get("registration_stime"))) {
			String startdate = StringUtil.getContent(param.get("registration_sdate")) + StringUtil.getContent(param.get("registration_stime"));
			startdate = startdate.replaceAll("-", "").replaceAll(":", "");
			if (startdate.length() <= 12) {
				startdate += "00";
			}
			param.put("registration_startdate", startdate);
		}
		if (!StringUtil.isEmpty(param.get("registration_edate")) && !StringUtil.isEmpty(param.get("registration_etime"))) {
			String enddate = StringUtil.getContent(param.get("registration_edate")) + StringUtil.getContent(param.get("registration_etime"));
			enddate = enddate.replaceAll("-", "").replaceAll(":", "");
			if (enddate.length() <= 12) {
				enddate += "00";
			}
			param.put("registration_enddate", enddate);
		}
		
		if (banner != null && !banner.isEmpty()) {
			Map<String, Object> b = resourceService.upload(request, "s_workshop", banner);
			param.put("banner", b.get("path"));
		}
		
		int newIdx = sSocietyMapper.workshopNewIdx(param);
		param.put("idx", newIdx);
		mSocietyMapper.workshopWrite(param);
		
		if (lectures != null) {
			String[] titles = lectures.get("titles");
			if (titles != null) {
				for (int i = 0; i < titles.length; i++) {
					if (!StringUtil.isEmpty(titles[i])) {
						Map<String, Object> p = new HashMap<>();
						p.put("sitecd", param.get("sitecd"));
						p.put("code", param.get("code"));
						p.put("widx", newIdx);
						p.put("idx", i+1);
						p.put("type", lectures.get("types")[i]);
						p.put("title", lectures.get("titles")[i]);
						p.put("cost1", StringUtil.getContent(lectures.get("costs1"), i, "0"));
						p.put("cost2", StringUtil.getContent(lectures.get("costs2"), i, "0"));
						p.put("limited", StringUtil.getContent(lectures.get("limiteds"), i, "0"));
						String[] enddates = lectures.get("enddates");
						if (enddates.length > 0 && !StringUtil.isEmpty(enddates[i])) {
							p.put("enddate", StringUtil.getContent(lectures.get("enddates")[i]).replaceAll("-", ""));
						}
						String[] courses1 = lectures.get("courses1");
						if (courses1.length > 0 && !StringUtil.isEmpty(courses1[i])) {
							p.put("use_course", "Y");
							p.put("course1", lectures.get("courses1")[i]);
							p.put("course2", lectures.get("courses2")[i]);
							p.put("course1_limited", StringUtil.getContent(lectures.get("courses1_limited"), i, "0"));
							p.put("course2_limited", StringUtil.getContent(lectures.get("courses2_limited"), i, "0"));
						} else {
							p.put("use_course", "N");
						}
						
						mSocietyMapper.workshopLectureAdd(p);
					}
				}
			}
		}
		
		if (!StringUtil.isEmpty(param.get("files"))) {
			mSocietyMapper.workshopUploadedFileUpdate(param);
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminWorkshopModify(HttpServletRequest request, Map<String, Object> param, MultipartFile banner, Map<String, String[]> lectures) throws Exception {
		// 작성 필수값 정리
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		if (!StringUtil.isEmpty(param.get("sdate")) && !StringUtil.isEmpty(param.get("stime"))) {
			String startdate = StringUtil.getContent(param.get("sdate")) + StringUtil.getContent(param.get("stime"));
			startdate = startdate.replaceAll("-", "").replaceAll(":", "");
			if (startdate.length() <= 12) {
				startdate += "00";
			}
			param.put("startdate", startdate);
		}
		if (!StringUtil.isEmpty(param.get("edate")) && !StringUtil.isEmpty(param.get("etime"))) {
			String enddate = StringUtil.getContent(param.get("edate")) + StringUtil.getContent(param.get("etime"));
			enddate = enddate.replaceAll("-", "").replaceAll(":", "");
			if (enddate.length() <= 12) {
				enddate += "00";
			}
			param.put("enddate", enddate);
		}
		if (!StringUtil.isEmpty(param.get("registration_sdate")) && !StringUtil.isEmpty(param.get("registration_stime"))) {
			String startdate = StringUtil.getContent(param.get("registration_sdate")) + StringUtil.getContent(param.get("registration_stime"));
			startdate = startdate.replaceAll("-", "").replaceAll(":", "");
			if (startdate.length() <= 12) {
				startdate += "00";
			}
			param.put("registration_startdate", startdate);
		}
		if (!StringUtil.isEmpty(param.get("registration_edate")) && !StringUtil.isEmpty(param.get("registration_etime"))) {
			String enddate = StringUtil.getContent(param.get("registration_edate")) + StringUtil.getContent(param.get("registration_etime"));
			enddate = enddate.replaceAll("-", "").replaceAll(":", "");
			if (enddate.length() <= 12) {
				enddate += "00";
			}
			param.put("registration_enddate", enddate);
		}
		
		if (banner != null && !banner.isEmpty()) {
			Map<String, Object> old = sSocietyMapper.workshopView(param);
			if (!StringUtil.isEmpty(old.get("banner"))) {
				resourceService.delete(StringUtil.getContent(old.get("banner")));
			}
			Map<String, Object> b = resourceService.upload(request, "s_workshop", banner);
			param.put("banner", b.get("path"));
		}
		
		mSocietyMapper.workshopModify(param);
		if (!StringUtil.isEmpty(param.get("files"))) {
			mSocietyMapper.workshopUploadedFileUpdate(param);
			mSocietyMapper.workshopRemovedFileUpdate(param);
		} else {
			mSocietyMapper.workshopDeleteFiles(param);
		}
		
		mSocietyMapper.workshopDeleteLecture(param);
		if (lectures != null) {
			String[] titles = lectures.get("titles");
			if (titles != null) {
				for (int i = 0; i < titles.length; i++) {
					if (!StringUtil.isEmpty(titles[i])) {
						Map<String, Object> p = new HashMap<>();
						p.put("sitecd", param.get("sitecd"));
						p.put("code", param.get("code"));
						p.put("widx", param.get("idx"));
						p.put("idx", i+1);
						p.put("type", lectures.get("types")[i]);
						p.put("title", lectures.get("titles")[i]);
						p.put("cost1", StringUtil.getContent(lectures.get("costs1"), i, "0"));
						p.put("cost2", StringUtil.getContent(lectures.get("costs2"), i, "0"));
						p.put("limited", StringUtil.getContent(lectures.get("limiteds"), i, "0"));
						String[] enddates = lectures.get("enddates");
						if (enddates.length > 0 && !StringUtil.isEmpty(enddates[i])) {
							p.put("enddate", StringUtil.getContent(lectures.get("enddates")[i]).replaceAll("-", ""));
						}
						String[] courses1 = lectures.get("courses1");
						if (courses1.length > 0 && !StringUtil.isEmpty(courses1[i])) {
							p.put("use_course", "Y");
							p.put("course1", lectures.get("courses1")[i]);
							p.put("course2", lectures.get("courses2")[i]);
							p.put("course1_limited", StringUtil.getContent(lectures.get("courses1_limited"), i, "0"));
							p.put("course2_limited", StringUtil.getContent(lectures.get("courses2_limited"), i, "0"));
						} else {
							p.put("use_course", "N");
						}
						mSocietyMapper.workshopLectureAdd(p);
					}
				}
			}
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminWorkshopBannerDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {

		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		Map<String, Object> map = sSocietyMapper.workshopView(param);
		if (!StringUtil.isEmpty(map.get("banner"))) {
			resourceService.delete(StringUtil.getContent(map.get("banner")));
		}

		mSocietyMapper.workshopBannerDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	@Transactional
	public RESTResult apiAdminWorkshopDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		Map<String, Object> item = sSocietyMapper.workshopView(param);
		if (!StringUtil.isEmpty(item.get("banner"))) {
			resourceService.delete(StringUtil.getContent(item.get("banner")));
		}
		
		mSocietyMapper.workshopDelete(param);
		mSocietyMapper.workshopDeleteRegistration(param);
		mSocietyMapper.workshopDeleteLecture(param);
		mSocietyMapper.workshopDeleteFiles(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminWorkshopStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		mSocietyMapper.workshopModifyStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminWorkshopRegistrationItem(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		Map<String, Object> registration = sSocietyMapper.workshopRegistrationItem(param);
		if (registration == null) {
			throw new Exception("입력하신 정보로 등록된 정보가 없습니다.");
		}
		
		registration.put("lectures", sSocietyMapper.workshopRegistrationCheckLecture(registration));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, registration);
	}
	
	@Transactional
	public RESTResult apiAdminWorkshopRegistrationItemModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		List<String> lectures = new ArrayList<>();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			if (key.startsWith("lectures")) {
				if ("Y".equals(param.get(key))) {
					lectures.add(key.split("-")[1]);
				}
			}
		}
		
		mSocietyMapper.adminWorkshopRegistrationModify(param);
		mSocietyMapper.adminWorkshopRegistrationDeleteLectures(param);
		for (String lidx : lectures) {
			Map<String, Object> p = new HashMap<>();
			p.put("sitecd", param.get("sitecd"));
			p.put("code", param.get("code"));
			p.put("widx", param.get("widx"));
			p.put("ridx", param.get("idx"));
			p.put("lidx", lidx);
			p.put("type", param.get("type-"+lidx));
			p.put("course1", "Y".equals(param.get("course1-"+lidx)) ? "Y":"N");
			p.put("course2", "Y".equals(param.get("course2-"+lidx)) ? "Y":"N");
			mSocietyMapper.workshopRegistrationLecture(p);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminWorkshopRegistrationListAll(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		
		// 기본값 정리
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		int numPerPage = StringUtil.isEmpty(param.get("num_per_page")) ? this.numPerPage : Integer.parseInt(StringUtil.getContent(param.get("num_per_page")));
		int pagePerBlock = StringUtil.isEmpty(param.get("page_per_block")) ? this.pagePerBlock : Integer.parseInt(StringUtil.getContent(param.get("page_per_block")));

		// 페이징 작성
		int totalCount = sSocietyMapper.adminWorkshopRegistrationListAllTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());

		// 반환값 입력
		ret.put("list", sSocietyMapper.adminWorkshopRegistrationListAllList(param));
		ret.put("workshops", sSocietyMapper.adminWorkshopRegistrationListAllWorkshops(param));
		ret.put("paging", paging);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiAdminWorkshopRegistrationList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sSocietyMapper.adminWorkshopRegistrationListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		List<Map<String, Object>> list = sSocietyMapper.adminWorkshopRegistrationList(param);
		for (Map<String, Object> m : list) {
			m.put("lectures", sSocietyMapper.adminWorkshopRegistrationListLecture(m));
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", list);
		ret.put("total", sSocietyMapper.adminWorkshopRegistrationCost(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public ModelAndView apiAdminWorkshopRegistrationExcel(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("content"))) {
			throw new Exception("사유가 누락되었습니다.");
		}
		
		Map<String, Object> logMap = new HashMap<>();
		logMap.put("sitecd", ss.getSitecd());
		logMap.put("midx", getMemberSession(request).getIdx());
		logMap.put("type", "A");
		logMap.put("code", "REGISTRATION_LIST");
		logMap.put("title", "사전등록 정보 다운로드");
		logMap.put("content", param.get("content"));
		mCommonMapper.excelLog(logMap);
		
		StringBuffer sb = new StringBuffer();
		List<Map<String, Object>> list = sSocietyMapper.adminWorkshopRegistrationExcel(param);
		for (Map<String, Object> m : list) {
			List<Map<String, Object>> lectures = sSocietyMapper.adminWorkshopRegistrationListLecture(m);
			sb.setLength(0);
			for (int j = 0; j < lectures.size(); j++) { 
				Map<String, Object> l = lectures.get(j);
				if (j != 0) {
					sb.append(" / ");
				}
				sb.append(StringUtil.getContent(l.get("lecture_title")));
				if (!"N".equals(l.get("lecture_type"))) {
					sb.append(" (").append(l.get("apply_text")).append(")");
				}
				if ("Y".equals(l.get("course1")) || "Y".equals(l.get("course2"))) {
					sb.append(" - 코스 : ");
				}
				if ("Y".equals(l.get("course1"))) {
					sb.append(l.get("lecture_course1"));
				}
				if ("Y".equals(l.get("course2"))) {
					if ("Y".equals(l.get("course1"))) {
						sb.append(", ");
					}
					sb.append(l.get("lecture_course2"));
				}
			}
			
			m.put("lectures", lectures);
			m.put("lectures_text", sb.toString());
			m.put("regdate_text", DateUtil.getDateString(StringUtil.getContent(m.get("regdate")), "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss"));
		}
		
		// 반환값 생성
		List<String> columns = new ArrayList<>();
		columns.add("lectures_text");
		columns.add("name");
		columns.add("name_eng");
		columns.add("major");
		columns.add("office");
		columns.add("mobile");
		columns.add("zipcode");
		columns.add("address");
		columns.add("address_etc");
		columns.add("email");
		columns.add("license");
		columns.add("income_cost");
		columns.add("income_date");
		columns.add("income_name");
		columns.add("status");
		columns.add("regdate_text");
		
		Map<String, String> columnTitles = new HashMap<>();
		columnTitles.put("lectures_text", "행사");
		columnTitles.put("name", "이름(국문)");
		columnTitles.put("name_eng", "이름(영문)");
		columnTitles.put("major", "전문과목");
		columnTitles.put("office", "병원명");
		columnTitles.put("mobile", "연락처");
		columnTitles.put("zipcode", "우편번호");
		columnTitles.put("address", "기본주소");
		columnTitles.put("address_etc", "상세주소");
		columnTitles.put("email", "이메일");
		columnTitles.put("license", "의사면허번호");
		columnTitles.put("income_cost", "총금액");
		columnTitles.put("income_date", "결제일");
		columnTitles.put("income_name", "입금자명");
		columnTitles.put("status", "입금상태");
		columnTitles.put("regdate_text", "등록일");
		
		Map<String, Object> excel = new HashMap<>();
		excel.put("filename", "REGISTRATIONS");
		excel.put("columns", columns);
		excel.put("titles", columnTitles);
		excel.put("list", list);
		return new ModelAndView(new ExcelDownloadView(), excel);
	}
	
	@Transactional
	public RESTResult apiAdminWorkshopRegistrationStatus(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		mSocietyMapper.workshopRegistrationStatus(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminWorkshopRegistrationDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		mSocietyMapper.workshopRegistrationDeleteItem(param);
		mSocietyMapper.workshopRegistrationDeleteItemLecture(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for Front
	public RESTResult apiWorkshopCfg(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sSocietyMapper.workshopCfgItem(param);
		if (cfg == null) {
			throw new Exception("워크샵 정보를 읽어올 수 없습니다.");
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		if (ms.isLogin()) {
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}

		// 반환값 입력
		ret.put("cfg", cfg);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiWorkshopList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		Map<String, Object> cfg = sSocietyMapper.workshopCfgItem(param);
		if (cfg == null) {
			throw new Exception("워크샵 정보를 읽어올 수 없습니다.");
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		
		// 기본값 정리
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		int numPerPage = StringUtil.isEmpty(param.get("num_per_page")) ? this.numPerPage : Integer.parseInt(StringUtil.getContent(param.get("num_per_page")));
		int pagePerBlock = StringUtil.isEmpty(param.get("page_per_block")) ? this.pagePerBlock : Integer.parseInt(StringUtil.getContent(param.get("page_per_block")));

		// 페이징 작성
		int totalCount = sSocietyMapper.workshopListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());

		// 반환값 입력
		ret.put("cfg", cfg);
		ret.put("list", sSocietyMapper.workshopList(param));
		ret.put("paging", paging);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiWorkshopView(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 워크샵 정보 조회
		Map<String, Object> cfg = sSocietyMapper.workshopCfgItem(param);
		if (cfg == null) {
			throw new Exception("워크샵 정보를 읽어올 수 없습니다.");
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		ret.put("cfg", cfg);
		ret.put("article", sSocietyMapper.workshopView(param));
		ret.put("files", sSocietyMapper.workshopViewFileList(param));
		ret.put("lectures", sSocietyMapper.workshopViewLectureList(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiWorkshopRegistration(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		List<String> lectures = new ArrayList<>();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			if (key.startsWith("lectures")) {
				if ("Y".equals(param.get(key))) {
					lectures.add(key.split("-")[1]);
				}
			}
		}
		
		// 워크샵 정보 조회
		Map<String, Object> info = sSocietyMapper.workshopRegistrationInfo(param);
		if (info == null) {
			throw new Exception("정보를 읽어올 수 없습니다.");
		}
		
		if (!"Y".equals(info.get("registration"))) {
			throw new Exception("사전등록을 할 수 없습니다.");
		}
		
		if (!"Y".equals(info.get("can_registration"))) {
			throw new Exception("사전등록 기간이 아닙니다.");
		}
		
		for (String lidx : lectures) {
			Map<String, Object> p = new HashMap<>();
			p.put("sitecd", param.get("sitecd"));
			p.put("code", param.get("code"));
			p.put("widx", param.get("idx"));
			p.put("idx", lidx);
			
			Map<String, Object> lec = sSocietyMapper.workshopRegistrationLectureInfo(p);
			if (lec == null) {
				throw new Exception("정보를 읽어올 수 없습니다.");
			}
			
			if (!"Y".equals(lec.get("can_registration"))) {
				throw new Exception(StringUtil.getContent(lec.get("title"))+"의 인원이 만료되었습니다.");
			}
			if ("Y".equals(lec.get("use_course"))) {
				if (!"Y".equals(lec.get("can_course1"))) {
					throw new Exception(StringUtil.getContent(lec.get("course1"))+"의 인원이 만료되었습니다.");
				}
				
				if (!"Y".equals(lec.get("can_course2"))) {
					throw new Exception(StringUtil.getContent(lec.get("course2"))+"의 인원이 만료되었습니다.");
				}
			}
		}
		
		if (sSocietyMapper.workshopRegistrationCheckCount(param) > 0) {
			throw new Exception("이미 등록된 면허번호 입니다.");
		}
		
		int newIdx = sSocietyMapper.workshopRegistrationNewIdx(param);
		param.put("widx", param.get("idx"));
		param.put("idx", newIdx);
		
		mSocietyMapper.workshopRegistration(param);
		for (String lidx : lectures) {
			Map<String, Object> p = new HashMap<>();
			p.put("sitecd", param.get("sitecd"));
			p.put("code", param.get("code"));
			p.put("widx", param.get("widx"));
			p.put("ridx", newIdx);
			p.put("lidx", lidx);
			p.put("type", param.get("type-"+lidx));
			p.put("course1", "Y".equals(param.get("course1-"+lidx)) ? "Y":"N");
			p.put("course2", "Y".equals(param.get("course2-"+lidx)) ? "Y":"N");
			mSocietyMapper.workshopRegistrationLecture(p);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiWorkshopRegistrationCheck(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 워크샵 정보 조회
		Map<String, Object> cfg = sSocietyMapper.workshopCfgItem(param);
		if (cfg == null) {
			throw new Exception("워크샵 정보를 읽어올 수 없습니다.");
		}
		
		Map<String, Object> registration = sSocietyMapper.workshopRegistrationCheck(param);
		if (registration == null) {
			throw new Exception("입력하신 정보로 등록된 정보가 없습니다.");
		}
		
		registration.put("lectures", sSocietyMapper.workshopRegistrationCheckLecture(registration));
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		ret.put("cfg", cfg);
		ret.put("registration", registration);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiWorkshopRegistrationModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		mSocietyMapper.workshopRegistrationModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiWorkshopMonthly(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("필수코드가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("year"))) {
			param.put("year", DateUtil.getDateString("yyyy"));
		}
		
		if (StringUtil.isEmpty(param.get("month"))) {
			param.put("month", DateUtil.getDateString("MM"));
		}
		
		int year = Integer.parseInt(StringUtil.getContent(param.get("year")));
		int month = Integer.parseInt(StringUtil.getContent(param.get("month")));
		int size = StringUtil.isEmpty(param.get("size")) ? 1 : Integer.parseInt(StringUtil.getContent(param.get("size")));
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		List<Map<String, Object>> list = new ArrayList<>();
		
		// 회원정보정리
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		ret.put("cfg", sSocietyMapper.workshopCfgItem(param));
		ret.put("list", list);
		
		for (int i = 0; i < size; i++) {
			Map<String, Object> m = new HashMap<>();
			m.put("year", year);
			m.put("month", String.format("%02d", month));

			Map<String, Object> pMap = new HashMap<>();
			pMap.putAll(param);
			pMap.putAll(m);
			
			m.put("list", sSocietyMapper.workshopMonthlyList(pMap));
			list.add(m);
			if (month == 12) {
				year++;
				month = 1;
			} else {
				month++;
			}
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiWorkshopFullCalendar(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("필수코드가 누락되었습니다.");
		}
				
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSocietyMapper.workshopMonthlyListCalendar(param));
	}
	
	public RESTResult apiAdminHospitalList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sSocietyMapper.adminHospitalListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sSocietyMapper.adminHospitalList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiAdminHospitalAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("office"))) {
			throw new Exception("병원명을 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("의사명을 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("zipcode")) || StringUtil.isEmpty(param.get("address"))) {
			throw new Exception("주소를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("phone"))) {
			throw new Exception("전화번호를 입력해 주세요.");
		}
		
		mSocietyMapper.adminHospitalAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminHospitalItem(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSocietyMapper.adminHospitalItem(param));
	}
	
	@Transactional
	public RESTResult apiAdminHospitalModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("office"))) {
			throw new Exception("병원명을 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("의사명을 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("zipcode")) || StringUtil.isEmpty(param.get("address"))) {
			throw new Exception("주소를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("phone"))) {
			throw new Exception("전화번호를 입력해 주세요.");
		}
		
		mSocietyMapper.adminHospitalModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminHospitalRemove(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mSocietyMapper.adminHospitalRemove(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSearchMember(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());

		// 기본값 정리
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}

		// 페이징 작성
		int totalCount = sSocietyMapper.memberListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());

		Map<String, Object> ret = new HashMap<>();
		ret.put("count", sSocietyMapper.memberCount(param));
		ret.put("list", sSocietyMapper.memberList(param));
		ret.put("paging", paging);
				
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiSearchHospital(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());

		// 기본값 정리
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}

		// 페이징 작성
		int totalCount = sSocietyMapper.hospitalListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());

		Map<String, Object> ret = new HashMap<>();
		ret.put("list", sSocietyMapper.hospitalList(param));
		ret.put("paging", paging);
				
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiSearchHospitalLocation(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSocietyMapper.hospitalLocationSearch(param));
	}
	
}
