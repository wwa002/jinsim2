package kr.dmon.wb.service;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.simpleemail.*;
import com.amazonaws.services.simpleemail.model.*;
import com.amazonaws.util.IOUtils;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterAdminMapper;
import kr.dmon.wb.mapper.master.MasterResourceMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveAdminMapper;
import kr.dmon.wb.mapper.slave.SlaveBoardMapper;
import kr.dmon.wb.mapper.slave.SlaveBuilderMapper;
import kr.dmon.wb.mapper.slave.SlaveConferenceMapper;
import kr.dmon.wb.mapper.slave.SlaveDBMapper;
import kr.dmon.wb.mapper.slave.SlaveEventMapper;
import kr.dmon.wb.mapper.slave.SlaveMemberMapper;
import kr.dmon.wb.mapper.slave.SlaveResourceMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveSocietyMapper;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.EncryptUtils;
import kr.dmon.wb.utils.StringUtil;

@Service
public class ResourceService extends BaseService {

	private static Logger logger = Logger.getLogger(ResourceService.class);
	
	@Value("${builder.aws.accessKey}")
	private String awsAccessKey;
	
	@Value("${builder.aws.secretKey}")
	private String awsSecretKey;
	
	@Value("${builder.aws.bucket}")
	private String awsBucket;
	
	@Value("${builder.aws.s3endpoint}")
	private String awsS3EndPoint;
	
	@Autowired
	MasterResourceMapper mResourceMapper;
	
	@Autowired
	SlaveResourceMapper sResourceMapper;
	
	@Autowired
	SlaveBuilderMapper sBuilderMapper;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterAdminMapper mAdminMapper;
	
	@Autowired
	SlaveAdminMapper sAdminMapper;
	
	@Autowired
	SlaveMemberMapper sMemberMapper;
	
	@Autowired
	SlaveBoardMapper sBoardMapper;
	
	@Autowired
	SlaveDBMapper sDBMapper;
	
	@Autowired
	SlaveEventMapper sEventMapper;
	
	@Autowired
	SlaveConferenceMapper sConferenceMapper;
	
	@Autowired
	SlaveSocietyMapper sSocietyMapper;
	
	@Autowired
	RestTemplate restTemplete;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	private boolean isValidMime(Object mime) {
		return isValidMime(StringUtil.getContent(mime));
	}
	
	private boolean isValidMime(String mime) {
		if (StringUtil.isEmpty(mime)) {
			return false;
		}
		String m = mime.toLowerCase();
		if (m.startsWith("text") || m.startsWith("image")) {
			return true;
		} else if (m.equals("application/font-woff") || m.equals("application/font-woff2")) {
			return true;
		}
		return false;
	}
	
	private File getTemporaryFile(HttpServletRequest request, String path) throws Exception {
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator+DateUtil.getDateString("yyyyMMdd")+File.separator;
		File file = new File(tempPath+path);
		if (!file.exists()) {
			logger.debug("Download From S3 : " + path);
			S3Object s3obj = download(path);
			S3ObjectInputStream ois = s3obj.getObjectContent();
			
			Files.createDirectories(Paths.get(file.getParentFile().getAbsolutePath()));
			IOUtils.copy(ois, new FileOutputStream(file));
		}
		return file;
	}
	
	private File getThumbnail(File file, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("width")) && StringUtil.isEmpty(param.get("height"))) {
			return file;
		} else if (StringUtil.isEmpty(param.get("width"))) {
			return getThumbnail(file, 0, Integer.parseInt(StringUtil.getContent(param.get("height"))));
		} else if (StringUtil.isEmpty(param.get("height"))) {
			return getThumbnail(file, Integer.parseInt(StringUtil.getContent(param.get("width"))), 0);
		} else {
			return getThumbnail(file, Integer.parseInt(StringUtil.getContent(param.get("width"))), Integer.parseInt(StringUtil.getContent(param.get("height"))));
		}
	}
	
	private File getThumbnail(File file, int width, int height) throws Exception {
		String filename = file.getName().toLowerCase();
		String extension = filename.substring(filename.lastIndexOf(".")+1);
		if (extension.equals("png") || extension.equals("jpg")) {
			String folder = file.getParentFile().getAbsolutePath() + File.separator + "thumbnails";
			File dir = new File(folder);
			if (!dir.exists()) {
				Files.createDirectories(Paths.get(folder));
			}

			String destname = width+"x"+height+"_"+filename;
			File dest = new File(folder+File.separator+destname);
			if (dest.exists()) {
				return dest;
			}
			
			BufferedImage bufferOriginal = ImageIO.read(file);
			if (width == 0) {
				width = (bufferOriginal.getWidth() * height)/bufferOriginal.getHeight();
			} else if (height == 0) {
				height = (bufferOriginal.getHeight() * width)/bufferOriginal.getWidth();
			}
			BufferedImage bufferThumbnail = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
			Graphics2D graphic = bufferThumbnail.createGraphics();
			graphic.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphic.drawImage(bufferOriginal, 0, 0, width, height, null);
			ImageIO.write(bufferThumbnail, extension, dest);
			return dest;
		}
		
		return file;
	}
	
	public ResponseEntity<byte[]> imageDownload(String path, HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(path)) {
			throw new Exception("정보를 읽어올 수 없습니다.");
		}
		
		File file = getTemporaryFile(request, path);
		if (!StringUtil.isEmpty(param.get("width")) || !StringUtil.isEmpty(param.get("height"))) {
			file = getThumbnail(file, param);
		}
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (path.endsWith("png")) {
			headers.setContentType(MediaType.IMAGE_PNG);
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (path.endsWith("jpg")) {
			headers.setContentType(MediaType.IMAGE_JPEG);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (path.endsWith("gif")) {
			headers.setContentType(MediaType.IMAGE_GIF);
			response.setContentType(MediaType.IMAGE_GIF_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(StringUtil.getContent(param.get("name")), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	// for WEB
	public ResponseEntity<byte[]> resourceDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sResourceMapper.itemForFile(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (isValidMime(item.get("mime"))) {
			String mime = item.get("mime").toString();
			MediaType mt = MediaType.valueOf(mime);
			headers.setContentType(mt);
			response.setContentType(mt.toString());
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> resourceFrontDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		if (ss.getResource() == null) {
			ss.setResource(sResourceMapper.listForResource(param));
		}
		
		Map<String, Object> item = ss.getResource().get(StringUtil.getContent(param.get("idx")));
		if (item == null) {
			ss.setResource(sResourceMapper.listForResource(param));
		}
		
		item = ss.getResource().get(StringUtil.getContent(param.get("idx")));
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (isValidMime(item.get("mime"))) {
			String mime = item.get("mime").toString();
			MediaType mt = MediaType.valueOf(mime);
			headers.setContentType(mt);
			response.setContentType(mt.toString());
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> resourceFrontDownloadDir(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		String dir1= StringUtil.getContent(param.get("dir1"));
		String dir2= StringUtil.getContent(param.get("dir2"));
		String dir3= StringUtil.getContent(param.get("dir3"));
		String name= StringUtil.getContent(param.get("name"));
		if (StringUtil.isEmpty(dir1)) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (ss.getResource() == null) {
			ss.setResource(sResourceMapper.listForResource(param));
		}
		
		if (!StringUtil.isEmpty(dir3)) {
			String parent = ss.getResourceIdx("0", dir1);
			parent = ss.getResourceIdx(parent, dir2);
			parent = ss.getResourceIdx(parent, dir3);
			String idx = ss.getResourceIdx(parent, name);
			param.put("idx", idx);
		} else if (!StringUtil.isEmpty(dir2)) {
			String parent = ss.getResourceIdx("0", dir1);
			parent = ss.getResourceIdx(parent, dir2);
			String idx = ss.getResourceIdx(parent, name);
			param.put("idx", idx);
		} else {
			String parent = ss.getResourceIdx("0", dir1);
			String idx = ss.getResourceIdx(parent, name);
			param.put("idx", idx);
		}
		
		return resourceFrontDownload(request, response, param);
	}
	
	public ResponseEntity<byte[]> resourceBoardDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if (ms.isLogin()) {
			Map<String, Object> pMem = new HashMap<>();
			pMem.put("sitecd", ms.getSitecd());
			pMem.put("idx", ms.getIdx());
			
			mem = sMemberMapper.getInfoFromIdx(pMem);
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		Map<String, Object> item = sBoardMapper.fileItem(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		// 회원전용검증
		String mime = StringUtil.getContent(item.get("mime")).toLowerCase(); 
		if (mime.endsWith("png") || mime.endsWith("jpg") || mime.endsWith("jpeg") || mime.endsWith("jpe") || mime.endsWith("gif")) {
			if (!StringUtil.isEmpty(cfg.get("permit_list"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				if (!"ADMIN".equals(mem.get("type"))) {
					String[] pers = StringUtil.getContent(cfg.get("permit_list")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("권한이 없습니다.");
					}
				}
			}
		} else {
			if (!StringUtil.isEmpty(cfg.get("permit_view"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				if (!"ADMIN".equals(mem.get("type"))) {
					String[] pers = StringUtil.getContent(cfg.get("permit_view")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("권한이 없습니다.");
					}
				}
			}
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		if (!StringUtil.isEmpty(param.get("width")) || !StringUtil.isEmpty(param.get("height"))) {
			file = getThumbnail(file, param);
		}
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (isValidMime(item.get("mime"))) {
			MediaType mt = MediaType.valueOf(mime);
			headers.setContentType(mt);
			response.setContentType(mt.toString());
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> resourceProfile(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sBuilderMapper.systemMemberItemForId(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("photo")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		String name = item.get("photo").toString().toLowerCase();
		if (name.endsWith("png")) {
			headers.setContentType(MediaType.IMAGE_PNG);
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (name.endsWith("jpg") || name.endsWith("jpeg") || name.endsWith("jpe")) {
			headers.setContentType(MediaType.IMAGE_JPEG);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (name.endsWith("gif")) {
			headers.setContentType(MediaType.IMAGE_GIF);
			response.setContentType(MediaType.IMAGE_GIF_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> resourceFrontProfile(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sBuilderMapper.systemMemberItemForId(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("photo")));
		if (!StringUtil.isEmpty(param.get("width")) || !StringUtil.isEmpty(param.get("height"))) {
			file = getThumbnail(file, param);
		}
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		String name = item.get("photo").toString().toLowerCase();
		if (name.endsWith("png")) {
			headers.setContentType(MediaType.IMAGE_PNG);
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (name.endsWith("jpg") || name.endsWith("jpeg") || name.endsWith("jpe")) {
			headers.setContentType(MediaType.IMAGE_JPEG);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (name.endsWith("gif")) {
			headers.setContentType(MediaType.IMAGE_GIF);
			response.setContentType(MediaType.IMAGE_GIF_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> resourceProfileTemp(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("filename"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		File file = getTemporaryFile(request, ss.getSitecd()+"/profile/"+StringUtil.getContent(param.get("filename")));
		if (!StringUtil.isEmpty(param.get("width")) || !StringUtil.isEmpty(param.get("height"))) {
			file = getThumbnail(file, param);
		}
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		String name = StringUtil.getContent(param.get("filename")).toLowerCase();
		if (name.endsWith("png")) {
			headers.setContentType(MediaType.IMAGE_PNG);
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (name.endsWith("jpg") || name.endsWith("jpeg") || name.endsWith("jpe")) {
			headers.setContentType(MediaType.IMAGE_JPEG);
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if (name.endsWith("gif")) {
			headers.setContentType(MediaType.IMAGE_GIF);
			response.setContentType(MediaType.IMAGE_GIF_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(StringUtil.getContent(param.get("filename")), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> castIconDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.castItem(param);
		if (item == null) {
			throw new Exception("캐스트 정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("image_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> castBackgroundDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.castItem(param);
		if (item == null) {
			throw new Exception("캐스트 정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("background_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("background_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> castPageDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.castPageItem(param);
		if (item == null) {
			throw new Exception("캐스트 페이지 정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("image_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> infoIconDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.infoItem(param);
		if (item == null) {
			throw new Exception("캐스트 정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("image_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> infoContentIconDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.infoContentItem(param);
		if (item == null) {
			throw new Exception("콘텐츠 정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("image_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> infoContentBackgroundDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.infoContentItem(param);
		if (item == null) {
			throw new Exception("콘텐츠 정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("background_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("background_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> infoContentPageDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("dbidx")) || StringUtil.isEmpty(param.get("dbgidx")) || StringUtil.isEmpty(param.get("dbcidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.infoPageItem(param);
		if (item == null) {
			throw new Exception("콘텐츠 정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("image_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> eventIconDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sEventMapper.adminItem(param);
		if (item == null) {
			throw new Exception("이벤트 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> eventPageDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("eidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sEventMapper.pageItem(param);
		if (item == null) {
			throw new Exception("이벤트 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> eventPlanDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sEventMapper.adminPlanItem(param);
		if (item == null) {
			throw new Exception("프로젝트 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> hospitalPhotoDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.hospitalPhotoItem(param);
		if (item == null) {
			throw new Exception("정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("image_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> pharmacyPhotoDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.pharmacyPhotoItem(param);
		if (item == null) {
			throw new Exception("정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("image_path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("image_path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> sessionDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sConferenceMapper.sessionItem(param);
		if (item == null) {
			throw new Exception("세션 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> workshopBannerDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sSocietyMapper.workshopView(param);
		if (item == null) {
			throw new Exception("워크샵 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("banner")), request, response, param);
	}
	
	public ResponseEntity<byte[]> workshopFileDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code")) || StringUtil.isEmpty(param.get("widx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sSocietyMapper.workshopViewForFile(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if (isValidMime(item.get("mime"))) {
			String mime = item.get("mime").toString();
			MediaType mt = MediaType.valueOf(mime);
			headers.setContentType(mt);
			response.setContentType(mt.toString());
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("name").toString(), "UTF-8").replaceAll("\\+", "%20"));
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<byte[]> mailFileDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("tidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}

		Map<String, Object> item = sAdminMapper.mailTemplateFileItem(param); 
		if (item == null) {
			throw new Exception("잘못된 파일 정보 입니다.");
		}
		
		// 회원전용검증
		String filename = StringUtil.getContent(item.get("filename")).toLowerCase(); 
		if (filename.endsWith("png") || filename.endsWith("jpg") || filename.endsWith("jpeg") || filename.endsWith("jpe") || filename.endsWith("gif")) {
			return imageDownload(StringUtil.getContent(item.get("path")), request, response, param);
		}
		
		File file = getTemporaryFile(request, StringUtil.getContent(item.get("path")));
		byte[] bytes = Files.readAllBytes(file.toPath());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("attachment", URLEncoder.encode(item.get("filename").toString(), "UTF-8").replaceAll("\\+", "%20"));
		return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
	}
	
	public ResponseEntity<byte[]> storePhotoDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("sidx")) || StringUtil.isEmpty(param.get("idx")) || StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		Map<String, Object> item = sDBMapper.storePhotoItem(param);
		if (item == null) {
			throw new Exception("정보를 읽어올 수 없습니다.");
		}
		
		if (StringUtil.isEmpty(item.get("path"))) {
			throw new Exception("이미지 정보를 읽어올 수 없습니다.");
		}
		
		return imageDownload(StringUtil.getContent(item.get("path")), request, response, param);
	}
	
	public ResponseEntity<byte[]> summernoteDownload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		String path = ss.getSitecd() + "/summernote/" + StringUtil.getContent(param.get("name"));
		return imageDownload(path, request, response, param);
	}
	
	// for API
	private List<Map<String, Object>> listForJSTree(String sitecd, int parent, int depth) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("sitecd", sitecd);
		param.put("parent", parent);
		
		Map<String, Object> state = new HashMap<>();
		state.put("opened", depth == 0);
		state.put("disabled", false);
		state.put("selected", false);
		
		List<Map<String, Object>> children = new ArrayList<>();
		List<Map<String, Object>> list = sResourceMapper.listForJSTree(param);
		for (Map<String, Object> m : list) {
			Map<String, Object> child = new HashMap<>();
			child.put("id", m.get("idx"));
			child.put("text", m.get("name"));
			child.put("data", m);
			child.put("state", state);
			if (!"Y".equals(m.get("isfolder"))) {
				child.put("icon", "none");
			}
			child.put("children", listForJSTree(sitecd, Integer.parseInt(m.get("idx").toString()), depth + 1));
			children.add(child);
		}
		return children;
	}
	
	public JSONObject apiJSTree(HttpServletRequest request, Map<String, Object> param) throws Exception {
		JSONObject ret = new JSONObject();
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		Map<String, Object> state = new HashMap<>();
		state.put("opened", true);
		state.put("disabled", false);
		state.put("selected", false);
		
		param.put("parent", 0);
		ret.put("id", "0");
		ret.put("text", "ROOT");
		ret.put("state", state);
		ret.put("children", listForJSTree(ms.getSitecd(), 0, 0));
		return ret;
	}
	
	public RESTResult apiFolderList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			param.put("idx", 0);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sResourceMapper.listForFolder(param));
	}
	
	public RESTResult apiFolderAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("folder"))) {
			throw new Exception("폴더명을 입력해 주세요.");
		}
		
		if (sResourceMapper.checkFolder(param) > 0) {
			throw new Exception("같은 폴더명이 존재합니다.");
		}
		
		if (StringUtil.isEmpty(param.get("parent"))) {
			param.put("parent", 0);
		}
		
		mResourceMapper.createFolder(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	private void deleteFolder(String sitecd, int idx) throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("sitecd", sitecd);
		param.put("idx", idx);
		
		List<Map<String, Object>> list = sResourceMapper.listForFolder(param);
		for (Map<String, Object> m : list) {
			m.put("sitecd", sitecd);
			if ("Y".equals(m.get("isfolder"))) {
				deleteFolder(sitecd, Integer.parseInt(m.get("idx").toString()));
			} else {
				delete(m.get("path").toString());
			}
			mResourceMapper.deleteFile(m);
		}
		mResourceMapper.deleteFile(param);
	}
	
	@Transactional
	public RESTResult apiDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}

		Map<String, Object> item = sResourceMapper.itemForFile(param);
		if ("Y".equals(item.get("isfolder"))) {
			deleteFolder(ms.getSitecd(), Integer.parseInt(param.get("idx").toString()));
		} else {
			delete(item.get("path").toString());
			mResourceMapper.deleteFile(item);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	@Transactional
	public RESTResult apiUploadDropzone(MultipartHttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		Iterator<String> itr = request.getFileNames();
		while (itr.hasNext()) {
			String uploadedFile = itr.next();
			MultipartFile file = request.getFile(uploadedFile);
			Map<String, Object> map = new HashMap<>();
			map.put("sitecd", ms.getSitecd());
			map.put("filename", file.getOriginalFilename());
			map.put("mimetype", file.getContentType());
			map.put("size", file.getSize());
			map.put("parent", param.get("idx"));
			map.putAll(upload(request, file));
			mResourceMapper.uploadFile(map);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiUploadBoard(MultipartHttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		Map<String, Object> map = new HashMap<>();
		map.put("sitecd", ms.getSitecd());
		map.put("code", param.get("code"));
		if (file.getOriginalFilename().endsWith("file")) {
			map.put("filename", "photo.png");
			map.put("mimetype", "image/png");
		} else {
			map.put("filename", file.getOriginalFilename());
			map.put("mimetype", file.getContentType());
		}
		map.put("size", file.getSize());
		map.putAll(upload(request, "board", file));
			
		int newIdx = sResourceMapper.boardFileNewIdx(map);
		map.put("idx", newIdx);
		mResourceMapper.uploadBoardFile(map);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, newIdx);
	}
	
	@Transactional
	public RESTResult apiUploadBoardDropzone(MultipartHttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		List<Integer> list = new ArrayList<>();
		
		Iterator<String> itr = request.getFileNames();
		while (itr.hasNext()) {
			String uploadedFile = itr.next();
			MultipartFile file = request.getFile(uploadedFile);
			Map<String, Object> map = new HashMap<>();
			map.put("sitecd", ms.getSitecd());
			map.put("code", param.get("code"));
			map.put("filename", file.getOriginalFilename());
			map.put("mimetype", file.getContentType());
			map.put("size", file.getSize());
			map.putAll(upload(request, "board", file));
			
			int newIdx = sResourceMapper.boardFileNewIdx(map);
			map.put("idx", newIdx);
			mResourceMapper.uploadBoardFile(map);
			list.add(newIdx);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, list);
	}
	
	@Transactional
	public RESTResult apiUploadWorkshopDropzone(MultipartHttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		List<Integer> list = new ArrayList<>();
		
		Iterator<String> itr = request.getFileNames();
		while (itr.hasNext()) {
			String uploadedFile = itr.next();
			MultipartFile file = request.getFile(uploadedFile);
			Map<String, Object> map = new HashMap<>();
			map.put("sitecd", ms.getSitecd());
			map.put("code", param.get("code"));
			map.put("filename", file.getOriginalFilename());
			map.put("mimetype", file.getContentType());
			map.put("size", file.getSize());
			map.putAll(upload(request, "s_workshop", file));
			
			int newIdx = sResourceMapper.societyWorkshopFileNewIdx(map);
			map.put("idx", newIdx);
			mResourceMapper.uploadSocietyWorkshopFile(map);
			list.add(newIdx);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, list);
	}
	
	// for AWS
	private AWSCredentials getCredentials() {
		return new BasicAWSCredentials(awsAccessKey, awsSecretKey);
	}
	
	private AmazonS3Client getS3Client() {
		AmazonS3Client client = new AmazonS3Client(getCredentials());
		client.setEndpoint(awsS3EndPoint);
		return client;
	}
	
	public String getUrl(String path) {
		return "https://"+awsS3EndPoint+"/"+awsBucket+"/"+path;
	}
	
	private Map<String, Object> upload(HttpServletRequest request, MultipartFile file) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator;
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = ms.getSitecd() + "/resource/" + destName;
	
		File convertFile = new File(tempPath+destName);
		try {
			file.transferTo(convertFile);
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, convertFile);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} finally {
			convertFile.delete();
		}
		
		return map;
	}
	
	public Map<String, Object> upload(HttpServletRequest request, String path, MultipartFile file) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		String tempPath = request.getSession().getServletContext().getRealPath("/")+"temp"+File.separator;
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		if ("file".equals(extension)) {
			extension = "png";
		}
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = ms.getSitecd() + "/" + path + "/" + destName;
	
		File convertFile = new File(tempPath+destName);
		try {
			file.transferTo(convertFile);
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, convertFile);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} finally {
			convertFile.delete();
		}
		
		return map;
	}
	
	public Map<String, Object> upload(HttpServletRequest request, String path, File file) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		
		if (file == null || !file.exists()) {
			throw new Exception("파일 내용이 없습니다.");
		}
		
		Map<String, Object> map = new HashMap<>();
		
		String[] splitedName = file.getName().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName = UUID.randomUUID().toString() + "." + extension;
		String pathName = ms.getSitecd() + "/" + path + "/" + destName;
	
		try {
			PutObjectRequest por = new PutObjectRequest(awsBucket, pathName, file);
			PutObjectResult ret = getS3Client().putObject(por);
			if (ret != null) {
				map.put("destName", destName);
				map.put("path", pathName);
				map.put("url", "https://"+awsS3EndPoint+"/"+awsBucket+"/"+pathName);
			}
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which " +
				"means the client encountered " +
				"an internal error while trying to " +
				"communicate with S3, " +
				"such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		} 
		return map;
	}

	private S3Object download(String path) throws Exception {
		try {
            return getS3Client().getObject(new GetObjectRequest(awsBucket, path));
        } catch (AmazonServiceException ase) {
            logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
            logger.info("Error Message:    " + ase.getMessage());
            logger.info("HTTP Status Code: " + ase.getStatusCode());
            logger.info("AWS Error Code:   " + ase.getErrorCode());
            logger.info("Error Type:       " + ase.getErrorType());
            logger.info("Request ID:       " + ase.getRequestId());
            throw ase;
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to communicate with S3, such as not being able to access the network.");
            logger.info("Error Message: " + ace.getMessage());
            throw ace;
        }
	}
	
	private void move(String path, String destPath) throws Exception{
		try {
            getS3Client().copyObject(new CopyObjectRequest(awsBucket, path, awsBucket, destPath));
            getS3Client().deleteObject(new DeleteObjectRequest(awsBucket, path));
        } catch (AmazonServiceException ase) {
        	logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
        	logger.info("Error Message:    " + ase.getMessage());
        	logger.info("HTTP Status Code: " + ase.getStatusCode());
        	logger.info("AWS Error Code:   " + ase.getErrorCode());
        	logger.info("Error Type:       " + ase.getErrorType());
        	logger.info("Request ID:       " + ase.getRequestId());
        	throw ase;
        } catch (AmazonClientException ace) {
        	logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to  communicate with S3, such as not being able to access the network.");
        	logger.info("Error Message: " + ace.getMessage());
        	throw ace;
        } catch (Exception e) {
			throw e;
		}
	}
	
	public void delete(String path) throws Exception {
		try {
			getS3Client().deleteObject(new DeleteObjectRequest(awsBucket, path));
		} catch (AmazonServiceException ase) {
			logger.info("Caught an AmazonServiceException, which means your request made it to Amazon S3, but was rejected with an error response for some reason.");
			logger.info("Error Message:    " + ase.getMessage());
			logger.info("HTTP Status Code: " + ase.getStatusCode());
			logger.info("AWS Error Code:   " + ase.getErrorCode());
			logger.info("Error Type:       " + ase.getErrorType());
			logger.info("Request ID:       " + ase.getRequestId());
			throw ase;
		} catch (AmazonClientException ace) {
		    logger.info("Caught an AmazonClientException, which means the client encountered an internal error while trying to communicate with S3, such as not being able to access the network.");
		    logger.info("Error Message: " + ace.getMessage());
		    throw ace;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public boolean sendMail(String[] to, String from, String subject, String content) throws Exception {
		Destination destination = new Destination().withToAddresses(to);
		
		Content sesSubject = new Content().withData(subject);
		Content sesContent = new Content().withData(content);
		Body body = new Body().withText(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
	
	public boolean sendMailHtml(String[] to, String from, String subject, String content) throws Exception {
		Destination destination = new Destination().withToAddresses(to);
		
		Content sesSubject = new Content().withData(subject);
		Content sesContent = new Content().withData(content);
		Body body = new Body().withHtml(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(from).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}
	
	public boolean sendFromMailer(Map<String, Object> param) throws Exception {
		Destination destination = new Destination().withToAddresses(StringUtil.getContent(param.get("email")));
		
		Content sesSubject = new Content().withData(StringUtil.getContent(param.get("title")));
		Content sesContent = new Content().withData(StringUtil.getContent(param.get("content")));
		Body body = new Body().withHtml(sesContent);
		
		Message message = new Message().withSubject(sesSubject).withBody(body);
		SendEmailRequest request = new SendEmailRequest().withSource(StringUtil.getContent(param.get("from"))).withDestination(destination).withMessage(message);
		try {
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(getCredentials());
			Region REGION = Region.getRegion(Regions.US_WEST_2);
			client.setRegion(REGION);
			client.sendEmail(request);
			return true;
		} catch (Exception e) {
			logger.error(e);
			return false;
		}
	}

	@Transactional
	public void mailRead(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		mResourceMapper.mailRead(param);
		mResourceMapper.mailReadCount(param);
	}
	
	public void sendSMS(Map<String, Object> cfg, Map<String, Object> data) throws Exception {
		String content = StringUtil.getContent(data.get("content"));
		String[] sender = StringUtil.getContent(cfg.get("sender")).split("-");
		
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("user_id", EncryptUtils.base64Encode(StringUtil.getContent(cfg.get("auth1")))); // 아이디
		parameters.add("secure", EncryptUtils.base64Encode(StringUtil.getContent(cfg.get("auth2")))); // 인증키
		parameters.add("sphone1", EncryptUtils.base64Encode(sender[0])); // 보내는 번호
		parameters.add("sphone2", EncryptUtils.base64Encode(sender[1])); // 보내는 번호
		parameters.add("sphone3", EncryptUtils.base64Encode(sender[2])); // 보내는 번호
		parameters.add("rphone", EncryptUtils.base64Encode(StringUtil.getContent(data.get("mobile")).replaceAll("-", ""))); // 받는번호
		parameters.add("msg", EncryptUtils.base64Encode(content)); // 메시지
		parameters.add("mode", EncryptUtils.base64Encode("1"));
		
		if (content.length() <= 45) {
			parameters.add("smsType", EncryptUtils.base64Encode("S"));
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
			String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
			if (!result.startsWith("success")) {
				throw new Exception("발송에 실패하였습니다.");
			}
		} else {
			parameters.add("smsType", EncryptUtils.base64Encode("L"));
			parameters.add("subject", EncryptUtils.base64Encode(StringUtil.getContent(data.get("title"))));
			
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
			String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
			if (!result.startsWith("success")) {
				throw new Exception("발송에 실패하였습니다.");
			}
		}
	}
}
