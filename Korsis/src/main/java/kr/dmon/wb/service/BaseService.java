package kr.dmon.wb.service;

import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.StringUtil;

public abstract class BaseService {

	public abstract SiteSession getSiteSession(HttpServletRequest request) throws Exception;
	public abstract MemberSession getMemberSession(HttpServletRequest request) throws Exception;
	
	public Map<String, Object> replaceTemplate(Map<String, Object> html, Map<String, Object> data) throws Exception {
		Iterator<String> keys = html.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			html.put(key, replaceTemplate(StringUtil.getContent(html.get(key)), data));
		}
		return html;
	}
	
	public String replaceTemplate(String html, Map<String, Object> data) throws Exception {
		Iterator<String> keys = data.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			html = replaceTemplate(html, key, StringUtil.getContent(data.get(key)));
		}
		return html;
	}
	
	public String replaceTemplate(String html, String key, String data) throws Exception {
		return html.replaceAll("\\{\\{" + key + "\\}\\}", data);
	}
	
}
