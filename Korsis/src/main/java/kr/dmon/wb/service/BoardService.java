package kr.dmon.wb.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterBoardMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveBoardMapper;
import kr.dmon.wb.mapper.slave.SlaveMemberMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.api.APIService;
import kr.dmon.wb.session.BoardSession;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.StringUtil;

@Service
public class BoardService extends BaseService {

	private static Logger logger = Logger.getLogger(APIService.class);

	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterBoardMapper mBoardMapper;
	
	@Autowired
	SlaveBoardMapper sBoardMapper;
	
	@Autowired
	SlaveMemberMapper sMemberMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	@Transactional
	public RESTResult index(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		if (ms.isLogin()) {
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}

		// 기본값 정리
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		if (Integer.parseInt(StringUtil.getContent(param.get("page"))) == 1) {
			ret.put("banner", sBoardMapper.listBanner(param));
		}

		// 페이징 작성
		int totalCount = sBoardMapper.listTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());

		// 반환값 입력
		ret.put("list", sBoardMapper.list(param));
		ret.put("paging", paging);
		ret.put("keyword", StringUtil.getContent(param.get("keyword")));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult cfg(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		if (ms.isLogin()) {
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}

		// 반환값 입력
		ret.put("cfg", cfg);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult list(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		ret.put("me", ms.getData());
		if (ms.isLogin()) {
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		if (!ms.isAdmin()) {
			// 회원전용검증
			if ("Y".equals(cfg.get("permit_list_member"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
			}
			
			if (!StringUtil.isEmpty(cfg.get("permit_list"))) { 
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				String[] pers = StringUtil.getContent(cfg.get("permit_list")).split(",");
				boolean isOk = false;
				for (String p : pers) {
					if (ms.isPermitGcd(p)) {
						isOk = true;
						break;
					}
				}
				if (!isOk) {
					throw new Exception("목록 권한이 없습니다.");
				}
			}
		}
		
		// 기본값 정리
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		int numPerPage = Integer.parseInt(StringUtil.isEmpty(param.get("num_per_page")) ? StringUtil.getContent(cfg.get("num_per_page")) : StringUtil.getContent(param.get("num_per_page")));
		int pagePerBlock = Integer.parseInt(StringUtil.isEmpty(param.get("page_per_block")) ? StringUtil.getContent(cfg.get("page_per_block")) : StringUtil.getContent(param.get("page_per_block")));

		if ("PPPP".equals(ss.getSitecd()) && "partner".equals(StringUtil.getContent(param.get("code")))) {
			// 페이징 작성
			int totalCount = sBoardMapper.listPPPPPartnerTotal(param);
			Paging paging = new Paging();
			paging.setPageNo(Integer.parseInt(param.get("page").toString()));
			paging.setPageSize(numPerPage);
			paging.setBlockSize(pagePerBlock);
			paging.setTotalCount(totalCount);
			param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
			param.put("pageSize", paging.getPageSize());

			// 반환값 입력
			ret.put("cfg", cfg);
			ret.put("notice", sBoardMapper.listNotice(param));
			ret.put("list", sBoardMapper.listPPPPPartner(param));
			ret.put("paging", paging);
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
		} else if ("BEAUTYRUN".equals(ss.getSitecd()) && "partner".equals(StringUtil.getContent(param.get("code")))) {
			// 페이징 작성
			int totalCount = sBoardMapper.listBEAUTYRUNPartnerTotal(param);
			Paging paging = new Paging();
			paging.setPageNo(Integer.parseInt(param.get("page").toString()));
			paging.setPageSize(numPerPage);
			paging.setBlockSize(pagePerBlock);
			paging.setTotalCount(totalCount);
			param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
			param.put("pageSize", paging.getPageSize());

			// 반환값 입력
			ret.put("cfg", cfg);
			ret.put("notice", sBoardMapper.listNotice(param));
			ret.put("list", sBoardMapper.listBEAUTYRUNPartner(param));
			ret.put("paging", paging);
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
		}
		// 페이징 작성
		int totalCount = sBoardMapper.listTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());

		// 반환값 입력
		ret.put("cfg", cfg);
		ret.put("notice", sBoardMapper.listNotice(param));
		ret.put("list", sBoardMapper.list(param));
		ret.put("paging", paging);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult write(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 작성 필수값 정리
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		param.put("notice", "Y".equals(param.get("notice")) ? "Y" : "N");
		param.put("hidden", "Y".equals(param.get("hidden")) ? "Y" : "N");
		param.put("popup", "Y".equals(param.get("popup")) ? "Y" : "N");
		param.put("popup_width", StringUtil.isEmpty(param.get("popup_width"))?"0":param.get("popup_width"));
		param.put("popup_height", StringUtil.isEmpty(param.get("popup_height"))?"0":param.get("popup_height"));
		param.put("popup_top", StringUtil.isEmpty(param.get("popup_top"))?"0":param.get("popup_top"));
		param.put("popup_left", StringUtil.isEmpty(param.get("popup_left"))?"0":param.get("popup_left"));
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if ("Y".equals(cfg.get("permit_write_member"))) {
			if (!ms.isLogin()) {
				throw new Exception("로그인 후 이용해 주세요.");
			}
		}
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
			
			// 게시판 별명 사용에 따라 입력 이름 정리
			if ("Y".equals(StringUtil.getContent(cfg.get("nickname"))) && !StringUtil.isEmpty(mem.get("nickname"))) {
				param.put("name", mem.get("nickname"));
			} else {
				param.put("name", mem.get("name"));
			}
		} else {
			param.put("midx", 0);
		}
		
		if (!ms.isAdmin()) {
			// 회원전용검증
			if (!StringUtil.isEmpty(cfg.get("permit_write"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				String[] pers = StringUtil.getContent(cfg.get("permit_write")).split(",");
				boolean isOk = false;
				for (String p : pers) {
					if (ms.isPermitGcd(p)) {
						isOk = true;
						break;
					}
				}
				if (!isOk) {
					throw new Exception("작성 권한이 없습니다.");
				}
			} else {
				if (!ms.isLogin()) {
					// 비회원 허용
					if (StringUtil.isEmpty(param.get("name"))) {
						throw new Exception("이름을 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("passwd"))) {
						throw new Exception("비밀번호를 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("captcha"))) {
						throw new Exception("스팸방지코드를 입력해 주세요.");
					}
					
					if (!StringUtil.getContent(param.get("captcha")).equals(request.getSession().getAttribute("captchaAnswer"))) {
						throw new Exception("스팸방지코드를 확인해 주세요.");
					}
				}
			}
		}
		
		param.put("ip", request.getRemoteAddr());
		
		int newIdx = sBoardMapper.boardNewIdx(param);
		param.put("idx", newIdx);
		mBoardMapper.write(param);
		
		if (!StringUtil.isEmpty(param.get("files"))) {
			mBoardMapper.uploadedFileUpdate(param);
			mBoardMapper.updateMainPicture(param);
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult view(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if ("Y".equals(cfg.get("permit_view_member"))) {
			if (!ms.isLogin()) {
				throw new Exception("로그인 후 이용해 주세요.");
			}
		}
		
		ret.put("me", ms.getData());
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		if (!ms.isAdmin()) {
			Map<String, Object> article = sBoardMapper.viewForPermission(param);
			if ("Y".equals(StringUtil.getContent(article.get("hidden")))) {
				// 비밀 게시물
				BoardSession bs = (BoardSession) request.getSession().getAttribute(BoardSession.getSessionKey(article));
				if (bs == null) {
					// 원본 게시물 조회
					Map<String, Object> first = sBoardMapper.viewForFirstHidden(article);
					if (first == null) {
						throw new Exception("원글이 삭제되어 조회 할 수 없는 게시물 입니다.");
					}
					
					int midx = Integer.parseInt(StringUtil.getContent(first.get("midx"), "0"));
					if (midx == 0) {
						throw new Exception("비밀번호를 인증 후 이용해 주세요.");
					} else if (midx != 0 && midx == ms.getIdx()) {
						// 본인의 게시물은 패스
					} else {
						throw new Exception("비밀게시물은 본인과 관리자만 접근 할 수 있습니다.");
					}
				}
			} else {
				// 회원전용검증
				if (!StringUtil.isEmpty(cfg.get("permit_view"))) {
					if (!ms.isLogin()) {
						throw new Exception("로그인 후 이용해 주세요.");
					}
					
					String[] pers = StringUtil.getContent(cfg.get("permit_view")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("열람 권한이 없습니다.");
					}
				}
			}
		}
		
		
		// 조회수 적용
		param.put("ip", request.getRemoteAddr());
		if (sBoardMapper.viewCheckRead(param) == 0) {
			mBoardMapper.viewReadInsert(param);
		}
		if ("Y".equals(cfg.get("absolute_read_count"))) {
			mBoardMapper.viewCountAbsolute(param);
		} else {
			mBoardMapper.viewCount(param);
		}
		
		ret.put("cfg", cfg);
		ret.put("article", sBoardMapper.view(param));
		ret.put("files", sBoardMapper.viewFileList(param));
		ret.put("status", sBoardMapper.flagStatus(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult favorite(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		
		if (!ms.isAdmin()) {
			Map<String, Object> article = sBoardMapper.viewForPermission(param);
			if ("Y".equals(StringUtil.getContent(article.get("hidden")))) {
				// 비밀 게시물
				BoardSession bs = (BoardSession) request.getSession().getAttribute(BoardSession.getSessionKey(article));
				if (bs == null) {
					// 원본 게시물 조회
					Map<String, Object> first = sBoardMapper.viewForFirstHidden(article);
					if (first == null) {
						throw new Exception("원글이 삭제되어 조회 할 수 없는 게시물 입니다.");
					}
					
					int midx = Integer.parseInt(StringUtil.getContent(first.get("midx"), "0"));
					if (midx == 0) {
						throw new Exception("비밀번호를 인증 후 이용해 주세요.");
					} else if (midx != 0 && midx == ms.getIdx()) {
						// 본인의 게시물은 패스
					} else {
						throw new Exception("비밀게시물은 본인과 관리자만 접근 할 수 있습니다.");
					}
				}
			} else {
				// 회원전용검증
				if (!StringUtil.isEmpty(cfg.get("permit_view"))) {
					if (!ms.isLogin()) {
						throw new Exception("로그인 후 이용해 주세요.");
					}
					
					String[] pers = StringUtil.getContent(cfg.get("permit_view")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("열람 권한이 없습니다.");
					}
				}
			}
		}
		
		Map<String, Object> status = sBoardMapper.flagStatus(param);
		if (status == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		mBoardMapper.favorite(param);
		if ("Y".equals(status.get("favorite"))) {
			mBoardMapper.favoriteDown(param);
		} else {
			mBoardMapper.favoriteUp(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult recommend(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("midx", ms.getIdx());
		
		if (!ms.isAdmin()) {
			Map<String, Object> article = sBoardMapper.viewForPermission(param);
			if ("Y".equals(StringUtil.getContent(article.get("hidden")))) {
				// 비밀 게시물
				BoardSession bs = (BoardSession) request.getSession().getAttribute(BoardSession.getSessionKey(article));
				if (bs == null) {
					// 원본 게시물 조회
					Map<String, Object> first = sBoardMapper.viewForFirstHidden(article);
					if (first == null) {
						throw new Exception("원글이 삭제되어 조회 할 수 없는 게시물 입니다.");
					}
					
					int midx = Integer.parseInt(StringUtil.getContent(first.get("midx"), "0"));
					if (midx == 0) {
						throw new Exception("비밀번호를 인증 후 이용해 주세요.");
					} else if (midx != 0 && midx == ms.getIdx()) {
						// 본인의 게시물은 패스
					} else {
						throw new Exception("비밀게시물은 본인과 관리자만 접근 할 수 있습니다.");
					}
				}
			} else {
				// 회원전용검증
				if (!StringUtil.isEmpty(cfg.get("permit_view"))) {
					if (!ms.isLogin()) {
						throw new Exception("로그인 후 이용해 주세요.");
					}
					
					String[] pers = StringUtil.getContent(cfg.get("permit_view")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("열람 권한이 없습니다.");
					}
				}
			}
		}
		
		Map<String, Object> status = sBoardMapper.flagStatus(param);
		if (status == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		mBoardMapper.recommend(param);
		if ("Y".equals(status.get("recommend"))) {
			mBoardMapper.recommendDown(param);
		} else {
			mBoardMapper.recommendUp(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult modify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 작성 필수값 정리
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		param.put("notice", "Y".equals(param.get("notice")) ? "Y" : "N");
		param.put("hidden", "Y".equals(param.get("hidden")) ? "Y" : "N");
		param.put("popup", "Y".equals(param.get("popup")) ? "Y" : "N");
		param.put("popup_width", StringUtil.isEmpty(param.get("popup_width"))?"0":param.get("popup_width"));
		param.put("popup_height", StringUtil.isEmpty(param.get("popup_height"))?"0":param.get("popup_height"));
		param.put("popup_top", StringUtil.isEmpty(param.get("popup_top"))?"0":param.get("popup_top"));
		param.put("popup_left", StringUtil.isEmpty(param.get("popup_left"))?"0":param.get("popup_left"));
		
		if (!StringUtil.isEmpty(param.get("popup_startdate"))) {
			param.put("popup_startdate", StringUtil.getContent(param.get("popup_startdate")).replaceAll("-", ""));
		}
		if (!StringUtil.isEmpty(param.get("popup_enddate"))) {
			param.put("popup_enddate", StringUtil.getContent(param.get("popup_enddate")).replaceAll("-", ""));
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if ("Y".equals(cfg.get("permit_write_member"))) {
			if (!ms.isLogin()) {
				throw new Exception("로그인 후 이용해 주세요.");
			}
		}
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		Map<String, Object> article = sBoardMapper.viewForPermission(param);
		if (article == null) {
			throw new Exception("게시물을 읽어 올 수 없습니다.");
		}
		
		if (!ms.isAdmin()) {
			int article_midx = StringUtil.isEmpty(article.get("midx")) ? 0 : Integer.parseInt(StringUtil.getContent(article.get("midx")));  
			if (article_midx > 0) {
				if (article_midx != ms.getIdx()) {
					throw new Exception("본인의 게시물만 수정 할 수 있습니다.");
				}
			} else {
				article.put("passwd", param.get("passwd"));
				if (sBoardMapper.checkPassword(article) < 1) {
					throw new Exception("비밀번호가 잘못되었습니다.");
				}
			}
		}
		
		mBoardMapper.modify(param);
		if ("A".equals(article.get("thread"))) {
			mBoardMapper.modifyCategory(param);
		}
		if (!StringUtil.isEmpty(param.get("files"))) {
			mBoardMapper.uploadedFileUpdate(param);
			mBoardMapper.updateMainPicture(param);
			mBoardMapper.removedFileUpdate(param);
		} else {
			mBoardMapper.deleteFiles(param);
			mBoardMapper.updateMainPicture(param);
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult modifyCount(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		if (!ms.isAdmin()) {
			throw new Exception("관리자 전용 기능입니다.");
		}
		mBoardMapper.modifyCount(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult modifyBanner(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		if (!ms.isAdmin()) {
			throw new Exception("관리자 전용 기능입니다.");
		}
		mBoardMapper.modifyBanner(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult delete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		Map<String, Object> article = sBoardMapper.viewForPermission(param);
		if (article == null) {
			throw new Exception("게시물을 읽어 올 수 없습니다.");
		}
		
		if (!ms.isAdmin()) {
			int article_midx = StringUtil.isEmpty(article.get("midx")) ? 0 : Integer.parseInt(StringUtil.getContent(article.get("midx")));  
			if (article_midx > 0) {
				if (article_midx != ms.getIdx()) {
					throw new Exception("본인의 게시물만 삭제 할 수 있습니다.");
				}
			} else {
				article.put("passwd", param.get("passwd"));
				if (sBoardMapper.checkPassword(article) < 1) {
					throw new Exception("비밀번호가 잘못되었습니다.");
				}
			}
		}
		
		if ("N".equals(cfg.get("thread_delete"))) {
			article.put("sitecd", param.get("sitecd"));
			article.put("code", param.get("code"));
			if (sBoardMapper.deleteThread(article) > 0) {
				throw new Exception("답변이 있는 게시물은 삭제 할 수 없습니다.");
			}
		}
		
		mBoardMapper.delete(param);
		mBoardMapper.deleteComments(param);
		mBoardMapper.deleteReads(param);
		mBoardMapper.deleteFiles(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult reply(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 작성 필수값 정리
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if ("Y".equals(cfg.get("permit_reply_member"))) {
			if (!ms.isLogin()) {
				throw new Exception("로그인 후 이용해 주세요.");
			}
		}
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
			
			// 게시판 별명 사용에 따라 입력 이름 정리
			if ("Y".equals(StringUtil.getContent(cfg.get("nickname"))) && !StringUtil.isEmpty(mem.get("nickname"))) {
				param.put("name", mem.get("nickname"));
			} else {
				param.put("name", mem.get("name"));
			}
		} else {
			param.put("midx", 0);
		}
		
		if (!ms.isAdmin()) {
			// 회원전용검증
			if (!StringUtil.isEmpty(cfg.get("permit_reply"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				String[] pers = StringUtil.getContent(cfg.get("permit_reply")).split(",");
				boolean isOk = false;
				for (String p : pers) {
					if (ms.isPermitGcd(p)) {
						isOk = true;
						break;
					}
				}
				if (!isOk) {
					throw new Exception("답변 권한이 없습니다.");
				}
			} else {
				// 비회원 허용
				if (!ms.isLogin()) {
					if (StringUtil.isEmpty(param.get("name"))) {
						throw new Exception("이름을 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("passwd"))) {
						throw new Exception("비밀번호를 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("captcha"))) {
						throw new Exception("스팸방지코드를 입력해 주세요.");
					}
					
					if (!StringUtil.getContent(param.get("captcha")).equals(request.getSession().getAttribute("captchaAnswer"))) {
						throw new Exception("스팸방지코드를 확인해 주세요.");
					}
				}
			}
		}
		
		Map<String, Object> article = sBoardMapper.view(param);
		if (article == null) {
			throw new Exception("게시물을 읽어 올 수 없습니다.");
		}
		
		// 상위게시물의 fidx, hidden, category 상속 및 답변글은 공지 방지
		param.put("notice", "N");
		param.put("fidx", article.get("fidx"));
		param.put("hidden", article.get("hidden"));
		param.put("category", article.get("category"));
		
		// 상위게시물의 Thread를 상속받아 단계 증가
		article.put("sitecd", param.get("sitecd"));
		article.put("code", param.get("code"));
		Map<String, Object> replyThread = sBoardMapper.replyThread(article);
		if (replyThread != null) {
			String thread = replyThread.get("thread").toString();
			String thread_head = thread.substring(0, thread.length()-1);
			int thread_end = replyThread.get("rgt").toString().charAt(0)+1;
			param.put("thread", thread_head + String.valueOf(Character.toChars(thread_end)));
		} else {
			param.put("thread", article.get("thread")+"A");
		}  
		
		param.put("ip", request.getRemoteAddr());

		int newIdx = sBoardMapper.boardNewIdx(param);
		param.put("idx", newIdx);
		mBoardMapper.reply(param);
		
		if (!StringUtil.isEmpty(param.get("files"))) {
			mBoardMapper.uploadedFileUpdate(param);
			mBoardMapper.updateMainPicture(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult cert(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 작성 필수값 정리
		if (StringUtil.isEmpty(param.get("passwd"))) {
			throw new Exception("비밀번호를 입력해 주세요.");
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		Map<String, Object> article = sBoardMapper.viewForPermission(param);
		if (article == null) {
			throw new Exception("게시물 정보를 읽어 올 수 없습니다.");
		}
		
		if (!"A".equals(StringUtil.getContent(article.get("thread")))) {
			article = sBoardMapper.viewForFirstHidden(article);
		} 

		if (article == null) {
			throw new Exception("원글이 삭제되어 조회 할 수 없는 게시물 입니다.");
		}
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		article.put("passwd", param.get("passwd"));
		if (sBoardMapper.checkPassword(article) < 1) {
			throw new Exception("비밀번호가 잘못되었습니다.");
		}
		
		BoardSession bs = new BoardSession();
		bs.setData(article);
		request.getSession().setAttribute(bs.getId(), bs);
				
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult comment(HttpServletRequest request, Map<String, Object> param) throws Exception {
		String mode = StringUtil.getContent(param.get("mode"));
		if ("write".equals(mode)) {
			return commentWrite(request, param);
		} else if ("modify".equals(mode)) {
			return commentModify(request, param);
		} else if ("reply".equals(mode)) {
			return commentReply(request, param);
		} else if ("delete".equals(mode)) {
			return commentDelete(request, param);
		} else {
			return commentList(request, param);
		}
	}
	
	@Transactional
	public RESTResult commentList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if ("Y".equals(cfg.get("permit_view_member"))) {
			if (!ms.isLogin()) {
				throw new Exception("로그인 후 이용해 주세요.");
			}
		}
		ret.put("me", ms.getData());
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		if (!ms.isAdmin()) {
			// 회원전용검증
			if (!StringUtil.isEmpty(cfg.get("permit_view"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				String[] pers = StringUtil.getContent(cfg.get("permit_view")).split(",");
				boolean isOk = false;
				for (String p : pers) {
					if (ms.isPermitGcd(p)) {
						isOk = true;
						break;
					}
				}
				if (!isOk) {
					throw new Exception("권한이 없습니다.");
				}
			}
		}
		
		ret.put("cfg", cfg);
		ret.put("comments", sBoardMapper.commentList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult commentWrite(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if ("Y".equals(cfg.get("permit_list_comment"))) {
			if (!ms.isLogin()) {
				throw new Exception("로그인 후 이용해 주세요.");
			}
		}
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
			
			// 게시판 별명 사용에 따라 입력 이름 정리
			if ("Y".equals(StringUtil.getContent(cfg.get("nickname"))) && !StringUtil.isEmpty(mem.get("nickname"))) {
				param.put("name", mem.get("nickname"));
			} else {
				param.put("name", mem.get("name"));
			}
		} else {
			param.put("midx", 0);
		}
		
		if (!ms.isAdmin()) {
			// 회원전용검증
			if (!StringUtil.isEmpty(cfg.get("permit_comment"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				String[] pers = StringUtil.getContent(cfg.get("permit_write")).split(",");
				boolean isOk = false;
				for (String p : pers) {
					if (ms.isPermitGcd(p)) {
						isOk = true;
						break;
					}
				}
				if (!isOk) {
					throw new Exception("댓글 작성 권한이 없습니다.");
				}
			} else {
				if (!ms.isLogin()) {
					// 비회원 허용
					if (StringUtil.isEmpty(param.get("name"))) {
						throw new Exception("이름을 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("passwd"))) {
						throw new Exception("비밀번호를 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("captcha"))) {
						throw new Exception("스팸방지코드를 입력해 주세요.");
					}
					if (!StringUtil.getContent(param.get("captcha")).equals(request.getSession().getAttribute("captchaAnswer"))) {
						throw new Exception("스팸방지코드를 확인해 주세요.");
					}
				}
			}
		}
		
		param.put("ip", request.getRemoteAddr());
		
		int newIdx = sBoardMapper.commentNewIdx(param);
		param.put("idx", newIdx);
		mBoardMapper.commentWrite(param);
		mBoardMapper.commentCount(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult commentModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 입력값 확인
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		Map<String, Object> comment = sBoardMapper.commentItem(param);
		if (comment == null) {
			throw new Exception("댓글 정보를 읽어 올 수 없습니다.");
		}
		
		if (!ms.isAdmin()) {
			int comment_midx = StringUtil.isEmpty(comment.get("midx")) ? 0 : Integer.parseInt(StringUtil.getContent(comment.get("midx")));  
			if (comment_midx > 0) {
				// 회원의 게시물 일 경우. (관리자는 패스)
				if (comment_midx != ms.getIdx()) {
					throw new Exception("본인의 댓글만 수정 할 수 있습니다.");
				}
			} else {
				comment.put("passwd", param.get("passwd"));
				if (sBoardMapper.checkCommentPassword(comment) < 1) {
					throw new Exception("비밀번호가 잘못되었습니다.");
				}
			}
		}
		
		mBoardMapper.commentModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult commentReply(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 입력값 확인
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if ("Y".equals(cfg.get("permit_comment_member"))) {
			if (!ms.isLogin()) {
				throw new Exception("로그인 후 이용해 주세요.");
			}
		}
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
			
			// 게시판 별명 사용에 따라 입력 이름 정리
			if ("Y".equals(StringUtil.getContent(cfg.get("nickname"))) && !StringUtil.isEmpty(mem.get("nickname"))) {
				param.put("name", mem.get("nickname"));
			} else {
				param.put("name", mem.get("name"));
			}
		} else {
			param.put("midx", 0);
		}
		
		if (!ms.isAdmin()) {
			// 회원전용검증
			if (!StringUtil.isEmpty(cfg.get("permit_comment"))) {
				if (!ms.isLogin()) {
					throw new Exception("로그인 후 이용해 주세요.");
				}
				
				String[] pers = StringUtil.getContent(cfg.get("permit_comment")).split(",");
				boolean isOk = false;
				for (String p : pers) {
					if (ms.isPermitGcd(p)) {
						isOk = true;
						break;
					}
				}
				if (!isOk) {
					throw new Exception("답변 권한이 없습니다.");
				}
			} else {
				// 비회원 허용
				if (!ms.isLogin()) {
					// 비회원 허용
					if (StringUtil.isEmpty(param.get("name"))) {
						throw new Exception("이름을 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("passwd"))) {
						throw new Exception("비밀번호를 입력해 주세요.");
					}
					if (StringUtil.isEmpty(param.get("captcha"))) {
						throw new Exception("스팸방지코드를 입력해 주세요.");
					}
					
					if (!StringUtil.getContent(param.get("captcha")).equals(request.getSession().getAttribute("captchaAnswer_"+StringUtil.getContent(param.get("idx"))))) {
						throw new Exception("스팸방지코드를 확인해 주세요.");
					}
				}
			}
		}
		
		
		Map<String, Object> comment = sBoardMapper.commentItem(param);
		if (comment == null) {
			throw new Exception("댓글 정보를 읽어 올 수 없습니다.");
		}
		
		// 상위게시물의 fidx 상속
		param.put("fidx", comment.get("fidx"));
		
		// 상위게시물의 Thread를 상속받아 단계 증가
		comment.put("sitecd", param.get("sitecd"));
		comment.put("code", param.get("code"));
		Map<String, Object> replyThread = sBoardMapper.commentReplyThread(comment);
		if (replyThread != null) {
			String thread = replyThread.get("thread").toString();
			String thread_head = thread.substring(0, thread.length()-1);
			int thread_end = replyThread.get("rgt").toString().charAt(0)+1;
			param.put("thread", thread_head + String.valueOf(Character.toChars(thread_end)));
		} else {
			param.put("thread", comment.get("thread")+"A");
		}  

		param.put("ip", request.getRemoteAddr());
		
		int newIdx = sBoardMapper.commentNewIdx(param);
		param.put("idx", newIdx);
		mBoardMapper.commentReply(param);
		mBoardMapper.commentCount(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult commentDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 입력값 확인
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("필수 정보가 누락되었습니다.");
		}
		
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());

		// 게시판 정보 조회
		Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
		if (cfg == null) {
			throw new Exception("게시판 정보를 읽어올 수 없습니다.");
		}
		
		// 회원정보정리
		Map<String, Object> mem = null;
		MemberSession ms = getMemberSession(request);
		if (ms.isLogin()) {
			param.put("midx", ms.getIdx());
			
			mem = ms.getData();
			if (mem == null) {
				throw new Exception("회원정보를 읽어올 수 없습니다.");
			}
		}
		
		Map<String, Object> comment = sBoardMapper.commentItem(param);
		if (comment == null) {
			throw new Exception("댓글 정보를 읽어 올 수 없습니다.");
		}
		
		if (!ms.isAdmin()) {
			int comment_midx = StringUtil.isEmpty(comment.get("midx")) ? 0 : Integer.parseInt(StringUtil.getContent(comment.get("midx")));  
			if (comment_midx > 0) {
				// 회원의 게시물 일 경우. (관리자는 패스)
				if (comment_midx != ms.getIdx()) {
					throw new Exception("본인의 댓글만 삭제 할 수 있습니다.");
				}
			} else {
				comment.put("passwd", param.get("passwd"));
				if (sBoardMapper.checkCommentPassword(comment) < 1) {
					throw new Exception("비밀번호가 잘못되었습니다.");
				}
			}
		}
		
		
		if ("N".equals(cfg.get("thread_delete"))) {
			comment.put("sitecd", param.get("sitecd"));
			comment.put("code", param.get("code"));
			if (sBoardMapper.commentDeleteThread(comment) > 0) {
				throw new Exception("하위 댓글이 있는 댓글은 삭제 할 수 없습니다.");
			}
		}
		
		mBoardMapper.commentDelete(param);
		mBoardMapper.commentCount(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult popup(HttpServletRequest request, Map<String, Object> param) throws Exception {
		// 사이트 정보 조회
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sBoardMapper.popupList(param));
	}
}
