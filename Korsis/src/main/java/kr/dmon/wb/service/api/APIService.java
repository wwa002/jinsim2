package kr.dmon.wb.service.api;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveCommonMapper;
import kr.dmon.wb.mapper.slave.SlaveDBMapper;
import kr.dmon.wb.mapper.slave.SlaveEventMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.BaseService;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.EncryptUtils;
import kr.dmon.wb.utils.StringUtil;

@Service
public class APIService extends BaseService {

	private static Logger logger = Logger.getLogger(APIService.class);

	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;

	@Autowired
	SlaveEventMapper sEventMapper;
	
	@Autowired
	SlaveDBMapper sDBMapper;
	
	@Autowired
	SlaveCommonMapper sCommonMapper;
	
	@Autowired
	ResourceService resourceService;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	public RESTResult notice(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("os"))) {
			throw new Exception("OS 정보가 누락되었습니다.");
		}
		if (!"ios".equals(param.get("os")) && !"android".equals(param.get("os"))) {
			throw new Exception("지원되지 않는 OS입니다.");
		}

		if (ss.getSitecd().equals("PPPP")) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAPIMapper.noticeMedical(param));
		} else {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAPIMapper.notice(param));
		}
	}
	
	public RESTResult checkId(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		
		if (sAPIMapper.checkId(param) > 0) {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 아이디 입니다.", RESTResult.ERROR);
		} else {
			return new RESTResult(RESTResult.SUCCESS, "사용 가능한 아이디 입니다.", RESTResult.OK);
		}
	}
	
	public RESTResult checkNickname(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("nickname"))) {
			throw new Exception("별명을 입력해 주세요.");
		}
		
		if (sAPIMapper.checkNickname(param) > 0) {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 별명 입니다.", RESTResult.ERROR);
		} else {
			return new RESTResult(RESTResult.SUCCESS, "사용 가능한 별명 입니다.", RESTResult.OK);
		}
	}
	
	public RESTResult findId(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일을 입력해 주세요.");
		}
		
		Map<String, Object> m = sAPIMapper.findId(param);
		if (m == null) {
			throw new Exception("입력하신 아이디로 가입된 회원정보를 확인 할 수 없습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, m);
	}
	
	public RESTResult findPassword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일을 입력해 주세요.");
		}
		
		Map<String, Object> m = sAPIMapper.findPassword(param);
		if (m == null) {
			throw new Exception("입력하신 아이디로 가입된 회원정보를 확인 할 수 없습니다.");
		}
		m.put("passwd", StringUtil.randomString(15));
		mAPIMapper.resetPassword(m);
		
		param.put("code", "password");
		Map<String, Object> mailTemplate = sAPIMapper.getMailTemplate(param);
		if (mailTemplate != null) {
			mailTemplate.put("content", replaceTemplate(StringUtil.getContent(mailTemplate.get("content")), m));
			resourceService.sendMailHtml(new String[]{StringUtil.getContent(param.get("email"))}, ss.getEmail(), StringUtil.getContent(mailTemplate.get("title")), StringUtil.getContent(mailTemplate.get("content")));
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult join(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		param.put("ip", request.getRemoteAddr());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("passwd"))) {
			throw new Exception("비밀번호를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("name")) && StringUtil.isEmpty(param.get("name_first")) && StringUtil.isEmpty(param.get("name_last"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			param.put("name", StringUtil.getContent(param.get("name_last"))+StringUtil.getContent(param.get("name_first")));
		}
		
		if (sAPIMapper.checkId(param) > 0) {
			throw new Exception("이미 사용중인 아이디 입니다.");
		}
		
		if (!StringUtil.isEmpty(param.get("mobile"))) {
			param.put("mobile", StringUtil.getContent(param.get("mobile")).replaceAll("-", ""));
		}
		
		if (!StringUtil.isEmpty(param.get("profile"))) {
			StringBuffer sb = new StringBuffer();
			sb.append(request.getSession().getServletContext().getRealPath("/")).append("temp").append(File.separator);
			sb.append(DateUtil.getDateString("yyyyMMdd")).append(File.separator);
			sb.append(ss.getSitecd()).append(File.separator);
			sb.append("profile").append(File.separator);
			sb.append(StringUtil.getContent(param.get("profile")));
			File file = new File(sb.toString());
			if (file.exists()) {
				Map<String, Object> ret = resourceService.upload(request, "profile", file);
				param.put("path", ret.get("path"));
			}
		}
		
		int newIdx = sAPIMapper.memberNewIdx(param);
		String auth = EncryptUtils.toMD5(UUID.randomUUID().toString());
		param.put("idx", newIdx);
		param.put("memno", ss.getSitecd()+String.format("%04d", newIdx));
		param.put("auth", auth);
		mAPIMapper.memberJoin(param);
		mAPIMapper.memberJoinTerms(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, auth);
	}
	
	@Transactional
	public RESTResult joinSociety(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		param.put("ip", request.getRemoteAddr());
		
		if ("KORSIS".equals(ss.getSitecd())) {
			if (StringUtil.isEmpty(param.get("cert"))) {
				throw new Exception("인증번호를 입력해 주세요.");
			}
			String mobile = StringUtil.getContent(param.get("mobile1"))+StringUtil.getContent(param.get("mobile2"))+StringUtil.getContent(param.get("mobile3"));
			param.put("mobile", mobile);
			Map<String, Object> map = sAPIMapper.getSmsCert(param);
			if (map == null) {
				throw new Exception("인증 정보를 가져올 수 없습니다.");
			}
			if (!map.get("cert").equals(param.get("cert"))) {
				throw new Exception("인증 번호를 확인해 주세요.");
			}
		}
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("passwd"))) {
			throw new Exception("비밀번호를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (sAPIMapper.checkId(param) > 0) {
			throw new Exception("이미 사용중인 아이디 입니다.");
		}
		
		if (!StringUtil.isEmpty(param.get("mobile"))) {
			param.put("mobile", StringUtil.getContent(param.get("mobile")).replaceAll("-", ""));
		}
		
		if (!StringUtil.isEmpty(param.get("profile"))) {
			StringBuffer sb = new StringBuffer();
			sb.append(request.getSession().getServletContext().getRealPath("/")).append("temp").append(File.separator);
			sb.append(DateUtil.getDateString("yyyyMMdd")).append(File.separator);
			sb.append(ss.getSitecd()).append(File.separator);
			sb.append("profile").append(File.separator);
			sb.append(StringUtil.getContent(param.get("profile")));
			File file = new File(sb.toString());
			if (file.exists()) {
				Map<String, Object> ret = resourceService.upload(request, "profile", file);
				param.put("path", ret.get("path"));
			}
		}
		
		if (!StringUtil.isEmpty(param.get("birth"))) {
			param.put("birth", StringUtil.getContent(param.get("birth")).replaceAll("-", ""));
		}
		
		param.put("isuse", "N");
		
		int newIdx = sAPIMapper.memberNewIdx(param);
		String auth = EncryptUtils.toMD5(UUID.randomUUID().toString());
		param.put("idx", newIdx);
		param.put("memno", ss.getSitecd()+String.format("%04d", newIdx));
		param.put("auth", auth);
		mAPIMapper.memberJoin(param);
		mAPIMapper.memberJoinTerms(param);
	
		param.put("name_kr", param.get("name"));
		mAPIMapper.memberJoinSociety(param);
		
		param.put("code", "join");
		Map<String, Object> mailTemplate = sAPIMapper.getMailTemplate(param);
		if (mailTemplate != null) {
			mailTemplate.put("content", replaceTemplate(StringUtil.getContent(mailTemplate.get("content")), param));
			resourceService.sendMailHtml(new String[]{StringUtil.getContent(param.get("email"))}, ss.getEmail(), StringUtil.getContent(mailTemplate.get("title")), StringUtil.getContent(mailTemplate.get("content")));
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, auth);
	}
	
	@Transactional
	public RESTResult requestCertSMS(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호가 누락되었습니다.");
		}
		
		Map<String, Object> cfg = sAPIMapper.smsCfg(param);
		
		int cert = (int) (Math.floor(Math.random() * 1000000)+100000);
		if(cert>1000000){
		   cert = cert - 100000;
		}
		String content = "["+cert+"] "+ss.getTitle()+"에서 보내드리는 인증번호 입니다.";
		param.put("cert", cert);
		param.put("content", content);
		mAPIMapper.smsCert(param);
		resourceService.sendSMS(cfg, param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult login(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		
		param.put("sitecd", ss.getSitecd());
		param.put("ip", request.getRemoteAddr());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("passwd"))) {
			throw new Exception("비밀번호를 입력해 주세요.");
		}
		
		Map<String, Object> mem = sAPIMapper.login(param);
		if (mem == null) {
			throw new Exception("아이디와 비밀번호를 확인해 주세요.");
		}
		
		MemberSession ms = getMemberSession(request);
		ms.setData(mem);
		ms.setGcds(sAPIMapper.gcds(mem));
		
		if (StringUtil.isEmpty(ms.getAuth())) {
			String auth = EncryptUtils.toMD5(UUID.randomUUID().toString());
			param.put("auth", auth);
			mem.put("auth", auth);
			ms.setAuth(auth);
			mAPIMapper.loginAuthUpdate(param);
		} else {
			mAPIMapper.loginDateUpdate(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, mem);
	}
	
	@Transactional
	public RESTResult loginSociety(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		
		param.put("sitecd", ss.getSitecd());
		param.put("ip", request.getRemoteAddr());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		if (StringUtil.isEmpty(param.get("passwd"))) {
			throw new Exception("비밀번호를 입력해 주세요.");
		}
		
		Map<String, Object> mem = sAPIMapper.loginSociety(param);
		if (mem == null) {
			throw new Exception("아이디와 비밀번호를 확인해 주세요.");
		}
		
		if (!StringUtil.isEmpty(mem.get("date_delete"))) {
			throw new Exception("회원탈퇴를 진행중인 아이디 입니다.");
		}
		
		if (!"Y".equals(mem.get("isuse"))) {
			return new RESTResult(RESTResult.FAILURE, RESTResult.ERROR, mem);
		}
		
		MemberSession ms = getMemberSession(request);
		ms.setData(mem);
		ms.setGcds(sAPIMapper.gcds(mem));
		
		if (StringUtil.isEmpty(ms.getAuth())) {
			String auth = EncryptUtils.toMD5(UUID.randomUUID().toString());
			param.put("auth", auth);
			mem.put("auth", auth);
			ms.setAuth(auth);
			mAPIMapper.loginAuthUpdate(param);
		} else {
			mAPIMapper.loginDateUpdate(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, mem);
	}
	
	@Transactional
	public RESTResult loginSNS(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		
		param.put("sitecd", ss.getSitecd());
		param.put("ip", request.getRemoteAddr());
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		String sns = StringUtil.getContent(param.get("sns"), "sns");
		param.put("id", sns+"_"+StringUtil.getContent(param.get("id")));
		param.put("passwd", sns+"_"+StringUtil.getContent(param.get("id")));
		
		Map<String, Object> mem = sAPIMapper.login(param);
		if (mem != null) {
			MemberSession ms = getMemberSession(request);
			ms.setData(mem);
			ms.setGcds(sAPIMapper.gcds(mem));
			
			if (StringUtil.isEmpty(ms.getAuth())) {
				String auth = EncryptUtils.toMD5(UUID.randomUUID().toString());
				param.put("auth", auth);
				mem.put("auth", auth);
				ms.setAuth(auth);
				mAPIMapper.loginAuthUpdate(param);
				return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, mem);
			} else {
				mAPIMapper.loginDateUpdate(param);
				return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ms.getAuth());
			}
		} else {
			int newIdx = sAPIMapper.memberNewIdx(param);
			String auth = EncryptUtils.toMD5(UUID.randomUUID().toString());
			param.put("idx", newIdx);
			param.put("memno", ss.getSitecd()+String.format("%04d", newIdx));
			param.put("auth", auth);
			mAPIMapper.memberJoin(param);
			mAPIMapper.memberJoinTerms(param);
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, auth);
		}
	}
	
	public RESTResult myinfo(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("idx", ms.getIdx());
		
		Map<String, Object> mem = sAPIMapper.myinfo(param);
		if (mem == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		
		mem.put("terms", sAPIMapper.myinfoTerms(param));
		if (ss.useSociety()) {
			mem.put("society", sAPIMapper.myinfoSociety(param));
		}
		
		if ("PPPP".equals(ss.getSitecd())) {
			param.put("midx", ms.getIdx());
			mem.put("favorite_hospital", sDBMapper.myPageHospital(param));
			mem.put("favorite_pharmacy", sDBMapper.myPagePharmacy(param));
			mem.put("favorite_cast", sDBMapper.myPageCast(param));
			mem.put("favorite_infodb", sDBMapper.myPageInfodb(param));
			mem.put("favorite_event", sEventMapper.myPageEventFavorite(param));
			mem.put("apply_event", sEventMapper.myPageEventApply(param));
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, mem);
	}
	
	@Transactional
	public RESTResult myinfoUpdate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("idx", ms.getIdx());
		
		if (StringUtil.isEmpty(param.get("name")) && StringUtil.isEmpty(param.get("name_first")) && StringUtil.isEmpty(param.get("name_last"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			param.put("name", StringUtil.getContent(param.get("name_last"))+StringUtil.getContent(param.get("name_first")));
		}
		
		if (!StringUtil.isEmpty(param.get("mobile"))) {
			param.put("mobile", StringUtil.getContent(param.get("mobile")).replaceAll("-", ""));
		}
		
		if (!StringUtil.isEmpty(param.get("profile"))) {
			StringBuffer sb = new StringBuffer();
			sb.append(request.getSession().getServletContext().getRealPath("/")).append("temp").append(File.separator);
			sb.append(DateUtil.getDateString("yyyyMMdd")).append(File.separator);
			sb.append(ss.getSitecd()).append(File.separator);
			sb.append("profile").append(File.separator);
			sb.append(StringUtil.getContent(param.get("profile")));
			File file = new File(sb.toString());
			if (file.exists()) {
				Map<String, Object> ret = resourceService.upload(request, "profile", file);
				param.put("path", ret.get("path"));
			}
		}
		
		Map<String, Object> mem = sAPIMapper.myinfo(param);
		if (mem == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		
		mAPIMapper.memberUpdate(param);
		mAPIMapper.memberJoinTerms(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult myinfoExit(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("idx", ms.getIdx());
		
		mAPIMapper.memberSocietyExit(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult myinfoUpdatePassword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("idx", ms.getIdx());
		
		if (StringUtil.isEmpty(param.get("current_passwd"))) {
			throw new Exception("현재 비밀번호를 입력해 주세요.");
		}
		
		Map<String, Object> mem = sAPIMapper.checkPassword(param);
		if (mem == null) {
			throw new Exception("현재 비밀번호를 확인 해 주세요.");
		}
		
		mAPIMapper.memberPasswordChange(param);
		mAPIMapper.memberPasswordSociety(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult myinfoUpdateSociety(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("idx", ms.getIdx());
		
		if ("KORSIS".equals(ss.getSitecd()) && "N".equals(param.get("checkcert"))) {
			if (StringUtil.isEmpty(param.get("cert"))) {
				throw new Exception("인증번호를 입력해 주세요.");
			}
			String mobile = StringUtil.getContent(param.get("mobile1"))+StringUtil.getContent(param.get("mobile2"))+StringUtil.getContent(param.get("mobile3"));
			param.put("mobile", mobile);
			Map<String, Object> map = sAPIMapper.getSmsCert(param);
			if (map == null) {
				throw new Exception("인증 정보를 가져올 수 없습니다.");
			}
			if (!map.get("cert").equals(param.get("cert"))) {
				throw new Exception("인증 번호를 확인해 주세요.");
			}
		}
		
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (!StringUtil.isEmpty(param.get("mobile"))) {
			param.put("mobile", StringUtil.getContent(param.get("mobile")).replaceAll("-", ""));
		} else {
			param.put("mobile", StringUtil.getContent(param.get("mobile1"))+StringUtil.getContent(param.get("mobile2"))+StringUtil.getContent(param.get("mobile3")));
		}
		
		
		if (!StringUtil.isEmpty(param.get("profile"))) {
			StringBuffer sb = new StringBuffer();
			sb.append(request.getSession().getServletContext().getRealPath("/")).append("temp").append(File.separator);
			sb.append(DateUtil.getDateString("yyyyMMdd")).append(File.separator);
			sb.append(ss.getSitecd()).append(File.separator);
			sb.append("profile").append(File.separator);
			sb.append(StringUtil.getContent(param.get("profile")));
			File file = new File(sb.toString());
			if (file.exists()) {
				Map<String, Object> ret = resourceService.upload(request, "profile", file);
				param.put("path", ret.get("path"));
			}
		}
		
		if (!StringUtil.isEmpty(param.get("birth"))) {
			param.put("birth", StringUtil.getContent(param.get("birth")).replaceAll("-", ""));
		}
		
		Map<String, Object> mem = sAPIMapper.myinfo(param);
		if (mem == null) {
			throw new Exception("정보를 읽어 올 수 없습니다.");
		}
		
		mAPIMapper.memberUpdate(param);
		mAPIMapper.memberJoinTerms(param);
		
		param.put("name_kr", param.get("name"));
		mAPIMapper.memberUpdateSociety(param);
		
		param.put("code", "member_modify");
		param.put("id", ms.getId());
		Map<String, Object> mailTemplate = sAPIMapper.getMailTemplate(param);
		if (mailTemplate != null) {
			mailTemplate.put("content", replaceTemplate(StringUtil.getContent(mailTemplate.get("content")), param));
			resourceService.sendMailHtml(new String[]{StringUtil.getContent(param.get("email"))}, ss.getEmail(), StringUtil.getContent(mailTemplate.get("title")), StringUtil.getContent(mailTemplate.get("content")));
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult searchAll(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		Map<String, Object> ret = new HashMap<>();
		if ("PPPP".equals(ss.getSitecd())) {
			ret.put("hospital", sDBMapper.searchHospital(param));
			ret.put("pharmacy", sDBMapper.searchPharmacy(param));
			ret.put("cast", sDBMapper.apiCastSearch(param));
			ret.put("infodb", sDBMapper.apiInfoContentSearch(param));
			ret.put("event", sEventMapper.apiSearch(param));	
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult searchKeyword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sCommonMapper.keywordListAll(param));
	}
	
	public RESTResult profileUpload(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드 해주세요.");
		}
		
		MemberSession ms = getMemberSession(request);
		if (!ms.isLogin()) {
			throw new Exception("로그인 후 이용해 주세요.");
		}
		param.put("idx", ms.getIdx());
		
		StringBuffer sb = new StringBuffer();
		sb.append(request.getSession().getServletContext().getRealPath("/")).append("temp").append(File.separator);
		sb.append(DateUtil.getDateString("yyyyMMdd")).append(File.separator);
		sb.append(ss.getSitecd()).append(File.separator);
		sb.append("profile").append(File.separator);
		
		String tempPath = sb.toString();
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName; 
		if (StringUtil.isEmpty(extension) || "file".equals(extension)) {
			destName = UUID.randomUUID().toString() + ".png";
		} else {
			destName = UUID.randomUUID().toString() + "." + extension;
		}
		
		File convertFile = new File(tempPath+destName);
		file.transferTo(convertFile);
		
		Map<String, Object> up = resourceService.upload(request, "profile", convertFile);
		param.put("path", up.get("path"));
		
		Map<String, Object> mem = sAPIMapper.myinfo(param);
		if (!StringUtil.isEmpty(mem.get("photo"))) {
			resourceService.delete(StringUtil.getContent(mem.get("photo")));
		}
		
		mAPIMapper.memberProfilePhoto(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult profileUploadTemp(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드 해주세요.");
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append(request.getSession().getServletContext().getRealPath("/")).append("temp").append(File.separator);
		sb.append(DateUtil.getDateString("yyyyMMdd")).append(File.separator);
		sb.append(ss.getSitecd()).append(File.separator);
		sb.append("profile").append(File.separator);
		
		String tempPath = sb.toString();
		File mkdir = new File(tempPath);
		if (!mkdir.exists()) {
			mkdir.mkdirs();
		}
		
		String[] splitedName = file.getOriginalFilename().split("\\.");
		String extension = splitedName[splitedName.length - 1];
		String destName; 
		if (StringUtil.isEmpty(extension) || "file".equals(extension)) {
			destName = UUID.randomUUID().toString() + ".png";
		} else {
			destName = UUID.randomUUID().toString() + "." + extension;
		}
		
		File convertFile = new File(tempPath+destName);
		file.transferTo(convertFile);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, destName);
	}
	
	public RESTResult sendmail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 

		String referer = request.getHeader("referer");
		String serverName = request.getServerName();
		
		if (StringUtil.isEmpty(referer) || !referer.contains(serverName)) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("to"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("subject"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("content"))) {
			throw new Exception("내용을 입력해 주세요.");
		}
		
		String[] to = StringUtil.getContent(param.get("to")).split(",");

		resourceService.sendMail(to, ss.getEmail(), StringUtil.getContent(param.get("subject")), StringUtil.getContent(param.get("content")));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult refuseSms(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호를 입력해 주세요.");
		}
		
		if (!StringUtil.validMobile(StringUtil.getContent(param.get("mobile")))) {
			throw new Exception("올바른 형태의 휴대폰번호를 입력해 주세요. (- 포함)");
		}
		
		param.put("sitecd", ss.getSitecd());

		if (sAPIMapper.checkRefuseSMS(param) > 0) {
			throw new Exception("이미 수신거부된 번호 입니다.");
		}
		
		mAPIMapper.refuseSms(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult refuseEmail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
			throw new Exception("올바른 형태의 메일 주소를 입력해 주세요.");
		}
		
		param.put("sitecd", ss.getSitecd());

		if (sAPIMapper.checkRefuseEmail(param) > 0) {
			throw new Exception("이미 수신거부된 이메일 입니다.");
		}
		
		mAPIMapper.refuseEmail(param);

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult acceptSms(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호를 입력해 주세요.");
		}
		
		if (!StringUtil.validMobile(StringUtil.getContent(param.get("mobile")))) {
			throw new Exception("올바른 형태의 휴대폰번호를 입력해 주세요. (- 포함)");
		}
		
		param.put("sitecd", ss.getSitecd());

		if (sAPIMapper.checkRefuseSMS(param) == 0) {
			throw new Exception("수신 허용된 번호 입니다.");
		}
		
		mAPIMapper.acceptSms(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult acceptEmail(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		} 
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
			throw new Exception("올바른 형태의 메일 주소를 입력해 주세요.");
		}
		
		param.put("sitecd", ss.getSitecd());
		
		if (sAPIMapper.checkRefuseEmail(param) == 0) {
			throw new Exception("수신 허용된 메일 주소 입니다.");
		}
		
		mAPIMapper.acceptEmail(param);

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult storeList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAPIMapper.storeList(param));
	}
	
	
	public RESTResult summernoteUpload(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("사이트 정보를 읽어올 수 없습니다.");
		}
		param.put("sitecd", ss.getSitecd());
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드 해주세요.");
		}

		Map<String, Object> map = resourceService.upload(request, "summernote", file);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, request.getScheme() + "://" + request.getServerName()+"/api/summernote/" + StringUtil.getContent(map.get("destName")));
	}
}
