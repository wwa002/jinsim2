package kr.dmon.wb.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.StringUtil;

@Service
public class CommonService extends BaseService {

	private static Logger logger = Logger.getLogger(CommonService.class);

	@Autowired
	MasterSiteMapper mSiteMapper;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	public void accessJoin (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			SiteSession ss = getSiteSession(request);
			if (StringUtil.isEmpty(ss.getSitecd())) {
				return;
			}
			param.put("sitecd", ss.getSitecd());
			mSiteMapper.join(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@Transactional
	public void accessLogin (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			MemberSession ms = getMemberSession(request);
			if (ms.isLogin()) {
				SiteSession ss = getSiteSession(request);
				if (StringUtil.isEmpty(ss.getSitecd())) {
					return;
				}
				param.put("sitecd", ss.getSitecd());
				param.put("id", ms.getId());
				param.put("ip", request.getRemoteAddr());
				mSiteMapper.login(param);
				mSiteMapper.loginReferer(param);
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardWrite (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			SiteSession ss = getSiteSession(request);
			if (StringUtil.isEmpty(ss.getSitecd())) {
				return;
			}
			param.put("sitecd", ss.getSitecd());
			mSiteMapper.boardWrite(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardCommentWrite (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			SiteSession ss = getSiteSession(request);
			if (StringUtil.isEmpty(ss.getSitecd())) {
				return;
			}
			param.put("sitecd", ss.getSitecd());
			mSiteMapper.boardCommentWrite(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardUpload (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			SiteSession ss = getSiteSession(request);
			if (StringUtil.isEmpty(ss.getSitecd())) {
				return;
			}
			param.put("sitecd", ss.getSitecd());
			mSiteMapper.boardFileUpload(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void accessBoardDownload (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			SiteSession ss = getSiteSession(request);
			if (StringUtil.isEmpty(ss.getSitecd())) {
				return;
			}
			param.put("sitecd", ss.getSitecd());
			mSiteMapper.boardFileDownload(param);
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	@Transactional
	public void accessAnalysis (HttpServletRequest request, Map<String, Object> param) throws Exception {
		try {
			MemberSession ms = getMemberSession(request);
			if (ms.isAdmin()) {
				return;
			}
			
			SiteSession ss = getSiteSession(request);
			if (StringUtil.isEmpty(ss.getSitecd())) {
				return;
			}
			
			String ip = request.getRemoteAddr();
			String userAgent = request.getHeader("User-Agent");
			String referer = request.getHeader("referer");
			
			param.put("ip", ip);
			param.put("agent", userAgent);
			param.put("referer", referer);
			param.put("url", request.getRequestURL().toString());
			
			if (userAgent.contains("Google") || userAgent.contains("Yahoo") || userAgent.contains("naver") || userAgent.contains("bot")) {
				ss.setLog(ip + "-" + DateUtil.getDateString());
				ss.setAgent(userAgent);
				return;
			}
			
			param.put("sitecd", ss.getSitecd());
			
			Map<String, Object> url = sSiteMapper.getUrl(param);
			if (url == null) {
				mSiteMapper.insertUrl(param);
			} else {
				mSiteMapper.updateUrl(url);
			}
			if (sSiteMapper.countUrl(param) > 50000) {
				url = sSiteMapper.getMinUrl(param);
				mSiteMapper.deleteUrls(url);
			}
			
			if (!StringUtil.isEmpty(ss.getLog())) {
				mSiteMapper.updateAccessPageCounter(param);
				if (!StringUtil.isEmpty(param.get("keyword"))) {
					Map<String, Object> inKey = sSiteMapper.getInKey(param);
					if (inKey == null) {
						mSiteMapper.insertInKey(param);
					} else {
						mSiteMapper.updateInKey(inKey);
					}
				}
			} else {
				if (ms.isLogin()) {
					param.put("id", ms.getId());
				}
				
				param.put("engine", StringUtil.getSearchEngine(referer));
				param.put("outkey", StringUtil.getSearchKeyword(referer));
				param.put("browser", StringUtil.getBrowser(userAgent));
				
				mSiteMapper.insertReferer(param);
				if (sSiteMapper.countReferer(param) > 50000) {
					Map<String, Object> minRef = sSiteMapper.getMinReferer(param);
					mSiteMapper.deleteReferers(minRef);
				}
				if (!StringUtil.isEmpty(param.get("outkey"))) {
					Map<String, Object> outKey = sSiteMapper.getOutKey(param);
					if (outKey == null) {
						mSiteMapper.insertOutKey(param);
					} else {
						outKey.put("engine", param.get("engine"));
						mSiteMapper.updateOutKey(outKey);
					}
				}
				Map<String, Object> browser = sSiteMapper.getBrowser(param);
				if (browser == null) {
					mSiteMapper.insertBrowser(param);
				} else {
					mSiteMapper.updateBrowser(browser);
				}
				Map<String, Object> today = sSiteMapper.getToday(param);
				if (today == null) {
					mSiteMapper.insertToday(param);
				} else {
					mSiteMapper.updateToday(today);
				}
				Map<String, Object> info = sSiteMapper.getInfo(param);
				if (info == null) {
					mSiteMapper.insertInfo(param);
				} else {
					mSiteMapper.updateInfo(info);
				}

				ss.setLog(ip + "-" + DateUtil.getDateString());
				ss.setAgent(userAgent);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		
	}
}
