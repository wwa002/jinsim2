package kr.dmon.wb.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterAdminMapper;
import kr.dmon.wb.mapper.master.MasterConferenceMapper;
import kr.dmon.wb.mapper.master.MasterSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveAdminMapper;
import kr.dmon.wb.mapper.slave.SlaveConferenceMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.model.ConferenceRegisterXlsView;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.StringUtil;

@Service
public class ConferenceService extends BaseService {

	public static Logger logger = Logger.getLogger(ConferenceService.class);
	
	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	MasterSiteMapper mSiteMapper;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterAdminMapper mAdminMapper;
	
	@Autowired
	SlaveAdminMapper sAdminMapper;
	
	@Autowired
	MasterConferenceMapper mConferenceMapper;
	
	@Autowired
	SlaveConferenceMapper sConferenceMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	
	public String index(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> html = sSiteMapper.getConference(param);
			if (html == null) {
				return "/error/404";
			} else {
				if ("N".equals(StringUtil.getContent(html.get("status")))) {
					throw new Exception("잘못된 경로로 접근하셨습니다.");
				}
				
				if (StringUtil.isEmpty(html.get("tidx"))) {
					Map<String, Object> mainTemplate = sSiteMapper.getMainTemplate(ss.getCfg());
					if (mainTemplate != null) {
						html.putAll(mainTemplate);
					}
				}
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + html.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/template";
			}
		}
	}
	
	public String session(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			
			Map<String, Object> item = sConferenceMapper.sessionItem(param);
			if (item == null) {
				throw new Exception("세션 정보를 읽어 올 수 없습니다.");
			}
			
			Date startdate = DateUtil.getDateFromString(StringUtil.getContent(item.get("startdate")), "yyyy-MM-dd HH:mm:ss");
			Date enddate = DateUtil.getDateFromString(StringUtil.getContent(item.get("enddate")), "yyyy-MM-dd HH:mm:ss");
			Date nowdate = DateUtil.getDateFromString(StringUtil.getContent(item.get("currentdate")), "yyyy-MM-dd HH:mm:ss");
			if (nowdate.getTime() < startdate.getTime() || nowdate.getTime() > enddate.getTime()) {
				throw new Exception("운영 시간이 아닙니다.");
			}
			model.addAttribute("item", item);
			
			return "/conference/session";
		}
	}
	
	// for API Admin
	public RESTResult apiAdminCfg(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.getConferenceItem(param));
	}
	
	public ModelAndView registerExcel(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		Map<String, Object> map = new HashMap<>();
		map.put("list", sAdminMapper.conferenceRegisterListAll(param));
		return new ModelAndView(new ConferenceRegisterXlsView(), map);
	}
	
	public RESTResult apiAdminRegisterList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.conferenceRegisterListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.conferenceRegisterList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiAdminRegisterItem(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.conferenceRegisterItem(param));
	}
	
	@Transactional
	public RESTResult apiAdminRegisterModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		mAdminMapper.conferenceRegisterModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminRegisterDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		mAdminMapper.conferenceRegisterDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminSessionCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("타이틀을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("date"))) {
			throw new Exception("날짜가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("stime"))) {
			throw new Exception("시작시간이 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("etime"))) {
			throw new Exception("종료시간이 누락되었습니다.");
		}
		
		mAdminMapper.conferenceSessionCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminSessionModify(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("타이틀을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("date"))) {
			throw new Exception("날짜가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("stime"))) {
			throw new Exception("시작시간이 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("etime"))) {
			throw new Exception("종료시간이 누락되었습니다.");
		}
		
		Map<String, Object> item = sAdminMapper.getConferenceSessionItem(param);
		if (item == null) {
			throw new Exception("정보를 가져올 수 없습니다.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") 
					&& !file.getOriginalFilename().toLowerCase().endsWith("png")
					&& !file.getOriginalFilename().toLowerCase().endsWith("gif")
					&& !file.getOriginalFilename().toLowerCase().endsWith("pdf")) {
				throw new Exception("jpg, png, gif, pdf 파일만 업로드가 가능합니다.");
			}

			if (!StringUtil.isEmpty(item.get("path"))) {
				resourceService.delete(StringUtil.getContent(item.get("path")));
			}
			
			Map<String, Object> map = resourceService.upload(request, "conference", file);
			param.put("filename", file.getOriginalFilename());
			param.put("path", map.get("path"));
		}
		
		mAdminMapper.conferenceSessionModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminSessionDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		Map<String, Object> item = sAdminMapper.getConferenceSessionItem(param);
		if (item == null) {
			throw new Exception("정보를 가져올 수 없습니다.");
		}
		if (!StringUtil.isEmpty(item.get("path"))) {
			resourceService.delete(StringUtil.getContent(item.get("path")));
		}
		
		mAdminMapper.conferenceSessionDelete(param);
		mAdminMapper.conferenceSessionVoteDelete(param);
		mAdminMapper.conferenceSessionVoteResultDelete(param);
		mAdminMapper.conferenceSessionQuestionDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminSessionPage(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			throw new Exception("페이지 번호가 누락되었습니다.");
		}
		
		mAdminMapper.conferenceSessionPage(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminSessionQuestion(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.conferenceQuestionList(param));
	}
	
	public RESTResult apiAdminSessionQuestionPrincipal(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.conferenceQuestionListPrincipal(param));
	}
	
	public RESTResult apiAdminSessionQuestionMonitor(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.conferenceQuestionListMonitor(param));
	}
	
	@Transactional
	public RESTResult apiAdminSessionQuestionDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		mAdminMapper.conferenceQuestionDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminSessionQuestionShow(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		mAdminMapper.conferenceQuestionShow(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminSessionQuestionDisplay(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		mAdminMapper.conferenceQuestionDisplay(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminVoteAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		mAdminMapper.conferenceVoteAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminVoteList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.conferenceSessionVoteList(param));
	}
	
	@Transactional
	public RESTResult apiAdminVoteModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}

		mAdminMapper.conferenceVoteModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminVoteDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		mAdminMapper.conferenceVoteDelete(param);
		mAdminMapper.conferenceVoteResultDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAdminVoteRun(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		mAdminMapper.conferenceVoteRun(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAdminVoteResult(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("강연 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("item", sAdminMapper.conferenceSessionVoteItem(param));
		ret.put("result", sAdminMapper.conferenceSessionVoteResult(param));
		ret.put("list", sAdminMapper.conferenceSessionVoteResultList(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	// for API
	@Transactional
	public RESTResult apiRegister(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보 고유값이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("phone"))) {
			throw new Exception("연락처를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일을 입력해 주세요.");
		}
		
		if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
			throw new Exception("올바른 형태의 이메일을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("uncaptcha"))) {
			if (!StringUtil.getContent(param.get("captcha")).equals(request.getSession().getAttribute("captchaAnswer"))) {
				throw new Exception("스팸방지코드를 확인해 주세요.");
			}
		}
		
		if (sConferenceMapper.checkRegister(param) > 0) {
			throw new Exception("이미 등록되어 있는 메일 주소 입니다.");
		}
		
		mConferenceMapper.register(param);
		
		param.put("code", "registration_"+StringUtil.getContent(param.get("code")));
		Map<String, Object> mailTemplate = sAPIMapper.getMailTemplate(param);
		if (mailTemplate != null) {
			mailTemplate.put("content", replaceTemplate(StringUtil.getContent(mailTemplate.get("content")), param));
			resourceService.sendMailHtml(new String[]{StringUtil.getContent(param.get("email"))}, ss.getEmail(), StringUtil.getContent(mailTemplate.get("title")), StringUtil.getContent(mailTemplate.get("content")));
		}

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiRegisterCheck(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보 고유값이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일을 입력해 주세요.");
		}
		
		if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
			throw new Exception("올바른 형태의 이메일을 입력해 주세요.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sConferenceMapper.checkRegisterItem(param));
	}
	
	public RESTResult apiSessionList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보 고유값이 누락되었습니다.");
		}
		
		List<Map<String, Object>> date = sConferenceMapper.sesionDates(param);
		for (Map<String, Object> dt : date) {
			param.put("date", dt.get("date"));
			List<Map<String, Object>> place = sConferenceMapper.sessionPlaces(param);
			for (Map<String, Object> p : place) {
				param.put("place", p.get("place"));
				p.put("session", sConferenceMapper.sessions(param));
			}
			dt.put("place", place);
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, date);
	}
	
	public RESTResult apiSessionLecture(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보 고유값이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유정보가 누락되었습니다.");
		}
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("lecture", sConferenceMapper.sessionItem(param));
		ret.put("vote", sConferenceMapper.voteItems(param));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiSessionVote(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보 고유값이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("세션 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("투표 고유정보가 누락되었습니다.");
		}
		
		mConferenceMapper.voteResultAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSessionQuestion(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보 고유값이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("question"))) {
			throw new Exception("질문을 입력해 주세요.");
		}
		
		mConferenceMapper.questionAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSessionItem(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("행사정보 고유값이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("세션 고유정보가 누락되었습니다.");
		}
		Map<String, Object> map = sConferenceMapper.sessionItem(param);
		if (StringUtil.isEmpty(map.get("filename"))) {
			map.put("filetype", "N");
		} else if (StringUtil.getContent(map.get("filename")).toLowerCase().endsWith("pdf")) {
			map.put("filetype", "pdf");
		} else {
			map.put("filetype", "image");
		}
		map.put("path_api", "/api/conference/session/download/"+param.get("code")+"/"+param.get("idx")+"/"+map.get("filename"));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, map);
	}
}
