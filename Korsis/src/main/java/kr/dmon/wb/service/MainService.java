package kr.dmon.wb.service;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import kr.dmon.wb.exceptions.RequireLoginException;
import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveBoardMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.session.BoardSession;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.StringUtil;

@Service
public class MainService extends BaseService {

	private static Logger logger = Logger.getLogger(MainService.class);
	
	@Autowired
	MasterSiteMapper mSiteMapper;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	SlaveBoardMapper sBoardMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	public String index(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			Map<String, Object> html = sSiteMapper.getMainContent(ss.getCfg());
			if (html == null) {
				return "/error/404";
			} else {
				if (!ss.isUsed()) {
					return "/error/stop";
				}
				if (StringUtil.isEmpty(html.get("tidx"))) {
					Map<String, Object> mainTemplate = sSiteMapper.getMainTemplate(ss.getCfg());
					if (mainTemplate != null) {
						html.putAll(mainTemplate);
					}
				}
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/template";
			}
		}
	}
	
	public ResponseEntity<byte[]> robots(HttpServletRequest request, HttpServletResponse response) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("IS Builder Site");
		} 
		
		String robot = sSiteMapper.getRobots(ss.getSitecd());
		if (StringUtil.isEmpty(robot)) {
			robot = "User-agent: *\nAllow: /";
		}
		
		byte[] bytes = robot.getBytes();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		headers.setContentType(MediaType.TEXT_PLAIN);
		response.setContentType(MediaType.TEXT_PLAIN_VALUE);
		return new ResponseEntity<>(bytes, headers, HttpStatus.NOT_FOUND);
	}
	
	public String content(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> html = sSiteMapper.getContent(param);
			if (html == null) {
				return "/error/404";
			} else {
				MemberSession ms = getMemberSession(request);
				if ("Y".equals(html.get("ismember"))) {
					if (!ms.isLogin()) {
						throw new RequireLoginException("로그인 후 이용해 주세요.");
					}
				}
				if (!StringUtil.isEmpty(html.get("gcds"))) {
					if (!ms.isAdmin()) {
						if (!ms.isLogin()) {
							throw new RequireLoginException("로그인 후 이용해 주세요.");
						}
						
						String[] pers = StringUtil.getContent(html.get("gcds")).split(",");
						boolean isOk = false;
						for (String p : pers) {
							if (ms.isPermitGcd(p)) {
								isOk = true;
								break;
							}
						}
						if (!isOk) {
							throw new Exception("열람할 수 있는 권한이 없습니다.");
						}
					}
				}
				
				if (StringUtil.isEmpty(html.get("tidx"))) {
					Map<String, Object> mainTemplate = sSiteMapper.getMainTemplate(ss.getCfg());
					if (mainTemplate != null) {
						html.putAll(mainTemplate);
					}
				}
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + html.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/template";
			}
		}
	}
	
	public ResponseEntity<byte[]> etcResource(HttpServletRequest request, HttpServletResponse response, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		Map<String, Object> item = sSiteMapper.getResourceEtc(param);
		if (item == null) {
			throw new Exception("정보를 조회 할 수 없습니다.");
		}
		
		String content = StringUtil.getContent(item.get("content"));
		byte[] bytes = content.getBytes(Charset.defaultCharset());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentLength(bytes.length);
		
		if ("JS".equals(item.get("type"))) {
			response.setContentType("application/javascript");
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if ("CSS".equals(item.get("type"))) {
			response.setContentType("text/css");
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else if ("XML".equals(item.get("type"))) {
			headers.setContentType(MediaType.TEXT_XML);
			response.setContentType(MediaType.TEXT_XML_VALUE);
			return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
		} else {
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	public String boardList(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
			if (cfg == null) {
				throw new Exception("게시판 정보를 읽어올 수 없습니다.");
			}
			
			MemberSession ms = getMemberSession(request);
			if (!ms.isAdmin()) {
				// 회원전용검증
				if ("Y".equals(cfg.get("permit_list_member"))) {
					if (!ms.isLogin()) {
						throw new RequireLoginException("로그인 후 이용해 주세요.");
					}
				}
				if (!StringUtil.isEmpty(cfg.get("permit_list"))) {
					if (!ms.isLogin()) {
						throw new RequireLoginException("로그인 후 이용해 주세요.");
					}
					
					String[] pers = StringUtil.getContent(cfg.get("permit_list")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("목록 권한이 없습니다.");
					}
				}
			}
			
			Map<String, Object> html = sSiteMapper.getTemplate(cfg);
			if (html == null) {
				return "/error/404";
			} else {
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + cfg.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), "board-title", StringUtil.getContent(cfg.get("title"))));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/board/list";
			}
		}
	}
	public String boardView(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
			if (cfg == null) {
				throw new Exception("게시판 정보를 읽어올 수 없습니다.");
			}
			
			MemberSession ms = getMemberSession(request);
			if (!ms.isAdmin()) {
				if ("Y".equals(cfg.get("permit_view_member"))) {
					if (!ms.isLogin()) {
						throw new RequireLoginException("로그인 후 이용해 주세요.");
					}
				}
				
				Map<String, Object> article = sBoardMapper.viewForPermission(param);
				if ("Y".equals(StringUtil.getContent(article.get("hidden")))) {
					// 비밀 게시물
					BoardSession bs = (BoardSession) request.getSession().getAttribute(BoardSession.getSessionKey(article));
					if (bs == null) {
						// 원본 게시물 조회
						Map<String, Object> first = sBoardMapper.viewForFirstHidden(article);
						if (first == null) {
							throw new Exception("원글이 삭제되어 조회 할 수 없는 게시물 입니다.");
						}
						
						int midx = Integer.parseInt(StringUtil.getContent(first.get("midx"), "0"));
						if (midx == 0) {
							// 비회원의 게시물은 인증 페이지로 이동
							return "redirect:/board/"+StringUtil.getContent(param.get("code"))+"/"+StringUtil.getContent(param.get("idx"))+"/cert";
						} else if (midx != 0 && midx == ms.getIdx()) {
							// 본인의 게시물은 패스
						} else {
							throw new Exception("비밀게시물은 본인과 관리자만 접근 할 수 있습니다.");
						}
					}
				} else {
					// 회원전용검증
					if (!StringUtil.isEmpty(cfg.get("permit_view"))) {
						if (!ms.isLogin()) {
							throw new RequireLoginException("로그인 후 이용해 주세요.");
						}
						
						String[] pers = StringUtil.getContent(cfg.get("permit_view")).split(",");
						boolean isOk = false;
						for (String p : pers) {
							if (ms.isPermitGcd(p)) {
								isOk = true;
								break;
							}
						}
						if (!isOk) {
							throw new Exception("열람 권한이 없습니다.");
						}
					}
				}
			}
			
			Map<String, Object> html = sSiteMapper.getTemplate(cfg);
			if (html == null) {
				return "/error/404";
			} else {
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + cfg.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), "board-title", StringUtil.getContent(cfg.get("title"))));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/board/view";
			}
		}
	}
	public String boardWrite(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
			if (cfg == null) {
				throw new Exception("게시판 정보를 읽어올 수 없습니다.");
			}

			// 회원전용검증
			MemberSession ms = getMemberSession(request);
			if ("Y".equals(cfg.get("permit_write_member"))) {
				if (!ms.isLogin()) {
					throw new RequireLoginException("로그인 후 이용해 주세요.");
				}
			}
			
			if (!StringUtil.isEmpty(cfg.get("permit_write"))) {
				if (!ms.isLogin()) {
					throw new RequireLoginException("로그인 후 이용해 주세요.");
				}
				
				if (!ms.isAdmin()) {
					String[] pers = StringUtil.getContent(cfg.get("permit_write")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("작성 권한이 없습니다.");
					}
				}
			}
			
			Map<String, Object> html = sSiteMapper.getTemplate(cfg);
			if (html == null) {
				return "/error/404";
			} else {
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + cfg.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), "board-title", StringUtil.getContent(cfg.get("title"))));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/board/write";
			}
		}
	}
	public String boardModify(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
			if (cfg == null) {
				throw new Exception("게시판 정보를 읽어올 수 없습니다.");
			}
			
			MemberSession ms = getMemberSession(request);
			if ("Y".equals(cfg.get("permit_write_member"))) {
				if (!ms.isLogin()) {
					throw new RequireLoginException("로그인 후 이용해 주세요.");
				}
			}
			
			Map<String, Object> html = sSiteMapper.getTemplate(cfg);
			if (html == null) {
				return "/error/404";
			} else {
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + cfg.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), "board-title", StringUtil.getContent(cfg.get("title"))));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/board/modify";
			}
		}
	}
	public String boardReply(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
			if (cfg == null) {
				throw new Exception("게시판 정보를 읽어올 수 없습니다.");
			}
			
			// 회원전용검증
			MemberSession ms = getMemberSession(request);
			if ("Y".equals(cfg.get("permit_reply_member"))) {
				if (!ms.isLogin()) {
					throw new RequireLoginException("로그인 후 이용해 주세요.");
				}
			}
			
			if (!StringUtil.isEmpty(cfg.get("permit_reply"))) {
				if (!ms.isLogin()) {
					throw new RequireLoginException("로그인 후 이용해 주세요.");
				}
				
				if (!ms.isAdmin()) {
					String[] pers = StringUtil.getContent(cfg.get("permit_reply")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("답변 권한이 없습니다.");
					}
				}
			}
			
			Map<String, Object> html = sSiteMapper.getTemplate(cfg);
			if (html == null) {
				return "/error/404";
			} else {
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + cfg.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), "board-title", StringUtil.getContent(cfg.get("title"))));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/board/reply";
			}
		}
	}
	public String boardCert(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		} else {
			param.put("sitecd", ss.getSitecd());
			Map<String, Object> cfg = sBoardMapper.getBoardCfg(param);
			if (cfg == null) {
				throw new Exception("게시판 정보를 읽어올 수 없습니다.");
			}
			
			MemberSession ms = getMemberSession(request);
			if (!ms.isAdmin()) {
				// 회원전용검증
				if ("Y".equals(cfg.get("permit_view_member"))) {
					if (!ms.isLogin()) {
						throw new RequireLoginException("로그인 후 이용해 주세요.");
					}
				}
				
				if (!StringUtil.isEmpty(cfg.get("permit_view"))) {
					if (!ms.isLogin()) {
						throw new RequireLoginException("로그인 후 이용해 주세요.");
					}
					
					String[] pers = StringUtil.getContent(cfg.get("permit_view")).split(",");
					boolean isOk = false;
					for (String p : pers) {
						if (ms.isPermitGcd(p)) {
							isOk = true;
							break;
						}
					}
					if (!isOk) {
						throw new Exception("열람 권한이 없습니다.");
					}
				}
			}
			
			Map<String, Object> html = sSiteMapper.getTemplate(cfg);
			if (html == null) {
				return "/error/404";
			} else {
				ss.getCfg().put("title", StringUtil.getContent("[" + StringUtil.getContent(ss.getTitle()) + "] " + cfg.get("title")));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), ss.getCfg()));
				html.put("header", replaceTemplate(StringUtil.getContent(html.get("header")), "board-title", StringUtil.getContent(cfg.get("title"))));
				html.put("footer", replaceTemplate(StringUtil.getContent(html.get("footer")), ss.getCfg()));
				model.addAttribute("html", html);
				return "/board/cert";
			}
		}
	}
	
	public String refuseSms(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		}
		model.addAttribute("title", ss.getTitle());
		model.addAttribute("cfg", ss.getCfg());
		return "/refuse/sms";
	}
	
	public String refuseEmail(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			return "redirect:/builder/login";
		}
		model.addAttribute("title", ss.getTitle());
		model.addAttribute("cfg", ss.getCfg());
		return "/refuse/email";
	}
}
