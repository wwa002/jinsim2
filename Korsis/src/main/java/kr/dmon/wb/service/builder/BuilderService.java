package kr.dmon.wb.service.builder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterBuilderMapper;
import kr.dmon.wb.mapper.master.MasterScheduleMapper;
import kr.dmon.wb.mapper.master.MasterSocietyMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveBuilderMapper;
import kr.dmon.wb.mapper.slave.SlaveCommonMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveSocietyMapper;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.BaseService;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.StringUtil;

@Service
public class BuilderService extends BaseService {

	private static Logger logger = Logger.getLogger(BuilderService.class);
	
	@Value("${builder.default.num_per_page}")
	private int numPerPage;
	
	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;
	
	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;
	
	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	MasterBuilderMapper mBuilderMapper;
	
	@Autowired
	SlaveBuilderMapper sBuilderMapper;
	
	@Autowired
	MasterSocietyMapper mSocietyMapper;
	
	@Autowired
	SlaveSocietyMapper sSocietyMapper;
	
	@Autowired
	MasterScheduleMapper mScheduleMapper;
	
	@Autowired
	SlaveCommonMapper sCommonMapper;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;

	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	public void loadBuilder(HttpServletRequest request, Model model) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (ms == null || !ms.isBuilder()) {
			throw new Exception("빌더회원이 아닙니다.");
		}
		
		model.addAttribute("ms", ms);
		model.addAttribute("builder", sBuilderMapper.getBuilderInfo(ms.getIdx()));
		model.addAttribute("sites", sBuilderMapper.getSites());
		
		String sitecd = ms.getSitecd();
		if (!StringUtil.isEmpty(sitecd)) {
			model.addAttribute("site", sBuilderMapper.systemSite(sitecd));
			
			Map<String, Object> siteCfg = sBuilderMapper.systemSiteCfg(sitecd);
			model.addAttribute("siteCfg", siteCfg);
			if ("Y".equals(siteCfg.get("society"))) {
				model.addAttribute("societyCfg", sBuilderMapper.systemSocietyCfg(sitecd));
			}
		}
	}
	
	// for Web
	public String login(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		if (!getSiteSession(request).isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		return "/builder/login";
	}
	
	public void dashboard(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		model.addAttribute("stats", sBuilderMapper.dashboard());
		model.addAttribute("counter", sBuilderMapper.dashboardCounter());
	}
	
	public void systemSetting(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		model.addAttribute("smsCfg", sBuilderMapper.systemSMSCfg(getMemberSession(request).getSitecd()));
		model.addAttribute("appCfg", sBuilderMapper.systemAppCfg(getMemberSession(request).getSitecd()));
	}
	
	public void systemRobot(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
	}
	
	public void systemMemberList(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		model.addAttribute("grades", sBuilderMapper.systemMemberGradeListAll(param));
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sBuilderMapper.systemMemberListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sBuilderMapper.systemMemberList(param));
	}
	
	public void systemMemberGrade(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sBuilderMapper.systemMemberGradeListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sBuilderMapper.systemMemberGradeList(param));
	}
	
	public void resources(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
	}
	
	public void templates(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sBuilderMapper.templateListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sBuilderMapper.templateList(param));
	}
	
	public void templateEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		model.addAttribute("item", sBuilderMapper.templateItem(param));
	}
	
	public void boards(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sBuilderMapper.boardListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sBuilderMapper.boardList(param));
	}
	
	public void boardEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		model.addAttribute("templates", sBuilderMapper.templateListAll(param));
		model.addAttribute("grades", sBuilderMapper.systemMemberGradeListAll(param));
		model.addAttribute("item", sBuilderMapper.boardItem(param));
	}
	
	public void workshop(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sSocietyMapper.workshopCfgListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sSocietyMapper.workshopCfgList(param));
	}
	
	public void workshopEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		model.addAttribute("item", sSocietyMapper.workshopCfgItem(param));
	}
	
	public void contents(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sBuilderMapper.contentListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sBuilderMapper.contentList(param));
	}
	
	public void contentsEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		model.addAttribute("templates", sBuilderMapper.templateListAll(param));
		model.addAttribute("grades", sBuilderMapper.systemMemberGradeListAll(param));
		model.addAttribute("item", sBuilderMapper.contentItem(param));
	}
	
	public void etc(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sBuilderMapper.etcListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sBuilderMapper.etcList(param));
	}
	
	public void etcEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());

		model.addAttribute("item", sBuilderMapper.etcItem(param));
	}
	
	public void conference(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		model.addAttribute("templates", sBuilderMapper.templateListAll(param));
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
			model.addAttribute("page", 1);
		} else {
			model.addAttribute("page", param.get("page"));
		}
		
		int totalCount = sBuilderMapper.conferenceListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		
		param.put("start", (paging.getPageNo()-1) * numPerPage);
		param.put("pageSize", numPerPage);
		model.addAttribute("paging", paging);
		model.addAttribute("list", sBuilderMapper.conferenceList(param));
	}
	
	public void conferenceEdit(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadBuilder(request, model);
		
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		model.addAttribute("templates", sBuilderMapper.templateListAll(param));
		model.addAttribute("item", sBuilderMapper.conferenceItem(param));
	}

	// for API
	public RESTResult apiLogin(HttpServletRequest request, Map<String, Object> param) throws Exception {
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("passwd"))) {
			throw new Exception("비밀번호를 입력해 주세요.");
		}
		
		Map<String, Object> mem = sBuilderMapper.login(param);
		if (mem == null) {
			throw new Exception("아이디와 비밀번호를 확인해 주세요.");
		}
		
		MemberSession ms = getMemberSession(request);
		ms.setData(mem);
		mBuilderMapper.logined(mem);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	public RESTResult apiCheckSiteCd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("sitecd"))) {
			throw new Exception("사이트 코드를 입력해 주세요.");
		}
		
		if (sBuilderMapper.checkSiteCd(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 사이트코드 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiCheckGradeCd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("gcd"))) {
			throw new Exception("등급코드를 입력해 주세요.");
		}
		
		if (sBuilderMapper.checkGradeCd(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 등급코드 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiCheckId(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		
		if (sBuilderMapper.checkId(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 아이디 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiCheckBoard(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("게시판 코드를 입력해 주세요.");
		}
		
		if (sBuilderMapper.checkBoard(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 게시판 코드 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiCheckConference(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("학술대회 코드를 입력해 주세요.");
		}
		
		if (sBuilderMapper.checkConference(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 학술대회 코드 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiSiteSelection(HttpServletRequest request, Map<String, Object> param) throws Exception {
		getMemberSession(request).setSitecd(StringUtil.getContent(param.get("sitecd")));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}

	@Transactional
	public RESTResult apiSiteCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("sitecd"))) {
			throw new Exception("사이트 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("사이트명을 입력해 주세요.");
		}
		
		param.put("sitecd", param.get("sitecd").toString().toUpperCase());
		
		if (sBuilderMapper.checkSiteCd(param) == 0) {
			mBuilderMapper.siteCreate(param);
			mBuilderMapper.siteCreateConfig(param);
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 사이트코드 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiSiteList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sBuilderMapper.siteListAll(param));
	}
	
	public RESTResult apiSiteModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("사이트명을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("isused"))) {
			param.put("isused", "N");
		}
		
		mBuilderMapper.siteModify(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	public RESTResult apiServerList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sBuilderMapper.serverListAll(param));
	}
	
	public RESTResult apiServerMain(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("host"))) {
			throw new Exception("서버 호스트가 누락되었습니다.");
		}

		mScheduleMapper.electMainServer(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for System
	public RESTResult apiSystemURLList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sBuilderMapper.systemUrlList(param));
	}
	
	public RESTResult apiSystemURLAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("url"))) {
			throw new Exception("URL을 입력해 주세요.");
		}
		
		if (sBuilderMapper.systemUrlCount(param) > 0) {
			throw new Exception("이미 사용중인 URL 입니다.");
		}
		
		mBuilderMapper.systemUrlAdd(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemURLModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("url"))) {
			throw new Exception("URL을 입력해 주세요.");
		}
		
		if (sBuilderMapper.systemUrlCount(param) > 0) {
			throw new Exception("이미 사용중인 URL 입니다.");
		}
		
		mBuilderMapper.systemUrlModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemURLDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.systemUrlDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemURLMain(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.systemUrlMain(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSystemCfg(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cfg"))) {
			throw new Exception("항목이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("status"))) {
			param.put("status", "N");
		}
		
		mBuilderMapper.systemCfg(param);
		if ("app".equals(param.get("cfg")) && "Y".equals(param.get("status"))) {
			mBuilderMapper.systemAddCfgApp(param);
		}
		if ("society".equals(param.get("cfg")) && "Y".equals(param.get("status"))) {
			mBuilderMapper.systemAddCfgSociety(param);
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSystemCfgApp(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cfg"))) {
			throw new Exception("항목이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("status"))) {
			param.put("status", "N");
		}
		
		mBuilderMapper.systemCfgApp(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSystemCfgSociety(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("cfg"))) {
			throw new Exception("항목이 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("status"))) {
			param.put("status", "N");
		}
		
		mBuilderMapper.systemCfgSociety(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemSmsCfg(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		mBuilderMapper.systemSmsCfg(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemAppCfg(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		mBuilderMapper.systemAppCfg(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSystemRobot(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		mBuilderMapper.systemRobot(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for Member
	@Transactional
	public RESTResult apiSystemMemberSave(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		Map<String, Object> site = sBuilderMapper.systemSite(ms.getSitecd());
		
		param.put("name", StringUtil.getContent(param.get("name_last"))+StringUtil.getContent(param.get("name_first")));
		param.put("birth", StringUtil.getContent(param.get("birth")).replaceAll("-", ""));
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			// 가입
			if (StringUtil.isEmpty(param.get("id"))) {
				throw new Exception("아이디를 입력해 주세요.");
			}
			
			if (sBuilderMapper.checkId(param) > 0) {
				throw new Exception("이미 사용중인 아이디 입니다.");
			}
			
			if (file != null && !file.isEmpty()) {
				param.putAll(resourceService.upload(request, "profile", file));
			}
			
			int newIdx = sBuilderMapper.systemMemberNewIdx(param);
			param.put("idx", newIdx);
			param.put("memno", ms.getSitecd()+String.format("%04d", newIdx));
			mBuilderMapper.systemMemberJoin(param);
		} else {
			// 수정
			if (StringUtil.isEmpty(param.get("idx"))) {
				throw new Exception("고유정보가 누락되었습니다.");
			}
			
			Map<String, Object> item = sBuilderMapper.systemMemberItemForIdx(param);
			if (item == null) {
				throw new Exception("회원정보를 읽어 올 수 없습니다.");
			}
			if (file != null && !file.isEmpty()) {
				if (!StringUtil.isEmpty(item.get("photo"))) {
					resourceService.delete(item.get("photo").toString());
				}
				param.putAll(resourceService.upload(request, "profile", file));
			}
			mBuilderMapper.systemMemberModify(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSystemMemberDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		Map<String, Object> site = sBuilderMapper.systemSite(ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		Map<String, Object> item = sBuilderMapper.systemMemberItemForIdx(param);
		if (item == null) {
			throw new Exception("회원정보를 읽어 올 수 없습니다.");
		}
		if (!StringUtil.isEmpty(item.get("photo"))) {
			resourceService.delete(item.get("photo").toString());
		}

		mBuilderMapper.systemMemberDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemMemberGradeAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("gcd"))) {
			throw new Exception("등급코드가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("등급명이 누락되었습니다.");
		}
		
		if (sBuilderMapper.checkGradeCd(param) > 0) {
			throw new Exception("이미 사용중인 등급명 입니다.");
		}
		
		param.put("gcd", param.get("gcd").toString().toUpperCase());
		
		mBuilderMapper.systemMemberGradeAdd(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemMemberGradeModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("gcd"))) {
			throw new Exception("등급코드가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("등급명이 누락되었습니다.");
		}
		
		mBuilderMapper.systemMemberGradeModify(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSystemMemberGradeDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("gcd"))) {
			throw new Exception("등급코드가 누락되었습니다.");
		}
		
		mBuilderMapper.systemMemberGradeDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for Template
	public RESTResult apiTemplateCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("템플릿 제목을 입력해 주세요.");
		}
		
		mBuilderMapper.templateCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiTemplateCopy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.templateCopy(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiTemplateModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("템플릿 제목을 입력해 주세요.");
		}
		
		mBuilderMapper.templateModify(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiTemplateDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.templateDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiTemplateMain(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.templateMain(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	// for Content
	public RESTResult apiContentCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("콘텐츠 제목을 입력해 주세요.");
		}
		
		mBuilderMapper.contentCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiContentCopy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.contentCopy(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiContentModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("템플릿 제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			param.put("tidx", null);
		}
		
		param.put("ismember", "Y".equals(param.get("ismember")) ? "Y" : "N");
		mBuilderMapper.contentModify(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiContentDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.contentDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiContentMain(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.contentMain(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for Etc
	public RESTResult apiEtcCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("콘텐츠 제목을 입력해 주세요.");
		}
		
		mBuilderMapper.etcCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEtcCopy(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.etcCopy(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEtcModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("템플릿 제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			param.put("tidx", null);
		}
		
		param.put("ismember", "Y".equals(param.get("ismember")) ? "Y" : "N");
		mBuilderMapper.etcModify(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiEtcDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mBuilderMapper.etcDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for Board
	public RESTResult apiBoardCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("게시판 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("게시판 이름을 입력해 주세요.");
		}

		param.put("code", param.get("code").toString().toLowerCase());
		
		if (sBuilderMapper.checkBoard(param) > 0) {
			throw new Exception("이미 사용중인 게시판 코드 입니다.");
		}
		
		mBuilderMapper.boardCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiBoardModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("게시판 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("게시판 제목을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			param.put("tidx", null);
		}
		
		param.put("permit_list_member", "Y".equals(param.get("permit_list_member")) ? "Y" : "N");
		param.put("permit_view_member", "Y".equals(param.get("permit_view_member")) ? "Y" : "N");
		param.put("permit_write_member", "Y".equals(param.get("permit_write_member")) ? "Y" : "N");
		param.put("permit_reply_member", "Y".equals(param.get("permit_reply_member")) ? "Y" : "N");
		param.put("permit_comment_member", "Y".equals(param.get("permit_comment_member")) ? "Y" : "N");
		
		mBuilderMapper.boardModify(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiBoardDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("게시판 코드를 입력해 주세요.");
		}

		List<Map<String, Object>> list = sBuilderMapper.boardFileForDelete(param);
		for (Map<String, Object> m : list) {
			try {
				resourceService.delete(m.get("path").toString());
			} catch (Exception e) {
				logger.error(e);
			}
		}
		
		mBuilderMapper.boardFileDelete(param);
		mBuilderMapper.boardReadDelete(param);
		mBuilderMapper.boardCommentDelete(param);
		mBuilderMapper.boardDelete(param);
		mBuilderMapper.boardCfgDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	// for Conference
	public RESTResult apiConferenceCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("학술대회 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("학술대회 이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			throw new Exception("템플릿을 선택해 주세요.");
		}

		param.put("code", param.get("code").toString().toLowerCase());
		
		if (sBuilderMapper.checkConference(param) > 0) {
			throw new Exception("이미 사용중인 게시판 코드 입니다.");
		}
		
		mBuilderMapper.conferenceCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiConferenceModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("학술대회 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("학술대회 이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			throw new Exception("템플릿을 선택해 주세요.");
		}

		mBuilderMapper.conferenceModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	// for Workshop
	public RESTResult apiCheckWorkshop(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("워크샵 코드를 입력해 주세요.");
		}
		
		if (sSocietyMapper.checkWorkshop(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 워크샵 코드 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiSocietyWorkshopCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("워크샵 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("워크샵 이름을 입력해 주세요.");
		}

		param.put("code", param.get("code").toString().toLowerCase());
		
		if (sSocietyMapper.checkWorkshop(param) > 0) {
			throw new Exception("이미 사용중인 게시판 코드 입니다.");
		}
		
		mSocietyMapper.workshopCfgCreate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSocietyWorkshopModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("워크샵 코드를 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("워크샵 제목을 입력해 주세요.");
		}
		
		mSocietyMapper.workshopCfgModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSocietyWorkshopDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("워크샵 코드를 입력해 주세요.");
		}

		List<Map<String, Object>> list = sSocietyMapper.workshopFileForDelete(param);
		for (Map<String, Object> m : list) {
			try {
				resourceService.delete(m.get("path").toString());
			} catch (Exception e) {
				logger.error(e);
			}
		}
		
		mSocietyMapper.workshopFileDelete(param);
		mSocietyMapper.workshopRegistrationDelete(param);
		mSocietyMapper.workshopDelete(param);
		mSocietyMapper.workshopCfgDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
}
