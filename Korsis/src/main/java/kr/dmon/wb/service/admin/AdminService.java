package kr.dmon.wb.service.admin;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterAdminMapper;
import kr.dmon.wb.mapper.master.MasterCommonMapper;
import kr.dmon.wb.mapper.master.MasterDBMapper;
import kr.dmon.wb.mapper.master.MasterSocietyMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveAdminMapper;
import kr.dmon.wb.mapper.slave.SlaveBoardMapper;
import kr.dmon.wb.mapper.slave.SlaveCommonMapper;
import kr.dmon.wb.mapper.slave.SlaveDBMapper;
import kr.dmon.wb.mapper.slave.SlaveEventMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveSocietyMapper;
import kr.dmon.wb.model.Paging;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.BaseService;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.EncryptUtils;
import kr.dmon.wb.utils.StringUtil;

@Service
public class AdminService extends BaseService {

	private static Logger logger = Logger.getLogger(AdminService.class);

	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	ResourceService resourceService;

	@Autowired
	MasterAdminMapper mAdminMapper;
	
	@Autowired
	SlaveAdminMapper sAdminMapper;
	
	@Autowired
	MasterCommonMapper mCommonMapper;
	
	@Autowired
	SlaveCommonMapper sCommonMapper;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	SlaveBoardMapper sBoardMapper;
	
	@Autowired
	MasterDBMapper mDBMapper;
	
	@Autowired
	SlaveDBMapper sDBMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterSocietyMapper mSocietyMapper;
	
	@Autowired
	SlaveSocietyMapper sSocietyMapper;
	
	@Autowired
	SlaveEventMapper sEventMapper;
	
	@Autowired
	RestTemplate restTemplete;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	public Map<String, Object> loadAdmin(HttpServletRequest request, Model model) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (ms == null || !ms.isAdmin()) {
			throw new Exception("관리자가 아닙니다.");
		}
		
		model.addAttribute("ms", ms);
		
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		model.addAttribute("siteInfo", ss.getCfg());
		
		Map<String, Object> param = new HashMap<>();
		param.put("idx", ms.getIdx());
		param.put("sitecd", ss.getSitecd());
		
		model.addAttribute("admin", sAdminMapper.getAdminInfo(param));
		if (ss.useConference()) {
			model.addAttribute("menuConference", sAdminMapper.getConferenceList(param));
		}
		model.addAttribute("boards", sAdminMapper.getBoardList(param));
		if (ss.useMedicaldb()) {
			model.addAttribute("menuHospitalSubjects", sDBMapper.menuSubjects(param));
		}
		if (ss.useSociety()) {
			Map<String, Object> societyCfg = sAdminMapper.getSocietyCfg(param);
			model.addAttribute("societyCfg", societyCfg);
			if ("Y".equals(societyCfg.get("workshop"))) {
				model.addAttribute("workshops", sAdminMapper.getWorkshopList(param));				
			}
		}
		
		return ss.getCfg();
	}
	
	// for Web
	public String login(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		SiteSession ss = getSiteSession(request);
		if (ss.isBuilderSite()) {
			throw new Exception("잘못된 경로로 접근하셨습니다.");
		}
		model.addAttribute("cfg", ss.getCfg());
		return "/admin/login";
	}
	
	public void dashboard(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		loadAdmin(request, model);
	}
	
	public RESTResult apiDashboard(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("member", sAdminMapper.dashboardMember(param));
		ret.put("db", sAdminMapper.dashboardDB(param));
		ret.put("counter", sAdminMapper.dashboardCounter(param));
		ret.put("board", sAdminMapper.dashboardBoards(param));
		
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	/* Member Start */
	public void member(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("gcds", sAdminMapper.gradeList(param));
	}
	
	public void memberInfo(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("gcds", sAdminMapper.gradeList(param));
	}
	
	public RESTResult apiCheckId(HttpServletRequest request, Map<String, Object> param) throws Exception {
		MemberSession ms = getMemberSession(request);
		if (StringUtil.isEmpty(ms.getSitecd())) {
			throw new Exception("사이트를 선택해 주세요.");
		}
		param.put("sitecd", ms.getSitecd());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("아이디를 입력해 주세요.");
		}
		
		if (sAdminMapper.checkId(param) == 0) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		} else {
			return new RESTResult(RESTResult.FAILURE, "이미 사용중인 아이디 입니다.", RESTResult.ERROR);
		}
	}
	
	public RESTResult apiMemberList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.memberListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.memberList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiMemberInfo(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		Map<String, Object> map = sAdminMapper.memberInfo(param);
		map.put("terms", sAdminMapper.memberTerms(map));
		if (ss.useSociety()) {
			map.put("society", sAdminMapper.memberInfoSociety(map));
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, map);
		
	}
	
	@Transactional
	public RESTResult apiMemberSave(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		param.put("name", StringUtil.getContent(param.get("name_last"))+StringUtil.getContent(param.get("name_first")));
		param.put("birth", StringUtil.getContent(param.get("birth")).replaceAll("-", ""));
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			// 가입
			if (StringUtil.isEmpty(param.get("id"))) {
				throw new Exception("아이디를 입력해 주세요.");
			}
			
			if (sAdminMapper.checkId(param) > 0) {
				throw new Exception("이미 사용중인 아이디 입니다.");
			}
			
			if (file != null && !file.isEmpty()) {
				param.putAll(resourceService.upload(request, "profile", file));
			}
			
			int newIdx = sAdminMapper.memberNewIdx(param);
			param.put("idx", newIdx);
			param.put("memno", ss.getSitecd()+String.format("%04d", newIdx));
			mAdminMapper.memberJoin(param);
		} else {
			// 수정
			if (StringUtil.isEmpty(param.get("idx"))) {
				throw new Exception("고유정보가 누락되었습니다.");
			}
			
			Map<String, Object> item = sAdminMapper.memberItemForIdx(param);
			if (item == null) {
				throw new Exception("회원정보를 읽어 올 수 없습니다.");
			}
			if (file != null && !file.isEmpty()) {
				if (!StringUtil.isEmpty(item.get("photo"))) {
					resourceService.delete(item.get("photo").toString());
				}
				param.putAll(resourceService.upload(request, "profile", file));
			}
			mAdminMapper.memberModify(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMemberDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		Map<String, Object> item = sAdminMapper.memberItemForIdx(param);
		if (item == null) {
			throw new Exception("회원정보를 읽어 올 수 없습니다.");
		}
		if (!StringUtil.isEmpty(item.get("photo"))) {
			resourceService.delete(item.get("photo").toString());
		}
		
		mAdminMapper.memberDelete(param);
		mAdminMapper.memberDeleteSociety(param);
		mAdminMapper.memberDeleteTerms(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMemberGCDList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("ID가 누락되었습니다.");
		}
		
		Map<String, Object> mem = sAdminMapper.memberItemForId(param);
		if (mem == null) {
			throw new Exception("회원정보를 조회할 수 없습니다.");
		}
		param.put("midx", mem.get("idx"));
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.memberGCDList(param));
	}
	
	@Transactional
	public RESTResult apiMemberGCDAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("ID가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gcd"))) {
			throw new Exception("등급 정보가 누락되었습니다.");
		}
		
		Map<String, Object> mem = sAdminMapper.memberItemForId(param);
		if (mem == null) {
			throw new Exception("회원정보를 조회할 수 없습니다.");
		}
		param.put("midx", mem.get("idx"));
		if (sAdminMapper.memberGCDCheck(param) > 0) {
			throw new Exception("이미 등록된 등급 정보 입니다.");
		}
		
		mAdminMapper.memberGCDAdd(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMemberGCDDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("id"))) {
			throw new Exception("ID가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gcd"))) {
			throw new Exception("등급 정보가 누락되었습니다.");
		}
		
		Map<String, Object> mem = sAdminMapper.memberItemForId(param);
		if (mem == null) {
			throw new Exception("회원정보를 조회할 수 없습니다.");
		}
		param.put("midx", mem.get("idx"));
		if (sAdminMapper.memberGCDCheck(param) < 1) {
			throw new Exception("등급 정보를 확인해 주세요.");
		}
		
		mAdminMapper.memberGCDDelete(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* Member End */
	
	/* APP Start */
	public void appKeyword(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("app"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	public void appPush(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("app"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	public void appNotice(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("app"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public RESTResult apiAppKeywordList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sCommonMapper.keywordListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sCommonMapper.keywordList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiAppKeywordListAll(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sCommonMapper.keywordListAll(param));
	}
	
	@Transactional
	public RESTResult apiAppKeywordCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		if (StringUtil.isEmpty(param.get("value"))) {
			throw new Exception("키워드를 입력해 주세요.");
		}
		mCommonMapper.keywordCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAppKeywordDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		mCommonMapper.keywordDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAppKeywordUp(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> cSort = sCommonMapper.keywordSort(param);
		int sort = Integer.parseInt(cSort.get("sort").toString());
		int mx = Integer.parseInt(cSort.get("mx").toString());
		if (sort <= 1) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		}
		param.put("sort", sort-1);
		mCommonMapper.keywordSortDown(param);
		mCommonMapper.keywordSortUpdate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAppKeywordDown(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> cSort = sCommonMapper.keywordSort(param);
		int sort = Integer.parseInt(cSort.get("sort").toString());
		int mx = Integer.parseInt(cSort.get("mx").toString());
		if (sort >= mx) {
			return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
		}
		param.put("sort", sort+1);
		mCommonMapper.keywordSortUp(param);
		mCommonMapper.keywordSortUpdate(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiAppSystemList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.systemListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.systemList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiAppSystemProc(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("code"))) {
			throw new Exception("종류가 누락되었습니다.");
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("sitecd", ss.getSitecd());
		if ("MAINTENANCE".equals(param.get("code"))) {
			if (StringUtil.isEmpty(param.get("message"))) {
				throw new Exception("메세지가 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("startdt"))) {
				throw new Exception("시작일이 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("starttm"))) {
				throw new Exception("시작시간이 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("enddt"))) {
				throw new Exception("종료일이 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("endtm"))) {
				throw new Exception("종료시간이 누락되었습니다.");
			}
			
			Date start = DateUtil.getDateFromString(param.get("startdt").toString()+param.get("starttm").toString(), "yyyy-MM-ddHH:mm");
			Date end = DateUtil.getDateFromString(param.get("enddt").toString()+param.get("endtm").toString(), "yyyy-MM-ddHH:mm");
			if (start.getTime() > end.getTime()) {
				throw new Exception("종료일이 시작일 보다 앞설 수 없습니다.");
			}
			
			map.put("code", param.get("code"));
			map.put("message", param.get("message"));
			map.put("startdate", DateUtil.getDateString(start, "yyyyMMddHHmmss"));
			map.put("enddate", DateUtil.getDateString(end, "yyyyMMddHHmmss"));
			map.put("status", StringUtil.isEmpty(param.get("status")) ? "N" : param.get("status"));
			
		} else if ("NOTICE".equals(param.get("code"))) {
			if (StringUtil.isEmpty(param.get("os"))) {
				throw new Exception("장비정보가 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("message"))) {
				throw new Exception("메세지가 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("startdt"))) {
				throw new Exception("시작일이 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("starttm"))) {
				throw new Exception("시작시간이 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("enddt"))) {
				throw new Exception("종료일이 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("endtm"))) {
				throw new Exception("종료시간이 누락되었습니다.");
			}
			
			Date start = DateUtil.getDateFromString(param.get("startdt").toString()+param.get("starttm").toString(), "yyyy-MM-ddHH:mm");
			Date end = DateUtil.getDateFromString(param.get("enddt").toString()+param.get("endtm").toString(), "yyyy-MM-ddHH:mm");
			if (start.getTime() > end.getTime()) {
				throw new Exception("종료일이 시작일 보다 앞설 수 없습니다.");
			}
			
			map.put("code", param.get("code"));
			map.put("os", param.get("os"));
			map.put("message", param.get("message"));
			map.put("startdate", DateUtil.getDateString(start, "yyyyMMddHHmmss"));
			map.put("enddate", DateUtil.getDateString(end, "yyyyMMddHHmmss"));
			map.put("status", StringUtil.isEmpty(param.get("status")) ? "N" : param.get("status"));
			
		} else {
			if (StringUtil.isEmpty(param.get("os"))) {
				throw new Exception("장비정보가 누락되었습니다.");
			}
			if (StringUtil.isEmpty(param.get("version"))) {
				throw new Exception("버전정보가 누락되었습니다.");
			}
			
			map.put("code", "VERSION");
			map.put("os", param.get("os"));
			map.put("version", param.get("version"));
			map.put("status", StringUtil.isEmpty(param.get("status")) ? "N" : param.get("status"));
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			mAdminMapper.systemAdd(map);
		} else {
			map.put("idx", param.get("idx"));
			mAdminMapper.systemModify(map);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiAppSystemDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		mAdminMapper.systemDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* APP End */
	
	/* Board Start */
	public void board(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		param.put("sitecd", siteInfo.get("sitecd"));
		
		model.addAttribute("boardCfg", sBoardMapper.getBoardCfg(param));
	}
	/* Board End */
	
	/* Workshop Start */
	public void workshop(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		param.put("sitecd", siteInfo.get("sitecd"));
		
		model.addAttribute("workshopCfg", sSocietyMapper.workshopCfgItem(param));
	}
	/* Workshop End */
	
	/* Board Start */
	public void conference(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		param.put("sitecd", siteInfo.get("sitecd"));
		
		model.addAttribute("conferenceCfg", sAdminMapper.getConferenceItem(param));
		model.addAttribute("mainUrl", sSiteMapper.getMainUrl(StringUtil.getContent(siteInfo.get("sitecd"))));
		if (!StringUtil.isEmpty(param.get("idx"))) {
			model.addAttribute("item", sAdminMapper.getConferenceSessionItem(param));
		}
	}
	/* Board End */
	
	/* Event Start */
	public void eventManager(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("event"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		if (!StringUtil.isEmpty(param.get("idx"))) {
			model.addAttribute("item", sEventMapper.adminItem(param));
		}
	}
	
	public void eventBanner(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("event"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void eventProject(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("event"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		if (!StringUtil.isEmpty(param.get("idx"))) {
			model.addAttribute("item", sEventMapper.adminPlanItem(param));
		}
	}
	
	public void eventNew(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("event"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void eventWeek(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("event"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	/* Event End */
	
	/* Medical DB Start */
	public void mapMedical(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("medicaldb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void hospital(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("medicaldb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("codes", sDBMapper.hospitalCodes());
		model.addAttribute("subjects", sDBMapper.hospitalSubjects());
	}
	
	public void hospitalEditor(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("medicaldb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("codes", sDBMapper.hospitalCodes());
		model.addAttribute("subjects", sDBMapper.hospitalSubjects());
		model.addAttribute("item", sDBMapper.hospitalItem(param));
		model.addAttribute("detail", sDBMapper.hospitalItemDetail(param));
	}
	
	public void pharmacy(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("medicaldb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void pharmacyEditor(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("medicaldb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("item", sDBMapper.pharmacyItem(param));
	}
	
	public void hospitalMapping(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("medicaldb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("item", sDBMapper.menuSubjectItem(param));
		model.addAttribute("codes", sDBMapper.hospitalCodes());
		model.addAttribute("subjects", sDBMapper.hospitalSubjects());
	}
	/* Medical DB End */
	
	/* Cast Start */
	public void cast(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("cast"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void castPage(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("cast"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("item", sDBMapper.castItem(param));
	}
	/* Cast End */
	
	/* Info DB Start */
	public void info(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("infodb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void infoGroups(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("infodb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("dbitem", sDBMapper.infoItem(param));
	}
	
	public void infoContents(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("infodb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("dbitem", sDBMapper.infoItem(param));
		model.addAttribute("dbgitem", sDBMapper.infoGroupItem(param));
	}
	
	public void infoPages(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("infodb"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("dbitem", sDBMapper.infoItem(param));
		model.addAttribute("dbgitem", sDBMapper.infoGroupItem(param));
		model.addAttribute("dbcitem", sDBMapper.infoContentItem(param));
	}
	/* Info DB End */
	
	
	/* SMS Start */
	public void smsList(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("sms"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		
		model.addAttribute("smsCfg", sSiteMapper.getSmsCfg(siteInfo));
		model.addAttribute("gcds", sAdminMapper.gradeList(param));
		model.addAttribute("contacts", sAdminMapper.smsGroupListAll(param));
	}
	
	public void smsContacts(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("sms"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("gcds", sAdminMapper.gradeList(param));
	}
	
	public void smsRefuse(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("sms"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	@Transactional
	public RESTResult apiSmsSend(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("content"))) {
			throw new Exception("내용을 입력해 주세요.");
		}
		
		if (StringUtil.getContent(param.get("target")).equals("P")) {
			if (StringUtil.isEmpty(param.get("mobile"))) {
				throw new Exception("휴대폰 번호를 입력해 주세요.");
			}
			if (!StringUtil.validMobile(StringUtil.getContent(param.get("mobile")))) {
				throw new Exception("올바른 형태의 휴대폰번호를 입력해 주세요. (- 포함)");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("G")) {
			if (StringUtil.isEmpty(param.get("gcd"))) {
				throw new Exception("회원등급을 선택해 주세요.");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("C")) {
			if (StringUtil.isEmpty(param.get("contact"))) {
				throw new Exception("연락처를 선택해 주세요.");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("A")) {
		} else {
			throw new Exception("발송 대상이 선택되지 않았습니다.");
		}
		
		if (!StringUtil.isEmpty(param.get("senddate")) && !StringUtil.isEmpty(param.get("sendtime"))) {
			String date = StringUtil.getContent(param.get("senddate"));
			String time = StringUtil.getContent(param.get("sendtime"));
			param.put("send_date", date.replaceAll("-", "")+time.replaceAll(":", "")+"00");
		} else if (StringUtil.isEmpty(param.get("senddate")) && StringUtil.isEmpty(param.get("sendtime"))) {
		} else {
			throw new Exception("예약발송의 날짜와 시간을 정확하게 입력해 주세요.");
		}
		
		int newIdx = sAdminMapper.smsNewIdx(param);
		param.put("idx", newIdx);
		
		mAdminMapper.smsSend(param);
		mAdminMapper.smsSendContact(param);
		mAdminMapper.smsSendTotal(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsSendList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.smsSendListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.smsSendListList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiSmsDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		mAdminMapper.smsDelete(param);
		mAdminMapper.smsDeleteContact(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsCafe24Count(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		Map<String, Object> smsCfg = sSiteMapper.getSmsCfg(param);
		MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
		parameters.add("user_id", EncryptUtils.base64Encode(StringUtil.getContent(smsCfg.get("auth1")))); // 아이디
		parameters.add("secure", EncryptUtils.base64Encode(StringUtil.getContent(smsCfg.get("auth2")))); // 인증키
		parameters.add("mode", EncryptUtils.base64Encode("1"));
		
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
		String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_remain.php"), entity, String.class);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, result);
	}
	
	@Transactional
	public RESTResult apiSmsContactsGroupAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		mAdminMapper.smsGroupAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsGroupModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		mAdminMapper.smsGroupModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsGroupDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mAdminMapper.smsGroupDelete(param);
		mAdminMapper.smsGroupDeleteContacts(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsContactsGroupList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.smsGroupListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.smsGroupList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiSmsContactsContactAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gcd"))) {
			if (StringUtil.isEmpty(param.get("name"))) {
				throw new Exception("이름을 입력해 주세요.");
			}
			
			if (StringUtil.isEmpty(param.get("mobile"))) {
				throw new Exception("휴대폰 번호를 입력해 주세요.");
			}
			
			mAdminMapper.smsContactAdd(param);
		} else {
			List<Map<String, Object>> list = sAdminMapper.memberInGroup(param);
			for (Map<String, Object> m : list) {
				if (StringUtil.isEmpty(m.get("mobile"))) {
					continue;
				}
				
				m.put("gidx", param.get("gidx"));
				if (sAdminMapper.smsContactCheck(m) == 0) {
					mAdminMapper.smsContactAdd(m);
				}
			}
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsContactModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("mobile"))) {
			throw new Exception("휴대폰 번호를 입력해 주세요.");
		}
		
		mAdminMapper.smsContactModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiSmsContactsContactDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mAdminMapper.smsContactDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiSmsContactsContactList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.smsContactListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.smsContactList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiSmsRefuseList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.smsRefuseListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.smsRefuseList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	/* SMS End */
	
	/* Mailer Start */
	public void mailerList(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("mailer"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void mailerItem(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("mailer"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("item", sAdminMapper.mailSendItem(param));
	}
	
	public void mailerSend(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("mailer"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		
		model.addAttribute("gcds", sAdminMapper.gradeList(param));
		model.addAttribute("contacts", sAdminMapper.mailGroupListAll(param));
		model.addAttribute("templates", sAdminMapper.mailTemplateListAll(param));
	}
	
	public void mailerTemplate(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("mailer"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void mailerTemplateView(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("mailer"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("template", sAdminMapper.mailTemplateItem(param));
	}
	
	public void mailerContacts(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("mailer"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		model.addAttribute("gcds", sAdminMapper.gradeList(param));
	}
	
	public void mailerRefuse(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("mailer"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	@Transactional
	public RESTResult apiMailTemplateAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		mAdminMapper.mailTemplateAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailTemplateItem(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.mailTemplateItem(param));
	}
	
	@Transactional
	public RESTResult apiMailTemplateModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("제목을 입력해 주세요.");
		}
		
		mAdminMapper.mailTemplateModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailTemplateDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		mAdminMapper.mailTemplateDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailTemplateFile(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 입력해 주세요.");
		}
		
		Map<String, Object> map = resourceService.upload(request, "mailer", file);
		param.put("filename", file.getOriginalFilename());
		param.put("path", map.get("path"));
		if (StringUtil.isEmpty(param.get("idx"))) {
			mAdminMapper.mailTemplateFileAdd(param);
		} else {
			Map<String, Object> old = sAdminMapper.mailTemplateFileItem(param);
			resourceService.delete(StringUtil.getContent(old.get("path")));
			mAdminMapper.mailTemplateFileModify(param);
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailTemplateFileList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("tidx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.mailTemplateFileList(param));
	}
	
	@Transactional
	public RESTResult apiMailTemplateFileDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("tidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		Map<String, Object> map = sAdminMapper.mailTemplateFileItem(param);
		resourceService.delete(StringUtil.getContent(map.get("path")));
		mAdminMapper.mailTemplateFileItemDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailTemplateList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.mailTemplateListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.mailTemplateList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiMailSend(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("content"))) {
			throw new Exception("내용을 입력해 주세요.");
		}
		
		if (StringUtil.getContent(param.get("target")).equals("P")) {
			if (StringUtil.isEmpty(param.get("email"))) {
				throw new Exception("이메일 주소를 입력해 주세요.");
			}
			if (!StringUtil.validEmail(StringUtil.getContent(param.get("email")))) {
				throw new Exception("올바른 형태의 메일주소를 입력해 주세요.");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("G")) {
			if (StringUtil.isEmpty(param.get("gcd"))) {
				throw new Exception("회원등급을 선택해 주세요.");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("C")) {
			if (StringUtil.isEmpty(param.get("contact"))) {
				throw new Exception("연락처를 선택해 주세요.");
			}
		} else if (StringUtil.getContent(param.get("target")).equals("A")) {
		} else {
			throw new Exception("발송 대상이 선택되지 않았습니다.");
		}
		
		if (!StringUtil.isEmpty(param.get("senddate")) && !StringUtil.isEmpty(param.get("sendtime"))) {
			String date = StringUtil.getContent(param.get("senddate"));
			String time = StringUtil.getContent(param.get("sendtime"));
			param.put("send_date", date.replaceAll("-", "")+time.replaceAll(":", "")+"00");
		} else if (StringUtil.isEmpty(param.get("senddate")) && StringUtil.isEmpty(param.get("sendtime"))) {
		} else {
			throw new Exception("예약발송의 날짜와 시간을 정확하게 입력해 주세요.");
		}
		
		int newIdx = sAdminMapper.mailNewIdx(param);
		param.put("idx", newIdx);
		
		mAdminMapper.mailSend(param);
		mAdminMapper.mailSendContact(param);
		mAdminMapper.mailSendTotal(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailSendList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.mailSendListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.mailSendList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiMailDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}

		mAdminMapper.mailDelete(param);
		mAdminMapper.mailDeleteContact(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsGroupAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		mAdminMapper.mailGroupAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsGroupModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("연락처 이름을 입력해 주세요.");
		}
		
		mAdminMapper.mailGroupModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsGroupDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mAdminMapper.mailGroupDelete(param);
		mAdminMapper.mailGroupDeleteContacts(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailContactsGroupList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.mailGroupListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.mailGroupList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiMailContactsContactAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gcd"))) {
			if (StringUtil.isEmpty(param.get("name"))) {
				throw new Exception("이름을 입력해 주세요.");
			}
			
			if (StringUtil.isEmpty(param.get("email"))) {
				throw new Exception("이메일 주소를 입력해 주세요.");
			}
			
			mAdminMapper.mailContactAdd(param);
		} else {
			List<Map<String, Object>> list = sAdminMapper.memberInGroup(param);
			for (Map<String, Object> m : list) {
				if (StringUtil.isEmpty(m.get("email"))) {
					continue;
				}
				
				m.put("gidx", param.get("gidx"));
				if (sAdminMapper.mailContactCheck(m) == 0) {
					mAdminMapper.mailContactAdd(m);
				}
			}
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsContactModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("name"))) {
			throw new Exception("이름을 입력해 주세요.");
		}
		
		if (StringUtil.isEmpty(param.get("email"))) {
			throw new Exception("이메일 주소를 입력해 주세요.");
		}
		
		mAdminMapper.mailContactModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiMailContactsContactDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		mAdminMapper.mailContactDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiMailContactsContactList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("gidx"))) {
			throw new Exception("연락처 그룹 정보가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.mailContactListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.mailContactList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiMailRefuseList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.mailRefuseListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.mailRefuseList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	/* Mailer End */
	
	/* Store Start */
	public void store(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("store"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
	}
	
	public void storeEditor(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
		if (!"Y".equals(siteInfo.get("store"))) {
			throw new Exception("사용하지 않는 기능입니다.");
		}
		param.put("sitecd", siteInfo.get("sitecd"));
		
		model.addAttribute("item", sAdminMapper.storeItem(param));
	}
	
	public RESTResult apiStoreList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sAdminMapper.storeListTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sAdminMapper.storeList(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	@Transactional
	public RESTResult apiStoreCreate(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("상점명을 입력해 주세요.");
		}
		
		mAdminMapper.storeCreate(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiStoreModify(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("title"))) {
			throw new Exception("상점명을 입력해 주세요.");
		}
		
		mAdminMapper.storeModify(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiStorePhotoAdd(HttpServletRequest request, Map<String, Object> param, MultipartFile file) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		if (file == null || file.isEmpty()) {
			throw new Exception("파일을 업로드해 주세요.");
		}
		
		if (file != null && !file.isEmpty()) {
			if (!file.getOriginalFilename().toLowerCase().endsWith("jpg") && !file.getOriginalFilename().toLowerCase().endsWith("png")) {
				throw new Exception("png, jpg 파일만 업로드가 가능합니다.");
			}
		}
		
		Map<String, Object> map = resourceService.upload(request, "store", file);
		param.put("image", file.getOriginalFilename());
		param.put("image_path", map.get("path"));
		param.put("size", file.getSize());
		mAdminMapper.storePhotoAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiStorePhotoList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.storePhotoList(param));
	}
	
	@Transactional
	public RESTResult apiStorePhotoRemove(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("sidx")) || StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유번호가 누락되었습니다.");
		}
		
		Map<String, Object> map = sAdminMapper.storePhotoItem(param);
		resourceService.delete(StringUtil.getContent(map.get("path")));
		mAdminMapper.storePhotoRemove(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	
	@Transactional
	public RESTResult apiStoreDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("idx"))) {
			throw new Exception("고유정보가 누락되었습니다.");
		}
		
		param.put("sidx", param.get("idx"));
		
		List<Map<String, Object>> files = sAdminMapper.storePhotoList(param);
		for(Map<String, Object> m : files) {
			resourceService.delete(StringUtil.getContent(m.get("path")));
		}

		mAdminMapper.storeDeletePhtos(param);
		mAdminMapper.storeDelete(param);
		mAdminMapper.storeDeleteHospital(param);
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiStoreManagers(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.storeManagers(param));
	}
	
	public RESTResult apiStoreManagerList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.storeManagersListAll(param));
	}
	
	@Transactional
	public RESTResult apiStoreManagerAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("상점 고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("midx"))) {
			throw new Exception("회원 고유번호가 누락되었습니다.");
		}
		
		if (sAdminMapper.storeManagerCheck(param) > 0) {
			throw new Exception("이미 등록된 관리 회원 입니다.");
		}
		
		mAdminMapper.storeManagerAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiStoreManagerDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("상점 고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("midx"))) {
			throw new Exception("회원 고유번호가 누락되었습니다.");
		}
		
		if (sAdminMapper.storeManagerCheck(param) == 0) {
			throw new Exception("등록되지 않은 관리 회원 입니다.");
		}
		
		mAdminMapper.storeManagerDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	public RESTResult apiStoreHospitals(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.storeHospitals(param));
	}
	
	public RESTResult apiStoreHospitalList(HttpServletRequest request, Map<String, Object> param) throws Exception {
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sAdminMapper.storeHospitalsListAll(param));
	}
	
	@Transactional
	public RESTResult apiStoreHospitalAdd(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("상점 고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("병원 고유번호가 누락되었습니다.");
		}
		
		if (sAdminMapper.storeHospitalCheck(param) > 0) {
			throw new Exception("이미 등록된 병원 입니다.");
		}
		
		mAdminMapper.storeHospitalAdd(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@Transactional
	public RESTResult apiStoreHospitalDelete(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());

		if (StringUtil.isEmpty(param.get("sidx"))) {
			throw new Exception("상점 고유번호가 누락되었습니다.");
		}
		
		if (StringUtil.isEmpty(param.get("ykiho"))) {
			throw new Exception("회원 고유번호가 누락되었습니다.");
		}
		
		if (sAdminMapper.storeHospitalCheck(param) == 0) {
			throw new Exception("등록되지 않은 병원 입니다.");
		}
		
		mAdminMapper.storeHospitalDelete(param);
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	/* Store End */
	
	/* Stats Start */
	public void stats(HttpServletRequest request, Map<String, Object> param, Model model) throws Exception {
		Map<String, Object> siteInfo = loadAdmin(request, model);
	}

	public RESTResult apiStatsAnalytics(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSiteMapper.apiStatsAnalytics(param));
	}
	
	public RESTResult apiStatsCounter(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSiteMapper.apiStatsCounter(param));
	}
	
	public RESTResult apiStatsBrowser(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("total", sSiteMapper.apiStatsBrowserTotal(param));
		for (String b : StringUtil.BROWSERS) {
			param.put("browser", b);
			ret.put(b.replaceAll(" ", "_"), sSiteMapper.apiStatsBrowserTotal(param));
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiStatsInKeyword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSiteMapper.apiStatsInKeyword(param));
	}
	
	public RESTResult apiStatsOutKeyword(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSiteMapper.apiStatsOutKeyword(param));
	}
	
	public RESTResult apiStatsReferer(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		
		if (StringUtil.isEmpty(param.get("page"))) {
			param.put("page", 1);
		}
		
		// 페이징 작성
		int totalCount = sSiteMapper.apiStatRefererTotal(param);
		Paging paging = new Paging();
		paging.setPageNo(Integer.parseInt(param.get("page").toString()));
		paging.setPageSize(numPerPage);
		paging.setBlockSize(pagePerBlock);
		paging.setTotalCount(totalCount);
		param.put("start", (paging.getPageNo()-1) * paging.getPageSize());
		param.put("pageSize", paging.getPageSize());
		
		// 반환값 생성
		Map<String, Object> ret = new HashMap<>();
		ret.put("paging", paging);
		ret.put("list", sSiteMapper.apiStatReferer(param));
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	
	public RESTResult apiStatsUrl(HttpServletRequest request, Map<String, Object> param) throws Exception {
		if (StringUtil.isEmpty(param.get("year"))) {
			throw new Exception("연도가 누락되었습니다.");
		}
		if (StringUtil.isEmpty(param.get("month"))) {
			throw new Exception("월이 누락되었습니다.");
		}
		param.put("sitecd", getSiteSession(request).getSitecd());
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sSiteMapper.apiStatsUrl(param));
	}
	/* Stats End */
}
