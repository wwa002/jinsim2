package kr.dmon.wb.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.dmon.wb.mapper.master.MasterTestMapper;
import kr.dmon.wb.mapper.slave.SlaveScheduleMapper;
import kr.dmon.wb.mapper.slave.SlaveTestMapper;

@Service
public class TestService {

	private static Logger logger = Logger.getLogger(TestService.class);
	
	@Autowired
	MasterTestMapper tmMapper;
	
	@Autowired
	SlaveTestMapper tsMapper;
	
	@Autowired
	SlaveScheduleMapper sScheduleMapper;
	
	public void test() throws Exception {
		
		List<String> tables = sScheduleMapper.listTables();
		for (String table : tables) {
			logger.debug("table >>>> " + table);
		}
	}

}
