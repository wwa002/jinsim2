package kr.dmon.wb.service;

import java.io.File;
import java.net.InetAddress;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import kr.dmon.wb.mapper.master.MasterScheduleMapper;
import kr.dmon.wb.mapper.slave.SlaveScheduleMapper;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.EncryptUtils;
import kr.dmon.wb.utils.StringUtil;

@Service
public class ScheduleService extends BaseService {
	
	private static Logger logger = Logger.getLogger(ScheduleService.class);
	
	private static boolean isSMSSending = false;
	private static boolean isMailSending = false;
	
	
	@Autowired
	ResourceLoader resourceLoader;
	
	@Autowired
	MasterScheduleMapper mScheduleMapper;
	
	@Autowired
	SlaveScheduleMapper sScheduleMapper;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	RestTemplate restTemplete;

	public String getServerName() throws Exception {
		return InetAddress.getLocalHost().getHostName();
	}
	
	/* Server Management Start */
	public boolean isMainServer() throws Exception {
		if (sScheduleMapper.isMainServer(getServerName()) > 0) {
			return true;
		}
		return false;
	}
	
	public void serverStatus() throws Exception {
		Map<String, Object> param = new HashMap<>();
		param.put("host", getServerName());
		param.put("mem_total", String.format("%.1f MB", ((double) Runtime.getRuntime().totalMemory() / (1024 * 1024))));
		param.put("mem_free", String.format("%.1f MB", ((double) Runtime.getRuntime().freeMemory() / (1024 * 1024))));
		param.put("mem_max", String.format("%.1f MB", ((double) Runtime.getRuntime().maxMemory() / (1024 * 1024))));

		if (sScheduleMapper.checkServer(param) > 0) {
			mScheduleMapper.updateServer(param);
		} else {
			mScheduleMapper.insertServer(param);
		}
	}
	
	public void electMainServer() throws Exception {
		mScheduleMapper.removeOldServer();
		if (sScheduleMapper.checkMainServer() > 0) {
			return;
		}
		
		Map<String, Object> param = new HashMap<>();
		param.put("host", sScheduleMapper.getNewMainServer());
		mScheduleMapper.electMainServer(param);
	}
	/* Server Management End */
	
	public void removeTemporaryFiles() throws Exception {
		Resource resource = resourceLoader.getResource("/temp");
		File temporaryDir = resource.getFile();
		if (temporaryDir.isDirectory()) {
			File[] files = temporaryDir.listFiles();
			for (File f : files) {
				if (!DateUtil.getDateString("yyyyMMdd").equals(f.getName())) {
					deleteFile(f);
				}
			}
		}
	}
	
	public void removeBoardFiles() throws Exception {
		try {
			List<Map<String, Object>> list = sScheduleMapper.getRemovedBoardFiles();
			for (Map<String, Object> m : list) {
				try {
					resourceService.delete(m.get("path").toString());
					mScheduleMapper.removeBoardFile(m);
				} catch (Exception e) {
					logger.error(e);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void removeWorkshopFiles() throws Exception {
		try {
			List<Map<String, Object>> list = sScheduleMapper.getRemovedWorkshopFiles();
			for (Map<String, Object> m : list) {
				try {
					resourceService.delete(m.get("path").toString());
					mScheduleMapper.removeWorkshopFile(m);
				} catch (Exception e) {
					logger.error(e);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	private void deleteFile(File path) {
		if (!path.exists()) {
			return;
		}
		
		if (path.isDirectory()) {
			File[] files = path.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					deleteFile(file);
				} else {
					file.delete();
				}
			}
		}

		path.delete();
	}
	
	/* SMS Service Start */
	public void sendSMS() throws Exception {
		if (!isMainServer()) {
			return;
		}
		
		if (isSMSSending) {
			return;
		}
		
		try {
			isSMSSending = true;
			
			List<Map<String, Object>> list = sScheduleMapper.smsList();
			for (Map<String, Object> m : list) {
				if ("M".equals(StringUtil.getContent(m.get("send_type")))) {
					if (sScheduleMapper.checkRefuseSMS(m) > 0) {
						m.put("status", "R");
						mScheduleMapper.smsSendedResult(m);
						mScheduleMapper.smsSendedCount(m);
						continue;
					}
				}
				
				m.put("content", replaceTemplate(StringUtil.getContent(m.get("content")), "name", StringUtil.getContent(m.get("name"))));
				
				if ("CAFE24".equals(StringUtil.getContent(m.get("type")))) {
					String content = StringUtil.getContent(m.get("content"));
					String[] sender = StringUtil.getContent(m.get("sender")).split("-");
					
					MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
					parameters.add("user_id", EncryptUtils.base64Encode(StringUtil.getContent(m.get("auth1")))); // 아이디
					parameters.add("secure", EncryptUtils.base64Encode(StringUtil.getContent(m.get("auth2")))); // 인증키
					parameters.add("sphone1", EncryptUtils.base64Encode(sender[0])); // 보내는 번호
					parameters.add("sphone2", EncryptUtils.base64Encode(sender[1])); // 보내는 번호
					parameters.add("sphone3", EncryptUtils.base64Encode(sender[2])); // 보내는 번호
					parameters.add("rphone", EncryptUtils.base64Encode(StringUtil.getContent(m.get("mobile")).replaceAll("-", ""))); // 받는번호
					parameters.add("msg", EncryptUtils.base64Encode(content)); // 메시지
					parameters.add("mode", EncryptUtils.base64Encode("1"));
					
					if (content.length() <= 45) {
						parameters.add("smsType", EncryptUtils.base64Encode("S"));
						
						HttpHeaders headers = new HttpHeaders();
						HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
						String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
						m.put("status", result.startsWith("success") ? "S" : "E");
					} else {
						parameters.add("smsType", EncryptUtils.base64Encode("L"));
						parameters.add("subject", EncryptUtils.base64Encode(StringUtil.getContent(m.get("title"))));
						
						HttpHeaders headers = new HttpHeaders();
						HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(parameters, headers);
						String result = restTemplete.postForObject(new URI("http://sslsms.cafe24.com/sms_sender.php"), entity, String.class);
						m.put("status", result.startsWith("success") ? "S" : "E");
					}
				} else {
					m.put("status", "E");
				}
				
				mScheduleMapper.smsSendedResult(m);
				mScheduleMapper.smsSendedCount(m);
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			isSMSSending = false;
		}
	}
	/* SMS Service End */
	
	/* Mail Service Start */
	public void sendMail() throws Exception {
		if (!isMainServer()) {
			return;
		}
		
		if (isMailSending) {
			return;
		}
		
		try {
			isMailSending = true;
			
			List<Map<String, Object>> list = sScheduleMapper.mailList();
			for (Map<String, Object> m : list) {
				boolean send = true;
				if ("M".equals(StringUtil.getContent(m.get("send_type")))) {
					if (sScheduleMapper.checkRefuseMail(m) > 0) {
						m.put("status", "R");
						mScheduleMapper.mailSendedResult(m);
						mScheduleMapper.mailSendedCount(m);
						send = false;
					}
				}
				if (send) {
					String readCounter = "<br /><img src='"+StringUtil.getContent(m.get("read_url"))+"' alt='' />";
					m.put("title", replaceTemplate(StringUtil.getContent(m.get("title")), "name", StringUtil.getContent(m.get("name"))));
					m.put("content", replaceTemplate(StringUtil.getContent(m.get("content")), "name", StringUtil.getContent(m.get("name"))));
					m.put("content", StringUtil.getContent(m.get("content")) + readCounter);
					
					try {
						m.put("status", resourceService.sendFromMailer(m) ? "S" : "E");
					} catch (Exception e) {
						logger.error(e);
						m.put("status", "E");
					}
					
					mScheduleMapper.mailSendedResult(m);
					mScheduleMapper.mailSendedCount(m);
				}
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			isMailSending = false;
		}
	}
	/* Mail Service End */

	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		return null;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		return null;
	}
}
