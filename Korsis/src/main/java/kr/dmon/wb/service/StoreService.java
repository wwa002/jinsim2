package kr.dmon.wb.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import kr.dmon.wb.mapper.master.MasterAPIMapper;
import kr.dmon.wb.mapper.master.MasterSiteMapper;
import kr.dmon.wb.mapper.master.MasterStoreMapper;
import kr.dmon.wb.mapper.slave.SlaveAPIMapper;
import kr.dmon.wb.mapper.slave.SlaveSiteMapper;
import kr.dmon.wb.mapper.slave.SlaveStoreMapper;
import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.session.MemberSession;
import kr.dmon.wb.session.SiteSession;
import kr.dmon.wb.utils.StringUtil;

@Service
public class StoreService extends BaseService {

	public static Logger logger = Logger.getLogger(StoreService.class);
	
	@Value("${builder.default.num_per_page}")
	private int numPerPage;

	@Value("${builder.default.page_per_block}")
	private int pagePerBlock;

	@Value("${builder.small.num_per_page}")
	private int numPerPageSmall;

	@Value("${builder.small.page_per_block}")
	private int pagePerBlockSmall;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	MasterSiteMapper mSiteMapper;
	
	@Autowired
	SlaveSiteMapper sSiteMapper;
	
	@Autowired
	MasterAPIMapper mAPIMapper;
	
	@Autowired
	SlaveAPIMapper sAPIMapper;
	
	@Autowired
	MasterStoreMapper mStoreMapper;
	
	@Autowired
	SlaveStoreMapper sStoreMapper;
	
	@Override
	public SiteSession getSiteSession(HttpServletRequest request) throws Exception {
		SiteSession ss = (SiteSession) request.getSession().getAttribute(SiteSession.SESSION_KEY);
		if (ss == null) {
			ss = new SiteSession();
			ss.setData(sSiteMapper.getSiteInfo(request.getServerName()));
			request.getSession().setAttribute(SiteSession.SESSION_KEY, ss);
		}
		return ss;
	}
	
	@Override
	public MemberSession getMemberSession(HttpServletRequest request) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (ms == null) {
			ms = new MemberSession(getSiteSession(request).getSitecd());
			request.getSession().setAttribute(MemberSession.SESSION_KEY, ms);
		}
		
		if (!ms.isLogin()) {
			if (!StringUtil.isEmpty(request.getHeader("auth"))) {
				Map<String, Object> param = new HashMap<>();
				param.put("sitecd", getSiteSession(request).getSitecd());
				param.put("auth", request.getHeader("auth"));
				
				Map<String, Object> mem = sAPIMapper.loginForHeader(param);
				ms.setData(mem);
				ms.setGcds(sAPIMapper.gcds(mem));
				mAPIMapper.loginDateUpdate(mem);
			}
		}
		return ms;
	}
	
	public RESTResult apiSearch(HttpServletRequest request, Map<String, Object> param) throws Exception {
		SiteSession ss = getSiteSession(request);
		param.put("sitecd", ss.getSitecd());
		
		if (StringUtil.isEmpty(param.get("keyword"))) {
			if (StringUtil.isEmpty(param.get("nelat")) || StringUtil.isEmpty(param.get("nelng"))
					|| StringUtil.isEmpty(param.get("swlat")) || StringUtil.isEmpty(param.get("swlng"))) {
				throw new Exception("범위정보가 누락되었습니다.");
			}
		}
		
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, sStoreMapper.search(param));
	}
	
}
