package kr.dmon.wb.model;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.StringUtil;

@Component
public class ConferenceRegisterXlsView extends AbstractXlsView {

	private final String[] headers = {"No", "이름", "직함", "근무처", "부서", "연락처", "이메일", "영업담당자"
			, "교통수단", "출발지", "숙박", "도착시간 (체크인)", "출발시간 (체크아웃)", "투숙인원 (성인)", "투숙인원 (소아)"
			, "객실타입", "숙박문의", "기타문의", "메모", "등록일", "최종 수정일"
			};
	private final String[] columns = {"no", "name", "title", "office", "department", "phone", "email", "marketer"
			, "traffic", "traffic_start", "stay", "stay_checkin", "stay_checkout", "stay_adult", "stay_child"
			, "stay_bed", "stay_etc", "etc", "memo", "regdate", "moddate"
			};
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Sheet sheet = workbook.createSheet();
		
		int currentRow = 0;
		int currentColumn = 0;
		Row row = sheet.createRow(currentRow++);
		
		for (String header : headers) {
			Cell cell = row.createCell(currentColumn++);
			cell.setCellValue(header);
			sheet.autoSizeColumn(currentColumn-1);
		}
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) model.get("list");
		for (Map<String, Object> m : list) {
			currentColumn = 0;
			row = sheet.createRow(currentRow++);
			for (String column : columns) {
				Cell cell = row.createCell(currentColumn++);
				if (column.equals("no")) {
					cell.setCellValue(currentRow-1);
				} else if (column.equals("traffic")) {
					String traffic = StringUtil.getContent(m.get(column));
					if ("C".equals(traffic)) {
						cell.setCellValue("자가");						
					} else if ("B".equals(traffic)) {
						cell.setCellValue("대중교통");
					} else {
						cell.setCellValue("미정");
					}
				} else if (column.equals("stay")) {
					String stay = StringUtil.getContent(m.get(column));
					if ("Y".equals(stay)) {
						cell.setCellValue("숙박함");						
					} else {
						cell.setCellValue("숙박안함");
					}
				} else if (column.equals("stay_bed")) {
					String stay_bed = StringUtil.getContent(m.get(column));
					if ("D".equals(stay_bed)) {
						cell.setCellValue("Double");						
					} else {
						cell.setCellValue("Twin");
					}
				} else if (column.equals("regdate") || column.equals("moddate")) {
					cell.setCellValue(DateUtil.getDateString(StringUtil.getContent(m.get(column)), "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss"));
				} else {
					cell.setCellValue(StringUtil.getContent(m.get(column)));
				}
			}
		}
		
		currentColumn = 0;
		for (String header : headers) {
			sheet.autoSizeColumn(currentColumn++);
		}
		
		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "attachment; filename=backup_"+DateUtil.getDateString()+".xls");
	}

}
