package kr.dmon.wb.model;

public class Paging {
	private int pageSize;		//게시글수
	private int firstPageNo;	//첫 번째 페이지 번호
	private int prevPageNo;		//이전 페이지 번호
	private int startPageNo;	//시작페이지 (페이징 네비게이션 기준)
	private int pageNo;			//페이지 번호
	private int endPageNo;		//끝 페이지 (페이징 네비게이션 기준)
	private int nextPageNo;		//다음 페이지 번호
	private int finalPageNo;	//마지막 페이지 번호
	private int totalCount;		//게시 글 전체수
	
	private int prevBlockNo;
	private int nextBlockNo;
	private int blockSize;
	
	private int virtualRecordNo;
	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getFirstPageNo() {
		return firstPageNo;
	}
	public void setFirstPageNo(int firstPageNo) {
		this.firstPageNo = firstPageNo;
	}
	public int getPrevPageNo() {
		return prevPageNo;
	}
	public void setPrevPageNo(int prevPageNo) {
		this.prevPageNo = prevPageNo;
	}
	public int getStartPageNo() {
		return startPageNo;
	}
	public void setStartPageNo(int startPageNo) {
		this.startPageNo = startPageNo;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getEndPageNo() {
		return endPageNo;
	}
	public void setEndPageNo(int endPageNo) {
		this.endPageNo = endPageNo;
	}
	public int getNextPageNo() {
		return nextPageNo;
	}
	public void setNextPageNo(int nextPageNo) {
		this.nextPageNo = nextPageNo;
	}
	public int getFinalPageNo() {
		return finalPageNo;
	}
	public void setFinalPageNo(int finalPageNo) {
		this.finalPageNo = finalPageNo;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
		this.makePaging();
	}
	public int getPrevBlockNo() {
		return prevBlockNo;
	}
	public void setPrevBlockNo(int prevBlockNo) {
		this.prevBlockNo = prevBlockNo;
	}
	public int getNextBlockNo() {
		return nextBlockNo;
	}
	public void setNextBlockNo(int nextBlockNo) {
		this.nextBlockNo = nextBlockNo;
	}
	public int getBlockSize() {
		return blockSize;
	}
	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}
	public int getVirtualRecordNo() {
		return virtualRecordNo;
	}
	public void setVirtualRecordNo(int virtualRecordNo) {
		this.virtualRecordNo = virtualRecordNo;
	}
	
	private void makePaging(){
		if (this.totalCount == 0) return;				//게시 글 전체수가 없는 경우
		if (this.pageNo == 0) this.setPageNo(1);		//기본 값 설정 (1번 페이지)
		if (this.pageSize == 0) this.setPageSize(10);	//기본 값 설정 (10개)
		if (this.blockSize == 0) this.setBlockSize(10);	//기본 값 설정 (10개)
		
		int finalPage = (totalCount + (pageSize - 1)) / pageSize;	//마지막 페이지

		if (this.pageNo > finalPage) this.setPageNo(finalPage);		//현재 페이지가 마지막 페이지보다 크다면 마지막페이지로
		
		if (this.pageNo < 0 || this.pageNo > finalPage) this.pageNo = 1; // 페이지 유효성 체크
		
		boolean isNowFirst = pageNo == 1 ? true : false; // 시작 페이지 (전체)
        boolean isNowFinal = pageNo == finalPage ? true : false; // 마지막 페이지 (전체)

        int startPage = ((pageNo - 1) / blockSize) * blockSize + 1; // 시작 페이지 (페이징 네비 기준)
        int endPage = startPage + blockSize - 1; // 끝 페이지 (페이징 네비 기준)

        if (endPage > finalPage) { // [마지막 페이지 (페이징 네비 기준) > 마지막 페이지] 보다 큰 경우
            endPage = finalPage;
        }

        this.setFirstPageNo(1); // 첫 번째 페이지 번호

        if (isNowFirst) {
            this.setPrevPageNo(1); // 이전 페이지 번호
        } else {
            this.setPrevPageNo(((pageNo - 1) < 1 ? 1 : (pageNo - 1))); // 이전 페이지 번호
        }

        this.setStartPageNo(startPage); // 시작 페이지 (페이징 네비 기준)
        this.setEndPageNo(endPage); // 끝 페이지 (페이징 네비 기준)

        if (isNowFinal) {
            this.setNextPageNo(finalPage); // 다음 페이지 번호
        } else {
            this.setNextPageNo(((pageNo + 1) > finalPage ? finalPage : (pageNo + 1))); // 다음 페이지 번호
        }
        //pageNo : 현제 페이지
  		int blockpage = ((pageNo-1)/blockSize)*blockSize + 1;
  		if (blockpage == 1){
  			this.setPrevBlockNo(1);
  		}else{
  			this.setPrevBlockNo(blockpage - blockSize);
  		}
  		
  		if ((endPage + blockSize) > finalPage){
  			this.setNextBlockNo(finalPage);
  		}else{
  			this.setNextBlockNo(blockpage + blockSize);
  		}

        this.setFinalPageNo(finalPage); // 마지막 페이지 번호
        this.setVirtualRecordNo(totalCount - pageSize * (pageNo-1));
	}
}
