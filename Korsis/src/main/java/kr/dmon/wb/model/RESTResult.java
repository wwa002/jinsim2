package kr.dmon.wb.model;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONValue;

public class RESTResult {
	
	public static final String SUCCESS = "success";
	public static final String FAILURE = "failure";
	
	public static final int OK = 0;
	public static final int ERROR = 1;
	
	private String status;
	private String message;
	private int error;
	private Object data;
	
	public RESTResult() {
		status = FAILURE;
		message = "API Error";
		error = ERROR;
	}
	
	public RESTResult(String status, int error) {
		this.status = status;
		this.message = error == OK ? "" : "Error";
		this.error = error;
	}
	
	public RESTResult(String status, String message, int error) {
		this.status = status;
		this.message = message;
		this.error = error;
	}
	
	public RESTResult(String status, String message, int error, Object data) {
		this.status = status;
		this.message = message;
		this.error = error;
		this.data = data;
	}
	
	public RESTResult(String status, int error, Object data) {
		this.status = status;
		this.message = error == OK ? "" : "Error";
		this.error = error;
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
	
	public void setSuccess() {
		this.status = SUCCESS;
		this.message = "OK";
	}
	
	public void setSuccess(String message) {
		this.status = SUCCESS;
		this.message = message;
	}
	
	public void setSuccess(String message, Object data) {
		this.status = SUCCESS;
		this.message = message;
		this.data = data;
	}
	
	public void setFailure(String message) {
		this.status = FAILURE;
		this.message = message;
	}
	
	@Override
	public String toString() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", status);
		map.put("message", message);
		map.put("error", error);
		map.put("data", data);
		return JSONValue.toJSONString(map);
	}

}
