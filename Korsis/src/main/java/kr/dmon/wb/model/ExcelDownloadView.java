package kr.dmon.wb.model;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import kr.dmon.wb.utils.DateUtil;
import kr.dmon.wb.utils.StringUtil;

public class ExcelDownloadView extends AbstractXlsView {

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Sheet sheet = workbook.createSheet();
		
		int currentRow = 0;
		int currentColumn = 0;
		Row row = sheet.createRow(currentRow++);
		
		List<String> columns = (List<String>) model.get("columns");
		Map<String, String> titles = (Map<String, String>) model.get("titles");
		
		Cell cell = row.createCell(currentColumn++);
		cell.setCellValue("NO");
		
		for (String header : columns) {
			cell = row.createCell(currentColumn++);
			cell.setCellValue(titles.get(header));
		}
		
		List<Map<String, Object>> list = (List<Map<String, Object>>) model.get("list");
		int i = 1;
		for (Map<String, Object> m : list) {
			currentColumn = 0;
			row = sheet.createRow(currentRow++);
			
			cell = row.createCell(currentColumn++);
			cell.setCellValue(i++);
			
			for (String header : columns) {
				cell = row.createCell(currentColumn++);
				cell.setCellValue(StringUtil.getContent(m.get(header)));
			}
		}
		
		currentColumn = 0;
		sheet.autoSizeColumn(currentColumn++);
		for (String header : columns) {
			sheet.autoSizeColumn(currentColumn++);
		}

		response.setContentType("Application/Msexcel");
		response.setHeader("Content-Disposition", "attachment; filename="+StringUtil.getContent(model.get("filename"))+"_"+DateUtil.getDateString()+".xls");
	}

}
