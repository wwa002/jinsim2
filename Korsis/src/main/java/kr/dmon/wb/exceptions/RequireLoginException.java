package kr.dmon.wb.exceptions;

public class RequireLoginException extends Exception {
	
	public RequireLoginException(String message) {
		super(message);
	}

}
