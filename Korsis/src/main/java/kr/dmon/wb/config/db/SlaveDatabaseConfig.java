package kr.dmon.wb.config.db;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@MapperScan(value="kr.dmon.wb.mapper.slave", sqlSessionFactoryRef="slaveSqlSessionFactory")
@EnableTransactionManagement
public class SlaveDatabaseConfig {
	
	@Bean(name = "slaveDataSource")
    public DataSource slaveDataSource() {
		DataSource ds = new DataSource();
		ds.setDriverClassName(DatabaseConfig.Slave.getDriver());
		ds.setUrl(DatabaseConfig.Slave.getUrl());
		ds.setUsername(DatabaseConfig.Slave.getUsername());
		ds.setPassword(DatabaseConfig.Slave.getPassword());
		ds.setTestWhileIdle(true);     
		ds.setTimeBetweenEvictionRunsMillis(DatabaseConfig.getBetweenEvictionRunsMillis());
		ds.setInitSQL(DatabaseConfig.getInitSQL());
		ds.setValidationQuery(DatabaseConfig.getValidationQuery());
		ds.setRemoveAbandoned(true);
		ds.setRemoveAbandonedTimeout(60);
		ds.setLogAbandoned(true);
        return ds;
    }
 
    @Bean(name = "slaveSqlSessionFactory")
    public SqlSessionFactory slaveSqlSessionFactory(@Qualifier("slaveDataSource") DataSource slaveDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setConfigLocation(applicationContext.getResource("classpath:/mybatis-config.xml"));
        sqlSessionFactoryBean.setDataSource(slaveDataSource);
        sqlSessionFactoryBean.setTypeAliasesPackage("kr.dmon.wb");
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:/mapper/slave/**/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }
 
    @Bean(name = "slaveSqlSessionTemplate")
    public SqlSessionTemplate slaveSqlSessionTemplate(@Qualifier("slaveSqlSessionFactory") SqlSessionFactory slaveSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(slaveSqlSessionFactory);
    }
	
}
