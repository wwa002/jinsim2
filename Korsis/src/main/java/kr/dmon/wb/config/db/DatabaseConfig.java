package kr.dmon.wb.config.db;

public class DatabaseConfig {
	private static boolean DEBUG_DB = false;
	private static String SITE_DRIVER = "com.mysql.jdbc.Driver";
	private static String DEBUG_URL = "jdbc:mysql://localhost:3306/builder?autoReconnect=true&useUnicode=yes&characterEncoding=utf8";
	private static String DEBUG_USERNAME = "root";
	private static String DEBUG_PASSWORD = "dktlskdy";
	
	public static String getInitSQL() {
		return "set names utf8mb4";
	}
	
	public static String getValidationQuery() {
		return "SELECT 1";
	}
	
	public static int getBetweenEvictionRunsMillis() {
		return 30000;
	}
	
	public static class Master {
		private static String driver = SITE_DRIVER;
		private static String url = DEBUG_DB ? DEBUG_URL : "jdbc:mysql://jinsim.cjuqk1iutqhp.ap-northeast-2.rds.amazonaws.com:3306/korsis?autoReconnect=true&useUnicode=yes&characterEncoding=utf8";
		private static String username = DEBUG_DB ? DEBUG_USERNAME : "korsis";
		private static String password = DEBUG_DB ? DEBUG_PASSWORD : "elahs7045";
		
		public static String getDriver() {
			return driver;
		}
		public static String getUrl() {
			return url;
		}
		public static String getUsername() {
			return username;
		}
		public static String getPassword() {
			return password;
		}
	}
	
	public static class Slave {
		private static String driver = SITE_DRIVER;
		private static String url = DEBUG_DB ? DEBUG_URL : "jdbc:mysql://jinsim.cjuqk1iutqhp.ap-northeast-2.rds.amazonaws.com:3306/korsis?autoReconnect=true&useUnicode=yes&characterEncoding=utf8";
		private static String username = DEBUG_DB ? DEBUG_USERNAME : "korsis";
		private static String password = DEBUG_DB ? DEBUG_PASSWORD : "elahs7045";
		
		public static String getDriver() {
			return driver;
		}
		public static String getUrl() {
			return url;
		}
		public static String getUsername() {
			return username;
		}
		public static String getPassword() {
			return password;
		}
	}

}
