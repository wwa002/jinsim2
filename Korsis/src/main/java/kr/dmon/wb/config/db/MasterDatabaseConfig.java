package kr.dmon.wb.config.db;


import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@MapperScan(value="kr.dmon.wb.mapper.master", sqlSessionFactoryRef="masterSqlSessionFactory")
@EnableTransactionManagement
public class MasterDatabaseConfig {
	
	@Bean(name = "masterDataSource")
    @Primary
    public DataSource masterDataSource() {
		DataSource ds = new DataSource();
		ds.setDriverClassName(DatabaseConfig.Master.getDriver());
		ds.setUrl(DatabaseConfig.Master.getUrl());
		ds.setUsername(DatabaseConfig.Master.getUsername());
		ds.setPassword(DatabaseConfig.Master.getPassword());
		ds.setTestWhileIdle(true);     
		ds.setTimeBetweenEvictionRunsMillis(DatabaseConfig.getBetweenEvictionRunsMillis());
		ds.setInitSQL(DatabaseConfig.getInitSQL());
		ds.setValidationQuery(DatabaseConfig.getValidationQuery());
		ds.setRemoveAbandoned(true);
		ds.setRemoveAbandonedTimeout(60);
		ds.setLogAbandoned(true);
        return ds;
    }
 
    @Bean(name = "masterSqlSessionFactory")
    @Primary
    public SqlSessionFactory masterSqlSessionFactory(@Qualifier("masterDataSource") DataSource masterDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setConfigLocation(applicationContext.getResource("classpath:/mybatis-config.xml"));
        sqlSessionFactoryBean.setDataSource(masterDataSource);
        sqlSessionFactoryBean.setTypeAliasesPackage("kr.dmon.wb");
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:/mapper/master/**/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }
 
    @Bean(name = "masterSqlSessionTemplate")
    @Primary
    public SqlSessionTemplate masterSqlSessionTemplate(@Qualifier("masterSqlSessionFactory") SqlSessionFactory masterSqlSessionFactory) throws Exception {
        return new SqlSessionTemplate(masterSqlSessionFactory);
    }
	
}
