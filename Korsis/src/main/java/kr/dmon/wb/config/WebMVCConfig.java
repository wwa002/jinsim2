package kr.dmon.wb.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import kr.dmon.wb.interceptors.api.APIAdminInterceptor;
import kr.dmon.wb.interceptors.api.APIBuilderInterceptor;
import kr.dmon.wb.interceptors.web.AdminInterceptor;
import kr.dmon.wb.interceptors.web.BuilderInterceptor;
import kr.dmon.wb.interceptors.web.MainInterceptor;

@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// Front
		registry.addInterceptor(new MainInterceptor()).addPathPatterns("/**").excludePathPatterns("/builder/**", "/admin/**", "/api/**");
				
		// Builder
		registry.addInterceptor(new BuilderInterceptor()).addPathPatterns("/builder/**");
		registry.addInterceptor(new APIBuilderInterceptor()).addPathPatterns("/api/builder/**");
		
		// Admin
		registry.addInterceptor(new AdminInterceptor()).addPathPatterns("/admin/**");
		registry.addInterceptor(new APIAdminInterceptor()).addPathPatterns("/api/admin/**");
	}
	
}
