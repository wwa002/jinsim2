package kr.dmon.wb.interceptors;

import org.springframework.web.servlet.HandlerInterceptor;

public abstract class BaseInterceptor implements HandlerInterceptor {
	
	public static String SITESTART = "SITE_START_TIME";
	public static String SITEEND = "SITE_END_TIME";
	public static String SITETAKEN = "SITE_TAKEN_TIME";
	
}
