package kr.dmon.wb.interceptors.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import kr.dmon.wb.interceptors.BaseInterceptor;
import kr.dmon.wb.session.MemberSession;

public class APIBuilderInterceptor extends BaseInterceptor {

	public static Logger logger = Logger.getLogger(APIBuilderInterceptor.class);

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (request.getRequestURI().equals("/api/builder/login")) {
			return true;
		} else {
			if (ms == null) {
				return false;
			}
			if (!ms.isLogin()) {
				return false;
			}
			if (!ms.isBuilder()) {
				return false;
			}
		}
		return true;
	}

}