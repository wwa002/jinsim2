package kr.dmon.wb.interceptors.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import kr.dmon.wb.interceptors.BaseInterceptor;
import kr.dmon.wb.session.MemberSession;

public class BuilderInterceptor extends BaseInterceptor {

	public static Logger logger = Logger.getLogger(BuilderInterceptor.class);

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		MemberSession ms = (MemberSession) request.getSession().getAttribute(MemberSession.SESSION_KEY);
		if (request.getRequestURI().equals("/builder/login")) {
			if (ms == null || !ms.isLogin()) {
				return true;
			} else {
				if (ms.isBuilder()) {
					response.sendRedirect("/builder");
					return false;
				} else {
					response.sendRedirect("/logout");
					return false;
				}
			}
		} else {
			if (ms == null) {
				response.sendRedirect("/logout");
				return false;
			}
			if (!ms.isLogin()) {
				response.sendRedirect("/logout");
				return false;
			}
			if (!ms.isBuilder()) {
				response.sendRedirect("/logout");
				return false;
			}
		}

		return true;
	}

}