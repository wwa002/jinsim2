package kr.dmon.wb.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.EventService;
import kr.dmon.wb.service.ResourceService;

@RestController
@RequestMapping("/api/event")
public class APIEventController {

	public static Logger logger = Logger.getLogger(APIEventController.class);

	@Autowired
	EventService eventService;
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping
	public RESTResult eventList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiEventList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{idx}")
	public RESTResult eventView(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			return eventService.apiEventView(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{idx}/apply")
	public RESTResult eventApply(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			return eventService.apiEventApply(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{idx}/favorite")
	public RESTResult eventFavorite(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			return eventService.apiEventFavorite(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/exhibition")
	public RESTResult exhibitionList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiExhibition(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/exhibition/detail")
	public RESTResult exhibitionDetail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiExhibitionDetail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/category")
	public RESTResult category(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiCategory(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/categories")
	public RESTResult categories(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiCategories(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/icon/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> eventIcon(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.eventIconDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/page/{eidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> eventPage(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int eidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("eidx", eidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.eventPageDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/plan/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> planIcon(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.eventPlanDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
}
