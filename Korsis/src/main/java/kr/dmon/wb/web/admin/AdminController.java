package kr.dmon.wb.web.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.dmon.wb.service.admin.AdminService;
import kr.dmon.wb.utils.StringUtil;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	public static Logger logger = Logger.getLogger(AdminController.class);

	@Autowired
	AdminService adminService;
	
	@RequestMapping
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		return "redirect:/admin/dashboard";
	}
	
	@RequestMapping("/login")
	public String login(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return adminService.login(request, param, model);
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/dashboard")
	public String dashboard(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.dashboard(request, param, model);
			return "/admin/dashboard";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	/* Member Start */
	@RequestMapping("/member")
	public String member(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.member(request, param, model);
			return "/admin/member/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/member/{id:.+}")
	public String memberInfo(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String id) throws Exception {
		try {
			param.put("id", id);
			adminService.memberInfo(request, param, model);
			return "/admin/member/info";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Member End */
	
	/* APP Start */
	@RequestMapping("/app/keyword")
	public String appKeyword(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.appKeyword(request, param, model);
			return "/admin/app/keyword";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	@RequestMapping("/app/push")
	public String appPush(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.appPush(request, param, model);
			return "/admin/app/push";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	@RequestMapping("/app/notice")
	public String appNotice(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.appNotice(request, param, model);
			return "/admin/app/notice";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* APP End */
	
	/* Board Start */
	@RequestMapping("/board/{code}")
	public String board(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			adminService.board(request, param, model);
			return "/admin/board/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/board/{code}/write")
	public String boardWrite(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			adminService.board(request, param, model);
			return "/admin/board/write";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/board/{code}/{idx}")
	public String boardView(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.board(request, param, model);
			return "/admin/board/view";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/board/{code}/{idx}/modify")
	public String boardModify(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.board(request, param, model);
			return "/admin/board/modify";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/board/{code}/{idx}/reply")
	public String boardReply(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.board(request, param, model);
			return "/admin/board/reply";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Board End */
	
	/* Society Start */
	@RequestMapping("/society/member/list")
	public String societyMemberList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.member(request, param, model);
			return "/admin/society/member/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/member/manage/{id}")
	public String societyMemberList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String id) throws Exception {
		try {
			param.put("id", id);
			adminService.member(request, param, model);
			return "/admin/society/member/manage";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/member/join")
	public String societyMemberJoin(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.member(request, param, model);
			return "/admin/society/member/join";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/member/exit")
	public String societyMemberExit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.member(request, param, model);
			return "/admin/society/member/exit";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/workshop/{code}")
	public String societyWorkshop(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			adminService.workshop(request, param, model);
			return "/admin/society/workshop/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/workshop/{code}/write")
	public String societyWorkshopWrite(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			adminService.workshop(request, param, model);
			return "/admin/society/workshop/write";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/workshop/{code}/{idx}/modify")
	public String societyWorkshopModify(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.workshop(request, param, model);
			return "/admin/society/workshop/modify";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/workshop/{code}/{idx}/registration")
	public String societyWorkshopRegistration(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.workshop(request, param, model);
			return "/admin/society/workshop/registration";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/workshop/{code}/{widx}/registration/{idx}")
	public String societyWorkshopRegistrationModify(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int widx, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", widx);
			param.put("idx", idx);
			adminService.loadAdmin(request, model);
			return "/admin/society/workshop/registration_modify";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/hospital")
	public String societyHospital(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.loadAdmin(request, model);
			return "/admin/society/hospital";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Society End */
	
	/* Conference Start */
	@RequestMapping("/conference/{code}")
	public String conference(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			adminService.conference(request, param, model);
			return "/admin/conference/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/conference/{code}/add")
	public String conferenceAdd(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			adminService.conference(request, param, model);
			return "/admin/conference/add";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/conference/{code}/{idx}")
	public String conferenceEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.conference(request, param, model);
			return "/admin/conference/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	@RequestMapping("/conference/{code}/{idx}/{mode}")
	public String conferencePlay(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx, @PathVariable String mode) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			adminService.conference(request, param, model);
			return "/admin/conference/"+mode;
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Conference End */
	
	/* Event Start */
	@RequestMapping("/event/manager")
	public String eventManager(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.eventManager(request, param, model);
			return "/admin/event/manager";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/manager/{idx}")
	public String eventManagerEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.eventManager(request, param, model);
			return "/admin/event/manager_editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/banner")
	public String eventBanner(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.eventBanner(request, param, model);
			return "/admin/event/banner";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/project")
	public String eventProject(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.eventProject(request, param, model);
			return "/admin/event/project";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/project/{idx}")
	public String eventProjectEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.eventProject(request, param, model);
			return "/admin/event/project_editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/new")
	public String eventNew(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.eventNew(request, param, model);
			return "/admin/event/new";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/event/week")
	public String eventWeek(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.eventWeek(request, param, model);
			return "/admin/event/week";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	/* Event End */

	/* Medical DB Start */
	@RequestMapping("/db/map/medical")
	public String mapMedical(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.mapMedical(request, param, model);
			return "/admin/db/map_medical";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/hospital")
	public String hospital(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.hospital(request, param, model);
			return "/admin/db/db_hospital";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/hospital/editor/{ykiho}")
	public String hospitalEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String ykiho) throws Exception {
		try {
			param.put("ykiho", ykiho);
			adminService.hospitalEditor(request, param, model);
			return "/admin/db/db_hospital_editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/pharmacy")
	public String pharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.pharmacy(request, param, model);
			return "/admin/db/db_pharmacy";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/pharmacy/editor/{hpid}")
	public String pharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String hpid) throws Exception {
		try {
			param.put("hpid", hpid);
			adminService.pharmacyEditor(request, param, model);
			return "/admin/db/db_pharmacy_editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/hospital/{idx}")
	public String hospitalMapping(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.hospitalMapping(request, param, model);
			return "/admin/db/hospital";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Medical DB End */
	
	/* Cast Start */
	@RequestMapping("/db/cast")
	public String cast(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.cast(request, param, model);
			return "/admin/db/cast";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/cast/{idx}")
	public String castPage(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.castPage(request, param, model);
			return "/admin/db/cast_page";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Cast End */
	
	/* Info DB Start */
	@RequestMapping("/db/info")
	public String info(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.info(request, param, model);
			return "/admin/db/info";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/info/{dbidx}")
	public String infoGroups(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int dbidx) throws Exception {
		try {
			param.put("dbidx", dbidx);
			adminService.infoGroups(request, param, model);
			return "/admin/db/info_groups";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/info/{dbidx}/{dbgidx}")
	public String infoContents(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int dbidx, @PathVariable int dbgidx) throws Exception {
		try {
			param.put("dbidx", dbidx);
			param.put("dbgidx", dbgidx);
			adminService.infoContents(request, param, model);
			return "/admin/db/info_contents";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/db/info/{dbidx}/{dbgidx}/{dbcidx}")
	public String infoContentPages(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int dbidx, @PathVariable int dbgidx, @PathVariable int dbcidx) throws Exception {
		try {
			param.put("dbidx", dbidx);
			param.put("dbgidx", dbgidx);
			param.put("dbcidx", dbcidx);
			adminService.infoPages(request, param, model);
			return "/admin/db/info_pages";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Info DB End */
	
	/* SMS Start */
	@RequestMapping("/sms/list")
	public String smsList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.smsList(request, param, model);
			return "/admin/sms/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/sms/contacts")
	public String smsContacts(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.smsContacts(request, param, model);
			return "/admin/sms/contacts";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/sms/refuse")
	public String smsRefuse(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.smsRefuse(request, param, model);
			return "/admin/sms/refuse";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* SMS End */
	
	
	/* Mailer Start */
	@RequestMapping("/mailer/list")
	public String mailerList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.mailerList(request, param, model);
			return "/admin/mailer/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/item/{idx}")
	public String mailerItem(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.mailerItem(request, param, model);
			return "/admin/mailer/item";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/send")
	public String mailerSend(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.mailerSend(request, param, model);
			return "/admin/mailer/send";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/send/view")
	public String mailerTemplateSampleView(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return "/admin/mailer/send_view";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/template")
	public String mailerTemplate(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.mailerTemplate(request, param, model);
			return "/admin/mailer/template";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/template/{idx}")
	public String mailerTemplateEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.mailerTemplateView(request, param, model);
			return "/admin/mailer/template_editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/template/{idx}/view")
	public String mailerTemplateView(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.mailerTemplateView(request, param, model);
			return "/admin/mailer/template_display";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/contacts")
	public String mailerContacts(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.mailerContacts(request, param, model);
			return "/admin/mailer/contacts";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/mailer/refuse")
	public String mailerRefuse(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.mailerRefuse(request, param, model);
			return "/admin/mailer/refuse";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Mailer End */
	
	/* Store Start */
	@RequestMapping("/store")
	public String store(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.store(request, param, model);
			return "/admin/store/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/store/{idx}")
	public String storeEditor(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			adminService.storeEditor(request, param, model);
			return "/admin/store/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Store End */
	
	/* Stats Start */
	@RequestMapping("/stats")
	public String stats(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			adminService.stats(request, param, model);
			return "/admin/stats";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	/* Stats End */
	
}
