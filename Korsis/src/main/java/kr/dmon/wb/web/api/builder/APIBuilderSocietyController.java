package kr.dmon.wb.web.api.builder;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.builder.BuilderService;

@RestController
@RequestMapping("/api/builder/society")
public class APIBuilderSocietyController {

	public static Logger logger = Logger.getLogger(APIBuilderSocietyController.class);
	
	@Autowired
	BuilderService builderService;
	
	
	@RequestMapping("/workshop/create")
	public RESTResult create(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSocietyWorkshopCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/modify")
	public RESTResult modify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSocietyWorkshopModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSocietyWorkshopDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
