package kr.dmon.wb.web.api.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.ConferenceService;
import kr.dmon.wb.service.admin.AdminService;

@RestController
@RequestMapping("/api/admin/conference")
public class APIAdminConferenceController {

	public static Logger logger = Logger.getLogger(APIAdminConferenceController.class);
	
	@Autowired
	AdminService adminService;
	
	@Autowired
	ConferenceService conferenceService;
	
	@RequestMapping("/cfg")
	public RESTResult cfg(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminCfg(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/register/excel")
	public ModelAndView registerExcel(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		return conferenceService.registerExcel(request, param);
	}
	
	@RequestMapping("/register/list")
	public RESTResult registerList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminRegisterList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/register/item")
	public RESTResult registerItem(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminRegisterItem(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/register/modify")
	public RESTResult registerModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminRegisterModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/register/delete")
	public RESTResult registerDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminRegisterDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/create")
	public RESTResult create(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/modify")
	public RESTResult modify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return conferenceService.apiAdminSessionModify(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/page")
	public RESTResult page(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionPage(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/question")
	public RESTResult question(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionQuestion(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/question/principal")
	public RESTResult questionPrincipal(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionQuestionPrincipal(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/question/monitor")
	public RESTResult questionMonitor(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionQuestionMonitor(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/question/delete")
	public RESTResult questionDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionQuestionDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/question/show")
	public RESTResult questionShow(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionQuestionShow(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/question/display")
	public RESTResult questionDisplay(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminSessionQuestionDisplay(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote/list")
	public RESTResult voteList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminVoteList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote/add")
	public RESTResult voteAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminVoteAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote/modify")
	public RESTResult voteModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminVoteModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote/delete")
	public RESTResult voteDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminVoteDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote/run")
	public RESTResult voteRun(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminVoteRun(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/vote/result")
	public RESTResult voteResult(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiAdminVoteResult(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	
}
