package kr.dmon.wb.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.service.SocietyService;

@RestController
@RequestMapping("/api/society")
public class APISocietyController {
	
	public static Logger logger = Logger.getLogger(APISocietyController.class);

	@Autowired
	ResourceService resourceService;
	
	@Autowired
	SocietyService societyService;
	
	@RequestMapping("/workshop/{code}/cfg")
	public RESTResult workshopCfg(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return societyService.apiWorkshopCfg(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}")
	public RESTResult workshopList(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return societyService.apiWorkshopList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}")
	public RESTResult workshopView(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return societyService.apiWorkshopView(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/registration")
	public RESTResult workshopRegistration(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return societyService.apiWorkshopRegistration(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/registration/modify")
	public RESTResult workshopRegistrationModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", idx);
			return societyService.apiWorkshopRegistrationModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/registration/check")
	public RESTResult workshopRegistrationCheck(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return societyService.apiWorkshopRegistrationCheck(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/banner")
	public ResponseEntity<byte[]> workshopViewBanner(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return resourceService.workshopBannerDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping("/workshop/download/{code}/{widx}/{idx}")
	public ResponseEntity<byte[]> workshopViewDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int widx, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", widx);
			param.put("idx", idx);
			return resourceService.workshopFileDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping("/workshop/monthly")
	public RESTResult workshopMonthly(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiWorkshopMonthly(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/fullcalendar")
	public RESTResult workshopFullCalendar(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiWorkshopFullCalendar(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/member")
	public RESTResult searchMember(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiSearchMember(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital/location")
	public RESTResult searchHospitalLocation(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiSearchHospitalLocation(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital")
	public RESTResult searchHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiSearchHospital(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
