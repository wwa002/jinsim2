package kr.dmon.wb.web.api.builder;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.builder.BuilderService;

@RestController
@RequestMapping("/api/builder/system")
public class APIBuilderSystemController {
	
	public static Logger logger = Logger.getLogger(APIBuilderSystemController.class);
	
	@Autowired
	BuilderService builderService;
	
	@RequestMapping("/url/list")
	public RESTResult urlList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemURLList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/url/add")
	public RESTResult urlAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemURLAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/url/modify")
	public RESTResult urlModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemURLModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/url/delete")
	public RESTResult urlDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemURLDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/url/main")
	public RESTResult urlMain(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemURLMain(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cfg")
	public RESTResult cfg(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemCfg(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cfg/app")
	public RESTResult cfgApp(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemCfgApp(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cfg/society")
	public RESTResult cfgSociety(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemCfgSociety(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/sms/cfg")
	public RESTResult smsCfg(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemSmsCfg(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/app/cfg")
	public RESTResult appFirebase(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemAppCfg(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/save")
	public RESTResult memberSave(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return builderService.apiSystemMemberSave(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/delete")
	public RESTResult memberDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemMemberDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	
	@RequestMapping("/member/grade/add")
	public RESTResult memberGradeAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemMemberGradeAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/grade/modify")
	public RESTResult memberGradeModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemMemberGradeModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/grade/delete")
	public RESTResult memberGradeDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemMemberGradeDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/robot")
	public RESTResult robot(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiSystemRobot(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

}
