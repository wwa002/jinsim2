package kr.dmon.wb.web.api.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.admin.AdminService;

@RestController
@RequestMapping("/api/admin/app")
public class APIAdminAppController {

	public static Logger logger = Logger.getLogger(APIAdminAppController.class);
	
	@Autowired
	AdminService adminService;
	
	@RequestMapping("/keyword/list")
	public RESTResult keywordList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppKeywordList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/keyword/all")
	public RESTResult keywordListAll(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppKeywordListAll(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/keyword/create")
	public RESTResult keywordCreate(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppKeywordCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/keyword/delete")
	public RESTResult keywordDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppKeywordDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/keyword/up")
	public RESTResult keywordUp(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppKeywordUp(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/keyword/down")
	public RESTResult keywordDown(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppKeywordDown(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/system")
	public RESTResult systemList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppSystemList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/system/save")
	public RESTResult systemProc(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppSystemProc(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/system/delete")
	public RESTResult systemDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiAppSystemDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
}
