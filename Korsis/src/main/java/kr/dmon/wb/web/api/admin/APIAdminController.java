package kr.dmon.wb.web.api.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.admin.AdminService;

@RestController
@RequestMapping("/api/admin")
public class APIAdminController {

	public static Logger logger = Logger.getLogger(APIAdminController.class);

	@Autowired
	AdminService adminService;
	
	@RequestMapping
	public RESTResult index(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@RequestMapping("/dashboard")
	public RESTResult dashboard(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiDashboard(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/analytics")
	public RESTResult statsAnalytics(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsAnalytics(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/counter")
	public RESTResult statsCounter(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsCounter(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/browser")
	public RESTResult statsBrowser(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsBrowser(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/inkeyword")
	public RESTResult statsInKeyword(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsInKeyword(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/outkeyword")
	public RESTResult statsOutKeyword(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsOutKeyword(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/referer")
	public RESTResult statsReferer(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsReferer(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/stats/url")
	public RESTResult statsUrl(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStatsUrl(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/sms/send")
	public RESTResult smsSend(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsSend(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/send/list")
	public RESTResult smsSendList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsSendList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/delete")
	public RESTResult smsDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/cafe24/count")
	public RESTResult smsCafe24Count(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsCafe24Count(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/add")
	public RESTResult smsContactsGroupAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/modify")
	public RESTResult smsContactsGroupModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/delete")
	public RESTResult smsContactsGroupDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/group/list")
	public RESTResult smsContactsGroupList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsGroupList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/add")
	public RESTResult smsContactsContactAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/modify")
	public RESTResult smsContactsContactModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/delete")
	public RESTResult smsContactsContactDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/contacts/contact/list")
	public RESTResult smsContactsContactList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsContactsContactList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/sms/refuse/list")
	public RESTResult smsRefuseList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiSmsRefuseList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/send")
	public RESTResult mailSend(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailSend(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/send/list")
	public RESTResult mailSendList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailSendList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/delete")
	public RESTResult mailDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/add")
	public RESTResult mailTemplateAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
		
	@RequestMapping("/mail/template/item")
	public RESTResult mailTemplateItem(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateItem(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/modify")
	public RESTResult mailTemplateModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/delete")
	public RESTResult mailTemplateDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/template/file")
	public RESTResult mailTemplateFile(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return adminService.apiMailTemplateFile(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/mail/template/file/list")
	public RESTResult mailTemplateFileList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateFileList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/mail/template/file/delete")
	public RESTResult mailTemplateFileDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateFileDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	
	@RequestMapping("/mail/template/list")
	public RESTResult mailTemplateList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailTemplateList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/add")
	public RESTResult mailContactsGroupAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/modify")
	public RESTResult mailContactsGroupModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/delete")
	public RESTResult mailContactsGroupDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/group/list")
	public RESTResult mailContactsGroupList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsGroupList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/add")
	public RESTResult mailContactsContactAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/modify")
	public RESTResult mailContactsContactModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/delete")
	public RESTResult mailContactsContactDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/contacts/contact/list")
	public RESTResult mailContactsContactList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailContactsContactList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/mail/refuse/list")
	public RESTResult mailRefuseList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMailRefuseList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
}
