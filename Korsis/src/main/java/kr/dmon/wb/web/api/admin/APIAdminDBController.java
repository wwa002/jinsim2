package kr.dmon.wb.web.api.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.DBService;
import kr.dmon.wb.service.admin.AdminService;

@RestController
@RequestMapping("/api/admin/db")
public class APIAdminDBController {

	public static Logger logger = Logger.getLogger(APIAdminDBController.class);

	@Autowired
	AdminService adminService;
	
	@Autowired
	DBService dbService;
	
	@RequestMapping("/hospital/add")
	public RESTResult hospitalAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiHospitalAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/modify/basic")
	public RESTResult hospitalModifyBasic(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiHospitalModifyBasic(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/modify/doctor")
	public RESTResult hospitalModifyDoctor(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiHospitalModifyDoctor(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/modify/detail")
	public RESTResult hospitalModifyDetail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiHospitalModifyDetail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/delete")
	public RESTResult hospitalDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiHospitalDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/pharmacy/add")
	public RESTResult pharmacyAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiPharmacyAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/pharmacy/modify/basic")
	public RESTResult pharmacyModifyBasic(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiPharmacyModifyBasic(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/pharmacy/modify/detail")
	public RESTResult pharmacyModifyDetail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiPharmacyModifyDetail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/pharmacy/delete")
	public RESTResult pharmacyDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiPharmacyDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	@RequestMapping("/search/hospital/in")
	public RESTResult searchHospitalIn(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiSearchHospitalSubjectIn(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital/notin")
	public RESTResult searchHospitalNotIn(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiSearchHospitalSubjectNotIn(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital/add")
	public RESTResult searchHospitalAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiSearchHospitalSubjectAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital/delete")
	public RESTResult searchHospitalDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiSearchHospitalSubjectDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast")
	public RESTResult castList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiCastList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/create")
	public RESTResult castCreate(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image, @RequestParam(required=false) MultipartFile background) throws Exception {
		try {
			return dbService.apiCastCreate(request, param, image, background);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/modify")
	public RESTResult castModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image, @RequestParam(required=false) MultipartFile background) throws Exception {
		try {
			return dbService.apiCastModify(request, param, image, background);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/delete")
	public RESTResult castDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiCastDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/status")
	public RESTResult castStatus(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiCastStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/banner")
	public RESTResult castBanner(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiCastBanner(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/page")
	public RESTResult castPageList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiCastPageList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/page/create")
	public RESTResult castPageCreate(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return dbService.apiCastPageCreate(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/page/modify")
	public RESTResult castPageModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return dbService.apiCastPageModify(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/page/delete")
	public RESTResult castPageDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiCastPageDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info")
	public RESTResult infoList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/create")
	public RESTResult infoCreate(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image) throws Exception {
		try {
			return dbService.apiInfoCreate(request, param, image);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/modify")
	public RESTResult infoModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image) throws Exception {
		try {
			return dbService.apiInfoModify(request, param, image);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/status")
	public RESTResult infoStatus(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/delete")
	public RESTResult infoDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	@RequestMapping("/info/group")
	public RESTResult infoGroupList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoGroupList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/group/create")
	public RESTResult infoGroupCreate(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoGroupCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/group/modify")
	public RESTResult infoGroupModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoGroupModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/group/delete")
	public RESTResult infoGroupDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoGroupDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/content")
	public RESTResult infoContentList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoContentList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/content/create")
	public RESTResult infoContentCreate(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image, @RequestParam(required=false) MultipartFile background) throws Exception {
		try {
			return dbService.apiInfoContentCreate(request, param, image, background);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/content/modify")
	public RESTResult infoContentModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image, @RequestParam(required=false) MultipartFile background) throws Exception {
		try {
			return dbService.apiInfoContentModify(request, param, image, background);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/content/delete")
	public RESTResult infoContentDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoContentDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	@RequestMapping("/info/page")
	public RESTResult infoPageList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoPageList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/page/create")
	public RESTResult infoPageCreate(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return dbService.apiInfoPageCreate(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/page/modify")
	public RESTResult infoPageModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return dbService.apiInfoPageModify(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info/page/delete")
	public RESTResult infoPageDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return dbService.apiInfoPageDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
}
