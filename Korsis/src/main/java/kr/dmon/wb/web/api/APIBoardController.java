package kr.dmon.wb.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.BoardService;
import kr.dmon.wb.service.CommonService;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.utils.StringUtil;

@RestController
@RequestMapping("/api/board")
public class APIBoardController {

	public static Logger logger = Logger.getLogger(APIBoardController.class);
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	BoardService boardService;
	
	@RequestMapping
	public RESTResult index(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return boardService.index(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}/cfg")
	public RESTResult cfg(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return boardService.cfg(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}")
	public RESTResult list(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return boardService.list(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}/write")
	public RESTResult write(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return boardService.write(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessBoardWrite(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/{code}/{idx}")
	public RESTResult view(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.view(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}/{idx}/favorite")
	public RESTResult favorite(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.favorite(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}/{idx}/recommend")
	public RESTResult recommend(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.recommend(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}/{idx}/modify")
	public RESTResult modify(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.modify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	@RequestMapping("/{code}/{idx}/count")
	public RESTResult count(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.modifyCount(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	@RequestMapping("/{code}/{idx}/banner")
	public RESTResult banner(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.modifyBanner(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	@RequestMapping("/{code}/{idx}/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.delete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}/{idx}/reply")
	public RESTResult reply(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.reply(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessBoardWrite(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/{code}/{idx}/cert")
	public RESTResult cert(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return boardService.cert(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/{code}/{cidx}/comment")
	public RESTResult comment(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int cidx) throws Exception {
		try {
			param.put("code", code);
			param.put("cidx", cidx);
			return boardService.comment(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				String mode = StringUtil.getContent(param.get("mode"));
				if ("write".equals(mode) || "reply".equals(mode)) {
					commonService.accessBoardCommentWrite(request, param);
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/{code}/upload")
	public RESTResult upload(MultipartHttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @RequestParam MultipartFile file) throws Exception {
		try {
			param.put("code", code);
            return resourceService.apiUploadBoard(request, param, file);
        } catch (Exception e) {
        	return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
        } finally {
			try {
				commonService.accessBoardUpload(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping(value="/{code}/upload/dropzone", method=RequestMethod.POST)
	public RESTResult uploadDropzone(MultipartHttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
            return resourceService.apiUploadBoardDropzone(request, param);
        } catch (Exception e) {
        	return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
        } finally {
			try {
				commonService.accessBoardUpload(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping(value="/{code}/download/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> download(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.resourceBoardDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} finally {
			try {
				commonService.accessBoardDownload(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	
	@RequestMapping("/popup")
	public RESTResult popup(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return boardService.popup(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
