package kr.dmon.wb.web.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.CommonService;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.service.api.APIService;

@RestController
@RequestMapping("/api")
public class APIController {
	
	public static Logger logger = Logger.getLogger(APIController.class);

	@Autowired
	CommonService commonService;
	
	@Autowired
	APIService apiService;
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping
	public RESTResult index(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@RequestMapping("/notice")
	public RESTResult notice(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.notice(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/check/id")
	public RESTResult checkId(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.checkId(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/check/nickname")
	public RESTResult checkNickname(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.checkNickname(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/find/id")
	public RESTResult findId(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.findId(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/find/password")
	public RESTResult findPassword(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.findPassword(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/join")
	public RESTResult join(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.join(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessJoin(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/join/society")
	public RESTResult joinSociety(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.joinSociety(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessJoin(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/request/cert/sms")
	public RESTResult requestCertSms(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.requestCertSMS(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	@RequestMapping("/profile/upload")
	public RESTResult profileUpload(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return apiService.profileUpload(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/profile/upload/temp")
	public RESTResult profileUploadTemp(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return apiService.profileUploadTemp(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/login")
	public RESTResult login(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.login(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessLogin(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/login/society")
	public RESTResult loginSociety(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.loginSociety(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessLogin(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/login/{sns}")
	public RESTResult loginNaver(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String sns) throws Exception {
		try {
			param.put("sns", sns);
			return apiService.loginSNS(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessLogin(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/logout")
	public RESTResult logout(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		HttpSession session = request.getSession();
		if (session != null) {
			session.invalidate();
		}
		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK);
	}
	
	@RequestMapping("/myinfo")
	public RESTResult myinfo(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.myinfo(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/myinfo/exit")
	public RESTResult myinfoExit(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.myinfoExit(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/myinfo/update")
	public RESTResult myinfoUpdate(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.myinfoUpdate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/myinfo/update/password")
	public RESTResult myinfoUpdatePassword(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.myinfoUpdatePassword(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/myinfo/update/society")
	public RESTResult myinfoUpdateSociety(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.myinfoUpdateSociety(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/all")
	public RESTResult searchAll(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.searchAll(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/search/keyword")
	public RESTResult searchKeyword(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.searchKeyword(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	@RequestMapping("/sendmail")
	public RESTResult sendmail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.sendmail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/refuse/sms")
	public RESTResult refuseSms(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.refuseSms(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/refuse/email")
	public RESTResult refuseEmail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.refuseEmail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/accept/sms")
	public RESTResult acceptSms(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.acceptSms(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/accept/email")
	public RESTResult acceptEmail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.acceptEmail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/store")
	public RESTResult storeList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiService.storeList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/summernote/upload")
	public RESTResult summernoteUpload(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return apiService.summernoteUpload(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/summernote/{name:.+}")
	public ResponseEntity<byte[]> download(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String name) throws Exception {
		try {
			param.put("name", name);
			return resourceService.summernoteDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	
	
	
	// for MIRCERA
	/*
	@RequestMapping("/drug")
	public RESTResult drug(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		
		List<Map<String, Object>> mircera = new ArrayList<Map<String, Object>>(){{
			add(new HashMap<String, Object>(){{put("volume", 50); put("cost", 70735); put("medicine", "mircera"); put("idx", 2);}});
			add(new HashMap<String, Object>(){{put("volume", 75); put("cost", 92808); put("medicine", "mircera"); put("idx", 3);}});
			add(new HashMap<String, Object>(){{put("volume", 100); put("cost", 109788); put("medicine", "mircera"); put("idx", 4);}});
			add(new HashMap<String, Object>(){{put("volume", 120); put("cost", 120784); put("medicine", "mircera"); put("idx", 5);}});
			add(new HashMap<String, Object>(){{put("volume", 150); put("cost", 136050); put("medicine", "mircera"); put("idx", 6);}});
			add(new HashMap<String, Object>(){{put("volume", 200); put("cost", 160856); put("medicine", "mircera"); put("idx", 7);}});
			add(new HashMap<String, Object>(){{put("volume", 250); put("cost", 188153); put("medicine", "mircera"); put("idx", 8);}});
			add(new HashMap<String, Object>(){{put("volume", 360); put("cost", 244092); put("medicine", "mircera"); put("idx", 9);}});
		}};
		List<Map<String, Object>> nesp = new ArrayList<Map<String, Object>>(){{
			add(new HashMap<String, Object>(){{put("volume", 20); put("cost", 25019); put("medicine", "nesp"); put("idx", 11);}});
			add(new HashMap<String, Object>(){{put("volume", 30); put("cost", 31274); put("medicine", "nesp"); put("idx", 12);}});
			add(new HashMap<String, Object>(){{put("volume", 40); put("cost", 36590); put("medicine", "nesp"); put("idx", 13);}});
			add(new HashMap<String, Object>(){{put("volume", 60); put("cost", 45738); put("medicine", "nesp"); put("idx", 14);}});
			add(new HashMap<String, Object>(){{put("volume", 120); put("cost", 68607); put("medicine", "nesp"); put("idx", 15);}});
		}};
		List<Map<String, Object>> epokine = new ArrayList<Map<String, Object>>(){{
			add(new HashMap<String, Object>(){{put("volume", 1000); put("cost", 5316); put("medicine", "epokine"); put("idx", 21);}});
			add(new HashMap<String, Object>(){{put("volume", 2000); put("cost", 8644); put("medicine", "epokine"); put("idx", 22);}});
			add(new HashMap<String, Object>(){{put("volume", 3000); put("cost", 10227); put("medicine", "epokine"); put("idx", 23);}});
			add(new HashMap<String, Object>(){{put("volume", 4000); put("cost", 13576); put("medicine", "epokine"); put("idx", 24);}});
			add(new HashMap<String, Object>(){{put("volume", 6000); put("cost", 14812); put("medicine", "epokine"); put("idx", 25);}});
			add(new HashMap<String, Object>(){{put("volume", 8000); put("cost", 18297); put("medicine", "epokine"); put("idx", 26);}});
			add(new HashMap<String, Object>(){{put("volume", 10000); put("cost", 22252); put("medicine", "epokine"); put("idx", 27);}});
		}};
		List<Map<String, Object>> espogen = new ArrayList<Map<String, Object>>(){{
			add(new HashMap<String, Object>(){{put("volume", 1000); put("cost", 5303); put("medicine", "espogen"); put("idx", 41);}});
			add(new HashMap<String, Object>(){{put("volume", 2000); put("cost", 8583); put("medicine", "espogen"); put("idx", 42);}});
			add(new HashMap<String, Object>(){{put("volume", 3000); put("cost", 10294); put("medicine", "espogen"); put("idx", 43);}});
			add(new HashMap<String, Object>(){{put("volume", 4000); put("cost", 13631); put("medicine", "espogen"); put("idx", 44);}});
			add(new HashMap<String, Object>(){{put("volume", 6000); put("cost", 15554); put("medicine", "espogen"); put("idx", 45);}});
			add(new HashMap<String, Object>(){{put("volume", 8000); put("cost", 19270); put("medicine", "espogen"); put("idx", 46);}});
			add(new HashMap<String, Object>(){{put("volume", 10000); put("cost", 22167); put("medicine", "espogen"); put("idx", 47);}});
		}};
		List<Map<String, Object>> eporon = new ArrayList<Map<String, Object>>(){{
			add(new HashMap<String, Object>(){{put("volume", 1000); put("cost", 5255); put("medicine", "eporon"); put("idx", 51);}});
			add(new HashMap<String, Object>(){{put("volume", 2000); put("cost", 8549); put("medicine", "eporon"); put("idx", 52);}});
			add(new HashMap<String, Object>(){{put("volume", 3000); put("cost", 10083); put("medicine", "eporon"); put("idx", 53);}});
			add(new HashMap<String, Object>(){{put("volume", 4000); put("cost", 13280); put("medicine", "eporon"); put("idx", 54);}});
			add(new HashMap<String, Object>(){{put("volume", 5000); put("cost", 14383); put("medicine", "eporon"); put("idx", 55);}});
			add(new HashMap<String, Object>(){{put("volume", 6000); put("cost", 15554); put("medicine", "eporon"); put("idx", 68);}});
			add(new HashMap<String, Object>(){{put("volume", 10000); put("cost", 22097); put("medicine", "eporon"); put("idx", 56);}});
		}};
		List<Map<String, Object>> eposis = new ArrayList<Map<String, Object>>(){{
			add(new HashMap<String, Object>(){{put("volume", 2000); put("cost", 8542); put("medicine", "eposis"); put("idx", 61);}});
			add(new HashMap<String, Object>(){{put("volume", 3000); put("cost", 10239); put("medicine", "eposis"); put("idx", 62);}});
			add(new HashMap<String, Object>(){{put("volume", 4000); put("cost", 12145); put("medicine", "eposis"); put("idx", 63);}});
			add(new HashMap<String, Object>(){{put("volume", 5000); put("cost", 12544); put("medicine", "eposis"); put("idx", 64);}});
			add(new HashMap<String, Object>(){{put("volume", 6000); put("cost", 14692); put("medicine", "eposis"); put("idx", 65);}});
			add(new HashMap<String, Object>(){{put("volume", 8000); put("cost", 17769); put("medicine", "eposis"); put("idx", 66);}});
			add(new HashMap<String, Object>(){{put("volume", 10000); put("cost", 20296); put("medicine", "eposis"); put("idx", 67);}});
		}};
		
		List<String> medicine_names = new ArrayList<>();
		medicine_names.add("Epokine");
		medicine_names.add("Espogen");
		medicine_names.add("Eporon");
		medicine_names.add("Eposis");
		
		Map<String, Object> medicine_data = new HashMap<>();
		medicine_data.put("Epokine", epokine);
		medicine_data.put("Espogen", espogen);
		medicine_data.put("Eporon", eporon);
		medicine_data.put("Eposis", eposis);

		Map<String, Object> etc = new HashMap<>();
		etc.put("names", medicine_names);
		etc.put("datas", medicine_data);
		
		
		Map<String, Object> ret = new HashMap<>();
		ret.put("MIRCERA", mircera);
		ret.put("NESP", nesp);
		ret.put("ETC", etc);

		return new RESTResult(RESTResult.SUCCESS, RESTResult.OK, ret);
	}
	*/
}
