package kr.dmon.wb.web.api.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.EventService;
import kr.dmon.wb.service.admin.AdminService;

@RestController
@RequestMapping("/api/admin/event")
public class APIAdminEventController {

	public static Logger logger = Logger.getLogger(APIAdminEventController.class);

	@Autowired
	AdminService adminService;
	
	@Autowired
	EventService eventService;
	
	@RequestMapping
	public RESTResult list(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/create")
	public RESTResult create(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/modify")
	public RESTResult modify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image) throws Exception {
		try {
			return eventService.apiAdminModify(request, param, image);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/status")
	public RESTResult status(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search")
	public RESTResult search(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminSearch(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/page")
	public RESTResult pageList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPageList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/page/create")
	public RESTResult pageCreate(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return eventService.apiAdminPageCreate(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/page/modify")
	public RESTResult pageModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return eventService.apiAdminPageModify(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/page/delete")
	public RESTResult pageDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPageDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/banner")
	public RESTResult bannerList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminBannerList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/banner/add")
	public RESTResult bannerAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminBannerAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/banner/delete")
	public RESTResult bannerDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminBannerDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/banner/status")
	public RESTResult bannerStatus(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminBannerStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan")
	public RESTResult planList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiPlanAdminList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/create")
	public RESTResult planCreate(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPlanCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/modify")
	public RESTResult planModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile image) throws Exception {
		try {
			return eventService.apiAdminPlanModify(request, param, image);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/delete")
	public RESTResult planDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPlanDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/status")
	public RESTResult planStatus(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPlanStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/search")
	public RESTResult planSearch(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPlanSearch(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/page")
	public RESTResult planPageList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPlanPageList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/page/add")
	public RESTResult planPageAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPlanPageAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/plan/page/delete")
	public RESTResult planPageDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminPlanPageDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping("/new")
	public RESTResult newList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminNewList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/new/add")
	public RESTResult newAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminNewAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/new/delete")
	public RESTResult newDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminNewDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/new/status")
	public RESTResult newStatus(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminNewStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/week")
	public RESTResult weekList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminWeekList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/week/add")
	public RESTResult weekAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminWeekAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/week/delete")
	public RESTResult weekDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminWeekDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/week/status")
	public RESTResult weekStatus(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return eventService.apiAdminWeekStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
