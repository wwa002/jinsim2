package kr.dmon.wb.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kr.dmon.wb.service.TestService;

@Controller
@RequestMapping("/test")
public class TestController {
	
	private static Logger logger = Logger.getLogger(MainController.class);

	@Autowired
	TestService testService;
	
	@RequestMapping("/{view}")
	public String test(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String view) throws Exception {
		testService.test();
		return "/test/"+view;
	}
	/*
	
	@RequestMapping(method=RequestMethod.POST)
	public String post(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		testService.saveTest(param);
		return "redirect:/test";
	}
	*/
	
}
