package kr.dmon.wb.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.service.StoreService;

@RestController
@RequestMapping("/api/store")
public class APIStoreController {

	@Autowired
	ResourceService resourceService;
	
	@Autowired
	StoreService storeService;
	
	@RequestMapping("/search")
	public RESTResult searchHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return storeService.apiSearch(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/{sidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> storePhoto(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int sidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("sidx", sidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.storePhotoDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
}
