package kr.dmon.wb.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.dmon.wb.exceptions.RequireLoginException;
import kr.dmon.wb.service.CommonService;
import kr.dmon.wb.service.MainService;
import kr.dmon.wb.service.ResourceService;

@Controller
public class MainController {

	private static Logger logger = Logger.getLogger(MainController.class);

	@Autowired
	CommonService commonService;
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	MainService mainService;
	
	@RequestMapping(value = { "", "/" })
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return mainService.index(request, param, model);
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping(value = { "/robot.txt", "/robots.txt" })
	public ResponseEntity<byte[]> robots(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return mainService.robots(request, response);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(e.getMessage().getBytes(), headers, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			HttpSession session = request.getSession();
			if (session != null) {
				session.invalidate();
			}
			return "redirect:/";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping({"/site/{code1}", "/site/{code1}/{code2}", "/site/{code1}/{code2}/{code3}"})
	public String content(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3) throws Exception {
		try {
			param.put("type", "SITE");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			return mainService.content(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping({"/js/{code1}/{name:.+}", "/js/{code1}/{code2}/{name:.+}", "/js/{code1}/{code2}/{code3}/{name:.+}"})
	public ResponseEntity<byte[]> etcJS(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3, @PathVariable String name) throws Exception {
		try {
			param.put("type", "JS");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			param.put("filename", name);
			return mainService.etcResource(request, response, param, model);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping({"/css/{code1}/{name:.+}", "/css/{code1}/{code2}/{name:.+}", "/css/{code1}/{code2}/{code3}/{name:.+}"})
	public ResponseEntity<byte[]> etcCSS(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3, @PathVariable String name) throws Exception {
		try {
			param.put("type", "CSS");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			param.put("filename", name);
			return mainService.etcResource(request, response, param, model);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping({"/xml/{code1}/{name:.+}", "/xml/{code1}/{code2}/{name:.+}", "/xml/{code1}/{code2}/{code3}/{name:.+}"})
	public ResponseEntity<byte[]> etcXML(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3, @PathVariable String name) throws Exception {
		try {
			param.put("type", "XML");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			param.put("filename", name);
			return mainService.etcResource(request, response, param, model);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping({"/app/{code1}", "/app/{code1}/{code2}", "/app/{code1}/{code2}/{code3}"})
	public String app(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code1, @PathVariable(required=false) String code2, @PathVariable(required=false) String code3) throws Exception {
		try {
			param.put("type", "APP");
			param.put("code1", code1);
			param.put("code2", code2);
			param.put("code3", code3);
			return mainService.content(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/board/{code}")
	public String boardList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return mainService.boardList(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/board/{code}/write")
	public String boardWrite(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return mainService.boardWrite(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/board/{code}/{idx}")
	public String boardView(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return mainService.boardView(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/board/{code}/{idx}/modify")
	public String boardModify(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return mainService.boardModify(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/board/{code}/{idx}/reply")
	public String boardReply(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return mainService.boardReply(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/board/{code}/{idx}/cert")
	public String boardCert(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return mainService.boardCert(request, param, model);
		} catch (RequireLoginException e) {
			if ("KORSIS".equals(mainService.getSiteSession(request).getSitecd())) {
				return "redirect:/site/member/login?url="+request.getRequestURI();
			} else {
				model.addAttribute("message", e.getMessage());
				return "/switch/historyBackWithMessage";
			}
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/refuse/sms")
	public String refuseSms(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return mainService.refuseSms(request, param, model);
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping("/refuse/email")
	public String refuseEmail(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return mainService.refuseEmail(request, param, model);
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		} finally {
			try {
				commonService.accessAnalysis(request, param);
			} catch (Exception e) {
				logger.error(e);
			}
		}
	}
	
	@RequestMapping(value="/mail/{tidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> mailFile(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int tidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("tidx", tidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.mailFileDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
}
