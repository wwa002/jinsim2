package kr.dmon.wb.web.api.builder;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.builder.BuilderService;

@RestController
@RequestMapping("/api/builder/template")
public class APIBuilderTemplateController {
	
	public static Logger logger = Logger.getLogger(APIBuilderTemplateController.class);
	
	@Autowired
	BuilderService builderService;

	@RequestMapping("/create")
	public RESTResult create(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiTemplateCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/copy")
	public RESTResult copy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiTemplateCopy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/modify")
	public RESTResult modify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiTemplateModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiTemplateDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/main")
	public RESTResult main(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return builderService.apiTemplateMain(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
