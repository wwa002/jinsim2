package kr.dmon.wb.web.api.builder;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.ResourceService;

@RestController
@RequestMapping("/api/builder/resource")
public class APIBuilderResourceController {
	public static Logger logger = Logger.getLogger(APIBuilderResourceController.class);
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping("/jstree")
	public JSONObject jstree(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiJSTree(request, param);
		} catch (Exception e) {
			return new JSONObject();
		}
	}
	
	@RequestMapping("/folder/list")
	public RESTResult forderList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiFolderList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/folder/add")
	public RESTResult folderAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiFolderAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return resourceService.apiDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/upload/dropzone", method=RequestMethod.POST)
	public RESTResult uploadDropzone(MultipartHttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
            return resourceService.apiUploadDropzone(request, param);
        } catch (Exception e) {
        	return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
        }
	}
}
