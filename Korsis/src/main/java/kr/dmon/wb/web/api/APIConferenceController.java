package kr.dmon.wb.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.ConferenceService;
import kr.dmon.wb.service.ResourceService;

@RestController
@RequestMapping("/api/conference")
public class APIConferenceController {

	public static Logger logger = Logger.getLogger(APIConferenceController.class);
	
	@Autowired
	ResourceService resourceService;
	
	@Autowired
	ConferenceService conferenceService;
	
	@RequestMapping("/register")
	public RESTResult register(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiRegister(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/register/check")
	public RESTResult registerCheck(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiRegisterCheck(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/list")
	public RESTResult notice(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiSessionList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/lecture")
	public RESTResult lecture(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiSessionLecture(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/vote")
	public RESTResult vote(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiSessionVote(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/question")
	public RESTResult question(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return conferenceService.apiSessionQuestion(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/session/{code}/{idx}")
	public RESTResult sessionItem(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return conferenceService.apiSessionItem(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/session/download/{code}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> eventPage(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.sessionDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
}
