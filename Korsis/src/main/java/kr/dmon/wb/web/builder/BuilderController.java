package kr.dmon.wb.web.builder;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.service.builder.BuilderService;

@Controller
@RequestMapping("/builder")
public class BuilderController {

	public static Logger logger = Logger.getLogger(BuilderController.class);
	
	@Autowired
	BuilderService builderService;
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		return "redirect:/builder/dashboard";
	}
	
	@RequestMapping("/login")
	public String login(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			return builderService.login(request, param, model);
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/dashboard")
	public String dashboard(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.dashboard(request, param, model);
			return "/builder/dashboard";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/system/setting")
	public String system(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.systemSetting(request, param, model);
			return "/builder/system/setting";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/system/member/list")
	public String systemMemberList(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.systemMemberList(request, param, model);
			return "/builder/system/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/system/member/grade")
	public String systemMemberGrade(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.systemMemberGrade(request, param, model);
			return "/builder/system/grade";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/system/robot")
	public String systemRobot(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.systemRobot(request, param, model);
			return "/builder/system/robot";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/resources")
	public String resources(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.resources(request, param, model);
			return "/builder/resources";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping(value="/resources/download/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> resourcesDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.resourceDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/resources/profile/{id:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> resourcesProfile(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String id) throws Exception {
		try {
			param.put("id", id);
			return resourceService.resourceProfile(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping("/templates")
	public String templates(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.templates(request, param, model);
			return "/builder/templates/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/templates/{idx}")
	public String templateEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			builderService.templateEdit(request, param, model);
			return "/builder/templates/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/boards")
	public String boards(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.boards(request, param, model);
			return "/builder/boards/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/boards/{code}")
	public String boardEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			builderService.boardEdit(request, param, model);
			return "/builder/boards/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/workshop")
	public String workshops(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.workshop(request, param, model);
			return "/builder/society/workshop/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/society/workshop/{code}")
	public String workshopEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			builderService.workshopEdit(request, param, model);
			return "/builder/society/workshop/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/contents")
	public String contents(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.contents(request, param, model);
			return "/builder/contents/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/contents/{idx}")
	public String contentEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			builderService.contentsEdit(request, param, model);
			return "/builder/contents/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/etc")
	public String etc(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.etc(request, param, model);
			return "/builder/etc/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/etc/{idx}")
	public String etcEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable int idx) throws Exception {
		try {
			param.put("idx", idx);
			builderService.etcEdit(request, param, model);
			return "/builder/etc/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/conference")
	public String conference(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		try {
			builderService.conference(request, param, model);
			return "/builder/conference/list";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
	
	@RequestMapping("/conference/{code}")
	public String conferenceEdit(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			builderService.conferenceEdit(request, param, model);
			return "/builder/conference/editor";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "/switch/historyBackWithMessage";
		}
	}
}
