package kr.dmon.wb.web.api.admin;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.service.SocietyService;

@RestController
@RequestMapping("/api/admin/society")
public class APIAdminSocietyController {

	public static Logger logger = Logger.getLogger(APIAdminSocietyController.class);

	@Autowired
	ResourceService resourceService;
	
	@Autowired
	SocietyService societyService;
	
	@RequestMapping("/member/list")
	public RESTResult memberList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminMemberList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/excel")
	public ModelAndView memberExcel(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		return societyService.apiAdminMemberExcel(request, param);
	}

	@RequestMapping("/member/type")
	public RESTResult memberType(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminMemberType(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/confirm")
	public RESTResult memberConfirm(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminMemberConfirm(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/recovery")
	public RESTResult memberRecovery(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminMemberRecovery(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/save/basic")
	public RESTResult memberSaveBasic(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminMemberSaveBasic(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/save/society")
	public RESTResult memberSaveSociety(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminMemberSaveSociety(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/save/agree")
	public RESTResult memberSaveAgree(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminMemberSaveAgree(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/member/save/permission")
	public RESTResult memberSavePermission(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) String[] permission) throws Exception {
		try {
			if (permission == null || permission.length == 0) {
				param.put("permission", null);
			} else {
				param.put("permission", String.join(",", permission));
			}
			
			return societyService.apiAdminMemberSavePermission(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/write")
	public RESTResult workshopWrite(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @RequestParam(required=false) MultipartFile banner) throws Exception {
		try {
			param.put("code", code);
			return societyService.apiAdminWorkshopWrite(request, param, banner, null);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/workshop/{code}/write/korsis")
	public RESTResult workshopWriteKorsis(HttpServletRequest request, @RequestParam Map<String, Object> param
			, @PathVariable String code, @RequestParam(required=false) MultipartFile banner
			, @RequestParam(required=false) String[] titles
			, @RequestParam(required=false) String[] types
			, @RequestParam(required=false) String[] costs1
			, @RequestParam(required=false) String[] costs2
			, @RequestParam(required=false) String[] limiteds
			, @RequestParam(required=false) String[] enddates
			, @RequestParam(required=false) String[] courses1
			, @RequestParam(required=false) String[] courses1_limited
			, @RequestParam(required=false) String[] courses2
			, @RequestParam(required=false) String[] courses2_limited
			) throws Exception {
		try {
			param.put("code", code);
			
			Map<String, String[]> lectures = new HashMap<>();
			lectures.put("titles", titles);
			lectures.put("types", types);
			lectures.put("costs1", costs1);
			lectures.put("costs2", costs2);
			lectures.put("limiteds", limiteds);
			lectures.put("enddates", enddates);
			lectures.put("courses1", courses1);
			lectures.put("courses1_limited", courses1_limited);
			lectures.put("courses2", courses2);
			lectures.put("courses2_limited", courses2_limited);
			
			return societyService.apiAdminWorkshopWrite(request, param, banner, lectures);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		} 
	}
	
	@RequestMapping("/workshop/{code}/{idx}/modify")
	public RESTResult workshopModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx, @RequestParam(required=false) MultipartFile banner) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return societyService.apiAdminWorkshopModify(request, param, banner, null);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/modify/korsis")
	public RESTResult workshopModifyKorsis(HttpServletRequest request, @RequestParam Map<String, Object> param
			, @PathVariable String code, @PathVariable int idx, @RequestParam(required=false) MultipartFile banner
			, @RequestParam(required=false) String[] titles
			, @RequestParam(required=false) String[] types
			, @RequestParam(required=false) String[] costs1
			, @RequestParam(required=false) String[] costs2
			, @RequestParam(required=false) String[] limiteds
			, @RequestParam(required=false) String[] enddates
			, @RequestParam(required=false) String[] courses1
			, @RequestParam(required=false) String[] courses1_limited
			, @RequestParam(required=false) String[] courses2
			, @RequestParam(required=false) String[] courses2_limited
			) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			
			Map<String, String[]> lectures = new HashMap<>();
			lectures.put("titles", titles);
			lectures.put("types", types);
			lectures.put("costs1", costs1);
			lectures.put("costs2", costs2);
			lectures.put("limiteds", limiteds);
			lectures.put("enddates", enddates);
			lectures.put("courses1", courses1);
			lectures.put("courses1_limited", courses1_limited);
			lectures.put("courses2", courses2);
			lectures.put("courses2_limited", courses2_limited);
			
			return societyService.apiAdminWorkshopModify(request, param, banner, lectures);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/banner/delete")
	public RESTResult workshopBannerDelete(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return societyService.apiAdminWorkshopBannerDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/delete")
	public RESTResult workshopDelete(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return societyService.apiAdminWorkshopDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{idx}/status")
	public RESTResult workshopStatus(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("idx", idx);
			return societyService.apiAdminWorkshopStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{widx}/{idx}")
	public RESTResult workshopRegistrationItem(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int widx, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", widx);
			param.put("idx", idx);
			return societyService.apiAdminWorkshopRegistrationItem(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{widx}/{idx}/modify")
	public RESTResult workshopRegistrationItemModify(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int widx, @PathVariable int idx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", widx);
			param.put("idx", idx);
			return societyService.apiAdminWorkshopRegistrationItemModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/registration")
	public RESTResult workshopRegistration(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
			return societyService.apiAdminWorkshopRegistrationListAll(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{widx}/registration")
	public RESTResult workshopRegistration(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int widx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", widx);
			return societyService.apiAdminWorkshopRegistrationList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{widx}/registration/excel")
	public ModelAndView workshopRegistrationExcel(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int widx) throws Exception {
		param.put("code", code);
		param.put("widx", widx);
		return societyService.apiAdminWorkshopRegistrationExcel(request, param);
	}
	
	@RequestMapping("/workshop/{code}/{widx}/registration/status")
	public RESTResult workshopRegistrationStatus(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int widx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", widx);
			return societyService.apiAdminWorkshopRegistrationStatus(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/workshop/{code}/{widx}/registration/delete")
	public RESTResult workshopRegistrationDelete(HttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code, @PathVariable int widx) throws Exception {
		try {
			param.put("code", code);
			param.put("widx", widx);
			return societyService.apiAdminWorkshopRegistrationDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/workshop/{code}/upload/dropzone", method=RequestMethod.POST)
	public RESTResult uploadDropzone(MultipartHttpServletRequest request, @RequestParam Map<String, Object> param, @PathVariable String code) throws Exception {
		try {
			param.put("code", code);
            return resourceService.apiUploadWorkshopDropzone(request, param);
        } catch (Exception e) {
        	return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
        }
	}
	
	@RequestMapping("/hospital/list")
	public RESTResult hospitalList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminHospitalList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/add")
	public RESTResult hospitalAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminHospitalAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/item")
	public RESTResult hospitalItem(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminHospitalItem(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/modify")
	public RESTResult hospitalModify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminHospitalModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/remove")
	public RESTResult hospitalRemove(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return societyService.apiAdminHospitalRemove(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
