package kr.dmon.wb.web;

import java.io.File;
import java.nio.file.Files;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.utils.StringUtil;
import nl.captcha.Captcha;
import nl.captcha.backgrounds.GradiatedBackgroundProducer;
import nl.captcha.servlet.CaptchaServletUtil;

@Controller
@RequestMapping("/res")
public class ResController {

	private static Logger logger = Logger.getLogger(ResController.class);
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping(value={"/{dir1}/{name:.+}", "/{dir1}/{dir2}/{name:.+}", "/{dir1}/{dir2}/{dir3}/{name:.+}"}, method=RequestMethod.GET)
	public ResponseEntity<byte[]> resourcesDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String dir1, @PathVariable(required=false) String dir2, @PathVariable(required=false) String dir3, @PathVariable String name) throws Exception {
		try {
			if (StringUtil.isNumeric(dir1)) {
				param.put("idx", dir1);
				param.put("name", name);
				return resourceService.resourceFrontDownload(request, response, param);
			} else {
				param.put("dir1", dir1);
				param.put("dir2", dir2);
				param.put("dir3", dir3);
				param.put("name", name);
				return resourceService.resourceFrontDownloadDir(request, response, param);
			}
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/profile/{id:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> resourcesProfile(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String id) throws Exception {
		try {
			param.put("id", id);
			return resourceService.resourceFrontProfile(request, response, param);
		} catch (Exception e) {
			String icon = request.getSession().getServletContext().getRealPath("/")+"resources"+File.separator+"images"+File.separator+"user.png";
			if (resourceService.getSiteSession(request).getSitecd().equals("PPPP")) {
				icon = request.getSession().getServletContext().getRealPath("/")+"resources/modules/pppp"+File.separator+"images/app"+File.separator+"ic_pppp.png";
			}
			File file = new File(icon);
			if (file.exists()) {
				try {
					byte[] bytes = Files.readAllBytes(file.toPath());
					HttpHeaders headers = new HttpHeaders();
					headers.setContentLength(bytes.length);
					headers.setContentType(MediaType.IMAGE_PNG);
					response.setContentType(MediaType.IMAGE_PNG_VALUE);
					return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
				} catch (Exception e2) {
					logger.error(e2);
				}
			}
			 
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/profile/temp/{filename:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> resourcesTempProfile(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable String filename) throws Exception {
		try {
			param.put("filename", filename);
			return resourceService.resourceProfileTemp(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping("/captcha")
	public void captcha(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param) throws Exception {
		try {
			int width = !StringUtil.isEmpty(param.get("width")) ? Integer.parseInt(StringUtil.getContent("width")) : 150;
			int height = !StringUtil.isEmpty(param.get("height")) ? Integer.parseInt(StringUtil.getContent("height")) : 35;
			Captcha captcha = new Captcha.Builder(width, height).addText().addBackground(new GradiatedBackgroundProducer()).addNoise().addBorder().build();
			if (!StringUtil.isEmpty(param.get("idx"))) {
				request.getSession().setAttribute("captchaAnswer_"+StringUtil.getContent(param.get("idx")), captcha.getAnswer());
			} else {
				request.getSession().setAttribute("captchaAnswer", captcha.getAnswer());
			}
			
			response.setHeader("Cache-Control", "no-store");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setContentType("image/jpeg");
			
			CaptchaServletUtil.writeImage(response, captcha.getImage());
		} catch (Exception e) {
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="/mail/read/{sidx}/{idx}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> mailRead(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int sidx, @PathVariable int idx) throws Exception {
		try {
			param.put("sidx", sidx);
			param.put("idx", idx);
			resourceService.mailRead(request, response, param);
		} catch (Exception e) {
			logger.error(e);
		}
		
		String icon = request.getSession().getServletContext().getRealPath("/")+"resources"+File.separator+"images"+File.separator+"readmail.png";
		File file = new File(icon);
		if (file.exists()) {
			try {
				byte[] bytes = Files.readAllBytes(file.toPath());
				HttpHeaders headers = new HttpHeaders();
				headers.setContentLength(bytes.length);
				headers.setContentType(MediaType.IMAGE_PNG);
				response.setContentType(MediaType.IMAGE_PNG_VALUE);
				return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
			} catch (Exception e) {
				logger.error(e);
			}
		} 
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.TEXT_PLAIN);
		return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
	}
}
