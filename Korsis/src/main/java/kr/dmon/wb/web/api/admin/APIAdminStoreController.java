package kr.dmon.wb.web.api.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.admin.AdminService;

@RestController
@RequestMapping("/api/admin/store")
public class APIAdminStoreController {

	public static Logger logger = Logger.getLogger(APIAdminStoreController.class);
	
	@Autowired
	AdminService adminService;
	
	@RequestMapping("/list")
	public RESTResult list(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/managers")
	public RESTResult managers(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreManagers(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/manager/list")
	public RESTResult managerList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreManagerList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/manager/add")
	public RESTResult managerAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreManagerAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/manager/delete")
	public RESTResult managerDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreManagerDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospitals")
	public RESTResult hospitals(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreHospitals(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/list")
	public RESTResult hospitalList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreHospitalList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/add")
	public RESTResult hospitalAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreHospitalAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/hospital/delete")
	public RESTResult hospitalDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreHospitalDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/create")
	public RESTResult create(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreCreate(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/modify")
	public RESTResult modify(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreModify(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/photo/add")
	public RESTResult photoAdd(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return adminService.apiStorePhotoAdd(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/photo/list")
	public RESTResult photoList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStorePhotoList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/photo/remove")
	public RESTResult photoRemove(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStorePhotoRemove(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}

	@RequestMapping("/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiStoreDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
