package kr.dmon.wb.web.api;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.ResourceService;
import kr.dmon.wb.service.api.APIDBService;

@RestController
@RequestMapping("/api/db")
public class APIDBController {

	public static Logger logger = Logger.getLogger(APIDBController.class);
	
	@Autowired
	APIDBService apiDBService; 
	
	@Autowired
	ResourceService resourceService;
	
	@RequestMapping("/search/map/store")
	public RESTResult searchMapStore(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiSearchMapStore(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/medical")
	public RESTResult searchMedical(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiSearchMedical(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital")
	public RESTResult searchHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiSearchHospital(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital/subject")
	public RESTResult searchHospitalSubject(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiSearchHospitalSubject(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital/subject2")
	public RESTResult searchHospitalSubject2(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiSearchHospitalSubject2(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/hospital/theme")
	public RESTResult searchHospitalTheme(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiSearchHospitalTheme(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/search/pharmacy")
	public RESTResult searchPharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiSearchPharmacy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/hospital")
	public RESTResult listHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListHospital(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/hospital/subject")
	public RESTResult listHospitalSubject(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListHospitalSubject(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/hospital/type")
	public RESTResult listHospitalType(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListHospitalType(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/hospital/special")
	public RESTResult listHospitalSpecial(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListHospitalSpecial(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/hospital/local")
	public RESTResult listHospitalLocal(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListHospitalLocal(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/pharmacy")
	public RESTResult listPharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListPharmacy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/infodb")
	public RESTResult infodbList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListInfodb(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list/cast")
	public RESTResult castList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiListCast(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/information/hospital")
	public RESTResult informationHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiInformationHospital(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/information/pharmacy")
	public RESTResult informationPharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiInformationPharmacy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/comment/hospital")
	public RESTResult commentHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiCommentHospital(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/comment/pharmacy")
	public RESTResult commentPharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiCommentPharmacy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/rating/hospital")
	public RESTResult ratingHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiRatingHospital(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/rating/pharmacy")
	public RESTResult ratingPharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiRatingPharmacy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/favorite/hospital")
	public RESTResult favoriteHospital(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiFavoriteHospital(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/favorite/pharmacy")
	public RESTResult favoritePharmacy(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiFavoritePharmacy(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/information/infodb")
	public RESTResult InformationInfodb(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiInformationInfodb(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/information/infodb/detail")
	public RESTResult InformationInfodbDetail(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiInformationInfodbDetail(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/information/cast")
	public RESTResult InformationCast(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiInformationCast(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/cast/favorite")
	public RESTResult castFavorite(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiCastFavorite(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	@RequestMapping("/cast/recommend")
	public RESTResult castRecommend(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiCastRecommend(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	@RequestMapping("/infodb/favorite")
	public RESTResult infodbFavorite(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiInfodbFavorite(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	@RequestMapping("/infodb/recommend")
	public RESTResult infodbRecommend(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return apiDBService.apiInfodbRecommend(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping(value="/cast/icon/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> castIcon(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.castIconDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/cast/background/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> castBackground(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.castBackgroundDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/cast/page/{cidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> castPage(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int cidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("cidx", cidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.castPageDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}

	@RequestMapping(value="/info/icon/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> infoIcon(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.infoIconDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/info/content/icon/{dbidx}/{dbgidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> infoContentIcon(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int dbidx, @PathVariable int dbgidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("dbidx", dbidx);
			param.put("dbgidx", dbgidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.infoContentIconDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/info/content/background/{dbidx}/{dbgidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> infoContentBackground(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int dbidx, @PathVariable int dbgidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("dbidx", dbidx);
			param.put("dbgidx", dbgidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.infoContentBackgroundDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/info/content/page/{dbidx}/{dbgidx}/{dbcidx}/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> infoContentBackground(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int dbidx, @PathVariable int dbgidx, @PathVariable int dbcidx, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("dbidx", dbidx);
			param.put("dbgidx", dbgidx);
			param.put("dbcidx", dbcidx);
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.infoContentPageDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping("/hospital/photo/upload")
	public RESTResult hospitalPhotoUpload(MultipartHttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam MultipartFile file) throws Exception {
		try {
            return apiDBService.apiHospitalPhotoUpload(request, param, file);
        } catch (Exception e) {
        	return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
        } 
	}
	
	@RequestMapping("/pharmacy/photo/upload")
	public RESTResult pharmacyPhotoUpload(MultipartHttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam MultipartFile file) throws Exception {
		try {
            return apiDBService.apiPharmacyPhotoUpload(request, param, file);
        } catch (Exception e) {
        	return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
        } 
	}

	@RequestMapping(value="/hospital/photo/download/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> hospitalPhotoDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.hospitalPhotoDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	@RequestMapping(value="/pharmacy/photo/download/{idx}/{name:.+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> pharmacyPhotoDownload(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param, @PathVariable int idx, @PathVariable String name) throws Exception {
		try {
			param.put("idx", idx);
			param.put("name", name);
			return resourceService.pharmacyPhotoDownload(request, response, param);
		} catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.TEXT_PLAIN);
			return new ResponseEntity<>(null, headers, HttpStatus.NOT_FOUND);
		} 
	}
	
	
}
