package kr.dmon.wb.web.app;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/app")
public class AppController {

	private static Logger logger = Logger.getLogger(AppController.class);
	
	
	@RequestMapping
	public String index(HttpServletRequest request, @RequestParam Map<String, Object> param, Model model) throws Exception {
		return "/app/index";
	}
}
