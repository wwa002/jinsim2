package kr.dmon.wb.web.api.admin;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import kr.dmon.wb.model.RESTResult;
import kr.dmon.wb.service.admin.AdminService;

@RestController
@RequestMapping("/api/admin/member")
public class APIAdminMemberController {

	public static Logger logger = Logger.getLogger(APIAdminMemberController.class);
	
	@Autowired
	AdminService adminService;
	
	@RequestMapping("/check/id")
	public RESTResult checkId(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiCheckId(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/list")
	public RESTResult list(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/info")
	public RESTResult info(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberInfo(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/save")
	public RESTResult save(HttpServletRequest request, @RequestParam Map<String, Object> param, @RequestParam(required=false) MultipartFile file) throws Exception {
		try {
			return adminService.apiMemberSave(request, param, file);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/delete")
	public RESTResult delete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/gcd/list")
	public RESTResult gcdList(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberGCDList(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/gcd/add")
	public RESTResult gcdAdd(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberGCDAdd(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
	@RequestMapping("/gcd/delete")
	public RESTResult gcdDelete(HttpServletRequest request, @RequestParam Map<String, Object> param) throws Exception {
		try {
			return adminService.apiMemberGCDDelete(request, param);
		} catch (Exception e) {
			return new RESTResult(RESTResult.FAILURE, e.getMessage(), RESTResult.ERROR);
		}
	}
	
}
