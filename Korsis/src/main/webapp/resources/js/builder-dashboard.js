$(function(){
	$("#site-list").dataTable({
		"dom":'<"text-right"f><t><"text-center"p>'
		, "ordering": true
		, "bLengthChange": false
		, "searching": true
		, "language" : {
            "paginate" : {"previous" : "<", "next" : ">"}
        },
		columns : [
			{data:"idx", "class":"text-center"}
			, {data:"sitecd", "class":"text-center"}
			, {data:"title", render : function(data, type, row){
				return '<a href="http://'+row.url+'" target="_blank">'+data+'</a>';
            }}
			, {data:"email", "class":"text-center", render : function(data, type, row){
				return '<a href="mailto:'+data+'">'+data+'</a>';
            }}
			, {data:"isused", "class":"text-center", render : function(data, type, row){
				if (data == 'Y') {
					return '<span class="label label-success">사용</span>'
				} else if (data == 'T') {
					return '<span class="label label-warning">테스트</span>'
				} else {
					return '<span class="label label-danger">사용중지</span>'
				}
            }}
			, {data:"regdate", "class":"text-center", render : function(data, type, row){
				 return getDefaultDataFormat(data);
            }}
			, {data:"idx", "class":"text-center", render : function(data, type, row){
				return '<button type="button" class="btn btn-warning btn-xs site-modify" data-idx="'+row.idx+'" data-title="'+row.title+'" data-email="'+row.email+'" data-isused="'+row.isused+'" data-type="'+row.type+'">수정</button>';
            }}
		              ]
		, ajax : {
			url : "/api/builder/site/list"
			, type : "POST"
			, data:function(d) {
				return $.extend({}, d, {});
			}
			, dataSrc:function(json) {
				console.log(json);
				return json.data;
			}
		}
	});
	
	$("#server-list").dataTable({
		"dom":'<"text-right"f><t><"text-center"p>'
		, "ordering": true
		, "bLengthChange": false
		, "info" : false
		, "searching": false
		, "language" : {
            "paginate" : {"previous" : "<", "next" : ">"}
        },
		columns : [
			{data:"idx", "class":"text-center"}
			, {data:"host", render : function(data, type, row){
				if (row.status == 'ON') {
					return '<span class="btn btn-circle btn-primary">ON</span> ' + data;
				} else {
					return '<span class="btn btn-circle btn-danger">OFF</span> ' + data;
				}
            }}
			, {data:"type", "class":"text-center", render : function(data, type, row){
				if (data == 'MAIN') {
					return data;
				} else {
					return '<button class="btn btn-primary btn-tomain" data-host="'+row.host+'">'+data+'</button>';
				}
            }}
			, {data:"mem_total", "class":"text-center"}
			, {data:"mem_free", "class":"text-center"}
			, {data:"mem_max", "class":"text-center"}
			, {data:"regdate", "class":"text-center", render : function(data, type, row){
				 return getDefaultDataFormat(data);
            }}
			, {data:"moddate", "class":"text-center", render : function(data, type, row){
				 return getDefaultDataFormat(data);
            }}
		              ]
		, ajax : {
			url : "/api/builder/server/list"
			, type : "POST"
			, data:function(d) {
				return $.extend({}, d, {});
			}
			, dataSrc:function(json) {
				console.log(json);
				return json.data;
			}
		}
	});
	
	$(document).on("click", ".site-modify", function(){
		$("#idx-modify").val($(this).data("idx"));
		$("#title-modify").val($(this).data("title"));
		$("#email-modify").val($(this).data("email"));
		$("#isused-modify").val($(this).data("isused"));
		$("#type-modify").val($(this).data("type"));
		$("#siteForm-wrapper").hide();
		$("#siteModifyForm-wrapper").show();
	});
	
	$("#sitecd-check").click(function(){
		var sitecd = $("#sitecd").val();
		$.ajax({type:"POST", url : "/api/builder/check/sitecd", cache:false
			, data : {sitecd:sitecd}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("사용할 수 있는 코드 입니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#site-create").click(function(){
		if (confirm("입력하신 정보로 생성하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/site/create", cache:false
				, data : $("#siteForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$("#site-modify-cancel").click(function(){
		$("#siteForm-wrapper").show();
		$("#siteModifyForm-wrapper").hide();
		return true;
	});
	
	$("#site-modify").click(function(){
		if (confirm("입력하신 정보로 수정하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/site/modify", cache:false
				, data : $("#siteModifyForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on("click", ".btn-tomain", function(){
		if (confirm('선택하신 서버를 메인서버로 지정하시겠습니까?')) {
			var host = $(this).data('host');
			$.ajax({type:"POST", url : "/api/builder/server/main", cache:false
				, data : {host:host}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});

		var dataset = [ {label : "접속자", data : data2, color : "#1ab394",
				bars : {show : true, align : "center", barWidth : 24 * 60 * 60 * 600, lineWidth : 0}
			}, {
				label : "페이지 뷰", data : data3, yaxis : 2, color : "#1C84C6",
				lines : {lineWidth : 1, show : true, fill : true, fillColor : {colors : [ {opacity : 0.2}, {opacity : 0.4} ]}},
				splines : {show : false,tension : 0.6,lineWidth : 1,fill : 0.1}
			} ];

			
		var options = {
				xaxis : {
					mode : "time",
					tickSize : [ 3, "day" ],
					tickLength : 0,
					axisLabel : "Date",
					axisLabelUseCanvas : true,
					axisLabelFontSizePixels : 12,
					axisLabelFontFamily : 'Arial',
					axisLabelPadding : 10,
					color : "#d5d5d5"
				},
				yaxes : [ {
					position : "left",
					color : "#d5d5d5",
					axisLabelUseCanvas : true,
					axisLabelFontSizePixels : 12,
					axisLabelFontFamily : 'Arial',
					axisLabelPadding : 3
				}, {
					position : "right",
					clolor : "#d5d5d5",
					axisLabelUseCanvas : true,
					axisLabelFontSizePixels : 12,
					axisLabelFontFamily : ' Arial',
					axisLabelPadding : 67
				} ],
				legend : {
					noColumns : 1,
					labelBoxBorderColor : "#000000",
					position : "nw"
				},
				grid : {
					hoverable : false,
					borderWidth : 0
				}
			};

			var previousPoint = null, previousLabel = null;

			$.plot($("#flot-dashboard-chart"), dataset, options);
});