function goPage(page, params) {
	if (page == 0) {
		return;
	}
	location.href="?page="+page+params;
}

function getDefaultDataFormat(str) {
	if (!str) {
		return '';
	}
	switch (str.length) {
	case 8:
		var year = str.substring(0, 4);
		var month = str.substring(4, 6);
		var day = str.substring(6, 8);
		return year + '-' + month + '-' + day;
	case 14:
		var year = str.substring(0, 4);
		var month = str.substring(4, 6);
		var day = str.substring(6, 8);
		var hour = str.substring(8, 10);
		var minute = str.substring(10, 12);
		var second = str.substring(12, 14);
		return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
	default:
		return '';
	}
}

function getDateFromFormat(str) {
	return new Date(str.replace(
		    /^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/,
		    '$4:$5:$6 $2/$3/$1'
		));
}

function getDateStr(date) {
	var mm = date.getMonth() + 1; // getMonth() is zero-based
	var dd = date.getDate();
	return [ date.getFullYear(), '-', (mm > 9 ? '' : '0') + mm, '-', (dd > 9 ? '' : '0') + dd ].join('');
}

function convertDateString(str) {
	return getDateStr(getDateFromFormat(str));
}

function formatDate(date) {
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var ampm = hours >= 12 ? 'PM' : 'AM';
	hours = hours % 12;
	hours = hours ? hours : 12; // the hour '0' should be '12'
	minutes = minutes < 10 ? '0' + minutes : minutes;
	var strTime = ampm + ' ' + hours + ':' + minutes;
	return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + strTime;
}

function formatSizeUnits(bytes) {
    if ( ( bytes >> 30 ) & 0x3FF ) {
//        bytes = ( bytes >>> 30 ) + '.' + ( bytes & (3*0x3FF )) + 'GB' ;
        bytes = ( bytes >>> 30 ) + 'GB' ;
    } else if ( ( bytes >> 20 ) & 0x3FF ) {
//        bytes = ( bytes >>> 20 ) + '.' + ( bytes & (2*0x3FF ) ) + 'MB' ;
        bytes = ( bytes >>> 20 ) + 'MB' ;
    } else if ( ( bytes >> 10 ) & 0x3FF ) {
//        bytes = ( bytes >>> 10 ) + '.' + ( bytes & (0x3FF ) ) + 'KB' ;
        bytes = ( bytes >>> 10 ) + 'KB' ;
    } else if ( ( bytes >> 1 ) & 0x3FF ) {
        bytes = ( bytes >>> 1 ) + 'Bytes' ;
    } else {
        bytes = bytes + 'Byte' ;
    }
    return bytes ;
}

$(function(){
	$('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
	
	$(".datepicker").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
	});
	$('.clockpicker').clockpicker({
	    placement: 'top',
	    align: 'right',
	    autoclose:true
	});
	
	$("#site-selector").change(function(){
		var sitecd = $(this).val();
		$.ajax({type:"POST", url : "/api/builder/site/selection", cache:false
			, data : {sitecd:sitecd}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					if (sitecd == "") {
						document.location.href="/builder/dashboard";
					} else {
						document.location.reload();
					}
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
});