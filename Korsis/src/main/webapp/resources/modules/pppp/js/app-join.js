/**
 * MODULE : APP-Join (default)
 * DATE : 2017-02-06
 * AUTHOR : 나태호 
 */

$(function(){
	initBirthForm();
	
	$('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
	
	$("#nickname").keyup(function(){
		$("#check-nickname").val("N");
	});
	$("#btn-nickname").click(function(){
		if (!$("#nickname").val()) {
			toast("별명을 입력해 주세요.");
			$("#nickname").focus();
			return;
		}
		$.ajax({
			type : "POST"
			, url : "/api/check/nickname"
			, cache : false
			, data : {nickname : $("#nickname").val()}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					toast("사용할 수 있는 별명입니다.");
					$("#check-nickname").val("Y");
				} else {
					toast(data.message);
				}
			}
			, error : function(data){
				toast(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-photo").click(function(){addFile("profile");});
	
	$("#agree-all").on('ifChecked', function(){
		$(".agree").iCheck("check");
	});
	$("#agree-all").on('ifUnchecked', function(){
		$(".agree").iCheck("uncheck");
	});
	
	$("#btn-submit").click(function(){
		if (!checkNull("email", "이메일을 입력해 주세요.")) {
			return;
		}

		if (!isEmail($("#email").val())) {
			toast("올바른 형식의 이메일을 입력해 주세요.");
			$("#email").focus();
			return;
		}
		
		$("#id").val($("#email").val());
		
		if (!checkNull("name", "이름을 입력해 주세요.")) {
			return;
		}
		
		if (!checkNull("nickname", "별명을 입력해 주세요.")) {
			return;
		}
		
		if ($("#check-nickname").val() == "N") {
			toast("별명의 중복검사를 해주세요.");
			$("#nickname").focus();
			return;
		}
		
		if (!checkNull("passwd", "비밀번호를 입력해 주세요.")) {
			return;
		}
		
		if (!checkNull("passwd_re", "비밀번호 확인을 입력해 주세요.")) {
			return;
		}
		
		if ($("#passwd").val() != $("#passwd_re").val()) {
			toast("비밀번호와 비밀번호 확인이 다릅니다..");
			$("#passwd_re").focus();
			return;
		}
		
		if (!$("#agree-service").prop("checked")) {
			toast("삐뽀삐뽀 서비스 이용약관에 동의해 주세요.");
			return;
		}
		
		if (!$("#agree-privacy").prop("checked")) {
			toast("개인정보 수집이용 및 취급 위탁 동의");
			return;
		}
		
		if (!$("#ageover14").prop("checked")) {
			toast("14세 이상만 회원가입 할 수 있습니다.");
			return;
		}
		
		$("#mobile").val($("#mobile1").val()+$("#mobile2").val());
		$("#birth").val($("#year").val()+$("#month").val()+$("#day").val());
		
		$.ajax({
			type:"POST"
			, url : "/api/join"
			, cache:false
			, data : $("#joinForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					toast("회원가입되었습니다.");
					finish();
				} else {
					toast(data.message);
				}
			}
			, error : function(data) {
				toast(data.responseJSON.error);
			}
		});
	});
});

function initBirthForm() {
	var $year = $("#year");
	var y = new Date().getFullYear();
	for (var i = parseInt(y); i >= 1940; i--) {
		$year.append('<option value="'+i+'">'+i+'년</option>');
	}
	var $month = $('#month');
	for (var i = 1; i <= 12; i++) {
		if (i < 10) {
			$month.append('<option value="0'+i+'">'+i+'월</option>');
		} else {
			$month.append('<option value="'+i+'">'+i+'월</option>');
		}
	}
	var $day = $('#day');
	for (var i = 1; i <= 31; i++) {
		if (i < 10) {
			$day.append('<option value="0'+i+'">'+i+'일</option>');
		} else {
			$day.append('<option value="'+i+'">'+i+'일</option>');
		}
	}
}

function uploadedFile(filename) {
	$("#btn-photo").attr("src", '/res/profile/temp/'+filename+'?width=150');
	$("#profile").val(filename);
}

function checkNull(id, msg) {
	if (!$("#" + id).val()) {
		toast(msg);
		$("#"+id).focus();
		return false;
	}
	return true;
}
