var JoinForm = function () {
    return {
        initJoinForm: function () {
	        // Validation       
	        $("#joinForm").validate({                   
	            // Rules for form validation
	            rules:
	            {
	                id:
	                {
	                    required: true
	                },
	                passwd:
	                {
	                    required: true,
	                    minlength: 3,
	                    maxlength: 20
	                },
	                passwdConfirm:
	                {
	                    required: true,
	                    minlength: 3,
	                    maxlength: 20,
	                    equalTo: '#passwd'
	                },
	                name_family:
	                {
	                    required: true
	                },
	                name:
	                {
	                    required: true
	                },
	                sex:
	                {
	                    required: true
	                }
	            },
	            
	            messages:
	            {
	                id:
	                {
	                    required: '아이디를 입력해 주세요.'
	                }, 
	                passwd:
	                {
	                    required: "비밀번호를 입력해 주세요.",
	                    minlength: "비밀번호는 최소 3글자에서 최대 20글자 입니다.",
	                    maxlength: "비밀번호는 최소 3글자에서 최대 20글자 입니다."
	                },
	                passwdConfirm:
	                {
	                    required: "비밀번호 확인을 입력해 주세요.",
	                    minlength: "비밀번호는 최소 3글자에서 최대 20글자 입니다.",
	                    maxlength: "비밀번호는 최소 3글자에서 최대 20글자 입니다.",
	                    equalTo: "비밀번호와 다릅니다. 다시 입력해 주세요."
	                },
	                name_family:
	                {
	                    required: "성을 입력해 주세요."
	                },
	                name:
	                {
	                    required: "이름을 입력해 주세요."
	                },
	                sex:
	                {
	                    required: "성별을 선택해 주세요."
	                }
	            },                  
	            submitHandler : function(form) {
	            	$(form).ajaxSubmit({
	            		beforeSend: function()
	                    {
	                        $('#sky-form4 button[type="submit"]').attr('disabled', true);
	                    },
	                    success: function(data)
	                    {
	                    	if (data.error == 0) {
	                    		document.location.href = "/login";
	                    	} else {
	                    		alert(data.message);
	                    	}
	                    }
	            	});
	            },
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
}();