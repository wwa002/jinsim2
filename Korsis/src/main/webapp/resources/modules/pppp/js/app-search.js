/**
 * MODULE : APP-MyPage (default)
 * DATE : 2017-02-06
 * AUTHOR : 나태호 
 */

var $keywordWrapper;
function search() {
  	var keyword = $('#keyword').val();
  	if (!keyword) {
      	toast('검색어를 입력해 주세요.');
      	return;
    }
 	
  	$.ajax({
		type:"GET"
		, url : "/api/search/all"
      	, data : {keyword:keyword}
		, cache : false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$keywordWrapper.empty();
				
				var hospitalTemplate = $("#hospital-item").html();
				var pharmacyTemplate = $("#pharmacy-item").html();
				var eventTemplate = $("#event-item").html();
				var castTemplate = $("#cast-item").html();
				var infodbTemplate = $("#infodb-item").html();
				Mustache.parse(hospitalTemplate);
				Mustache.parse(pharmacyTemplate);
				Mustache.parse(eventTemplate);
				Mustache.parse(castTemplate);
				Mustache.parse(infodbTemplate);
				
				var $hospital = $("#wrapper-hospital-item");
				var $pharmacy = $("#wrapper-pharmacy-item");
				var $event = $("#wrapper-event-item");
				var $cast = $("#wrapper-cast-item");
				var $infodb = $("#wrapper-infodb-item");
				
				$hospital.empty();
				if (data.data.hospital.length > 0) {
					for (var i=0; i < data.data.hospital.length; i++) {
						var d = data.data.hospital[i];
						$hospital.append(Mustache.render(hospitalTemplate, {ykiho:d.ykiho, yadmNm:d.yadmNm, addr:d.addr, telno:d.telno}));
					}
					$('#wrapper-hospital').removeClass('hidden');
				} else {
					$('#wrapper-hospital').addClass('hidden');
				}
				
				$pharmacy.empty();
				if (data.data.pharmacy.length > 0) {
					for (var i=0; i < data.data.pharmacy.length; i++) {
						var d = data.data.pharmacy[i];
						$pharmacy.append(Mustache.render(pharmacyTemplate, {hpid:d.hpid, dutyName:d.dutyName, dutyAddr:d.dutyAddr, dutyTel1:d.dutyTel1}));
					}
					$('#wrapper-pharmacy').removeClass('hidden');
				} else {
					$('#wrapper-pharmacy').addClass('hidden');
				}
				
				$event.empty();
				if (data.data.event.length > 0) {
					for (var i=0; i < data.data.event.length; i++) {
						var d = data.data.event[i];
						$event.append(Mustache.render(eventTemplate, {idx:d.idx, subject:d.subject, image:d.image, cost:numberWithCommas(d.cost), yadmNm:d.yadmNm, sgguCdNm:d.sgguCdNm, emdongNm:d.emdongNm}));
					}
					$('#wrapper-event').removeClass('hidden');
				} else {
					$('#wrapper-event').addClass('hidden');
				}
				
				$cast.empty();
				if (data.data.cast.length > 0) {
					for (var i=0; i < data.data.cast.length; i++) {
						var d = data.data.cast[i];
						$cast.append(Mustache.render(castTemplate, {idx:d.idx, subject:d.subject, image:d.image}));
					}
					$('#wrapper-cast').removeClass('hidden');
				} else {
					$('#wrapper-cast').addClass('hidden');
				}
				
				$infodb.empty();
				if (data.data.infodb.length > 0) {
					for (var i=0; i < data.data.infodb.length; i++) {
						var d = data.data.infodb[i];
						$infodb.append(Mustache.render(infodbTemplate, {dbidx:d.dbidx, dbgidx:d.dbgidx, idx:d.idx, subject:d.subject, image:d.image, content:d.content, favorite_count:numberWithCommas(d.favorite_count), read_count:numberWithCommas(d.read_count)}));
					}
					$('#wrapper-infodb').removeClass('hidden');
				} else {
					$('#wrapper-infodb').addClass('hidden');
				}
				
            } else {
				toast(data.message);
			}
		}
		, error : function(data) {
			toast(data.responseJSON.error);
		}
	});
}

$(function(){
  	$keywordWrapper = $('#search-keyword');
  	$("#btn-back").click(function(){
		finish();
	});
  	
	$.ajax({
		type:"GET"
		, url : "/api/search/keyword"
		, cache : false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				for (var i = 0; i < data.data.length; i++) {
                  	var d = data.data[i];
                  	$keywordWrapper.append('<span class="label label-info btn-keyword" data-keyword="'+d.value+'">'+d.value+'</span> ');
                }
			} else {
				toast(data.message);
			}
		}
		, error : function(data) {
			toast(data.responseJSON.error);
		}
	});
  
	$('#btn-search').click(function(){
      	search();
    });  	
  	$(document).on('click', '.btn-keyword', function(){
      	$('#keyword').val($(this).data('keyword'));
      	search();
    });
  	
  	$(document).on('click',".btn-call", function(){
		document.location.href="tel:"+$(this).data("phone");
		return false;
	});
	
	$(document).on('click',".hp-hospital", function(){
		openView($(this).data("subject"), "hospital", "ykiho="+$(this).data("ykiho"))
	});
	
	$(document).on('click',".hp-pharmacy", function(){
		openView($(this).data("subject"), "pharmacy", "hpid="+$(this).data("hpid"))
	});
	
	$(document).on('click',".event-item", function(){
		openView($(this).data("subject"), "event", "idx="+$(this).data("idx"))
	});
	
	$(document).on('click',".cast-item", function(){
		openView($(this).data("subject"), "cast/view", "idx="+$(this).data("idx"))
	});
	
	$(document).on('click',".medical-content-item", function(){
		var dbidx = $(this).data('dbidx');
		var dbgidx = $(this).data('dbgidx');
		var idx = $(this).data('idx');
		openView($(this).data("subject"), "infodb/view", 'dbidx='+dbidx+'&dbgidx='+dbgidx+'&idx='+idx)
	});
});