function goPage(page, params) {
	location.href="?page="+page+params;
}

$(function(){
	
	$(".datepicker").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
	});
	$('.clockpicker').clockpicker({
	    placement: 'top',
	    align: 'right',
	    donetext: '완료'
	});
	
	$("#hospital-search").click(function(){
		var keyword = $("#hospital-name").val();
		if (!keyword) {
			alert("병원명을 입력후 검색해 주세요.");
			$("#hospital-name").focus();
			return;
		}
		
		$.ajax({
			type : "GET"
				, url : "/api/event/hospital"
				, cache : false
				, data : {keyword:keyword}
				, success : function(data){
					console.log(data);
					if (data.error == 0) {
						if (data.data.length == 0) {
							alert("검색된 병원이 없습니다.");
							return;
						}
						$("#ykiho").empty();
						$("#ykiho").append('<option value="">병원 선택</option>');
						$("#ykiho").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							var html = '<option value="'+d.ykiho+'">'+d.yadmNm+' ('+d.telno+') - '+d.addr+'</option>';
							$("#ykiho").append(html);
						}
						$("#form-hospital").show();
					}
				}
				, error : function(data){
					alert(data.responseJSON.error);
				}
		});
	});
	
	$("#pharmacy-search").click(function(){
		var keyword = $("#pharmacy-name").val();
		if (!keyword) {
			alert("약국명을 입력후 검색해 주세요.");
			$("#pharmacy-name").focus();
			return;
		}
		
		$.ajax({
			type : "GET"
				, url : "/api/event/pharmacy"
				, cache : false
				, data : {keyword:keyword}
				, success : function(data){
					console.log(data);
					if (data.error == 0) {
						if (data.data.length == 0) {
							alert("검색된 약국이 없습니다.");
							return;
						}
						$("#hpid").empty();
						$("#hpid").append('<option value="">약국 선택</option>');
						$("#hpid").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							var html = '<option value="'+d.hpid+'">'+d.dutyName+' ('+d.dutyTel1+') - '+d.dutyAddr+'</option>';
							$("#hpid").append(html);
						}
						$("#form-pharmacy").show();
					}
				}
				, error : function(data){
					alert(data.responseJSON.error);
				}
		});
	});
});