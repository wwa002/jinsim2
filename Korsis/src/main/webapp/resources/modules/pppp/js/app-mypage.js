/**
 * MODULE : APP-MyPage (default)
 * DATE : 2017-02-06
 * AUTHOR : 나태호 
 */

$(function(){
	logined();
	
	$("body").css("background", "#ffffff");
	$("#btn-back").click(function(){
		finish();
	});
});

function logined() {
	$.ajax({type:"GET", url : "/api/myinfo", cache:false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$("#btn-photo").attr("src", '/res/profile/'+data.data.id);
				$("#name").html('<span id="btn-logout">'+data.data.nickname + ' <i class="fa fa-sign-out"></i></span>');
				loginProc(data.data);
			} else {
				logoutProc();
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function uploadedFile(type, url, filename, realname) {
	$.ajax({
		type:"POST"
		, url : "/api/photo/app"
		, cache:false
		, data : {url:url}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$("#btn-photo").attr("src", url);
			} else {
				toast(data.message);
			}
		}
		, error : function(data) {
			toast(data.responseJSON.error);
		}
	});
}

function logoutProc() {
	$("#btn-join").click(function(){
		openView("회원가입", "join");
	});
	$("#btn-login").click(function(){
		openView("로그인", "login");
	});
	
	$("#btn-medical, #btn-event, #btn-cast").click(function(){
		requestLogin();
	});
	
	$(".logout-wrapper").removeClass('hidden');
}

function loginProc(data) {
	$("#btn-logout").click(function(){
		$.ajax({type:"GET", url : "/api/logout", cache:false
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					logouted();
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#mypage-button-items").addClass('mypage-button-logined');
	$(".logout-wrapper").addClass('hidden');
	
	$(".login-wrapper").removeClass('hidden');
	
	var height = $(window).height();
	height -= $("#mypage-main-wrapper").height();
	height -= $("#mypage-button-wrapper").height();
	height -= $("#mypage-title-wrapper").height();
	$("#mypage-content-wrapper").height(height);
	$("#mypage-content-wrapper").css("overflow-x", "hidden");
	$("#mypage-content-wrapper").css("overflow-y", "auto");
	
	$("#btn-photo").click(function(){addFile("profile");});
	
	$("#btn-hp").attr("src", "/resources/modules/pppp/images/app/cc_plus_on.png");
	$("#btn-hp").click(function(){
		$("#btn-hp").attr("src", "/resources/modules/pppp/images/app/cc_plus_on.png");
		$("#btn-event").attr("src", "/resources/modules/pppp/images/app/cc_gift_off.png");
		$("#btn-info").attr("src", "/resources/modules/pppp/images/app/cc_dictionary_off.png");
		
		$("#show-tab-hp div").each(function(i){
			$(this).removeClass("active");
			if (i == 0) {
				$(this).addClass("active");
			}
		});
		$("#show-tab-hp").removeClass('hidden');
		$("#show-tab-event").addClass('hidden');
		$("#show-tab-info").addClass('hidden');
		
		$(".content-wrapper").addClass('hidden');
		$("#wrapper-hospital").removeClass('hidden');
		
	});
	$("#btn-event").click(function(){
		$("#btn-hp").attr("src", "/resources/modules/pppp/images/app/cc_plus_off.png");
		$("#btn-event").attr("src", "/resources/modules/pppp/images/app/cc_gift_on.png");
		$("#btn-info").attr("src", "/resources/modules/pppp/images/app/cc_dictionary_off.png");
		
		$("#show-tab-event div").each(function(i){
			$(this).removeClass("active");
			if (i == 0) {
				$(this).addClass("active");
			}
		});
		
		$("#show-tab-hp").addClass('hidden');
		$("#show-tab-event").removeClass('hidden');
		$("#show-tab-info").addClass('hidden');
		
		$(".content-wrapper").addClass('hidden');
		$("#wrapper-apply-event").removeClass('hidden');
	});
	$("#btn-info").click(function(){
		$("#btn-hp").attr("src", "/resources/modules/pppp/images/app/cc_plus_off.png");
		$("#btn-event").attr("src", "/resources/modules/pppp/images/app/cc_gift_off.png");
		$("#btn-info").attr("src", "/resources/modules/pppp/images/app/cc_dictionary_on.png");
		
		$("#show-tab-info div").each(function(i){
			$(this).removeClass("active");
			if (i == 0) {
				$(this).addClass("active");
			}
		});
		
		$("#show-tab-hp").addClass('hidden');
		$("#show-tab-event").addClass('hidden');
		$("#show-tab-info").removeClass('hidden');
		
		$(".content-wrapper").addClass('hidden');
		$("#wrapper-cast").removeClass('hidden');
	});
	
	$(".btn-show").click(function(){
		$(".content-wrapper").addClass('hidden');
		var type = $(this).data("type");
		switch (type) {
		case 'hospital':
			$("#show-tab-hp div").removeClass("active");
			$(this).addClass("active");
			$("#wrapper-hospital").removeClass('hidden');
			break;
		case 'pharmacy':
			$("#show-tab-hp div").removeClass("active");
			$(this).addClass("active");
			$("#wrapper-pharmacy").removeClass('hidden');
			break;
		case 'apply-event':
			$("#show-tab-event div").removeClass("active");
			$(this).addClass("active");
			$("#wrapper-apply-event").removeClass('hidden');
			break;
		case 'favorite-event':
			$("#show-tab-event div").removeClass("active");
			$(this).addClass("active");
			$("#wrapper-favorite-event").removeClass('hidden');
			break;
		case 'cast':
			$("#show-tab-info div").removeClass("active");
			$(this).addClass("active");
			$("#wrapper-cast").removeClass('hidden');
			break;
		case 'medical':
			$("#show-tab-info div").removeClass("active");
			$(this).addClass("active");
			$("#wrapper-medical").removeClass('hidden');
			break;
		}
	});
	
	$(document).on('click',".btn-call", function(){
		document.location.href="tel:"+$(this).data("phone");
		return false;
	});
	
	$(document).on('click',".hp-hospital", function(){
		openView($(this).data("subject"), "hospital", "ykiho="+$(this).data("ykiho"))
	});
	
	$(document).on('click',".hp-pharmacy", function(){
		openView($(this).data("subject"), "pharmacy", "hpid="+$(this).data("hpid"))
	});
	
	$(document).on('click',".event-item", function(){
		openView($(this).data("subject"), "event", "idx="+$(this).data("idx"))
	});
	
	$(document).on('click',".cast-item", function(){
		openView($(this).data("subject"), "cast/view", "idx="+$(this).data("idx"))
	});
	
	$(document).on('click',".medical-content-item", function(){
		var dbidx = $(this).data('dbidx');
		var dbgidx = $(this).data('dbgidx');
		var idx = $(this).data('idx');
		openView($(this).data("subject"), "infodb/view", 'dbidx='+dbidx+'&dbgidx='+dbgidx+'&idx='+idx)
	});
	
	var hospitalTemplate = $("#hospital-item").html();
	var pharmacyTemplate = $("#pharmacy-item").html();
	var eventTemplate = $("#event-item").html();
	var castTemplate = $("#cast-item").html();
	var infodbTemplate = $("#infodb-item").html();
	Mustache.parse(hospitalTemplate);
	Mustache.parse(pharmacyTemplate);
	Mustache.parse(eventTemplate);
	Mustache.parse(castTemplate);
	Mustache.parse(infodbTemplate);
	
	var $hospital = $("#wrapper-hospital");
	var $pharmacy = $("#wrapper-pharmacy");
	var $eventApply = $("#wrapper-apply-event");
	var $eventFavorite = $("#wrapper-favorite-event");
	var $cast = $("#wrapper-cast");
	var $infodb = $("#wrapper-medical");
	
	for (var i=0; i < data.favorite_hospital.length; i++) {
		var d = data.favorite_hospital[i];
		$hospital.append(Mustache.render(hospitalTemplate, {ykiho:d.ykiho, yadmNm:d.yadmNm, addr:d.addr, telno:d.telno}));
	}
	
	for (var i=0; i < data.favorite_pharmacy.length; i++) {
		var d = data.favorite_pharmacy[i];
		$pharmacy.append(Mustache.render(pharmacyTemplate, {hpid:d.hpid, dutyName:d.dutyName, dutyAddr:d.dutyAddr, dutyTel1:d.dutyTel1}));
	}
	
	for (var i=0; i < data.apply_event.length; i++) {
		var d = data.apply_event[i];
		$eventApply.append(Mustache.render(eventTemplate, {idx:d.idx, subject:d.subject, image:d.image, cost:numberWithCommas(d.cost), yadmNm:d.yadmNm, sgguCdNm:d.sgguCdNm, emdongNm:d.emdongNm}));
	}
	
	for (var i=0; i < data.favorite_event.length; i++) {
		var d = data.favorite_event[i];
		$eventFavorite.append(Mustache.render(eventTemplate, {idx:d.idx, subject:d.subject, image:d.image, cost:numberWithCommas(d.cost), yadmNm:d.yadmNm, sgguCdNm:d.sgguCdNm, emdongNm:d.emdongNm}));
	}
	
	for (var i=0; i < data.favorite_cast.length; i++) {
		var d = data.favorite_cast[i];
		$cast.append(Mustache.render(castTemplate, {idx:d.idx, subject:d.subject, image:d.image}));
	}
	
	for (var i=0; i < data.favorite_infodb.length; i++) {
		var d = data.favorite_infodb[i];
		$infodb.append(Mustache.render(infodbTemplate, {dbidx:d.dbidx, dbgidx:d.dbgidx, idx:d.idx, subject:d.subject, image:d.image, content:d.content, favorite_count:numberWithCommas(d.favorite_count), read_count:numberWithCommas(d.read_count)}));
	}
	
}