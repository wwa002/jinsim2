function getBoardTitle(code) {
	switch (code) {
	case "dental": return "치아교정";
	case "plastic": return "성형수술";
	case "doublejaw": return "양악수술";
	case "dermatology": return "피부과";
	case "pediatrics": return "소아과";
	case "woman": return "여성병원";
	case "eye": return "안과";
	case "medicine": return "내과";
	case "surgery": return "외과";
	case "oriental": return "한의원";
	case "urology": return "비뇨기과";
	case "otolaryngology": return "이비인후과";
	}
}

function getOS() {
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;
	if (/windows phone/i.test(userAgent)) {
		return "Windows Phone";
	}
	if (/android/i.test(userAgent)) {
		return "Android";
	}
	if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
		return "iOS";
	}
	return "unknown";
}

function addCommas(x) {
    var parts = x.toString().replace(/,/g,'').split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


function getURLParameters(paramName) {
	var sURL = window.document.URL.toString();
	if (sURL.indexOf("?") > 0) {
		var arrParams = sURL.split("?");
		var arrURLParams = arrParams[1].split("&");
		var arrParamNames = new Array(arrURLParams.length);
		var arrParamValues = new Array(arrURLParams.length);
		
		var i = 0;
		for (i = 0; i<arrURLParams.length; i++) {
		    var sParam =  arrURLParams[i].split("=");
			arrParamNames[i] = sParam[0];
			if (sParam[1] != "")
				arrParamValues[i] = unescape(sParam[1]);
			else
				arrParamValues[i] = '';
		}
	
		for (i=0; i<arrURLParams.length; i++) {
			if (arrParamNames[i] == paramName) {
				return arrParamValues[i];
			}
		}
		return '';
	}
}
  

function toast(msg) {
	/*
	alert(msg);
	/**/
	
	//*
	switch(getOS()) {
	case 'Android':
		window.android.toast(msg);
		break;
	case 'iOS':
		document.location.href="plans://toast?message="+msg;
		break;
	default:
		break;
	}
	/**/
}

function share(title, url) {
	switch(getOS()) {
	case 'Android':
		window.android.share(title, url);
		break;
	case 'iOS':
		document.location.href="plans://share?title="+title+"&url="+url;
		break;
	default:
		break;
	}
}

function openBoard(code) {
	openView(getBoardTitle(code), "board/list", "code="+code);
}

function openView(title, mode, qs) {
	switch(getOS()) {
	case 'Android':
		window.android.openView(title, mode, qs);
		break;
	case 'iOS':
		document.location.href="plans://openView?title="+title+"&mode="+mode+"&"+qs;
		break;
	default:
		break;
	}
}

function openLink(title, link) {
	switch(getOS()) {
	case 'Android':
		window.android.openLink(title, link);
		break;
	case 'iOS':
		document.location.href="plans://openLink?title="+title+"&link="+link;
		break;
	default:
		break;
	}
}

function logined(auth) {
	switch(getOS()) {
	case 'Android':
		window.android.logined(auth);
		break;
	case 'iOS':
		document.location.href="plans://logined?auth="+auth;
		break;
	default:
		break;
	}
}

function logouted() {
	switch(getOS()) {
	case 'Android':
		window.android.logouted();
		document.location.reload();
		break;
	case 'iOS':
		document.location.href="plans://logouted";
		break;
	default:
		break;
	}
}

function requestLogin() {
	openView("로그인", "login");
}

function requestSNSLogin(sns) {
	switch(getOS()) {
	case 'Android':
		window.android.requestLogin(sns);
		break;
	case 'iOS':
		document.location.href="plans://sns?sns="+sns;
		break;
	default:
		break;
	}
}

function finish() {
	switch(getOS()) {
	case 'Android':
		window.android.finish();
		break;
	case 'iOS':
		document.location.href="plans://finish";
		break;
	default:
		break;
	}
}

function addFile(type) {
	switch(getOS()) {
	case 'Android':
		window.android.addFile(type);
		break;
	case 'iOS':
		document.location.href="plans://addFile?type="+type;
		break;
	default:
		break;
	}
}

function addFile(code, type) {
	switch(getOS()) {
	case 'Android':
		window.android.addFile(code, type);
		break;
	case 'iOS':
		document.location.href="plans://addFile?code="+code+"&type="+type;
		break;
	default:
		break;
	}
}

function finishWithMessage(message) {
	switch(getOS()) {
	case 'Android':
		if (message != '') {
			toast(message);
		}
		finish();
		break;
	case 'iOS':
		document.location.href="pou://finishWithMessage?message="+message;
		break;
	default:
		break;
	}
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getAfterTime(str) {
	var date = new Date(str.replace(
		    /^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/,
		    '$4:$5:$6 $2/$3/$1'
		));
	var now = new Date();
	var size = (now.getTime() - date.getTime())/1000;
	if (size < 60) {
		return "방금";
	} else if (size >= 60 && size < 3600) {
		return Math.floor(size/60)+"분전";
	} else if (size >= 3600 && size < 86400) {
		return Math.floor(size/3600)+"시간 전";
	} else if (size >= 86400 && size < 2419200) {
		return Math.floor(size/86400)+"일 전";
	} else {
		return str.replace(
			    /^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/,
			    '$1년 $2월 $3일'
			); 
	}
}

// Board
function applyBanner(b, me) {
	var template = $("#banner-item").html();
	Mustache.parse(template);

	swiper.removeAllSlides();
	for (var i = 0; i < b.length; i++) {
		console.log(b[i]);
		var isLogin = !(!me);
		var code = b[i].code;
		var idx = b[i].idx;
		var subject = b[i].subject;
		var recommend = b[i].cnt_recommend;
		var comment = b[i].cnt_comment;
		var favorite = b[i].cnt_favorite;
		var read = b[i].cnt_read;
		var url = b[i].picture != '' ? '/api/board/'+code+'/download/' + b[i].picture + '/banner' : '';
		console.log(url);
		var date = getAfterTime(b[i].regdate);
		var rendered = Mustache.render(template
				, {isLogin:isLogin, idx:idx, code:code, subject:subject, url:url
					, recommend:recommend, comment:comment, favorite:favorite, read:read
					, date:date});
		swiper.appendSlide(rendered);
	}
}

function applyList(l, me) {
	var templateDefault = $("#list-item-default").html();
	var templateImage = $("#list-item-image").html();
	var templatePostScript = $("#list-item-postscript").html();
	
	Mustache.parse(templateDefault);
	Mustache.parse(templateImage);
	Mustache.parse(templatePostScript);
	
	for (var i = 0; i < l.length; i++) {
		/* console.log(l[i]); */
		var isLogin = !(!me);
		var code = l[i].code;
		var idx = l[i].idx;
		var code_text = getBoardTitle(l[i].code);
		var category_text = l[i].category == "후기" ? "병원후기" : "자유대화";
		var before = !l[i].var1 ? '' : '/api/board/'+code+'/download/'+l[i].var1+'/img';
		var after = !l[i].var2 ? '' : '/api/board/'+code+'/download/'+l[i].var2+'/img';
		var url = l[i].picture != '' ? '/api/board/'+code+'/download/'+l[i].picture+'/img' : "";
		var subject = l[i].subject;
		var recommend = l[i].cnt_recommend;
		var comment = l[i].cnt_comment;
		var favorite = l[i].cnt_favorite;
		var read = l[i].cnt_read;
		var date = getAfterTime(l[i].regdate);
		var rendered;
		
		if (l[i].category == "후기") {
			if (before && after) {
				rendered = Mustache.render(templatePostScript, {isLogin:isLogin, idx:idx, lock:"isLock", code:code, code_text:code_text, category_text:category_text, before:before, after:after, subject:subject, recommend:recommend, comment:comment, favorite:favorite, read:read, date:date});
			} else if (url != "") {
				rendered = Mustache.render(templateImage, {isLogin:isLogin, idx:idx, lock:"isLock", code:code, code_text:code_text, category_text:category_text, image:url, subject:subject, recommend:recommend, comment:comment, favorite:favorite, read:read, date:date});
			} else {
				rendered = Mustache.render(templateDefault, {isLogin:isLogin, idx:idx, lock:"isLock", code:code, code_text:code_text, category_text:category_text, subject:subject, recommend:recommend, comment:comment, favorite:favorite, read:read, date:date});
			}
		} else if (url != "") {
			rendered = Mustache.render(templateImage, {isLogin:isLogin, idx:idx, code:code, code_text:code_text, category_text:category_text, image:url, subject:subject, recommend:recommend, comment:comment, favorite:favorite, read:read, date:date});
		} else {
			rendered = Mustache.render(templateDefault, {isLogin:isLogin, idx:idx, code:code, code_text:code_text, category_text:category_text, subject:subject, recommend:recommend, comment:comment, favorite:favorite, read:read, date:date});
		}
		$("#list").append(rendered);		
	}
}

// Cast
function applyCastBanner(b) {
	var template = $("#cast-banner-item").html();
	Mustache.parse(template);

	swiper.removeAllSlides();
	for (var i = 0; i < b.length; i++) {
		console.log(b[i]);
		var image = '/api/db/cast/icon/'+b[i].idx+'/'+b[i].image+'?width=800';
		var rendered = Mustache.render(template, {idx:b[i].idx, url:image, subject:b[i].subject});
		swiper.appendSlide(rendered);
	}
}

function applyCastList(l) {
	var template = $("#cast-item").html();
	Mustache.parse(template);
	
	for (var i = 0; i < l.length; i++) {
		/* console.log(l[i]); */
		var idx = l[i].idx;
		var subject = l[i].subject;
		var image = '/api/db/cast/icon/'+l[i].idx+'/'+l[i].image+'?width=500';
		var rendered = Mustache.render(template, {idx:idx, subject:subject, url:image});
		$("#list").append(rendered);		
	}
}

// InfoDB Content
function applyMedicalList(l) {
	var template = $("#infodb-item").html();
	Mustache.parse(template);
	
	for (var i = 0; i < l.length; i++) {
		var idx = l[i].idx;
		var subject = l[i].subject;
		var content = l[i].content;
		var image = '/api/db/info/icon/'+l[i].idx+'/'+l[i].image+'?width=800';
		var rendered = Mustache.render(template, {idx:idx, subject:subject, content:content, url:image});
		$("#list").append(rendered);		
	}
}

function applyMedicalGroupList(l) {
	var template = $("#medical-group-item").html();
	Mustache.parse(template);
	
	for (var i = 0; i < l.length; i++) {
		var idx = l[i].idx;
		var subject = l[i].subject;
		var rendered = Mustache.render(template, {idx:idx, subject:subject});
		$("#list").append(rendered);
		applyMedicalContentList(idx, l[i].items);
	}
}

function applyMedicalContentList(id, l) {
	var template = $("#medical-content-item").html();
	Mustache.parse(template);
	
	for (var i = 0; i < l.length; i++) {
		var dbidx = l[i].dbidx;
		var dbgidx = l[i].dbgidx;
		var idx = l[i].idx;
		var subject = l[i].subject;
		var content = l[i].content;
		var url = '/api/db/info/content/icon/'+dbidx+'/'+dbgidx+'/'+idx+'/'+l[i].image+'?width=500';
		var read = l[i].read_count;
		var favorite = l[i].favorite_count;
		var rendered = Mustache.render(template, {dbidx:dbidx, dbgidx:dbgidx, idx:idx, subject:subject, content:content, url:url, read:read, favorite:favorite});
		$("#medical-group-list-"+id).append(rendered);		
	}
}