/**
 * MODULE : Glim&
 * DATE : 2017-03-20
 * AUTHOR : 나태호 
 */

var paths;
var code;
var ssid;

$(function(){
	paths = document.location.pathname.split('/');
	code = paths[2];
	ssid = getURLParameters('ssid');
	activeMenus();
	
	if (!ssid) {
		loadSession();
	} else {
		loadLecture();
	}

	if ($(window).width() < 768) {
		scale = 0.5;
	} else {
		scale = 1.0;
	}
	
	
	$(document).on('click', '.btn-agenda-date', function(){
		var dt = $(this).data('date');
		$('.btn-agenda-date').removeClass('active');
		$(this).addClass('active');
		$('.place-item').addClass('hidden');
		$('#place-'+dt).removeClass('hidden');
	});
	
	$(document).on('click', '.btn-show', function(){
		var subject = $(this).data('subject');
		if (confirm('\''+subject+'\'에 참여하시겠습니까?')) {
			var code = $(this).data('code');
			var idx = $(this).data('idx');
			document.location.href = '/conference/'+code+'/agenda/session?ssid='+idx;
		}
	});
	
	$('.btn-vote-o').click(function(){
		voteAnswer1 = 'N'; voteAnswer2 = 'N'; voteAnswer3 = 'N'; voteAnswer4 = 'N'; voteAnswer5 = 'N';
		switch ($(this).data('answer')) {
		case 1: voteAnswer1 = 'Y'; break;
		case 2: voteAnswer2 = 'Y'; break;
		case 3: voteAnswer3 = 'Y'; break;
		case 4: voteAnswer4 = 'Y'; break;
		case 5: voteAnswer5 = 'Y'; break;
		}
		
		/*
		$('.btn-vote-o').removeClass('btn-success');
		$('.btn-vote-o').addClass('btn-default');
		
		$(this).removeClass('btn-default');
		$(this).addClass('btn-success');
		*/
		
		$.ajax({
			type:"POST"
			, url : "/api/conference/session/vote"
			, cache:false
			, data : {code:code, sidx:ssid, idx:vote.idx
				, answer1:voteAnswer1, answer2:voteAnswer2, answer3:voteAnswer3, answer4:voteAnswer4, answer5:voteAnswer5
				, answer_etc:$('#answer-etc').val()}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					voteClear();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('.btn-vote-m').click(function(){
		switch ($(this).data('answer')) {
		case 1:
			if (voteAnswer1 == 'N') {
				voteAnswer1 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer1 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 2:
			if (voteAnswer2 == 'N') {
				voteAnswer2 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer2 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 3:
			if (voteAnswer3 == 'N') {
				voteAnswer3 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer3 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 4:
			if (voteAnswer4 == 'N') {
				voteAnswer4 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer4 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 5:
			if (voteAnswer5 == 'N') {
				voteAnswer5 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer5 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		}
	});
	
	$('#vote-o-compleate, #vote-m-compleate, #vote-s-compleate').click(function(){
		if (vote.type == 'S') {
			if (!$('#answer-etc').val()) {
				alert('답을 입력해 주세요.');
				return false;
			}
		} else {
			if (voteAnswer1 == 'N' && voteAnswer2 == 'N' && voteAnswer3 == 'N'&& voteAnswer4 == 'N'&& voteAnswer5 == 'N') {
				alert('답을 선택해 주세요.');
				return false;
			}
		}
		$.ajax({
			type:"POST"
			, url : "/api/conference/session/vote"
			, cache:false
			, data : {code:code, sidx:ssid, idx:vote.idx
				, answer1:voteAnswer1, answer2:voteAnswer2, answer3:voteAnswer3, answer4:voteAnswer4, answer5:voteAnswer5
				, answer_etc:$('#answer-etc').val()}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					voteClear();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});

	$('#vote-o, #vote-m, #vote-s').on('hidden.bs.modal', function(e){
		voteClear();
	});
	
	$('#btn-question').click(function(){
		$('#modal-question').modal('show');
	});
	
	$('#btn-question-complete').click(function(){
		if (!$('#question').val()) {
			alert('질문을 입력해 주세요.');
			return false;
		}
		$.ajax({
			type:"POST"
			, url : "/api/conference/session/question"
			, cache:false
			, data : {code:code, idx:ssid, question:$('#question').val()}
			, success : function(data) {
				if (data.error == 0) {
					alert("질문 했습니다.");
					$('#question').val('');
					$('#modal-question').modal('hide');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});

function activeMenus() {
	$('nav ul li').removeClass('active');
	$('li.'+paths[3]).addClass('active');
}

function loadSession() {
	if (!$('li.agenda').hasClass('active')) {
		return;
	}
	
	$.ajax({type:"GET", url : '/api/conference/session/list', cache:false
		, data : {code:code}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $dateList = $('#date-list');
				$dateList.empty();
				
				var dateTemplate = $('#date-item').html();
				var placeTemplate = $('#place-item').html();
				var sessionTemplate = $('#session-item').html();
				Mustache.parse(dateTemplate);
				Mustache.parse(placeTemplate);
				Mustache.parse(sessionTemplate);
				
				var date = data.data;
				for (var i = 0; i < date.length; i++) {
					var d = date[i];
					d.active = i == 0;
					$dateList.append(Mustache.render(dateTemplate, d));
					
					var $placeList = $('#place-list');
					for (var j = 0; j < d.place.length; j++) {
						var p = d.place[j];
						p.date = d.date;
						p.active = i == 0;
						var principal = p.principal1;
						if (p.principal2) {
							principal += '<br />' + p.principal2;
						}
						p.principal = principal;
						p.i = i;
						p.j = j;
						$placeList.append(Mustache.render(placeTemplate, p));
						
						var $sessionList = $('#session-'+i+'-'+j);
						var nowdate = new Date();
						for (var k = 0; k < p.session.length; k++) {
							var s = p.session[k];
							var startdate = strToDate(s.date+' '+s.stime, 'yyyy-mm-dd hh:ii:ss');
							var enddate = strToDate(s.date+' '+s.etime, 'yyyy-mm-dd hh:ii:ss');
							s.subject = s.title;
							s.canplay = startdate < nowdate && enddate > nowdate && s.type == 'S';
							s.cls = s.type == 'N' ? 'normal' : 'speak';
							$sessionList.append(Mustache.render(sessionTemplate, s));
						}
					}
				}
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadLecture() {
	$.ajax({type:"GET", url : '/api/conference/session/'+code+'/'+ssid, cache:false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				if (data.data.filetype == "pdf") {
					$('#pdf-container').removeClass('hidden');
					
					canvas = document.getElementById('the-canvas'),
			        ctx = canvas.getContext('2d');
					
					document.getElementById('zoomin').addEventListener('click', onZoomIn);
					document.getElementById('zoomout').addEventListener('click', onZoomOut);
					document.getElementById('zoomfit').addEventListener('click', onZoomFit);
					PDFJS.getDocument(data.data.path_api).then(function (pdfDoc_) {
					    pdfDoc = pdfDoc_;
					    var documentPagesNumber = pdfDoc.numPages;

					    $('#page_num').on('change', function() {
					        var pageNumber = Number($(this).val());

					        if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
					            queueRenderPage(pageNumber, scale);
					        }

					    });
					    renderPage(pageNum, scale);
					});
					
				} else if (data.data.filetype == "image") {
					$('#image-container').removeClass('hidden');
					$('#session-image').attr('src', data.data.path_api);
				} else {
				}
				
				loadData();
				setInterval(loadData, 5000);
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function strToDate(date, format) {
	var normalized      = date.replace(/[^a-zA-Z0-9]/g, '-');
	var normalizedFormat= format.toLowerCase().replace(/[^a-zA-Z0-9]/g, '-');
	var formatItems     = normalizedFormat.split('-');
	var dateItems       = normalized.split('-');
	
	var monthIndex  = formatItems.indexOf("mm");
	var dayIndex    = formatItems.indexOf("dd");
	var yearIndex   = formatItems.indexOf("yyyy");
	var hourIndex     = formatItems.indexOf("hh");
	var minutesIndex  = formatItems.indexOf("ii");
	var secondsIndex  = formatItems.indexOf("ss");
	
	var today = new Date();
	
	var year  = yearIndex>-1  ? dateItems[yearIndex]    : today.getFullYear();
	var month = monthIndex>-1 ? dateItems[monthIndex]-1 : today.getMonth()-1;
	var day   = dayIndex>-1   ? dateItems[dayIndex]     : today.getDate();
	
	var hour    = hourIndex>-1      ? dateItems[hourIndex]    : today.getHours();
	var minute  = minutesIndex>-1   ? dateItems[minutesIndex] : today.getMinutes();
	var second  = secondsIndex>-1   ? dateItems[secondsIndex] : today.getSeconds();

	return new Date(year,month,day,hour,minute,second);
}

function getURLParameters(paramName) {
	var sURL = window.document.URL.toString();
	if (sURL.indexOf("?") > 0) {
		var arrParams = sURL.split("?");
		var arrURLParams = arrParams[1].split("&");
		var arrParamNames = new Array(arrURLParams.length);
		var arrParamValues = new Array(arrURLParams.length);
		
		var i = 0;
		for (i = 0; i<arrURLParams.length; i++) {
		    var sParam =  arrURLParams[i].split("=");
			arrParamNames[i] = sParam[0];
			if (sParam[1] != "")
				arrParamValues[i] = unescape(sParam[1]);
			else
				arrParamValues[i] = '';
		}
	
		for (i=0; i<arrURLParams.length; i++) {
			if (arrParamNames[i] == paramName) {
				return arrParamValues[i];
			}
		}
		return '';
	}
}


function loadData() {
	$.ajax({
		type:"GET"
		, url : "/api/conference/session/lecture"
		, cache:false
		, data : {code:code, idx:ssid}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				if (data.data.lecture.filename.endsWith('pdf')) {
					if (pdfDoc) {
						var scale = pdfDoc.scale;
						if (pageNum != data.data.lecture.page) {
			            	queueRenderPage(data.data.lecture.page, scale);
			            	pageNum = data.data.lecture.page;
						}
					}
				}
				if (data.data.vote.length > 0) {
					voteRun(data.data.vote[0]);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}


var pdfDoc = null, pageNum = 1, pageRendering = false, pageNumPending = null, scale = 0.5, zoomRange = 0.25;
var canvas;
var ctx;

function renderPage(num, scale) {
	pageRendering = true;
	pdfDoc.getPage(num).then(function(page) {
		var viewport = page.getViewport(scale);
		canvas.height = viewport.height;
		canvas.width = viewport.width;

		var renderContext = {
			canvasContext : ctx,
			viewport : viewport
		};
		var renderTask = page.render(renderContext);

		renderTask.promise.then(function() {
			pageRendering = false;
			if (pageNumPending !== null) {
				renderPage(pageNumPending);
				pageNumPending = null;
			}
		});
	});
}

function queueRenderPage(num) {
	if (pageRendering) {
		pageNumPending = num;
	} else {
		renderPage(num, scale);
	}
}

function onZoomIn() {
	if (scale >= pdfDoc.scale) {
		return;
	}
	scale += zoomRange;
	var num = pageNum;
	renderPage(num, scale)
}

function onZoomOut() {
	if (scale >= pdfDoc.scale) {
		return;
	}
	scale -= zoomRange;
	var num = pageNum;
	queueRenderPage(num, scale);
}

function onZoomFit() {
	if (scale >= pdfDoc.scale) {
		return;
	}
	scale = 1;
	var num = pageNum;
	queueRenderPage(num, scale);
}

var vote;
var voteAnswer1='N', voteAnswer2='N', voteAnswer3='N', voteAnswer4='N', voteAnswer5='N';
var voteTimer;
var voteTime = 0;
function voteRun(v) {
	if (vote) {
		return;
	}
	vote = v;
	var voteAnswer1='N', voteAnswer2='N', voteAnswer3='N', voteAnswer4='N', voteAnswer5='N';
	switch (vote.type) {
	case 'O':
		$('#question-o').text(vote.question);
		$('#btn-vote-o-1').text(vote.answer1);
		$('#btn-vote-o-2').text(vote.answer2);
		$('#btn-vote-o-3').text(vote.answer3);
		$('#btn-vote-o-4').text(vote.answer4);
		$('#btn-vote-o-5').text(vote.answer5);
		$('#vote-o').modal('show');
		voteTime = 10;
		voteTimer = setInterval(voteRunner, 1000);
		break;
	case 'M':
		$('#question-m').text(vote.question);
		$('#btn-vote-m-1').text(vote.answer1);
		$('#btn-vote-m-2').text(vote.answer2);
		$('#btn-vote-m-3').text(vote.answer3);
		$('#btn-vote-m-4').text(vote.answer4);
		$('#btn-vote-m-5').text(vote.answer5);
		$('#vote-m').modal('show');
		voteTime = 10;
		voteTimer = setInterval(voteRunner, 1000);
		break;
	case 'S':
		$('#question-s').text(vote.question);
		$('#answer-etc').val('');
		$('#vote-s').modal('show');
		break;
	}
}

function voteRunner() {
	if (voteTime <= 0) {
		voteClear();
		return;
	}
	
	voteTime--;
	$('.vote-second').text(voteTime);
}

function voteClear() {
	if (voteTimer) {
		clearInterval(voteTimer);
		voteTimer = null;
	}
	vote=null;
	voteTime=0;
	voteAnswer1='N', voteAnswer2='N', voteAnswer3='N', voteAnswer4='N', voteAnswer5='N';
	$('#vote-o').modal('hide');
	$('.btn-vote-o').removeClass('btn-success');
	$('.btn-vote-o').addClass('btn-default');
	
	$('#vote-m').modal('hide');
	$('.btn-vote-m').removeClass('btn-success');
	$('.btn-vote-m').addClass('btn-default');
	
	$('#vote-s').modal('hide');
	$('#answer-etc').val('');
}