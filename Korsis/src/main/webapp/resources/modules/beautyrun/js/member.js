function goPage(page, params) {
	location.href="?page="+page+params;
}

$(function(){
	
	$(".datepicker").datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
	});
	$('.clockpicker').clockpicker({
	    placement: 'top',
	    align: 'right',
	    donetext: '완료'
	});
	
});