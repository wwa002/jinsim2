/**
 * MODULE : APP-MyPage (default)
 * DATE : 2017-02-06
 * AUTHOR : 나태호 
 */

$(function(){
	$("body").css("background", "#ffffff");
	$("#btn-login").click(function(){
		if (!$("#id").val()) {
			toast("이메일을 입력해 주세요");
			$("#id").focus();
			return;
		}
		if (!$("#passwd").val()) {
			toast("비밀번호를 입력해 주세요");
			$("#passwd").focus();
			return;
		}
		
		$.ajax({
			type:"POST"
			, url : "/api/login"
			, cache:false
			, data : $("#loginForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					logined(data.data.auth);
					finish();
				} else {
					toast(data.message);
				}
			}
			, error : function(data) {
				toast(data.responseJSON.error);
			}
		});
	});
	
	$('#btn-login-facebook').click(function(){
		requestSNSLogin('facebook');
	});
	$('#btn-login-kakao').click(function(){
		requestSNSLogin('kakao');
	});
	$('#btn-login-naver').click(function(){
		requestSNSLogin('naver');
	});
	
});