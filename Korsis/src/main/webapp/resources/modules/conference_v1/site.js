/**
 * MODULE : Conference Site (default)
 * DATE : 2017-02-17
 * AUTHOR : 나태호 
 */

var paths;
var code;

$(function(){
	paths = document.location.pathname.split('/');
	code = paths[2];

	App.init();
	loadNotice();
	loadSession();
	
	$('.clockpicker').clockpicker({
	    placement: 'bottom',
	    align: 'right',
	    autoclose:true
	});
	
	$(document).on('click', '.btn-show', function(){
		var subject = $(this).data('subject');
		if (confirm('\''+subject+'\'에 참여하시겠습니까?')) {
			var code = $(this).data('code');
			var idx = $(this).data('idx');
			window.open('/conference/'+code+'/session/'+idx);
		}
	});
	
	$("#captchaImg").click(function(){
		$("#captchaImg").attr("src", "/res/captcha?id=" + Math.random());
	});
	
	$('#btn-registration').click(function(){
		openRegisterForm(1);
	});
	
	$('#btn-registration-check').click(function(){
		$('#register-check').modal('show');
	});
	
	$('.btn-next, .btn-prev').click(function(){
		openRegisterForm($(this).data('step'));
	});
	
	$('#btn-register').click(function(){
		registration();
	});
	
	$('#btn-check').click(function(){
		registrationCheck();
	});
});

function openRegisterForm(step) {
	$('.modal').modal('hide');
	$('#register-step-'+step).modal('show');
}

function loadNotice() {
	$.ajax({type:"GET", url : '/api/board/2017spring-notice', cache:false
		, data : {}
		, success : function(data) {
//			console.log(data);
			if (data.error == 0) {
				var list = data.data.list;
				
				var $list = $('#notice-list');
				var template = $("#notice-item").html();
				Mustache.parse(template);
				
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(template, {
						idx:l.idx, code:'2017spring-notice', subject:l.subject, name:l.name, image:l.picture, date:l.regdate.substring(0, 4)+"-"+l.regdate.substring(4, 6)+"-"+l.regdate.substring(6, 8)
					});
					$list.append(rendered);
				}
				
				Owl2Carouselv1.initOwl2Carouselv1();
				Owl2Carouselv2.initOwl2Carouselv2();
				Owl2Carouselv3.initOwl2Carouselv3();
				Owl2Carouselv4.initOwl2Carouselv4();
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadSession() {
	$.ajax({type:"GET", url : '/api/conference/session/list', cache:false
		, data : {code:'2017spring'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $dateList = $('#date-list');
				$dateList.empty();
				
				var dateTemplate = $('#date-item').html();
				var placeTemplate = $('#place-item').html();
				var sessionTemplate = $('#session-item').html();
				Mustache.parse(dateTemplate);
				Mustache.parse(placeTemplate);
				Mustache.parse(sessionTemplate);
				
				var date = data.data;
				for (var i = 0; i < date.length; i++) {
					var d = date[i];
					$dateList.append(Mustache.render(dateTemplate, {date:d.date}));
					
					var $placeList = $('#place-'+d.date);
					for (var j = 0; j < d.place.length; j++) {
						var p = d.place[j];
						var principal = p.principal1;
						if (p.principal2) {
							principal += ' / ' + p.principal2;
						}
						$placeList.append(Mustache.render(placeTemplate, {place:p.place, i:i, j:j, principal:principal}));
						
						var $sessionList = $('#session-'+i+'-'+j);
						var nowdate = new Date();
						for (var k = 0; k < p.session.length; k++) {
							var s = p.session[k];
							var startdate = strToDate(s.date+' '+s.stime, 'yyyy-mm-dd hh:ii:ss');
							var enddate = strToDate(s.date+' '+s.etime, 'yyyy-mm-dd hh:ii:ss');
							s.subject = s.title;
							s.canplay = startdate < nowdate && enddate > nowdate && s.type == 'S';
							$sessionList.append(Mustache.render(sessionTemplate, s));
						}
					}
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function strToDate(date, format) {
	var normalized      = date.replace(/[^a-zA-Z0-9]/g, '-');
	var normalizedFormat= format.toLowerCase().replace(/[^a-zA-Z0-9]/g, '-');
	var formatItems     = normalizedFormat.split('-');
	var dateItems       = normalized.split('-');
	
	var monthIndex  = formatItems.indexOf("mm");
	var dayIndex    = formatItems.indexOf("dd");
	var yearIndex   = formatItems.indexOf("yyyy");
	var hourIndex     = formatItems.indexOf("hh");
	var minutesIndex  = formatItems.indexOf("ii");
	var secondsIndex  = formatItems.indexOf("ss");
	
	var today = new Date();
	
	var year  = yearIndex>-1  ? dateItems[yearIndex]    : today.getFullYear();
	var month = monthIndex>-1 ? dateItems[monthIndex]-1 : today.getMonth()-1;
	var day   = dayIndex>-1   ? dateItems[dayIndex]     : today.getDate();
	
	var hour    = hourIndex>-1      ? dateItems[hourIndex]    : today.getHours();
	var minute  = minutesIndex>-1   ? dateItems[minutesIndex] : today.getMinutes();
	var second  = secondsIndex>-1   ? dateItems[secondsIndex] : today.getSeconds();

	return new Date(year,month,day,hour,minute,second);
}

function registration() {
	if (!$('#name').val()) {
		alert('이름을 입력해 주세요.');
		openRegisterForm(1);
		$('#name').focus();
		return;
	}
	
	if (!$('#phone').val()) {
		alert('연락처를 입력해 주세요.');
		openRegisterForm(1);
		$('#phone').focus();
		return;
	}
	
	if (!$('#email').val()) {
		alert('이메일을 입력해 주세요.');
		openRegisterForm(1);
		$('#email').focus();
		return;
	}
	
	if (!$('#captcha-form').val()) {
		alert('스팸방지 코드를 입력해 주세요.');
		openRegisterForm(3);
		$('#captcha-form').focus();
		return;
	}
	
	var name = $('#name').val();
	var office = $('#office').val();
	var phone = $('#phone').val();
	var email = $('#email').val();
	var marketer = $('#marketer').val();
	var traffic = '';
	if ($('#traffic_1').prop('checked')) {
		traffic = $('#traffic_1').val(); 
	}
	if ($('#traffic_2').prop('checked')) {
		traffic = $('#traffic_2').val(); 
	}
	if ($('#traffic_3').prop('checked')) {
		traffic = $('#traffic_3').val(); 
	}
	var traffic_start = $('#traffic_start').val();
	var etc = $('#etc').val();
	var stay = '';
	if ($('#stay_1').prop('checked')) {
		stay = $('#stay_1').val(); 
	}
	if ($('#stay_2').prop('checked')) {
		stay = $('#stay_2').val(); 
	}
	var stay_checkin = $('#stay_checkin').val();
	var stay_checkout = $('#stay_checkout').val();
	var stay_adult = $('#stay_adult').val();
	var stay_child = $('#stay_child').val();
	var stay_bed = '';
	if ($('#stay_bed_1').prop('checked')) {
		stay_bed = $('#stay_bed_1').val(); 
	}
	if ($('#stay_bed_2').prop('checked')) {
		stay_bed = $('#stay_bed_2').val(); 
	}
	var stay_etc = $('#stay_etc').val();
	var captcha = $('#captcha-form').val();
	$.ajax({
		type:"POST"
		, url : "/api/conference/register"
		, cache:false
		, data : {code:code, name:name, office:office, phone:phone, email:email, marketer:marketer
				, traffic:traffic, traffic_start:traffic_start, etc:etc
				, stay:stay, stay_checkin:stay_checkin, stay_checkout:stay_checkout, stay_adult:stay_adult, stay_child:stay_child, stay_bed:stay_bed, stay_etc:stay_etc
				, captcha:captcha
			}
		, success : function(data) {
			if (data.error == 0) {
				alert('등록해 주셔서 감사합니다.');
				document.location.reload();
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
	
}

function registrationCheck() {
	if (!$('#check-name').val()) {
		alert('이름을 입력해 주세요.');
		$('#check-name').focus();
		return;
	}
	
	if (!$('#check-email').val()) {
		alert('이메일을 입력해 주세요.');
		$('#check-email').focus();
		return;
	}
	
	var name = $('#check-name').val();
	var email = $('#check-email').val();

	$.ajax({
		type:"POST"
		, url : "/api/conference/register/check"
		, cache:false
		, data : {code:code, name:name, email:email}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				if (!data.data) {
					alert('이름과 이메일을 확인해 주세요.');
				} else {
					alert('등록되어 있습니다.\n등록된 연락처 : '+data.data.phone);
				}
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
	
}