/**
 * MODULE : Site (default)
 * DATE : 2016-12-22
 * AUTHOR : 나태호 
 */

$(function(){
	logined();
	
	$("#btn-login").click(function(){
		login();
	});
	$("#btn-join").click(function(){
		join();
	});
});

function isValidEmail(email) {
	var ve = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	return ve.test(email);
}

function getParameter(key) {
	var _parammap = {};
	document.location.search.replace(/\?(?:([^=]+)=([^&]*)&?)/g, function() {
		function decode(s) {
			return decodeURIComponent(s.split("+").join(" "));
		}
		_parammap[decode(arguments[1])] = decode(arguments[2]);
	});

	return _parammap[key];
}

function login() {
	$.ajax({type:"POST", url : "/api/login", cache:false
		, data : $("#loginForm").serialize()
		, success : function(data) {
			if (data.error == 0) {
				var url = getParameter('url');
				if (typeof url == "undefined") {
					location.href='/';
				} else {
					window.location.href=url;
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function join() {
	var $nameFirst = $("#name-first");
	var $nameLast = $("#name-last");
	var $email = $("#email");
	var $id = $("#id");
	var $passwd = $("#passwd");
	var $service = $("#service");
	
	if (!$nameFirst.val()) {
		alert("이름을 입력해 주세요.");
		$nameFirst.focus();
		return;
	}
	
	if (!$nameLast.val()) {
		alert("이름을 입력해 주세요.");
		$nameLast.focus();
		return;
	}
	
	if (!$email.val()) {
		alert("이메일을 입력해 주세요.");
		$email.focus();
		return;
	}
	
	if (!isValidEmail($email.val())) {
		alert("올바른 형태의 이메일을 입력해 주세요.");
		$email.focus();
		return;
	}
	
	if (!$id.val()) {
		alert("아이디를 입력해 주세요.");
		$id.focus();
		return;
	}
	
	if (!$passwd.val()) {
		alert("비밀번호를 입력해 주세요.");
		$passwd.focus();
		return;
	}
	
	if (!$service.prop("checked")) {
		alert("서비스 이용약관에 동의해 주세요.");
		$service.focus();
		return;
	}
	
	$.ajax({type:"POST", url : "/api/join", cache:false
		, data : $("#joinForm").serialize()
		, success : function(data) {
			if (data.error == 0) {
				document.location.href='/site/login';
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function logined() {
	$.ajax({type:"GET", url : "/api/myinfo", cache:false
		, success : function(data) {
			if (data.error == 0) {
				if (data.data.type == "ADMIN") {
					$(".menu-nomember").addClass("hidden");
					$(".menu-member").addClass("hidden");
					$(".menu-admin").removeClass("hidden");
				} else {
					$(".menu-nomember").addClass("hidden");
					$(".menu-member").removeClass("hidden");
					$(".menu-admin").addClass("hidden");
				}
				
				switch (document.location.pathname) {
				case '/site/login':
				case '/site/join':
					document.location.href='/';
					break;
				case '/site/mypage':
					mypage(data.data);
					break;
				}
			} else {
				$(".menu-nomember").removeClass("hidden");
				$(".menu-member").addClass("hidden");
				$(".menu-admin").addClass("hidden");
				
				switch (document.location.pathname) {
				case '/site/mypage':
					document.location.href='/site/login?url='+encodeURIComponent(window.location.href);
					break;
				}
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function mypage(data) {
	$("#mypage-name").text(data.name);
}

