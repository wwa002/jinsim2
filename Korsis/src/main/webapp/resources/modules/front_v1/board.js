/**
 * MODULE : Board (default)
 * DATE : 2016-12-28
 * AUTHOR : 나태호 
 */

var Board = function(){
	var paths;
	var code;
	var cfg;
	var me;
	var article;
	
	var $list;
	var $btnWrite;
	var $btnModify;
	var $btnReply;
	var $btnCancel;
	var $pagination;
	var $files;
	var $commentList;
	
	var files = [];
	
	function isLogin() {
		return me != null;
	}
	
	function isAdmin() {
		if (!isLogin()) {
			return false;
		}
		return "ADMIN" == me.type;
	}
	
	function nl2br(str){  
		return str.replace(/\n/g, "<br />");  
	}
	
	function getDateFromFormat(str) {
		return new Date(str.replace(
			    /^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/,
			    '$4:$5:$6 $2/$3/$1'
			));
	}

	function getDateStr(date) {
		var mm = date.getMonth() + 1; // getMonth() is zero-based
		var dd = date.getDate();
		return [ date.getFullYear(), '-', (mm > 9 ? '' : '0') + mm, '-', (dd > 9 ? '' : '0') + dd ].join('');
	}

	function convertDateString(str) {
		return getDateStr(getDateFromFormat(str));
	}

	function formatDate(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var strTime = ampm + ' ' + hours + ':' + minutes;
		return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + strTime;
	}

	function formatSizeUnits(bytes) {
	    if ( ( bytes >> 30 ) & 0x3FF ) { // bytes = ( bytes >>> 30 ) + '.' + ( bytes & (3*0x3FF )) + 'GB' ;
	        bytes = ( bytes >>> 30 ) + 'GB' ;
	    } else if ( ( bytes >> 20 ) & 0x3FF ) { // bytes = ( bytes >>> 20 ) + '.' + ( bytes & (2*0x3FF ) ) + 'MB' ;
	        bytes = ( bytes >>> 20 ) + 'MB' ;
	    } else if ( ( bytes >> 10 ) & 0x3FF ) { // bytes = ( bytes >>> 10 ) + '.' + ( bytes & (0x3FF ) ) + 'KB' ;
	        bytes = ( bytes >>> 10 ) + 'KB' ;
	    } else if ( ( bytes >> 1 ) & 0x3FF ) {
	        bytes = ( bytes >>> 1 ) + 'Bytes' ;
	    } else {
	        bytes = bytes + 'Byte' ;
	    }
	    return bytes ;
	}
	
	function getParameter(key) {
		var _parammap = {};
		document.location.search.replace(/\?(?:([^=]+)=([^&]*)&?)/g, function() {
			function decode(s) {
				return decodeURIComponent(s.split("+").join(" "));
			}
			_parammap[decode(arguments[1])] = decode(arguments[2]);
		});

		return _parammap[key];
	}
	
	function initList() {
		$list = $("#list");
		$btnWrite = $("#btn-write");
		$btnWrite.click(function(){
			document.location.href='/board/' + code + '/write';
		});
		$pagination = $("#pagination");
		$(document).on('click', '.btn-page', function(){
			loadList($(this).data('page'));
		});
		$("#keyword").val(getParameter('keyword'));
	}

	function loadList(page) {
		$.ajax({type:"GET", url : '/api/board/'+code, cache:false
			, data : {page:page, keyword:$("#keyword").val(), category:$("#category").val()}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					cfg = data.data.cfg;
					me = data.data.me;
					
					// 작성 권한에 따른 버튼 표기
					if (cfg.permit_write != "") {
						if (isAdmin()) {
							$btnWrite.removeClass('hidden');
						} else if (isLogin()) {
							var pers = cfg.permit_write.split(',');
							var isOk = false;
							for (var i = 0; i < pers.length; i++) {
								if (pers[i] == me.gcd) {
									isOk = true;
								}
							}
							if (isOk) {
								$btnWrite.removeClass('hidden');
							}
						}
					} else {
						$btnWrite.removeClass('hidden');
					}
					
					
					// 게시물 출력
					$list.empty();
					$pagination.empty();
					
					var notice = data.data.notice;
					var list = data.data.list;
					var paging = data.data.paging;
					
					var templateNotice = $("#list-item-notice").html();
					var templateList = $("#list-item").html();
					var templatePaging = $("#paging-item").html();
					Mustache.parse(templateNotice);
					Mustache.parse(templateList);
					Mustache.parse(templatePaging);
					
					for (var i = 0; i < notice.length; i++) {
						var n = notice[i];
						var rendered = Mustache.render(templateNotice, {
							idx:n.idx, code:code, subject:n.subject, name:n.name
							, date:n.regdate.substring(0, 4)+"-"+n.regdate.substring(4, 6)+"-"+n.regdate.substring(6, 8)
							, read:n.cnt_read
							, comments : n.cnt_comment > 0 ? "["+n.cnt_comment+"]" : ""
						});
						$list.append(rendered);
					}
					
					var virtualNo = paging.virtualRecordNo;
					for (var i = 0; i < list.length; i++) {
						var l = list[i];
						if (l.thread.length > 1) {
							var thread = "";
							for (var j = 1; j < l.thread.length; j++) {
								thread += "&nbsp;&nbsp;&nbsp;";
							}
							thread += "<i class=\"fa fa-reply\"></i>";
						} else {
							var thread = "";
						}
						
						var rendered = Mustache.render(templateList, {
							no:virtualNo--, idx:l.idx, code:code, subject:l.subject, name:l.name, date:l.regdate.substring(0, 4)+"-"+l.regdate.substring(4, 6)+"-"+l.regdate.substring(6, 8)
							, lock : l.hidden == "Y" ? "<i class=\"fa fa-lock\"></i>" : ""
							, thread : thread
							, read:l.cnt_read
							, comments : l.cnt_comment > 0 ? "["+l.cnt_comment+"]" : ""
						});
						$list.append(rendered);
					}
					
					$pagination.append(Mustache.render(templatePaging, {page:paging.prevBlockNo, pageTitle:"«"}));
					for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
						$pagination.append(Mustache.render(templatePaging, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
					}
					$pagination.append(Mustache.render(templatePaging, {page:paging.nextBlockNo, pageTitle:"»"}));

				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function initWrite() {
		$files = $("#files");
		$btnWrite = $("#btn-write");
		$btnCancel = $("#btn-cancel");
		
		$btnWrite.click(function(){
			writeConfirm();
		});
		$btnCancel.click(function(){
			history.back();
		});
		
		$("#content").hide();
		$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true});
		$("#my-awesome-dropzone").attr('action', '/api/board/'+code+'/upload/dropzone');
		Dropzone.options.myAwesomeDropzone = {
			init: function () {
				this.on("complete", function (data) {
					var res = JSON.parse(data.xhr.responseText);
					if (res.status == "success") {
						files.push(res.data);
						$files.val(files.join());
					}
				});
				this.on("processing", function(file){
					this.options.url = $("#my-awesome-dropzone").attr("action");
				});
			}
		};
		
		$.ajax({type:"GET", url : '/api/board/'+code+'/cfg', cache:false
			, data : {}
			, success : function(data) {
				if (data.error == 0) {
					cfg = data.data.cfg;
					me = data.data.me;
					
					if (me == null) {
						$("#id-wrapper").removeClass("hidden");
						$("#captcha-wrapper").removeClass("hidden");
						$("#captchaImg").click(function(){
							$("#captchaImg").attr("src", "/res/captcha?id=" + Math.random());
						});
					}
					
					if ("Y" == cfg.notice && me != null) {
						$("#notice-wrapper").removeClass("hidden");
					}
					if ("Y" == cfg.hidden) {
						$("#hidden-wrapper").removeClass("hidden");
					}
					if ("Y" == cfg.file) {
						$("#file-wrapper").removeClass("hidden");
					}
					if ("Y" == cfg.category) {
						$("#category-wrapper").removeClass("hidden");
						var cats = cfg.categories.split(',');
						for (i = 0; i < cats.length; i++) {
							$("#category").append('<option value="'+cats[i]+'">'+cats[i]+'</option>');
						}
					}
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function writeConfirm() {
		if (!$("#subject").val()) {
			alert('제목을 입력해 주세요.');
			$("#subject").focus();
			return;
		}
		
		if ($('#summernote').summernote('isEmpty') || $('#summernote').summernote('code') == "") {
			alert("내용을 입력해 주세요");
			return;
		}
		
		if (me == null) {
			if (!$("#name").val()) {
				alert('이름을 입력해 주세요.');
				$("#name").focus();
				return;
			}
			if (!$("#passwd").val()) {
				alert('비밀번호를 입력해 주세요.');
				$("#passwd").focus();
				return;
			}
			
			if (!$("#captcha-form").val()) {
				alert('스팸방지코드를 입력해 주세요.');
				$("#captcha-form").focus();
				return;
			}
			
			$("#captcha").val($("#captcha-form").val());
		}
		
		$("#content").val($('#summernote').summernote('code'));
		
		$.ajax({type:"POST", url : "/api/board/"+code+"/write", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert("등록되었습니다.");
					document.location.href='/board/'+code;
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function initCert() {
		$("#btn-cert").click(function(){
			if (!$("#passwd").val()) {
				alert("비밀번호를 입력해 주세요.");
				$("#passwd").focus();
				return;
			}
			
			var idx = paths[3];
			$.ajax({type:"POST", url : "/api/board/"+code+"/"+idx+"/cert", cache:false
				, data : {passwd:$("#passwd").val()}
				, success : function(data) {
					if (data.error == 0) {
						document.location.href="/board/"+code+"/"+idx;
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		});
	}
	
	function initView() {
		var idx = paths[3];
		
		$("#btn-reply").click(function(){
			document.location.href = '/board/'+code+'/'+idx+'/reply';
		});
		$("#btn-modify").click(function(){
			document.location.href = '/board/'+code+'/'+idx+'/modify';
		});
		$("#btn-delete").click(function(){
			if (article == null) {
				return;
			}
			if (article.midx == 0) {
				$("#modal-delete-password").modal("show");
			} else {
				deleteArticle();
			}
		});
		$("#btn-delete-password").click(function(){
			if (!$("#passwd").val()) {
				alert("비밀번호를 입력해 주세요.");
				$("#passwd").focus();
				return;
			}
			deleteArticle($("#passwd").val());
		});
		
		$.ajax({type:"GET", url : "/api/board/"+code+"/"+idx, cache:false
			, data : {}
			, success : function(data) {
				if (data.error == 0) {
					cfg = data.data.cfg;
					me = data.data.me;
					article = data.data.article;
					
					// 작성 권한에 따른 버튼 표기
					if (cfg.permit_reply != "") {
						if (isAdmin()) {
							$("#btn-reply").removeClass('hidden');
						} else if (isLogin()) {
							var pers = cfg.permit_reply.split(',');
							var isOk = false;
							for (var i = 0; i < pers.length; i++) {
								if (pers[i] == me.gcd) {
									isOk = true;
								}
							}
							if (isOk) {
								$("#btn-reply").removeClass('hidden');
							}
						}
					} else {
						$("#btn-reply").removeClass('hidden');
					}
					
					if ((me != null && me.idx == article.midx) || article.midx == 0) {
						$("#btn-modify").removeClass("hidden");
						$("#btn-delete").removeClass("hidden");
					}

					$("#name").text(article.name);
					$("#date").text(formatDate(getDateFromFormat(article.regdate)));
					$("#read").text(article.cnt_read);
					$("#subject").text(article.subject);
					$("#content").html(article.html == "Y" ? article.content : nl2br(article.content));
					
					var files = data.data.files;
					if (files.length > 0) {
						var template = $("#file-item").html();
						Mustache.parse(template);
						
						var $fileList = $("#file-list");
						$fileList.removeClass("hidden");
						for (var i = 0; i < files.length; i++) {
							var f = files[i];
							var rendered = Mustache.render(template, {
								no:i+1, idx:f.idx, code:code, filename:f.name, size:formatSizeUnits(f.size)
							});
							$fileList.append(rendered);
						}
					}
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function initComments() {
		$commentList = $("#comment-list");
		if (cfg == null) {
			$.ajax({type:"GET", url : '/api/board/'+code+'/cfg', cache:false
				, data : {}
				, success : function(data) {
					if (data.error == 0) {
						cfg = data.data.cfg;
						me = data.data.me;
						initCommentForm();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		} else {
			initCommentForm();
		}
	}
	
	function initCommentForm() {
		if ('Y' == cfg.comment ) {
			if (cfg.permit_comment != '') {
				if (isAdmin()) {
					$("#comment-write-wrapper").removeClass('hidden');
				} else if (isLogin()) {
					var pers = cfg.permit_comment.split(',');
					var isOk = false;
					for (var i = 0; i < pers.length; i++) {
						if (pers[i] == me.gcd) {
							isOk = true;
						}
					}
					if (isOk) {
						$("#comment-write-wrapper").removeClass('hidden');
					}
				}
			} else {
				$("#comment-write-wrapper").removeClass('hidden');
				if (me == null) {
					$("#comment-write-id-wrapper").removeClass('hidden');
					$(".captcha-wrapper").removeClass("hidden");
					$("#captchaImg").click(function(){
						$("#captchaImg").attr("src", "/res/captcha?id=" + Math.random());
					});
				}
			}
			
			$("#comment").summernote({height:150, minHeight:150, maxHeight:300, focus:true, disableDragAndDrop: true});
			$("#comment-modify").summernote({height:150, minHeight:150, maxHeight:300, focus:true, disableDragAndDrop: true});
			$("#comment-reply").summernote({height:150, minHeight:150, maxHeight:300, focus:true, disableDragAndDrop: true});
			$("#btn-comment-write").click(function(){
				commentWriteConfirm();
			});
		}
		
		$(document).on('click', '.btn-comment-reply', function(){
			var idx = $(this).data('idx');
			$("#comment-reply-idx").val(idx);
			if (isLogin()) {
				$("#comment-reply-id-wrapper").addClass("hidden");
			} else {
				$("#comment-reply-id-wrapper").removeClass("hidden");
				$("#captchaImg-reply").attr("src", "/res/captcha?id=" + Math.random()+"&idx="+idx);
				$("#captchaImg-reply").click(function(){
					$("#captchaImg-reply").attr("src", "/res/captcha?id=" + Math.random()+"&idx="+idx);
				});
			}
			$("#modal-comment-reply").modal('show');
		});
		$(document).on('click', '.btn-comment-modify', function(){
			var idx = $(this).data('idx');
			var midx = parseInt($(this).data('midx'));
			$("#comment-modify-idx").val(idx);
			$("#comment-modify").summernote('code', $("#comment-content-"+idx).html());
			if (isAdmin()) {
				$("#comment-modify-passwd-wrapper").addClass("hidden");
				$("#modal-comment-modify").modal('show');
			} else {
				if (midx == 0) {
					$("#comment-modify-passwd-wrapper").removeClass("hidden");
					$("#modal-comment-modify").modal('show');
				} else if (me != null && me.idx == midx) {
					$("#comment-modify-passwd-wrapper").addClass("hidden");
					$("#modal-comment-modify").modal('show');
				} else {
					alert('본인의 댓글만 수정 할 수 있습니다.');
				}
			}
		});
		$(document).on('click', '.btn-comment-delete', function(){
			if (isAdmin()) {
				deleteComment($(this).data('idx'));
			} else {
				var midx = parseInt($(this).data('midx'));
				if (midx == 0) {
					$("#comment-delete-idx").val($(this).data('idx'));
					$("#modal-comment-delete-password").modal('show');
				} else {
					if (me == null) {
						alert('본인의 댓글만 삭제 할 수 있습니다.');
					} else if (me.idx != midx) {
						alert('본인의 댓글만 삭제 할 수 있습니다.');
					} else {
						deleteComment($(this).data('idx'));
					}
				}
			}
		});
		
		$("#btn-comment-modify").click(function(){
			modifyComment();
		});
		
		$("#btn-comment-reply").click(function(){
			if (!isLogin()) {
				var $replyName = $("#comment-reply-name");
				if (!$replyName.val()) {
					alert('이름을 입력해 주세요.');
					$replyName.focus();
					return;
				}
				
				var $replyPasswd = $("#comment-reply-passwd");
				if (!$replyPasswd.val()) {
					alert('비밀번호를 입력해 주세요.');
					$replyPasswd.focus();
					return;
				}
				
				if (!$("#comment-reply-captcha").val()) {
					alert('스팸방지코드를 입력해 주세요.');
					$("#comment-reply-captcha").focus();
					return;
				}
			} 
			
			var $name = $("#comment-reply-name");
			var $passwd = $("#comment-reply-passwd");
			var content = $('#comment-reply').summernote('code');
			var idx = $('#comment-reply-idx').val();
			$.ajax({type:"POST", url : '/api/board/'+code+'/'+paths[3]+'/comment', cache:false
				, data : {mode:'reply', content:content, html:'Y', idx:idx, name:$name.val(), passwd:$passwd.val(), captcha:$("#comment-reply-captcha").val()}
				, success : function(data) {
					if (data.error == 0) {
						$('#comment-reply').summernote('code', '');
						$("#comment-reply-captcha").val('');
						$('#comment-reply-idx').val('');
						$("#modal-comment-reply").modal('hide');
						$name.val("");
						$passwd.val("");
						
						loadCommentList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		});
		
		$("#btn-comment-delete-password").click(function(){
			var $commentPasswd = $("#comment-passwd");
			if (!$commentPasswd.val()) {
				alert('비밀번호를 입력해 주세요.');
				$commentPasswd.focus();
				return;
			}
			deleteComment($('#comment-delete-idx').val());
		});
	}
	
	function loadCommentList() {
		var idx = paths[3];
		$.ajax({type:"GET", url : '/api/board/'+code+'/'+idx+'/comment', cache:false
			, data : {}
			, success : function(data) {
				if (data.error == 0) {
					me = data.data.me;
					cfg = data.data.cfg;
					$commentList.empty();
					var comments = data.data.comments;
					var template = $("#comment-item").html();
					Mustache.parse(template);
					for (var i = 0; i < comments.length; i++) {
						var c = comments[i];
						if (c.thread.length > 1) {
							var thread = 'margin-left:'+(c.thread.length * 20)+'px;';
						} else {
							var thread = "";
						}
						
						var myComment = c.midx == 0 || (me != null && me.idx == c.midx);  
						$commentList.append(Mustache.render(template, {
							idx:c.idx, midx:c.midx, name:c.name
							, photo:c.midx == 0 ? '/resources/images/user.png' : '/res/profile/'+c.id
							, date:formatDate(getDateFromFormat(c.regdate))
							, thread:thread, comment:(c.html == "Y" ? c.content : nl2br(c.content))
							, me:myComment
						}));
					}
					if ('Y' == cfg.comment ) {
						if (cfg.permit_comment != '') {
							if (isAdmin()) {
								$(".btn-comment-reply").removeClass('hidden');
							} else if (isLogin()) {
								var pers = cfg.permit_comment.split(',');
								var isOk = false;
								for (var i = 0; i < pers.length; i++) {
									if (pers[i] == me.gcd) {
										isOk = true;
									}
								}
								if (isOk) {
									$(".btn-comment-reply").removeClass('hidden');
								}
							}
						} else {
							$(".btn-comment-reply").removeClass('hidden');
						}
					}
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function commentWriteConfirm() {
		if (me == null) {
			var $name = $("#comment-write-name");
			if (!$name.val()) {
				alert('이름을 입력해 주세요.');
				$name.focus();
				return;
			}
			var $passwd = $("#comment-write-passwd");
			if (!$passwd.val()) {
				alert('비밀번호를 입력해 주세요.');
				$passwd.focus();
				return;
			}
			var $comment = $("#comment");
			if ($comment.summernote('isEmpty') || $comment.summernote('code') == "") {
				alert("내용을 입력해 주세요");
				return;
			}
			
			if (!$("#comment-captcha").val()) {
				alert('스팸방지코드를 입력해 주세요.');
				$("#comment-captcha").focus();
				return;
			}
		} else {
			var $comment = $("#comment");
			if ($comment.summernote('isEmpty') || $comment.summernote('code') == "") {
				alert("내용을 입력해 주세요");
				return;
			}
		}
		
		$.ajax({type:"POST", url : '/api/board/'+code+'/'+paths[3]+'/comment', cache:false
			, data : {mode:'write', name:$("#comment-write-name").val(), passwd:$("#comment-write-passwd").val(), content:$("#comment").summernote('code'), html:'Y', captcha:$("#comment-captcha").val()}
			, success : function(data) {
				if (data.error == 0) {
					$('#comment').summernote('code', '');
					$("#comment-captcha").val("");
					$("#captchaImg").attr("src", "/res/captcha?id=" + Math.random());
					loadCommentList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function deleteArticle(passwd) {
		var idx = paths[3];
		
		$.ajax({type:"POST", url : "/api/board/"+code+"/"+idx+"/delete", cache:false
			, data : {passwd:passwd}
			, success : function(data) {
				if (data.error == 0) {
					document.location.href="/board/"+code;
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function modifyComment() {
		var content = $('#comment-modify').summernote('code');
		var idx = $('#comment-modify-idx').val();
		$.ajax({type:"POST", url : '/api/board/'+code+'/'+paths[3]+'/comment', cache:false
			, data : {mode:'modify', content:content, html:'Y', idx:idx, passwd:$("#comment-modify-passwd").val()}
			, success : function(data) {
				if (data.error == 0) {
					$('#comment-modify').summernote('code', '');
					$('#comment-modify-idx').val('');
					$("#modal-comment-modify").modal('hide');
					loadCommentList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function deleteComment(idx) {
		$.ajax({type:"GET", url : '/api/board/'+code+'/'+paths[3]+'/comment', cache:false
			, data : {mode:'delete', idx:idx, passwd:$("#comment-passwd").val()}
			, success : function(data) {
				if (data.error == 0) {
					$("#comment-delete-idx").val('');
					$("#modal-comment-delete-password").modal('hide');
					loadCommentList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function initModify() {
		var idx = paths[3];
		
		$files = $("#files");
		$btnModify = $("#btn-modify");
		$btnCancel = $("#btn-cancel");
		
		$btnModify.click(function(){
			$("#content").val($('#summernote').summernote('code'));
			
			$.ajax({type:"POST", url : "/api/board/"+code+"/"+idx+"/modify", cache:false
				, data : $("#submitForm").serialize()
				, success : function(data) {
					if (data.error == 0) {
						alert("수정되었습니다.");
						document.location.href='/board/'+code+'/'+idx;
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		});
		$btnCancel.click(function(){
			history.back();
		});
		
		$("#content").hide();
		$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true});
		$("#my-awesome-dropzone").attr('action', '/api/board/'+code+'/upload/dropzone');
		Dropzone.options.myAwesomeDropzone = {
			init: function () {
				this.on("complete", function (data) {
					var res = JSON.parse(data.xhr.responseText);
					if (res.status == "success") {
						files.push(res.data);
						$files.val(files.join());
					}
				});
				this.on("processing", function(file){
					this.options.url = $("#my-awesome-dropzone").attr("action");
				});
			}
		};
		
		$.ajax({type:"GET", url : "/api/board/"+code+"/"+idx, cache:false
			, data : {}
			, success : function(data) {
				if (data.error == 0) {
					cfg = data.data.cfg;
					me = data.data.me;
					article = data.data.article;
					
					if (!isAdmin() && article.midx == 0) {
						$("#id-wrapper").removeClass("hidden");
					}
					
					if ("Y" == cfg.notice && me != null) {
						$("#notice-wrapper").removeClass("hidden");
						$("#notice").prop("checked", article.notice == "Y");
					}
					if ("Y" == cfg.hidden) {
						$("#hidden-wrapper").removeClass("hidden");
						$("#hidden").prop("checked", article.hidden == "Y");
					}
					if ("Y" == cfg.file) {
						$("#file-wrapper").removeClass("hidden");
					}
					if ("Y" == cfg.category) {
						$("#category-wrapper").removeClass("hidden");
						var cats = cfg.categories.split(',');
						for (i = 0; i < cats.length; i++) {
							$("#category").append('<option value="'+cats[i]+'">'+cats[i]+'</option>');
						}
						$("#category").val(article.category);
					}

					$("#subject").val(article.subject);
					$("#content").html(article.html == "Y" ? article.content : nl2br(article.content));
					$('#summernote').summernote('code', article.html == "Y" ? article.content : nl2br(article.content))
					
					var fileList = data.data.files;
					for (var i = 0; i < fileList.length; i++) {
						files.push(fileList[i].idx);
					}
					$("#files").val(files.join());
					
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function initReply() {
		$files = $("#files");
		$btnReply = $("#btn-reply");
		$btnCancel = $("#btn-cancel");
		
		$btnReply.click(function(){
			replyConfirm();
		});
		$btnCancel.click(function(){
			history.back();
		});
		
		$("#content").hide();
		$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true});
		$("#my-awesome-dropzone").attr('action', '/api/board/'+code+'/upload/dropzone');
		Dropzone.options.myAwesomeDropzone = {
			init: function () {
				this.on("complete", function (data) {
					var res = JSON.parse(data.xhr.responseText);
					if (res.status == "success") {
						files.push(res.data);
						$files.val(files.join());
					}
				});
				this.on("processing", function(file){
					this.options.url = $("#my-awesome-dropzone").attr("action");
				});
			}
		};
		
		$.ajax({type:"GET", url : '/api/board/'+code+'/cfg', cache:false
			, data : {}
			, success : function(data) {
				if (data.error == 0) {
					cfg = data.data.cfg;
					me = data.data.me;
					
					if (me == null) {
						$("#id-wrapper").removeClass("hidden");
						$("#captcha-wrapper").removeClass("hidden");
						$("#captchaImg").click(function(){
							$("#captchaImg").attr("src", "/res/captcha?id=" + Math.random());
						});
					}
					
					if ("Y" == cfg.file) {
						$("#file-wrapper").removeClass("hidden");
					}
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	function replyConfirm() {
		if (!$("#subject").val()) {
			alert('제목을 입력해 주세요.');
			$("#subject").focus();
			return;
		}
		
		if ($('#summernote').summernote('isEmpty') || $('#summernote').summernote('code') == "") {
			alert("내용을 입력해 주세요");
			return;
		}
		
		if (me == null) {
			if (!$("#name").val()) {
				alert('이름을 입력해 주세요.');
				$("#name").focus();
				return;
			}
			if (!$("#passwd").val()) {
				alert('비밀번호를 입력해 주세요.');
				$("#passwd").focus();
				return;
			}
			
			if (!$("#captcha-form").val()) {
				alert('스팸방지코드를 입력해 주세요.');
				$("#captcha-form").focus();
				return;
			}
			
			$("#captcha").val($("#captcha-form").val());
		} 
		
		var idx = paths[3];
		$("#content").val($('#summernote').summernote('code'));
		
		$.ajax({type:"POST", url : "/api/board/"+code+"/"+idx+"/reply", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert("등록되었습니다.");
					document.location.href='/board/'+code;
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	}
	
	return {
		init : function() {
			paths = document.location.pathname.split('/');
			code = paths[2];
			$(".board-menu").each(function(i){
				if ($(this).data('code') == code) {
					$(this).addClass('active');
				}
			});
			switch (paths.length) {
			case 4:
				if (paths[paths.length-1] == 'write') { // Write
					initWrite();
				} else { // View
					initView();
					initComments();
					loadCommentList();
				}
				break;
			case 5:
				switch (paths[paths.length-1]) {
				case 'modify': // Modify
					initModify();
					break;
				case 'reply': // Reply
					initReply();
					break;
				case 'cert': // Cert
					initCert();
					break;
				}
				break;
			default: // List
				initList();
				loadList();
				break;
			}
		}
	};
}();