/**
 * MODULE : Workshop (default)
 * DATE : 2017-03-22
 * AUTHOR : 나태호 
 */

var code;
var category;
var idx;

$(function(){
	moment.locale('ko'); 
	code = getURLParameters("code");
	category = getURLParameters("category");
	idx = getURLParameters("idx");
	if (!idx) {
		$('#calendars').removeClass('hidden');
		loadConfiguration();
		loadCalendar();
	} else {
		loadWorkshop();
	}
	
	$(document).on('click', '.btn-category', function(){
		category = $(this).data('category');
		$('.btn-category').removeClass('selected');
		$(this).addClass('selected');
		if ($('#calendars').data('type') == 'list') {
			loadList(1);
		} else {
			$('#calendar').fullCalendar('refetchEvents');
		}
	});
	
	$(document).on('click', '.btn-workshop', function(){
		var type = $(this).data('type');
		$('.btn-workshop').removeClass('selected');
		$('.btn-workshop').each(function(i){
			if ($(this).data('type') == type) {
				$(this).addClass('selected');
			}
		});
		$('.check-modify').remove();
		switch (type) {
		case 'info':
			$('#workshop-content').removeClass('hidden');
			$('#workshop-registration').addClass('hidden');
			$('#checkForm').addClass('hidden');
			break;
		case 'registration':
			$('#workshop-content').addClass('hidden');
			$('#workshop-registration').removeClass('hidden');
			$('#submitForm')[0].reset();
			$('.lectures, .lecture-type, .courses').prop('disabled', false);
			$('.modify-forms').addClass('hidden');
			$('#checkForm').addClass('hidden');
			break;
		case 'check':
			$('#workshop-content').addClass('hidden');
			$('#workshop-registration').addClass('hidden');
			$('#checkForm').removeClass('hidden');
			break;
		}
	});
	
	$(document).on('click', '.address-picker', function(){
		openAddress();
	});
	
	$(document).on('click', '.lectures', function(){
		if (!$(this).prop('checked')) {
			var idx = $(this).data('idx');
			$('.lecture-type-'+idx).prop('checked', false);
			$('.course-'+idx).prop('checked', false);
		}
		lectureCost();
	});
	
	$(document).on('click', '.lecture-type', function(){
		if ($(this).prop('checked')) {
			var idx = $(this).data('idx');
			$('#lectures-'+idx).prop('checked', true);
		}
		lectureCost();
	});
	
	$(document).on('click', '.courses', function(){
		if ($(this).prop('checked')) {
			var idx = $(this).data('idx');
			$('#lectures-'+idx).prop('checked', true);
		}
		lectureCost();
	});
	
	$(document).on('submit', '#submitForm', function(){
		registrationSubmit();
		return false;
	});
	
	$(document).on('keyup', '#name', function(){
		if ($('#btn-same-name').prop('checked')) {
			$('#income_name').val($(this).val());
		}
	});
	
	$(document).on('click', '#btn-same-name', function(){
		if($(this).prop('checked')) {
			$('#income_name').val($('#name').val());
		} else {
			$('#income_name').val('');
		}
	});
	
	$(document).on('submit', '#checkForm', function(){
		checkRegistration();
		return false;
	});
	
	$(document).on('click', '.btn-location', function(){
		document.location.href=$(this).data('url');
	});
	$(document).on('click', '.btn-link', function(){
		window.open($(this).data('url'));
	});
	
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	
	$('#btn-search').click(function(){
		loadList($(this).data('page'));
	});
});

function openAddress() {
	new daum.Postcode({
        oncomplete: function(data) {
        	console.log(data);
        	$('#zipcode').val(data.zonecode);
        	$('#address').val(data.address);
        }
    }).open();
}

function getURLParameters(paramName) {
	var sURL = window.document.URL.toString();
	if (sURL.indexOf("?") > 0) {
		var arrParams = sURL.split("?");
		var arrURLParams = arrParams[1].split("&");
		var arrParamNames = new Array(arrURLParams.length);
		var arrParamValues = new Array(arrURLParams.length);
		
		var i = 0;
		for (i = 0; i<arrURLParams.length; i++) {
		    var sParam =  arrURLParams[i].split("=");
			arrParamNames[i] = sParam[0];
			if (sParam[1] != "")
				arrParamValues[i] = unescape(sParam[1]);
			else
				arrParamValues[i] = '';
		}
	
		for (i=0; i<arrURLParams.length; i++) {
			if (arrParamNames[i] == paramName) {
				return arrParamValues[i];
			}
		}
		return '';
	}
}

function loadConfiguration() {
	$.ajax({
	 	type:'GET', url:'/api/society/workshop/'+code+'/cfg', cache:false,
        success: function(data) {
        	if (data.error == 0) {
        		var cfg = data.data.cfg;
        		var categories = cfg.categories.split(',');
        		var $categories = $('#categories');
        		$categories.append('<li class="btn-category selected" data-category="">전체</li>');
        		for (var i = 0; i < categories.length; i++) {
        			var cat = categories[i];
        			$categories.append('<li class="btn-category" data-category="'+cat+'">'+cat+'</li>');
        		}
        	} else {
        	}
        }
	});
}

function loadCalendar() {
	var type = $('#calendars').data('type');
	if (type == 'list') {
		loadList(1);
	} else {
		$('#calendar').fullCalendar({
			locale:'ko'
			, header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay,listMonth'
			}
			, events : function(start, end, timezone, callback) {
				 $.ajax({
					 	type:'GET', url:'/api/society/workshop/fullcalendar', cache:false,
			            data: {
			            	code:code, category:category
			            	, startdate:start.format('YYYYMMDDHHmmss')
			            	, enddate:end.format('YYYYMMDDHHmmss')
			            },
			            success: function(data) {
			            	console.log(data);
			            	var events = data.data;
			            	for (var i = 0; i < events.length; i++) {
			            		events[i].start = moment(events[i].startdate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
			            		events[i].end = moment(events[i].enddate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
			            		events[i].url = document.location.pathname+'?code='+code+'&category='+category+'&idx='+events[i].idx;
			            	}
			            	callback(events);
			            }
				 });
			}
		});
	}
}

function loadList(page) {
	$.ajax({
	 	type:'GET', url:'/api/society/workshop/'+code, cache:false,
        data: {
        	page:page, category:category, keyword:$('#keyword').val()
        },
        success: function(data) {
        	console.log(data);
        	if (data.error == 0) {
        		
        		var list = data.data.list;
        		var paging = data.data.paging;
        		
        		var $list = $('#calendar');
        		var $pagination = $('#pagination');
        		$list.empty();
        		$pagination.empty();
        		
        		var templateList = $("#list-item").html();
        		var templatePaging = $("#paging-item").html();
        		Mustache.parse(templateList);
        		Mustache.parse(templatePaging);
        		
        		var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = virtualNo--;
					if (l.startdate) {
						l.date = moment(l.startdate, 'YYYYMMDDHHmmss').format('YYYY년 MM월 DD일 HH:mm');
					}
					if (l.enddate) {
						l.date += ' ~ ' + moment(l.enddate, 'YYYYMMDDHHmmss').format('YYYY년 MM월 DD일 HH:mm');
					}
					l.isdate = !l.date ? false : true;
					l.islocation = !l.location ? false : true;
					l.ishomepage = !l.homepage ? false : true;
					$list.append(Mustache.render(templateList, l));
				}
        		
        		$pagination.append(Mustache.render(templatePaging, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(templatePaging, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(templatePaging, {page:paging.nextBlockNo, pageTitle:"»"}));
        		
        	} else {
        		alert(data.message);
        	}
        }
	});
}

function loadWorkshop() {
	$.ajax({
	 	type:'GET', url:'/api/society/workshop/'+code+'/'+idx, cache:false,
        success: function(data) {
        	if (data.error == 0) {
        		var cfg = data.data.cfg;
        		var files = data.data.files;
        		
        		assortArticle(data.data.article, data.data.lectures);
        		assortFiles(files);
        		
        		$('#workshop-information').removeClass('hidden');
        	} else {
        	}
        }
	});
}

function assortArticle(article, lecture) {
	var $info = $('#workshop-information');
	
	article.isRegistration = article.registration == 'Y';
	if (article.isRegistration) {
		var sdate = moment(article.registration_startdate, 'YYYYMMDDHHmmss');
		var edate = moment(article.registration_enddate, 'YYYYMMDDHHmmss');
		var curdate = new Date();
		article.canRegistration = sdate.toDate().getTime() <= curdate.getTime() && curdate.getTime() <= edate.toDate().getTime();
	} else {
		article.canRegistration = false;
	}

	article.isbanner = !article.banner ? false : true;
	
	var tabs = $('#workshop-tab-item').html();
	Mustache.parse(tabs);
	$info.append(Mustache.render(tabs, article));
	
	article.date = moment(article.startdate, 'YYYYMMDDHHmmss').format('YYYY년 MM월 DD일 HH:mm') + ' ~ ' + moment(article.enddate, 'YYYYMMDDHHmmss').format('YYYY년 MM월 DD일 HH:mm'); 
	var info = $('#workshop-info-item').html();
	Mustache.parse(info);
	$info.append(Mustache.render(info, article));
	
	if (article.isRegistration) {
		var check = $('#workshop-check-item').html();
		Mustache.parse(check);
		$info.append(Mustache.render(check, {}));
	}
	
	if (article.canRegistration) {
		var registration = $('#workshop-registration-item').html();
		Mustache.parse(registration);
		$info.append(Mustache.render(registration, article));
		$(".datepicker").datepicker({todayBtn: "linked", keyboardNavigation: false, forceParse: false, autoclose: true, format: "yyyy-mm-dd"});
		if (lecture) {
			var $lw = $('#workshop-lecture-wrapper');
			var lectureTemplate = $('#workshop-lecture-item').html();
			Mustache.parse(lectureTemplate);
			for (var i = 0; i < lecture.length; i++) {
				var l = lecture[i];
				l.istypem = l.type == "M";
				l.istypel = l.type == "L";
				l.istypes = l.type == "S";
				l.istypen = l.type == "N";
				l.is_course = l.use_course=="Y";
				l.is_course1 = l.use_course=="Y" && l.course1;
				l.is_course2 = l.use_course=="Y" && l.course2;
				$lw.append(Mustache.render(lectureTemplate, l));
			}
		}
	}
}

function assortFiles(files) {
	if (!files) {
		return;
	}
	
	var imgTemplate = $('#workshop-image-item').html();
	var fileTemplate = $('#workshop-file-item').html();
	Mustache.parse(imgTemplate);
	Mustache.parse(fileTemplate);
	
	var $imgList = $('#workshop-image');
	var $fileList = $('#workshop-file');
	
	var no = 1;
	for (var i = 0; i < files.length; i++) {
		var f = files[i];
		var extensions = f.path.split('.');
		var extension = extensions[extensions.length - 1].toLowerCase(); 
		console.log(f);
		
		if (extension == 'jpg' || extension == 'png' || extension == 'gif') {
			$imgList.append(Mustache.render(imgTemplate, f));
		} else {
			f.no = no++;
			$fileList.append(Mustache.render(fileTemplate, f));
		}
		
	}
	
}

function lectureCost() {
	var total = 0;
	$('.lectures').each(function(){
		if ($(this).prop('checked')) {
			var idx = $(this).data('idx');
			if ($(this).data('type') == 'N') {
				total += parseInt($('#type-'+idx+'-1').data('cost'));
			} else {
				$('.lecture-type-'+idx).each(function(){
					if ($(this).prop('checked')) {
						total += parseInt($(this).data('cost'));
					}
				});
			}
		}
	});
	$('#income_cost').val(total);
}

function registrationSubmit() {
	var total = 0;
	var count = 0;
	$('.lectures').each(function(){
		if ($(this).prop('checked')) {
			var idx = $(this).data('idx');
			total++;
			if ($(this).data('type') == 'N') {
				count++;
			} else {
				$('.lecture-type-'+idx).each(function(){
					if ($(this).prop('checked')) {
						count++;
					}
				});
			}
		}
	});
	console.log(total + ' ' + count);
	if (total != count) {
		alert('선택한 행사는 참여방식을 선택해 주셔야 합니다.');
		return;
	}
	
	if (!checkRequired('name', '이름을 입력해 주세요.')) { return; }
	if (!checkRequired('name_eng', '영문 이름을 입력해 주세요.')) { return; }
	if (!checkRequired('license', '의사 면허번호를 입력해 주세요.')) { return; }
	if (!checkRequired('major', '전문 과목을 입력해 주세요.')) { return; }
	if (!checkRequired('office', '병원명을 입력해 주세요.')) { return; }
	if (!checkRequired('address', '근무처 주소를 입력해 주세요.')) { openAddress(); return; }
	if (!checkRequired('address_etc', '근무처 주소를 입력해 주세요.')) { return; }
	if (!checkRequired('mobile', '휴대폰 번호를 입력해 주세요.')) { return; }
	if (!checkRequired('email', '이메일 주소를 입력해 주세요.')) { return; }
	if (!checkEmail($('#email').val())) {
		alert('올바른 형태의 이메일 주소를 입력해 주세요.');
		$('#passwd_re').focus();
		return;
	}
	if (!checkRequired('income_date', '입금 예정일을 입력해 주세요.')) { return; }
	if (!checkRequired('income_name', '입금자 명을 입력해 주세요.')) { return; }
	
	var url = !$('#idx').val() ? '/api/society/workshop/'+code+'/'+idx+'/registration' : '/api/society/workshop/'+code+'/'+idx+'/registration/modify';
	$.ajax({
	 	type:'POST', url:url, cache:false,
	 	data : $('#submitForm').serialize(), 
        success: function(data) {
        	if (data.error == 0) {
        		alert(!$('#idx').val() ? '사전등록이 완료되었습니다.' : '사전등록이 수정되었습니다.');
        		$('.modify-forms').addClass('hidden');
        		$('#submitForm')[0].reset();
        		
        		$('#workshop-content').addClass('hidden');
    			$('#workshop-registration').addClass('hidden');
    			$('#checkForm').removeClass('hidden');
    			
    			$('.btn-workshop').removeClass('selected');
    			$('.btn-workshop').each(function(i){
    				if ($(this).data('type') == 'check') {
    					$(this).addClass('selected');
    				}
    			});
        	} else {
        		alert(data.message);
        	}
        }
	});
}

function checkRegistration() {
	if (!checkRequired('check-name', '이름을 입력해 주세요.')) { return; }
	if (!checkRequired('check-license', '의사 면허번호를 입력해 주세요.')) { return; }
	$.ajax({
	 	type:'POST', url:'/api/society/workshop/'+code+'/'+idx+'/registration/check', cache:false,
	 	data : $('#checkForm').serialize(), 
        success: function(data) {
        	console.log(data);
        	if (data.error == 0) {
        		assortModify(data.data.registration);
        	} else {
        		alert(data.message);
        	}
        }
	});
}

function assortModify(data) {
	$("#checkForm").addClass('hidden');
	$('#workshop-registration').removeClass('hidden');
	
	console.log(data);
	$('#modify-name').text(data.name);
	$('#idx').val(data.idx);
	$('#name').val(data.name);
	$('#name_eng').val(data.name_eng);
	$('#license').val(data.license);
	$('#major').val(data.major);
	$('#office').val(data.office);
	$('#zipcode').val(data.zipcode);
	$('#address').val(data.address);
	$('#address_etc').val(data.address_etc);
	$('#mobile').val(data.mobile);
	$('#email').val(data.email);
	$('#income_cost').val(data.income_cost);
	$('#income_date').val(data.income_date);
	$('#income_name').val(data.income_name);
	
	for (var i = 0; i < data.lectures.length; i++) {
		var l = data.lectures[i];
		$('#lectures-'+l.lidx).prop('checked', true);
		if (l.type == "1") {
			$('#type-'+l.lidx+'-1').prop('checked', true);
		} else {
			$('#type-'+l.lidx+'-2').prop('checked', true);
		}
		if (l.course1 == "Y") {
			$('#course1-'+l.lidx).prop('checked', true);
		}
		if (l.course2 == "Y") {
			$('#course2-'+l.lidx).prop('checked', true);
		}
	}
	$('.lectures, .lecture-type, .courses').prop('disabled', true);
	$('.modify-forms').removeClass('hidden');
	
}