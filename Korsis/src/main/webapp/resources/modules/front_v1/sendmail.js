/**
 * MODULE : Send Mail (Default)
 * DATE : 2017-01-20
 * AUTHOR : 나태호
 */

$(function(){
	$("#sendmail").submit(function(){
		var $to = $("#to");
		var $email = $("#email");
		var $content = $("#content");
		var ve = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
		
		if (!$email.val()) {
			alert("이메일을 입력해 주세요.");
			$email.focus();
			return false;
		}
		
		if (!ve.test($email.val())) {
			alert("올바른 형태의 이메일을 입력해 주세요.");
			$email.focus();
			return false;
		}
		
		if (!$content.val()) {
			alert("내용을 입력해 주세요.");
			$content.focus();
			return false;
		}
		
		var subject = "[문의] 새로운 문의가 등록되었습니다.";
		var content = "문의자 : "+$email.val()+"\n\n"+$content.val();
		var $btnSend = $("#btn-send"); 
		var $btnSending = $("#btn-sending");
		
		$btnSend.addClass("hidden");
		$btnSending.removeClass("hidden");
		
		$.ajax({type:"POST", url : "/api/sendmail", cache:false
			, data : {to:$to.val(), subject:subject, content:content}
			, success : function(data) {
				if (data.error == 0) {
					alert("등록되었습니다.\n\n빠른 시일내에 연락드리겠습니다.");
					
					$email.val("");
					$content.val("");
					$btnSend.removeClass("hidden");
					$btnSending.addClass("hidden");
				} else {
					alert(data.message);
					
					$btnSend.removeClass("hidden");
					$btnSending.addClass("hidden");
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
				
				$btnSend.removeClass("hidden");
				$btnSending.addClass("hidden");
			}
		});
		
		return false;
	});
	
});