<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${item.yadmNm}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>병원검색</li>
			<li>DB</li>
			<li>병원</li>
			<li class="active"><strong>${item.yadmNm}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>기본정보</h5>
				</div>
				<div class="ibox-content">
					<form name="basicForm" id="basicForm" method="post" class="form-horizontal">
						<input type="hidden" name="ykiho" value="${ykiho}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">병원종류</label>
							<div class="col-lg-9">
								<select name="clCd" class="form-control" >
									<c:forEach items="${codes}" var="c">
										<option value="${c.clCd}" <c:if test="${item.clCd eq c.clCd}">selected="selected"</c:if>>${c.clCdNm}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">병원명</label>
							<div class="col-lg-9">
								<input type="text" name="yadmNm" value="${fn:escapeXml(item.yadmNm)}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">전화번호</label>
							<div class="col-lg-9">
								<input type="text" name="telno" value="${item.telno }" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">홈페이지</label>
							<div class="col-lg-9">
								<input type="text" name="hospUrl" value="${item.hospUrl }" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">주소</label>
							<div class="col-lg-3">
								<input type="text" name="postNo" value="${item.postNo }" class="form-control" />
							</div>
							<div class="col-lg-6">
								<input type="text" name="addr" value="${item.addr }" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">GEO 좌표</label>
							<div class="col-lg-4">
								<input type="text" name="YPos" value="${item.YPos }" placeholder="위도"  class="form-control" />
							</div>
							<div class="col-lg-4">
								<input type="text" name="XPos" value="${item.XPos }" placeholder="경도" class="form-control"  />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">개원일</label>
							<div class="col-lg-9">
								<fmt:parseDate value="${item.estbDd}" var="date" pattern="yyyyMMdd"/>
								<input type="text" name="estbDd" value="<fmt:formatDate value="${date}" pattern="yyyy-MM-dd"/>" class="form-control datepicker" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox">
				<div class="ibox-title">
					<h5>보유 의사 수 (명)</h5>
				</div>
				<div class="ibox-content">
					<form name="doctorForm" id="doctorForm" method="post" class="form-horizontal">
						<input type="hidden" name="ykiho" value="${ykiho}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">전체 의사수</label>
							<div class="col-lg-9">
								<input type="number" name="drTotCnt" value="${item.drTotCnt}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">전문의 수</label>
							<div class="col-lg-9">
								<input type="number" name="sdrCnt" value="${item.sdrCnt}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">일반의 수</label>
							<div class="col-lg-9">
								<input type="number" name="gdrCnt" value="${item.gdrCnt}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">레지던트 수</label>
							<div class="col-lg-9">
								<input type="number" name="resdntCnt" value="${item.resdntCnt}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">인턴 수</label>
							<div class="col-lg-9">
								<input type="number" name="intnCnt" value="${item.intnCnt}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>상세 정보</h5>
				</div>
				<div class="ibox-content">
					<form name="detailForm" id="detailForm" method="post" class="form-horizontal">
						<input type="hidden" name="ykiho" value="${ykiho}" />
						<div class="form-group">
							<label class="col-lg-4 control-label">공공건물(장소) 이름</label>
							<div class="col-lg-8">
								<input type="text" name="plcNm" value="${detail.plcNm}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">공공건물(장소) 방향</label>
							<div class="col-lg-8">
								<input type="text" name="plcDir" value="${detail.plcDir}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">공공건물(장소) 거리</label>
							<div class="col-lg-8">
								<input type="text" name="plcDist" value="${detail.plcDist}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">주차 가능대수</label>
							<div class="col-lg-8">
								<input type="text" name="parkQty" value="${detail.parkQty}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">주차장 운영여부</label>
							<div class="col-lg-8">
								<label class="radio-inline">
									<input type="radio" name="parkXpnsYn" value="Y"<c:if test="${detail.parkXpnsYn eq 'Y'}"> checked="checked"</c:if>>
									운영
								</label>
								<label class="radio-inline">
									<input type="radio" name="parkXpnsYn" value="N"<c:if test="${detail.parkXpnsYn eq 'N'}"> checked="checked"</c:if>>
									운영안함
								</label>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">주차 안내</label>
							<div class="col-lg-8">
								<input type="text" name="parkEtc" value="${detail.parkEtc}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">일요일 휴진 안내</label>
							<div class="col-lg-8">
								<input type="text" name="noTrmtSun" value="${detail.noTrmtSun}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">공휴일 휴진 안내</label>
							<div class="col-lg-8">
								<input type="text" name="noTrmtHoli" value="${detail.noTrmtHoli}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">주간 응급실 운영</label>
							<div class="col-lg-8">
								<label class="radio-inline">
									<input type="radio" name="emyDayYn" value="Y"<c:if test="${detail.emyDayYn eq 'Y'}"> checked="checked"</c:if>>
									운영
								</label>
								<label class="radio-inline">
									<input type="radio" name="emyDayYn" value="N"<c:if test="${detail.emyDayYn eq 'N'}"> checked="checked"</c:if>>
									운영안함
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-4 control-label">주간 응급실 전화번호 1</label>
							<div class="col-lg-8">
								<input type="text" name="emyDayTelNo1" value="${detail.emyDayTelNo1}" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-4 control-label">주간 응급실 전화번호 2</label>
							<div class="col-lg-8">
								<input type="text" name="emyDayTelNo2" value="${detail.emyDayTelNo2}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">야간 응급실 운영</label>
							<div class="col-lg-8">
								<label class="radio-inline">
									<input type="radio" name="emyNgtYn" value="Y"<c:if test="${detail.emyNgtYn eq 'Y'}"> checked="checked"</c:if>>
									운영
								</label>
								<label class="radio-inline">
									<input type="radio" name="emyNgtYn" value="N"<c:if test="${detail.emyNgtYn eq 'N'}"> checked="checked"</c:if>>
									운영안함
								</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-4 control-label">야간 응급실 전화번호 1</label>
							<div class="col-lg-8">
								<input type="text" name="emyNgtTelNo1" value="${detail.emyNgtTelNo1}" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-4 control-label">야간 응급실 전화번호 2</label>
							<div class="col-lg-8">
								<input type="text" name="emyNgtTelNo2" value="${detail.emyNgtTelNo2}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">월~금 점심시간</label>
							<div class="col-lg-8">
								<input type="text" name="lunchWeek" value="${detail.lunchWeek}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">일요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtSunStart}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtSunStart" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtSunEnd}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtSunEnd" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">월요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtMonStart}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtMonStart" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtMonEnd}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtMonEnd" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">화요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtTueStart}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtTueStart" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtTueEnd}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtTueEnd" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">수요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtWedStart}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtWedStart" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtWedEnd}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtWedEnd" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">목요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtThuStart}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtThuStart" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtThuEnd}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtThuEnd" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">금요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtFriStart}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtFriStart" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtFriEnd}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtFriEnd" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">토요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtSatStart}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtSatStart" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${detail.trmtSatEnd}" var="date" pattern="HHmm"/>
								<input type="text" name="trmtSatEnd" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						
						<div class="text-right">
							<button class="btn btn-primary" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#basicForm").submit(function(){
		$.ajax({type:"GET", url:"/api/admin/db/hospital/modify/basic", cache:false
			, data : $("#basicForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$("#doctorForm").submit(function(){
		$.ajax({type:"GET", url:"/api/admin/db/hospital/modify/doctor", cache:false
			, data : $("#doctorForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$("#detailForm").submit(function(){
		$.ajax({type:"GET", url:"/api/admin/db/hospital/modify/detail", cache:false
			, data : $("#detailForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
});
</script>