<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>병원</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>병원검색</li>
			<li>DB</li>
			<li class="active"><strong>병원</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-9">
			<div class="ibox">
				<div class="ibox-title">
					<h5>병원 목록 <small>(총 : <span id="size">0</span>건)</small></h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center hidden-xs">번호</th>
									<th class="text-center">병원명</th>
									<th class="text-center hidden-xs">종류</th>
									<th class="text-center">전화번호</th>
									<th class="text-center hidden-xs">홈페이지</th>
									<th class="text-center hidden-xs">지역</th>
									<th class="text-center">지도</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="ibox">
				<div class="ibox-title">
					<h5>병원 검색</h5>
				</div>
				<div class="ibox-content" style="display: block;">
					<form role="form" method="get" class="form" onsubmit="return false;">
						<div class="form-group">
							<label for="code" class="sr-only">
								병원종류
							</label> 
							<select id="code" name="code" class="form-control">
								<option value="">병원종류선택</option>
								<c:forEach items="${codes}" var="c">
									<option value="${c.clCd}" <c:if test="${c.clCd eq param.code}">selected="selected"</c:if>>${c.clCdNm}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label for="subject" class="sr-only">
								진료과목
							</label> 
							<select id="subject" name="subject" class="form-control">
								<option value="">진료과목선택</option>
								<c:forEach items="${subjects}" var="s">
									<option value="${s.dgsbjtCd}" <c:if test="${s.dgsbjtCd eq param.subject}">selected="selected"</c:if>>${s.dgsbjtCdNm}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label for="keyword" class="sr-only">
								키워드
							</label> 
							<input type="text" placeholder="키워드를 입력하세요" id="keyword" name="keyword" value="${param.keyword}" class="form-control">
						</div>
						<div class="text-right">
							<button class="btn btn-white" id="btn-search" type="submit">검색</button>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox">
				<div class="ibox-title">
					<h5>병원 생성</h5>
				</div>
				<div class="ibox-content">
					<form role="form" id="addForm" method="post" class="form" onsubmit="return false;">
						<div class="form-group">
							<label for="keyword" class="sr-only">
								병원명
							</label> 
							<input type="text" placeholder="병원명을 입력해 주세요" name="yadmNm" value="" class="form-control">
						</div>
						<div class="text-right">
							<button class="btn btn-primary" id="btn-add" type="submit">생성</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
	<tr>
		<td class="text-center hidden-xs">{{no}}</td>
		<td>
			<a href="/admin/db/hospital/editor/{{ykiho}}">{{yadmNm}}</a>
		</td>
		<td class="text-center hidden-xs">{{clCdNm}}</td>
		<td class="text-center"><a href="tel:{{telno}}">{{telno}}</a></td>
		<td class="text-center hidden-xs">
			{{#hospUrl}}
				<a href="{{hospUrl}}" target="_blank" class="btn btn-default"><i class="fa fa-anchor"></i> 홈페이지</a>
			{{/hospUrl}}
			{{^hospUrl}}-{{/hospUrl}}
		</td>
		<td class="text-center hidden-xs">{{sgguCdNm}}</td>
		<td class="text-center">
			<a href="https://www.google.co.kr/maps/search/{{yadmNm}}/@{{YPos}},{{XPos}},18z?hl=ko" target="_blank"><span class="btn btn-default btn-circle"><i class="fa fa-map-marker"></i></span></a>
		</td>
		<td class="text-center">
			<button type="button" class="btn btn-danger btn-xs btn-delete" data-ykiho="{{ykiho}}"><i class="fa fa-warning"></i> 삭제</button>
		</td>
	</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	var keyword = $("#keyword").val();
	var code = $("#code").val();
	var subject = $("#subject").val();
	$.ajax({type:"GET", url:"/api/db/list/hospital", cache:false
		, data : {page:page, code:code, subject:subject, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				$("#size").text(numberWithCommas(paging.totalCount));
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, ykiho:l.ykiho, yadmNm:l.yadmNm, YPos:l.YPos, XPos:l.XPos, clCdNm:l.clCdNm, sgguCdNm:l.sgguCdNm
						, telno:l.telno, hospUrl:l.hospUrl
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadList(1);	
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	$("#btn-search").click(function(){
		loadList(1);
	});
	
	$("#btn-add").click(function(){
		$.ajax({type:"GET", url:"/api/admin/db/hospital/add", cache:false
			, data : $("#addForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert("생성되었습니다.");
					$("#addForm")[0].reset();
					loadList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm("선택하신 정보를 삭제하시겠습니까?")) {
			var ykiho = $(this).data("ykiho");
			$.ajax({type:"POST", url:"/api/admin/db/hospital/delete", cache:false
				, data : {ykiho:ykiho}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>