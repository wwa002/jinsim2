<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${dbgitem.subject}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li><a href="/admin/db/info">정보 DB</a></li>
			<li><a href="/admin/db/info/${dbitem.idx}">${dbitem.subject}</a></li>
			<li class="active"><strong>${dbgitem.subject}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>콘텐츠 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-2">번호</th>
									<th class="text-center col-md-2">이미지</th>
									<th class="text-center">제목</th>
									<th class="text-center col-md-2">삭제</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>컨텐츠 생성</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="dbidx" value="${dbidx }" />
						<input type="hidden" name="dbgidx" value="${dbgidx }" />
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">내용</label>
							<div class="col-lg-9">
								<textarea rows="5" name="content" class="form-control"></textarea>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<input type="file" name="image" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">배경</label>
							<div class="col-lg-9">
								<input type="file" name="background" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">Tag</label>
							<div class="col-lg-9">
								<input type="text" name="tag" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-create" type="submit">생성</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>그룹 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="modifyForm" id="modifyForm" method="post" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="dbidx" value="${dbidx}"/>
						<input type="hidden" name="idx" value="${dbgidx}"/>
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" value="${fn:escapeXml(dbgitem.subject)}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-modify" type="submit">수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
	
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">
		{{#image}}
			<img src="/api/db/info/content/icon/{{dbidx}}/{{dbgidx}}/{{idx}}/{{image}}?width=100&height=56" alt="" class="img-responsive" />
		{{/image}}
		{{^image}}
			-
		{{/image}}
	</td>
	<td>
		<a href="/admin/db/info/{{dbidx}}/{{dbgidx}}/{{idx}}">{{subject}}</a>
	</td>
	<td class="text-center">
		<button class="btn btn-danger btn-delete" type="button" data-dbidx="{{dbidx}}" data-dbgidx="{{dbgidx}}" data-idx="{{idx}}">삭제</button>
	</td>
</tr>
</script>

<script type="text/javascript">
function loadList() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/db/info/content", cache:false
		, data : {dbidx:'${dbidx}', dbgidx:'${dbgidx}'}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				var list = data.data;
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);

				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, dbidx:l.dbidx, dbgidx:l.dbgidx, idx:l.idx, subject:l.subject
						, image:l.image
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}
$(function(){
	loadList();
	$("#btn-search").click(function(){
		loadList();
	});
	
	$("#btn-modify").click(function(){
		$.ajax({type:"POST", url : "/api/admin/db/info/group/modify", cache:false
			, data : $("#modifyForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-create").click(function(){
		$("#submitForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/db/info/content/create"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					$("#submitForm")[0].reset();
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#submitForm").submit();
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var dbidx = $(this).data('dbidx');
			var dbgidx = $(this).data('dbgidx');
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url:"/api/admin/db/info/content/delete", cache:false
				, data : {dbidx:dbidx, dbgidx:dbgidx, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>