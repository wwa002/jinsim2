<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${dbitem.subject}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li><a href="/admin/db/info">정보 DB</a></li>
			<li class="active"><strong>${dbitem.subject}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>그룹 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-2">번호</th>
									<th class="text-center">제목</th>
									<th class="text-center col-md-2">삭제</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>그룹 생성</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="dbidx" value="${dbidx}"/>
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-create" type="submit">생성</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>정보 DB 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="modifyForm" id="modifyForm" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="idx" value="${dbidx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" value="${fn:escapeXml(dbitem.subject)}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">내용</label>
							<div class="col-lg-9">
								<textarea rows="5" name="content" class="form-control">${fn:escapeXml(dbitem.content)}</textarea>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<c:choose>
									<c:when test="${dbitem.image_path ne null and dbitem.image_path ne ''}">
										<div class="row">
											<div class="col-sm-12">
												<img src="/api/db/info/icon/${dbitem.idx}/${dbitem.image}" alt="" class="img-responsive" />
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="col-sm-12">
												<input type="file" name="image" class="form-control" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="file" name="image" class="form-control" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-modify" type="submit">수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
	
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td>
		<a href="/admin/db/info/{{dbidx}}/{{idx}}">{{subject}}</a>
	</td>
	<td class="text-center">
		<button class="btn btn-danger btn-delete" type="button" data-dbidx="{{dbidx}}" data-idx="{{idx}}">삭제</button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/db/info/group", cache:false
		, data : {dbidx:'${dbidx}'}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				var list = data.data;
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);

				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, dbidx:l.dbidx, idx:l.idx, subject:l.subject
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}
$(function(){
	loadList();
	$("#btn-search").click(function(){
		loadList();
	});
	
	$("#btn-modify").click(function(){
		$("#modifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/db/info/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#modifyForm").submit();
	});
	
	$("#btn-create").click(function(){
		$.ajax({type:"POST", url : "/api/admin/db/info/group/create", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$("#submitForm")[0].reset();
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var dbidx = $(this).data('dbidx');
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url:"/api/admin/db/info/group/delete", cache:false
				, data : {dbidx:dbidx, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>