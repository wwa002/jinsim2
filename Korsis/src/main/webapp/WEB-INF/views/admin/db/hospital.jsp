<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${item.value}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>병원검색</li>
			<li class="active"><strong>${item.value}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-md-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>검색될 병원 목록 <small>(총 <span id="search-size">0</span> 건)</small></h5>
				</div>
				<div class="ibox-content">
					<form name="searchForm" id="searchForm" onsubmit="return false;" class="form-horizontal">
						<div class="form-group">
							<div class="col-sm-6 col-sm-offset-6">
								<div class="input-group">
									<input type="text" placeholder="키워드를 입력하세요" id="search-keyword" name="keyword" class="form-control">
									<span class="input-group-btn">
										<button class="btn btn-white" id="btn-search" type="submit">검색</button>
									</span>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center hidden-xs">번호</th>
									<th class="text-center">병원명</th>
									<th class="text-center hidden-xs">종류</th>
									<th class="text-center">연락처</th>
									<th class="text-center">지도</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list-search"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination-search"></ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>DB 병원 목록 <small>(총 <span id="db-size">0</span> 건)</small></h5>
				</div>
				<div class="ibox-content">
					<form name="dbForm" id="dbForm" onsubmit="return false;" class="form-horizontal">
						<div class="form-group">
							<div class="col-sm-3">
								<select name="code" id="code" class="form-control">
									<option value="">병원종류선택</option>
									<c:forEach items="${codes}" var="c">
										<option value="${c.clCd}" <c:if test="${c.clCd eq param.hcode}">selected="selected"</c:if>>${c.clCdNm}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-3">
								<select name="subject" id="subject" class="form-control">
									<option value="">진료과목선택</option>
									<c:forEach items="${subjects}" var="s">
										<option value="${s.dgsbjtCd}" <c:if test="${s.dgsbjtCd eq param.hsubject}">selected="selected"</c:if>>${s.dgsbjtCdNm}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-6">
								<div class="input-group">
									<input type="text" placeholder="키워드를 입력하세요" name="keyword" id="keyword-db" class="form-control">
									<span class="input-group-btn">
										<button class="btn btn-white" id="btn-db-search" type="submit">검색</button>
									</span>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center hidden-xs">번호</th>
									<th class="text-center">병원명</th>
									<th class="text-center hidden-xs">종류</th>
									<th class="text-center">연락처</th>
									<th class="text-center">지도</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list-db"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination-db"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-search-item" type="x-tmpl-mustache">
	<tr>
		<td class="text-center hidden-xs">{{no}}</td>
		<td>
			<a href="https://www.google.co.kr/maps/search/{{yadmNm}}/@{{YPos}},{{XPos}},18z?hl=ko" target="_blank">{{yadmNm}}</a>
		</td>
		<td class="text-center hidden-xs">{{clCdNm}}</td>
		<td class="text-center">
			<a href="tel:{{telno}}">{{telno}}</a>
			{{#hospUrl}}
				<a href="{{hospUrl}}" target="_blank" class="btn btn-default"><i class="fa fa-anchor"></i></a>
			{{/hospUrl}}
		</td>
		<td class="text-center">
			<a href="https://www.google.co.kr/maps/search/{{yadmNm}}/@{{YPos}},{{XPos}},18z?hl=ko" target="_blank"><span class="btn btn-default btn-circle"><i class="fa fa-map-marker"></i></span></a>
		</td>
		<td class="text-center">
			<button type="button" class="btn btn-danger btn-delete" data-ykiho="{{ykiho}}">제외</button>
		</td>
	</tr>
</script>

<script id="paging-search-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-search-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script id="list-db-item" type="x-tmpl-mustache">
	<tr>
		<td class="text-center hidden-xs">{{no}}</td>
		<td>
			<a href="https://www.google.co.kr/maps/search/{{yadmNm}}/@{{YPos}},{{XPos}},18z?hl=ko" target="_blank">{{yadmNm}}</a>
		</td>
		<td class="text-center hidden-xs">{{clCdNm}}</td>
		<td class="text-center">
			<a href="tel:{{telno}}">{{telno}}</a>
			{{#hospUrl}}
				<a href="{{hospUrl}}" target="_blank" class="btn btn-default"><i class="fa fa-anchor"></i></a>
			{{/hospUrl}}
		</td>
		<td class="text-center">
			<a href="https://www.google.co.kr/maps/search/{{yadmNm}}/@{{YPos}},{{XPos}},18z?hl=ko" target="_blank"><span class="btn btn-default btn-circle"><i class="fa fa-map-marker"></i></span></a>
		</td>
		<td class="text-center">
			<button type="button" class="btn btn-primary btn-add" data-ykiho="{{ykiho}}">추가</button>
		</td>
	</tr>
</script>

<script id="paging-db-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-db-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
var idx = "${idx}";
function loadList(page) {
	var $list = $("#list-search");
	var $pagination = $("#pagination-search");
	var keyword = $("#search-keyword").val();
	$.ajax({type:"GET", url:"/api/admin/db/search/hospital/in", cache:false
		, data : {page:page, idx:idx, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				$("#search-size").text(numberWithCommas(paging.totalCount));
				
				var listTemplate = $("#list-search-item").html();
				var pagingTemplate = $("#paging-search-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, ykiho:l.ykiho, yadmNm:l.yadmNm, YPos:l.YPos, XPos:l.XPos, clCdNm:l.clCdNm
						, telno:l.telno, hospUrl:l.hospUrl
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadDBList(page) {
	var $list = $("#list-db");
	var $pagination = $("#pagination-db");
	var keyword = $("#keyword-db").val();
	var code = $("#code").val();
	var subject = $("#subject").val();
	$.ajax({type:"GET", url:"/api/admin/db/search/hospital/notin", cache:false
		, data : {page:page, code:code, subject:subject, idx:idx, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				$("#db-size").text(numberWithCommas(paging.totalCount));
				
				var listTemplate = $("#list-db-item").html();
				var pagingTemplate = $("#paging-db-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, ykiho:l.ykiho, yadmNm:l.yadmNm, YPos:l.YPos, XPos:l.XPos, clCdNm:l.clCdNm
						, telno:l.telno, hospUrl:l.hospUrl
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadList(1);	
	$(document).on('click', '.btn-search-page', function(){
		loadList($(this).data('page'));
	});
	$("#btn-search").click(function(){
		loadList(1);
	});
	
	loadDBList(1);	
	$(document).on('click', '.btn-db-page', function(){
		loadDBList($(this).data('page'));
	});
	$("#btn-db-search").click(function(){
		loadDBList(1);
	});
	
	$(document).on('click', '.btn-add', function(){
		var ykiho = $(this).data('ykiho');
		$.ajax({type:"POST", url:"/api/admin/db/search/hospital/add", cache:false
			, data : {idx:idx, ykiho:ykiho}
			, success : function(data) {
				if (data.error == 0) {
					loadList(1);
					loadDBList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		var ykiho = $(this).data('ykiho');
		$.ajax({type:"POST", url:"/api/admin/db/search/hospital/delete", cache:false
			, data : {idx:idx, ykiho:ykiho}
			, success : function(data) {
				if (data.error == 0) {
					loadList(1);
					loadDBList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>