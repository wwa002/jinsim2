<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${item.subject}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li><a href="/admin/db/cast">캐스트</a></li>
			<li class="active"><strong>${item.subject}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>${item.subject}</h5>
				</div>
				<div class="ibox-content">
					${item.content}
				</div>
			</div>
			
			<div id="list"></div>
			
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>페이지 생성</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="cidx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<input type="file" name="file" id="file" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-page-create" type="submit">생성</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>캐스트 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="modifyForm" id="modifyForm" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="idx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" value="${fn:escapeXml(item.subject)}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">내용</label>
							<div class="col-lg-9">
								<textarea rows="5" name="content" class="form-control">${fn:escapeXml(item.content)}</textarea>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<c:choose>
									<c:when test="${item.image_path ne null and item.image_path ne ''}">
										<div class="row">
											<div class="col-sm-12">
												<img src="/api/db/cast/icon/${item.idx}/${item.image}" alt="" class="img-responsive" />
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="col-sm-12">
												<input type="file" name="image" class="form-control" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="file" name="image" class="form-control" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">배경</label>
							<div class="col-lg-9">
								<c:choose>
									<c:when test="${item.background_path ne null and item.background_path ne ''}">
										<div class="row">
											<div class="col-sm-12">
												<img src="/api/db/cast/background/${item.idx}/${item.background}" alt="" class="img-responsive" />
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="col-sm-12">
												<input type="file" name="background" class="form-control" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="file" name="background" class="form-control" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">출처</label>
							<div class="col-lg-9">
								<input type="text" name="source" class="form-control" value="${fn:escapeXml(item.source)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">Tag</label>
							<div class="col-lg-9">
								<input type="text" name="tag" class="form-control" value="${fn:escapeXml(item.tag)}" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-modify" type="submit">수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script id="list-item" type="x-tmpl-mustache">
<div class="ibox">
	<div class="ibox-title">
		<h5>{{no}} 페이지</h5>
	</div>
	<div class="ibox-content">
		<div><img src="/api/db/cast/page/{{cidx}}/{{idx}}/{{image}}?width=500" alt="" class="img-responsive" /></div>
		<hr />
		<div>
			<form role="form" id="modifyForm-{{cidx}}-{{idx}}" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
				<input type="hidden" name="cidx" value="{{cidx}}" />
				<input type="hidden" name="idx" value="{{idx}}" />
				<div class="form-group">
					<label class="col-lg-3 control-label">이미지</label>
					<div class="col-lg-9">
						<input type="file" name="file" class="form-control" />
					</div>
				</div>
				<hr />
				<div class="text-right">
					<button class="btn btn-danger btn-page-delete" type="button" data-cidx="{{cidx}}" data-idx="{{idx}}">삭제</button>
					<button class="btn btn-warning btn-page-modify" data-cidx="{{cidx}}" data-idx="{{idx}}" type="submit">수정</button>
				</div>
			</form>
		</div>
	</div>
</div>
</script>

<script type="text/javascript">
function loadPages() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/db/cast/page", cache:false
		, data : {cidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				var list = data.data;
				
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);
				
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, cidx:l.cidx, idx:l.idx, image:l.image
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadPages();
	
	$("#btn-modify").click(function(){
		$("#modifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/db/cast/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#modifyForm").submit();
	});
	
	$("#btn-page-create").click(function(){
		$("#submitForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/db/cast/page/create"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					loadPages();
					$("#submitForm")[0].reset();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#submitForm").submit();
	});
	
	$(document).on('click', '.btn-page-modify', function(){
		var cidx = $(this).data('cidx');
		var idx = $(this).data('idx');
		var form = '#modifyForm-'+cidx+'-'+idx;
		$(form).ajaxForm({
			type:"POST"
			, url:"/api/admin/db/cast/page/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					loadPages();
					$(form)[0].reset();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$(form).submit();
	});
	
	$(document).on('click', '.btn-page-delete', function(){
		var cidx = $(this).data('cidx');
		var idx = $(this).data('idx');
		$.ajax({type:"GET", url : "/api/admin/db/cast/page/delete", cache:false
			, data : {cidx:cidx, idx:idx}
			, success : function(data) {
				if (data.error == 0) {
					loadPages();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>