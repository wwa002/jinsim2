<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>지도</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>병원검색</li>
			<li>DB</li>
			<li class="active"><strong>지도</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="ibox">
			<div class="ibox-title">
				<h5>지도</h5>
			</div>
			<div class="ibox-content">
				<div class="google-map" id="map"></div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>병원 목록</h5>
				</div>
				<div class="ibox-content" style="display: block;">
					<table id="hospital-list" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th class="col-md-3">병원명</th>
							<th class="col-md-3">전화번호</th>
							<th>주소</th>
						</tr>
					</thead>
					<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>약국 목록</h5>
				</div>
				<div class="ibox-content" style="display: block;">
					<table id="pharmacy-list" class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th class="col-md-3">약국명</th>
							<th class="col-md-3">전화번호</th>
							<th>주소</th>
						</tr>
					</thead>
					<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal inmodal in" id="hospital-detail" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<i class="fa fa-hospital-o modal-icon"></i>
					<h4 class="modal-title" id="hospital-name"></h4>
					<small id="hospital-code"></small>
				</div>
				<div class="modal-body">
					<table class="table table-striped">
					<tbody>
						<tr>
							<th class="col-lg-3 col-md-4 col-sm-4 col-xs-5">전화번호</th>
							<td id="hospital-tel"></td>
						</tr>
						<tr>
							<th>홈페이지</th>
							<td id="hospital-web"></td>
						</tr>
						<tr>
							<th>우편번호</th>
							<td id="hospital-zipcode"></td>
						</tr>
						<tr>
							<th>주소</th>
							<td id="hospital-address"></td>
						</tr>
						<tr>
							<th>전체의사</th>
							<td id="hospital-total"></td>
						</tr>
						<tr>
							<th>전문의</th>
							<td id="hospital-sdr"></td>
						</tr>
						<tr>
							<th>일반의</th>
							<td id="hospital-gdr"></td>
						</tr>
						<tr>
							<th>레지던트</th>
							<td id="hospital-resdnt"></td>
						</tr>
						<tr>
							<th>인턴</th>
							<td id="hospital-intn"></td>
						</tr>
					</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal inmodal in" id="pharmacy-detail" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<i class="fa fa-medkit modal-icon"></i>
					<h4 class="modal-title" id="pharmacy-name"></h4>
				</div>
				<div class="modal-body">
					<table class="table table-striped">
					<tbody>
						<tr>
							<th class="col-lg-3 col-md-4 col-sm-4 col-xs-5">전화번호</th>
							<td id="pharmacy-tel"></td>
						</tr>
						<tr>
							<th>우편번호</th>
							<td id="pharmacy-zipcode"></td>
						</tr>
						<tr>
							<th>주소</th>
							<td id="pharmacy-address"></td>
						</tr>
						<tr>
							<th>근무시간 (월)</th>
							<td id="pharmacy-time-mon"></td>
						</tr>
						<tr>
							<th>근무시간 (화)</th>
							<td id="pharmacy-time-tue"></td>
						</tr>
						<tr>
							<th>근무시간 (수)</th>
							<td id="pharmacy-time-wed"></td>
						</tr>
						<tr>
							<th>근무시간 (목)</th>
							<td id="pharmacy-time-thu"></td>
						</tr>
						<tr>
							<th>근무시간 (금)</th>
							<td id="pharmacy-time-fri"></td>
						</tr>
						<tr>
							<th>근무시간 (토)</th>
							<td id="pharmacy-time-sat"></td>
						</tr>
						<tr>
							<th>근무시간 (일)</th>
							<td id="pharmacy-time-sun"></td>
						</tr>
						<tr>
							<th>근무시간 (공휴일)</th>
							<td id="pharmacy-time-etc"></td>
						</tr>
					</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script src="/resources/bootstrap/inspinia/js/plugins/dataTables/datatables.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_KrFi4wysdJLy7SahETqhPM1Y3R7lIdc"></script>
<script type="text/javascript">
var map;
var markers = [];
var myLat;
var myLng;
var datatableOptions = {pageLength: 15, responsive: true, ordering:true, bLengthChange:false, search:false
		, language:{
			zeroRecords:"검색결과가 없습니다."
			, info:"_START_ ~ _END_ [검색결과 _TOTAL_ 건][전체 _MAX_ 건]"
			, infoEmpty : "0 ~ 0 [전체 0 건]"
			, infoFiltered : ""
			, paginate : { "previous" : "<", "next" : ">"}
}};

function init() {
	var mapOptions = {
            zoom: 16,
            center: new google.maps.LatLng(37.5609447,126.9795475)
        };
	var mapEl = document.getElementById('map');
	map = new google.maps.Map(mapEl, mapOptions);
	google.maps.event.addListener(map, 'dragend', loadData);
	google.maps.event.addListener(map, 'zoom_changed', loadData);
	google.maps.event.addListenerOnce(map, 'idle', loadData);
	getCurrentPosition();
}

function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

function clearMarkers() {
	setMapOnAll(null);
}

function showMarkers() {
	setMapOnAll(map);
}
function deleteMarkers() {
	clearMarkers();
	markers = [];
}

function getCurrentPosition() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(pos){
			myLat = pos.coords.latitude;
			myLng = pos.coords.longitude
			map.setCenter(new google.maps.LatLng(myLat, myLng));
			markers.push(new google.maps.Marker({position:{lat:myLat, lng:myLng}, map: map}));
			google.maps.event.addListenerOnce(map, 'idle', loadData);
		});
	}
}

function loadData() {
	var nelat  =   map.getBounds().getNorthEast().lat();   
	var nelng   =   map.getBounds().getNorthEast().lng();
	var swlat  =   map.getBounds().getSouthWest().lat();   
	var swlng   =   map.getBounds().getSouthWest().lng();   
    
    console.log("NE : " + nelat + ", " + nelng);
    console.log("SW : " + swlat + ", " + swlng);
    
    $.ajax({
		type : "GET"
			, url : "/api/db/search/medical"
			, cache : false
			, data : {nelat:nelat, nelng:nelng, swlat:swlat, swlng:swlng}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					deleteMarkers();
					if (myLat && myLng) {
						markers.push(new google.maps.Marker({position:{lat:myLat, lng:myLng}, map: map}));
					}
					
					var hospital = data.data.hospital;
					$("#hospital-list").DataTable().destroy();
					$("#hospital-list tbody").empty();
					for (var i = 0; i < hospital.length; i++) {
						var h = hospital[i];
						var html = "<tr data-ykiho=\""+h.ykiho+"\" class=\"link-hospital\"><td>"+h.yadmNm+"</td><td>"+h.telno+"</td><td>"+h.addr+"</td></tr>";
						$("#hospital-list tbody").append(html);

						var marker = new google.maps.Marker({
								position:{lat:parseFloat(h.lat), lng:parseFloat(h.lng)}
								, icon : "/resources/images/pin_hospital.png"
								, map: map
								, customType:"hospital"
								, customData:h.ykiho
							});
						google.maps.event.addListener(marker, 'click', function() {
							showHospital(this.customData);
						});
						markers.push(marker);
					}
					$("#hospital-list").DataTable(datatableOptions).draw();
					
					var pharmacy = data.data.pharmacy;
					$("#pharmacy-list").DataTable().destroy();
					$("#pharmacy-list tbody").empty();
					for (var i = 0; i < pharmacy.length; i++) {
						var p = pharmacy[i];
						var html = "<tr data-hpid=\""+p.hpid+"\" class=\"link-pharmacy\"><td>"+p.dutyName+"</td><td>"+p.dutyTel1+"</td><td>"+p.dutyAddr+"</td></tr>";
						$("#pharmacy-list tbody").append(html);
						
						var marker = new google.maps.Marker({
							position:{lat:parseFloat(p.lat), lng:parseFloat(p.lng)}
								, icon : "/resources/images/pin_pharmacy.png"
								, map: map
								, customType:"pharmacy"
								, customData:p.hpid
							}); 
						google.maps.event.addListener(marker, 'click', function() {
							showPharmacy(this.customData);
						});
						markers.push(marker);
					}
					$("#pharmacy-list").DataTable(datatableOptions).draw();
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

google.maps.event.addDomListener(window, 'load', init);

function setOpenTime(id, start, end) {
	if (start != "" && end != "") {
		$("#"+id).text(start.substring(0, 2)+":"+start.substring(2,4)+" ~ " +end.substring(0, 2)+":"+end.substring(2,4));
	} else {
		$("#"+id).text("-");
	}
}

function showHospital(ykiho) {
	if (!ykiho) {
		return;
	}
	
	$.ajax({
		type : "GET"
			, url : "/api/db/information/hospital"
			, cache : false
			, data : {ykiho:ykiho}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					$("#hospital-name").text(data.data.yadmNm);
					$("#hospital-code").text(data.data.clCdNm);
					$("#hospital-tel").text(data.data.telno);
					if (data.data.hospUrl == "") {
						$("#hospital-web").text("-");
					} else {
						$("#hospital-web").html('<a href="'+data.data.hospUrl+'" target="_blank">'+data.data.hospUrl+'</a>');
					}
					$("#hospital-zipcode").text(data.data.postNo);
					$("#hospital-address").text(data.data.addr);
					$("#hospital-total").text(data.data.drTotCnt + "명");
					$("#hospital-sdr").text(data.data.sdrCnt + "명");
					$("#hospital-gdr").text(data.data.gdrCnt + "명");
					$("#hospital-resdnt").text(data.data.resdntCnt + "명");
					$("#hospital-intn").text(data.data.intnCnt + "명");
					$("#hospital-detail").modal("show");
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function showPharmacy(hpid) {
	if (!hpid) {
		return;
	}
	$.ajax({
		type : "GET"
			, url : "/api/db/information/pharmacy"
			, cache : false
			, data : {hpid:hpid}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					$("#pharmacy-name").text(data.data.dutyName);
					$("#pharmacy-tel").text(data.data.dutyTel1);
					$("#pharmacy-zipcode").text(data.data.postCdn1+data.data.postCdn2);
					$("#pharmacy-address").text(data.data.dutyAddr);
					setOpenTime("pharmacy-time-mon", data.data.dutyTime1s, data.data.dutyTime1c);
					setOpenTime("pharmacy-time-tue", data.data.dutyTime2s, data.data.dutyTime2c);
					setOpenTime("pharmacy-time-wed", data.data.dutyTime3s, data.data.dutyTime3c);
					setOpenTime("pharmacy-time-thu", data.data.dutyTime4s, data.data.dutyTime4c);
					setOpenTime("pharmacy-time-fri", data.data.dutyTime5s, data.data.dutyTime5c);
					setOpenTime("pharmacy-time-sat", data.data.dutyTime6s, data.data.dutyTime6c);
					setOpenTime("pharmacy-time-sun", data.data.dutyTime7s, data.data.dutyTime7c);
					setOpenTime("pharmacy-time-etc", data.data.dutyTime8s, data.data.dutyTime8c);
					$("#pharmacy-detail").modal("show");
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

$(function(){
	$("#hospital-list").DataTable(datatableOptions);
	$("#pharmacy-list").DataTable(datatableOptions);
	
	$(document).on("click", ".link-hospital", function(){
		showHospital($(this).data("ykiho"));
	});
	
	$(document).on("click", ".link-pharmacy", function(){
		showPharmacy($(this).data("hpid"));
	});
});
</script>