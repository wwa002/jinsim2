<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${item.dutyName}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>병원검색</li>
			<li>DB</li>
			<li>약국</li>
			<li class="active"><strong>${item.dutyName}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>기본정보</h5>
				</div>
				<div class="ibox-content">
					<form name="basicForm" id="basicForm" class="form-horizontal">
						<input type="hidden" name="hpid" value="${hpid}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">약국명</label>
							<div class="col-lg-9">
								<input type="text" name="dutyName" value="${item.dutyName}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">전화번호</label>
							<div class="col-lg-9">
								<input type="text" name="dutyTel1" value="${item.dutyTel1 }" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">우편번호</label>
							<div class="col-lg-3">
								<input type="text" name="postCdn1" value="${item.postCdn1 }" class="form-control" />
							</div>
							<div class="col-lg-3">
								<input type="text" name="postCdn2" value="${item.postCdn2 }" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">주소</label>
							<div class="col-lg-9">
								<input type="text" name="dutyAddr" value="${item.dutyAddr }" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">간이약도</label>
							<div class="col-lg-9">
								<input type="text" name="dutyMapimg" value="${item.dutyMapimg }" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">메모</label>
							<div class="col-lg-9">
								<input type="text" name="dutyEtc" value="${item.dutyEtc }" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">GEO 좌표</label>
							<div class="col-lg-4">
								<input type="text" name="wgs84Lat" value="${item.wgs84Lat }" placeholder="위도"  class="form-control" />
							</div>
							<div class="col-lg-4">
								<input type="text" name="wgs84Lon" value="${item.wgs84Lon }" placeholder="경도" class="form-control"  />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>상세 정보</h5>
				</div>
				<div class="ibox-content">
					<form name="detailForm" id="detailForm" method="post" class="form-horizontal">
						<input type="hidden" name="hpid" value="${hpid}" />
						<div class="form-group">
							<label class="col-lg-4 control-label">공휴일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime8s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime8s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime8c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime8c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">일요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime7s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime7s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime7c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime7c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">월요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime1s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime1s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime1c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime1c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">화요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime2s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime2s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime2c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime2c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">수요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime3s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime3s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime3c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime3c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">목요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime4s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime4s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime4c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime4c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">금요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime5s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime5s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime5c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime5c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-4 control-label">토요일 영업시간</label>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime6s}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime6s" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
							<div class="col-lg-4">
								<fmt:parseDate value="${item.dutyTime6c}" var="date" pattern="HHmm"/>
								<input type="text" name="dutyTime6c" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						
						<div class="text-right">
							<button class="btn btn-primary" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#basicForm").submit(function(){
		$.ajax({type:"GET", url:"/api/admin/db/pharmacy/modify/basic", cache:false
			, data : $("#basicForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$("#detailForm").submit(function(){
		$.ajax({type:"GET", url:"/api/admin/db/pharmacy/modify/detail", cache:false
			, data : $("#detailForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});

});
</script>