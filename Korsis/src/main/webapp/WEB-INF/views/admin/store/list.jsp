<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>상점</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li class="active"><strong>상점</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>상점 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<div class="col-sm-offset-2 col-sm-2">
									<select id="type" class="form-control">
										<option value="">상점종류</option>
										<option value="STORE" <c:if test="${param.type eq 'STORE' }"> selected="selected"</c:if>>일반상점</option>
										<option value="HOSPITAL" <c:if test="${param.type eq 'HOSPITAL' }"> selected="selected"</c:if>>병원</option>
										<option value="ETC" <c:if test="${param.type eq 'ETC' }"> selected="selected"</c:if>>기타</option>
									</select>
								</div>
								<div class="col-sm-2">
									<select id="category" class="form-control"></select>
								</div>
								<div class="form-group">
									<div class="col-sm-6">
										<div class="input-group">
											<input type="text" id="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">종류</th>
									<th class="text-center">카테고리</th>
									<th class="text-center">상점명</th>
									<th class="text-center">전화번호</th>
									<th class="text-center">홈페이지</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>상점 등록</h5>
				</div>
				<div class="ibox-content">
					<form id="storeForm" class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-4 control-label">종류</label>
							<div class="col-sm-8">
								<select name="type" id="form-type" class="form-control">
									<option value="STORE">일반상점</option>
									<option value="HOSPITAL">병원</option>
									<option value="ETC">기타</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">카테고리</label>
							<div class="col-sm-8">
								<select name="category" id="form-category" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">상점명</label>
							<div class="col-sm-8">
								<input type="text" name="title" id="form-title" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="submit" class="btn btn-primary">생성</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{{typeHtml}}}</td>
	<td class="text-center">{{category_text}}</td>
	<td class="text-center"><a href="/admin/store/{{idx}}">{{title}}</a></td>
	<td class="text-center">{{phone}}</td>
	<td class="text-center">
		{{#isHomepage}}
			<a href="{{homepage}}" target="_blank"><i class="fa fa-anchor"></i> 홈페이지</a>
		{{/isHomepage}}
		{{^isHomepage}} - {{/isHomepage}}
	</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	var type = $("#type").val();
	var category = $("#category").val();
	var keyword = $("#keyword").val();
	$.ajax({type:"GET", url:"/api/admin/store/list", cache:false
		, data : {page:page, type:type, category:category, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.typeHtml = "";
					switch (l.type) {
					case 'STORE':
						l.typeHtml = '<span class="label label-default">일반상점</span>';
						break;
					case 'HOSPITAL':
						l.typeHtml = '<span class="label label-info">병원</span>';
						break;
					case 'ETC':
						l.typeHtml = '<span class="label label-warning">기타</span>';
						break;
					}
					l.no = virtualNo--;
					l.isHomepage = l.homepage ? true : false;
					$list.append(Mustache.render(listTemplate, l));
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadCategory() {
	$.ajax({
		type : "GET"
			, url : "/api/event/category"
			, cache : false
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					$("#category").empty();
					$('#category').append('<option value="">카테고리</option>');
					$("#form-category").empty();
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						$('#category').append('<option value="'+d.idx+'">'+d.value+'</option>');
						$("#form-category").append('<option value="'+d.idx+'">'+d.value+'</option>');
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

$(function(){
	loadList(1);
	loadCategory();
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	$("#btn-search").click(function(){
		loadList(1);
	});
	
	$('#storeForm').submit(function(){
		$.ajax({type:"POST", url:"/api/admin/store/create", cache:false
			, data : $('#storeForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					loadList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm("선택하신 상점을 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/admin/store/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>