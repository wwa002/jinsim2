<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>상점</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li class="active"><strong>상점</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>상점 정보 수정</h5>
				</div>
				<div class="ibox-content">
					<form id="storeModifyForm" class="form-horizontal">
						<input type="hidden" name="idx" value="${idx}" />
						<div class="form-group">
							<label class="col-sm-4 control-label">종류</label>
							<div class="col-sm-8">
								<select name="type" id="type" class="form-control">
									<option value="STORE" <c:if test="${item.type eq 'STORE'}">selected="selected"</c:if>>일반상점</option>
									<option value="HOSPITAL" <c:if test="${item.type eq 'HOSPITAL'}">selected="selected"</c:if>>병원</option>
									<option value="ETC" <c:if test="${item.type eq 'ETC'}">selected="selected"</c:if>>기타</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">카테고리</label>
							<div class="col-sm-8">
								<select name="category" id="category" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">상점명</label>
							<div class="col-sm-8">
								<input type="text" name="title" value="${item.title}" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">연락처</label>
							<div class="col-sm-8">
								<input type="text" name="phone" value="${item.phone}" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">홈페이지</label>
							<div class="col-sm-8">
								<input type="text" name="homepage" value="${item.homepage}" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">우편번호</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" name="zipcode" id="zipcode" value="${item.zipcode}" class="form-control address-picker" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default address-picker"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">기본주소</label>
							<div class="col-sm-8">
								<input type="text" name="address" id="address" value="${item.address}" class="form-control address-picker" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">상세주소</label>
							<div class="col-sm-8">
								<input type="text" name="address_etc" id="address_etc" value="${item.address_etc}" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">위도</label>
							<div class="col-sm-8">
								<input type="text" name="latitude" id="latitude" value="${item.latitude}" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">경도</label>
							<div class="col-sm-8">
								<input type="text" name="longitude" id="longitude" value="${item.longitude}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-sm-4 control-label">일요일</label>
							<div class="col-sm-4">
								<input type="text" name="sun_start" id="sun_start" value="${item.sun_start}" class="form-control clockpicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="sun_end" id="sun_end" value="${item.sun_end}" class="form-control clockpicker" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">월요일</label>
							<div class="col-sm-4">
								<input type="text" name="mon_start" id="mon_start" value="${item.mon_start}" class="form-control clockpicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="mon_end" id="mon_end" value="${item.mon_end}" class="form-control clockpicker" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">화요일</label>
							<div class="col-sm-4">
								<input type="text" name="tue_start" id="tue_start" value="${item.tue_start}" class="form-control clockpicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="tue_end" id="tue_end" value="${item.tue_end}" class="form-control clockpicker" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">수요일</label>
							<div class="col-sm-4">
								<input type="text" name="wed_start" id="wed_start" value="${item.wed_start}" class="form-control clockpicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="wed_end" id="wed_end" value="${item.wed_end}" class="form-control clockpicker" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">목요일</label>
							<div class="col-sm-4">
								<input type="text" name="thu_start" id="thu_start" value="${item.thu_start}" class="form-control clockpicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="thu_end" id="thu_end" value="${item.thu_end}" class="form-control clockpicker" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">금요일</label>
							<div class="col-sm-4">
								<input type="text" name="fri_start" id="fri_start" value="${item.fri_start}" class="form-control clockpicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="fri_end" id="fri_end" value="${item.fri_end}" class="form-control clockpicker" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">토요일</label>
							<div class="col-sm-4">
								<input type="text" name="sat_start" id="sat_start" value="${item.sat_start}" class="form-control clockpicker" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="sat_end" id="sat_end" value="${item.sat_end}" class="form-control clockpicker" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4 text-right">
								<button type="submit" class="btn btn-primary">저장</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>상점 사진</h5>
				</div>
				<div class="ibox-content">
					<div id="list"></div>
					<form role="form" name="imageForm" id="imageForm" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="sidx" value="${idx}" />
						<div class="form-group">
							<div class="col-lg-12">
								<div class="input-group">
									<input type="file" name="file" id="file" class="form-control" />
									<span class="input-group-btn">
										<button class="btn btn-primary" id="btn-photo-add" type="submit"><i class="fa fa-plus"></i></button>
									</span>
								</div>
								
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>관리 회원 편집</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-xs-10">
							<select id="manager" class="form-control"></select>
						</div>
						<div class="col-xs-2">
							<button id="btn-manager-add" type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-2" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">회원명</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list-manager"></tbody>
						</table>
					</div>
				</div>
			</div>
			
			<div id="hospital-wrapper" class="ibox hidden">
				<div class="ibox-title">
					<h5>관리대상 병원 편집</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
										<input type="text" name="keyword" id="keyword" maxlength="10" class="form-control" placeholder="병원명을 입력해 주세요" />
										<span class="input-group-btn">
											<button type="button" id="btn-search-hospital" class="btn btn-default"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="row hidden" id="hospital-wrapper">
								<div class="col-sm-12">
									<hr />
									<div class="row">
										<div class="col-xs-10">
											<select id="hospital" class="form-control"></select>
										</div>
										<div class="col-xs-2">
											<button id="btn-hospital-add" type="button" class="btn btn-primary"><i class="fa fa-plus"></i></button>
										</div>
									</div>
								</div>
							</div>
							<hr />
						</div>
					</div>
					
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-2" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">병원명</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list-hospital"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<!-- Daum 주소 -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script id="manager-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{name}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-danger btn-manager-delete" data-midx="{{midx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="hospital-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{title}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-danger btn-hospital-delete" data-ykiho="{{ykiho}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="list-item" type="x-tmpl-mustache">
<div class="row">
	<div class="col-sm-12">
		<img src="/api/store/{{sidx}}/{{idx}}/{{name}}?width=500" alt="" class="img-responsive" />
		<button class="btn btn-danger btn-photo-delete pull-right" type="button" data-sidx="{{sidx}}" data-idx="{{idx}}">삭제</button>
	</div>
</div>
<hr />
</script>

<script type="text/javascript">
var type = '${item.type}';
var category = '${item.category}';
function loadManagers() {
	$.ajax({type:"GET", url:"/api/admin/store/managers", cache:false
		, success : function(data) {
			if (data.error == 0) {
				console.log(data);
				var $list = $("#manager");
				var list = data.data;
				$list.empty();
				for (var i = 0; i < list.length; i++) {
					var d = list[i];
					$list.append('<option value="'+d.idx+'">'+d.name+'</option>');
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadManagersList() {
	var $list = $("#list-manager");
	$.ajax({type:"GET", url:"/api/admin/store/manager/list", cache:false
		, data : {sidx:'${idx}'}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				
				var list = data.data;
				var listTemplate = $("#manager-item").html();
				Mustache.parse(listTemplate);
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, midx:l.midx, name:l.name
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadHospitalsList() {
	var $list = $("#list-hospital");
	$.ajax({type:"GET", url:"/api/admin/store/hospital/list", cache:false
		, data : {sidx:'${idx}'}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				
				var list = data.data;
				var listTemplate = $("#hospital-item").html();
				Mustache.parse(listTemplate);
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, ykiho:l.ykiho, title:l.yadmNm
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadCategory() {
	$.ajax({
		type : "GET"
			, url : "/api/event/category"
			, cache : false
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					$("#category").empty();
					$('#category').append('<option value="">카테고리</option>');
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						$('#category').append('<option value="'+d.idx+'">'+d.value+'</option>');
					}
					$('#category').val(category);
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function getGeocode(address) {
	$.ajax({
		type : "GET"
			, url : "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=ko&address="+address
			, cache : false
			, success : function(data){
				if (data.results.length > 0) {
					var lat = data.results[0].geometry.location.lat;
					var lng = data.results[0].geometry.location.lng;
					$('#latitude').val(lat);
					$('#longitude').val(lng);
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function loadPages() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/store/photo/list", cache:false
		, data : {sidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				var list = data.data;
				
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);
				
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					$list.append(Mustache.render(listTemplate, l));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	loadPages();
	loadCategory();
	loadManagers();
	loadManagersList();
	loadHospitalsList();
	if (type == 'HOSPITAL') {
		$('#hospital-wrapper').removeClass('hidden');
	}
	$('#type').change(function(){
		if ($(this).val() == 'HOSPITAL') {
			$('#hospital-wrapper').removeClass('hidden');
		} else {
			$('#hospital-wrapper').addClass('hidden');
		}
	});
	
	$('.address-picker').click(function(){
		new daum.Postcode({
	        oncomplete: function(data) {
	        	$('#zipcode').val(data.zonecode);
	        	$('#address').val(data.address);
	        	getGeocode(data.address);
	        }
	    }).open();
	});
	
	$('#storeModifyForm').submit(function(){
		$.ajax({type:"GET", url:"/api/admin/store/modify", cache:false
			, data : $('#storeModifyForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('수정되었습니다.');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$("#btn-photo-add").click(function(){
		$("#imageForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/store/photo/add"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					loadPages();
					$("#imageForm")[0].reset();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#imageForm").submit();
	});
	
	$(document).on('click', '.btn-photo-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var sidx = $(this).data('sidx');
			var idx = $(this).data('idx');
			$.ajax({type:"GET", url:"/api/admin/store/photo/remove", cache:false
				, data : {sidx:sidx, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadPages();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	
	$("#btn-manager-add").click(function(){
		var midx = $("#manager").val();
		if (!midx) {
			alert("관리할 회원을 선택해 주세요.");
			return;
		}
		
		$.ajax({type:"GET", url:"/api/admin/store/manager/add", cache:false
			, data : {sidx:'${idx}', midx:midx}
			, success : function(data) {
				if (data.error == 0) {
					loadManagersList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-manager-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var midx=$(this).data('midx');
			$.ajax({type:"GET", url:"/api/admin/store/manager/delete", cache:false
				, data : {sidx:'${idx}', midx:midx}
				, success : function(data) {
					if (data.error == 0) {
						loadManagersList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$("#btn-search-hospital").click(function(){
		var $keyword = $("#keyword");
		if (!$keyword.val()) {
			alert("키워드를 입력해 주세요.");
			$keyword.focus();
			return;
		}
		$.ajax({type:"GET", url:"/api/admin/store/hospitals", cache:false
			, data : {keyword:$keyword.val()}
			, success : function(data) {
				if (data.error == 0) {
					console.log(data);
					var $list = $("#hospital");
					var list = data.data;
					$list.empty();
					for (var i = 0; i < list.length; i++) {
						var d = list[i];
						$list.append('<option value="'+d.ykiho+'">'+d.yadmNm+'</option>');
					}
					$("#hospital-wrapper").removeClass("hidden");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-hospital-add").click(function(){
		var ykiho = $("#hospital").val();
		if (!ykiho) {
			alert("관리대상 병원을 선택해 주세요.");
			return;
		}
		
		$.ajax({type:"GET", url:"/api/admin/store/hospital/add", cache:false
			, data : {sidx:'${idx}', ykiho:ykiho}
			, success : function(data) {
				if (data.error == 0) {
					loadHospitalsList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-hospital-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var ykiho=$(this).data('ykiho');
			$.ajax({type:"GET", url:"/api/admin/store/hospital/delete", cache:false
				, data : {sidx:'${idx}', ykiho:ykiho}
				, success : function(data) {
					if (data.error == 0) {
						loadHospitalsList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
});
</script>