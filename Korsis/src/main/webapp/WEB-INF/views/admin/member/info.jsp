<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>접속통계</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li class="active"><strong>접속통계</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>기본 정보</h5>
				</div>
				<div class="ibox-content">
					<form id="memberModifyForm" class="form-horizontal" enctype="multipart/form-data" onsubmit="return false;">
						<input type="hidden" name="idx" id="idx-modify" />
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호</label>
							<div class="col-sm-8">
								<input type="password" name="passwd" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">회원타입</label>
							<div class="col-sm-8">
								<select name="type" id="type-modify" class="form-control">
									<option value="MEMBER">일반회원</option>
									<option value="ADMIN">관리자</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이름</label>
							<div class="col-sm-4">
								<input type="text" name="name_last" id="name_last-modify" placeholder="성" class="form-control" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="name_first" id="name_first-modify" placeholder="이름" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">별명</label>
							<div class="col-sm-8">
								<input type="text" name="nickname" id="nickname-modify" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이메일</label>
							<div class="col-sm-8">
								<input type="text" name="email" id="email-modify" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">생일</label>
							<div class="col-sm-8">
								<input type="text" name="birth" id="birth-modify" class="form-control datepicker" maxlength="10" placeholder="yyyy-MM-dd" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">성별</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="M" name="sex" id="sex-m-modify"> <i></i> 남성 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="F" name="sex" id="sex-f-modify"> <i></i> 여성 
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">프로필</label>
							<div class="col-sm-8">
								<input type="file" name="file" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="isuse" id="isuse-y-modify"> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="isuse" id="isuse-n-modify"> <i></i> 차단
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="member-modify" class="btn btn-primary">정보수정</button>
								<button type="reset" id="member-modify-cancel" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<c:if test="${siteInfo.society eq 'Y'}">
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-title">
						<h5>학회 정보</h5>
					</div>
					<div class="ibox-content">
					</div>
				</div>
			</div>
		</c:if>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>등급 정보</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<ul id="gcd-list"></ul>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-9">
							<select id="gcd" class="form-control">
								<option value="">회원등급</option>
								<c:forEach items="${gcds}" var="g">
									<option value="${g.gcd}" <c:if test="${param.gcd eq g.gcd }"> selected="selected"</c:if>>${g.title}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-xs-3">
							<button id="btn-gcd-add" class="btn btn-block btn-primary">추가</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="gcd-item" type="x-tmpl-mustache">
<li>
	{{title}} - 등록일({{regdate}}) <button type="button" data-gcd="{{gcd}}" class="btn btn-xs btn-danger btn-gcd-delete"><i class="fa fa-trash"></i></button>
</li>
</script>

<script type="text/javascript">
function loadDefaultInfo() {
	$.ajax({type:"GET", url:"/api/admin/member/info", cache:false
		, data : {id:'${id}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$("#idx-modify").val(data.data.idx);
				$("#type-modify").val(data.data.type);
				var name = data.data.name;
				if (name) {
					$("#name_last-modify").val(name.substring(0, 1));
					$("#name_first-modify").val(name.substring(1));
				}
				$("#nickname-modify").val(data.data.nickname);
				$("#email-modify").val(data.data.email);
				var birth = data.data.birth;
				if (birth) {
					var text = birth.substring(0, 4);
					text += "-" + birth.substring(4, 6);
					text += "-" + birth.substring(6, 8);
					$("#birth-modify").val(text);
				}
				if (data.data.sex == "F") {
					$("#sex-f-modify").iCheck("check");
				} else {
					$("#sex-m-modify").iCheck("check");
				}
				if (data.data.isuse == "Y") {
					$("#isuse-y-modify").iCheck("check");
				} else {
					$("#isuse-n-modify").iCheck("check");
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadGCDInfo() {
	$.ajax({type:"GET", url:"/api/admin/member/gcd/list", cache:false
		, data : {id:'${id}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $gcdList = $("#gcd-list");
				$gcdList.empty();
				
				var list = data.data;
				var template = $("#gcd-item").html();
				Mustache.parse(template);
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(template, {
						title:l.title, gcd:l.gcd, regdate:getDefaultDataFormat(l.regdate)
					});
					$gcdList.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadDefaultInfo();
	loadGCDInfo();
	
	$("#member-modify").click(function(){
		$("#memberModifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/member/save"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#memberModifyForm").submit();
	});
	
	$("#btn-gcd-add").click(function(){
		var gcd = $("#gcd").val();
		if (!gcd) {
			alert('등급을 선택해 주세요.');
			$("#gcd").focus();
			return;
		}
		
		$.ajax({type:"POST", url:"/api/admin/member/gcd/add", cache:false
			, data : {id:'${id}', gcd:gcd}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-gcd-delete', function(){
		var gcd = $(this).data("gcd");
		
		$.ajax({type:"POST", url:"/api/admin/member/gcd/delete", cache:false
			, data : {id:'${id}', gcd:gcd}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>