<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>접속통계</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li class="active"><strong>접속통계</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-2">
			<select id="year" class="form-control">
				<fmt:formatDate value="${today}" pattern="yyyy" var="start"/> 
				<c:forEach begin="0" end="5" var="i" step="1">
					<option value="${start-i}">${start-i}년</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-sm-2">
			<select id="month" class="form-control">
				<fmt:formatDate value="${today}" pattern="MM" var="month"/>
				<c:forEach begin="1" end="12" var="i" step="1">
					<c:choose>
						<c:when test="${i < 10 }">
							<option value="0${i}" <c:if test="${i eq month}">selected="selected"</c:if>>${i}월</option>
						</c:when>
						<c:otherwise>
							<option value="${i}" <c:if test="${i eq month}">selected="selected"</c:if>>${i}월</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select>
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-sm-12">
			<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class=""><a data-toggle="tab" href="#tab-1">자료분석</a></li>
					<li class="active"><a data-toggle="tab" href="#tab-2">접속통계</a></li>
					<li class=""><a data-toggle="tab" href="#tab-3">브라우져</a></li>
					<li class=""><a data-toggle="tab" href="#tab-4">내부키워드</a></li>
					<li class=""><a data-toggle="tab" href="#tab-5">유입키워드</a></li>
					<li class=""><a data-toggle="tab" href="#tab-6">접속경로</a></li>
					<li class=""><a data-toggle="tab" href="#tab-7">인기URL</a></li>
				</ul>
				<div class="tab-content">
					<div id="tab-1" class="tab-pane">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<colgroup>
										<col class="col-sm-2" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-*" />
									</colgroup>
									<thead>
										<th class="text-center">날짜</th>
										<th class="text-center">방문자</th>
										<th class="text-center">로그인</th>
										<th class="text-center">회원가입</th>
										<th class="text-center">탈퇴</th>
										<th class="text-center">게시물</th>
										<th class="text-center">댓글</th>
										<th class="text-center">업로드</th>
										<th class="text-center">다운로드</th>
										<th></th>
									</thead>
									<tbody id="list-analytics"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="tab-2" class="tab-pane active">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<colgroup>
										<col class="col-sm-2" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-*" />
									</colgroup>
									<thead>
										<th class="text-center">날짜</th>
										<th class="text-center">순방문</th>
										<th class="text-center">페이지뷰</th>
										<th class="text-center">평균뷰</th>
										<th></th>
									</thead>
									<tbody id="list-counter"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="tab-3" class="tab-pane">
						<div class="panel-body">
							<div class="flot-chart-pie-content" id="browser-chart" style="width:500px; height:400px;"></div>
						</div>
					</div>
					<div id="tab-4" class="tab-pane">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<colgroup>
										<col class="col-sm-2" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-*" />
									</colgroup>
									<thead>
										<th class="text-center">날짜</th>
										<th class="text-center">키워드</th>
										<th class="text-center">검색수</th>
										<th></th>
									</thead>
									<tbody id="list-inkeyword"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="tab-5" class="tab-pane">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<colgroup>
										<col class="col-sm-1" />
										<col class="col-sm-*" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
									</colgroup>
									<thead>
										<th class="text-center">순위</th>
										<th class="text-center">키워드</th>
										<th class="text-center">네이버</th>
										<th class="text-center">구글</th>
										<th class="text-center">다음</th>
										<th class="text-center">네이트</th>
										<th class="text-center">야후</th>
										<th class="text-center">기타</th>
										<th class="text-center">합계</th>
									</thead>
									<tbody id="list-outkeyword"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div id="tab-6" class="tab-pane">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<colgroup>
										<col class="col-sm-1" />
										<col class="col-sm-2" />
										<col class="col-sm-1" />
										<col class="col-sm-4" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-2" />
										<col class="col-sm-*" />
									</colgroup>
									<thead>
										<th class="text-center">번호</th>
										<th class="text-center">IP</th>
										<th class="text-center">회원여부</th>
										<th class="text-center">접속경로</th>
										<th class="text-center">브라우저</th>
										<th class="text-center">키워드</th>
										<th class="text-center">접속시간</th>
										<th></th>
									</thead>
									<tbody id="list-referer"></tbody>
								</table>
							</div>
							<div class="text-center">
								<ul class="pagination" id="pagination"></ul>
							</div>
						</div>
					</div>
					<div id="tab-7" class="tab-pane">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<colgroup>
										<col class="col-sm-1" />
										<col class="col-sm-3" />
										<col class="col-sm-1" />
										<col class="col-sm-*" />
									</colgroup>
									<thead>
										<th class="text-center">번호</th>
										<th class="text-center">URL</th>
										<th class="text-center">조회수</th>
										<th></th>
									</thead>
									<tbody id="list-url"></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<!-- Data picker -->
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.js"></script>
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.pie.js"></script>

<script id="list-analytics-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{date}}</td>
	<td class="text-center">{{visit}}</td>
	<td class="text-center">{{login}}</td>
	<td class="text-center">{{memjoin}}</td>
	<td class="text-center">{{memout}}</td>
	<td class="text-center">{{board}}</td>
	<td class="text-center">{{comment}}</td>
	<td class="text-center">{{upload}}</td>
	<td class="text-center">{{download}}</td>
	<td class="text-center"></td>
</tr>
</script>

<script id="list-counter-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{date}}</td>
	<td class="text-center">{{hit}}</td>
	<td class="text-center">{{page}}</td>
	<td class="text-center">{{stats}}</td>
	<td class="text-center"></td>
</tr>
</script>

<script id="list-inkeyword-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{date}}</td>
	<td class="text-center">{{keyword}}</td>
	<td class="text-center">{{hit}}</td>
	<td class="text-center"></td>
</tr>
</script>

<script id="list-outkeyword-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{keyword_text}}</td>
	<td class="text-center">{{naver}}</td>
	<td class="text-center">{{google}}</td>
	<td class="text-center">{{daum}}</td>
	<td class="text-center">{{nate}}</td>
	<td class="text-center">{{yahoo}}</td>
	<td class="text-center">{{etc}}</td>
	<td class="text-center">{{total}}</td>
</tr>
</script>

<script id="list-referer-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{ip}}</td>
	<td class="text-center">{{id}}</td>
	<td class="text-center">{{referer}}</td>
	<td class="text-center">{{browser}}</td>
	<td class="text-center">{{keyword}}</td>
	<td class="text-center">{{date}}</td>
	<td class="text-center"></td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
    <li class="{{active}}">
        <a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
    </li>
</script>

<script id="list-url-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class=""><a href="{{url}}" target="_blank">{{url}}</a></td>
	<td class="text-center">{{hit}}</td>
	<td class="text-center"></td>
</tr>
</script>

<script type="text/javascript">
var $year, $month;
function loadAnalytics() {
	$.ajax({type:"GET", url : "/api/admin/stats/analytics", cache:false
		, data : {year:$year.val(), month:$month.val()}
		, success : function(data) {
			if (data.error == 0) {
				var $list = $("#list-analytics");
				$list.empty();
				
				var template = $("#list-analytics-item").html();
				Mustache.parse(template);
				
				for (var i = 0; i < data.data.length; i++) {
					var l = data.data[i];
					$list.append(Mustache.render(template, {
						date:l.date.substring(0, 4)+'-'+l.date.substring(4, 6)+'-'+l.date.substring(6, 8)
						, visit:l.visit, login:l.login, memjoin:l.memjoin, memout:l.memout
						, board:l.board, comment:l.comment, upload:l.upload, download:l.download
					}));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadCounter() {
	$.ajax({type:"GET", url : "/api/admin/stats/counter", cache:false
		, data : {year:$year.val(), month:$month.val()}
		, success : function(data) {
			if (data.error == 0) {
				var $list = $("#list-counter");
				$list.empty();
				
				var template = $("#list-counter-item").html();
				Mustache.parse(template);
				
				for (var i = 0; i < data.data.length; i++) {
					var l = data.data[i];
					$list.append(Mustache.render(template, {
						date:l.date.substring(0, 4)+'-'+l.date.substring(4, 6)+'-'+l.date.substring(6, 8)
						, hit:l.hit, page:l.page, stats:(l.page/l.hit).toFixed(1)
					}));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadBrowser() {
	$.ajax({type:"GET", url : "/api/admin/stats/browser", cache:false
		, data : {year:$year.val(), month:$month.val()}
		, success : function(data) {
			if (data.error == 0) {
				
				var d = [
				         {label:'MSIE 6', data:data.data.MSIE_6, color:"#0d93fa"}
				         , {label:'MSIE 7', data:data.data.MSIE_7, color:"#0d93fa"}
				         , {label:'MSIE 8', data:data.data.MSIE_8, color:"#0d93fa"}
				         , {label:'MSIE 9', data:data.data.MSIE_9, color:"#0d93fa"}
				         , {label:'MSIE 10', data:data.data.MSIE_10, color:"#0d93fa"}
				         , {label:'MSIE 11', data:data.data.MSIE_11, color:"#0d93fa"}
				         , {label:'Chrome', data:data.data.Chrome, color:"#2cad0e"}
				         , {label:'Safari', data:data.data.Safari, color:"#e6e6e6"}
				         , {label:'Firefox', data:data.data.Firefox, color:"#ff3f3f"}
				         , {label:'Opera', data:data.data.Opera, color:"#ef3fff"}
				];

				var chart = $.plot($("#browser-chart"), d, {
			        series: {pie: {show: true}},
			        grid: {hoverable: true},
			        tooltip: true,
			        tooltipOpts: {content: "%p.0%, %s", shifts: {x: 20, y: 0}, defaultTheme: false}
			    });

			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadInKeyword() {
	$.ajax({type:"GET", url : "/api/admin/stats/inkeyword", cache:false
		, data : {year:$year.val(), month:$month.val()}
		, success : function(data) {
			if (data.error == 0) {
				var $list = $("#list-inkeyword");
				$list.empty();
				
				var template = $("#list-inkeyword-item").html();
				Mustache.parse(template);
				
				for (var i = 0; i < data.data.length; i++) {
					var l = data.data[i];
					$list.append(Mustache.render(template, {
						date:l.date.substring(0, 4)+'-'+l.date.substring(4, 6)+'-'+l.date.substring(6, 8)
						, hit:l.hit, keyword:l.keyword
					}));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadOutKeyword() {
	$.ajax({type:"GET", url : "/api/admin/stats/outkeyword", cache:false
		, data : {year:$year.val(), month:$month.val()}
		, success : function(data) {
			if (data.error == 0) {
				var $list = $("#list-outkeyword");
				$list.empty();
				
				var template = $("#list-outkeyword-item").html();
				Mustache.parse(template);
				
				for (var i = 0; i < data.data.length; i++) {
					var l = data.data[i];
					l.no = i+1;
					l.keyword_text = decodeURIComponent(l.keyword);
					$list.append(Mustache.render(template, l));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadReferer(page) {
	$.ajax({type:"GET", url : "/api/admin/stats/referer", cache:false
		, data : {year:$year.val(), month:$month.val(), page:page}
		, success : function(data) {
			if (data.error == 0) {
				var $list = $("#list-referer");
				$list.empty();
				
				var paging = data.data.paging;
				var list = data.data.list;
				var template = $("#list-referer-item").html();
				Mustache.parse(template);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					$list.append(Mustache.render(template, {
						no:virtualNo--, ip:l.ip, id:l.id, referer:getSearchEngine(l.referer)
						, keyword:getSearchKeyword(l.referer) , browser:getBrowser(l.agent), date:getDefaultDataFormat(l.regdate)
					}));
				}
				
				var $pagination = $("#pagination");
				$pagination.empty();
				template = $("#paging-item").html();
				Mustache.parse(template);
				
				$pagination.append(Mustache.render(template, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(template, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(template, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadUrl() {
	$.ajax({type:"GET", url : "/api/admin/stats/url", cache:false
		, data : {year:$year.val(), month:$month.val()}
		, success : function(data) {
			if (data.error == 0) {
				var $list = $("#list-url");
				$list.empty();
				
				var template = $("#list-url-item").html();
				Mustache.parse(template);
				
				for (var i = 0; i < data.data.length; i++) {
					var l = data.data[i];
					$list.append(Mustache.render(template, {
						no:i+1, url:l.url, hit:l.hit
					}));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	$year = $("#year");
	$month = $("#month");
	
	loadAnalytics();
	loadCounter();
	loadBrowser();
	loadInKeyword();
	loadOutKeyword();
	loadReferer(1);
	loadUrl();
	$("#year, #month").change(function(){
		loadAnalytics();
		loadCounter();
		loadBrowser();
		loadInKeyword();
		loadOutKeyword();
		loadReferer(1);
		loadUrl();
	});
	
	$(document).on('click', '.btn-page', function(){
		loadReferer($(this).data('page'));
	});
});
</script>