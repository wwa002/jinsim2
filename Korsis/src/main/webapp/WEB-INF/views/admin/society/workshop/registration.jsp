<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2><span class="workshop-title"></span> - 사전등록</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>워크샵</li>
			<li>${workshopCfg.title}</li>
			<li class="workshop-title"></li>
			<li class="active"><strong>사전등록</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>워크샵 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal">
								<input type="hidden" name="page" id="page" value="1" />
								<div class="form-group">
									<div class="col-sm-4">
										<span class="label label-danger">전체 금액 : <span id="cost-total"></span></span>
										<span class="label label-info">입금 완료 금액 : <span id="cost-paid"></span></span>
									</div>
									<div class="col-sm-2">
										<select name="status" id="status" class="form-control">
											<option value="">상태</option>
											<option value="N">미입금</option>
											<option value="Y">입금완료</option>
										</select>
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색어를 입력해 주세요." class="form-control" value="" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 text-right">
							<button class="btn btn-primary btn-excel" data-type="search"><i class="fa fa-file-excel-o"></i> 검색 엑셀백업</button>
							<button class="btn btn-primary btn-excel" data-type="all"><i class="fa fa-file-excel-o"></i> 전체 엑셀백업</button>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">이름</th>
									<th class="text-center">근무처</th>
									<th class="text-center">행사</th>
									<th class="text-center">의사면허번호</th>
									<th class="text-center">사전등록비</th>
									<th class="text-center">입금상태</th>
									<th class="text-center">결제예정일</th>
									<th class="text-center">입금자명</th>
									<th class="text-center">등록일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="excel-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title">엑셀 용도 입력</h4>
			</div>
			<div class="modal-body text-center">
				<form id="excelForm" action="/api/admin/society/workshop/${code}/${idx}/registration/excel" class="form-horizontal" target="excel-downloader" method="post">
					<input type="hidden" name="status" id="excel-status" />
					<input type="hidden" name="keyword" id="excel-keyword" />
					<input type="hidden" id="excel-backup-type" />
					<textarea id="content" name="content" class="form-control" rows="5" placeholder="엑셀백업을 받으실 용도를 입력해 주십시오."></textarea>
				</form>
				<iframe id="excel-downloader" name="excel-downloader" class="hidden"></iframe>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btn-download">Download</button>
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{{no}}}</td>
	<td>{{name}}</td>
	<td class="text-center">{{office}}</td>
	<td>{{{lecture_text}}}</td>
	<td class="text-center">{{license}}</td>
	<td class="text-center">{{income_cost_text}}</td>
	<td class="text-center">
		<select class="status form-control" data-idx="{{idx}}">
			<option value="N" {{^isStatus}}selected="selected"{{/isStatus}}>미입금</option>
			<option value="Y" {{#isStatus}}selected="selected"{{/isStatus}}>입금완료</option>
		</select>
	</td>
	<td class="text-center">{{income_date}}</td>
	<td class="text-center">{{income_name}}</td>
	<td class="text-center">{{date}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>


<script type="text/javascript">
function load(page) {
	$('#page').val(page);
	var $list = $("#list");
	var $pagination = $("#pagination");
	 
	$.ajax({type:"GET", url : "/api/admin/society/workshop/${code}/${idx}/registration", cache:false
		, data : $('#searchForm').serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var cfg = data.data.cfg;
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = virtualNo--;
					l.isStatus = l.status=='Y';
					l.date = moment(l.regdate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD');
					l.income_cost_text = numberWithCommas(l.income_cost);
					
					for (var j = 0; j < l.lectures.length; j++) {
						if (j == 0) {
							l.lecture_text = l.lectures[j].lecture_title;
						} else {
							l.lecture_text += '<br />'+l.lectures[j].lecture_title;
						}
						if (l.lectures[j].lecture_type != 'N') {
							l.lecture_text += ' ('+l.lectures[j].apply_text+')';
						}
					}
					
					var rendered = Mustache.render(listTemplate, l);
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
				$('#cost-total').text(numberWithCommas(data.data.total.total));
				$('#cost-paid').text(numberWithCommas(data.data.total.paid));
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	load();
	loadWorkshop();
	$("#btn-write").click(function(){
		document.location.href='/admin/society/workshop/${code}/write';
	});
	$("#searchForm").submit(function(){
		load(1);
		return false;
	});
	$(document).on('click', '.btn-page', function(){
		load($(this).data('page'));
	});
	
	$(document).on('change', '.status', function(){
		var idx = $(this).data('idx');
		var status = $(this).val();
		$.ajax({type:"POST", url : '/api/admin/society/workshop/${code}/${idx}/registration/status', cache:false
			, data : {idx:idx, status:status}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					load($('#page').val());
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		
	});
	
	$(document).on('click', '.btn-modify', function(){
		window.open('/admin/society/workshop/${code}/${idx}/registration/'+$(this).data('idx'), 'popup', 'width=800,height=750,status=1');
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url : '/api/admin/society/workshop/${code}/${idx}/registration/delete', cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						alert("삭제되었습니다.");
						load($('#page').val());
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$('.btn-excel').click(function(){
		$('#excel-backup-type').val($(this).data('type'));
		$('#excel-modal').modal('show');
	});
	
	$('#btn-download').click(function(){
		var type = $('#excel-type').val();
		if (!$('#content').val()) {
			alert('사유를 입력해 주세요.');
			return false;
		}
		
		if ($('#excel-backup-type').val() == 'search') {
			$('#excel-status').val($('#status').val());
			$('#excel-keyword').val($('#keyword').val());
		} else {
			$('#excel-status').val('');
			$('#excel-keyword').val('');
		}
		
		$('#excelForm').submit();
	});
});

function loadWorkshop() {
	$.ajax({type:"GET", url : "/api/society/workshop/${code}/${idx}", cache:false
		, data : {}
		, success : function(data) {
			if (data.error == 0) {
				var article = data.data.article;
				$('.workshop-title').text(article.title);
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}
</script>