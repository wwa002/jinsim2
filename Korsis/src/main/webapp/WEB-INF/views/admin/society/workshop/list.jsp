<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${workshopCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>워크샵</li>
			<li class="active"><strong>${workshopCfg.title}</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button id="btn-write" type="button" class="btn btn-primary"><i class="fa fa-edit"></i> 워크샵 등록</button>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>${workshopCfg.title}</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="tabs-container">
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#tab-1">워크샵 목록</a></li>
									<li class=""><a data-toggle="tab" href="#tab-2">신청자 목록</a></li>
								</ul>
								<div class="tab-content">
									<div id="tab-1" class="tab-pane active">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-12">
													<form id="searchForm" class="form-horizontal">
														<input type="hidden" name="page" id="page" value="1" />
														<input type="hidden" name="admin" id="admin" value="Y" />
														<div class="form-group">
															<c:choose>
																<c:when test="${workshopCfg.category eq 'Y'}">
																	<div class="col-sm-2">
																		<select name="category" id="category" class="form-control">
																			<option value="">카테고리</option>
																			<c:forEach items="${fn:split(workshopCfg.categories, ',')}" var="c">
																				<option value="${c}">${c}</option>
																			</c:forEach>
																		</select>
																	</div>
																</c:when>
																<c:otherwise>
																	<input type="hidden" name="category" id="category" value="" />
																</c:otherwise>
															</c:choose>
															<div class="col-sm-4">
																<div class="input-group">
																	<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력해 주세요." class="form-control" value="${param.keyword}" />
																	<span class="input-group-btn">
																		<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
											<div class="table-responsive">
												<table class="table table-striped table-hover">
													<colgroup>
														<col class="col-sm-1" />
														<col class="col-sm-*" />
														<col class="col-sm-2" />
														<col class="col-sm-2" />
														<col class="col-sm-1" />
														<col class="col-sm-2" />
													</colgroup>
													<thead>
														<tr>
															<th class="text-center">번호</th>
															<th class="text-center">제목</th>
															<th class="text-center">장소</th>
															<th class="text-center">날짜</th>
															<th class="text-center">등록일</th>
															<th class="text-center">관리</th>
														</tr>
													</thead>
													<tbody id="list"></tbody>
												</table>
											</div>
											<div class="text-center">
												<ul class="pagination" id="pagination"></ul>
											</div>
										</div>
									</div>
									<div id="tab-2" class="tab-pane">
										<div class="panel-body">
											<div class="row">
												<div class="col-sm-12">
													<form id="searchForm2" class="form-horizontal">
														<input type="hidden" name="page" id="page2" value="1" />
														<div class="form-group">
															<c:choose>
																<c:when test="${workshopCfg.category eq 'Y'}">
																	<div class="col-sm-2">
																		<select name="category" id="category2" class="form-control">
																			<option value="">카테고리</option>
																			<c:forEach items="${fn:split(workshopCfg.categories, ',')}" var="c">
																				<option value="${c}">${c}</option>
																			</c:forEach>
																		</select>
																	</div>
																</c:when>
																<c:otherwise>
																	<input type="hidden" name="category" id="category2" value="" />
																</c:otherwise>
															</c:choose>
															<div class="col-sm-2">
																<select name="widx" id="workshops" class="form-control"></select>
															</div>
															<div class="col-sm-2">
																<select name="status" id="status" class="form-control">
																	<option value="">입금상태</option>
																	<option value="N">미입금</option>
																	<option value="Y">입금완료</option>
																</select>
															</div>
															<div class="col-sm-4">
																<div class="input-group">
																	<input type="text" name="keyword" id="keyword2" placeholder="검색어를 입력해 주세요." class="form-control" value="${param.keyword}" />
																	<span class="input-group-btn">
																		<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
																	</span>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
											
											<div class="table-responsive">
												<table class="table table-striped table-hover">
													<thead>
														<tr>
															<th class="text-center">번호</th>
															<th class="text-center">워크샵</th>
															<th class="text-center">이름</th>
															<th class="text-center">근무처</th>
															<th class="text-center">의사면허번호</th>
															<th class="text-center">사전등록비</th>
															<th class="text-center">입금상태</th>
															<th class="text-center">결제예정일</th>
															<th class="text-center">입금자명</th>
															<th class="text-center">등록일</th>
														</tr>
													</thead>
													<tbody id="list2"></tbody>
												</table>
											</div>
											<div class="text-center">
												<ul class="pagination" id="pagination2"></ul>
											</div>
											
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{{no}}}</td>
	<td><a href="/admin/society/workshop/${code}/{{idx}}/modify">{{title}}</a> {{comments}}</td>
	<td class="text-center">{{location}}</td>
	<td class="text-center">{{term}}</td>
	<td class="text-center">{{date}}</td>
	<td class="text-center">
		<a href="/admin/society/workshop/${code}/{{idx}}/registration" class="btn btn-default"><i class="fa fa-list"></i> 사전등록</a>
		<button type="button" class="btn btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i> 삭제</button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page2" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script id="list2-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{{no}}}</td>
	<td>{{title}}</td>
	<td>{{name}}</td>
	<td class="text-center">{{office}}</td>
	<td class="text-center">{{license}}</td>
	<td class="text-center">{{income_cost_text}}</td>
	<td class="text-center">
		{{#isStatus}}입금완료{{/isStatus}}
		{{^isStatus}}미입금{{/isStatus}}
	</td>
	<td class="text-center">{{income_date}}</td>
	<td class="text-center">{{income_name}}</td>
	<td class="text-center">{{date}}</td>
</tr>
</script>

<script id="paging2-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page2" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>



<script type="text/javascript">
function load(page) {
	$('#page').val(page);
	var $list = $("#list");
	var $pagination = $("#pagination");
	 
	$.ajax({type:"GET", url : "/api/society/workshop/${code}", cache:false
		, data : $('#searchForm').serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var cfg = data.data.cfg;
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = virtualNo--;
					l.date = moment(l.regdate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD');
					l.term = l.startdate ? moment(l.startdate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD') : '';
					if (l.enddate) {
						l.term += ' ~ '+ moment(l.enddate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD');
					}
					var rendered = Mustache.render(listTemplate, l);
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function load2(page) {
	$('#page2').val(page);
	var $list = $("#list2");
	var $pagination = $("#pagination2");
	 
	$.ajax({type:"GET", url : "/api/admin/society/workshop/${code}/registration", cache:false
		, data : $('#searchForm2').serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list2-item").html();
				var pagingTemplate = $("#paging2-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = virtualNo--;
					l.isStatus = l.status=='Y';
					l.date = moment(l.regdate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD');
					l.income_cost_text = numberWithCommas(l.income_cost);
					var rendered = Mustache.render(listTemplate, l);
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
				var workshops = data.data.workshops;
				if (workshops) {
					var $workshops = $('#workshops');
					var w = $workshops.val();
					$workshops.empty();
					$workshops.append('<option value="">워크샵</option>');
					for (var i = 0; i < workshops.length; i++) {
						var d = workshops[i];
						$workshops.append('<option value="'+d.idx+'">'+d.title+'</option>');
					}
					$workshops.val(w);
					
				}
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	load();
	load2();
	$("#btn-write").click(function(){
		document.location.href='/admin/society/workshop/${code}/write';
	});
	$("#searchForm").submit(function(){
		load(1);
		return false;
	});
	$(document).on('click', '.btn-page', function(){
		load($(this).data('page'));
	});
	$("#searchForm2").submit(function(){
		load2(1);
		return false;
	});
	$(document).on('click', '.btn-page2', function(){
		load2($(this).data('page'));
	});
	
	$('#category2, #workshops, #status').change(function(){
		load2(1);
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('게시물을 삭제하시겠습니까?\n삭제한 게시물은 되돌릴 수 없습니다.')) {
			var url = "/api/admin/society/workshop/${code}/"+$(this).data("idx")+"/delete";
			$.ajax({type:"GET", url : url, cache:false
				, data : {}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						alert("삭제되었습니다.");
						load();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>