<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>정보수정</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>회원</li>
			<li class="active"><strong>정보수정</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>기본 정보</h5>
				</div>
				<div class="ibox-content">
					<form id="memberModifyForm" class="form-horizontal" enctype="multipart/form-data">
						<input type="hidden" name="idx" class="idx" />
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호</label>
							<div class="col-sm-8">
								<input type="password" name="passwd" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">회원타입</label>
							<div class="col-sm-8">
								<select name="type" id="type" class="form-control">
									<option value="MEMBER">일반회원</option>
									<option value="ADMIN">관리자</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이름</label>
							<div class="col-sm-8">
								<input type="text" name="name" id="name" placeholder="이름" class="form-control" />
						</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">휴대폰</label>
							<div class="col-sm-8">
								<input type="text" name="mobile" id="mobile" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이메일</label>
							<div class="col-sm-8">
								<input type="text" name="email" id="email" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">생일</label>
							<div class="col-sm-8">
								<input type="text" name="birth" id="birth" class="form-control datepicker" maxlength="10" placeholder="yyyy-MM-dd" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">성별</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="M" name="sex" class="sex"> <i></i> 남성 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="F" name="sex" class="sex"> <i></i> 여성 
									</label>
								</div>
							</div>
						</div>
						<div class="form-group hidden">
							<label class="col-sm-4 control-label">프로필</label>
							<div class="col-sm-8">
								<input type="file" name="file" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="isuse" class="isuse"> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="isuse" class="isuse"> <i></i> 차단
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="submit" class="btn btn-primary">정보수정</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<c:if test="${siteInfo.society eq 'Y'}">
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-title">
						<h5>학회 정보</h5>
					</div>
					<div class="ibox-content">
						<form id="societyModifyForm" class="form-horizontal">
							<input type="hidden" name="idx" class="idx" />
							<div class="form-group">
								<label class="col-sm-4 control-label">이름 (영문)</label>
								<div class="col-sm-8">
									<input type="text" name="name_eng" id="name_eng" placeholder="이름 (영문)" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">전문과목</label>
								<div class="col-sm-8">
									<select name="major" id="major" class="form-control">
										<option value="">과목선택</option>
	                                    <option value="비뇨기과" >비뇨기과</option>
	                                    <option value="영상의학과" >영상의학과</option>
	                                    <option value="병리과" >병리과</option>
	                                    <option value="내과" >내과</option>
	                                    <option value="방사선종양학과" >방사선종양학과</option>
	                                    <option value="외과" >외과</option>
	                                    <option value="흉부외과" >흉부외과</option>
	                                    <option value="신경외과" >신경외과</option>
	                                    <option value="정형외과" >정형외과</option>
	                                    <option value="성형외과" >성형외과</option>
	                                    <option value="산부인과" >산부인과</option>
	                                    <option value="피부과" >피부과</option>
	                                    <option value="안과" >안과</option>
	                                    <option value="이비인후과" >이비인후과</option>
	                                    <option value="정신과" >정신과</option>
	                                    <option value="신경과" >신경과</option>
	                                    <option value="마취통증의학과" >마취통증의학과</option>
	                                    <option value="가정의학과" >가정의학과</option>
	                                    <option value="응급의학과" >응급의학과</option>
	                                    <option value="재활의학과" >재활의학과</option>
	                                    <option value="영상의학과" >영상의학과</option>
	                                    <option value="핵의학과" >핵의학과</option>
	                                    <option value="진단검사의학과" >진단검사의학과</option>
	                                    <option value="의공학과" >의공학과</option>
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label">의사 면허번호</label>
								<div class="col-sm-8">
									<input type="text" name="license_doctor" id="license_doctor" placeholder="" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">전문의 번호</label>
								<div class="col-sm-8">
									<input type="text" name="license_expert" id="license_expert" placeholder="" class="form-control" />
								</div>
							</div>
							
							<hr />
							<h4>근무처 정보</h4>
							
							<div class="form-group">
								<label class="col-sm-4 control-label">근무처 구분</label>
								<div class="col-sm-8">
									<select name="office_type" id="office_type" class="form-control">
	                                    <option value="대학병원" >대학병원</option>
	                                    <option value="종합병원" >종합병원</option>
	                                    <option value="개인병원" >개인병원</option>
	                                    <option value="기타" >기타</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">병원명</label>
								<div class="col-sm-8">
									<input type="text" name="office" id="office" placeholder="" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">우편번호</label>
								<div class="col-sm-8">
									<input type="text" name="zipcode" id="zipcode" placeholder="" class="form-control address-picker" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">기본주소</label>
								<div class="col-sm-8">
									<input type="text" name="address" id="address" placeholder="" class="form-control address-picker" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">상세주소</label>
								<div class="col-sm-8">
									<input type="text" name="address_etc" id="address_etc" placeholder="" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">전화번호</label>
								<div class="col-sm-8">
									<input type="text" name="phone1" id="phone1" placeholder="" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">팩스번호</label>
								<div class="col-sm-8">
									<input type="text" name="phone2" id="phone2" placeholder="" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-8 col-sm-offset-4">
									<button type="submit" class="btn btn-primary">정보수정</button>
									<button type="reset" class="btn btn-default">취소</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</c:if>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>약관 정보</h5>
				</div>
				<div class="ibox-content">
					<form id="agreeModifyForm" class="form-horizontal">
						<input type="hidden" name="idx" class="idx" />
						<div class="form-group">
							<label class="col-sm-4 control-label">SMS 이용 여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="sms" class="sms"> <i></i> 예 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="sms" class="sms"> <i></i> 아니오 
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">회원 검색 동의</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="party" class="party"> <i></i> 예 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="party" class="party"> <i></i> 아니오
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">안내 메일 및 우편물 수신</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="mail" class="mail"> <i></i> 예 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="mail" class="mail"> <i></i> 아니오
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">고유식별번호 동의</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="license" class="license"> <i></i> 예 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="license" class="license"> <i></i> 아니오
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="submit" class="btn btn-primary">정보수정</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		
			<div class="ibox hidden" id="admin-permission">
				<div class="ibox-title">
					<h5>권한 정보</h5>
				</div>
				<div class="ibox-content">
					<form id="permissionForm" class="form-horizontal">
						<input type="hidden" name="idx" class="idx" />
						<div class="form-group">
							<div class="col-sm-12">
								<label>
									 <input type="checkbox" id="permission-all"> <i></i> 전체
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label>
									<input type="checkbox" name="permission" value="MEMBER" class="permissions"> <i></i> 회원관리 
								</label>
							</div>
							<div class="col-sm-4">
								<label>
									<input type="checkbox" name="permission" value="REGISTRATION" class="permissions"> <i></i> 사전등록 
								</label>
							</div>
							<div class="col-sm-4">
								<label>
									<input type="checkbox" name="permission" value="MAILER" class="permissions"> <i></i> 메일발송 
								</label>
							</div>
							<div class="col-sm-4">
								<label>
									<input type="checkbox" name="permission" value="STATS" class="permissions"> <i></i> 접속통계 
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 text-right">
								<button type="submit" class="btn btn-primary">권한수정</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="ibox">
				<div class="ibox-title">
					<h5>등급 정보</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<ul id="gcd-list"></ul>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-9">
							<select id="gcd" class="form-control">
								<option value="">회원등급</option>
								<c:forEach items="${gcds}" var="g">
									<option value="${g.gcd}" <c:if test="${param.gcd eq g.gcd }"> selected="selected"</c:if>>${g.title}</option>
								</c:forEach>
							</select>
						</div>
						<div class="col-xs-3">
							<button id="btn-gcd-add" class="btn btn-block btn-primary">추가</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<!-- Daum 주소 -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script id="gcd-item" type="x-tmpl-mustache">
<li>
	{{title}} - 등록일({{regdate}}) <button type="button" data-gcd="{{gcd}}" class="btn btn-xs btn-danger btn-gcd-delete"><i class="fa fa-trash"></i></button>
</li>
</script>

<script type="text/javascript">
function loadDefaultInfo() {
	$.ajax({type:"GET", url:"/api/admin/member/info", cache:false
		, data : {id:'${id}'}
		, success : function(data) {
			if (data.error == 0) {
				$(".idx").val(data.data.idx);
				$("#type").val(data.data.type);
				$("#name").val(data.data.name);
				$("#mobile").val(data.data.mobile);
				$("#email").val(data.data.email);
				
				if (data.data.type == 'ADMIN') {
					$('#admin-permission').removeClass('hidden');
				}
				
				var birth = data.data.birth;
				if (birth) {
					var text = birth.substring(0, 4);
					text += "-" + birth.substring(4, 6);
					text += "-" + birth.substring(6, 8);
					$("#birth").val(text);
				}
				
				$('.sex').each(function(){
					if ($(this).val() == data.data.sex) {
						$(this).iCheck("check");
					}
				});
				$('.isuse').each(function(){
					if ($(this).val() == data.data.isuse) {
						$(this).iCheck("check");
					}
				});
				
				if (data.data.society) {
					$('#name_eng').val(data.data.society.name_eng);
					$('#major').val(data.data.society.major);
					$('#license_doctor').val(data.data.society.license_doctor);
					$('#license_expert').val(data.data.society.license_expert);
					$('#office_type').val(data.data.society.office_type);
					$('#office').val(data.data.society.office);
					$('#zipcode').val(data.data.society.zipcode);
					$('#address').val(data.data.society.address);
					$('#address_etc').val(data.data.society.address_etc);
					$('#phone1').val(data.data.society.phone1);
					$('#phone2').val(data.data.society.phone2);
					if (data.data.society.permission) {
						var str = data.data.society.permission.split(',');
						for (var i = 0; i < str.length; i++) {
							$('.permissions').each(function(){
								if ($(this).val() == str[i]) {
									$(this).prop('checked', true);
								}
							});
						}
						permissionChecked();
					}
				}
				if (data.data.terms) {
					$('.sms').each(function(){
						if ($(this).val() == data.data.terms.sms) {
							$(this).iCheck("check");
						}
					});
					$('.party').each(function(){
						if ($(this).val() == data.data.terms.party) {
							$(this).iCheck("check");
						}
					});
					$('.mail').each(function(){
						if ($(this).val() == data.data.terms.mail) {
							$(this).iCheck("check");
						}
					});
					$('.license').each(function(){
						if ($(this).val() == data.data.terms.license) {
							$(this).iCheck("check");
						}
					});
					
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadGCDInfo() {
	$.ajax({type:"GET", url:"/api/admin/member/gcd/list", cache:false
		, data : {id:'${id}'}
		, success : function(data) {
			if (data.error == 0) {
				var $gcdList = $("#gcd-list");
				$gcdList.empty();
				
				var list = data.data;
				var template = $("#gcd-item").html();
				Mustache.parse(template);
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(template, {
						title:l.title, gcd:l.gcd, regdate:getDefaultDataFormat(l.regdate)
					});
					$gcdList.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function permissionChecked() {
	var checked = true;
	$('.permissions').each(function(){
		if (!$(this).prop('checked')) {
			checked = false;
		}
	});
	$('#permission-all').prop('checked', checked);
}

$(function(){
	loadDefaultInfo();
	loadGCDInfo();
	
	$('.address-picker').click(function(){
		new daum.Postcode({
	        oncomplete: function(data) {
	        	$('#zipcode').val(data.zonecode);
	        	$('#address').val(data.address);
	        }
	    }).open();
	});
	$('.permissions').click(function(){
		permissionChecked();
	});
	$('#permission-all').click(function(event){
		$('.permissions').prop('checked', $(this).prop('checked'));
	});
	
	$('#type').change(function(){
		if ($(this).val() == 'ADMIN') {
			$('#admin-permission').removeClass('hidden');
		} else {
			$('#admin-permission').addClass('hidden');
		}
	});
	
	$('#memberModifyForm').submit(function(){
		$.ajax({type:"GET", url:"/api/admin/society/member/save/basic", cache:false
			, data : $('#memberModifyForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('수정되었습니다.');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	$('#societyModifyForm').submit(function(){
		$.ajax({type:"GET", url:"/api/admin/society/member/save/society", cache:false
			, data : $('#societyModifyForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('수정되었습니다.');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$('#agreeModifyForm').submit(function(){
		$.ajax({type:"GET", url:"/api/admin/society/member/save/agree", cache:false
			, data : $('#agreeModifyForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('수정되었습니다.');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$('#permissionForm').submit(function(){
		$.ajax({type:"GET", url:"/api/admin/society/member/save/permission", cache:false
			, data : $('#permissionForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('수정되었습니다.');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$("#member-modify").click(function(){
		$("#memberModifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/member/save"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#memberModifyForm").submit();
	});
	
	$("#btn-gcd-add").click(function(){
		var gcd = $("#gcd").val();
		if (!gcd) {
			alert('등급을 선택해 주세요.');
			$("#gcd").focus();
			return;
		}
		
		$.ajax({type:"POST", url:"/api/admin/member/gcd/add", cache:false
			, data : {id:'${id}', gcd:gcd}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-gcd-delete', function(){
		var gcd = $(this).data("gcd");
		
		$.ajax({type:"POST", url:"/api/admin/member/gcd/delete", cache:false
			, data : {id:'${id}', gcd:gcd}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>