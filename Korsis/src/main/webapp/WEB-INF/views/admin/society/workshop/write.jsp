<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${workshopCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>워크샵</li>
			<li>${workshopCfg.title}</li>
			<li class="active"><strong>등록</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-write" class="btn btn-primary"><i class="fa fa-save"></i> 작성하기</button>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>작성</h5>
				</div>
				<div class="ibox-content">
					<form id="submitForm" class="form-horizontal">
						<input type="hidden" name="files" id="files" value="" />
						<div class="row">
							<div class="col-sm-12">
								<c:choose>
									<c:when test="${workshopCfg.category eq 'Y'}">
										<div class="form-group">
											<label class="col-sm-2 control-label">카테고리</label>
											<div class="col-sm-10">
												<select name="category" class="form-control">
													<option value="">카테고리 선택</option>
													<c:forEach items="${fn:split(workshopCfg.categories, ',')}" var="c">
														<option value="${c}">${c}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="hidden" name="category" value="" />
									</c:otherwise>
								</c:choose>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사명</label>
									<div class="col-sm-10">
										<input type="text" name="title" id="title" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사장소</label>
									<div class="col-sm-10">
										<input type="text" name="location" id="location" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사일</label>
									<div class="col-sm-2">
										<input type="text" name="sdate" id="sdate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="stime" id="stime" class="form-control clockpicker" />
									</div>
									<div class="col-sm-1 text-center">~</div>
									<div class="col-sm-2">
										<input type="text" name="edate" id="edate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="etime" id="etime" class="form-control clockpicker" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">주최</label>
									<div class="col-sm-10">
										<input type="text" name="hosted" id="hosted" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">문의처</label>
									<div class="col-sm-10">
										<input type="text" name="contact" id="contact" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">홈페이지</label>
									<div class="col-sm-10">
										<input type="text" name="homepage" id="homepage" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사배너</label>
									<div class="col-sm-10">
										<input type="file" name="banner" id="banner" class="form-control" />
									</div>
								</div>
								<hr />
								<div class="form-group">
									<label class="col-sm-2 control-label">사전등록</label>
									<div class="col-sm-10">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="registration" id="registration-y"> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="registration" id="registration-n" checked="checked"> <i></i> 사용안함
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">사전등록 일정</label>
									<div class="col-sm-2">
										<input type="text" name="registration_sdate" id="registration_sdate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="registration_stime" id="registration_stime" class="form-control clockpicker" />
									</div>
									<div class="col-sm-1 text-center">~</div>
									<div class="col-sm-2">
										<input type="text" name="registration_edate" id="registration_edate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="registration_etime" id="registration_etime" class="form-control clockpicker" />
									</div>
								</div>
								<hr />
								<div class="form-group">
									<label class="col-sm-2 control-label">노출</label>
									<div class="col-sm-10">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="status" id="status-y" checked="checked"> <i></i> 노출 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="status" id="status-n"> <i></i> 비노출
										</label>
									</div>
								</div>
								<div id="lecture-wrapper" class="hidden">
									<hr />
									<div class="form-group">
										<label class="col-sm-2 control-label">
											행사추가
											<button type="button" id="btn-lecture-add" class="btn btn-xs btn-primary">
												<i class="fa fa-plus"></i>
											</button>
										</label>
										<div class="col-sm-9" id="lecture-list">
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<textarea name="content" id="content" class="hidden"></textarea>
								<div id="summernote"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>첨부파일</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<ul id="file-list">
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<form id="my-awesome-dropzone" action="/api/admin/society/workshop/${code}/upload/dropzone" class="dropzone" action="#">
								<div class="dropzone-previews"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="file-item" type="x-tmpl-mustache">
<li>
	{{filename}} <button type="button" data-idx="{{idx}}" class="btn btn-xs btn-danger btn-file-delete"><i class="fa fa-trash"></i></button>
</li>
</script>

<script id="lecture-item" type="x-tmpl-mustache">
<div id="lecture-item-{{no}}" class="lecture-item">
	<div class="form-group">
		<label class="col-sm-2 control-label">
			행사명
		</label>
		<div class="col-sm-9 col-xs-10">
			<input type="text" name="titles" placeholder="행사명" class="form-control" />
		</div>
		<div class="col-sm-1 col-xs-2">
			<button type="button" class="btn btn-danger btn-xs btn-lecture-del" data-no="{{no}}"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">
			종류
		</label>
		<div class="col-sm-4">
			<select name="types" class="form-control btn-lecture-type" data-no="{{no}}">
				<option value="M">정회원/비회원</option>
				<option value="L">처음수강/재수강</option>
				<option value="S">전공의</option>
				<option value="N">없음</option>
			</select>
		</div>
		<label class="col-sm-2 col-xs-12 control-label">
			금액
		</label>
		<div class="col-sm-2 col-xs-6">
			<input type="number" name="costs1" placeholder="정회원" id="lecture-cost1-{{no}}" class="form-control" />
		</div>
		<div class="col-sm-2 col-xs-6">
			<input type="number" name="costs2" placeholder="비회원" id="lecture-cost2-{{no}}" class="form-control" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">
			인원제한
		</label>
		<div class="col-sm-4">
			<input type="number" name="limiteds" placeholder="" class="form-control" />
		</div>
		<label class="col-sm-2 control-label">
			신청 종료일
		</label>
		<div class="col-sm-4">
			<input type="text" name="enddates" placeholder="" class="form-control datepicker" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 col-xs-12 control-label">
			코스추가 <input type="checkbox" value="Y" class="btn-course" data-no="{{no}}" placeholder="" />
		</label>
		<div class="col-sm-6 col-xs-6 course-{{no}} hidden">
			<input type="text" name="courses1" placeholder="" class="form-control" />
		</div>
		<label class="col-sm-1 col-xs-2 course-{{no}} hidden">
			인원
		</label>
		<div class="col-sm-2 col-xs-4 course-{{no}} hidden">
			<input type="number" name="courses1_limited" placeholder="" class="form-control" />
		</div>
	</div>
	<div class="form-group course-{{no}} hidden">
		<div class="col-sm-6 col-sm-offset-2 col-xs-6">
			<input type="text" name="courses2" placeholder="" class="form-control" />
		</div>
		<label class="col-sm-1 col-xs-2">
			인원
		</label>
		<div class="col-sm-2 col-xs-4">
			<input type="number" name="courses2_limited" placeholder="" class="form-control" />
		</div>
	</div>
	<hr />
</div>
</script>

<script type="text/javascript">
var files = [];
var sitecd = '${siteInfo.sitecd}';
$(function(){
	$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	
	Dropzone.options.myAwesomeDropzone = {
		init: function () {
			this.on("complete", function (data) {
				console.log(data);
				var res = JSON.parse(data.xhr.responseText);
				if (res.status == "success") {
					files.push(res.data);
					$("#files").val(files.join());
					var template = $("#file-item").html();
					Mustache.parse(template);
					$("#file-list").append(Mustache.render(template, {
						idx:res.data, filename:data.name
					}));
					this.removeFile(data);
				}
			});
		}
	};
	
	if ('KORSIS' == sitecd) {
		initLecture();
	}
	
	$("#btn-write").click(function(){
		$("#content").val($('#summernote').summernote('code'));
		
		var url = 'KORSIS' == sitecd ? '/api/admin/society/workshop/${code}/write/korsis' : '/api/admin/society/workshop/${code}/write';
		$("#submitForm").ajaxForm({
			type:"POST"
			, url:url
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					alert("등록되었습니다.");
					document.location.href='/admin/society/workshop/${code}';
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#submitForm").submit();
	});
	
	$(document).on('click', '.btn-file-delete', function(){
		var idx = $(this).data('idx');
		var newFiles = [];
		for (var i = 0; i < files.length; i++) {
			if (files[i] != idx) {
				newFiles.push(files[i]);
			}
		}
		files = newFiles;
		$("#files").val(files.join());
		$(this).parent().remove();
	});
});

var lectureSize = -1;
function initLecture() {
	lectureSize = -1;
	$('#lecture-wrapper').removeClass('hidden');
	$('#btn-lecture-add').click(function(){
		addLecture();
	});
	$(document).on('click', '.btn-lecture-del', function(){
		$('#lecture-item-'+$(this).data('no')).remove();
		if ($('.lecture-item').length == 0) {
			lectureSize = -1;
		}
	});
	$(document).on('change', '.btn-lecture-type', function(){
		var no = $(this).data('no');
		var $cost1 = $('#lecture-cost1-'+no);
		var $cost2 = $('#lecture-cost2-'+no);
		$cost1.val('');
		$cost2.val('');
		$cost2.removeClass('hidden');
		switch ($(this).val()) {
		case 'M':
			$cost1.attr('placeholder', '정회원');
			$cost2.attr('placeholder', '비회원');
			break;
		case 'L':
			$cost1.attr('placeholder', '처음수강');
			$cost2.attr('placeholder', '재수강');
			break;
		case 'S':
			$cost1.attr('placeholder', '');
			$cost2.addClass('hidden');
		case 'N':
			$cost1.attr('placeholder', '');
			$cost2.addClass('hidden');
			break;
		}
	});
	$(document).on('click', '.btn-course', function(){
		var no = $(this).data('no');
		if ($(this).prop('checked')) {
			$('.course-'+no).removeClass('hidden');
		} else {
			$('.course-'+no).addClass('hidden');
			$('.course-'+no+' input').val('');
		}
	});
}

function addLecture() {
	lectureSize++;
	var $list = $("#lecture-list"); 
	var template = $("#lecture-item").html();
	Mustache.parse(template);
	$list.append(Mustache.render(template, {no:lectureSize}));
	$(".datepicker").datepicker({todayBtn: "linked", keyboardNavigation: false, forceParse: false, autoclose: true, format: "yyyy-mm-dd"});
}
</script>