<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${workshopCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>워크샵</li>
			<li>${workshopCfg.title}</li>
			<li class="active"><strong>수정</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-modify" class="btn btn-primary"><i class="fa fa-save"></i> 수정하기</button>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>수정</h5>
				</div>
				<div class="ibox-content">
					<form id="submitForm" class="form-horizontal">
						<input type="hidden" name="idx" value="${idx}" />
						<input type="hidden" name="files" id="files" value="" />
						<div class="row">
							<div class="col-sm-12">
								<c:choose>
									<c:when test="${workshopCfg.category eq 'Y'}">
										<div class="form-group">
											<label class="col-sm-2 control-label">카테고리</label>
											<div class="col-sm-10">
												<select name="category" id="category" class="form-control">
													<option value="">카테고리 선택</option>
													<c:forEach items="${fn:split(workshopCfg.categories, ',')}" var="c">
														<option value="${c}">${c}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="hidden" id="category" name="category" value="" />
									</c:otherwise>
								</c:choose>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사명</label>
									<div class="col-sm-10">
										<input type="text" name="title" id="title" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사장소</label>
									<div class="col-sm-10">
										<input type="text" name="location" id="location" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사일</label>
									<div class="col-sm-2">
										<input type="text" name="sdate" id="sdate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="stime" id="stime" class="form-control clockpicker" />
									</div>
									<div class="col-sm-1 text-center">~</div>
									<div class="col-sm-2">
										<input type="text" name="edate" id="edate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="etime" id="etime" class="form-control clockpicker" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">주최</label>
									<div class="col-sm-10">
										<input type="text" name="hosted" id="hosted" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">문의처</label>
									<div class="col-sm-10">
										<input type="text" name="contact" id="contact" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">홈페이지</label>
									<div class="col-sm-10">
										<input type="text" name="homepage" id="homepage" class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">행사배너</label>
									<div class="col-sm-6">
										<input type="file" name="banner" id="banner" class="form-control" />
									</div>
									<div class="col-sm-2 btn-banner hidden">
										<button type="button" id="btn-banner-view" class="btn btn-default btn-block"><i class="fa fa-laptop"></i> 보기</button>
									</div>
									<div class="col-sm-2 btn-banner hidden">
										<button type="button" id="btn-banner-delete" class="btn btn-danger btn-block"><i class="fa fa-trash"></i> 삭제</button>
									</div>
								</div>
								<hr />
								<div class="form-group">
									<label class="col-sm-2 control-label">사전등록</label>
									<div class="col-sm-10">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="registration" id="registration-y"> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="registration" id="registration-n" checked="checked"> <i></i> 사용안함
										</label>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">사전등록 일정</label>
									<div class="col-sm-2">
										<input type="text" name="registration_sdate" id="registration_sdate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="registration_stime" id="registration_stime" class="form-control clockpicker" />
									</div>
									<div class="col-sm-1 text-center">~</div>
									<div class="col-sm-2">
										<input type="text" name="registration_edate" id="registration_edate" class="form-control datepicker" />
									</div>
									<div class="col-sm-2">
										<input type="text" name="registration_etime" id="registration_etime" class="form-control clockpicker" />
									</div>
								</div>
								<hr />
								<div class="form-group">
									<label class="col-sm-2 control-label">노출</label>
									<div class="col-sm-10">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="status" id="status-y" checked="checked"> <i></i> 노출 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="status" id="status-n"> <i></i> 비노출
										</label>
									</div>
								</div>
								<div id="lecture-wrapper" class="hidden">
									<hr />
									<div class="form-group">
										<label class="col-sm-2 control-label">
											행사추가
											<button type="button" id="btn-lecture-add" class="btn btn-xs btn-primary">
												<i class="fa fa-plus"></i>
											</button>
										</label>
										<div class="col-sm-9" id="lecture-list">
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<textarea name="content" id="content" class="hidden"></textarea>
								<div id="summernote"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>첨부파일</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<ul id="file-list">
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<form id="my-awesome-dropzone" action="/api/admin/society/workshop/${code}/upload/dropzone" class="dropzone" action="#">
								<div class="dropzone-previews"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="file-item" type="x-tmpl-mustache">
<li>
	{{filename}} <button type="button" data-idx="{{idx}}" class="btn btn-xs btn-danger btn-file-delete"><i class="fa fa-trash"></i></button>
</li>
</script>

<script id="lecture-item" type="x-tmpl-mustache">
<div id="lecture-item-{{no}}" class="lecture-item">
	<div class="form-group">
		<label class="col-sm-2 control-label">
			행사명
		</label>
		<div class="col-sm-9 col-xs-10">
			<input type="text" name="titles" placeholder="행사명" value="{{title}}" class="form-control" />
		</div>
		<div class="col-sm-1 col-xs-2">
			<button type="button" class="btn btn-danger btn-xs btn-lecture-del" data-no="{{no}}"><i class="fa fa-minus"></i></button>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">
			종류
		</label>
		<div class="col-sm-4">
			<select name="types" class="form-control btn-lecture-type" data-no="{{no}}">
				<option value="M"{{#istypem}} selected="selected"{{/istypem}}>정회원/비회원</option>
				<option value="L"{{#istypel}} selected="selected"{{/istypel}}>처음수강/재수강</option>
				<option value="S"{{#istypes}} selected="selected"{{/istypes}}>전공의</option>
				<option value="N"{{#istypen}} selected="selected"{{/istypen}}>없음</option>
			</select>
		</div>
		<label class="col-sm-2 col-xs-12 control-label">
			금액
		</label>
		<div class="col-sm-2 col-xs-6">
			{{#istypem}}
				<input type="number" name="costs1" placeholder="정회원" value="{{cost1}}" id="lecture-cost1-{{no}}" class="form-control" />
			{{/istypem}}
			{{#istypel}}
				<input type="number" name="costs1" placeholder="처음수강" value="{{cost1}}" id="lecture-cost1-{{no}}" class="form-control" />
			{{/istypel}}
			{{#istypes}}
				<input type="number" name="costs1" placeholder="전공의" value="{{cost1}}" id="lecture-cost1-{{no}}" class="form-control" />
			{{/istypes}}
			{{#istypen}}
				<input type="number" name="costs1" placeholder="정회원" value="{{cost1}}" id="lecture-cost1-{{no}}" class="form-control" />
			{{/istypen}}
		</div>
		<div class="col-sm-2 col-xs-6">
			{{#istypem}}
				<input type="number" name="costs2" placeholder="비회원" value="{{cost2}}" id="lecture-cost2-{{no}}" class="form-control" />
			{{/istypem}}
			{{#istypel}}
				<input type="number" name="costs2" placeholder="재수강" value="{{cost2}}" id="lecture-cost2-{{no}}" class="form-control" />
			{{/istypel}}
			{{#istypes}}
				<input type="number" name="costs2" placeholder="" value="{{cost2}}" id="lecture-cost2-{{no}}" class="form-control hidden" />
			{{/istypes}}
			{{#istypen}}
				<input type="number" name="costs2" placeholder="" value="{{cost2}}" id="lecture-cost2-{{no}}" class="form-control hidden" />
			{{/istypen}}
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">
			인원제한
		</label>
		<div class="col-sm-4">
			<input type="number" name="limiteds" value="{{limited}}" class="form-control" />
		</div>
		<label class="col-sm-2 control-label">
			신청 종료일
		</label>
		<div class="col-sm-4">
			<input type="text" name="enddates" value="{{enddate_text}}" class="form-control datepicker" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 col-xs-12 control-label">
			코스추가 <input type="checkbox" value="Y" class="btn-course" data-no="{{no}}" placeholder="" {{#iscourse}}checked="checked"{{/iscourse}} />
		</label>
		<div class="col-sm-6 col-xs-6 course-{{no}} {{^iscourse}}hidden{{/iscourse}}">
			<input type="text" name="courses1" value="{{course1}}" placeholder="" class="form-control" />
		</div>
		<label class="col-sm-1 col-xs-2 course-{{no}} {{^iscourse}}hidden{{/iscourse}}">
			인원
		</label>
		<div class="col-sm-2 col-xs-4 course-{{no}} {{^iscourse}}hidden{{/iscourse}}">
			<input type="number" name="courses1_limited" value="{{course1_limited}}" placeholder="" class="form-control" />
		</div>
	</div>
	<div class="form-group course-{{no}} {{^iscourse}}hidden{{/iscourse}}">
		<div class="col-sm-6 col-sm-offset-2 col-xs-6">
			<input type="text" name="courses2" value="{{course2}}" placeholder="" class="form-control" />
		</div>
		<label class="col-sm-1 col-xs-2">
			인원
		</label>
		<div class="col-sm-2 col-xs-4">
			<input type="number" name="courses2_limited" value="{{course2_limited}}" placeholder="" class="form-control" />
		</div>
	</div>
	<hr />
</div>
</script>

<script type="text/javascript">
var files = [];
var sitecd = '${siteInfo.sitecd}';
var var1 = '';
function load() {
	$.ajax({type:"GET", url : "/api/society/workshop/${code}/${idx}", cache:false
		, data : {}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var article = data.data.article;
				var fileList = data.data.files;
				if ("Y" == article.status) {
					$("#status-y").iCheck('check');
				} else {
					$("#status-n").iCheck('check');
				}
				if (article.category) {
					$("#category").val(article.category);
				}
				$("#title").val(article.title);
				if (article.location) {$("#location").val(article.location);}
				if (article.startdate) {
					$("#sdate").val(article.startdate.substring(0, 4)+"-"+article.startdate.substring(4, 6)+"-"+article.startdate.substring(6, 8));
					$("#stime").val(article.startdate.substring(8, 10)+":"+article.startdate.substring(10, 12)+":"+article.startdate.substring(12, 14));
				}
				if (article.enddate) {
					$("#edate").val(article.enddate.substring(0, 4)+"-"+article.enddate.substring(4, 6)+"-"+article.enddate.substring(6, 8));
					$("#etime").val(article.enddate.substring(8, 10)+":"+article.enddate.substring(10, 12)+":"+article.enddate.substring(12, 14));
				}
				if (article.hosted) {$("#hosted").val(article.hosted);}
				if (article.contact) {$("#contact").val(article.contact);}
				if (article.homepage) {$("#homepage").val(article.homepage);}
				
				if ("Y" == article.registration) {
					$("#registration-y").iCheck('check');
				} else {
					$("#registration-n").iCheck('check');
				}
				if (article.registration_startdate) {
					$("#registration_sdate").val(article.registration_startdate.substring(0, 4)+"-"+article.registration_startdate.substring(4, 6)+"-"+article.registration_startdate.substring(6, 8));
					$("#registration_stime").val(article.registration_startdate.substring(8, 10)+":"+article.registration_startdate.substring(10, 12)+":"+article.registration_startdate.substring(12, 14));
				}
				if (article.registration_enddate) {
					$("#registration_edate").val(article.registration_enddate.substring(0, 4)+"-"+article.registration_enddate.substring(4, 6)+"-"+article.registration_enddate.substring(6, 8));
					$("#registration_etime").val(article.registration_enddate.substring(8, 10)+":"+article.registration_enddate.substring(10, 12)+":"+article.registration_enddate.substring(12, 14));
				}
				
				if (article.banner) {
					$('.btn-banner').removeClass('hidden');
				}
				
				if (article.content) {
					$("#content").html(article.content);
					$('#summernote').summernote('code', article.content);
				}
				
				var $fileList = $("#file-list");
				var template = $("#file-item").html();
				Mustache.parse(template);
				for (var i = 0; i < fileList.length; i++) {
					files.push(fileList[i].idx);
					$fileList.append(Mustache.render(template, {
						idx:fileList[i].idx, filename:fileList[i].name
					}));
				}
				$("#files").val(files.join());
				
				addLecture(data.data.lectures);
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	
	Dropzone.options.myAwesomeDropzone = {
		init: function () {
			this.on("complete", function (data) {
				console.log(data);
				var res = JSON.parse(data.xhr.responseText);
				if (res.status == "success") {
					files.push(res.data);
					$("#files").val(files.join());
					var template = $("#file-item").html();
					Mustache.parse(template);
					$("#file-list").append(Mustache.render(template, {
						idx:res.data, filename:data.name
					}));
					this.removeFile(data);
				}
			});
		}
	};
	
	if ('KORSIS' == sitecd) {
		initLecture();
	}
	
	load();
	
	$('#btn-banner-view').click(function(){
		window.open('/api/society/workshop/${code}/${idx}/banner');
	});
	
	$('#btn-banner-delete').click(function(){
		$.ajax({type:"GET", url : "/api/admin/society/workshop/${code}/${idx}/banner/delete", cache:false
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("삭제되었습니다.");
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-modify").click(function(){
		$("#content").val($('#summernote').summernote('code'));
		
		var url = 'KORSIS' == sitecd ? '/api/admin/society/workshop/${code}/${idx}/modify/korsis' : '/api/admin/society/workshop/${code}/${idx}/modify';
		$("#submitForm").ajaxForm({
			type:"POST"
			, url:url
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					alert("수정되었습니다.");
					document.location.href='/admin/society/workshop/${code}';
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#submitForm").submit();
		
	});
	
	$(document).on('click', '.btn-file-delete', function(){
		var idx = $(this).data('idx');
		var newFiles = [];
		for (var i = 0; i < files.length; i++) {
			if (files[i] != idx) {
				newFiles.push(files[i]);
			}
		}
		files = newFiles;
		$("#files").val(files.join());
		$(this).parent().remove();
	});
});

var lectureSize = -1;
function initLecture() {
	lectureSize = -1;
	$('#lecture-wrapper').removeClass('hidden');
	$('#btn-lecture-add').click(function(){
		addLecture();
	});
	$(document).on('click', '.btn-lecture-del', function(){
		$('#lecture-item-'+$(this).data('no')).remove();
		if ($('.lecture-item').length == 0) {
			lectureSize = -1;
		}
	});
	$(document).on('change', '.btn-lecture-type', function(){
		var no = $(this).data('no');
		var $cost1 = $('#lecture-cost1-'+no);
		var $cost2 = $('#lecture-cost2-'+no);
		$cost1.val('');
		$cost2.val('');
		$cost2.removeClass('hidden');
		switch ($(this).val()) {
		case 'M':
			$cost1.attr('placeholder', '정회원');
			$cost2.attr('placeholder', '비회원');
			break;
		case 'L':
			$cost1.attr('placeholder', '처음수강');
			$cost2.attr('placeholder', '재수강');
			break;
		case 'S':
			$cost1.attr('placeholder', '');
			$cost2.addClass('hidden');
			break;
		case 'N':
			$cost1.attr('placeholder', '');
			$cost2.addClass('hidden');
			break;
		}
	});
	$(document).on('click', '.btn-course', function(){
		var no = $(this).data('no');
		if ($(this).prop('checked')) {
			$('.course-'+no).removeClass('hidden');
		} else {
			$('.course-'+no).addClass('hidden');
			$('.course-'+no+' input').val('');
		}
	});
}

function addLecture(lectures) {
	console.log(lectures);
	var $list = $("#lecture-list"); 
	var template = $("#lecture-item").html();
	Mustache.parse(template);
	if (lectures) {
		for (var i = 0; i < lectures.length; i++) {
			var l = lectures[i];
			lectureSize++;
			l.no = lectureSize;
			l.istypem = l.type == 'M';
			l.istypel = l.type == 'L';
			l.istypes = l.type == 'S';
			l.istypen = l.type == 'N';
			l.enddate_text = l.enddate ? l.enddate.substring(0, 4)+'-'+l.enddate.substring(4, 6)+'-'+l.enddate.substring(6, 8) : '';
			l.iscourse = l.use_course == 'Y';
			$list.append(Mustache.render(template, l));
		}
		$(".datepicker").datepicker({todayBtn: "linked", keyboardNavigation: false, forceParse: false, autoclose: true, format: "yyyy-mm-dd"});
	} else {
		lectureSize++;
		$list.append(Mustache.render(template, {no:lectureSize, istypem:true}));
		$(".datepicker").datepicker({todayBtn: "linked", keyboardNavigation: false, forceParse: false, autoclose: true, format: "yyyy-mm-dd"});
	}
}
</script>