<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>탈퇴 요청 회원</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>회원</li>
			<li class="active"><strong>탈퇴 요청 회원</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>회원 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<input type="hidden" name="page" id="page" value="1" />
								<input type="hidden" name="order" id="order" value="idx" />
								<input type="hidden" name="sort" id="sort" value="desc" />
								<input type="hidden" name="withdraw" value="Y" />
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색어를 입력해 주세요." class="form-control" value="" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">아이디</th>
									<th class="text-center">이름
										<i class="fa fa-sort-desc btn-sort" data-order="name"></i>
									</th>
									<th class="text-center">이메일</th>
									<th class="text-center">근무처
										<i class="fa fa-sort-desc btn-sort" data-order="office_type"></i>
									</th>
									<th class="text-center">연락처</th>
									<th class="text-center">회원가입일
										<i class="fa fa-sort-desc btn-sort" data-order="regdate"></i>
									</th>
									<th class="text-center">삭제요청일
										<i class="fa fa-sort-desc btn-sort" data-order="deldate"></i>
									</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">
		<a href="/admin/society/member/manage/{{id}}">{{id_text}}</a>
	</td>
	<td class="text-center">
		<a href="/admin/society/member/manage/{{id}}">{{name}}</a>
	</td>
	<td class="text-center"><a href="mailto:{{email}}">{{email}}</a></td>
	<td class="text-center">{{office_type}}</td>
	<td class="text-center">{{phone1}}</td>
	<td class="text-center">{{regdate_text}}</td>
	<td class="text-center">{{deldate_text}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-success btn-recovery" data-idx="{{idx}}"><i class="fa fa-refresh"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	$('#page').val(page);
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	$.ajax({type:"GET", url:"/api/admin/society/member/list", cache:false
		, data : $('#searchForm').serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					
					l.no = virtualNo--;
					l.regdate_text = moment(l.regdate, 'YYYYMMDDHHmmss').format('YYYY-MM-DD');
					l.deldate_text = !l.date_delete ? '-' : moment(l.date_delete, 'YYYYMMDDHHmmss').format('YYYY-MM-DD');
					l.isTypeRegular = l.society_type=='R';
					l.isTypeMember = l.society_type=='M';
					l.isTypeWait = l.society_type=='W';
					l.id_text = l.id.substring(0, 3);
					for (var j = 3; j < l.id.length; j++) {
						l.id_text += '*';
					}
					
					var rendered = Mustache.render(listTemplate, l);
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	loadList(1);
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	
	$('#searchForm').submit(function(){
		loadList(1);
		return false;
	});
	
	$('.btn-sort').click(function(){
		var order = $(this).data('order');
		var sort = $('#sort').val();
		
		$('#order').val(order);
		$('#sort').val(sort=='desc' ? 'asc' : 'desc');
		$('.btn-sort').removeClass('fa-sort-desc').removeClass('fa-sort-asc').addClass('fa-sort-desc');
		$(this).removeClass('fa-sort-desc').removeClass('fa-sort-asc').addClass('fa-sort-'+(sort=='desc' ? 'asc' : 'desc'));
		loadList(1);
	});
	
	$(document).on('click', '.btn-recovery', function(){
		if (confirm("선택하신 회원을 복원하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/admin/society/member/recovery", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm("선택하신 회원을 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/admin/member/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
});


</script>

