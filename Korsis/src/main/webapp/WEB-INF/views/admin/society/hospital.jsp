<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>병원 검색</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li class="active"><strong>병원 검색</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>병원 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<input type="hidden" name="page" id="page" value="1" />
								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-4">
										<select name="category" id="category" class="form-control">
											<option value="">병원구분</option>
											<option value="의원">의원</option>
											<option value="개인병원">개인병원</option>
											<option value="준종합병원">준종합병원</option>
											<option value="대학병원">대학병원</option>
											<option value="보건소">보건소</option>
											<option value="기타">기타</option>
										</select>
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력해 주세요." class="form-control" value="" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">병원구분</th>
									<th class="text-center">병원명</th>
									<th class="text-center">의사명</th>
									<th class="text-center">전화번호</th>
									<th class="text-center">홈페이지</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div id="write-wrapper" class="ibox">
				<div class="ibox-title">
					<h5>병원 등록</h5>
				</div>
				<div class="ibox-content">
					<form id="writeForm" class="form-horizontal">
						<div class="form-group">
							<div class="col-sm-12">
								<select name="category" class="form-control">
									<option value="의원">의원</option>
									<option value="개인병원">개인병원</option>
									<option value="준종합병원">준종합병원</option>
									<option value="대학병원">대학병원</option>
									<option value="보건소">보건소</option>
									<option value="기타">기타</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">병원명</label>
							<div class="col-sm-9">
								<input type="text" name="office" placeholder="병원명" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">의사명</label>
							<div class="col-sm-9">
								<input type="text" name="name" placeholder="의사명" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">병원주소</label>
							<div class="col-sm-9">
								<div class="input-group">
									<input type="text" name="zipcode" placeholder="우편번호" class="form-control address-picker zipcode" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default address-picker"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="address" placeholder="기본주소" class="form-control address-picker address" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="address_etc" placeholder="상세주소" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">홈페이지</label>
							<div class="col-sm-9">
								<input type="text" name="homepage" placeholder="홈페이지" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">전화번호</label>
							<div class="col-sm-9">
								<input type="text" name="phone" placeholder="전화번호" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">팩스번호</label>
							<div class="col-sm-9">
								<input type="text" name="fax" placeholder="팩스번호" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류1</label>
							<div class="col-sm-9">
								<input type="text" name="var1" placeholder="분류1" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류2</label>
							<div class="col-sm-9">
								<input type="text" name="var2" placeholder="분류2" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류3</label>
							<div class="col-sm-9">
								<input type="text" name="var3" placeholder="분류3" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류4</label>
							<div class="col-sm-9">
								<input type="text" name="var4" placeholder="분류4" class="form-control" />
							</div>
						</div>
						
						<hr />
						
						<div class="text-right">
							<button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> 등록</button>
						</div>
					</form>
				</div>
			</div>
			
			<div id="modify-wrapper" class="ibox hidden">
				<div class="ibox-title">
					<h5>병원 수정</h5>
				</div>
				<div class="ibox-content">
					<form id="modifyForm" class="form-horizontal">
						<input type="hidden" name="idx" id="idx-modify" />
						<div class="form-group">
							<div class="col-sm-12">
								<select name="category" id="category-modify" class="form-control">
									<option value="의원">의원</option>
									<option value="개인병원">개인병원</option>
									<option value="준종합병원">준종합병원</option>
									<option value="대학병원">대학병원</option>
									<option value="보건소">보건소</option>
									<option value="기타">기타</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">병원명</label>
							<div class="col-sm-9">
								<input type="text" name="office" id="office-modify" placeholder="병원명" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">의사명</label>
							<div class="col-sm-9">
								<input type="text" name="name" id="name-modify" placeholder="의사명" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">병원주소</label>
							<div class="col-sm-9">
								<div class="input-group">
									<input type="text" name="zipcode" id="zipcode-modify" placeholder="우편번호" class="form-control address-picker zipcode" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default address-picker"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="address" id="address-modify" placeholder="기본주소" class="form-control address-picker address" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="address_etc" id="address_etc-modify" placeholder="상세주소" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">홈페이지</label>
							<div class="col-sm-9">
								<input type="text" name="homepage" id="homepage-modify" placeholder="홈페이지" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">전화번호</label>
							<div class="col-sm-9">
								<input type="text" name="phone" id="phone-modify" placeholder="전화번호" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">팩스번호</label>
							<div class="col-sm-9">
								<input type="text" name="fax" id="fax-modify" placeholder="팩스번호" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류1</label>
							<div class="col-sm-9">
								<input type="text" name="var1" id="var1-modify" placeholder="분류1" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류2</label>
							<div class="col-sm-9">
								<input type="text" name="var2" id="var2-modify" placeholder="분류2" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류3</label>
							<div class="col-sm-9">
								<input type="text" name="var3" id="var3-modify" placeholder="분류3" class="form-control" />
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">분류4</label>
							<div class="col-sm-9">
								<input type="text" name="var4" id="var4-modify" placeholder="분류4" class="form-control" />
							</div>
						</div>
						
						<hr />
						
						<div class="text-right">
							<button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> 수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<!-- Daum 주소 -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{category}}</td>
	<td class="text-center">{{office}}</td>
	<td class="text-center">{{name}}</td>
	<td class="text-center">{{phone}}</td>
	<td class="text-center"><button type="button" class="btn btn-default btn-xs btn-link" data-url="{{homepage}}"><i class="fa fa-anchor"></i> 홈페이지</button></td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	$('#page').val(page);
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	$.ajax({type:"GET", url:"/api/admin/society/hospital/list", cache:false
		, data : $('#searchForm').serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = virtualNo--;
					$list.append(Mustache.render(listTemplate, l));
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	loadList(1);
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	
	$('#searchForm').submit(function(){
		loadList(1);
		return false;
	});
	
	$('.address-picker').click(function(){
		new daum.Postcode({
	        oncomplete: function(data) {
	        	$('.zipcode').val(data.zonecode);
	        	$('.address').val(data.address);
	        }
	    }).open();
	});
	
	$(document).on('click', '.btn-link', function(){
		window.open($(this).data('url'));
	});
	
	$('#writeForm').submit(function(){
		$.ajax({type:"POST", url:"/api/admin/society/hospital/add", cache:false
			, data : $('#writeForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					$('#writeForm')[0].reset();
					loadList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$(document).on('click', '.btn-modify', function(){
		var idx = $(this).data('idx');
		$.ajax({type:"GET", url:"/api/admin/society/hospital/item", cache:false
			, data : {idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$('#idx-modify').val(idx);
					$('#category-modify').val(data.data.category);
					$('#office-modify').val(data.data.office);
					$('#name-modify').val(data.data.name);
					$('#zipcode-modify').val(data.data.zipcode);
					$('#address-modify').val(data.data.address);
					$('#address_etc-modify').val(data.data.address_etc);
					$('#homepage-modify').val(data.data.homepage);
					$('#phone-modify').val(data.data.phone);
					$('#fax-modify').val(data.data.fax);
					$('#var1-modify').val(data.data.var1);
					$('#var2-modify').val(data.data.var2);
					$('#var3-modify').val(data.data.var3);
					$('#var4-modify').val(data.data.var4);
					
					$('#write-wrapper').addClass('hidden');
					$('#modify-wrapper').removeClass('hidden');
					
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('#modifyForm').submit(function(){
		$.ajax({type:"POST", url:"/api/admin/society/hospital/modify", cache:false
			, data : $('#modifyForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					$('#writeForm')[0].reset();
					$('#modifyForm')[0].reset();
					$('#write-wrapper').removeClass('hidden');
					$('#modify-wrapper').addClass('hidden');
					loadList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm("선택하신 병원을 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/admin/society/hospital/remove", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
});


</script>

