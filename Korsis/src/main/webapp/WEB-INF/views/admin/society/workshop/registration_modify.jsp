<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-popup-header.jsp"%>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-content">
					<form id="modifyForm" class="form-horizontal">
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<caption>행사선택</caption>
								<thead>
									<tr>
										<th>행사</th>
										<th colspan="2">참여방식</th>
										<th colspan="2">Course</th>
									</tr>
								</thead>
								<tbody id="workshop-lecture-wrapper"></tbody>
							</table>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">한글 성명</label>
							<div class="col-sm-4"><input type="text" name="name" id="name" class="form-control" placeholder="예) 홍길동 (띄어쓰기 없이 입력, 홍 길동[x], 김 길[x])" /></div>
							<label class="col-sm-2 control-label">영문 성명</label>
							<div class="col-sm-4"><input type="text" name="name_eng" id="name_eng" class="form-control" placeholder="예) Hong, Gil Dong" /></div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">의사면허번호</label>
							<div class="col-sm-4"><input type="text" name="license" id="license" class="form-control" placeholder="의사면허번호를 입력해 주세요." /></div>
							<label class="col-sm-2 control-label">전문 과목</label>
							<div class="col-sm-4"><input type="text" name="major" id="major" class="form-control" placeholder="전문과목을 입력해 주세요." /></div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">병원명</label>
							<div class="col-sm-4"><input type="text" name="office" id="office" class="form-control" placeholder="병원명을 입력해 주세요." /></div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">주소</label>
							<div class="col-sm-4">
								<div class="input-group">
									<input type="text" name="zipcode" id="zipcode" class="address-picker form-control" readonly="readonly" placeholder="우편번호" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default address-picker"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
							<div class="col-sm-6"><input type="text" name="address" id="address" readonly="readonly" class="address-picker form-control" placeholder="검색하여 주소를 입력해 주세요." /></div>
						</div>
						<div class="form-group">
							<div class="col-sm-10 col-sm-offset-2"><input type="text" name="address_etc" id="address_etc" class="form-control" placeholder="상세주소를 입력해 주세요." /></div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">휴대폰</label>
							<div class="col-sm-4"><input type="text" name="mobile" id="mobile" class="form-control" placeholder="휴대폰 번호를 입력해 주세요." /></div>
							<label class="col-sm-2 control-label">이메일</label>
							<div class="col-sm-4"><input type="text" name="email" id="email" class="form-control" placeholder="이메일을 입력해 주세요." /></div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">입금액</label>
							<div class="col-sm-4"><input type="number" name="income_cost" id="income_cost" class="form-control" readonly="readonly" placeholder="입금액은 자동 계산됩니다." /></div>
							<label class="col-sm-2 control-label">입금 예정일</label>
							<div class="col-sm-4"><input type="text" name="income_date" id="income_date" placeholder="yyyy-MM-dd" class="datepicker form-control" /></div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-2 control-label">입금자 명</label>
							<div class="col-sm-4"><input type="text" name="income_name" id="income_name" class="form-control" placeholder="입금자명을 입력해 주세요." /></div>
							<div class="col-sm-6">
								<h5>입금 계좌번호</h5>
								<p>
									등록비를 아래 계좌로 입금해 주세요.<br />
									우리은행: 1002-753-360688 (예금주: 신동아)<br />
									※ 입금 시 실명과 병원명을 기재해 주시기 바랍니다. 
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 text-right">
								<button type="submit" class="btn btn-warning"><i class="fa fa-edit"></i> 수정</button>
								<button type="reset" class="btn btn-default btn-close"><i class="fa fa-ban"></i> 닫기</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

	<script id="workshop-lecture-item" type="x-tmpl-mustache">
	<tr>
		{{#istypen}}
			<td colspan="3">
				<label><input type="checkbox" name="lectures-{{idx}}" value="Y" id="lectures-{{idx}}" data-type="{{type}}" data-idx="{{idx}}" class="lectures" /><i></i> {{title}} 참가</label>
				<input type="hidden" name="type-{{idx}}" id="type-{{idx}}-1" value="1" data-cost="{{cost1}}" class="type-{{idx}}" />
			</td>
		{{/istypen}}
		{{^istypen}}
			<td>
				<label><input type="checkbox" name="lectures-{{idx}}" value="Y" id="lectures-{{idx}}" data-type="{{type}}" data-idx="{{idx}}" class="lectures" /><i></i> {{title}} 참가</label>
			</td>
		{{/istypen}}
		{{#istypes}}
			<td>
				<label>
					<input type="radio" name="type-{{idx}}" id="type-{{idx}}-1" value="1" data-cost="{{cost1}}" data-idx="{{idx}}" class="type-{{idx}} lecture-type lecture-type-{{idx}}" /><i></i> 전공의
				</label>
			</td>
			<td>
				<label>
					<input type="radio" name="type-{{idx}}" id="type-{{idx}}-2" value="2" data-cost="{{cost2}}" data-idx="{{idx}}" class="type-{{idx}} lecture-type lecture-type-{{idx}} hidden" /><i></i> 
				</label>
			</td>
		{{/istypes}}
		{{#istypem}}
			<td>
				<label>
					<input type="radio" name="type-{{idx}}" id="type-{{idx}}-1" value="1" data-cost="{{cost1}}" data-idx="{{idx}}" class="type-{{idx}} lecture-type lecture-type-{{idx}}" /><i></i> 정회원
				</label>
			</td>
			<td>
				<label>
					<input type="radio" name="type-{{idx}}" id="type-{{idx}}-2" value="2" data-cost="{{cost2}}" data-idx="{{idx}}" class="type-{{idx}} lecture-type lecture-type-{{idx}}" /><i></i> 비회원
				</label>
			</td>
		{{/istypem}}
		{{#istypel}}
			<td>
				<label>
					<input type="radio" name="type-{{idx}}" id="type-{{idx}}-1" value="1" data-cost="{{cost1}}" data-idx="{{idx}}" class="type-{{idx}} lecture-type lecture-type-{{idx}}" /><i></i> 처음수강
				</label>
			</td>
			<td>
				<label>
					<input type="radio" name="type-{{idx}}" id="type-{{idx}}-2" value="2" data-cost="{{cost2}}" data-idx="{{idx}}" class="type-{{idx}} lecture-type lecture-type-{{idx}}" /><i></i> 재수강
				</label>
			</td>
		{{/istypel}}
		<td>
			{{#is_course1}}
				<label>
					<input type="checkbox" name="course1-{{idx}}" id="course1-{{idx}}" data-idx="{{idx}}" value="Y" class="courses course-{{idx}}" /><i></i> {{course1}}참가
				</label>
			{{/is_course1}}
		</td>
		<td>
			{{#is_course2}}
				<label>
					<input type="checkbox" name="course2-{{idx}}" id="course2-{{idx}}" data-idx="{{idx}}" value="Y" class="courses course-{{idx}}" /><i></i> {{course2}}참가
				</label>
			{{/is_course2}}
		</td>
	</tr>
	</script>


<%@ include file="/WEB-INF/views/include/admin-popup-footer.jsp"%>
<!-- Daum 주소 -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>

<script type="text/javascript">
$(function(){
	loadWorkshop();
	
	$('.btn-close').click(function(){ window.close(); });
	$(document).on('click', '.address-picker', function(){
		openAddress();
	});
	
	$(document).on('click', '.lectures', function(){
		if (!$(this).prop('checked')) {
			var idx = $(this).data('idx');
			$('.lecture-type-'+idx).prop('checked', false);
			$('.course-'+idx).prop('checked', false);
		}
		lectureCost();
	});
	
	$(document).on('click', '.lecture-type', function(){
		if ($(this).prop('checked')) {
			var idx = $(this).data('idx');
			$('#lectures-'+idx).prop('checked', true);
		}
		lectureCost();
	});
	
	$(document).on('click', '.courses', function(){
		if ($(this).prop('checked')) {
			var idx = $(this).data('idx');
			$('#lectures-'+idx).prop('checked', true);
		}
		lectureCost();
	});
	
	$('#modifyForm').submit(function(){
		$.ajax({type:"POST", url : "/api/admin/society/workshop/${code}/${widx}/${idx}/modify", cache:false
			, data:$('#modifyForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					alert('수정되었습니다.');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
});

function openAddress() {
	new daum.Postcode({
        oncomplete: function(data) {
        	console.log(data);
        	$('#zipcode').val(data.zonecode);
        	$('#address').val(data.address);
        }
    }).open();
}

function lectureCost() {
	var total = 0;
	$('.lectures').each(function(){
		if ($(this).prop('checked')) {
			var idx = $(this).data('idx');
			if ($(this).data('type') == 'N') {
				total += parseInt($('#type-'+idx+'-1').data('cost'));
			} else {
				$('.lecture-type-'+idx).each(function(){
					if ($(this).prop('checked')) {
						total += parseInt($(this).data('cost'));
					}
				});
			}
		}
	});
	$('#income_cost').val(total);
}

function loadWorkshop() {
	$.ajax({type:"GET", url : "/api/society/workshop/${code}/${widx}", cache:false
		, data : {}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				assortLectures(data.data.lectures);
				loadRegistration();
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadRegistration() {
	$.ajax({type:"GET", url : "/api/admin/society/workshop/${code}/${widx}/${idx}", cache:false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				assortForm(data.data);
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function assortLectures(lecture) {
	if (!lecture) {
		return;
	}

	var $lw = $('#workshop-lecture-wrapper');
	var lectureTemplate = $('#workshop-lecture-item').html();
	Mustache.parse(lectureTemplate);
	for (var i = 0; i < lecture.length; i++) {
		var l = lecture[i];
		l.istypem = l.type == "M";
		l.istypel = l.type == "L";
		l.istypen = l.type == "N";
		l.istypes = l.type == "S";
		l.is_course = l.use_course=="Y";
		l.is_course1 = l.use_course=="Y" && l.course1;
		l.is_course2 = l.use_course=="Y" && l.course2;
		$lw.append(Mustache.render(lectureTemplate, l));
	}
}

function assortForm(data) {
	$('#modify-name').text(data.name);
	$('#idx').val(data.idx);
	$('#name').val(data.name);
	$('#name_eng').val(data.name_eng);
	$('#license').val(data.license);
	$('#major').val(data.major);
	$('#office').val(data.office);
	$('#zipcode').val(data.zipcode);
	$('#address').val(data.address);
	$('#address_etc').val(data.address_etc);
	$('#mobile').val(data.mobile);
	$('#email').val(data.email);
	$('#income_cost').val(data.income_cost);
	$('#income_date').val(data.income_date);
	$('#income_name').val(data.income_name);
	
	for (var i = 0; i < data.lectures.length; i++) {
		var l = data.lectures[i];
		$('#lectures-'+l.lidx).prop('checked', true);
		if (l.type == "1") {
			$('#type-'+l.lidx+'-1').prop('checked', true);
		} else {
			$('#type-'+l.lidx+'-2').prop('checked', true);
		}
		if (l.course1 == "Y") {
			$('#course1-'+l.lidx).prop('checked', true);
		}
		if (l.course2 == "Y") {
			$('#course2-'+l.lidx).prop('checked', true);
		}
	}
	
}
</script>