<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${boardCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>게시판</li>
			<li class="active"><strong>${boardCfg.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>게시물 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">제목</th>
									<th class="text-center">작성자</th>
									<th class="text-center">조회</th>
									<th class="text-center">배너</th>
									<th class="text-center">등록일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>검색</h5>
						</div>
						<div class="ibox-content">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<c:choose>
									<c:when test="${boardCfg.category eq 'Y'}">
										<div class="form-group">
											<label class="col-sm-3 control-label">카테고리</label>
											<div class="col-sm-9">
												<select name="category" id="category" class="form-control">
													<option value="">카테고리 선택</option>
													<c:forEach items="${fn:split(boardCfg.categories, ',')}" var="c">
														<option value="${c}">${c}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="hidden" name="category" id="category" value="" />
									</c:otherwise>
								</c:choose>
								<div class="form-group">
									<label class="col-sm-3 control-label">
										키워드
									</label>
									<div class="col-sm-9">
										<div class="input-group">
											<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력해 주세요." class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="button" id="btn-search" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>게시물 등록</h5>
						</div>
						<div class="ibox-content">
							<button id="btn-write" type="button" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> 게시물 등록</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item-notice" type="x-tmpl-mustache">
<tr>
	<td class="text-center"><span class="label label-info">공지</span></td>
	<td><a href="/admin/board/${code}/{{idx}}">{{subject}}</a> {{comments}}</td>
	<td class="text-center">{{name}}</td>
	<c:choose>
		<c:when test="${boardCfg.absolute_read_count eq 'N'}">
			<td class="text-center">
				<input type="number" value="{{read}}" data-idx="{{idx}}" class="form-control cnt-read" />
			</td>
		</c:when>
		<c:otherwise>
			<td class="text-center">{{read}}회</td>
		</c:otherwise>
	</c:choose>
	<td class="text-center">
		{{#banner}}
	    	<input type="checkbox" name="banner" value="Y" class="banner" checked="checked" data-idx="{{idx}}" />
		{{/banner}}
		{{^banner}}
			<input type="checkbox" name="banner" value="Y" class="banner" data-idx="{{idx}}" />
		{{/banner}}
	</td> 
	<td class="text-center">{{date}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{{no}}}</td>
	<td>{{{thread}}} {{{lock}}} <a href="/admin/board/${code}/{{idx}}">{{subject}}</a> {{comments}}</td>
	<td class="text-center">{{name}}</td>
	<c:choose>
		<c:when test="${boardCfg.absolute_read_count eq 'N'}">
			<td class="text-center">
				<input type="number" value="{{read}}" data-idx="{{idx}}" class="form-control cnt-read" />
			</td>
		</c:when>
		<c:otherwise>
			<td class="text-center">{{read}}회</td>
		</c:otherwise>
	</c:choose>
	<td class="text-center">
		{{#banner}}
	    	<input type="checkbox" name="banner" value="Y" class="banner" checked="checked" data-idx="{{idx}}" />
		{{/banner}}
		{{^banner}}
			<input type="checkbox" name="banner" value="Y" class="banner" data-idx="{{idx}}" />
		{{/banner}}
	</td> 
	<td class="text-center">{{date}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>


<script type="text/javascript">
function load(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	 
	$.ajax({type:"GET", url : "/api/board/${code}", cache:false
		, data : {page:page, keyword:$("#keyword").val(), category:$("#category").val()}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var cfg = data.data.cfg;
				var notice = data.data.notice;
				var list = data.data.list;
				var paging = data.data.paging;
				
				var noticeTemplate = $("#list-item-notice").html();
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(noticeTemplate);
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				for (var i = 0; i < notice.length; i++) {
					var n = notice[i];
					var rendered = Mustache.render(noticeTemplate, {
						idx:n.idx, subject:n.subject, name:n.name, date:n.regdate.substring(0, 4)+"-"+n.regdate.substring(4, 6)+"-"+n.regdate.substring(6, 8)
						, read:n.cnt_read, banner:n.banner=='Y'
						, comments : n.cnt_comment > 0 ? "["+n.cnt_comment+"]" : ""
					});
					$list.append(rendered);
				}
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					if (l.thread.length > 1) {
						var thread = "";
						for (var j = 1; j < l.thread.length; j++) {
							thread += "&nbsp;&nbsp;&nbsp;";
						}
						thread += "<i class=\"fa fa-reply\"></i>";
					} else {
						var thread = "";
					}
					
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, idx:l.idx, subject:l.subject, name:l.name, date:l.regdate.substring(0, 4)+"-"+l.regdate.substring(4, 6)+"-"+l.regdate.substring(6, 8)
						, lock : l.hidden == "Y" ? "<i class=\"fa fa-lock\"></i>" : ""
						, thread : thread
						, read:l.cnt_read, banner:l.banner=='Y'
						, comments : l.cnt_comment > 0 ? "["+l.cnt_comment+"]" : ""
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	load();
	$("#btn-write").click(function(){
		document.location.href='/admin/board/${code}/write';
	});
	$("#btn-search").click(function(){
		load(1);
	});
	$(document).on('click', '.btn-page', function(){
		load($(this).data('page'));
	});
	$(document).on('click', '.btn-modify', function(){
		document.location.href='/admin/board/${code}/'+$(this).data('idx')+'/modify';
	});
	$(document).on('click', '.btn-delete', function(){
		if (confirm('게시물을 삭제하시겠습니까?\n삭제한 게시물은 되돌릴 수 없습니다.')) {
			var url = "/api/board/${code}/"+$(this).data("idx")+"/delete";
			$.ajax({type:"GET", url : url, cache:false
				, data : {}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						alert("삭제되었습니다.");
						load();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('change', '.cnt-read', function(){
		var idx = $(this).data('idx');
		var size = $(this).val();
		$.ajax({type:"POST", url : "/api/board/${code}/"+idx+"/count", cache:false
			, data : {size:size}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					load();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		
	});
	$(document).on('click', '.banner', function(){
		var idx = $(this).data('idx');
		var status = $(this).prop("checked") ? 'Y' : 'N';
		$.ajax({type:"POST", url : "/api/board/${code}/"+idx+"/banner", cache:false
			, data : {status:status}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					load();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		
	});
	
});
</script>