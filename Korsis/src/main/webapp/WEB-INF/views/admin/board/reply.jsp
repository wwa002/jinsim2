<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${boardCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>게시판</li>
			<li>${boardCfg.title}</li>
			<li class="active"><strong>답변</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-reply" class="btn btn-primary"><i class="fa fa-reply"></i> 답변하기</button>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>답변 작성</h5>
				</div>
				<div class="ibox-content">
					<form id="submitForm" class="form-horizontal">
						<input type="hidden" name="html" value="Y" />
						<input type="hidden" name="files" id="files" value="" />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">답변 제목</label>
									<div class="col-sm-8">
										<input type="text" name="subject" id="subject" class="form-control" />
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<textarea name="content" id="content" class="hidden"></textarea>
								<div id="summernote"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>첨부파일</h5>
				</div>
				<div class="ibox-content">
					<form id="my-awesome-dropzone" action="/api/board/${code}/upload/dropzone" class="dropzone" action="#">
						<div class="dropzone-previews"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script type="text/javascript">
var files = [];

function load() {
	$.ajax({type:"GET", url : "/api/board/${code}/${idx}", cache:false
		, data : {}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var article = data.data.article;
				$("#subject").val(article.subject);
				$("#content").html(article.html == "Y" ? article.content : nl2br(article.content));
				$('#summernote').summernote('code', article.html == "Y" ? article.content : nl2br(article.content));
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	
	Dropzone.options.myAwesomeDropzone = {
		init: function () {
			this.on("complete", function (data) {
				var res = JSON.parse(data.xhr.responseText);
				if (res.status == "success") {
					for (var i = 0; i < res.data.length; i++) {
						files.push(res.data[i]);
					}
					$("#files").val(files.join());
				}
			});
		}
	};
	
	load();
	
	$("#btn-reply").click(function(){
		$("#content").val($('#summernote').summernote('code'));
		
		$.ajax({type:"POST", url : "/api/board/${code}/${idx}/reply", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("등록되었습니다.");
					document.location.href='/admin/board/${code}';
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>