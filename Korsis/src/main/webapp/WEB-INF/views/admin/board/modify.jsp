<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${boardCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>게시판</li>
			<li>${boardCfg.title}</li>
			<li class="active"><strong>수정</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-modify" class="btn btn-primary"><i class="fa fa-save"></i> 수정하기</button>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>수정</h5>
				</div>
				<div class="ibox-content">
					<form id="submitForm" class="form-horizontal">
						<input type="hidden" name="idx" value="${idx}" />
						<input type="hidden" name="html" value="Y" />
						<input type="hidden" name="hidden" value="N" />
						<input type="hidden" name="files" id="files" value="" />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">제목</label>
									<c:choose>
										<c:when test="${boardCfg.notice eq 'Y'}">
											<div class="col-sm-8">
												<input type="text" name="subject" id="subject" class="form-control" />
											</div>
											<div class="col-sm-2">
												<div class="i-checks">
													<label>
														<input type="checkbox" value="Y" id="notice" name="notice" /> 공지
													</label>
												</div>
											</div>
										</c:when>
										<c:otherwise>
											<div class="col-sm-10">
												<input type="text" name="subject" id="subject" class="form-control" />
											</div>
											<input type="hidden" name="notice" value="N" />
										</c:otherwise>
									</c:choose>
								</div>
								<c:choose>
									<c:when test="${boardCfg.category eq 'Y'}">
										<div class="form-group">
											<label class="col-sm-2 control-label">카테고리</label>
											<div class="col-sm-10">
												<select name="category" id="category" class="form-control">
													<option value="">카테고리 선택</option>
													<c:forEach items="${fn:split(boardCfg.categories, ',')}" var="c">
														<option value="${c}">${c}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="hidden" id="category" name="category" value="" />
									</c:otherwise>
								</c:choose>
								
								<c:if test="${boardCfg.popup eq 'Y' }">
									<div class="form-group">
										<label class="col-sm-2 control-label">팝업</label>
										<div class="col-sm-4">
											<div class="i-checks">
												<label>
													<input type="checkbox" value="Y" name="popup" id="popup" /> 팝업사용
												</label>
											</div>
										</div>
										<label class="col-sm-2 control-label popup hidden">팝업 디자인</label>
										<div class="col-sm-4 popup hidden">
											<select name="popup_skin" id="popup_skin" class="form-control">
												<option value="popup1">스킨 1</option>
												<option value="popup2">스킨 2</option>
												<option value="popup3">스킨 3</option>
											</select>
										</div>
										<!-- 
										<div class="col-sm-2 popup hidden">
											<button type="button" class="btn btn-default btn-block"><i class="fa fa-laptop"></i> 미리보기</button>
										</div>
										 -->
									</div>
									<div class="form-group popup hidden">
										<label class="col-sm-2 control-label">팝업 사이즈</label>
										<div class="col-sm-2">
											<input type="number" name="popup_width" id="popup_width" placeholder="가로"  class="form-control" />
										</div>
										<div class="col-sm-2">
											<input type="number" name="popup_height" id="popup_height" placeholder="세로"  class="form-control" />
										</div>
										<label class="col-sm-2 control-label">팝업 위치</label>
										<div class="col-sm-2">
											<input type="number" name="popup_top" id="popup_top" placeholder="위에서"  class="form-control" />
										</div>
										<div class="col-sm-2">
											<input type="number" name="popup_left" id="popup_left" placeholder="왼쪽에서"  class="form-control" />
										</div>
									</div>
									<div class="form-group popup hidden">
										<label class="col-sm-2 control-label">팝업 URL</label>
										<div class="col-sm-4">
											<input type="text" name="popup_url" id="popup_url" placeholder="http://www.example.com"  class="form-control" />
										</div>
										<label class="col-sm-2 control-label">팝업 날짜</label>
										<div class="col-sm-2">
											<input type="text" name="popup_startdate" id="popup_startdate" placeholder="yyyy-MM-dd"  class="form-control datepicker" />
										</div>
										<div class="col-sm-2">
											<input type="text" name="popup_enddate" id="popup_enddate" placeholder="yyyy-MM-dd"  class="form-control datepicker" />
										</div>
									</div>
								</c:if>
								
								<c:choose>
									<c:when test="${siteInfo.sitecd eq 'PPPP' and boardCfg.code eq 'partner'}">
										<div class="form-group">
											<label class="col-sm-2 control-label">제휴병원</label>
											<div class="col-sm-5">
												<div class="input-group">
													<input type="text" id="hospital-name" value="" class="form-control" />
													<span class="input-group-btn">
														<button type="button" id="hospital-search" class="btn btn-default"><i class="fa fa-search"></i></button>
													</span>
												</div>
											</div>
											<div class="col-sm-5 hidden" id="var1-wrapper">
												<select name="var1" id="var1" class="form-control">
													<option value="">:::: 병원선택 ::::</option>
												</select>
											</div>
										</div>
									</c:when>
									<c:when test="${siteInfo.sitecd eq 'BEAUTYRUN' and boardCfg.code eq 'partner'}">
										<div class="form-group">
											<label class="col-sm-2 control-label">아카데미</label>
											<div class="col-sm-5">
												<div class="input-group">
													<input type="text" id="academy-name" value="" class="form-control" />
													<span class="input-group-btn">
														<button type="button" id="academy-search" class="btn btn-default"><i class="fa fa-search"></i></button>
													</span>
												</div>
											</div>
											<div class="col-sm-5 hidden" id="var1-wrapper">
												<select name="var1" id="var1" class="form-control">
													<option value="">:::: 업체선택 ::::</option>
												</select>
											</div>
										</div>
									</c:when>
								</c:choose>
							</div>
							<div class="col-sm-12">
								<textarea name="content" id="content" class="hidden"></textarea>
								<div id="summernote"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>첨부파일</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<ul id="file-list">
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<form id="my-awesome-dropzone" action="/api/board/${code}/upload/dropzone" class="dropzone" action="#">
								<div class="dropzone-previews"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="file-item" type="x-tmpl-mustache">
<li>
	{{filename}} <button type="button" data-idx="{{idx}}" class="btn btn-xs btn-danger btn-file-delete"><i class="fa fa-trash"></i></button>
</li>
</script>

<script type="text/javascript">
var files = [];
var var1 = '';

function load() {
	$.ajax({type:"GET", url : "/api/board/${code}/${idx}", cache:false
		, data : {}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var article = data.data.article;
				var fileList = data.data.files;
				$("#hidden").val(article.hidden);
				if ("Y" == article.notice) {
					$("#notice").iCheck('check');
				}
				$("#category").val(article.category);
				
				$("#subject").val(article.subject);
				$("#content").html(article.html == "Y" ? article.content : nl2br(article.content));
				$('#summernote').summernote('code', article.html == "Y" ? article.content : nl2br(article.content));
				
				var $fileList = $("#file-list");
				var template = $("#file-item").html();
				Mustache.parse(template);
				for (var i = 0; i < fileList.length; i++) {
					files.push(fileList[i].idx);
					$fileList.append(Mustache.render(template, {
						idx:fileList[i].idx, filename:fileList[i].name
					}));
				}
				$("#files").val(files.join());
				
				if ('Y' == article.popup) {
					$('#popup').iCheck('check');
					$('.popup').removeClass('hidden');
					$('#popup_skin').val(article.popup_skin);
					$('#popup_width').val(article.popup_width);
					$('#popup_height').val(article.popup_height);
					$('#popup_top').val(article.popup_top);
					$('#popup_left').val(article.popup_left);
					$('#popup_url').val(article.popup_url);
					$('#popup_startdate').val(moment(article.popup_startdate, 'YYYYMMDD').format('YYYY-MM-DD'));
					$('#popup_enddate').val(moment(article.popup_enddate, 'YYYYMMDD').format('YYYY-MM-DD'));
				}
				
				if ('${siteInfo.sitecd}' == 'PPPP' && '${code}' == 'partner' && article.var1) {
					var1 = article.var1
					loadHospital();
				} else if ('${siteInfo.sitecd}' == 'BEAUTYRUN' && '${code}' == 'partner' && article.var1) {
					var1 = article.var1
					loadAcademy();
				}
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	$("#summernote").summernote({height:300, minHeight:300, maxHeight:600, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	
	Dropzone.options.myAwesomeDropzone = {
		init: function () {
			this.on("complete", function (data) {
				console.log(data);
				var res = JSON.parse(data.xhr.responseText);
				if (res.status == "success") {
					files.push(res.data);
					$("#files").val(files.join());
					var template = $("#file-item").html();
					Mustache.parse(template);
					$("#file-list").append(Mustache.render(template, {
						idx:res.data, filename:data.name
					}));
					this.removeFile(data);
				}
			});
		}
	};
	
	load();
	
	$("#btn-modify").click(function(){
		$("#content").val($('#summernote').summernote('code'));
		
		$.ajax({type:"POST", url : "/api/board/${code}/${idx}/modify", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("수정되었습니다.");
					document.location.href='/admin/board/${code}/${idx}';
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-file-delete', function(){
		var idx = $(this).data('idx');
		var newFiles = [];
		for (var i = 0; i < files.length; i++) {
			if (files[i] != idx) {
				newFiles.push(files[i]);
			}
		}
		files = newFiles;
		$("#files").val(files.join());
		$(this).parent().remove();
	});
	
	$("#hospital-search").click(function(){
		var keyword = $("#hospital-name").val();
		if (!keyword) {
			alert("병원명을 입력후 검색해 주세요.");
			$("#hospital-name").focus();
			return;
		}
		loadHospital();
	});
	
	$("#academy-search").click(function(){
		var keyword = $("#academy-name").val();
		if (!keyword) {
			alert("업체명을 입력후 검색해 주세요.");
			$("#academy-name").focus();
			return;
		}
		loadAcademy();
	});
	
	$('#popup').on('ifChecked', function(){
		$('.popup').removeClass('hidden');
	});
	$('#popup').on('ifUnchecked', function(){
		$('.popup').addClass('hidden');
	});
});

function loadHospital() {
	var keyword = $("#hospital-name").val();
	if ('${siteInfo.sitecd}' == 'PPPP' && '${code}' == 'partner' && var1) {
		keyword = var1;
	}
	$.ajax({
		type : "GET"
			, url : "/api/db/search/hospital"
			, cache : false
			, data : {keyword:keyword}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					if (data.data.length == 0) {
						alert("검색된 병원이 없습니다.");
						return;
					}
					$("#var1").empty();
					$("#var1").append('<option value="">:::: 병원 선택 ::::</option>');
					$("#var1").append('<option value="">===========================</option>');
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						var html = '<option value="'+d.ykiho+'">'+d.yadmNm+' ('+d.telno+') - '+d.addr+'</option>';
						$("#var1").append(html);
					}
					$("#var1-wrapper").removeClass('hidden');
					if ('${siteInfo.sitecd}' == 'PPPP' && '${code}' == 'partner' && var1) {
						$('#var1').val(var1);
						var1 = '';
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function loadAcademy() {
	var keyword = $("#academy-name").val();
	if ('${siteInfo.sitecd}' == 'BEAUTYRUN' && '${code}' == 'partner' && var1) {
		keyword = var1;
	}
	$.ajax({
		type : "GET"
			, url : "/api/store/search"
			, cache : false
			, data : {keyword:keyword}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					if (data.data.length == 0) {
						alert("검색된 업체가 없습니다.");
						return;
					}
					$("#var1").empty();
					$("#var1").append('<option value="">:::: 업체 선택 ::::</option>');
					$("#var1").append('<option value="">===========================</option>');
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						var html = '<option value="'+d.idx+'">'+d.title+' ('+d.phone+') - '+d.address+'</option>';
						$("#var1").append(html);
					}
					$("#var1-wrapper").removeClass('hidden');
					if ('${siteInfo.sitecd}' == 'BEAUTYRUN' && '${code}' == 'partner' && var1) {
						$('#var1').val(var1);
						var1 = '';
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}
</script>