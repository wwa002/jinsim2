<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${boardCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>게시판</li>
			<li>${boardCfg.title}</li>
			<li class="active"><strong id="title">열람</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<div class="btn-group">
				<button type="button" id="btn-reply" class="btn btn-success"><i class="fa fa-reply"></i></button>
				<button type="button" id="btn-modify" class="btn btn-warning"><i class="fa fa-edit"></i></button>
				<button type="button" id="btn-delete" class="btn btn-danger"><i class="fa fa-trash"></i></button>
			</div>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="row hidden" id="gallery-wrapper">
				<div class="col-sm-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5>갤러리</h5>
						</div>
						<div class="ibox-content">
							<div class="lightBoxGallery" id="gallery">
								<!-- The Gallery as lightbox dialog, should be a child element of the document body -->
	                            <div id="blueimp-gallery" class="blueimp-gallery">
									<div class="slides"></div>
									<h3 class="title"></h3>
									<a class="prev">‹</a>
									<a class="next">›</a>
									<a class="close">×</a>
									<a class="play-pause"></a>
									<ol class="indicator"></ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox">
						<div class="ibox-title">
							<h5 id="subject"></h5>
							<div class="ibox-tools pull-right">
								<span class="label label-primary"><i class="fa fa-calendar"></i> <span id="date"></span></span>
								<span class="label label-info"><i class="fa fa-eye"></i> <span id="read"></span></span>
							</div>
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-sm-12" id="content"></div>
							</div>
							<div class="row">
								<div class="col-sm-12" id="comment-list"></div>
							</div>
							<hr />
							<div class="row">
								<div class="col-sm-12">
									<div class="social-feed-box">
										<form name="commentForm" id="commentForm" onsubmit="return false;">
											<div class="row">
												<div class="col-sm-12"><div id="comment"></div></div>
											</div>
											<div class="row">
												<div class="col-sm-12 text-right">
													<button type="button" id="btn-comment-write" class="btn btn-primary margin-10"><i class="fa fa-edit"></i> 댓글 작성</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>첨부파일</h5>
				</div>
				<div class="ibox-content">
					<ul id="file-list">
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="modal-reply" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated flipInY">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-reply modal-icon"></i>
				<h4 class="modal-title">댓글 답변</h4>
				<small class="font-bold">
					선택하신 댓글에 답변 댓글을 추가합니다.
				</small>
			</div>
			<div class="modal-body">
				<input type="hidden" id="comment-reply-idx" value="" />
				<div id="comment-reply"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">닫기</button>
				<button type="button" class="btn btn-primary" id="btn-comment-reply"><i class="fa fa-reply"></i>댓글 작성</button>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="modal-modify" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content animated flipInY">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-edit modal-icon"></i>
				<h4 class="modal-title">댓글 수정</h4>
				<small class="font-bold">
					등록된 댓글을 수정합니다.
				</small>
			</div>
			<div class="modal-body">
				<input type="hidden" id="comment-modify-idx" value="" />
				<div id="comment-modify"></div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">닫기</button>
				<button type="button" class="btn btn-primary" id="btn-comment-modify"><i class="fa fa-edit"></i> 댓글 수정</button>
			</div>
		</div>
	</div>
</div>


<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script id="file-item" type="x-tmpl-mustache">
<li>
	<a href="/api/board/${code}/download/{{idx}}/{{filename}}" target="_blank"><i class="fa fa-download"></i> {{filename}}</a>
</li>
</script>

<script id="gallery-item" type="x-tmpl-mustache">
<a href="{{url}}" title="{{filename}}" data-gallery=""><img src="{{url}}" alt="{{filename}}"/></a> 
</script>

<script id="comment-item" type="x-tmpl-mustache">
<div class="social-feed-box" style="{{thread}}">
	<div class="social-avatar">
		<a href="javascript:void(0);" class="pull-left">
			{{#thread}}
			<i class="fa fa-reply"></i>
			{{/thread}}
			{{#photo}}
				<img alt="image" src="{{photo}}">
			{{/photo}}
			{{^photo}}
				<i class="fa fa-user"></i>
			{{/photo}}
		</a>
		<div class="pull-right">
			<div class="btn-group">
				<button type="button" class="btn btn-xs btn-success btn-comment-reply" data-idx="{{idx}}"><i class="fa fa-reply"></i></button>
				<button type="button" class="btn btn-xs btn-warning btn-comment-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
				<button type="button" class="btn btn-xs btn-danger btn-comment-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
			</div>
		</div>
		<div class="media-body">
			<a href="#">{{name}}</a>
			<small class="text-muted">{{date}}</small>
		</div>
	</div>
	<div class="social-body" id="comment-content-{{idx}}">{{{content}}}</div>
</div>
</script>

<script type="text/javascript">

function load() {
	$.ajax({type:"GET", url : "/api/board/${code}/${idx}", cache:false
		, data : {}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var article = data.data.article;
				var files = data.data.files;
				
				$("#date").text(formatDate(getDateFromFormat(article.regdate)));
				$("#read").text(article.cnt_read);
				$("#title").text(article.subject);
				$("#subject").text(article.subject);
				$("#content").html(article.html == "Y" ? article.content : nl2br(article.content));
				
				var $fileList = $("#file-list");
				var $gallery = $("#gallery");
				var fileTemplate = $("#file-item").html();
				var galleryTemplate = $("#gallery-item").html();
				Mustache.parse(fileTemplate);
				Mustache.parse(galleryTemplate);
				
				var gall_count = 0;
				for (var i = 0; i < files.length; i++) {
					$fileList.append(Mustache.render(fileTemplate, {
						idx:files[i].idx, filename:files[i].name
					}));
					if (files[i].mime.startsWith('image')) {
						$gallery.prepend(Mustache.render(galleryTemplate, {
							url:'/api/board/${code}/download/'+files[i].idx+'/'+files[i].name, filename:files[i].name
						}));
						gall_count++;
					}
				}
				if (gall_count > 0) {
					$("#gallery-wrapper").removeClass("hidden");
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

function loadComments() {
	$.ajax({type:"GET", url : "/api/board/${code}/${idx}/comment", cache:false
		, data : {}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $commentList = $("#comment-list");
				var template = $("#comment-item").html();
				Mustache.parse(template);
				
				var comments = data.data.comments;
				$commentList.empty();
				for (var i = 0; i < comments.length; i++) {
					var c = comments[i];
					var size = c.thread.length;
					var marginLeft = size > 1 ? marginLeft = "margin-left:"+((size-1) * 15)+"px;" : "";
					$commentList.append(Mustache.render(template, {
						idx:c.idx, name:c.name, photo:(c.id == '' ? '' : '/res/profile/'+c.id)
						, date:formatDate(getDateFromFormat(c.regdate))
						, thread:marginLeft
						, content:(c.html == "Y" ?c.content : nl2br(c.content))
					}));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	load();
	loadComments();
	
	$("#comment").summernote({height:150, minHeight:150, maxHeight:300, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	$("#comment-modify").summernote({height:150, minHeight:150, maxHeight:300, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	$("#comment-reply").summernote({height:150, minHeight:150, maxHeight:300, focus:true, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	
	$("#btn-reply").click(function(){
		document.location.href='/admin/board/${code}/${idx}/reply';
	});
	$("#btn-modify").click(function(){
		document.location.href='/admin/board/${code}/${idx}/modify';
	});
	$("#btn-delete").click(function(){
		if (confirm('게시물을 삭제하시겠습니까?\n삭제한 게시물은 되돌릴 수 없습니다.')) {
			$.ajax({type:"GET", url : "/api/board/${code}/${idx}/delete", cache:false
				, data : {}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						alert("삭제되었습니다.");
						document.location.href='/admin/board/${code}';
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$("#btn-comment-write").click(function(){
		var content = $('#comment').summernote('code');
		$.ajax({type:"POST", url : "/api/board/${code}/${idx}/comment", cache:false
			, data : {mode:'write', content:content, html:'Y'}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$('#comment').summernote('code', '');
					loadComments();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-comment-modify").click(function(){
		var content = $('#comment-modify').summernote('code');
		var idx = $('#comment-modify-idx').val();
		$.ajax({type:"POST", url : "/api/board/${code}/${idx}/comment", cache:false
			, data : {mode:'modify', content:content, html:'Y', idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$('#comment-modify').summernote('code', '');
					$('#comment-modify-idx').val('');
					$("#modal-modify").modal('hide');
					loadComments();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-comment-reply").click(function(){
		var content = $('#comment-reply').summernote('code');
		var idx = $('#comment-reply-idx').val();
		$.ajax({type:"POST", url : "/api/board/${code}/${idx}/comment", cache:false
			, data : {mode:'reply', content:content, html:'Y', idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$('#comment-reply').summernote('code', '');
					$('#comment-reply-idx').val('');
					$("#modal-reply").modal('hide');
					loadComments();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-comment-delete', function(){
		if (confirm('댓글을 삭제하시겠습니까?\n삭제한 댓글은 되돌릴 수 없습니다.')) {
			var idx = $(this).data('idx');
			$.ajax({type:"GET", url : "/api/board/${code}/${idx}/comment", cache:false
				, data : {mode:'delete', idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						loadComments();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-comment-modify', function(){
		var idx = $(this).data('idx');
		$('#comment-modify').summernote('code', $("#comment-content-"+idx).html());
		$('#comment-modify-idx').val(idx);
		$("#modal-modify").modal('show');
	});
	
	$(document).on('click', '.btn-comment-reply', function(){
		var idx = $(this).data('idx');
		$('#comment-modify').summernote('code', '');
		$('#comment-reply-idx').val(idx);
		$("#modal-reply").modal('show');
	});
	
	
});
</script>