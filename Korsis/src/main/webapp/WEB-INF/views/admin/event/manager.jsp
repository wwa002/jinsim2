<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>관리</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>이벤트</li>
			<li class="active"><strong>관리</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>이벤트 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" id="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center col-md-2">대표이미지</th>
									<th class="text-center">제목</th>
									<th class="text-center">비용</th>
									<th class="text-center col-md-2">상태</th>
									<th class="text-center col-md-2">삭제</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>병원 이벤트 생성</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">카테고리</label>
							<div class="col-lg-9">
								<select name="cat1" id="form-category1" class="form-control"></select>
							</div>
						</div>
						<div class="form-group hidden" id="form-category-sub">
							<label class="col-lg-3 control-label">세부 카테고리</label>
							<div class="col-lg-9">
								<select name="cat2" id="form-category2" class="form-control"></select>
							</div>
						</div>
						<c:if test="${siteInfo.medicaldb eq 'Y'}">
							<hr />
							<div class="form-group">
								<label class="col-lg-3 control-label">병원검색</label>
								<div class="col-lg-9">
									<div class="input-group">
										<input type="text" id="hospital-name" value="" class="form-control" />
										<span class="input-group-btn">
											<button type="button" id="hospital-search" class="btn btn-default"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group hidden" id="form-hospital">
								<label class="col-lg-3 control-label">병원 선택</label>
								<div class="col-lg-9">
									<select name="ykiho" id="ykiho" class="form-control"></select>
								</div>
							</div>
						</c:if>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">상점</label>
							<div class="col-lg-9">
								<select name="sidx" id="sidx" class="form-control"></select>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-create" type="submit">생성</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">
		{{#image}}
			<img src="/api/event/icon/{{idx}}/{{image}}?height=50" alt="" class="img-thumbnail" />
		{{/image}}
		{{^image}}
			-
		{{/image}}
	</td>
	<td>
		<a href="/admin/event/manager/{{idx}}">{{subject}}</a>
	</td>
	<td class="text-center">{{cost}}원</td>
	<td class="text-center">
		{{#status}}
		<select class="form-control status" data-idx="{{idx}}">
			<option value="Y" selected="selected">노출</option>
			<option value="N">비노출</option>
		</select>
		{{/status}}
		{{^status}}
		<select class="form-control status" data-idx="{{idx}}">
			<option value="Y">노출</option>
			<option value="N" selected="selected">비노출</option>
		</select>
		{{/status}}
	</td>
	<td class="text-center">
		<button class="btn btn-danger btn-delete" type="button" data-idx="{{idx}}">삭제</button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	var keyword = $("#keyword").val();
	$.ajax({type:"GET", url:"/api/admin/event", cache:false
		, data : {page:page, keyword:keyword}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, idx:l.idx, subject:l.subject, image:l.image
						, cost:numberWithCommas(l.cost)
						, status:l.status=='Y'
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadCategory(category) {
	$.ajax({
		type : "GET"
			, url : "/api/event/category"
			, cache : false
			, data : {category:category}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					if (!category) {
						$("#form-category1").empty();
						$("#form-category2").empty();
						$("#form-category2").val("");
						
						$("#form-category1").append('<option value="">카테고리를 선택해 주세요.</option>');
						$("#form-category1").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							$("#form-category1").append('<option value="'+d.idx+'">'+d.value+'</option>');
						}
					} else {
						$("#form-category2").empty();
						$("#form-category2").val("");
						
						if (data.data.length > 0) {
							$("#form-category2").append('<option value="">세부 카테고리를 선택해 주세요.</option>');
							$("#form-category2").append('<option value="">===========================</option>');
							for (var i = 0; i < data.data.length; i++) {
								var d = data.data[i];
								$("#form-category2").append('<option value="'+d.idx+'">'+d.value+'</option>');
							}
							$("#form-category-sub").removeClass('hidden');
						} else {
							$("#form-category-sub").addClass('hidden');
						}
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function loadStore() {
	$.ajax({
		type : "GET"
			, url : "/api/store"
			, cache : false
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					$("#sidx").append('<option value="">상점을 선택해 주세요.</option>');
					$("#sidx").append('<option value="">===========================</option>');
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						$("#sidx").append('<option value="'+d.idx+'">'+d.title+'</option>');
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function loadHospital() {
	var keyword = $("#hospital-name").val();
	$.ajax({
		type : "GET"
			, url : "/api/db/search/hospital"
			, cache : false
			, data : {keyword:keyword}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					if (data.data.length == 0) {
						alert("검색된 병원이 없습니다.");
						return;
					}
					$("#ykiho").empty();
					$("#ykiho").append('<option value="">병원 선택</option>');
					$("#ykiho").append('<option value="">===========================</option>');
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						var html = '<option value="'+d.ykiho+'">'+d.yadmNm+' ('+d.telno+') - '+d.addr+'</option>';
						$("#ykiho").append(html);
					}
					$("#form-hospital").removeClass('hidden');
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

$(function(){
	loadList(1);
	$("#btn-search").click(function(){
		loadList(1);
	});
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	
	loadCategory();
	$("#form-category1").change(function(){
		loadCategory($(this).val());
	});
	loadStore();
	
	$("#hospital-search").click(function(){
		var keyword = $("#hospital-name").val();
		if (!keyword) {
			alert("병원명을 입력후 검색해 주세요.");
			$("#hospital-name").focus();
			return;
		}
		
		loadHospital();
	});
	
	$("#btn-create").click(function(){
		$.ajax({type:"POST", url : "/api/admin/event/create", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					document.location.href='/admin/event/manager/'+data.data;
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('change', '.status', function(){
		var idx = $(this).data('idx');
		var status = $(this).val();
		$.ajax({type:"POST", url : "/api/admin/event/status", cache:false
			, data : {idx:idx, status:status}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url:"/api/admin/event/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>