<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2><strong>${item.subject}</strong></h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>이벤트</li>
			<li>프로젝트</li>
			<li class="active"><strong>${item.subject}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>${item.subject}</h5>
				</div>
				<c:if test="${item.image_path ne null and item.image_path ne ''}">
					<div class="ibox-content">
						<img src="/api/event/plan/${item.idx}/${item.image}" alt="" class="img-responsive" />
					</div>
				</c:if>
			</div>
			
			<div id="list"></div>
			
			<div class="ibox">
				<div class="ibox-content">
					${item.content}
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>이벤트 추가</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="pidx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">이벤트 검색</label>
							<div class="col-lg-9">
								<div class="input-group">
									<input type="text" id="event-name" value="" class="form-control" />
									<span class="input-group-btn">
										<button type="button" id="event-search" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group hidden" id="form-event">
							<label class="col-lg-3 control-label">이벤트 선택</label>
							<div class="col-lg-9">
								<select name="eidx" id="eidx" class="form-control"></select>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-add" type="submit">추가</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>프로젝트 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="modifyForm" id="modifyForm" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return false;">
						<input type="hidden" name="idx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" class="form-control" value="${fn:escapeXml(item.subject)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<c:choose>
									<c:when test="${item.image_path ne null and item.image_path ne ''}">
										<div class="row">
											<div class="col-sm-12">
												<img src="/api/event/plan/${item.idx}/${item.image}" alt="" class="img-responsive" />
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="col-sm-12">
												<input type="file" name="image" class="form-control" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="file" name="image" class="form-control" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<div class="col-lg-12">
								<textarea id="content" name="content" class="form-control hidden">${fn:escapeXml(item.content)}</textarea>
								<div id="summernote"></div>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-modify" type="submit">수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script id="list-item" type="x-tmpl-mustache">
<div class="ibox">
	<div class="ibox-title">
		<h5>{{no}} - {{subject}} <small>{{yadmNm}}</small></h5>
	</div>
	<div class="ibox-content">
		<div><img src="/api/event/icon/{{eidx}}/{{image}}" alt="" class="img-responsive" /></div>
		<hr />
		<div class="text-right">
			<button class="btn btn-default btn-show" type="button" data-eidx="{{eidx}}">상세보기</button>
			<button class="btn btn-danger btn-delete" type="button" data-pidx="{{pidx}}" data-idx="{{idx}}">삭제</button>
		</div>
	</div>
</div>
</script>

<script type="text/javascript">
function loadPages() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/event/plan/page", cache:false
		, data : {pidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				var list = data.data;
				
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);
				
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, idx:l.idx, pidx:l.pidx, eidx:l.eidx, subject:l.subject, image:l.image, yadmNm:l.yadmNm
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	$("#summernote").summernote({height:200, minHeight:200, maxHeight:600, focus:false, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	$('#summernote').summernote('code', $("#content").val());
	
	loadPages();
	
	$("#event-search").click(function(){
		var keyword = $("#event-name").val();
		if (!keyword) {
			alert("이벤트명을 입력후 검색해 주세요.");
			$("#event-name").focus();
			return;
		}
		
		$.ajax({
			type : "GET"
				, url : "/api/admin/event/search"
				, cache : false
				, data : {keyword:keyword}
				, success : function(data){
					console.log(data);
					if (data.error == 0) {
						if (data.data.length == 0) {
							alert("이벤트가 없습니다.");
							return;
						}
						$("#eidx").empty();
						$("#eidx").append('<option value="">이벤트 선택</option>');
						$("#eidx").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							var html = '<option value="'+d.idx+'">'+d.subject;
							if (d.yadaNm != '') {
								html += ' ('+d.yadmNm+')';
							}
							html += '</option>';
							$("#eidx").append(html);
						}
						$("#form-event").removeClass('hidden');
					}
				}
				, error : function(data){
					alert(data.responseJSON.error);
				}
		});
	});
	
	$("#btn-add").click(function(){
		$.ajax({type:"POST", url : "/api/admin/event/plan/page/add", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadPages();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-modify").click(function(){
		$("#content").val($('#summernote').summernote('code'));
		$("#modifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/event/plan/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#modifyForm").submit();
	});
	
	$(document).on('click', '.btn-show', function(){
		document.location.href='/admin/event/manager/'+$(this).data('eidx');
	});
	
	$(document).on('click', '.btn-delete', function(){
		var pidx = $(this).data('pidx');
		var idx = $(this).data('idx');
		$.ajax({type:"POST", url : "/api/admin/event/plan/page/delete", cache:false
			, data : {pidx:pidx, idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadPages();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
});
</script>