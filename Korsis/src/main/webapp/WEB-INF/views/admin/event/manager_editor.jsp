<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2><strong>${item.subject}</strong></h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>이벤트</li>
			<li>관리</li>
			<li class="active"><strong>${item.subject}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>${item.subject}</h5>
				</div>
			</div>
			
			<div id="list"></div>
			
			<div class="ibox">
				<div class="ibox-content">
					${item.content}
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>페이지 생성</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="eidx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<input type="file" name="file" id="file" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-page-create" type="submit">생성</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>병원 이벤트 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="modifyForm" id="modifyForm" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return false;">
						<input type="hidden" name="idx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">제목</label>
							<div class="col-lg-9">
								<input type="text" name="subject" class="form-control" value="${fn:escapeXml(item.subject)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">비용</label>
							<div class="col-lg-9">
								<input type="number" name="cost" value="${fn:escapeXml(item.cost)}" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">카테고리</label>
							<div class="col-lg-9">
								<select name="cat1" id="form-category1" class="form-control"></select>
							</div>
						</div>
						<div class="form-group hidden" id="form-category-sub">
							<label class="col-lg-3 control-label">세부 카테고리</label>
							<div class="col-lg-9">
								<select name="cat2" id="form-category2" class="form-control"></select>
							</div>
						</div>
						<c:if test="${siteInfo.medicaldb eq 'Y'}">
							<hr />
							<div class="form-group">
								<label class="col-lg-3 control-label">병원검색</label>
								<div class="col-lg-9">
									<div class="input-group">
										<input type="text" id="hospital-name" value="" class="form-control" />
										<span class="input-group-btn">
											<button type="button" id="hospital-search" class="btn btn-default"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group hidden" id="form-hospital">
								<label class="col-lg-3 control-label">병원 선택</label>
								<div class="col-lg-9">
									<select name="ykiho" id="ykiho" class="form-control"></select>
								</div>
							</div>
						</c:if>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">상점</label>
							<div class="col-lg-9">
								<select name="sidx" id="sidx" class="form-control"></select>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">이미지</label>
							<div class="col-lg-9">
								<c:choose>
									<c:when test="${item.image_path ne null and item.image_path ne ''}">
										<div class="row">
											<div class="col-sm-12">
												<img src="/api/event/icon/${item.idx}/${item.image}" alt="" class="img-responsive" />
											</div>
										</div>
										<hr />
										<div class="row">
											<div class="col-sm-12">
												<input type="file" name="image" class="form-control" />
											</div>
										</div>
									</c:when>
									<c:otherwise>
										<input type="file" name="image" class="form-control" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<div class="col-lg-12">
								<textarea id="content" name="content" class="form-control hidden">${fn:escapeXml(item.content)}</textarea>
								<div id="summernote"></div>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">시작</label>
							<div class="col-lg-9">
								<div class="row">
									<c:choose>
										<c:when test="${item.startdate ne null and item.startdate ne '' }">
											<fmt:parseDate value="${item.startdate}" var="date" pattern="yyyyMMddHHmmss"/>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													<input type="text" name="startdt" id="startdt" value="<fmt:formatDate value="${date}" pattern="yyyy-MM-dd"/>" class="form-control datepicker" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
													<input type="text" name="starttm" id="starttm" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
												</div>
											</div>
										</c:when>
										<c:otherwise>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													<input type="text" name="startdt" id="startdt" value="" class="form-control datepicker" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
													<input type="text" name="starttm" id="starttm" value="" class="form-control clockpicker" />
												</div>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">종료</label>
							<div class="col-lg-9">
								<div class="row">
									<c:choose>
										<c:when test="${item.enddate ne null and item.enddate ne '' }">
											<fmt:parseDate value="${item.enddate}" var="date" pattern="yyyyMMddHHmmss"/>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													<input type="text" name="enddt" id="enddt" value="<fmt:formatDate value="${date}" pattern="yyyy-MM-dd"/>" class="form-control datepicker" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
													<input type="text" name="endtm" id="endtm" value="<fmt:formatDate value="${date}" pattern="HH:mm"/>" class="form-control clockpicker" />
												</div>
											</div>
										</c:when>
										<c:otherwise>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
													<input type="text" name="enddt" id="enddt" value="" class="form-control datepicker" />
												</div>
											</div>
											<div class="col-md-6">
												<div class="input-group">
													<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
													<input type="text" name="endtm" id="endtm" value="" class="form-control clockpicker" />
												</div>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-modify" type="submit">수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script id="list-item" type="x-tmpl-mustache">
<div class="ibox">
	<div class="ibox-title">
		<h5>{{no}} 페이지</h5>
	</div>
	<div class="ibox-content">
		<div><img src="/api/event/page/{{eidx}}/{{idx}}/{{image}}?width=500" alt="" class="img-responsive" /></div>
		<hr />
		<div>
			<form role="form" id="modifyForm-{{eidx}}-{{idx}}" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
				<input type="hidden" name="eidx" value="{{eidx}}" />
				<input type="hidden" name="idx" value="{{idx}}" />
				<div class="form-group">
					<label class="col-lg-3 control-label">이미지</label>
					<div class="col-lg-9">
						<input type="file" name="file" class="form-control" />
					</div>
				</div>
				<hr />
				<div class="text-right">
					<button class="btn btn-danger btn-page-delete" type="button" data-eidx="{{eidx}}" data-idx="{{idx}}">삭제</button>
					<button class="btn btn-warning btn-page-modify" data-eidx="{{eidx}}" data-idx="{{idx}}" type="submit">수정</button>
				</div>
			</form>
		</div>
	</div>
</div>
</script>

<script type="text/javascript">
var cat1 = '${item.cat1}';
var cat2 = '${item.cat2}';
var sidx = '${item.sidx}';
var ykiho = '${item.ykiho}';

function loadPages() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/event/page", cache:false
		, data : {eidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				var list = data.data;
				
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);
				
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, eidx:l.eidx, idx:l.idx, image:l.image
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadCategory(category) {
	$.ajax({
		type : "GET"
			, url : "/api/event/category"
			, cache : false
			, data : {category:category}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					if (!category) {
						$("#form-category1").empty();
						$("#form-category2").empty();
						$("#form-category2").val("");
						
						$("#form-category1").append('<option value="">카테고리를 선택해 주세요.</option>');
						$("#form-category1").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							$("#form-category1").append('<option value="'+d.idx+'">'+d.value+'</option>');
						}
						if (cat1 != '') {
							$("#form-category1").val(cat1);
							cat1 = '';
						}
					} else {
						$("#form-category2").empty();
						$("#form-category2").val("");
						
						if (data.data.length > 0) {
							$("#form-category2").append('<option value="">세부 카테고리를 선택해 주세요.</option>');
							$("#form-category2").append('<option value="">===========================</option>');
							for (var i = 0; i < data.data.length; i++) {
								var d = data.data[i];
								$("#form-category2").append('<option value="'+d.idx+'">'+d.value+'</option>');
							}
							$("#form-category-sub").removeClass('hidden');
						} else {
							$("#form-category-sub").addClass('hidden');
						}
						if (cat2 != '') {
							$("#form-category2").val(cat2);
							cat2 = '';
						}
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function loadStore() {
	$.ajax({
		type : "GET"
			, url : "/api/store"
			, cache : false
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					$("#sidx").append('<option value="">상점을 선택해 주세요.</option>');
					$("#sidx").append('<option value="">===========================</option>');
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						$("#sidx").append('<option value="'+d.idx+'">'+d.title+'</option>');
					}
					if (sidx != '') {
						$("#sidx").val(sidx);
						sidx = '';
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

function loadHospital() {
	var keyword = $("#hospital-name").val();
	if (ykiho) {
		keyword = ykiho;
	}
	$.ajax({
		type : "GET"
			, url : "/api/db/search/hospital"
			, cache : false
			, data : {keyword:keyword}
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					if (data.data.length == 0) {
						alert("검색된 병원이 없습니다.");
						return;
					}
					$("#ykiho").empty();
					$("#ykiho").append('<option value="">병원 선택</option>');
					$("#ykiho").append('<option value="">===========================</option>');
					for (var i = 0; i < data.data.length; i++) {
						var d = data.data[i];
						var html = '<option value="'+d.ykiho+'">'+d.yadmNm+' ('+d.telno+') - '+d.addr+'</option>';
						$("#ykiho").append(html);
					}
					$("#form-hospital").removeClass('hidden');
					
					if (ykiho != '') {
						$("#ykiho").val(ykiho);
						ykiho = '';
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
}

$(function(){
	$("#summernote").summernote({height:200, minHeight:200, maxHeight:600, focus:false, disableDragAndDrop: true, callbacks: {
	    onImageUpload: function(files) {
	    	for (var i = files.length - 1; i >= 0; i--) {
	    		sendSummernoteFile(files[i], this);
	    	}
	      }
	    }});
	$('#summernote').summernote('code', $("#content").val());
	
	loadPages();
	loadCategory();
	if (cat1 != '') {
		loadCategory(cat1);
	}
	$("#form-category1").change(function(){
		cat2 = '';
		loadCategory($(this).val());
	});
	loadStore();
	
	if (ykiho != '') {
		loadHospital();
	}
	
	$("#hospital-search").click(function(){
		var keyword = $("#hospital-name").val();
		if (!keyword) {
			alert("병원명을 입력후 검색해 주세요.");
			$("#hospital-name").focus();
			return;
		}
		
		loadHospital();
	});
	
	$("#btn-modify").click(function(){
		$("#content").val($('#summernote').summernote('code'));
		$("#modifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/event/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#modifyForm").submit();
	});
	
	$("#btn-page-create").click(function(){
		$("#submitForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/event/page/create"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					loadPages();
					$("#submitForm")[0].reset();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#submitForm").submit();
	});
	
	$(document).on('click', '.btn-page-modify', function(){
		var eidx = $(this).data('eidx');
		var idx = $(this).data('idx');
		var form = '#modifyForm-'+eidx+'-'+idx;
		$(form).ajaxForm({
			type:"POST"
			, url:"/api/admin/event/page/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					loadPages();
					$(form)[0].reset();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$(form).submit();
	});
	
	$(document).on('click', '.btn-page-delete', function(){
		var eidx = $(this).data('eidx');
		var idx = $(this).data('idx');
		$.ajax({type:"GET", url : "/api/admin/event/page/delete", cache:false
			, data : {eidx:eidx, idx:idx}
			, success : function(data) {
				if (data.error == 0) {
					loadPages();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>