<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>배너</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>이벤트</li>
			<li class="active"><strong>배너</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>배너 목록</h5>
				</div>
			</div>
			
			<div id="list"></div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>배너 등록</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-lg-3 control-label">타입</label>
							<div class="col-lg-9">
								<label class="radio-inline">
									<input type="radio" name="type" id="type_1" class="type" value="E" checked="checked">
									이벤트
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" id="type_2" class="type" value="P">
									프로젝트
								</label>
							</div>
						</div>
						<div id="type-event-wrapper">
							<hr />
							<div class="form-group">
								<label class="col-lg-3 control-label">이벤트 검색</label>
								<div class="col-lg-9">
									<div class="input-group">
										<input type="text" id="event-name" value="" class="form-control" />
										<span class="input-group-btn">
											<button type="button" id="event-search" class="btn btn-default"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group" id="form-event">
								<label class="col-lg-3 control-label">이벤트 선택</label>
								<div class="col-lg-9">
									<select name="eidx" id="eidx" class="form-control"></select>
								</div>
							</div>
						</div>
						<div id="type-plan-wrapper" class="hidden">
							<hr />
							<div class="form-group">
								<label class="col-lg-3 control-label">프로젝트 검색</label>
								<div class="col-lg-9">
									<div class="input-group">
										<input type="text" id="plan-name" value="" class="form-control" />
										<span class="input-group-btn">
											<button type="button" id="plan-search" class="btn btn-default"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group" id="form-plan">
								<label class="col-lg-3 control-label">프로젝트 선택</label>
								<div class="col-lg-9">
									<select name="pidx" id="pidx" class="form-control"></select>
								</div>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-add" type="submit">추가</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<div class="ibox">
	<div class="ibox-title"><h5>{{no}} - {{subject}}</h5></div>
	<div class="ibox-content">
		{{#isEvent}}
		<div><img src="/api/event/icon/{{eidx}}/{{image}}?width=500" alt="" class="img-thumbnail" /></div>
		{{/isEvent}}
		{{#isPlan}}
		<div><img src="/api/event/plan/{{pidx}}/{{image}}?width=500" alt="" class="img-thumbnail" /></div>
		{{/isPlan}}
		<hr />
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				{{#status}}
				<select class="form-control status" data-idx="{{idx}}">
					<option value="Y" selected="selected">노출</option>
					<option value="N">비노출</option>
				</select>
				{{/status}}
				{{^status}}
				<select class="form-control status" data-idx="{{idx}}">
					<option value="Y">노출</option>
					<option value="N" selected="selected">비노출</option>
				</select>
				{{/status}}
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 text-right">
				<button class="btn btn-default btn-show" type="button" data-type="{{type}}" data-eidx="{{eidx}}" data-pidx="{{pidx}}">상세보기</button>
				<button class="btn btn-danger btn-delete" type="button" data-type="{{type}}" data-eidx="{{eidx}}" data-pidx="{{pidx}}" data-idx="{{idx}}">삭제</button>
			</div>
		</div>
	</div>
</div>
</script>

<script type="text/javascript">
function loadList() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/event/banner", cache:false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				
				var list = data.data;
				
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);
				
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, type:l.type, idx:l.idx, eidx:l.eidx, pidx:l.pidx, subject:l.subject, image:l.image
						, status:l.status=='Y', isEvent:l.type=='E', isPlan:l.type=='P'
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}
$(function(){
	loadList();
	
	$(".type").click(function(){
		if ($("#type_1").prop("checked")) {
			$("#type-event-wrapper").removeClass('hidden');
			$("#type-plan-wrapper").addClass('hidden');
		} else if ($("#type_2").prop("checked")) {
			$("#type-event-wrapper").addClass('hidden');
			$("#type-plan-wrapper").removeClass('hidden');
		} else {
			$("#type_1").prop("checked", true);
			$("#type-event-wrapper").removeClass('hidden');
			$("#type-plan-wrapper").addClass('hidden');
		}
	});
	
	$("#event-search").click(function(){
		var keyword = $("#event-name").val();
		if (!keyword) {
			alert("이벤트명을 입력후 검색해 주세요.");
			$("#event-name").focus();
			return;
		}
		
		$.ajax({
			type : "GET"
				, url : "/api/admin/event/search"
				, cache : false
				, data : {keyword:keyword}
				, success : function(data){
					console.log(data);
					if (data.error == 0) {
						if (data.data.length == 0) {
							alert("이벤트가 없습니다.");
							return;
						}
						$("#eidx").empty();
						$("#eidx").append('<option value="">이벤트 선택</option>');
						$("#eidx").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							var html = '<option value="'+d.idx+'">'+d.subject;
							if (d.yadaNm != '') {
								html += ' ('+d.yadmNm+')';
							}
							html += '</option>';
							$("#eidx").append(html);
						}
						$("#form-event").removeClass('hidden');
					}
				}
				, error : function(data){
					alert(data.responseJSON.error);
				}
		});
	});
	
	$("#plan-search").click(function(){
		var keyword = $("#plan-name").val();
		if (!keyword) {
			alert("프로젝트명을 입력후 검색해 주세요.");
			$("#plan-name").focus();
			return;
		}
		
		$.ajax({
			type : "GET"
				, url : "/api/admin/event/plan/search"
				, cache : false
				, data : {keyword:keyword}
				, success : function(data){
					console.log(data);
					if (data.error == 0) {
						if (data.data.length == 0) {
							alert("검색된 프로젝트가 없습니다.");
							return;
						}
						$("#pidx").empty();
						$("#pidx").append('<option value="">프로젝트 선택</option>');
						$("#pidx").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							var html = '<option value="'+d.idx+'">'+d.subject+'</option>';
							$("#pidx").append(html);
						}
						$("#form-plan").show();
					}
				}
				, error : function(data){
					alert(data.responseJSON.error);
				}
		});
	});
	
	$("#btn-add").click(function(){
		$.ajax({type:"POST", url : "/api/admin/event/banner/add", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('change', '.status', function(){
		var idx = $(this).data('idx');
		var status = $(this).val();
		$.ajax({type:"POST", url : "/api/admin/event/banner/status", cache:false
			, data : {idx:idx, status:status}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url:"/api/admin/event/banner/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-show', function(){
		var type = $(this).data('type');
		switch (type) {
		case 'E':
			document.location.href='/admin/event/manager/'+$(this).data('eidx');
			break;
		case 'P':
			document.location.href='/admin/event/project/'+$(this).data('pidx');
			break;
		}
	});
	
});
</script>