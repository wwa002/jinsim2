<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>주간 인기 이벤트</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>이벤트</li>
			<li class="active"><strong>주간 인기 이벤트</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>주간 인기 이벤트 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center col-md-2">대표이미지</th>
									<th class="text-center">제목</th>
									<th class="text-center">비용</th>
									<th class="text-center col-md-2">상태</th>
									<th class="text-center col-md-2">삭제</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>주간 인기 이벤트</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="submitForm" id="submitForm" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-lg-3 control-label">이벤트 검색</label>
							<div class="col-lg-9">
								<div class="input-group">
									<input type="text" id="event-name" value="" class="form-control" />
									<span class="input-group-btn">
										<button type="button" id="event-search" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group hidden" id="form-event">
							<label class="col-lg-3 control-label">이벤트 선택</label>
							<div class="col-lg-9">
								<select name="eidx" id="eidx" class="form-control"></select>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-add" type="submit">추가</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">
		{{#image}}
			<img src="/api/event/icon/{{eidx}}/{{image}}?height=50" alt="" class="img-thumbnail" />
		{{/image}}
		{{^image}}
			-
		{{/image}}
	</td>
	<td><a href="/admin/event/manager/{{eidx}}">{{subject}}</a></td>
	<td class="text-center">{{cost}}원</td>
	<td class="text-center">
		{{#status}}
		<select class="form-control status" data-idx="{{idx}}">
			<option value="Y" selected="selected">노출</option>
			<option value="N">비노출</option>
		</select>
		{{/status}}
		{{^status}}
		<select class="form-control status" data-idx="{{idx}}">
			<option value="Y">노출</option>
			<option value="N" selected="selected">비노출</option>
		</select>
		{{/status}}
	</td>
	<td class="text-center">
		<button class="btn btn-danger btn-delete" type="button" data-idx="{{idx}}">삭제</button>
	</td>
</tr>
</script>
<script type="text/javascript">
function loadList() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/event/week", cache:false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$list.empty();
				
				var list = data.data;
				
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);
				
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:i+1, eidx:l.eidx, idx:l.idx, subject:l.subject, image:l.image
						, cost:numberWithCommas(l.cost)
						, status:l.status=='Y'
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadList();

	$("#event-search").click(function(){
		var keyword = $("#event-name").val();
		if (!keyword) {
			alert("이벤트명을 입력후 검색해 주세요.");
			$("#event-name").focus();
			return;
		}
		
		$.ajax({
			type : "GET"
				, url : "/api/admin/event/search"
				, cache : false
				, data : {keyword:keyword}
				, success : function(data){
					console.log(data);
					if (data.error == 0) {
						if (data.data.length == 0) {
							alert("이벤트가 없습니다.");
							return;
						}
						$("#eidx").empty();
						$("#eidx").append('<option value="">이벤트 선택</option>');
						$("#eidx").append('<option value="">===========================</option>');
						for (var i = 0; i < data.data.length; i++) {
							var d = data.data[i];
							var html = '<option value="'+d.idx+'">'+d.subject;
							if (d.yadaNm != '') {
								html += ' ('+d.yadmNm+')';
							}
							html += '</option>';
							$("#eidx").append(html);
						}
						$("#form-event").removeClass('hidden');
					}
				}
				, error : function(data){
					alert(data.responseJSON.error);
				}
		});
	});
	
	$("#btn-add").click(function(){
		$.ajax({type:"POST", url : "/api/admin/event/week/add", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('change', '.status', function(){
		var idx = $(this).data('idx');
		var status = $(this).val();
		$.ajax({type:"POST", url : "/api/admin/event/week/status", cache:false
			, data : {idx:idx, status:status}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url:"/api/admin/event/week/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
});
</script>