<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-popup-header.jsp"%>
<div class="wrapper wrapper-content white-bg">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-md-12">
						<h3>세션등록</h3>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="col-md-12">
						<form role="form" name="submitForm" id="submitForm" method="post" class="form-horizontal" onsubmit="return false;">
							<input type="hidden" name="code" id="code" value="${code}" />
							<div class="form-group">
								<label class="col-xs-3 control-label">종류</label>
								<div class="col-xs-9">
									<label class="radio-inline">
										<input type="radio" name="type" id="type_1" class="type" value="N" checked="checked" />
										일반
									</label>
									<label class="radio-inline">
										<input type="radio" name="type" id="type_2" class="type" value="S" />
										강연
									</label>
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-xs-3 control-label">장소 (홀)</label>
								<div class="col-xs-9">
									<input type="text" name="place" id="place" class="form-control" value="" />
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-xs-3 control-label">타이틀</label>
								<div class="col-xs-9">
									<input type="text" name="title" id="title" class="form-control" value="" />
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-xs-3 control-label">좌장 1</label>
								<div class="col-xs-9">
									<input type="text" name="principal1" id="principal1" class="form-control" value="" />
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-xs-3 control-label">좌장 2</label>
								<div class="col-xs-9">
									<input type="text" name="principal2" id="principal2" class="form-control" value="" />
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-xs-3 control-label">발표자</label>
								<div class="col-xs-9">
									<input type="text" name="speaker" id="speaker" class="form-control" value="" />
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-xs-3 control-label">날짜</label>
								<div class="col-xs-9">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										<input type="text" name="date" id="date" value="" class="form-control datepicker" />
									</div>
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-xs-3 control-label">시간</label>
								<div class="col-xs-9">
									<div class="row">
										<div class="col-xs-6">
											<div class="input-group">
												<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
												<input type="text" name="stime" id="stime" value="" class="form-control clockpicker" />
											</div>
										</div>
										<div class="col-xs-6">
											<div class="input-group">
												<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
												<input type="text" name="etime" id="etime" value="" class="form-control clockpicker" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr />
							<div class="text-right">
								<button class="btn btn-primary" id="btn-save" type="submit">생성</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-popup-footer.jsp"%>

<script type="text/javascript">
$(function(){
	$("#btn-save").click(function(){
		$.ajax({
			type:"POST"
			, url : "/api/admin/conference/session/create"
			, cache:false
			, data : $('#submitForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					opener.location.reload();
					self.close();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>
