<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${conferenceCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>학술대회</li>
			<li class="active"><strong>${conferenceCfg.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="tabs-container">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#tab-basic">기본정보</a></li>
					<li><a data-toggle="tab" href="#tab-session">세션관리</a></li>
					<c:if test="${conferenceCfg.register eq 'Y'}">
						<li class=""><a data-toggle="tab" href="#tab-register">사전등록관리</a></li>
					</c:if>
					<c:if test="${conferenceCfg.abstracts eq 'Y'}">
						<li class=""><a data-toggle="tab" href="#tab-abstracts">초록등록관리</a></li>
					</c:if>
					<c:if test="${conferenceCfg.stay eq 'Y'}">
						<li class=""><a data-toggle="tab" href="#tab-stay">숙박관리</a></li>
					</c:if>
					<c:if test="${conferenceCfg.tour eq 'Y'}">
						<li class=""><a data-toggle="tab" href="#tab-tour">여행/투어관리</a></li>
					</c:if>
				</ul>
				<div class="tab-content">
					<div id="tab-basic" class="tab-pane active">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<h3>접속정보</h3>
									<div>기본 URL : http://${mainUrl}/c/${code}</div>
									<div>단축 URL : <span id="shortUrl"></span></div>
									<div>QR 코드</div>
									<div id="qrcode"></div>
								</div>
							</div>
						</div>
					</div>
					<div id="tab-session" class="tab-pane">
						<div class="panel-body">
							<div class="pull-right">
								<button class="btn btn-primary" onclick="window.open('/admin/conference/${code}/add', 'conference_add', 'width=600,height=850,status=1')">생성</button>
							</div>
							<div class="row">
								<div class="col-md-12" id="date-list"></div>
							</div>
						</div>
					</div>
					<div id="tab-register" class="tab-pane">
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-12">
									<form id="registerSearchForm" class="form-horizontal" onsubmit="return false;">
										<div class="form-group">
											<div class="col-sm-4 col-sm-offset-6">
												<div class="input-group">
													<input type="text" id="register-keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
													<span class="input-group-btn">
														<button type="button" id="btn-register-search" class="btn btn-default"><i class="fa fa-search"></i></button>
													</span>
												</div>
											</div>
											<div class="col-sm-2">
												<button type="button" id="btn-register-excel" class="btn btn-primary btn-block"><i class="fa fa-file-excel-o"></i> 엑셀다운</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-hover">
									<colgroup>
										<col class="col-sm-1" />
										<col class="col-sm-2" />
										<col class="col-sm-*" />
										<col class="col-sm-2" />
										<col class="col-sm-2" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
										<col class="col-sm-1" />
									</colgroup>
									<thead>
										<tr>
											<th class="text-center">번호</th>
											<th class="text-center">이름</th>
											<th class="text-center">근무지</th>
											<th class="text-center">연락처</th>
											<th class="text-center">이메일</th>
											<th class="text-center">교통수단</th>
											<th class="text-center">도착시간</th>
											<th class="text-center">출발시간</th>
											<th class="text-center">관리</th>
										</tr>
									</thead>
									<tbody id="register-list"></tbody>
								</table>
							</div>
							<div class="text-center">
								<ul class="pagination" id="register-pagination"></ul>
							</div>
							<iframe id="register-excel" class="hidden"></iframe>
						</div>
					</div>
					<div id="tab-abstracts" class="tab-pane">
						<div class="panel-body">
						</div>
					</div>
					<div id="tab-stay" class="tab-pane">
						<div class="panel-body">
						</div>
					</div>
					<div id="tab-tour" class="tab-pane">
						<div class="panel-body">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="register-data" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">사전등록정보</h4>
			</div>
			<div class="modal-body text-center">
				<form role="form" id="register-modify-form" method="post" class="form-horizontal" onsubmit="return false;">
					<input type="hidden" name="code" id="register-code" />
					<input type="hidden" name="idx" id="register-idx" />
					<div class="form-group">
						<label class="col-lg-3 control-label">이름</label>
						<div class="col-lg-9">
							<input type="text" name="name" id="register-name" class="form-control" placeholder="이름을 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">직함</label>
						<div class="col-lg-9">
							<input type="text" name="title" id="register-title" class="form-control" placeholder="직함을 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">근무처</label>
						<div class="col-lg-9">
							<input type="text" name="office" id="register-office" class="form-control" placeholder="근무처를 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">부서</label>
						<div class="col-lg-9">
							<input type="text" name="department" id="register-department" class="form-control" placeholder="부서를 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">연락처</label>
						<div class="col-lg-9">
							<input type="text" name="phone" id="register-phone" class="form-control" placeholder="연락처를 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">이메일</label>
						<div class="col-lg-9">
							<input type="email" name="email" id="register-email" class="form-control" placeholder="이메일을 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">영업담당자</label>
						<div class="col-lg-9">
							<input type="text" name="marketer" id="register-marketer" class="form-control" placeholder="영업당당자 이름을 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">교통수단</label>
						<div class="col-lg-9 text-left">
							<input type="radio" name="traffic" id="register-traffic-1" value="C" />
							<label for="traffic_1">자가운전</label>
							<input type="radio" name="traffic" id="register-traffic-2" value="B" />
							<label for="traffic_2">대중교통</label>
							<input type="radio" name="traffic" id="register-traffic-3" value="N" />
							<label for="traffic_3">미정</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">출발지</label>
						<div class="col-lg-9">
							<input type="text" name="traffic_start" id="register-traffic-start" class="form-control" placeholder="출발지를 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">기타 요청 및 문의</label>
						<div class="col-lg-9">
							<input type="text" name="etc" id="register-etc" class="form-control" placeholder="기타 요청 및 문의사항을 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">숙박신청</label>
						<div class="col-lg-9 text-left">
							<input type="radio" name="stay" id="register-stay-1" value="Y" />
							<label for="stay_1">숙박</label>
							<input type="radio" name="stay" id="register-stay-2" value="N" />
							<label for="stay_2">숙박하지 않음</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">도착시간</label>
						<div class="col-lg-9">
							<input type="text" name="stay_checkin" id="register-stay-checkin" class="form-control clockpicker" placeholder="예상 체크인 시간을 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">출발시간</label>
						<div class="col-lg-9">
							<input type="text" name="stay_checkout" id="register-stay-checkout" class="form-control clockpicker" placeholder="예상 체크아웃 시간을 입력해 주세요." />
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">투숙인원</label>
						<div class="col-lg-9 text-left">
							<select name="stay_adult" id="register-stay-adult">
								<option value="0">성인</option>
								<option value="1">성인 1인</option>
								<option value="2">성인 2인</option>
								<option value="3">성인 3인</option>
								<option value="4">성인 4인</option>
							</select>
							<select name="stay_child" id="register-stay-child">
								<option value="0">소아</option>
								<option value="1">소아 1인</option>
								<option value="2">소아 2인</option>
								<option value="3">소아 3인</option>
								<option value="4">소아 4인</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">객실타입</label>
						<div class="col-lg-9 text-left">
							<input type="radio" name="stay_bed" id="register-stay-bed-1" value="D" />
							<label for="bed_1">Double</label>
							<input type="radio" name="stay_bed" id="register-stay-bed-2" value="T" />
							<label for="bed_2">Twin</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-3 control-label">숙박 문의 및 요청</label>
						<div class="col-lg-9">
							<input type="text" name="stay_etc" id="register-stay-etc" class="form-control" placeholder="숙박관련 문의 및 요청을 입력해 주세요." />
						</div>
					</div>
					<hr />
					<h3>메모</h3>
					<textarea name="memo" id="register-memo" rows="5" class="form-control"></textarea>
				</form>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btn-register-modify">수정</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<script id="date-item" type="x-tmpl-mustache">
<div class="row">
	<div class="col-md-12">
		<h3>{{date}}</h3>
		<div id="place-{{date}}"></div>
	</div>
</div>
</script>

<script id="place-item" type="x-tmpl-mustache">
<div class="table-responsive">
	<table class="table table-striped table-hover">
		<caption>장소 : {{place}}<br /> 좌장 : {{{principal}}}</caption>
		<thead>
			<tr>
				<th class="text-center col-md-1">시작</th>
				<th class="text-center col-md-1">종료</th>
				<th class="text-center col-md-*">타이틀</th>
				<th class="text-center col-md-3">강연자</th>
				<th class="text-center col-md-3">모니터</th>
				<th class="text-center col-md-2">관리</th>
			</tr>
		</thead>
		<tbody id="session-{{i}}-{{j}}"></tbody>
	</table>
</div>
</script>

<script id="session-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{stime}}</td>
	<td class="text-center">{{etime}}</td>
	<td class=""><a href="/admin/conference/{{code}}/{{idx}}">{{title}}</a></td>
	<td class="text-center">{{{speaker}}}</td>
	<td class="text-center">
		<div class="btn-group">
			<button type="button" class="btn btn-xs btn-default btn-play" data-type="question" data-idx="{{idx}}"><i class="fa fa-laptop"></i> 질문 모니터</button>
			<button type="button" class="btn btn-xs btn-success btn-play" data-type="play" data-idx="{{idx}}"><i class="fa fa-laptop"></i> 강연자</button>
			<button type="button" class="btn btn-xs btn-warning btn-play" data-type="principal" data-idx="{{idx}}"><i class="fa fa-gavel"></i> 좌장</button>
		</div>
	</td>
	<td class="text-center">
		<div class="btn-group">
			<button type="button" class="btn btn-xs btn-primary btn-play" data-type="monitor" data-idx="{{idx}}"><i class="fa fa-filter"></i> 운영자</button>
			<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i> 삭제</button>
		</div>
	</td>
</tr>
</script>

<script id="register-list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{name}} {{#isTitle}}({{title}}){{/isTitle}} </td>
	<td class="text-center">{{office}} {{#isDepartment}}({{department}}){{/isDepartment}} </td>
	<td class="text-center">{{phone}}</td>
	<td class="text-center"><a href="mailto:{{email}}">{{email}}</a></td>
	<td class="text-center">{{traffic_text}}</td>
	<td class="text-center">{{stay_checkin}}</td>
	<td class="text-center">{{stay_checkout}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-success btn-register-view" data-idx="{{idx}}"><i class="fa fa-laptop"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-register-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>
<script id="register-paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-register-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function load() {
	gapi.client.setApiKey('AIzaSyDPvKF4YJBueGygxrBPOtFfgSuT2dWqjQ8');
	gapi.client.load('urlshortener', 'v1',function(){
		var request = gapi.client.urlshortener.url.insert({
	        'resource' : {
	            'longUrl' : "http://${mainUrl}/c/${code}"
	        }
	    });
	    request.execute(function(response) {
	        if (response.id != null) {        
	            var shortUrl = response.id;
	            $('#shortUrl').text(shortUrl);
	            $("#qrcode").qrcode({width:300, height:300, text: shortUrl});
	        } else {
	            alert("error: creating short url");
	        }
	    });
	});
}

function loadSession() {
	$.ajax({
		type:"POST"
		, url : "/api/conference/session/list"
		, cache:false
		, data : {code:'${code}'}
		, success : function(data) {
			if (data.error == 0) {
				var $dateList = $('#date-list');
				$dateList.empty();
				
				var dateTemplate = $('#date-item').html();
				var placeTemplate = $('#place-item').html();
				var sessionTemplate = $('#session-item').html();
				Mustache.parse(dateTemplate);
				Mustache.parse(placeTemplate);
				Mustache.parse(sessionTemplate);
				
				var date = data.data;
				for (var i = 0; i < date.length; i++) {
					var d = date[i];
					$dateList.append(Mustache.render(dateTemplate, {date:d.date}));
					
					var $placeList = $('#place-'+d.date);
					for (var j = 0; j < d.place.length; j++) {
						var p = d.place[j];
						var principal = p.principal1;
						if (p.principal2) {
							principal += ' / ' + p.principal2;
						}
						$placeList.append(Mustache.render(placeTemplate, {place:p.place, i:i, j:j, principal:principal}));
						
						var $sessionList = $('#session-'+i+'-'+j);
						for (var k = 0; k < p.session.length; k++) {
							var s = p.session[k];
							$sessionList.append(Mustache.render(sessionTemplate, s));
						}
					}
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadRegisterList(page) {
	var $list = $("#register-list");
	var $pagination = $("#register-pagination");
	
	var keyword = $("#register-keyword").val();
	$.ajax({type:"GET", url:"/api/admin/conference/register/list", cache:false
		, data : {page:page, keyword:keyword, code:'${code}'}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#register-list-item").html();
				var pagingTemplate = $("#register-paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = virtualNo--;
					switch (l.traffic) {
					case 'C':
						l.traffic_text = '자가운전';
						break;
					case 'B':
						l.traffic_text = '대중교통';
						break;
					case 'N':
						l.traffic_text = '미정';
						break;
					}
					l.isTitle = !l.title ? false : true;
					l.isDepartment = !l.department ? false : true;
					var rendered = Mustache.render(listTemplate, l);
					$list.append(rendered);
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadSession();
	loadRegisterList(1)
	$("#btn-register-search").click(function(){
		loadRegisterList(1);
	});
	$(document).on('click', '.btn-register-page', function(){
		loadRegisterList($(this).data('page'));
	});
	
	$("#btn-save").click(function(){
		$.ajax({
			type:"POST"
			, url : "/api/admin/conference/session/create"
			, cache:false
			, data : $('#submitForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					loadSession();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/conference/session/delete"
				, cache:false
				, data : {code:'${code}', idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadSession();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-play', function(){
		window.open('/admin/conference/${code}/'+$(this).data('idx')+'/'+$(this).data('type'));
	});
	
	$(document).on('click', '.btn-register-view', function(){
		var idx = $(this).data('idx');
		$.ajax({type:"GET", url:"/api/admin/conference/register/item", cache:false
			, data : {code:'${code}', idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$('#register-code').val(data.data.code);
					$('#register-idx').val(data.data.idx);
					$('#register-name').val(data.data.name);
					$('#register-title').val(data.data.title);
					$('#register-office').val(data.data.office);
					$('#register-department').val(data.data.department);
					$('#register-phone').val(data.data.phone);
					$('#register-email').val(data.data.email);
					$('#register-marketer').val(data.data.marketer);
					switch (data.data.traffic) {
					case 'C':
						$('#register-traffic-1').prop('checked', true);
						break;
					case 'B':
						$('#register-traffic-2').prop('checked', true);
						break;
					case 'N':
						$('#register-traffic-3').prop('checked', true);
						break;
					}
					$('#register-traffic-start').val(data.data.traffic_start);
					$('#register-etc').val(data.data.etc);
					switch (data.data.stay) {
					case 'Y':
						$('#register-stay-1').prop('checked', true);
						break;
					case 'N':
						$('#register-stay-2').prop('checked', true);
						break;
					}

					$('#register-stay-checkin').val(data.data.stay_checkin);
					$('#register-stay-checkout').val(data.data.stay_checkout);
					$('#register-stay-adult').val(data.data.stay_adult);
					$('#register-stay-child').val(data.data.stay_child);
					
					switch (data.data.stay_bed) {
					case 'D':
						$('#register-stay-bed-1').prop('checked', true);
						break;
					case 'T':
						$('#register-stay-bed-2').prop('checked', true);
						break;
					}
					$('#register-stay-etc').val(data.data.stay_etc);
					$('#register-memo').val(data.data.memo);
					
					$('#register-data').modal('show');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-register-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({type:"POST", url:"/api/admin/conference/register/delete", cache:false
				, data : {code:'${code}', idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadRegisterList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$('#btn-register-modify').click(function(){
		$.ajax({type:"POST", url:"/api/admin/conference/register/modify", cache:false
			, data : $('#register-modify-form').serialize()
			, success : function(data) {
				if (data.error == 0) {
					$('.modal').modal('hide');
					loadRegisterList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$('#btn-register-excel').click(function(){
		$('#register-excel').attr('src', '/api/admin/conference/register/excel?code=${code}&keyword='+$('#register-keyword').val());
	});
	
});
</script>

<!-- Google -->
<script src="https://apis.google.com/js/client.js?onload=load"></script>