<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-popup-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${conferenceCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>학술대회</li>
			<li><a href="/admin/conference/${code}">${conferenceCfg.title}</a></li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>실시간 투표</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-*">질문</th>
									<th class="text-center col-md-3">관리</th>
								</tr>
							</thead>
							<tbody id="vote-list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="ibox" id="vote-form-add">
				<div class="ibox-title">
					<h5>실시간 질문</h5>
				</div>
				<div class="ibox-content" id="question-list" style="max-height:600px; overflow:auto;"></div>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="vote-modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title">투표 진행중</h4>
				<small class="font-bold">현재 투표를 진행중입니다.</small>
			</div>
			<div class="modal-body text-center">
				남은시간 : <span id="vote-second"></span>초
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="vote-result" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title" id="vote-result-title"></h4>
			</div>
			<div class="modal-body text-center">
				<canvas id="chart" height="140"></canvas>
				<hr />
				<div id="vote-result-answer" class="row">
					<div class="col-sm-2 col-sm-offset-1 text-center" id="vote-result-answer1"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer2"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer3"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer4"></div>
					<div class="col-sm-2 text-center" id="vote-result-answer5"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-popup-footer.jsp"%>

<script id="vote-list-item" type="x-tmpl-mustache">
<tr>
	<td>{{question}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-primary btn-run" data-idx="{{idx}}"><i class="fa fa-play"></i></button>
		<button type="button" class="btn btn-xs btn-success btn-result" data-idx="{{idx}}"><i class="fa fa-laptop"></i></button>
	</td>
</tr>
</script>
<script id="vote-result-list-item" type="x-tmpl-mustache">
<tr>
	<td>{{no}}</td>
	<td>{{answer1}}</td>
	<td>{{answer2}}</td>
	<td>{{answer3}}</td>
	<td>{{answer4}}</td>
	<td>{{answer5}}</td>
	<td>{{answer_etc}}</td>
</tr>
</script>

<script id="question-list-item" type="x-tmpl-mustache">
<div>
	{{no}}. {{question}}
</div>
<hr />
</script>

<script type="text/javascript">
var timer;
var time = 0;

function loadVoteList() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/conference/vote/list"
		, cache:false
		, data : {code:'${code}', sidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $list = $('#vote-list');
				$list.empty();
				
				var template = $('#vote-list-item').html();
				Mustache.parse(template);
				
				var list = data.data;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					$list.append(Mustache.render(template, list[i]));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadQuestionList() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/conference/session/question/principal"
		, cache:false
		, data : {code:'${code}', idx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $list = $('#question-list');
				$list.empty();
				
				var template = $('#question-list-item').html();
				Mustache.parse(template);
				
				var list = data.data;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = i+1;
					$list.append(Mustache.render(template, l));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function voteTimerStart() {
	if (timer) {
		alert('현재 진행중인 투표가 있습니다.');
		return;
	}
	
	time = 15;
	$('#vote-second').text(time);
	timer = setInterval(voteTimerRunning, 1000);
	$('#vote-modal').modal('show');
}

function voteTimerRunning() {
	if (time <= 0) {
		clearInterval(timer);
		timer = null;
		$('#vote-modal').modal('hide');
		return;
	}
	
	time--;
	$('#vote-second').text(time);
}

$(function(){
	loadVoteList();
	loadQuestionList();
	setInterval(loadQuestionList, 5000);
	
	$(document).on('click', '.btn-run', function(){
		if (timer) {
			alert('현재 진행중인 투표가 있습니다.');
			return;
		}
		if (confirm('투표를 시작하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/conference/vote/run"
				, cache:false
				, data : {code:'${code}', sidx:'${idx}', idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						voteTimerStart();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-result', function(){
		var idx=$(this).data('idx');
		$.ajax({
			type:"GET"
			, url : "/api/admin/conference/vote/result"
			, cache:false
			, data : {code:'${code}', sidx:'${idx}', idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$('#vote-result-title').text(data.data.item.question);
					$('#vote-result-answer1').html('1. '+data.data.item.answer1 + '<br />'+data.data.result.answer1+'회<br />'+parseFloat(data.data.result.answer1_percent).toFixed(1)+'%');
					$('#vote-result-answer2').html('2. '+data.data.item.answer2 + '<br />'+data.data.result.answer2+'회<br />'+parseFloat(data.data.result.answer2_percent).toFixed(1)+'%');
					$('#vote-result-answer3').html('3. '+data.data.item.answer3 + '<br />'+data.data.result.answer3+'회<br />'+parseFloat(data.data.result.answer3_percent).toFixed(1)+'%');
					$('#vote-result-answer4').html('4. '+data.data.item.answer4 + '<br />'+data.data.result.answer4+'회<br />'+parseFloat(data.data.result.answer4_percent).toFixed(1)+'%');
					$('#vote-result-answer5').html('5. '+data.data.item.answer5 + '<br />'+data.data.result.answer5+'회<br />'+parseFloat(data.data.result.answer5_percent).toFixed(1)+'%');
					if (data.data.item.type == 'S') {
						$('#vote-result-answer').hide();
					} else {
						$('#vote-result-answer').show();
					}
					
					var $list = $('#vote-result-history');
					$list.empty();
					
					var doughnutData = {
				        labels: [data.data.item.answer1
				                 , data.data.item.answer2
				                 , data.data.item.answer3
				                 , data.data.item.answer4
				                 , data.data.item.answer5],
				        datasets: [{
				            data: [parseFloat(data.data.result.answer1_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer2_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer3_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer4_percent).toFixed(1)
				                   ,parseFloat(data.data.result.answer5_percent).toFixed(1)],
				            backgroundColor: ["#a3e1d4","#dedede","#b5b8cf","#f3a4a4","#e5e19c"]
				        }]
				    } ;

				    var doughnutOptions = {
				        responsive: true
				    };

				    var ctx4 = document.getElementById("chart").getContext("2d");
				    new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});
					$('#vote-result').modal('show');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>