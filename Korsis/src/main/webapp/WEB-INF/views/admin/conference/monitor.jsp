<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-popup-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${conferenceCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>학술대회</li>
			<li><a href="/admin/conference/${code}">${conferenceCfg.title}</a></li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-6">
			<div class="ibox" id="vote-form-add">
				<div class="ibox-title">
					<h5>실시간 질문</h5>
				</div>
				<div class="ibox-content" id="question-list" style="max-height:600px; overflow:auto;"></div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-popup-footer.jsp"%>
<script id="question-list-item" type="x-tmpl-mustache">
<div>
	<div class="pull-right">
		{{^isstatus}}
			<button type="button" class="btn btn-xs btn-primary btn-question-show" data-idx="{{idx}}"><i class="fa fa-play"></i></button>
		{{/isstatus}}
		{{#ismonitor}}
			<button type="button" class="btn btn-xs btn-success btn-question-monitor" data-idx="{{idx}}"><i class="fa fa-laptop"></i></button>
		{{/ismonitor}}
		{{^ismonitor}}
			<button type="button" class="btn btn-xs btn-default btn-question-monitor" data-idx="{{idx}}"><i class="fa fa-laptop"></i></button>
		{{/ismonitor}}
		<button type="button" class="btn btn-xs btn-danger btn-question-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</div>
	{{no}}. {{question}}
</div>
<hr />
</script>

<script type="text/javascript">
function loadQuestionList() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/conference/session/question"
		, cache:false
		, data : {code:'${code}', idx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $list = $('#question-list');
				$list.empty();
				
				var template = $('#question-list-item').html();
				Mustache.parse(template);
				
				var list = data.data;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					l.no = i+1;
					l.isstatus = l.status=='Y';
					l.ismonitor = l.monitor=='Y';
					$list.append(Mustache.render(template, l));
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadQuestionList();
	setInterval(loadQuestionList, 5000);
	$(document).on('click', '.btn-question-delete', function(){
		if (confirm('삭제 하시겠습니까?')) {
			var idx=$(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/conference/session/question/delete"
				, cache:false
				, data : {code:'${code}', sidx:'${idx}', idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						loadQuestionList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-question-show', function(){
		if (confirm('해당 질문을 좌장에게 표시 하시겠습니까?')) {
			var idx=$(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/conference/session/question/show"
				, cache:false
				, data : {code:'${code}', sidx:'${idx}', idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						loadQuestionList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-question-monitor', function(){
		var idx=$(this).data('idx');
		$.ajax({
			type:"GET"
			, url : "/api/admin/conference/session/question/display"
			, cache:false
			, data : {code:'${code}', sidx:'${idx}', idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadQuestionList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>