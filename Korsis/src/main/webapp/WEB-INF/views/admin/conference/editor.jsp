<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>${conferenceCfg.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>학술대회</li>
			<li><a href="/admin/conference/${code}">${conferenceCfg.title}</a></li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>투표 목록</h5>
				</div>
				<div class="ibox-content">
					<table class="footable table table-stripped toggle-arrow-tiny">
						<thead>
						<tr>
							<th class="text-center col-md-*">질문</th>
							<th data-hide="all">답1</th>
							<th data-hide="all">답2</th>
							<th data-hide="all">답3</th>
							<th data-hide="all">답4</th>
							<th data-hide="all">답5</th>
							<th class="text-center col-md-3">관리</th>
						</tr>
						</thead>
						<tbody id="list"></tbody>
						<tfoot>
							<tr>
							    <td colspan="5">
							        <ul class="pagination pull-right"></ul>
							    </td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox" id="vote-form-add">
				<div class="ibox-title">
					<h5>투표 등록</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="voteForm" id="voteForm" method="post" class="form-horizontal" onsubmit="return false;" enctype="multipart/form-data">
						<input type="hidden" name="code" id="code" value="${code}" />
						<input type="hidden" name="sidx" id="sidx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">종류</label>
							<div class="col-lg-9">
								<label class="radio-inline">
									<input type="radio" name="type" class="type" value="O" checked="checked" />
									객관식
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" class="type" value="M" />
									객관식 (다중)
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" class="type" value="S" />
									주관식
								</label>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">질문</label>
							<div class="col-lg-9">
								<input type="text" name="question" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 1</label>
							<div class="col-lg-9">
								<input type="text" name="answer1" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 2</label>
							<div class="col-lg-9">
								<input type="text" name="answer2" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 3</label>
							<div class="col-lg-9">
								<input type="text" name="answer3" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 4</label>
							<div class="col-lg-9">
								<input type="text" name="answer4" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 5</label>
							<div class="col-lg-9">
								<input type="text" name="answer5" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-add" type="submit">등록</button>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox hidden" id="vote-form-modify">
				<div class="ibox-title">
					<h5>투표 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="voteModifyForm" id="voteModifyForm" method="post" class="form-horizontal" onsubmit="return false;" enctype="multipart/form-data">
						<input type="hidden" name="code" value="${code}" />
						<input type="hidden" name="sidx" value="${idx}" />
						<input type="hidden" name="idx" id="idx" />
						<div class="form-group">
							<label class="col-lg-3 control-label">종류</label>
							<div class="col-lg-9">
								<label class="radio-inline">
									<input type="radio" name="type" class="type-modify" value="O" />
									객관식
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" class="type-modify" value="M" />
									객관식 (다중)
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" class="type-modify" value="S" />
									주관식
								</label>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">질문</label>
							<div class="col-lg-9">
								<input type="text" name="question" id="question" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 1</label>
							<div class="col-lg-9">
								<input type="text" name="answer1" id="answer1" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 2</label>
							<div class="col-lg-9">
								<input type="text" name="answer2" id="answer2" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 3</label>
							<div class="col-lg-9">
								<input type="text" name="answer3" id="answer3" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 4</label>
							<div class="col-lg-9">
								<input type="text" name="answer4" id="answer4" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">답변 5</label>
							<div class="col-lg-9">
								<input type="text" name="answer5" id="answer5" class="form-control" />
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-modify" type="submit">등록</button>
							<button class="btn btn-default" id="btn-modify-cancel" type="reset">취소</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>세션 정보 수정</h5>
				</div>
				<div class="ibox-content">
					<form role="form" name="modifyForm" id="modifyForm" method="post" class="form-horizontal" onsubmit="return false;" enctype="multipart/form-data">
						<input type="hidden" name="code" value="${code}" />
						<input type="hidden" name="idx" value="${idx}" />
						<div class="form-group">
							<label class="col-lg-3 control-label">종류</label>
							<div class="col-lg-9">
								<label class="radio-inline">
									<input type="radio" name="type" class="type" value="N" <c:if test="${item.type eq 'N'}"> checked="checked"</c:if> />
									일반
								</label>
								<label class="radio-inline">
									<input type="radio" name="type" class="type" value="S" <c:if test="${item.type eq 'S'}"> checked="checked"</c:if> />
									강연
								</label>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">장소 (홀)</label>
							<div class="col-lg-9">
								<input type="text" name="place" class="form-control" value="${fn:escapeXml(item.place)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">타이틀</label>
							<div class="col-lg-9">
								<input type="text" name="title" class="form-control" value="${fn:escapeXml(item.title)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">좌장 1</label>
							<div class="col-lg-9">
								<input type="text" name="principal1" class="form-control" value="${fn:escapeXml(item.principal1)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">좌장 2</label>
							<div class="col-lg-9">
								<input type="text" name="principal2" class="form-control" value="${fn:escapeXml(item.principal2)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">발표자</label>
							<div class="col-lg-9">
								<input type="text" name="speaker" class="form-control" value="${fn:escapeXml(item.speaker)}" />
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">발표자료</label>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-lg-12">
										<input type="file" name="file" class="form-control" accept="image/*,application/pdf" />
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										&lt;jpg, png, gif, pdf 만 등록가능&gt;
									</div>
								</div>
								<c:if test="${item.path ne null and item.path ne ''}">
									<div class="row">
										<div class="col-lg-12">
											<a href="/api/conference/session/download/${item.code}/${item.idx}/${item.filename}" target="_blank">${item.filename }</a>
										</div>
									</div>
								</c:if>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">날짜</label>
							<div class="col-lg-9">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input type="text" name="date" value="${item.date}" class="form-control datepicker" />
								</div>
							</div>
						</div>
						<hr />
						<div class="form-group">
							<label class="col-lg-3 control-label">시간</label>
							<div class="col-lg-9">
								<div class="row">
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
											<input type="text" name="stime" value="${item.stime}" class="form-control clockpicker" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="input-group">
											<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
											<input type="text" name="etime" value="${item.etime}" class="form-control clockpicker" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-save" type="submit">수정</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>

<!-- FooTable -->
<script src="/resources/bootstrap/inspinia/js/plugins/footable/footable.all.min.js"></script>

<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td>{{question}}</td>
	<td>{{answer1}}</td>
	<td>{{answer2}}</td>
	<td>{{answer3}}</td>
	<td>{{answer4}}</td>
	<td>{{answer5}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="{{idx}}" data-type="{{type}}" data-question="{{question}}" data-answer1="{{answer1}}" data-answer2="{{answer2}}" data-answer3="{{answer3}}" data-answer4="{{answer4}}" data-answer5="{{answer5}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script type="text/javascript">
function loadList() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/conference/vote/list"
		, cache:false
		, data : {code:'${code}', sidx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				var $list = $('#list');
				$list.empty();
				
				var template = $('#list-item').html();
				Mustache.parse(template);
				
				var list = data.data;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					$list.append(Mustache.render(template, list[i]));
				}
				$('.footable').trigger('footable_redraw'); 
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	$('.footable').footable();
	loadList();

	$("#btn-save").click(function(){
		$("#modifyForm").ajaxForm({
			type:"POST"
			, url:"/api/admin/conference/session/modify"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#modifyForm").submit();
	});
	
	$("#btn-add").click(function(){
		$.ajax({
			type:"POST"
			, url : "/api/admin/conference/vote/add"
			, cache:false
			, data : $('#voteForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					$('#voteForm')[0].reset();
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#btn-modify").click(function(){
		$.ajax({
			type:"POST"
			, url : "/api/admin/conference/vote/modify"
			, cache:false
			, data : $('#voteModifyForm').serialize()
			, success : function(data) {
				if (data.error == 0) {
					$('#voteModifyForm')[0].reset();
					loadList();
					$('#vote-form-add').removeClass('hidden');
					$('#vote-form-modify').addClass('hidden');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제하시겠습니까?')) {
			var idx = $(this).data('idx');
			$.ajax({
				type:"GET"
				, url : "/api/admin/conference/vote/delete"
				, cache:false
				, data : {code:'${code}', sidx:'${idx}', idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-modify', function(){
		$('#idx').val($(this).data('idx'));
		$('#question').val($(this).data('question'));
		$('#answer1').val($(this).data('answer1'));
		$('#answer2').val($(this).data('answer2'));
		$('#answer3').val($(this).data('answer3'));
		$('#answer4').val($(this).data('answer4'));
		$('#answer5').val($(this).data('answer5'));
		var type = $(this).data('type');
		$('.type-modify').each(function(i){
			if ($(this).attr('value') == type) {
				$(this).prop('checked', true);
			}
		});
		
		$('#vote-form-add').addClass('hidden');
		$('#vote-form-modify').removeClass('hidden');
	});
	
	$('#btn-modify-cancel').click(function(){
		$('#vote-form-add').removeClass('hidden');
		$('#vote-form-modify').addClass('hidden');
	});
});
</script>