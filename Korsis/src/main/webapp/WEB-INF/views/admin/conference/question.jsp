<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>[${siteInfo.title}] 관리자 페이지</title>
	<link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/resources/plugins/jstree/themes/default/style.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
	
	<link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
	<link href="/resources/css/admin.css" rel="stylesheet">
	<style type="text/css">
		#question-wrapper {
			display:none;
			position:absolute;
			background:white;
			top:20%;
			left:20%;
			right:20%;
			bottom:20%;
			border-radius: 50px;
			padding:50px;
			z-index:100;
		}
		#question-content {
			position:relative;
			top:50%;
			padding-top:50px;
			transform: translateY(-50%);
			text-align:center;
			font-size:5em;
			word-break: break-all;
		}
		
	</style>
	
</head>

<body class="gray-bg">

	<div id="question-wrapper">
		<div id="question-content">
		</div>
	</div>


	<!-- Mainly scripts -->
	<script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
	<script src="/resources/bootstrap/inspinia/js/bootstrap.min.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	
	<!-- Custom and plugin javascript -->
	<script src="/resources/bootstrap/inspinia/js/inspinia.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/pace/pace.min.js"></script>

	<!-- iCheck -->
	<script src="/resources/bootstrap/inspinia/js/plugins/iCheck/icheck.min.js"></script>
	
	<!-- Data picker -->
	<script src="/resources/bootstrap/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>

	<!-- Clock picker -->
	<script src="/resources/bootstrap/inspinia/js/plugins/clockpicker/clockpicker.js"></script>
	
	
	<script src="/resources/js/admin.js"></script>
</body>
</html>


<script type="text/javascript">
var timer;
var time = 0;

function loadConfig() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/conference/cfg"
		, cache:false
		, data : {code:'${code}'}
		, success : function(data) {
			if (data.error == 0) {
				if (data.data.background) {
					$('body').css('background-image', 'url('+data.data.background+')');
					$('body').css('background-size', 'cover');
					$('body').css('background-repeat', 'no-repeat');
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadQuestion() {
	$.ajax({
		type:"POST"
		, url : "/api/admin/conference/session/question/monitor"
		, cache:false
		, data : {code:'${code}', idx:'${idx}'}
		, success : function(data) {
			if (data.error == 0) {
				if (data.data) {
					$('#question-content').text(data.data.question);
					$('#question-wrapper').show();
				} else {
					$('#question-wrapper').hide();
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadConfig();
	loadQuestion();
	setInterval(loadQuestion, 1000);
});
</script>