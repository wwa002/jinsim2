<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>메일 발송 연락처</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>메일</li>
			<li class="active"><strong>발송 연락처</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>연락처 그룹 추가</h5>
				</div>
				<div class="ibox-content">
					<form id="groupForm" class="form-horizontal" enctype="multipart/form-data">
						<div class="form-group">
							<div class="col-sm-12">
								<div class="input-group">
									<input type="text" name="title" class="form-control" placeholder="연락처 이름을 입력해 주세요." />
									<span class="input-group-btn">
										<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button>
									</span>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox">
				<div class="ibox-title">
					<h5>연락처 그룹 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" id="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">이름</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="group-list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="group-pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		<div id="contacts-wrapper" class="col-sm-6 hidden">
			<div class="ibox">
				<div class="ibox-title">
					<h5>연락처 항목 추가</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="contactForm" class="form-horizontal" enctype="multipart/form-data">
								<input type="hidden" name="gidx" id="gidx" />
								<div class="form-group">
									<div class="col-xs-6">
										<input type="text" name="name" class="form-control" placeholder="이름을 입력해 주세요." />
									</div>
									<div class="col-xs-6">
										<div class="input-group">
											<input type="text" name="email" class="form-control" placeholder="이메일 주소를 입력해 주세요." />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<h4>회원 그룹으로 추가</h4>
							<c:forEach items="${gcds}" var="g">
								<button class="btn btn-default btn-group-add" data-gcd="${g.gcd}">
									<i class="fa fa-plus"></i>
									'${g.title}' 추가
								</button>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		
			<div class="ibox">
				<div class="ibox-title">
					<h5>연락처 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="contactsSearchForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" id="contacts-keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button id="btn-contacts-search" type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">이름</th>
									<th class="text-center">휴대폰</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="contacts-list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="contacts-pagination"></ul>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="group-list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">
		<input type="text" id="title-{{idx}}" value="{{title}}" class="form-control" placeholder="연락처 이름을 입력해 주세요." />
	</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-primary btn-group-view" data-idx="{{idx}}"><i class="fa fa-list"></i></button>
		<button type="button" class="btn btn-xs btn-warning btn-group-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-group-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="group-paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-group-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script id="contact-list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">
		<input type="text" id="name-{{idx}}" value="{{name}}" class="form-control" placeholder="이름을 입력해 주세요." />
	</td>
	<td class="text-center">
		<input type="text" id="email-{{idx}}" value="{{email}}" class="form-control" placeholder="이메일 주소를 입력해 주세요." />
	</td>
	<td class="text-center">
		<button type="button" class="btn btn-xs btn-warning btn-contact-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
		<button type="button" class="btn btn-xs btn-danger btn-contact-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
	</td>
</tr>
</script>

<script id="contact-paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-contact-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadGroupList(page) {
	var $list = $("#group-list");
	var $pagination = $("#group-pagination");
	
	var keyword = $("#keyword").val();
	$.ajax({type:"GET", url:"/api/admin/mail/contacts/group/list", cache:false
		, data : {page:page, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#group-list-item").html();
				var pagingTemplate = $("#group-paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, idx:l.idx, title:l.title
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function loadContactList(page) {
	var $list = $("#contacts-list");
	var $pagination = $("#contacts-pagination");
	
	var gidx = $("#gidx").val();
	var keyword = $("#contacts-keyword").val();
	$.ajax({type:"GET", url:"/api/admin/mail/contacts/contact/list", cache:false
		, data : {page:page, gidx:gidx, keyword:keyword}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#contact-list-item").html();
				var pagingTemplate = $("#contact-paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, idx:l.idx, name:l.name, email:l.email
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

$(function(){
	loadGroupList(1);
	$(document).on('click', '.btn-group-page', function(){
		loadGroupList($(this).data('page'));
	});
	$(document).on('click', '.btn-contact-page', function(){
		loadContactList($(this).data('page'));
	});
	
	$("#btn-search").click(function(){
		loadGroupList(1);
	});
	$("#btn-contacts-search").click(function(){
		loadContactList(1);
	});
	
	$("#groupForm").submit(function(){
		$.ajax({type:"POST", url:"/api/admin/mail/contacts/group/add", cache:false
			, data : $("#groupForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					$("#groupForm")[0].reset();
					loadGroupList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});

	$(document).on('click', '.btn-group-view', function(){
		$("#gidx").val($(this).data("idx"));
		$("#contacts-keyword").val("");
		$("#contacts-wrapper").removeClass("hidden");
		loadContactList(1);
	});
	$(document).on('click', '.btn-group-modify', function(){
		var idx = $(this).data("idx");
		var title = $("#title-"+idx).val();
		$.ajax({type:"POST", url:"/api/admin/mail/contacts/group/modify", cache:false
			, data : {idx:idx, title:title}
			, success : function(data) {
				if (data.error == 0) {
					alert("수정되었습니다.");
					loadGroupList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	$(document).on('click', '.btn-group-delete', function(){
		if (confirm('삭제한 데이터는 되돌릴 수 없습니다.\n삭제 하시겠습니까?')) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/admin/mail/contacts/group/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						$("#gidx").val("");
						$("#contacts-keyword").val("");
						$("#contacts-wrapper").addClass("hidden");
						loadGroupList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$("#contactForm").submit(function(){
		$.ajax({type:"POST", url:"/api/admin/mail/contacts/contact/add", cache:false
			, data : $("#contactForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					$("#contactForm")[0].reset();
					loadContactList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
		return false;
	});
	
	$(document).on('click', '.btn-contact-modify', function(){
		var idx = $(this).data("idx");
		var gidx = $("#gidx").val();
		var name = $("#name-"+idx).val();
		var email = $("#email-"+idx).val();
		$.ajax({type:"POST", url:"/api/admin/mail/contacts/contact/modify", cache:false
			, data : {gidx:gidx, idx:idx, name:name, email:email}
			, success : function(data) {
				if (data.error == 0) {
					alert("수정되었습니다.");
					loadContactList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	$(document).on('click', '.btn-contact-delete', function(){
		if (confirm('삭제한 데이터는 되돌릴 수 없습니다.\n삭제 하시겠습니까?')) {
			var idx = $(this).data("idx");
			var gidx = $("#gidx").val();
			$.ajax({type:"POST", url:"/api/admin/mail/contacts/contact/delete", cache:false
				, data : {gidx:gidx, idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadContactList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
	$(".btn-group-add").click(function(){
		var gidx = $("#gidx").val();
		var gcd = $(this).data("gcd");
		$.ajax({type:"POST", url:"/api/admin/mail/contacts/contact/add", cache:false
			, data : {gidx:gidx, gcd:gcd}
			, success : function(data) {
				if (data.error == 0) {
					$("#contactForm")[0].reset();
					loadContactList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>
