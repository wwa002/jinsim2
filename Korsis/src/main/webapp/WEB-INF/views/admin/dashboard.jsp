<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-8 col-md-8">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>접속량</h5>
						</div>
						<div class="ibox-content">
							<div class="flot-chart">
								<div class="flot-chart-content" id="flot-dashboard-chart"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
			<div class="row">
				<div class="col-md-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-success pull-right">일간</span>
							<h5>가입자</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins" id="member-today"></h1>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-info pull-right">주간</span>
							<h5>가입자</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins" id="member-week"></h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-primary pull-right">월간</span>
							<h5>가입자</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins" id="member-month"></h1>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-danger pull-right">전체</span>
							<h5>회원수</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins" id="member-all"></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<c:if test="${siteInfo.sitecd eq 'PPPP'}">
		<div class="row">
			<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>캐스트</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins" id="db-cast"></h1>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>정보 DB</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins" id="db-infodb"></h1>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>병원</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins" id="db-hospital"></h1>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>약국</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins" id="db-pharmacy"></h1>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	
	<div class="row">
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>최근 게시물</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-md-2">게시판</th>
									<th class="text-center">제목</th>
									<th class="text-center col-md-2">작성자</th>
									<th class="text-center col-md-2">작성일</th>
									<th class="text-center col-md-1">조회수</th>
								</tr>
							</thead>
							<tbody id="board-list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6">
			<div class="ibox">
				<div class="ibox-title">
					<h5>업데이트 및 시스템 공지</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">제목</th>
									<th class="text-center col-md-2">등록일</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<!-- Flot -->
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.js"></script>
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.time.js"></script>

<script id="board-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">
		<a href="/admin/board/{{code}}"><span class="btn btn-info btn-xs btn-block">{{title}}</span></a>
	</td>
	<td>
		<a href="/admin/board/{{code}}/{{idx}}">{{subject}}</a>
	</td>
	<td class="text-center">{{{name}}}</td>
	<td class="text-center">{{date}}</td>
	<td class="text-center">{{read}}</td>
</tr>
</script>

<script type="text/javascript">
function gd(year, month, day) {
	return new Date(year, month - 1, day).getTime();
}

function gd2(date) {
	var year = parseInt(date.substring(0, 4));
	var month = parseInt(date.substring(4, 6));
	var day = parseInt(date.substring(6, 8));
	return new Date(year, month - 1, day).getTime();
}

$(function(){
	$.ajax({
		type : "GET"
			, url : "/api/admin/dashboard"
			, cache : false
			, success : function(data){
				console.log(data);
				if (data.error == 0) {
					var member = data.data.member;
					$("#member-today").text(numberWithCommas(member.today)+'명');
					$("#member-week").text(numberWithCommas(member.week)+'명');
					$("#member-month").text(numberWithCommas(member.month)+'명');
					$("#member-all").text(numberWithCommas(member.total)+'명');
					
					var db = data.data.db;
					$("#db-cast").text(numberWithCommas(db.cast)+'건');
					$("#db-infodb").text(numberWithCommas(db.infodb)+'건');
					$("#db-hospital").text(numberWithCommas(db.hospital)+'건');
					$("#db-pharmacy").text(numberWithCommas(db.pharmacy)+'건');
					
					var counter = data.data.counter;
					var data2 = [];
					var data3 = [];
					for (var i = 0; i < counter.length; i++) {
						var c = counter[i];
						data2.push([gd2(c.date), c.visit]);
						data3.push([gd2(c.date), c.page]);
					}
					
					var dataset = [ 
						{label : "접속자", data : data2, color : "#1ab394", bars : {show : true, align : "center", barWidth : 24 * 60 * 60 * 600, lineWidth : 0}}
						, {label : "페이지 뷰", data : data3, yaxis : 2, color : "#1C84C6"
							, lines : {lineWidth : 1, show : true, fill : true
								, fillColor : {colors : [ {opacity : 0.2}, {opacity : 0.4} ]}}
							, splines : {show : false,tension : 0.6,lineWidth : 1,fill : 0.1}}
						];
					var options = {
						xaxis : {mode : "time", tickSize : [ 3, "day" ], tickLength : 0, axisLabel : "Date", axisLabelUseCanvas : true, axisLabelFontSizePixels : 12, axisLabelFontFamily : 'Arial', axisLabelPadding : 10, color : "#d5d5d5"}
						, yaxes : [ 
							{position : "left", color : "#d5d5d5", axisLabelUseCanvas : true, axisLabelFontSizePixels : 12, axisLabelFontFamily : 'Arial', axisLabelPadding : 3}
							, {position : "right", clolor : "#d5d5d5", axisLabelUseCanvas : true, axisLabelFontSizePixels : 12, axisLabelFontFamily : ' Arial', axisLabelPadding : 67}
							]
						, legend : {noColumns : 1, labelBoxBorderColor : "#000000", position : "nw"}
						, grid : {hoverable : false, borderWidth : 0}
					};
					var previousPoint = null, previousLabel = null;
					$.plot($("#flot-dashboard-chart"), dataset, options);
					
					var board = data.data.board;
					var $boardList = $("#board-list");
					var boardListTemplate = $("#board-item").html();
					Mustache.parse(boardListTemplate);
					for (var i = 0; i < board.length; i++) {
						var b = board[i];
						var name = b.id != '' ? '<a href="/admin/member/'+b.id+'">'+b.name+'</a>':b.name;
						var subject = b.subject;
						if (b.cnt_comment > 0) {
							subject += ' ['+b.cnt_comment+']';
						}
						var rendered = Mustache.render(boardListTemplate, {
							code:b.code, idx:b.idx, title:b.title, subject:subject, name:name
							, read:numberWithCommas(b.cnt_read), date:getDefaultDataFormat(b.regdate.substring(0, 8))
						});
						$boardList.append(rendered);
					}
				}
			}
			, error : function(data){
				alert(data.responseJSON.error);
			}
	});
	
});
</script>