<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>접속공지</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>APP</li>
			<li class="active"><strong>접속공지</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>공지 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">종류</th>
									<th class="text-center">장비</th>
									<th class="text-center">버전</th>
									<th class="text-center">메세지</th>
									<th class="text-center">시간</th>
									<th class="text-center">사용여부</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination" id="pagination"></ul>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>시스템 공지</h5>
				</div>
				<div class="ibox-content" style="display: block;">
					<form role="form" name="submitForm" id="submitForm" method="post" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="idx" id="idx" value="" />
						<div id="wrapper-code">
							<div class="form-group">
								<label class="col-lg-3 control-label">종류</label>
								<div class="col-lg-9">
									<label class="radio-inline">
										<input type="radio" name="code" id="code_1" class="code" value="MAINTENANCE" checked="checked" />
										점검
									</label>
									<label class="radio-inline">
										<input type="radio" name="code" id="code_2" class="code" value="NOTICE" />
										공지
									</label>
									<label class="radio-inline">
										<input type="radio" name="code" id="code_3" class="code" value="VERSION" />
										버전
									</label>
								</div>
							</div>
							<hr />
						</div>
						<div id="wrapper-os" class="hidden">
							<div class="form-group">
								<label class="col-lg-3 control-label">장비</label>
								<div class="col-lg-9">
									<label class="radio-inline">
										<input type="radio" name="os" id="os_1" value="all" checked="checked" />
										전체
									</label>
									<label class="radio-inline">
										<input type="radio" name="os" id="os_2" value="ios" />
										아이폰
									</label>
									<label class="radio-inline">
										<input type="radio" name="os" id="os_3" value="android" />
										안드로이드
									</label>
								</div>
							</div>
							<hr />
						</div>
						<div id="wrapper-version" class="hidden">
							<div class="form-group">
								<label class="col-lg-3 control-label">버전</label>
								<div class="col-lg-9">
									<input type="number" name="version" id="version" value="1" class="form-control" />
								</div>
							</div>
							<hr />
						</div>
						<div id="wrapper-message">
							<div class="form-group">
								<label class="col-lg-3 control-label">메세지</label>
								<div class="col-lg-9">
									<textarea name="message" id="message" rows="5" class="form-control"></textarea>
								</div>
							</div>
							<hr />
						</div>
						<div id="wrapper-date">
							<div class="form-group">
								<label class="col-lg-3 control-label">시작</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" name="startdt" id="startdt" value="" class="form-control datepicker" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
												<input type="text" name="starttm" id="starttm" value="" class="form-control clockpicker" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-group">
								<label class="col-lg-3 control-label">종료</label>
								<div class="col-lg-9">
									<div class="row">
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
												<input type="text" name="enddt" id="enddt" value="" class="form-control datepicker" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
												<input type="text" name="endtm" id="endtm" value="" class="form-control clockpicker" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<hr />
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">사용여부</label>
							<div class="col-lg-9">
								<label class="radio-inline">
									<input type="radio" name="status" id="status_1" value="Y" checked="checked" />
									사용
								</label>
								<label class="radio-inline">
									<input type="radio" name="status" id="status_2" value="N" />
									해제
								</label>
							</div>
						</div>
						<hr />
						<div class="text-right">
							<button class="btn btn-primary" id="btn-save" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">{{no}}</td>
	<td class="text-center">{{{code_text}}}</td>
	<td class="text-center">{{os}}</td>
	<td class="text-center">{{version}}</td>
	<td>{{message}}</td>
	<td class="text-center">{{date}}</td>
	<td class="text-center">{{status}}</td>
	<td class="text-center">
		<button type="button" class="btn btn-warning btn-modify" data-idx="{{idx}}" data-code="{{code}}" data-os="{{os}}" data-version="{{version}}" data-message="{{message}}" data-startdate="{{startdate}}" data-enddate="{{enddate}}" data-status="{{status}}">수정</button>
		<button type="button" class="btn btn-danger btn-delete" data-idx="{{idx}}">삭제</button>
	</td>
</tr>
</script>

<script id="paging-item" type="x-tmpl-mustache">
<li class="{{active}}">
	<a href="javascript:void(0);" class="btn-page" data-page="{{page}}" title="{{page}}페이지로 이동">{{pageTitle}}</a>
</li>
</script>

<script type="text/javascript">
function loadList(page) {
	var $list = $("#list");
	var $pagination = $("#pagination");
	
	$.ajax({type:"GET", url:"/api/admin/app/system", cache:false
		, data : {page:page}
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				$pagination.empty();
				
				var list = data.data.list;
				var paging = data.data.paging;
				
				var listTemplate = $("#list-item").html();
				var pagingTemplate = $("#paging-item").html();
				Mustache.parse(listTemplate);
				Mustache.parse(pagingTemplate);
				
				var virtualNo = paging.virtualRecordNo;
				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var code_text = "";
					var date = '';
					switch (l.code) {
					case 'MAINTENANCE':
						code_text = '<span class="label label-danger">점검</span>';
						date = getDefaultDataFormat(l.startdate) + ' ~ ' + getDefaultDataFormat(l.enddate);
						break;
					case 'NOTICE':
						code_text = '<span class="label label-info">공지</span>';
						date = getDefaultDataFormat(l.startdate) + ' ~ ' + getDefaultDataFormat(l.enddate);
						break;
					case 'VERSION':
						code_text = '<span class="label label-default">버전</span>';
						break;
					}
					
					var rendered = Mustache.render(listTemplate, {
						no:virtualNo--, code_text:code_text, os:l.os, version:l.version
						, message:l.message, date:date, status:l.status
						, idx:l.idx, code:l.code, startdate:l.startdate, enddate:l.enddate, status:l.status
					});
					$list.append(rendered);
				}
				
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.prevBlockNo, pageTitle:"«"}));
				for (var i = paging.startPageNo; i <= paging.endPageNo; i++) {
					$pagination.append(Mustache.render(pagingTemplate, {page:i, pageTitle:i, active:i == paging.pageNo ? "active" : ""}));
				}
				$pagination.append(Mustache.render(pagingTemplate, {page:paging.nextBlockNo, pageTitle:"»"}));
				
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

function validWrapper() {
	if ($("#code_1").prop("checked")) {
		$("#wrapper-os").addClass("hidden");
		$("#wrapper-version").addClass("hidden");
		$("#wrapper-message").removeClass("hidden");
		$("#wrapper-date").removeClass("hidden");
	} else if ($("#code_2").prop("checked")) {
		$("#wrapper-os").removeClass("hidden");
		$("#wrapper-version").addClass("hidden");
		$("#wrapper-message").removeClass("hidden");
		$("#wrapper-date").removeClass("hidden");
	} else {
		$("#wrapper-os").removeClass("hidden");
		$("#wrapper-version").removeClass("hidden");
		$("#wrapper-message").addClass("hidden");
		$("#wrapper-date").addClass("hidden");
	}
}

$(function(){
	$(".code").click(function(){
		validWrapper();
	});
	
	loadList(1);
	$(document).on('click', '.btn-page', function(){
		loadList($(this).data('page'));
	});
	
	$("#btn-save").click(function(){
		if ($("#code_1").prop("checked") || $("#code_2").prop("checked")) {
			if (!$("#message").val()) {
				alert("메세지를 입력해 주세요.");
				$("#message").focus();
				return;
			}
			
			if (!$("#startdt").val()) {
				alert("시작일을 입력해 주세요.");
				$("#startdt").focus();
				return;
			}
			
			if (!$("#starttm").val()) {
				alert("시작시간을 입력해 주세요.");
				$("#starttm").focus();
				return;
			}
			
			if (!$("#enddt").val()) {
				alert("종료일을 입력해 주세요.");
				$("#enddt").focus();
				return;
			}
			
			if (!$("#endtm").val()) {
				alert("종료시간을 입력해 주세요.");
				$("#endtm").focus();
				return;
			}
		} else {
			if (!$("#version").val()) {
				alert("버전을 입력해 주세요.");
				$("#version").focus();
				return false;
			}
		}

		$.ajax({type:"POST", url:"/api/admin/app/system/save", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				if (data.error == 0) {
					$("#submitForm")[0].reset();
					$("#idx").val('');
					loadList(1);
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-modify', function(){
		$("#idx").val($(this).data("idx"));
		if ($(this).data("code") == "MAINTENANCE") {
			$("#code_1").prop("checked", true);
			$("#message").val($(this).data("message"));
			var startdate = $(this).data("startdate")+"";
			var enddate = $(this).data("enddate")+"";
			$("#startdt").val(startdate.substring(0, 4)+"-"+startdate.substring(4, 6)+"-"+startdate.substring(6, 8));
			$("#starttm").val(startdate.substring(8, 10)+":"+startdate.substring(10, 12));
			$("#enddt").val(enddate.substring(0, 4)+"-"+enddate.substring(4, 6)+"-"+enddate.substring(6, 8));
			$("#endtm").val(enddate.substring(8, 10)+":"+enddate.substring(10, 12));
		} else if ($(this).data("code") == "NOTICE") {
			$("#code_2").prop("checked", true);
			if ($(this).data("os") == "all") {
				$("#os_1").prop("checked", true);
			} else if ($(this).data("os") == "ios") {
				$("#os_2").prop("checked", true);
			} else {
				$("#os_3").prop("checked", true);
			}
			$("#message").val($(this).data("message"));
			var startdate = $(this).data("startdate")+"";
			var enddate = $(this).data("enddate")+"";
			$("#startdt").val(startdate.substring(0, 4)+"-"+startdate.substring(4, 6)+"-"+startdate.substring(6, 8));
			$("#starttm").val(startdate.substring(8, 10)+":"+startdate.substring(10, 12));
			$("#enddt").val(enddate.substring(0, 4)+"-"+enddate.substring(4, 6)+"-"+enddate.substring(6, 8));
			$("#endtm").val(enddate.substring(8, 10)+":"+enddate.substring(10, 12));
		} else {
			$("#code_3").prop("checked", true);
			if ($(this).data("os") == "all") {
				$("#os_1").prop("checked", true);
			} else if ($(this).data("os") == "ios") {
				$("#os_2").prop("checked", true);
			} else {
				$("#os_3").prop("checked", true);
			}
			$("#startdt").val("");
			$("#starttm").val("");
			$("#enddt").val("");
			$("#endtm").val("");
		}
		
		$("#status_1").prop("checked", $(this).data("status") == "Y");
		$("#status_2").prop("checked", $(this).data("status") != "Y");
		validWrapper();
	});

	$(document).on('click', '.btn-delete', function(){
		if (confirm('삭제한 데이터는 되돌릴 수 없습니다.\n삭제 하시겠습니까?')) {
			var idx = $(this).data("idx");
			$.ajax({
				type:"POST"
				, url : "/api/admin/app/system/delete"
				, cache:false
				, data : {idx:idx}
				, success : function(data) {
					if (data.error == 0) {
						loadList(1);
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>