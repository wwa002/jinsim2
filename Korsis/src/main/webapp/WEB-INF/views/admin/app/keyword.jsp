<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="today" value="<%=new java.util.Date()%>" />
<%@ include file="/WEB-INF/views/include/admin-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>키워드</h2>
		<ol class="breadcrumb">
			<li><a href="/">HOME</a></li>
			<li><a href="/admin/dashboard">ADMIN</a></li>
			<li>APP</li>
			<li class="active"><strong>키워드</strong></li>
		</ol>
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>키워드 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="text-center col-lg-2">순위</th>
									<th class="text-center col-lg-*">키워드</th>
									<th class="text-center col-lg-4">관리</th>
								</tr>
							</thead>
							<tbody id="list"></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-title">
					<h5>키워드 추가</h5>
				</div>
				<div class="ibox-content" style="display: block;">
					<form role="form" name="submitForm" id="submitForm" method="post" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-lg-3 control-label">키워드</label>
							<div class="col-lg-9">
								<input type="text" name="value" id="value" class="form-control" />
							</div>
						</div>
						<div class="text-right">
							<button class="btn btn-primary" id="btn-create" type="submit">등록</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/admin-footer.jsp"%>
<script id="list-item" type="x-tmpl-mustache">
<tr>
	<td class="text-center">
		{{sort}}
	</td>
	<td class="text-center">
		{{value}}
	</td>
	<td class="text-center">
		<div class="btn-group">
			<button type="button" data-idx="{{idx}}" class="btn btn-default btn-up"><i class="fa fa-caret-square-o-up"></i></button>
			<button type="button" data-idx="{{idx}}" class="btn btn-default btn-down"><i class="fa fa-caret-square-o-down"></i></button>
			<button type="button" data-idx="{{idx}}" class="btn btn-danger btn-delete"><i class="fa fa-trash"></i></button>
		</div>
	</td>
</tr>
</script>

<script type="text/javascript">
function loadList() {
	var $list = $("#list");
	$.ajax({type:"GET", url:"/api/admin/app/keyword/all", cache:false
		, success : function(data) {
			if (data.error == 0) {
				$list.empty();
				var list = data.data;
				var listTemplate = $("#list-item").html();
				Mustache.parse(listTemplate);

				for (var i = 0; i < list.length; i++) {
					var l = list[i];
					var rendered = Mustache.render(listTemplate, {
						sort:l.sort, idx:l.idx, value:l.value
					});
					$list.append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}


$(function(){
	loadList();
	
	$("#btn-create").click(function(){
		$.ajax({type:"POST", url : "/api/admin/app/keyword/create", cache:false
			, data : $("#submitForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					$("#submitForm")[0].reset();
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});

	$(document).on('click', '.btn-delete', function(){
		var idx = $(this).data('idx');
		$.ajax({type:"POST", url : "/api/admin/app/keyword/delete", cache:false
			, data : {idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-up', function(){
		var idx = $(this).data('idx');
		$.ajax({type:"POST", url : "/api/admin/app/keyword/up", cache:false
			, data : {idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-down', function(){
		var idx = $(this).data('idx');
		$.ajax({type:"POST", url : "/api/admin/app/keyword/down", cache:false
			, data : {idx:idx}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					loadList();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
});
</script>