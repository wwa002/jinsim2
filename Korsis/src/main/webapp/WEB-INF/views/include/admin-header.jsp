<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>[${siteInfo.title}] 관리자 페이지</title>
	<link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/resources/plugins/jstree/themes/default/style.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
	
	<!-- Summernote -->
	<link href="/resources/plugins/summernote/summernote.css" rel="stylesheet">
	
	<!-- Dropzone -->
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/basic.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/dropzone.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/plugins/codemirror/ambiance.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
	<link href="/resources/css/admin.css" rel="stylesheet">
</head>

<body>
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav metismenu" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element">
							<c:if test="${admin.photo ne null and admin.photo ne '' }">
								<span>
									<img alt="image" class="img-circle nav-profile" src="/res/profile/${admin.id}">
								</span>
							</c:if>
							<a href="#"> <span class="clear"> <span class="m-t-xs">
										<strong class="font-bold">${admin.name}</strong>
								</span> <small class="text-muted text-xs">${admin.type}</small>
							</span>
							</a>
						</div>
						<div class="logo-element">${admin.name}</div>
					</li>
					<li class="<c:if test="${path eq '/admin/dashboard'}">active</c:if>">
						<a href="/admin/dashboard"><i class="fa fa-dashboard"></i> <span
							class="nav-label">Dashboards</span></a>
					</li>
					<c:choose>
						<c:when test="${siteInfo.society eq 'Y'}">
							<c:if test="${fn:contains(admin.permission, 'MEMBER')}">
								<li class="<c:if test="${fn:startsWith(path, '/admin/society/member')}">active</c:if>">
									<a href="#"><i class="fa fa-users"></i> <span class="nav-label">회원</span> <span class="fa arrow"></span></a>
									<ul class="nav nav-second-level collapse">
										<li class="<c:if test="${fn:startsWith(path, '/admin/society/member/list')}">active</c:if>">
											<a href="/admin/society/member/list"><i class="fa fa-users"></i> <span class="nav-label">회원 관리</span></a>
										</li>
										<li class="<c:if test="${fn:startsWith(path, '/admin/society/member/join')}">active</c:if>">
											<a href="/admin/society/member/join"><i class="fa fa-check-circle-o"></i> <span class="nav-label">가입 요청 회원</span>
												<c:if test="${societyCfg.count_wait > 0 }">
													<span class="label label-warning pull-right">${societyCfg.count_wait}</span>
												</c:if>
											</a>
										</li>
										<li class="<c:if test="${fn:startsWith(path, '/admin/society/member/exit')}">active</c:if>">
											<a href="/admin/society/member/exit"><i class="fa fa-ban"></i> <span class="nav-label">탈퇴 요청 회원</span>
												<c:if test="${societyCfg.count_delete > 0 }">
													<span class="label label-danger pull-right">${societyCfg.count_delete}</span>
												</c:if>
											</a>
										</li>
									</ul>
								</li>
							</c:if>
						</c:when>
						<c:otherwise>
							<li class="<c:if test="${fn:startsWith(path, '/admin/member')}">active</c:if>">
								<a href="/admin/member"><i class="fa fa-users"></i> <span class="nav-label">회원</span></a>
							</li>
						</c:otherwise>
					</c:choose>
					
					<c:if test="${siteInfo.conference eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/conference')}">active</c:if>">
							<a href="#"><i class="fa fa-graduation-cap"></i> <span class="nav-label">학술대회</span> <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<c:forEach items="${menuConference}" var="c">
									<c:set var="b_url" value="/admin/conference/${c.code}" />
									<li class="<c:if test="${fn:startsWith(path, b_url)}">active</c:if>">
										<a href="/admin/conference/${c.code}"><i class="fa fa-edit"></i> ${c.title}</a>
									</li>
								</c:forEach>
							</ul>
						</li>
					</c:if>
					<c:if test="${siteInfo.app eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/app')}">active</c:if>">
							<a href="#"><i class="fa fa-mobile-phone"></i> <span class="nav-label">APP</span> <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li class="<c:if test="${fn:startsWith(path, '/admin/app/keyword')}">active</c:if>">
									<a href="/admin/app/keyword"><i class="fa fa-ticket"></i> 키워드</a>
								</li>
								<!-- 
								<li class="<c:if test="${fn:startsWith(path, '/admin/app/push')}">active</c:if>">
									<a href="/admin/app/push"><i class="fa fa-bullhorn"></i> 푸시알림</a>
								</li>
								 -->
								<li class="<c:if test="${fn:startsWith(path, '/admin/app/notice')}">active</c:if>">
									<a href="/admin/app/notice"><i class="fa fa-thumb-tack"></i> 접속공지</a>
								</li>
							</ul>
						</li>
					</c:if>
					<li class="<c:if test="${fn:startsWith(path, '/admin/board')}">active</c:if>">
						<a href="#"><i class="fa fa-archive"></i> <span class="nav-label">게시판</span> <span class="fa arrow"></span></a>
						<ul class="nav nav-second-level collapse">
							<c:forEach items="${boards}" var="b">
								<c:set var="b_url" value="/admin/board/${b.code}" />
								<li class="<c:if test="${fn:startsWith(path, b_url)}">active</c:if>">
									<a href="/admin/board/${b.code}"><i class="fa fa-edit"></i> ${b.title}</a>
								</li>
							</c:forEach>
						</ul>
					</li>
					<c:if test="${siteInfo.society eq 'Y' and  societyCfg.workshop eq 'Y' and fn:contains(admin.permission, 'REGISTRATION')}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/society/workshop')}">active</c:if>">
							<a href="#"><i class="fa fa-mortar-board"></i> <span class="nav-label">워크샵</span><span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<c:forEach items="${workshops}" var="w">
									<c:set var="w_url" value="/admin/society/workshop/${w.code}" />
									<li class="<c:if test="${fn:startsWith(path, w_url)}">active</c:if>">
										<a href="/admin/society/workshop/${w.code}"><i class="fa fa-edit"></i> ${w.title}</a>
									</li>
								</c:forEach>
							</ul>
						</li>
					</c:if>
					<c:if test="${siteInfo.event eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/event')}">active</c:if>">
							<a href="#"><i class="fa fa-gift"></i> <span class="nav-label">이벤트</span> <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li class="<c:if test="${fn:startsWith(path, '/admin/event/manager')}">active</c:if>">
									<a href="/admin/event/manager"><i class="fa fa-edit"></i> 관리</a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/admin/event/banner')}">active</c:if>">
									<a href="/admin/event/banner"><i class="fa fa-flag-checkered"></i> 이벤트 배너</a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/admin/event/project')}">active</c:if>">
									<a href="/admin/event/project"><i class="fa fa-briefcase"></i> 프로젝트</a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/admin/event/new')}">active</c:if>">
									<a href="/admin/event/new"><i class="fa fa-upload"></i> 신규 이벤트</a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/admin/event/week')}">active</c:if>">
									<a href="/admin/event/week"><i class="fa fa-thumbs-up"></i> 주간 인기 이벤트</a>
								</li>
							</ul>
						</li>
					</c:if>
					<c:if test="${siteInfo.medicaldb eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/db/hospital') or fn:startsWith(path, '/admin/db/pharmacy') or path eq '/admin/db/map/medical'}">active</c:if>">
							<a href="#"><i class="fa fa-search"></i> <span class="nav-label">병원 검색</span> <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li class="<c:if test="${path eq '/admin/db/map/medical' or path eq '/admin/db/hospital' or path eq '/admin/db/pharmacy' or fn:startsWith(path, '/admin/db/hospital/editor') or fn:startsWith(path, '/admin/db/pharmacy/editor')}">active</c:if>">
									<a href="#"><i class="fa fa-database"></i> <span class="nav-label">DB</span> <span class="fa arrow"></span></a>
									<ul class="nav nav-third-level">
										<li class="<c:if test="${path eq '/admin/db/map/medical'}">active</c:if>">
											<a href="/admin/db/map/medical"><i class="fa fa-map-marker"></i> 지도</a>
										</li>
										<li class="<c:if test="${path eq '/admin/db/hospital' or fn:startsWith(path, '/admin/db/hospital/editor')}">active</c:if>">
											<a href="/admin/db/hospital"><i class="fa fa-user-md"></i> 병원 DB</a>
										</li>
										<li class="<c:if test="${path eq '/admin/db/pharmacy' or fn:startsWith(path, '/admin/db/pharmacy/editor')}">active</c:if>">
											<a href="/admin/db/pharmacy"><i class="fa fa-medkit"></i> 약국 DB</a>
										</li>
									</ul>
								</li>
								<c:forEach items="${menuHospitalSubjects}" var="s">
									<li class="<c:if test="${fn:startsWith(path, '/admin/db/hospital/'.concat(s.idx))}">active</c:if>">
										<a href="/admin/db/hospital/${s.idx}"><i class="fa fa-list"></i> ${s.value}</a>
									</li>
								</c:forEach>
							</ul>
						</li>
					</c:if>
					<c:if test="${siteInfo.infodb eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/db/info')}">active</c:if>">
							<a href="/admin/db/info"><i class="fa fa-cubes"></i> <span class="nav-label">정보 DB</span></a>
						</li>
					</c:if>
					<c:if test="${siteInfo.cast eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/db/cast')}">active</c:if>">
							<a href="/admin/db/cast"><i class="fa fa-rss"></i> <span class="nav-label">캐스트</span></a>
						</li>
					</c:if>
					<c:if test="${siteInfo.sms eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/sms')}">active</c:if>">
							<a href="#"><i class="fa fa-mobile"></i> <span class="nav-label">SMS</span> <span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li class="<c:if test="${fn:startsWith(path, '/admin/sms/list')}">active</c:if>">
									<a href="/admin/sms/list"><i class="fa fa-send"></i> 발송 및 내역</a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/admin/sms/contacts')}">active</c:if>">
									<a href="/admin/sms/contacts"><i class="fa fa-users"></i> 연락처</a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/admin/sms/refuse')}">active</c:if>">
									<a href="/admin/sms/refuse"><i class="fa fa-warning"></i> 수신거부</a>
								</li>
							</ul>
						</li>
					</c:if>
					<c:if test="${siteInfo.mailer eq 'Y'}">
						<c:choose>
							<c:when test="${siteInfo.society eq 'Y' and !fn:contains(admin.permission, 'MAILER') }"></c:when>
							<c:otherwise>
								<li class="<c:if test="${fn:startsWith(path, '/admin/mailer')}">active</c:if>">
									<a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">메일</span> <span class="fa arrow"></span></a>
									<ul class="nav nav-second-level collapse">
										<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/list')}">active</c:if>">
											<a href="/admin/mailer/list"><i class="fa fa-list"></i> 발송내역</a>
										</li>
										<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/send')}">active</c:if>">
											<a href="/admin/mailer/send"><i class="fa fa-send"></i> 메일발송</a>
										</li>
										<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/template')}">active</c:if>">
											<a href="/admin/mailer/template"><i class="fa fa-crop"></i> 템플릿</a>
										</li>
										<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/contacts')}">active</c:if>">
											<a href="/admin/mailer/contacts"><i class="fa fa-users"></i> 연락처</a>
										</li>
										<li class="<c:if test="${fn:startsWith(path, '/admin/mailer/refuse')}">active</c:if>">
											<a href="/admin/mailer/refuse"><i class="fa fa-warning"></i> 수신거부</a>
										</li>
									</ul>
								</li>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${siteInfo.society eq 'Y' and societyCfg.workshop eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/society/hospital')}">active</c:if>">
							<a href="/admin/society/hospital"><i class="fa fa-hospital-o"></i> <span class="nav-label">병원 검색</span></a>
						</li>
					</c:if>
					<c:if test="${siteInfo.store eq 'Y'}">
						<li class="<c:if test="${fn:startsWith(path, '/admin/store')}">active</c:if>">
							<a href="/admin/store"><i class="fa fa-building"></i> <span class="nav-label">상점</span></a>
						</li>
					</c:if>
					<c:choose>
						<c:when test="${siteInfo.society eq 'Y' and !fn:contains(admin.permission, 'STATS') }"></c:when>
						<c:otherwise>
							<li class="<c:if test="${fn:startsWith(path, '/admin/stats')}">active</c:if>">
								<a href="/admin/stats"><i class="fa fa-tasks"></i> <span class="nav-label">접속통계</span></a>
							</li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</nav>

		<div id="page-wrapper" class="gray-bg">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top" role="navigation"
					style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
							href="#"><i class="fa fa-bars"></i> </a>
					</div>
					<ul class="nav navbar-top-links navbar-right">
						<li>
							<a href="/"> <i class="fa fa-home"></i> Home</a>
						</li>
						<li>
							<a href="/logout"> <i class="fa fa-sign-out"></i> Log-out</a>
						</li>
					</ul>
				</nav>
			</div>