<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

	<!-- Mainly scripts -->
	<script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
	<script src="/resources/bootstrap/inspinia/js/bootstrap.min.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	
	<!-- Custom and plugin javascript -->
	<script src="/resources/bootstrap/inspinia/js/inspinia.js"></script>
	<script src="/resources/bootstrap/inspinia/js/plugins/pace/pace.min.js"></script>

	<!-- JSTree -->
	<script src="/resources/plugins/jstree/jstree.min.js"></script>

	<!-- iCheck -->
	<script src="/resources/bootstrap/inspinia/js/plugins/iCheck/icheck.min.js"></script>
	
	<!-- Data picker -->
	<script src="/resources/bootstrap/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>

	<!-- Clock picker -->
	<script src="/resources/bootstrap/inspinia/js/plugins/clockpicker/clockpicker.js"></script>
	
	<!-- jQuery Form -->
	<script src="/resources/jquery/jquery.form.min.js"></script>
	
	<!-- DataTable -->
	<script src="/resources/bootstrap/inspinia/js/plugins/dataTables/datatables.min.js"></script>
	
	<!-- Template -->
	<script src="/resources/plugins/mustache/mustache.min.js"></script>
	
	<!-- SUMMERNOTE -->
	<script src="/resources/plugins/summernote/summernote.js"></script>
	<script src="/resources/plugins/summernote/lang/summernote-ko-KR.min.js"></script>
	
	<!-- DROPZONE -->
	<script src="/resources/bootstrap/inspinia/js/plugins/dropzone/dropzone.js"></script>
	
	<!-- CodeMirror -->
    <script src="/resources/bootstrap/inspinia/js/plugins/codemirror/codemirror.js"></script>
    <script src="/resources/bootstrap/inspinia/js/plugins/codemirror/mode/javascript/javascript.js"></script>
	
	<!-- blueimp gallery -->
    <script src="/resources/bootstrap/inspinia/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
	
	<!-- ChartJS-->
    <script src="/resources/bootstrap/inspinia/js/plugins/chartJs/Chart.min.js"></script>
    
    <!-- Moment -->
    <script src='/resources/plugins/moment/moment.min.js'></script>
	
	<script src="/resources/js/admin.js"></script>
</body>
</html>
