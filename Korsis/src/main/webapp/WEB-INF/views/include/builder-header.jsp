<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="path" value="${requestScope['javax.servlet.forward.servlet_path']}" />
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>[DMON] WebBuilder</title>
	<link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
	<link href="/resources/plugins/jstree/themes/default/style.min.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/iCheck/custom.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/basic.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/dropzone/dropzone.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/plugins/codemirror/codemirror.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/plugins/codemirror/ambiance.css" rel="stylesheet">
	<link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">
	<link href="/resources/css/builder.css" rel="stylesheet">
</head>

<body>
	<div id="wrapper">
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav metismenu" id="side-menu">
					<li class="nav-header">
						<div class="dropdown profile-element">
							<a href="#"> <span class="clear"> <span class="m-t-xs">
										<strong class="font-bold">${builder.name}</strong>
								</span> <small class="text-muted text-xs">${builder.type}</small>
							</span>
							</a>
						</div>
						<div class="logo-element">DMON</div>
					</li>
					<li class="<c:if test="${path eq '/builder/dashboard'}">active</c:if>">
						<a href="/builder/dashboard"><i class="fa fa-dashboard"></i> <span
							class="nav-label">Dashboards</span></a>
					</li>
					<li id="site-selector-wrapper">
						<select id="site-selector" class="form-control">
							<option value="">사이트선택</option>
							<c:forEach items="${sites}" var="s">
								<option value="${s.sitecd}" <c:if test="${s.sitecd eq ms.sitecd}">selected="selected"</c:if>>${s.title}</option>
							</c:forEach>
						</select>
					</li>
					
					<c:if test="${ms.sitecd ne null and ms.sitecd ne ''}">
						<c:if test="${siteCfg.society eq 'Y'}">
							<li class="<c:if test="${fn:startsWith(path, '/builder/society')}">active</c:if>">
								<a href="#"><i class="fa fa-mortar-board"></i> <span class="nav-label">학회시스템</span><span class="fa arrow"></span></a>
								<ul class="nav nav-second-level collapse">
									<li class="<c:if test="${fn:startsWith(path, '/builder/society/workshop')}">active</c:if>">
										<a href="/builder/society/workshop"><i class="fa fa-pencil"></i> <span class="nav-label">워크샵</span></a>
									</li>
								</ul>
							</li>
						</c:if>
						<c:if test="${siteCfg.conference eq 'Y'}">
							<li class="<c:if test="${fn:startsWith(path, '/builder/conference')}">active</c:if>">
								<a href="/builder/conference"><i class="fa fa-graduation-cap"></i> <span class="nav-label">학술대회</span></a>
							</li>
						</c:if>
						<li class="<c:if test="${fn:startsWith(path, '/builder/boards')}">active</c:if>">
							<a href="/builder/boards"><i class="fa fa-pencil-square"></i> <span class="nav-label">게시판</span></a>
						</li>
						
						<li class="<c:if test="${fn:startsWith(path, '/builder/resources') or fn:startsWith(path, '/builder/templates') or fn:startsWith(path, '/builder/contents') or fn:startsWith(path, '/builder/etc')}">active</c:if>">
							<a href="#"><i class="fa fa-laptop"></i> <span class="nav-label">화면관리</span><span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li class="<c:if test="${fn:startsWith(path, '/builder/resources')}">active</c:if>">
									<a href="/builder/resources"><i class="fa fa-cloud-upload"></i> <span class="nav-label">자원</span></a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/builder/templates')}">active</c:if>">
									<a href="/builder/templates"><i class="fa fa-crop"></i> <span class="nav-label">템플릿</span></a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/builder/contents')}">active</c:if>">
									<a href="/builder/contents"><i class="fa fa-rss-square"></i> <span class="nav-label">콘텐츠</span></a>
								</li>
								<li class="<c:if test="${fn:startsWith(path, '/builder/etc')}">active</c:if>">
									<a href="/builder/etc"><i class="fa fa-file-text"></i> <span class="nav-label">기타</span></a>
								</li>
							</ul>
						</li>
						
						<li class="<c:if test="${fn:startsWith(path, '/builder/system')}">active</c:if>">
							<a href="#"><i class="fa fa-gears"></i> <span class="nav-label">시스템</span><span class="fa arrow"></span></a>
							<ul class="nav nav-second-level collapse">
								<li class="<c:if test="${path eq '/builder/system/setting'}">active</c:if>">
									<a href="/builder/system/setting"><i class="fa fa-sliders"></i> 사용설정</a>
								</li>
								<li class="<c:if test="${path eq '/builder/system/member/list'}">active</c:if>">
									<a href="/builder/system/member/list"><i class="fa fa-users"></i> 회원관리</a>
								</li>
								<li class="<c:if test="${path eq '/builder/system/member/grade'}">active</c:if>">
									<a href="/builder/system/member/grade"><i class="fa fa-sort-amount-asc"></i> 회원등급설정</a>
								</li>
								<li class="<c:if test="${path eq '/builder/system/robot'}">active</c:if>">
									<a href="/builder/system/robot"><i class="fa fa-sitemap"></i> robots.txt</a>
								</li>
							</ul>
						</li>
						
					</c:if>
				</ul>
			</div>
		</nav>

		<div id="page-wrapper" class="gray-bg">
			<div class="row border-bottom">
				<nav class="navbar navbar-static-top  " role="navigation"
					style="margin-bottom: 0">
					<div class="navbar-header">
						<a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
							href="#"><i class="fa fa-bars"></i> </a>
					</div>
					<ul class="nav navbar-top-links navbar-right">
						<li>
							<a href="/logout"> <i class="fa fa-sign-out"></i> Log-out
						</a></li>
					</ul>
				</nav>
			</div>