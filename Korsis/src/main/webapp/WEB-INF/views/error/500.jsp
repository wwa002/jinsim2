<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>500 Error</title>

    <link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>500</h1>
        <h3 class="font-bold">Internal Server Error</h3>

        <div class="error-desc">
            The server encountered something unexpected that didn't allow it to complete the request. We apologize.<br/>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
    <script src="/resources/bootstrap/inspinia/js/bootstrap.min.js"></script>

</body>

</html>
