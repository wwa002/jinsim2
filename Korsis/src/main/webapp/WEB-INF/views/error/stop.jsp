<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>STOP</title>

    <link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>STOP</h1>
        <h3 class="font-bold">사용이 중지되었습니다.</h3>

        <div class="error-desc">
        	<p>
        		죄송합니다.<br/>
            	이용이 중지된 사이트 입니다.
			</p>
			<p>
				문의하실 내용이 있으시면 아래 주소로 메일 보내주시기 바랍니다.
			</p>
			<p>
				DMON : <a href="mailto:addios4u@dmon.kr">addios4u@dmon.kr</a><br />
				진심소프트 : <a href="mailto:admin@jinsimsoft.co.kr">jinka@jinsimsoft.co.kr</a>
				
			</p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
    <script src="/resources/bootstrap/inspinia/js/bootstrap.min.js"></script>

</body>

</html>
