<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>${item.title}</title>

    <link href="/resources/bootstrap/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="/resources/bootstrap/inspinia/css/animate.css" rel="stylesheet">
    <link href="/resources/bootstrap/inspinia/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">
	<div class="wrapper wrapper-content">
		<div class="row">
			<div class="col-lg-12">
				<c:choose>
					<c:when test="${fn:endsWith(item.filename, 'pdf')}">
						<div class="row">
							<div class="col-sm-12">
								<div class="text-center pdf-toolbar">
									<div class="btn-group">
										<button id="zoomout" class="btn btn-white"><i class="fa fa-search-minus"></i></button>
										<button id="zoomin" class="btn btn-white"><i class="fa fa-search-plus"></i></button>
										<button id="zoomfit" class="btn btn-white"> 100%</button>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12 text-center">
								<canvas id="the-canvas" class="pdfcanvas border-left-right border-top-bottom"></canvas>
							</div>
						</div>
					</c:when>
					<c:when test="${fn:endsWith(item.filename, 'jpg') or fn:endsWith(item.filename, 'png') or fn:endsWith(item.filename, 'gif')}">
						<img src="/api/conference/session/download/${code}/${idx}/${item.filename}" alt="" class="img-responsive" />
					</c:when>
					<c:otherwise>
						-
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-lg-12">
				<button type="button" id="btn-question" class="btn btn-warning btn-block">질문하기</button>
			</div>
		</div>
	</div>

<div class="modal inmodal" id="vote-o" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title">투표</h4>
				<small class="font-bold">
					투표를 진행해 주세요<br />
					남은시간 : <span class="vote-second"></span>초
				</small>
			</div>
			<div class="modal-body text-center">
				<div class="row">
					<div class="col-sm-12">
						<h3 id="question-o"></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<button type="button" id="btn-vote-o-1" data-answer="1" class="btn btn-default btn-block btn-vote-o"></button>
						<button type="button" id="btn-vote-o-2" data-answer="2" class="btn btn-default btn-block btn-vote-o"></button>
						<button type="button" id="btn-vote-o-3" data-answer="3" class="btn btn-default btn-block btn-vote-o"></button>
						<button type="button" id="btn-vote-o-4" data-answer="4" class="btn btn-default btn-block btn-vote-o"></button>
						<button type="button" id="btn-vote-o-5" data-answer="5" class="btn btn-default btn-block btn-vote-o"></button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="vote-o-compleate" class="btn btn-primary" data-dismiss="modal">투표완료</button>
				<button type="button" id="vote-o-cancel" class="btn btn-white" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="vote-m" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title">투표</h4>
				<small class="font-bold">
					투표를 진행해 주세요 (다중선택 가능)<br />
					남은시간 : <span class="vote-second"></span>초
				</small>
			</div>
			<div class="modal-body text-center">
				<div class="row">
					<div class="col-sm-12">
						<h3 id="question-m"></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<button type="button" id="btn-vote-m-1" data-answer="1" class="btn btn-default btn-block btn-vote-m"></button>
						<button type="button" id="btn-vote-m-2" data-answer="2" class="btn btn-default btn-block btn-vote-m"></button>
						<button type="button" id="btn-vote-m-3" data-answer="3" class="btn btn-default btn-block btn-vote-m"></button>
						<button type="button" id="btn-vote-m-4" data-answer="4" class="btn btn-default btn-block btn-vote-m"></button>
						<button type="button" id="btn-vote-m-5" data-answer="5" class="btn btn-default btn-block btn-vote-m"></button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="vote-m-compleate" class="btn btn-primary" data-dismiss="modal">투표완료</button>
				<button type="button" id="vote-m-cancel" class="btn btn-white" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="vote-s" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title">투표</h4>
				<small class="font-bold">
					질문에 답해주세요.
				</small>
			</div>
			<div class="modal-body text-center">
				<div class="row">
					<div class="col-sm-12">
						<h3 id="question-s"></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<textarea id="answer-etc" rows="5" class="form-control" placeholder="답변을 입력해 주세요."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="vote-s-compleate" class="btn btn-primary" data-dismiss="modal">투표완료</button>
				<button type="button" id="vote-s-cancel" class="btn btn-white" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>

<div class="modal inmodal" id="modal-question" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-laptop modal-icon"></i>
				<h4 class="modal-title">질문</h4>
			</div>
			<div class="modal-body text-center">
				<div class="row">
					<div class="col-sm-12">
						<textarea id="question" rows="5" class="form-control" placeholder="질문을 입력해 주세요."></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="btn-question-complete" class="btn btn-primary" data-dismiss="modal">질문하기</button>
				<button type="button" class="btn btn-white" data-dismiss="modal">닫기</button>
			</div>
		</div>
	</div>
</div>
	
</body>

</html>

<!-- Mainly scripts -->
<script src="/resources/bootstrap/inspinia/js/jquery-2.1.1.js"></script>
<script src="/resources/bootstrap/inspinia/js/bootstrap.min.js"></script>
<script src="/resources/bootstrap/inspinia/js/plugins/pdfjs/pdf.js"></script>

<script type="text/javascript">
var filename = '${item.filename}';
var url = '/api/conference/session/download/${code}/${idx}/${item.filename}';
var pdfDoc = null,
        pageNum = 1,
        pageRendering = false,
        pageNumPending = null,
        scale = 0.5,
        zoomRange = 0.25;
var canvas;
var ctx;

function renderPage(num, scale) {
    pageRendering = true;
    pdfDoc.getPage(num).then(function(page) {
        var viewport = page.getViewport(scale);
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        var renderContext = {
            canvasContext: ctx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        renderTask.promise.then(function () {
            pageRendering = false;
            if (pageNumPending !== null) {
                renderPage(pageNumPending);
                pageNumPending = null;
            }
        });
    });
}

function queueRenderPage(num) {
    if (pageRendering) {
        pageNumPending = num;
    } else {
        renderPage(num,scale);
    }
}

function onZoomIn() {
    if (scale >= pdfDoc.scale) {
        return;
    }
    scale += zoomRange;
    var num = pageNum;
    renderPage(num, scale)
}

function onZoomOut() {
    if (scale >= pdfDoc.scale) {
        return;
    }
    scale -= zoomRange;
    var num = pageNum;
    queueRenderPage(num, scale);
}

function onZoomFit() {
    if (scale >= pdfDoc.scale) {
        return;
    }
    scale = 1;
    var num = pageNum;
    queueRenderPage(num, scale);
}

function loadData() {
	$.ajax({
		type:"GET"
		, url : "/api/conference/session/lecture"
		, cache:false
		, data : {code:'${code}', idx:'${idx}'}
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				if (filename.endsWith('pdf')) {
					if (pdfDoc) {
						var scale = pdfDoc.scale;
						if (pageNum != data.data.lecture.page) {
			            	queueRenderPage(data.data.lecture.page, scale);
			            	pageNum = data.data.lecture.page;
						}
					}
				}
				if (data.data.vote.length > 0) {
					voteRun(data.data.vote[0]);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			alert(data.responseJSON.error);
		}
	});
}

var vote;
var voteAnswer1='N', voteAnswer2='N', voteAnswer3='N', voteAnswer4='N', voteAnswer5='N';
var voteTimer;
var voteTime = 0;
function voteRun(v) {
	if (vote) {
		return;
	}
	vote = v;
	var voteAnswer1='N', voteAnswer2='N', voteAnswer3='N', voteAnswer4='N', voteAnswer5='N';
	switch (vote.type) {
	case 'O':
		$('#question-o').text(vote.question);
		$('#btn-vote-o-1').text(vote.answer1);
		$('#btn-vote-o-2').text(vote.answer2);
		$('#btn-vote-o-3').text(vote.answer3);
		$('#btn-vote-o-4').text(vote.answer4);
		$('#btn-vote-o-5').text(vote.answer5);
		$('#vote-o').modal('show');
		voteTime = 10;
		voteTimer = setInterval(voteRunner, 1000);
		break;
	case 'M':
		$('#question-m').text(vote.question);
		$('#btn-vote-m-1').text(vote.answer1);
		$('#btn-vote-m-2').text(vote.answer2);
		$('#btn-vote-m-3').text(vote.answer3);
		$('#btn-vote-m-4').text(vote.answer4);
		$('#btn-vote-m-5').text(vote.answer5);
		$('#vote-m').modal('show');
		voteTime = 10;
		voteTimer = setInterval(voteRunner, 1000);
		break;
	case 'S':
		$('#question-s').text(vote.question);
		$('#answer-etc').val('');
		$('#vote-s').modal('show');
		break;
	}
}

function voteRunner() {
	if (voteTime <= 0) {
		voteClear();
		return;
	}
	
	voteTime--;
	$('.vote-second').text(voteTime);
}

function voteClear() {
	if (voteTimer) {
		clearInterval(voteTimer);
		voteTimer = null;
	}
	vote=null;
	voteTime=0;
	voteAnswer1='N', voteAnswer2='N', voteAnswer3='N', voteAnswer4='N', voteAnswer5='N';
	$('#vote-o').modal('hide');
	$('.btn-vote-o').removeClass('btn-success');
	$('.btn-vote-o').addClass('btn-default');
	
	$('#vote-m').modal('hide');
	$('.btn-vote-m').removeClass('btn-success');
	$('.btn-vote-m').addClass('btn-default');
	
	$('#vote-s').modal('hide');
	$('#answer-etc').val('');
}

$(function(){
	if (filename.endsWith('pdf')) {
		canvas = document.getElementById('the-canvas'),
        ctx = canvas.getContext('2d');
		
		document.getElementById('zoomin').addEventListener('click', onZoomIn);
		document.getElementById('zoomout').addEventListener('click', onZoomOut);
		document.getElementById('zoomfit').addEventListener('click', onZoomFit);
		PDFJS.getDocument(url).then(function (pdfDoc_) {
		    pdfDoc = pdfDoc_;
		    var documentPagesNumber = pdfDoc.numPages;

		    $('#page_num').on('change', function() {
		        var pageNumber = Number($(this).val());

		        if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
		            queueRenderPage(pageNumber, scale);
		        }

		    });
		    renderPage(pageNum, scale);
		});
	}
	
	loadData();
	setInterval(loadData, 5000);
	
	$('.btn-vote-o').click(function(){
		voteAnswer1 = 'N'; voteAnswer2 = 'N'; voteAnswer3 = 'N'; voteAnswer4 = 'N'; voteAnswer5 = 'N';
		switch ($(this).data('answer')) {
		case 1: voteAnswer1 = 'Y'; break;
		case 2: voteAnswer2 = 'Y'; break;
		case 3: voteAnswer3 = 'Y'; break;
		case 4: voteAnswer4 = 'Y'; break;
		case 5: voteAnswer5 = 'Y'; break;
		}
		
		$('.btn-vote-o').removeClass('btn-success');
		$('.btn-vote-o').addClass('btn-default');
		
		$(this).removeClass('btn-default');
		$(this).addClass('btn-success');
	});
	
	$('.btn-vote-m').click(function(){
		switch ($(this).data('answer')) {
		case 1:
			if (voteAnswer1 == 'N') {
				voteAnswer1 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer1 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 2:
			if (voteAnswer2 == 'N') {
				voteAnswer2 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer2 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 3:
			if (voteAnswer3 == 'N') {
				voteAnswer3 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer3 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 4:
			if (voteAnswer4 == 'N') {
				voteAnswer4 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer4 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		case 5:
			if (voteAnswer5 == 'N') {
				voteAnswer5 = 'Y';
				$(this).removeClass('btn-default');
				$(this).addClass('btn-success');
			} else {
				voteAnswer5 = 'N';
				$(this).removeClass('btn-success');
				$(this).addClass('btn-default');
			}
		break;
		}
	});
	
	$('#vote-o-compleate, #vote-m-compleate, #vote-s-compleate').click(function(){
		if (vote.type == 'S') {
			if (!$('#answer-etc').val()) {
				alert('답을 입력해 주세요.');
				return false;
			}
		} else {
			if (voteAnswer1 == 'N' && voteAnswer2 == 'N' && voteAnswer3 == 'N'&& voteAnswer4 == 'N'&& voteAnswer5 == 'N') {
				alert('답을 선택해 주세요.');
				return false;
			}
		}
		$.ajax({
			type:"POST"
			, url : "/api/conference/session/vote"
			, cache:false
			, data : {code:'${code}', sidx:'${idx}', idx:vote.idx
				, answer1:voteAnswer1, answer2:voteAnswer2, answer3:voteAnswer3, answer4:voteAnswer4, answer5:voteAnswer5
				, answer_etc:$('#answer-etc').val()}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					voteClear();
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});

	$('#vote-o, #vote-m, #vote-s').on('hidden.bs.modal', function(e){
		voteClear();
	});
	
	$('#btn-question').click(function(){
		$('#modal-question').modal('show');
	});
	
	$('#btn-question-complete').click(function(){
		if (!$('#question').val()) {
			alert('질문을 입력해 주세요.');
			return false;
		}
		$.ajax({
			type:"POST"
			, url : "/api/conference/session/question"
			, cache:false
			, data : {code:'${code}', idx:'${idx}', question:$('#question').val()}
			, success : function(data) {
				if (data.error == 0) {
					alert("질문 했습니다.");
					$('#question').val('');
					$('#modal-question').modal('hide');
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>
