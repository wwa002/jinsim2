<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${item.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>화면관리</li>
			<li>콘텐츠</li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-save" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<form id="modifyForm" class="form-horizontal" onsubmit="return false;">
				<input type="hidden" name="idx" value="${idx}" />
				<input type="hidden" name="gcds" id="gcds" value="${item.gcds}" />
				<div class="row">
					<div class="col-sm-12">
						<div class="ibox">
							<div class="ibox-title">
								<h5>콘텐츠 기본정보</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">제목</label>
											<div class="col-sm-10">
												<input type="text" name="title" class="form-control" value="${item.title}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">타입</label>
											<div class="col-sm-10">
												<select name="type" class="form-control">
													<option value="SITE" <c:if test="${item.type eq 'SITE'}">selected="selected"</c:if>>Web Site</option>
													<option value="APP" <c:if test="${item.type eq 'APP'}">selected="selected"</c:if>>Hybrid APP</option>
													<option value="CONFERENCE" <c:if test="${item.type eq 'CONFERENCE'}">selected="selected"</c:if>>학술대회</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">템플릿</label>
											<div class="col-sm-10">
												<select name="tidx" class="form-control">
													<option value="">템플릿 선택</option>
													<c:forEach items="${templates}" var="t">
														<option value="${t.idx}" <c:if test="${t.idx eq item.tidx}">selected="selected"</c:if>>${t.title}</option>
													</c:forEach>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">학술대회 코드</label>
											<div class="col-sm-10">
												<input type="text" name="conference_code" class="form-control" value="${item.conference_code}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">1차 경로</label>
											<div class="col-sm-10">
												<input type="text" name="code1" class="form-control" value="${item.code1}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">2차 경로</label>
											<div class="col-sm-10">
												<input type="text" name="code2" class="form-control" value="${item.code2}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">3차 경로</label>
											<div class="col-sm-10">
												<input type="text" name="code3" class="form-control" value="${item.code3}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">회원등급</label>
											<div class="col-sm-10">
												<label class="checkbox-inline i-checks">
													<input type="checkbox" name="ismember" value="Y" <c:if test="${item.ismember eq 'Y'}">checked="checked"</c:if>> 회원전용 
												</label>
												<c:forEach items="${grades}" var="g">
													<label class="checkbox-inline i-checks">
														<input type="checkbox" value="${g.gcd}" class="gcd"> ${g.title} 
													</label>
												</c:forEach>
												<br /> <small>(* 모든 선택 해제시 비회원 접근 가능)</small>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-sm-2 control-label">사용여부</label>
											<div class="col-sm-10">
												<label class="radio-inline i-checks">
													<input type="radio" value="Y" name="isuse" <c:if test="${item.isuse eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
												</label>
												<label class="radio-inline i-checks">
													<input type="radio" value="N" name="isuse" <c:if test="${item.isuse eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
												</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>Content</h3>
										<textarea name="content" id="content">${fn:escapeXml(item.content)}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>탐색기</h5>
				</div>
				<div class="ibox-content">
					<div id="resources"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
function loadTree() {
	$('#resources').jstree({
		'core' : {
			'data' : {
				'url' : function(node) {
					return "/api/builder/resource/jstree";
				},
				'data' : function(node) {
					return {
						'id' : node.id
					};
				}
			}
		}
	});
	
	$('#resources').on("changed.jstree", function (e, data) {
		console.log(data.node);
		if (data.node.id == "0") {
		} else {
			if (data.node.data.isfolder == "Y") {
			} else {
				console.log(data.node.data);
				var path = '/res/'+data.node.data.idx+'/'+data.node.data.name;
				window.prompt("경로복사", path);
			}
		}
	});
}

function getGcds() {
	var gcd = "";
	$(".gcd").each(function(i){
		if ($(this).prop("checked")) {
			if (gcd == "") {
				gcd = $(this).val();
			} else {
				gcd += ","+$(this).val();
			}
		}
	});
	return gcd;
}

function setGcds() {
	var gcds = $("#gcds").val();
	if (gcds) {
		var gs = gcds.split(",");
		for (var i = 0; i < gs.length; i++) {
			var g = gs[i];
			$(".gcd").each(function(){
				if ($(this).val() == g) {
					$(this).iCheck("check");
				}
			});
		}
	}	
}

function save() {
	content.save();
	$("#gcds").val(getGcds());
	
	$.ajax({type:"POST", url : "/api/builder/content/modify", cache:false
		, data : $("#modifyForm").serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				alert("저장되었습니다.");
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

var content;

$(function(){
	loadTree();
	setGcds();
	
	content = CodeMirror.fromTextArea(document.getElementById("content"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	
	$("#btn-save").click(function(){
		save();
	});

	$(window).bind('keydown', function(event) {
		if (event.ctrlKey || event.metaKey) {
			switch (String.fromCharCode(event.which).toLowerCase()) {
			case 's':
				event.preventDefault();
				save();
				break;
			}
		}
	});

});
</script>