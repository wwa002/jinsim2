<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>콘텐츠</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>화면관리</li>
			<li class="active"><strong>콘텐츠</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>콘텐츠 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-2" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">제목</th>
									<th class="text-center">경로</th>
									<th class="text-center">템플릿</th>
									<th class="text-center">등록일</th>
									<th class="text-center">수정일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td><a href="/builder/contents/${l.idx}">${l.title}</a></td>
										<td>
											<c:choose>
												<c:when test="${l.type eq 'SITE'}"><c:set var="path" value="/site" /></c:when>
												<c:when test="${l.type eq 'APP'}"><c:set var="path" value="/app" /></c:when>
												<c:when test="${l.type eq 'CONFERENCE'}"><c:set var="path" value="/conference/{code}" /></c:when>
												<c:otherwise></c:otherwise>
											</c:choose>
											<c:if test="${l.code1 ne null and l.code1 ne '' }">
												<c:set var="path" value="${path}/${l.code1}" />
											</c:if>
											<c:if test="${l.code2 ne null and l.code2 ne '' }">
												<c:set var="path" value="${path}/${l.code2}" />
											</c:if>
											<c:if test="${l.code3 ne null and l.code3 ne '' }">
												<c:set var="path" value="${path}/${l.code3}" />
											</c:if>
											<c:out value="${path}" />
										</td>
										<td class="text-center">
											<c:if test="${l.tidx ne null and l.tidx ne '' }">
												<a href="/builder/templates/${l.tidx}">${l.template}</a>
											</c:if>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.regdate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="MM.dd HH:mm"/>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.moddate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="MM.dd HH:mm"/>
										</td>
										<td class="text-center">
											<c:choose>
												<c:when test="${l.ismain eq 'Y'}">
													<button type="button" class="btn btn-xs btn-primary"><i class="fa fa-check-square"></i></button>
												</c:when>
												<c:otherwise>
													<button type="button" class="btn btn-xs btn-default btn-main" data-idx="${l.idx}"><i class="fa fa-check-square"></i></button>
												</c:otherwise>
											</c:choose>
											<button type="button" class="btn btn-xs btn-warning btn-copy" data-idx="${l.idx}"><i class="fa fa-copy"></i></button>
											<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="${l.idx}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox float-e-margins" id="siteForm-wrapper">
				<div class="ibox-title">
					<h5>콘텐츠 생성</h5>
				</div>
				<div class="ibox-content">
					<form id="contentForm" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">제목</label>
							<div class="col-sm-8">
								<input type="text" name="title" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="content-create" class="btn btn-primary">생성</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#content-create").click(function(){
		if (confirm("입력하신 정보로 콘텐츠를 생성하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/content/create", cache:false
				, data : $("#contentForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-main").click(function(){
		if (confirm("선택하신 콘텐츠를 메인화면으로 지정하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url : "/api/builder/content/main", cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-delete").click(function(){
		if (confirm("선택하신 콘텐츠를 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url : "/api/builder/content/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-copy").click(function(){
		if (confirm("선택하신 콘텐츠를 복제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url : "/api/builder/content/copy", cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>