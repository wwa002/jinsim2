<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>회원등급설정</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>시스템</li>
			<li class="active"><strong>회원등급설정</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>등급 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-2" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">등급코드</th>
									<th class="text-center">등급명</th>
									<th class="text-center">등록일</th>
									<th class="text-center">수정일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td class="text-center">${l.gcd}</td>
										<td class="text-center">${l.title}</td>
										<td class="text-center">
											<fmt:parseDate value="${l.regdate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="yyyy.MM.dd HH:mm:ss"/>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.moddate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="yyyy.MM.dd HH:mm:ss"/>
										</td>
										<td class="text-center">
											<button type="button" class="btn btn-xs btn-warning btn-modify" data-gcd="${l.gcd}" data-title="${l.title}"><i class="fa fa-edit"></i></button>
											<button type="button" class="btn btn-xs btn-danger btn-delete" data-gcd="${l.gcd}" data-title="${l.title}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox" id="grade-form-add">
				<div class="ibox-title">
					<h5>등급추가</h5>
				</div>
				<div class="ibox-content">
					<form id="addForm" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">등급코드</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" name="gcd" id="gcd" placeholder="영문자 10자 내" maxlength="10" class="form-control" />
									<span class="input-group-btn">
										<button type="button" id="grade-check" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">등급명</label>
							<div class="col-sm-8">
								<input type="text" name="title" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="grade-create" class="btn btn-primary">생성</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		
			<div class="ibox" id="grade-form-modify" style="display:none;">
				<div class="ibox-title">
					<h5>등급수정</h5>
				</div>
				<div class="ibox-content">
					<form id="modifyForm" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="gcd" id="gcd-modify" />
						<div class="form-group">
							<label class="col-sm-4 control-label">등급명</label>
							<div class="col-sm-8">
								<input type="text" name="title" id="title-modify" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="grade-modify" class="btn btn-primary">수정</button>
								<button type="reset" id="grade-modify-cancel" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
$(function(){
	
	$("#grade-check").click(function(){
		var gcd = $("#gcd").val();
		$.ajax({type:"POST", url : "/api/builder/check/gcd", cache:false
			, data : {gcd:gcd}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("사용할 수 있는 코드 입니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#grade-create").click(function(){
		if (confirm("입력하신 정보로 생성하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/system/member/grade/add", cache:false
				, data : $("#addForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$("#grade-modify").click(function(){
		if (confirm("입력하신 정보로 수정하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/system/member/grade/modify", cache:false
				, data : $("#modifyForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-delete").click(function(){
		if (confirm("선택하신 등급을 삭제하시겠습니까?")) {
			var gcd = $(this).data("gcd");
			$.ajax({type:"POST", url : "/api/builder/system/member/grade/delete", cache:false
				, data : {gcd:gcd}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-modify").click(function(){
		$("#gcd-modify").val($(this).data("gcd"));
		$("#title-modify").val($(this).data("title"));
		$("#grade-form-add").hide();
		$("#grade-form-modify").show();
	});
	
	$("#grade-modify-cancel").click(function(){
		$("#grade-form-add").show();
		$("#grade-form-modify").hide();
	});
	
	
});
</script>