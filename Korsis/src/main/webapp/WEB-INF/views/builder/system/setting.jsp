<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>사용설정</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>시스템</li>
			<li class="active"><strong>사용설정</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-8">
			<h2>기본 시스템</h2>
			<div class="row">
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 회원관리</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 게시판</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 콘텐츠</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 접속통계</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr />
			<h2>확장 시스템</h2>
			<div class="row">
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 학회시스템</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.society eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-society" data-cfg="society"> 
									<label class="onoffswitch-label" for="cfg-society"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 학술대회</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.conference eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-conference" data-cfg="conference"> 
									<label class="onoffswitch-label" for="cfg-conference"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> SMS</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.sms eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-sms" data-cfg="sms"> 
									<label class="onoffswitch-label" for="cfg-sms"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 메일발송</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.mailer eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-mailer" data-cfg="mailer"> 
									<label class="onoffswitch-label" for="cfg-mailer"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> APP</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.app eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-app" data-cfg="app"> 
									<label class="onoffswitch-label" for="cfg-app"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> Shop</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.shop eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-shop" data-cfg="shop"> 
									<label class="onoffswitch-label" for="cfg-shop"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 상점 시스템</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.store eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-store" data-cfg="store"> 
									<label class="onoffswitch-label" for="cfg-store"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 의료DB 시스템</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.medicaldb eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-medicaldb" data-cfg="medicaldb"> 
									<label class="onoffswitch-label" for="cfg-medicaldb"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 이벤트 시스템</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.event eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-event" data-cfg="event"> 
									<label class="onoffswitch-label" for="cfg-event"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 배너 시스템</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.banner eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-banner" data-cfg="banner"> 
									<label class="onoffswitch-label" for="cfg-banner"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 정보DB 시스템</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.infodb eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-infodb" data-cfg="infodb"> 
									<label class="onoffswitch-label" for="cfg-infodb"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-2">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 캐스트</a>
								<div class="onoffswitch">
									<input type="checkbox" <c:if test="${siteCfg.cast eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox cfg" id="cfg-cast" data-cfg="cast"> 
									<label class="onoffswitch-label" for="cfg-cast"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<c:if test="${siteCfg.app eq 'Y' }">
				<hr />
				<h2>APP</h2>
				<div class="row">
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> Beacon</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${appCfg.beacon eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox appCfg" id="appCfg-beacon" data-cfg="beacon"> 
										<label class="onoffswitch-label" for="appCfg-beacon"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> Chat</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${appCfg.chat eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox appCfg" id="appCfg-chat" data-cfg="chat"> 
										<label class="onoffswitch-label" for="appCfg-chat"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${siteCfg.society eq 'Y' }">
				<hr />
				<h2>학회 시스템</h2>
				<div class="row">
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 회비</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${societyCfg.fee eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox societyCfg" id="societyCfg-fee" data-cfg="fee"> 
										<label class="onoffswitch-label" for="societyCfg-fee"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 평점</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${societyCfg.average eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox societyCfg" id="societyCfg-average" data-cfg="average"> 
										<label class="onoffswitch-label" for="societyCfg-average"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 통계</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${societyCfg.stats eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox societyCfg" id="societyCfg-stats" data-cfg="stats"> 
										<label class="onoffswitch-label" for="societyCfg-stats"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 위원회</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${societyCfg.committee eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox societyCfg" id="societyCfg-committee" data-cfg="committee"> 
										<label class="onoffswitch-label" for="societyCfg-committee"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 워크샵</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${societyCfg.workshop eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox societyCfg" id="societyCfg-workshop" data-cfg="workshop"> 
										<label class="onoffswitch-label" for="societyCfg-workshop"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 병원검색</a>
									<div class="onoffswitch">
										<input type="checkbox" <c:if test="${societyCfg.hospital eq 'Y'}"> checked="checked"</c:if> class="onoffswitch-checkbox societyCfg" id="societyCfg-hospital" data-cfg="hospital"> 
										<label class="onoffswitch-label" for="societyCfg-hospital"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</c:if>
		</div>
		<div class="col-lg-4">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>도메인</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12" id="url-list">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<form id="domainForm" class="form-horizontal" onsubmit="return false;">
								<div class="form-group">
									<div class="col-sm-12">
										<div class="input-group">
											<input type="text" name="url" id="url" class="form-control" />
											<span class="input-group-btn">
												<button type="button" id="btn-url-add" class="btn btn-primary"><i class="fa fa-plus-square"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<c:if test="${siteCfg.sms eq 'Y'}">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>SMS</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								<form id="smsForm" class="form-horizontal" onsubmit="return false;">
									<div class="form-group">
										<label class="col-sm-3 control-label">발신자 번호</label>
										<div class="col-sm-9">
											<input type="text" name="sender" class="form-control" value="${smsCfg.sender}" placeholder="발신자 번호 (ex. 02-0000-0000)" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">업체</label>
										<div class="col-sm-9">
											<select name="type" id="sms-type" class="form-control">
												<option value="CAFE24" <c:if test="${smsCfg.type eq 'CAFE24'}">selected="selected"</c:if>>카페24</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">인증키 1</label>
										<div class="col-sm-9">
											<input type="text" name="auth1" class="form-control" value="${smsCfg.auth1}" placeholder="인증키 1 (Cafe24 UserID)" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">인증키 2</label>
										<div class="col-sm-9">
											<input type="text" name="auth2" class="form-control" value="${smsCfg.auth2}" placeholder="인증키 2 (Cafe24 Secure Key)" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">인증키 3</label>
										<div class="col-sm-9">
											<input type="text" name="auth3" class="form-control" value="${smsCfg.auth3}" placeholder="인증키 3" />
										</div>
									</div>
									<div>
										<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><i class="fa fa-save"></i> 저장</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			
			<c:if test="${siteCfg.app eq 'Y'}">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>APP</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								<form id="appForm" class="form-horizontal" onsubmit="return false;">
									<div class="form-group">
										<label class="col-sm-3 control-label">Firebase</label>
										<div class="col-sm-9">
											<input type="text" name="firebase" class="form-control" value="${appCfg.firebase}" placeholder="Firebase Server Key" />
										</div>
									</div>
									<div>
										<button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><i class="fa fa-save"></i> 저장</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script id="url-item" type="x-tmpl-mustache">
<div class="row">
	<div class="col-sm-12">
		<form name="urlModifyForm_{{idx}}" id="urlModifyForm_{{idx}}" class="form-horizontal" onsubmit="return false;">
			<input type="hidden" name="idx" value="{{idx}}" />
			<div class="form-group">
				<label class="col-lg-3 control-label">{{no}}. URL</label>
				<div class="col-lg-9">
					<div class="input-group">
						<input type="text" name="url" placeholder="url" class="form-control" required="required" value="{{url}}" />
						<span class="input-group-btn">
							<button type="button" class="btn btn-warning btn-url-modify" data-idx="{{idx}}"><i class="fa fa-edit"></i></button>
							<button type="button" class="btn btn-danger btn-url-delete" data-idx="{{idx}}"><i class="fa fa-trash"></i></button>
							{{#main}}
								<button type="button" class="btn btn-primary btn-url-main" data-idx="{{idx}}"><i class="fa fa-check-square"></i></button>
							{{/main}}
							{{^main}}
								<button type="button" class="btn btn-default btn-url-main" data-idx="{{idx}}"><i class="fa fa-check-square-o"></i></button>
							{{/main}}
						</span>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</script>

<script type="text/javascript">
function loadUrl() {
	var template = $("#url-item").html();
	Mustache.parse(template);
	
	$.ajax({type:"GET", url : "/api/builder/system/url/list", cache:false
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				$("#url-list").empty();
				for (var i=0 ; i < data.data.length; i++) {
					var d = data.data[i];
					var rendered = Mustache.render(template, {no:i+1, idx:d.idx, url:d.url, main:d.ismain=="Y"});
					$("#url-list").append(rendered);
				}
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}
$(function(){
	loadUrl();
	
	$("#btn-url-add").click(function(){
		if (confirm("입력하신 URL을 등록하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/system/url/add", cache:false
				, data : $("#domainForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						loadUrl();
						$("#url").val("");
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-url-modify', function(){
		if (confirm("입력하신 URL로 수정하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({
				type:"POST"
				, url:"/api/builder/system/url/modify"
				, cache:false
				, data:$("#urlModifyForm_"+idx).serialize()
				, success:function(data){
					console.log(data);
					if (data.error == 0) {
						loadUrl();
					} else {
						alert(data.message);
					}
				}
				, error:function(data){
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	$(document).on('click', '.btn-url-delete', function(){
		if (confirm("선택하신 URL을 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({
				type:"POST"
				, url:"/api/builder/system/url/delete"
				, cache:false
				, data:$("#urlModifyForm_"+idx).serialize()
				, success:function(data){
					console.log(data);
					if (data.error == 0) {
						loadUrl();
					} else {
						alert(data.message);
					}
				}
				, error:function(data){
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-url-main', function(){
		if (confirm("선택하신 URL을 메인 URL로 지정하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({
				type:"POST"
				, url:"/api/builder/system/url/main"
				, cache:false
				, data:$("#urlModifyForm_"+idx).serialize()
				, success:function(data){
					console.log(data);
					if (data.error == 0) {
						loadUrl();
					} else {
						alert(data.message);
					}
				}
				, error:function(data){
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	
	$(".cfg").click(function() {
		var cfg = $(this).data("cfg");
		var status = $(this).prop("checked") ? "Y" : "N";
		$.ajax({
			type:"POST"
			, url:"/api/builder/system/cfg"
			, cache:false
			, data:{cfg:cfg, status:status}
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
				document.location.reload();
			}
		});
	});
	
	$(".appCfg").click(function() {
		var cfg = $(this).data("cfg");
		var status = $(this).prop("checked") ? "Y" : "N";
		$.ajax({
			type:"POST"
			, url:"/api/builder/system/cfg/app"
			, cache:false
			, data:{cfg:cfg, status:status}
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
				document.location.reload();
			}
		});
	});
	
	$(".societyCfg").click(function() {
		var cfg = $(this).data("cfg");
		var status = $(this).prop("checked") ? "Y" : "N";
		$.ajax({
			type:"POST"
			, url:"/api/builder/system/cfg/society"
			, cache:false
			, data:{cfg:cfg, status:status}
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
				document.location.reload();
			}
		});
	});
	
	$("#smsForm").submit(function(){
		$.ajax({
			type:"POST"
			, url:"/api/builder/system/sms/cfg"
			, cache:false
			, data:$("#smsForm").serialize()
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#appForm").submit(function(){
		$.ajax({
			type:"POST"
			, url:"/api/builder/system/app/cfg"
			, cache:false
			, data:$("#appForm").serialize()
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					document.location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>