<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>회원관리</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>시스템</li>
			<li class="active"><strong>회원관리</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-9">
			<div class="ibox">
				<div class="ibox-title">
					<h5>회원 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-2 col-sm-offset-4">
										<select name="type" class="form-control">
											<option value="">회원종류</option>
											<option value="MEMBER" <c:if test="${param.type eq 'MEMBER' }"> selected="selected"</c:if>>일반회원</option>
											<option value="ADMIN" <c:if test="${param.type eq 'ADMIN' }"> selected="selected"</c:if>>관리자</option>
										</select>
									</div>
									<div class="col-sm-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-1" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">사진</th>
									<th class="text-center">이름 (별명)</th>
									<th class="text-center">회원종류</th>
									<th class="text-center">등록일</th>
									<th class="text-center">수정일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td class="text-center">
											<c:choose>
												<c:when test="${l.photo ne null and l.photo ne ''}">
													<img src="/builder/resources/profile/${l.id}" class="user-list-image" alt="" />
												</c:when>
												<c:otherwise>
													<img src="/resources/images/user.png" class="user-list-image" alt="" />
												</c:otherwise>
											</c:choose>
										</td>
										<td class="text-center">
											${l.name}
											<c:if test="${l.nickname ne null and l.nickname ne '' }">
												(${l.nickname})
											</c:if> 
										</td>
										<td class="text-center">
											<c:choose>
												<c:when test="${l.type eq 'ADMIN'}">
													<span class="label label-danger">관리자</span>
												</c:when>
												<c:otherwise>
													<span class="label label-info">일반회원</span>
												</c:otherwise>
											</c:choose>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.regdate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="yyyy.MM.dd HH:mm:ss"/>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.moddate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="yyyy.MM.dd HH:mm:ss"/>
										</td>
										<td class="text-center">
											<button type="button" class="btn btn-xs btn-warning btn-modify" data-idx="${l.idx}" data-type="${l.type}" data-name="${l.name}" data-nickname="${l.nickname}" data-email="${l.email}" data-birth="${l.birth}" data-sex="${l.sex}" data-isuse="${l.isuse}"><i class="fa fa-edit"></i></button>
											<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="${l.idx}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&type=${param.type}&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&type=${param.type}&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&type=${param.type}&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&type=${param.type}&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="ibox" id="member-form-add">
				<div class="ibox-title">
					<h5>회원추가</h5>
				</div>
				<div class="ibox-content">
					<form id="memberForm" class="form-horizontal" enctype="multipart/form-data" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">아이디</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" name="id" id="id" maxlength="10" class="form-control" />
									<span class="input-group-btn">
										<button type="button" id="id-check" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호</label>
							<div class="col-sm-8">
								<input type="password" name="passwd" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">회원타입</label>
							<div class="col-sm-8">
								<select name="type" class="form-control">
									<option value="MEMBER">일반회원</option>
									<option value="ADMIN">관리자</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이름</label>
							<div class="col-sm-4">
								<input type="text" name="name_last" placeholder="성" class="form-control" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="name_first" placeholder="이름" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">별명</label>
							<div class="col-sm-8">
								<input type="text" name="nickname" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이메일</label>
							<div class="col-sm-8">
								<input type="text" name="email" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">생일</label>
							<div class="col-sm-8">
								<input type="text" name="birth" class="form-control datepicker" maxlength="10" placeholder="yyyy-MM-dd" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">성별</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="M" name="sex" checked="checked"> <i></i> 남성 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="F" name="sex"> <i></i> 여성 
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">프로필</label>
							<div class="col-sm-8">
								<input type="file" name="file" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="isuse" checked="checked"> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="isuse"> <i></i> 차단
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="member-save" class="btn btn-primary">회원등록</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			
			<div class="ibox" id="member-form-modify" style="display:none;">
				<div class="ibox-title">
					<h5>회원수정</h5>
				</div>
				<div class="ibox-content">
					<form id="memberModifyForm" class="form-horizontal" enctype="multipart/form-data" onsubmit="return false;">
						<input type="hidden" name="idx" id="idx-modify" />
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호</label>
							<div class="col-sm-8">
								<input type="password" name="passwd" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">회원타입</label>
							<div class="col-sm-8">
								<select name="type" id="type-modify" class="form-control">
									<option value="MEMBER">일반회원</option>
									<option value="ADMIN">관리자</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이름</label>
							<div class="col-sm-4">
								<input type="text" name="name_last" id="name_last-modify" placeholder="성" class="form-control" />
							</div>
							<div class="col-sm-4">
								<input type="text" name="name_first" id="name_first-modify" placeholder="이름" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">별명</label>
							<div class="col-sm-8">
								<input type="text" name="nickname" id="nickname-modify" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이메일</label>
							<div class="col-sm-8">
								<input type="text" name="email" id="email-modify" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">생일</label>
							<div class="col-sm-8">
								<input type="text" name="birth" id="birth-modify" class="form-control datepicker" maxlength="10" placeholder="yyyy-MM-dd" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">성별</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="M" name="sex" id="sex-m-modify"> <i></i> 남성 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="F" name="sex" id="sex-f-modify"> <i></i> 여성 
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">프로필</label>
							<div class="col-sm-8">
								<input type="file" name="file" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="isuse" id="isuse-y-modify"> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="isuse" id="isuse-n-modify"> <i></i> 차단
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="member-modify" class="btn btn-primary">정보수정</button>
								<button type="reset" id="member-modify-cancel" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#id-check").click(function(){
		var id = $("#id").val();
		$.ajax({type:"POST", url : "/api/builder/check/id", cache:false
			, data : {id:id}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("사용할 수 있는 아이디 입니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#member-save").click(function(){
		$("#memberForm").ajaxForm({
			type:"POST"
			, url:"/api/builder/system/member/save"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#memberForm").submit();
	});
	
	$("#member-modify").click(function(){
		$("#memberModifyForm").ajaxForm({
			type:"POST"
			, url:"/api/builder/system/member/save"
			, enctype:"multipart/form-data"
			, success:function(data){
				console.log(data);
				if (data.error == 0) {
					location.reload();
				} else {
					alert(data.message);
				}
			}
			, error:function(data){
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
		$("#memberModifyForm").submit();
	});
	
	$(".btn-modify").click(function(){
		$("#idx-modify").val($(this).data("idx"));
		$("#type-modify").val($(this).data("type"));
		var name = $(this).data("name");
		if (name) {
			$("#name_last-modify").val(name.substring(0, 1));
			$("#name_first-modify").val(name.substring(1));
		}
		$("#nickname-modify").val($(this).data("nickname"));
		$("#email-modify").val($(this).data("email"));
		var birth = $(this).data("birth")+"";
		if (birth) {
			var text = birth.substring(0, 4);
			text += "-" + birth.substring(4, 6);
			text += "-" + birth.substring(6, 8);
			$("#birth-modify").val(text);
		}
		if ($(this).data("sex") == "F") {
			$("#sex-f-modify").iCheck("check");
		} else {
			$("#sex-m-modify").iCheck("check");
		}
		if ($(this).data("isuse") == "Y") {
			$("#isuse-y-modify").iCheck("check");
		} else {
			$("#isuse-n-modify").iCheck("check");
		}
		
		$("#member-form-add").hide();
		$("#member-form-modify").show();
	});
	
	$("#member-modify-cancel").click(function(){
		$("#member-form-add").show();
		$("#member-form-modify").hide();
	});
	
	$(".btn-delete").click(function(){
		if (confirm("선택하신 회원을 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url:"/api/builder/system/member/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
});
</script>
