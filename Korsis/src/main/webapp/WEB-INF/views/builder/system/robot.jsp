<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>robots.txt 설정</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>시스템</li>
			<li class="active"><strong>robots.txt</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>robots.txt</h5>
				</div>
				<div class="ibox-content">
					<form name="robotForm" id="robotForm" class="form-horizontal">
						<div class="form-group">
							<div class="col-sm-12">
								<textarea rows="20" name="robot" class="form-control">${fn:escapeXml(siteCfg.robot)}</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 text-right">
								<button type="submit" class="btn btn-primary">저장</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#robotForm").submit(function(){
		if (confirm("입력하신 내용을 저장하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/system/robot", cache:false
				, data : $("#robotForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						alert('저장되었습니다.');
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
		
		return false;
	});
});
</script>