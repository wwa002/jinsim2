<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>회원관리설정</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>시스템</li>
			<li class="active"><strong>회원관리설정</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-6">
			<h2>기본 시스템</h2>
			<div class="row">
				<div class="col-md-3">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 이름</a>
								<div class="onoffswitch">
									<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-name" data-cfg="name"> 
									<label class="onoffswitch-label" for="cfg-name"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 별명</a>
								<div class="onoffswitch">
									<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-nickname" data-cfg="nickname"> 
									<label class="onoffswitch-label" for="cfg-nickname"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 이메일</a>
								<div class="onoffswitch">
									<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-email" data-cfg="email"> 
									<label class="onoffswitch-label" for="cfg-email"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 생일</a>
								<div class="onoffswitch">
									<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-birth" data-cfg="birth"> 
									<label class="onoffswitch-label" for="cfg-birth"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-3">
					<div class="ibox">
						<div class="ibox-content product-box">
							<div class="product-desc">
								<a href="javascript:void(0);" class="product-name"> 성별</a>
								<div class="onoffswitch">
									<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-sex" data-cfg="sex"> 
									<label class="onoffswitch-label" for="cfg-sex"> 
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<c:if test="${site.type eq 'SOCIETY' }">
				<hr />
				<h2>학회 전용 시스템</h2>
				
				<div class="row">
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 의사면허번호</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-license_doctor" data-cfg="license_doctor"> 
										<label class="onoffswitch-label" for="cfg-license_doctor"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 전문의번호</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-license_expert" data-cfg="license_expert"> 
										<label class="onoffswitch-label" for="cfg-license_expert"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 영어이름</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-name_en" data-cfg="name_en"> 
										<label class="onoffswitch-label" for="cfg-name_en"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 주민등록번호</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-personal_number" data-cfg="personal_number"> 
										<label class="onoffswitch-label" for="cfg-personal_number"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 국가코드</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-country" data-cfg="country"> 
										<label class="onoffswitch-label" for="cfg-country"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 주소1</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-address" data-cfg="address"> 
										<label class="onoffswitch-label" for="cfg-address"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 주소2</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-address2" data-cfg="address2"> 
										<label class="onoffswitch-label" for="cfg-address2"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 연락처1</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-phone1" data-cfg="phone1"> 
										<label class="onoffswitch-label" for="cfg-phone1"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 연락처2</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-phone2" data-cfg="phone2"> 
										<label class="onoffswitch-label" for="cfg-phone2"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 연락처3</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-phone3" data-cfg="phone3"> 
										<label class="onoffswitch-label" for="cfg-phone3"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="ibox">
							<div class="ibox-content product-box">
								<div class="product-desc">
									<a href="javascript:void(0);" class="product-name"> 이메일2</a>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox cfg" id="cfg-email2" data-cfg="email2"> 
										<label class="onoffswitch-label" for="cfg-email2"> 
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>
			</c:if>
		</div>
		<div class="col-sm-6">
			<div class="ibox float-e-margins" id="siteForm-wrapper">
				<div class="ibox-title">
					<h5>가입폼 <small>(샘플)</small></h5>
				</div>
				<div class="ibox-content">
					<form class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">아이디</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" placeholder="아이디를 입력해 주세요." readonly="readonly" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호</label>
							<div class="col-sm-8">
								<input type="password" placeholder="비밀번호를 입력해 주세요." readonly="readonly" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">비밀번호 확인</label>
							<div class="col-sm-8">
								<input type="password" placeholder="비밀번호를 다시한번 입력해 주세요." readonly="readonly" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">별명</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" placeholder="별명을 입력해 주세요." readonly="readonly" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">이메일</label>
							<div class="col-sm-8">
								<input type="text" placeholder="이메일을 입력해 주세요." readonly="readonly" class="form-control" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>