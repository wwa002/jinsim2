<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>자원</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>화면관리</li>
			<li class="active"><strong>자원</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-4">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>탐색기 <small id="folder-path">(ROOT)</small></h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="folderForm" class="form-horizontal" onsubmit="return false;">
								<input type="hidden" name="parent" id="parent-folder" class="form-control" />
								<div class="form-group">
									<div class="col-sm-12">
										<div class="input-group">
											<input type="text" name="folder" id="folder" placeholder="폴더생성" class="form-control" />
											<span class="input-group-btn">
												<button type="button" id="btn-folder-add" class="btn btn-primary"><i class="fa fa-plus-square"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div id="resources"></div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="col-sm-8">
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>파일목록</h5>
						</div>
						<div class="ibox-content">
							<div class="table-responsive">
								<table id="file-list" class="table table-striped table-bordered table-hover">
									<colgroup>
										<col class="col-sm-4">
										<col class="col-sm-4">
										<col class="col-sm-1">
										<col class="col-sm-1">
										<col class="col-sm-2">
									</colgroup>
									<thead>
										<th class="text-center">이름</th>
										<th class="text-center">경로</th>
										<th class="text-center">종류</th>
										<th class="text-center">크기</th>
										<th class="text-center">관리</th>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>업로드 <small id="upload-path">(ROOT)</small></h5>
						</div>
						<div class="ibox-content">
							<form id="my-awesome-dropzone" action="/api/builder/resource/upload/dropzone?idx=0" class="dropzone" action="#">
								<div class="dropzone-previews"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
var table;
function loadTree() {
	$('#resources').jstree({
		'core' : {
			'data' : {
				'url' : function(node) {
					return "/api/builder/resource/jstree";
				},
				'data' : function(node) {
					return {
						'id' : node.id
					};
				}
			}
		}
	});
	
	$('#resources').on("changed.jstree", function (e, data) {
		console.log(data.node);
		if (data.node.id == "0") {
			$("#parent-folder").val(data.node.id);
			$("#folder-path").text("(ROOT)");
			$("#my-awesome-dropzone").attr("action", "/api/builder/resource/upload/dropzone?idx=0");
			$("#upload-path").text("(ROOT)");
			loadFolder(data.node.id);
		} else {
			if (data.node.data.isfolder == "Y") {
				$("#parent-folder").val(data.node.id);
				$("#folder-path").text("("+data.node.data.name+")");
				$("#my-awesome-dropzone").attr("action", "/api/builder/resource/upload/dropzone?idx=" + data.node.id);
				$("#upload-path").text("("+data.node.data.name+")");
				loadFolder(data.node.id);
			} else {
				window.open('/builder/resources/download/'+data.node.id+'/'+data.node.data.name);
			}
		}
	});
}
function reloadTree() {
	$('#resources').jstree("destroy");
	loadTree();
}

function loadFolder(idx) {
	$("#file-list").dataTable().fnDestroy();
	$("#file-list").dataTable({
			"ordering": false
			, "info":false
			, "bLengthChange": false
			, "searching": false
			, "language" : {
	            "paginate" : {"previous" : "<", "next" : ">"}
	        },
		columns : [
			{data:"name", render : function(data, type, row){
				var html = '<a href="javascript:void(0);" data-idx="'+row.idx+'" data-name="'+data+'" data-isfolder="'+row.isfolder+'" class="btn-item">';
				if (row.isfolder == "Y") {
					html += '<i class="fa fa-folder-o"></i> '+data; 
				} else {
					html += data;
				}
				return html;
			}}
			, {data:"isfolder", render : function(data, type, row){
				if (data == "Y") {
					return '-';
				} else {
					return '/res/'+row.idx+'/'+row.name;
				}
			}}
			, {data:"isfolder", "class":"text-center", render : function(data, type, row){
				if (data == "Y") {
					return "폴더";
				} else {
					return row.mime;
				}
			}}
			, {data:"size", "class":"text-center", render : function(data, type, row){
				if (data == 0) {
					return "-";
				} else {
					return formatSizeUnits(data);
				}
			}}
			, {data:"idx", "class":"text-center", render : function(data, type, row){
				var html = '';
				if (row.isfolder != "Y") {
					html += '<button type="button" class="btn btn-info btn-xs btn-clipboard" data-path="'+('/res/'+row.idx+'/'+row.name)+'"><i class="fa fa-clipboard"></i></button> ';
				}
				
				html += '<button type="button" class="btn btn-danger btn-xs btn-delete" data-idx="'+data+'"><i class="fa fa-trash"></i></button>';
				return html;
            }}
		]
		, ajax : {
			url : "/api/builder/resource/folder/list"
			, type : "GET"
			, data:{idx:idx}
			, dataSrc:function(json) {
				console.log(json);
				return json.data;
			}
		}
	});
}

$(function(){
	Dropzone.options.myAwesomeDropzone = {
		init: function () {
			this.on("complete", function (data) {
				var res = JSON.parse(data.xhr.responseText);
				if (res.status == "success") {
					reloadTree();
					loadFolder($("#parent-folder").val());
					this.removeFile(data);
				}
			});
			this.on("processing", function(file){
				this.options.url = $("#my-awesome-dropzone").attr("action");
			});
		}
	};
	
	loadTree();
	loadFolder(0);
	
	$("#btn-folder-add").click(function() {
		$.ajax({
			type : "POST",
			url : "/api/builder/resource/folder/add",
			cache : false,
			data : $("#folderForm").serialize(),
			success : function(data) {
				console.log(data);
				if (data.error == 0) {
					reloadTree();
					$("#folder").val("");
				} else {
					alert(data.message);
				}
			},
			error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$(document).on('click', '.btn-item', function(){
		var name = $(this).data("name");
		var idx = $(this).data("idx");
		if ("Y" == $(this).data("isfolder")) {
			loadFolder($(this).data("idx"));
			$("#parent-folder").val(idx);
			$("#folder-path").text("("+name+")");
			$("#my-awesome-dropzone").attr("action", "/api/builder/resource/upload/dropzone?idx=" + idx);
			$("#upload-path").text("("+name+")");
		} else {
			window.open('/builder/resources/download/'+idx+'/'+name);
		}
	});
	
	$(document).on('click', '.btn-delete', function(){
		var idx= $(this).data("idx");
		if (confirm("삭제된 데이터는 복원 할 수 없습니다.\n삭제하시겠습니까?")) {
			$.ajax({
				type : "GET",
				url : "/api/builder/resource/delete",
				cache : false,
				data : {idx:idx},
				success : function(data) {
					console.log(data);
					if (data.error == 0) {
						reloadTree();
						loadFolder($("#parent-folder").val());
					} else {
						alert(data.message);
					}
				},
				error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(document).on('click', '.btn-clipboard', function(){
		window.prompt("경로복사", $(this).data('path'));
	});

});
</script>


