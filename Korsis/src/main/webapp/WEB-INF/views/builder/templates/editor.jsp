<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${item.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>화면관리</li>
			<li><strong>템플릿</strong></li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-save" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<form id="modifyForm" class="form-horizontal" onsubmit="return false;">
				<input type="hidden" name="idx" value="${idx}" />
				<div class="row">
					<div class="col-sm-12">
						<div class="ibox">
							<div class="ibox-title">
								<h5>템플릿 기본 정보</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<div class="col-sm-12">
												<input type="text" name="title" class="form-control" value="${item.title}" />
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>Header</h3>
										<textarea name="header" id="header">${fn:escapeXml(item.header)}</textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>Footer</h3>
										<textarea name="footer" id="footer">${fn:escapeXml(item.footer)}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="ibox">
							<div class="ibox-title">
								<h5>게시판 지원 <small>(게시판 템플릿으로 사용 할 경우)</small></h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-12">
										<h3>목록</h3>
										<textarea name="board_list" id="board_list">${fn:escapeXml(item.board_list)}</textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>열람</h3>
										<textarea name="board_view" id="board_view">${fn:escapeXml(item.board_view)}</textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>작성</h3>
										<textarea name="board_write" id="board_write">${fn:escapeXml(item.board_write)}</textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>수정</h3>
										<textarea name="board_modify" id="board_modify">${fn:escapeXml(item.board_modify)}</textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>답글</h3>
										<textarea name="board_reply" id="board_reply">${fn:escapeXml(item.board_reply)}</textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<h3>인증</h3>
										<textarea name="board_cert" id="board_cert">${fn:escapeXml(item.board_cert)}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		
		<div class="col-sm-4">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>탐색기</h5>
				</div>
				<div class="ibox-content">
					<div id="resources"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
function loadTree() {
	$('#resources').jstree({
		'core' : {
			'data' : {
				'url' : function(node) {
					return "/api/builder/resource/jstree";
				},
				'data' : function(node) {
					return {
						'id' : node.id
					};
				}
			}
		}
	});
	
	$('#resources').on("changed.jstree", function (e, data) {
		console.log(data.node);
		if (data.node.id == "0") {
		} else {
			if (data.node.data.isfolder == "Y") {
			} else {
				console.log(data.node.data);
				var path = '/res/'+data.node.data.idx+'/'+data.node.data.name;
				window.prompt("경로복사", path);
			}
		}
	});
}

function save() {
	header.save();
	footer.save();
	board_list.save();
	board_view.save();
	board_write.save();
	board_modify.save();
	board_reply.save();
	board_cert.save();
	
	$.ajax({type:"POST", url : "/api/builder/template/modify", cache:false
		, data : $("#modifyForm").serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				alert("저장되었습니다.");
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

var header;
var footer;
var board_list;
var board_view;
var board_write;
var board_modify;
var board_reply;
var board_cert;

$(function(){
	loadTree();
	
	header = CodeMirror.fromTextArea(document.getElementById("header"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	footer = CodeMirror.fromTextArea(document.getElementById("footer"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	board_list = CodeMirror.fromTextArea(document.getElementById("board_list"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	board_view = CodeMirror.fromTextArea(document.getElementById("board_view"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	board_write = CodeMirror.fromTextArea(document.getElementById("board_write"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	board_modify = CodeMirror.fromTextArea(document.getElementById("board_modify"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	board_reply = CodeMirror.fromTextArea(document.getElementById("board_reply"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	board_cert = CodeMirror.fromTextArea(document.getElementById("board_cert"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	
	$("#btn-save").click(function(){
		save();
	});
	
	$(window).bind('keydown', function(event) {
		if (event.ctrlKey || event.metaKey) {
			switch (String.fromCharCode(event.which).toLowerCase()) {
			case 's':
				event.preventDefault();
				save();
				break;
			}
		}
	});

});
</script>