<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>템플릿</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>화면관리</li>
			<li class="active"><strong>템플릿</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>템플릿 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">제목</th>
									<th class="text-center">등록일</th>
									<th class="text-center">수정일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td><a href="/builder/templates/${l.idx}">${l.title}</a></td>
										<td class="text-center">
											<fmt:parseDate value="${l.regdate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="MM.dd HH:mm"/>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.moddate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="MM.dd HH:mm"/>
										</td>
										<td class="text-center">
											<c:choose>
												<c:when test="${l.ismain eq 'Y'}">
													<button type="button" class="btn btn-xs btn-primary"><i class="fa fa-check-square"></i></button>
												</c:when>
												<c:otherwise>
													<button type="button" class="btn btn-xs btn-default btn-main" data-idx="${l.idx}"><i class="fa fa-check-square"></i></button>
												</c:otherwise>
											</c:choose>
											<button type="button" class="btn btn-xs btn-warning btn-copy" data-idx="${l.idx}"><i class="fa fa-copy"></i></button>
											<button type="button" class="btn btn-xs btn-danger btn-delete" data-idx="${l.idx}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox float-e-margins" id="siteForm-wrapper">
				<div class="ibox-title">
					<h5>템플릿 생성</h5>
				</div>
				<div class="ibox-content">
					<form id="templateForm" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">제목</label>
							<div class="col-sm-8">
								<input type="text" name="title" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="template-create" class="btn btn-primary">생성</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#template-create").click(function(){
		if (confirm("입력하신 정보로 템플릿을 생성하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/template/create", cache:false
				, data : $("#templateForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-main").click(function(){
		if (confirm("선택하신 템플릿을 메인으로 지정하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url : "/api/builder/template/main", cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-delete").click(function(){
		if (confirm("선택하신 템플릿을 삭제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url : "/api/builder/template/delete", cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-copy").click(function(){
		if (confirm("선택하신 템플릿을 복제하시겠습니까?")) {
			var idx = $(this).data("idx");
			$.ajax({type:"POST", url : "/api/builder/template/copy", cache:false
				, data : {idx:idx}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
});
</script>