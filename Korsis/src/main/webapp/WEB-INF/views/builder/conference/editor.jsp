<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${item.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li><a href="/builder/conference">학술대회</a></li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-save" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content">
	<form id="modifyForm" class="form-horizontal">
		<input type="hidden" name="code" value="${code}" />
		<div class="row">
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-12">
						<div class="ibox">
							<div class="ibox-title">
								<h5>콘텐츠 기본정보</h5>
							</div>
							<div class="ibox-content">
								<div class="row">
									<div class="col-sm-12">
										<h3>내용</h3>
										<textarea name="content" id="content">${fn:escapeXml(item.content)}</textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>학술대회 정보 수정</h5>
					</div>
					<div class="ibox-content">
						<div class="form-group">
							<label class="col-sm-4 control-label">학술대회 이름</label>
							<div class="col-sm-8">
								<input type="text" name="title" value="${item.title}" class="form-control" />
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-4 control-label">템플릿</label>
									<div class="col-sm-8">
										<select name="tidx" class="form-control">
											<option value="">템플릿 선택</option>
											<c:forEach items="${templates}" var="t">
												<option value="${t.idx}" <c:if test="${t.idx eq item.tidx}">selected="selected"</c:if>>${t.title}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">모니터 배경화면 (URL)</label>
							<div class="col-sm-8">
								<input type="text" name="background" value="${item.background}" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사전등록</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="register" <c:if test="${item.register eq 'Y'}"> checked="checked"</c:if>> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="register" <c:if test="${item.register eq 'N'}"> checked="checked"</c:if>> <i></i> 사용안함
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">초록등록</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="abstracts" <c:if test="${item.abstracts eq 'Y'}"> checked="checked"</c:if>> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="abstracts" <c:if test="${item.abstracts eq 'N'}"> checked="checked"</c:if>> <i></i> 사용안함
									</label>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-4 control-label">숙박</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="stay" <c:if test="${item.stay eq 'Y'}"> checked="checked"</c:if>> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="stay" <c:if test="${item.stay eq 'N'}"> checked="checked"</c:if>> <i></i> 사용안함
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">여행/투어</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="tour" <c:if test="${item.tour eq 'Y'}"> checked="checked"</c:if>> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="tour" <c:if test="${item.tour eq 'N'}"> checked="checked"</c:if>> <i></i> 사용안함
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용여부</label>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="Y" name="status" <c:if test="${item.status eq 'Y'}"> checked="checked"</c:if>> <i></i> 사용 
									</label>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="i-checks">
									<label>
										<input type="radio" value="N" name="status" <c:if test="${item.status eq 'N'}"> checked="checked"</c:if>> <i></i> 사용안함
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
function save() {
	content.save();
	$.ajax({type:"POST", url : "/api/builder/conference/modify", cache:false
		, data : $("#modifyForm").serialize()
		, success : function(data) {
			console.log(data);
			if (data.error == 0) {
				alert("저장되었습니다.");
			} else {
				alert(data.message);
			}
		}
		, error : function(data) {
			console.log(data);
			alert(data.responseJSON.error);
		}
	});
}

var content;
$(function(){
	content = CodeMirror.fromTextArea(document.getElementById("content"), {lineNumbers: true, matchBrackets: true, styleActiveLine: true});
	$("#btn-save").click(function(){
		save();
	});
	
	$(window).bind('keydown', function(event) {
		if (event.ctrlKey || event.metaKey) {
			switch (String.fromCharCode(event.which).toLowerCase()) {
			case 's':
				event.preventDefault();
				save();
				break;
			}
		}
	});
});
</script>









