<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${item.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>학회시스템</li>
			<li>워크샵</li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-save" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content">
	<form id="modifyForm" class="form-horizontal">
		<input type="hidden" name="code" value="${code}" />
		<div class="row">
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-title">
						<h5>기본정보</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">게시판 이름</label>
									<div class="col-sm-9">
										<input type="text" name="title" class="form-control" value="${item.title}" />
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">카테고리 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="category" <c:if test="${item.category eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="category" <c:if test="${item.category eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">카테고리</label>
									<div class="col-sm-9">
										<div class="row">
											<div class="col-sm-12">
												<input type="text" name="categories" class="form-control" value="${item.categories}" />
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<small>* 쉼표로 구분 (ex - CAT1,CAT2)</small>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#btn-save").click(function(){
		$.ajax({type:"POST", url : "/api/builder/society/workshop/modify", cache:false
			, data : $("#modifyForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("저장되었습니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>









