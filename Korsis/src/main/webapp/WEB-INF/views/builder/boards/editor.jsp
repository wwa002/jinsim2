<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-8">
		<h2>${item.title}</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li>게시판</li>
			<li class="active"><strong>${item.title}</strong></li>
		</ol>
	</div>
	<div class="col-sm-4">
		<div class="title-action">
			<button type="button" id="btn-save" class="btn btn-primary"><i class="fa fa-save"></i> 저장</button>
		</div>
	</div>
</div>

<div class="wrapper wrapper-content">
	<form id="modifyForm" class="form-horizontal">
		<input type="hidden" name="code" value="${code}" />
		<div class="row">
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-title">
						<h5>기본정보</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">게시판 이름</label>
									<div class="col-sm-9">
										<input type="text" name="title" class="form-control" value="${item.title}" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">템플릿</label>
									<div class="col-sm-9">
										<select name="tidx" class="form-control">
											<option value="">템플릿 선택</option>
											<c:forEach items="${templates}" var="t">
												<option value="${t.idx}" <c:if test="${t.idx eq item.tidx}">selected="selected"</c:if>>${t.title}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">공지 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="notice" <c:if test="${item.notice eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="notice" <c:if test="${item.notice eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">팝업 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="popup" <c:if test="${item.popup eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="popup" <c:if test="${item.popup eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">별명 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="nickname" <c:if test="${item.nickname eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="nickname" <c:if test="${item.nickname eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">비밀 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="hidden" <c:if test="${item.hidden eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="hidden" <c:if test="${item.hidden eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">파일 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="file" <c:if test="${item.file eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="file" <c:if test="${item.file eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">댓글 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="comment" <c:if test="${item.comment eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="comment" <c:if test="${item.comment eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">즐겨찾기 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="favorite" <c:if test="${item.favorite eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="favorite" <c:if test="${item.favorite eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">추천 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="recommend" <c:if test="${item.recommend eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="recommend" <c:if test="${item.recommend eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-title">
						<h5>권한설정</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<input type="hidden" name="permit_list" id="permit_list" value="${item.permit_list}" />
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">목록</label>
									<div class="col-sm-10">
										<label class="checkbox-inline i-checks">
											<input type="checkbox" value="Y" name="permit_list_member" class="" <c:if test="${item.permit_list_member eq 'Y'}">checked="checked"</c:if> /> 회원전용
										</label> 
										<c:forEach items="${grades}" var="g">
											<label class="checkbox-inline i-checks">
												<input type="checkbox" value="${g.gcd}" class="permit_list"> ${g.title} 
											</label>
										</c:forEach>
										<br /> <small>(* 모든 선택 해제시 비회원 접근 가능)</small>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<input type="hidden" name="permit_view" id="permit_view" value="${item.permit_view}" />
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">열람</label>
									<div class="col-sm-10">
										<label class="checkbox-inline i-checks">
											<input type="checkbox" value="Y" name="permit_view_member" class="" <c:if test="${item.permit_view_member eq 'Y'}">checked="checked"</c:if> /> 회원전용
										</label>
										<c:forEach items="${grades}" var="g">
											<label class="checkbox-inline i-checks">
												<input type="checkbox" value="${g.gcd}" class="permit_view"> ${g.title} 
											</label>
										</c:forEach>
										<br /> <small>(* 모든 선택 해제시 비회원 접근 가능)</small>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<input type="hidden" name="permit_write" id="permit_write" value="${item.permit_write}" />
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">작성</label>
									<div class="col-sm-10">
										<label class="checkbox-inline i-checks">
											<input type="checkbox" value="Y" name="permit_write_member" class="" <c:if test="${item.permit_write_member eq 'Y'}">checked="checked"</c:if> /> 회원전용
										</label>
										<c:forEach items="${grades}" var="g">
											<label class="checkbox-inline i-checks">
												<input type="checkbox" value="${g.gcd}" class="permit_write"> ${g.title} 
											</label>
										</c:forEach>
										<br /> <small>(* 모든 선택 해제시 비회원 접근 가능)</small>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<input type="hidden" name="permit_reply" id="permit_reply" value="${item.permit_reply}" />
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">답변</label>
									<div class="col-sm-10">
										<label class="checkbox-inline i-checks">
											<input type="checkbox" value="Y" name="permit_reply_member" class="" <c:if test="${item.permit_reply_member eq 'Y'}">checked="checked"</c:if> /> 회원전용
										</label>
										<c:forEach items="${grades}" var="g">
											<label class="checkbox-inline i-checks">
												<input type="checkbox" value="${g.gcd}" class="permit_reply"> ${g.title} 
											</label>
										</c:forEach>
										<br /> <small>(* 모든 선택 해제시 비회원 접근 가능)</small>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<input type="hidden" name="permit_comment" id="permit_comment" value="${item.permit_comment}" />
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-2 control-label">댓글</label>
									<div class="col-sm-10">
										<label class="checkbox-inline i-checks">
											<input type="checkbox" value="Y" name="permit_comment_member" class="" <c:if test="${item.permit_comment_member eq 'Y'}">checked="checked"</c:if> /> 회원전용
										</label>
										<c:forEach items="${grades}" var="g">
											<label class="checkbox-inline i-checks">
												<input type="checkbox" value="${g.gcd}" class="permit_comment"> ${g.title} 
											</label>
										</c:forEach>
										<br /> <small>(* 모든 선택 해제시 비회원 접근 가능)</small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="ibox">
					<div class="ibox-title">
						<h5>세부설정</h5>
					</div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">카테고리 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="category" <c:if test="${item.category eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="category" <c:if test="${item.category eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">카테고리</label>
									<div class="col-sm-9">
										<div class="row">
											<div class="col-sm-12">
												<input type="text" name="categories" class="form-control" value="${item.categories}" />
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<small>* 쉼표로 구분 (ex - CAT1,CAT2)</small>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">답변형 사용</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="thread" <c:if test="${item.thread eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="thread" <c:if test="${item.thread eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">답변형 삭제</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="thread_delete" <c:if test="${item.thread_delete eq 'Y'}">checked="checked"</c:if>> <i></i> 삭제 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="thread_delete" <c:if test="${item.thread_delete eq 'N'}">checked="checked"</c:if>> <i></i> 삭제방지
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">답변 한계치</label>
									<div class="col-sm-9">
										<select name="thread_limit" class="form-control">
											<c:forEach begin="1" end="10" step="1" var="i">
												<option value="${i}" <c:if test="${i == item.thread_limit}">selected="selected"</c:if>>${i}개</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">New 표시기간</label>
									<div class="col-sm-9">
										<select name="new_day" class="form-control">
											<c:forEach begin="1" end="10" step="1" var="i">
												<option value="${i}" <c:if test="${i == item.new_day}">selected="selected"</c:if>>${i}일</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">정확한 조회수</label>
									<div class="col-sm-9">
										<label class="radio-inline i-checks">
											<input type="radio" value="Y" name="absolute_read_count" <c:if test="${item.absolute_read_count eq 'Y'}">checked="checked"</c:if>> <i></i> 사용 
										</label>
										<label class="radio-inline i-checks">
											<input type="radio" value="N" name="absolute_read_count" <c:if test="${item.absolute_read_count eq 'N'}">checked="checked"</c:if>> <i></i> 사용안함
										</label>
									</div>
								</div>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">페이지별 게시물수</label>
									<div class="col-sm-9">
										<select name="num_per_page" class="form-control">
											<c:forEach begin="5" end="100" step="5" var="i">
												<option value="${i}" <c:if test="${i == item.num_per_page}">selected="selected"</c:if>>${i}개</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-sm-3 control-label">블록당 페이지수</label>
									<div class="col-sm-9">
										<select name="page_per_block" class="form-control">
											<c:forEach begin="5" end="20" step="5" var="i">
												<option value="${i}" <c:if test="${i == item.page_per_block}">selected="selected"</c:if>>${i}개</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
function getPermit(str) {
	var permit = "";
	$("."+str).each(function(i){
		if ($(this).prop("checked")) {
			if (permit == "") {
				permit = $(this).val();
			} else {
				permit += "," + $(this).val();
			}
		}
	});
	return permit;
}
function setPermit(str) {
	var permit = $("#"+str).val();
	if (permit) {
		var arr = permit.split(",");
		for (var i = 0 ; i < arr.length; i++) {
			var p = arr[i];
			$("."+str).each(function(){
				if ($(this).val() == p) {
					$(this).iCheck("check");
				}
			});
		}
	}
}
$(function(){
	setPermit('permit_list');
	setPermit('permit_view');
	setPermit('permit_write');
	setPermit('permit_reply');
	setPermit('permit_comment');
	
	$("#btn-save").click(function(){
		$("#permit_list").val(getPermit('permit_list'));
		$("#permit_view").val(getPermit('permit_view'));
		$("#permit_write").val(getPermit('permit_write'));
		$("#permit_reply").val(getPermit('permit_reply'));
		$("#permit_comment").val(getPermit('permit_comment'));
		
		$.ajax({type:"POST", url : "/api/builder/board/modify", cache:false
			, data : $("#modifyForm").serialize()
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("저장되었습니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
});
</script>









