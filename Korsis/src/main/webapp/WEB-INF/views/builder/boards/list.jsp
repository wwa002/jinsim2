<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<h2>게시판</h2>
		<ol class="breadcrumb">
			<li><a href="/builder/dashboard">HOME</a></li>
			<li class="active"><strong>게시판</strong></li>
		</ol>
	</div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-title">
					<h5>게시판 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="row">
						<div class="col-sm-12">
							<form id="searchForm" class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-6">
										<div class="input-group">
											<input type="text" name="keyword" placeholder="검색" class="form-control" value="${param.keyword}" />
											<span class="input-group-btn">
												<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">제목</th>
									<th class="text-center">경로</th>
									<th class="text-center">템플릿</th>
									<th class="text-center">등록일</th>
									<th class="text-center">수정일</th>
									<th class="text-center">삭제</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="virtualRecordNo" value="${paging.virtualRecordNo}" />
								<c:forEach items="${list}" var="l">
									<tr>
										<td class="text-center">
											${virtualRecordNo}
											<c:set var="virtualRecordNo" value="${virtualRecordNo-1}" />
										</td>
										<td><a href="/builder/boards/${l.code}">${l.title}</a></td>
										<td>
											/board/${l.code}
										</td>
										<td class="text-center">
											<c:if test="${l.tidx ne null and l.tidx ne '' }">
												<a href="/builder/templates/${l.tidx}">${l.template}</a>
											</c:if>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.regdate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="yyyy.MM.dd HH:mm:ss"/>
										</td>
										<td class="text-center">
											<fmt:parseDate value="${l.moddate}" var="date" pattern="yyyyMMddHHmmss"/>
											<fmt:formatDate value="${date}" pattern="yyyy.MM.dd HH:mm:ss"/>
										</td>
										<td class="text-center">
											<button type="button" class="btn btn-xs btn-danger btn-delete" data-code="${l.code}"><i class="fa fa-trash"></i></button>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<div class="text-center">
						<ul class="pagination">
		                    <li>
		                        <a href="javascript:goPage(${paging.prevBlockNo}, '&keyword=${param.keyword}');">&laquo;</a>
		                    </li>
		                    <c:forEach begin="${paging.startPageNo}" end="${paging.endPageNo}" step="1" var="i">
		                    	<c:choose>
		                    		<c:when test="${i eq paging.pageNo }">
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:when>
		                    		<c:otherwise>
		                    			<li>
					                        <a href="javascript:goPage(${i}, '&keyword=${param.keyword}');" title="${i} 페이지로 이동">${i}</a>
					                    </li>
		                    		</c:otherwise>
		                    	</c:choose>
		                    </c:forEach>
		                    <li>
		                        <a href="javascript:goPage(${paging.nextBlockNo}, '&keyword=${param.keyword}');">&raquo;</a>
		                    </li>
		                </ul>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox float-e-margins" id="siteForm-wrapper">
				<div class="ibox-title">
					<h5>게시판 생성</h5>
				</div>
				<div class="ibox-content">
					<form id="boardForm" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">게시판 코드</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" name="code" id="code" maxlength="30" class="form-control" />
									<span class="input-group-btn">
										<button type="button" id="code-check" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">게시판 이름</label>
							<div class="col-sm-8">
								<input type="text" name="title" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="board-create" class="btn btn-primary">생성</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<script type="text/javascript">
$(function(){
	$("#code-check").click(function(){
		var code = $("#code").val();
		$.ajax({type:"POST", url : "/api/builder/check/board", cache:false
			, data : {code:code}
			, success : function(data) {
				console.log(data);
				if (data.error == 0) {
					alert("사용할 수 있는 코드 입니다.");
				} else {
					alert(data.message);
				}
			}
			, error : function(data) {
				console.log(data);
				alert(data.responseJSON.error);
			}
		});
	});
	
	$("#board-create").click(function(){
		if (confirm("입력하신 정보로 게시판을 생성하시겠습니까?")) {
			$.ajax({type:"POST", url : "/api/builder/board/create", cache:false
				, data : $("#boardForm").serialize()
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						document.location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
	$(".btn-delete").click(function(){
		if (confirm("게시판을 삭제하시겠습니까?\n삭제한 게시판은 되돌릴 수 없습니다.")) {
			var code = $(this).data("code");
			$.ajax({type:"POST", url : "/api/builder/board/delete", cache:false
				, data : {code:code}
				, success : function(data) {
					console.log(data);
					if (data.error == 0) {
						location.reload();
					} else {
						alert(data.message);
					}
				}
				, error : function(data) {
					console.log(data);
					alert(data.responseJSON.error);
				}
			});
		}
	});
	
});
</script>