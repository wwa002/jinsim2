<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/WEB-INF/views/include/builder-header.jsp"%>
<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-8 col-md-8">
			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>접속량</h5>
						</div>
						<div class="ibox-content">
							<div class="flot-chart">
								<div class="flot-chart-content" id="flot-dashboard-chart"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4">
			<div class="row">
				<div class="col-lg-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-success pull-right">전체</span>
							<h5>사이트</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins"><fmt:formatNumber value="${stats.site}" pattern="#,###" />개</h1>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-info pull-right">전체</span>
							<h5>회원수</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins">총 <fmt:formatNumber value="${stats.member}" pattern="#,###" />명</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-primary pull-right">오늘</span>
							<h5>접속자</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins"><fmt:formatNumber value="${stats.visit}" pattern="#,###" /> 명</h1>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<span class="label label-danger pull-right">오늘</span>
							<h5>페이지 뷰</h5>
						</div>
						<div class="ibox-content">
							<h1 class="no-margins"><fmt:formatNumber value="${stats.page}" pattern="#,###" /> 건</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>사이트</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table id="site-list" class="table table-striped table-bordered table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
								<col class="col-sm-2" />
								<col class="col-sm-1" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">코드</th>
									<th class="text-center">사이트명</th>
									<th class="text-center">이메일</th>
									<th class="text-center">사용여부</th>
									<th class="text-center">생성일</th>
									<th class="text-center">관리</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="ibox float-e-margins" id="siteForm-wrapper">
				<div class="ibox-title">
					<h5>사이트 생성</h5>
				</div>
				<div class="ibox-content">
					<form id="siteForm" class="form-horizontal" onsubmit="return false;">
						<div class="form-group">
							<label class="col-sm-4 control-label">사이트 코드</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" name="sitecd" id="sitecd" class="form-control" />
									<span class="input-group-btn">
										<button type="button" id="sitecd-check" class="btn btn-default"><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사이트 명</label>
							<div class="col-sm-8">
								<input type="text" name="title" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">관리자 메일</label>
							<div class="col-sm-8">
								<input type="email" name="email" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="site-create" class="btn btn-primary">생성</button>
								<button type="reset" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="ibox float-e-margins" id="siteModifyForm-wrapper" style="display:none;">
				<div class="ibox-title">
					<h5>사이트 수정</h5>
				</div>
				<div class="ibox-content">
					<form id="siteModifyForm" class="form-horizontal" onsubmit="return false;">
						<input type="hidden" name="idx" id="idx-modify" />
						<div class="form-group">
							<label class="col-sm-4 control-label">사이트 명</label>
							<div class="col-sm-8">
								<input type="text" name="title" id="title-modify" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">관리자 메일</label>
							<div class="col-sm-8">
								<input type="email" name="email" id="email-modify" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">사용상태</label>
							<div class="col-sm-8">
								<select name="isused" id="isused-modify" class="form-control">
									<option value="Y">사용</option>
									<option value="T">테스트</option>
									<option value="N">사용중지</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-4">
								<button type="button" id="site-modify" class="btn btn-warning">수정</button>
								<button type="reset" id="site-modify-cancel" class="btn btn-default">취소</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>서버 목록</h5>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table id="server-list" class="table table-striped table-hover">
							<colgroup>
								<col class="col-sm-1" />
								<col class="col-sm-*" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-1" />
								<col class="col-sm-2" />
								<col class="col-sm-2" />
							</colgroup>
							<thead>
								<tr>
									<th class="text-center">번호</th>
									<th class="text-center">서버명</th>
									<th class="text-center">서버종류</th>
									<th class="text-center">사용 메모리</th>
									<th class="text-center">여유 메모리</th>
									<th class="text-center">최대 메모리</th>
									<th class="text-center">서버 생성일</th>
									<th class="text-center">상태 변동일 </th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<%@ include file="/WEB-INF/views/include/builder-footer.jsp"%>
<!-- Flot -->
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.js"></script>
<script src="/resources/bootstrap/inspinia/js/plugins/flot/jquery.flot.time.js"></script>

<!-- Peity -->
<script src="/resources/bootstrap/inspinia/js/plugins/peity/jquery.peity.min.js"></script>

<!-- EayPIE -->
<script src="/resources/bootstrap/inspinia/js/plugins/easypiechart/jquery.easypiechart.js"></script>

<script type="text/javascript">
function gd(year, month, day) {
	return new Date(year, month - 1, day).getTime();
}
var data2 = [];
	<c:forEach items="${counter}" var="l">
		<fmt:parseDate value="${l.date}" var="date" pattern="yyyyMMdd"/>
	data2.push([gd(<fmt:formatDate value="${date}" pattern="yyyy, MM, dd"/>), parseInt('${l.visit}')]);
	</c:forEach>
var data3 = [];
	<c:forEach items="${counter}" var="l">
		<fmt:parseDate value="${l.date}" var="date" pattern="yyyyMMdd"/>
	data3.push([gd(<fmt:formatDate value="${date}" pattern="yyyy, MM, dd"/>), parseInt('${l.page}')]);
	</c:forEach>
</script>

<script src="/resources/js/builder-dashboard.js"></script>
