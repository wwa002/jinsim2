<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="ko">
<!--<![endif]-->
<head>
<title>한국통증중재시술연구회</title>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicon -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Web Fonts -->
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="/resources/bootstrap/unify/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/resources/bootstrap/unify/css/style.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="/resources/modules/korsis/css/header.css">
<link rel="stylesheet" href="/resources/bootstrap/unify/css/footers/footer-v1.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="/resources/bootstrap/unify/plugins/animate.css">
<link rel="stylesheet" href="/resources/bootstrap/unify/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="/resources/bootstrap/unify/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/resources/bootstrap/unify/plugins/parallax-slider/css/parallax-slider.css">

<!-- CSS Theme -->
<link rel="stylesheet" href="/resources/bootstrap/unify/css/theme-colors/default.css" id="style_color">
<link rel="stylesheet" href="/resources/bootstrap/unify/css/theme-skins/dark.css">

<!-- DatePicker -->
<link href="/resources/bootstrap/inspinia/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<!-- CSS Customization -->
<link rel="stylesheet" href="/resources/modules/korsis/css/style.css">
</head>

<body>
	<div class="wrapper">
		<!--=== Header ===-->
		<div class="header">
			<!-- Topbar -->
			<div class="topbar">
				<div class="container">
					<ul class="loginbar pull-right">
						<li><a href="/">HOME </a></li>
						<li class="menu-admin hidden"><a href="/admin">관리자 </a></li>
						<li class="menu-nomember hidden"><a href="/site/join">회원가입 </a></li>
						<li class="menu-nomember hidden"><a href="/site/login">로그인 </a></li>
						<li class="menu-member hidden"><a href="/logout">로그아웃 </a></li>
						<li><a href="/site/introduce/office">사무국 안내 </a></li>
					</ul>
				</div>
			</div>
			<!-- End Topbar -->
				
			<div class="container">
				<!-- Logo -->
				<a class="logo" href="/"><img src="/res/images/logo.png" alt="Logo"></a>
				<!-- End Logo -->

				<!-- Toggle get grouped for better mobile display -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span> <span class="fa fa-bars"></span>
				</button>
				<!-- End Toggle -->
			</div>
			<!--/end container-->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
				<div class="container">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 연구회 소개</a>
							<ul class="dropdown-menu">
								<li><a href="/site/introduce">인사말</a></li>
								<li><a href="/site/introduce/officer">임원/강사진</a></li>
								<li><a href="/site/introduce/history">연혁</a></li>
								<li><a href="/site/introduce/bylaw">회칙</a></li>
								<li><a href="/site/introduce/goals/1">목적</a></li>
								<li><a href="/site/introduce/office">사무국 안내</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 알림 광장</a>
							<ul class="dropdown-menu">
								<li><a href="/board/notice">공지사항</a></li>
								<li><a href="/board/news">연구회 소식</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 학술 행사</a>
							<ul class="dropdown-menu">
								<li><a href="/site/conference/workshop?code=schedule">행사소식</a></li>
								<li><a href="/site/conference/symposium?code=symposium">연구회 학술대회</a></li>
								<li><a href="/board/gallery">행사 포토갤러리</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 회원광장</a>
							<ul class="dropdown-menu">
								<li><a href="/board/free">게시판</a></li>
								<li><a href="/site/search/member">회원검색</a></li>
								<li><a href="/site/search/hospital">병원검색</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 자료실</a>
							<ul class="dropdown-menu">
								<li><a href="/board/lecture">자료실</a></li>
								<li><a href="/board/dissertation">논문검색</a></li>
								<li><a href="/board/isis">ISIS newsletter</a></li>
							</ul>
						</li>

						
					</ul>
				</div>
				<!--/end container-->
			</div>
			<!--/navbar-collapse-->
		</div>
		<!--=== End Header ===-->
		
		<!-- Content Start -->

		<!--=== Slider ===-->
		<div class="slider-inner">
			<div id="da-slider" class="da-slider">
				<div class="da-slide">
					<h2>
						한국통증중재시술연구회는 통증에 관심이 있는<br />모든 의사선생님께 개방된 연구회입니다.
					</h2>
					<p>
						특히 우리는 연구회는 Evdence Based Medicine을 추구하며,<br />
						통증의 비수술적 치료 발전을 위해, 세계적인 연구가 되고자 노력하고 있습니다.
					</p>
					<div class="da-img">
						<img class="img-responsive" src="/res/images/slider/main_01.png" alt="">
					</div>
				</div>
				<div class="da-slide">
					<h2>
						한국통증중재시술연구회는 통증에 관심이 있는<br />
						모든 의사선생님께 개방된 연구회입니다.
					</h2>
					<p>
						특히 우리는 연구회는 Evdence Based Medicine을 추구하며,<br />
						통증의 비수술적 치료 발전을 위해, 세계적인 연구가 되고자 노력하고 있습니다.
					</p>
					<div class="da-img">
						<img class="img-responsive" src="/res/images/slider/main_02.png" alt="">
					</div>
				</div>
				<div class="da-arrows">
					<span class="da-arrows-prev"></span> <span class="da-arrows-next"></span>
				</div>
			</div>
		</div>
		<!--/slider-->
		<!--=== End Slider ===-->

		<!--=== Purchase Block ===-->
		<div class="purchase">
			<div class="container overflow-h">
				<div class="row">
					<div class="col-md-9 animated fadeInLeft">
						<span>Unify is a clean and fully responsive incredible
							Template.</span>
						<p>At vero eos et accusamus et iusto odio dignissimos ducimus
							qui blanditiis praesentium voluptatum deleniti atque corrupti
							quos dolores et quas molestias excepturi vehicula sem ut
							volutpat. Ut non libero magna fusce condimentum eleifend enim a
							feugiat corrupti quos.</p>
					</div>
					<div class="col-md-3 btn-buy animated fadeInRight">
						<a href="#" class="btn-u btn-u-lg"><i
							class="fa fa-cloud-download"></i> Download Now</a>
					</div>
				</div>
			</div>
		</div>
		<!--/row-->
		<!-- End Purchase Block -->

		<div class="container">
		<!-- Recent Works -->
			<div class="row margin-bottom-20 main-banner-link">
				<div class="col-md-2 col-sm-3 main-banner-link">
					<div class="thumbnails thumbnail-style thumbnail-kenburn main-notice">
						<div class="caption text-center">
							<h3><a class="hover-effect" href="#"><i class="fa fa-list"></i> 공지사항</a></h3>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 main-banner-link">
					<div class="thumbnails thumbnail-style thumbnail-kenburn main-pds">
						<div class="caption text-center">
							<h3><a class="hover-effect" href="#"><i class="fa fa-list"></i> 자료실</a></h3>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 main-banner-link">
					<div class="thumbnails thumbnail-style thumbnail-kenburn main-qna">
						<div class="caption text-center">
							<h3><a class="hover-effect" href="#"><i class="fa fa-list"></i> Q&amp;A 게시판</a></h3>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 main-banner-link">
					<div class="thumbnails thumbnail-style thumbnail-kenburn main-search-member">
						<div class="caption text-center">
							<h3><a class="hover-effect" href="#"><i class="fa fa-list"></i> 회원 검색</a></h3>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 main-banner-link">
					<div class="thumbnails thumbnail-style thumbnail-kenburn main-search-hospital">
						<div class="caption text-center">
							<h3><a class="hover-effect" href="#"><i class="fa fa-list"></i> 병원 찾기</a></h3>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-3 main-banner-link">
					<div class="thumbnails thumbnail-style thumbnail-kenburn main-gallery">
						<div class="caption text-center">
							<h3><a class="hover-effect" href="#"><i class="fa fa-list"></i> 포토갤러리</a></h3>
						</div>
					</div>
				</div>
			</div>
			<!-- End Recent Works -->
		</div>

		<!-- Content End -->
		

		<!--=== Footer Version 1 ===-->
		<div class="footer-v1">
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<img src="/res/images/logo_footer.png" alt="" class="img-responsive" />
						</div>
						<div class="col-md-6">
							<p>
								COPYRIGHT BY THE KOREAN PAIN INTERVENTION SOCIETY. ALL RIGHTS RESERVED.<br />
								서울시 서대문구 연세로 50-1 (연세의대 신경외과학교실)
							</p>
						</div>
						<!-- Social Links -->
						<div class="col-md-3">
							<ul class="footer-socials list-inline">
								<li><a href="#">개인정보취급방침</a></li>
								<li><a href="#">이메일 무단 수집 거부</a></li>
								<li><a href="#">사무국</a></li>
							</ul>
						</div>
						<!-- End Social Links -->
					</div>
				</div>
			</div>
			<!--/copyright-->
		</div>
		<!--=== End Footer Version 1 ===-->
	</div>
	<!--/wrapper-->

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/jquery/jquery-migrate.min.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/bootstrap/js/bootstrap.min.js"></script>
	
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/back-to-top.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/smoothScroll.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/parallax-slider/js/modernizr.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/parallax-slider/js/jquery.cslider.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="/resources/bootstrap/unify/js/custom.js"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="/resources/bootstrap/unify/js/app.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/js/plugins/owl-carousel.js"></script>
	<script type="text/javascript" src="/resources/bootstrap/unify/js/plugins/parallax-slider.js"></script>
	
	<!-- Data picker -->
	<script src="/resources/bootstrap/inspinia/js/plugins/datapicker/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_KrFi4wysdJLy7SahETqhPM1Y3R7lIdc"></script>
	
	<!-- Template -->
	<script src="/resources/plugins/mustache/mustache.min.js"></script>
	
	<!-- Calendar -->
	<script src='/resources/plugins/moment/moment.min.js'></script>
	<script src='/resources/plugins/fullcalendar/fullcalendar.js'></script>
    <script src='/resources/plugins/fullcalendar/locale/ko.js'></script>
	
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			ParallaxSlider.initParallaxSlider();
		});
	</script>
	
	<script src="/resources/modules/korsis/js/site.js"></script>
	
	<!--[if lt IE 9]>
		<script src="/resources/bootstrap/unify/plugins/respond.js"></script>
		<script src="/resources/bootstrap/unify/plugins/html5shiv.js"></script>
		<script src="/resources/bootstrap/unify/plugins/placeholder-IE-fixes.js"></script>
	<![endif]-->

</body>
</html>
